/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { typography } from './src/utils/typography'
// import { enableScreens } from 'react-native-screens';
import CodePush from 'react-native-code-push';
console.log('ddd')
// enableScreens();
typography()
AppRegistry.registerComponent(appName, () =>
    CodePush({
        // updateDialog: {
        //     optionalInstallButtonLabel: 'Cài đặt',
        //     optionalIgnoreButtonLabel: 'Bỏ qua',
        //     title: 'Cập nhật có sẵn',
        //     optionalUpdateMessage: 'Đã có bản cập nhật, bạn có muốn cài đặt nó?',
        //     mandatoryUpdateMessage: 'Đã có bản cập nhật mới!',
        //     mandatoryContinueButtonLabel: 'Cập nhật ngay'
        // },
        // checkFrequency: CodePush.CheckFrequency.ON_APP_START, // bat dau
        checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,  // bat cu khi nao
        installMode: CodePush.InstallMode.IMMEDIATE,
        mandatoryInstallMode: CodePush.InstallMode.IMMEDIATE,
    })(App)
);