import * as Types2 from '../constants/ActionType2';
import HTTP from '../services/HTTP';
import Toast from 'react-native-simple-toast';

export const actGetPoup = () => {
    return dispatch => {
        return HTTP.callApi('popup', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types2.POUP_LIST_SUCCESS, data: response.data.data });
            } else {
                // dispatch({ type: Types.BANNER_LIST_FAILURE });
                // Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
        });
    };
}

export const actGetVersion = () => {
    return dispatch => {
        return HTTP.callApiWithHeader('setting-version', 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                return new Promise.resolve({ status: 200, data: response.data.data });

                // dispatch({ type: Types2.POUP_LIST_SUCCESS, data: response.data.data });
            } else {
                return new Promise.resolve({ status: -1 });

                // dispatch({ type: Types2.BANNER_LIST_FAILURE });
                // Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
        });
    };
}




