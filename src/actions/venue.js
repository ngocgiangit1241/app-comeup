import * as Types from '../constants/ActionType';
import * as Types2 from '../constants/ActionType2';
import HTTP from '../services/HTTP';
import NavigationService from '../router/NavigationService';
import firebase from 'react-native-firebase';
import Toast from 'react-native-simple-toast';
import { stat } from 'react-native-fs';

export const actVenueLists = (id, type) => {
    var url = '';
    if (type === 'city') {
        url = `venues?page=1&per_page=12&city_id=${id}`;
    } else {
        url = `venues?page=1&per_page=12&iso_code=${id}`;
    }
    return dispatch => {
        dispatch({ type: Types.VENUE_LIST_REQUEST });
        return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.VENUE_LIST_SUCCESS, data: response.data.data, page: 1 });
            } else {
                dispatch({ type: Types.VENUE_LIST_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.VENUE_LIST_FAILURE });
        });
    };
}

export const actListVenueList = (tags, area, page, time_open, status) => {
    if (!time_open) {
        time_open = ''
    }
    return dispatch => {
        dispatch({ type: Types2.LOAD_DATA_LIST_VENUE_REQUEST, check: 1 });

        return HTTP.callApiWithHeader(`collection/venues?tag=${tags}&area=${area}&page=${page}&open_now=${time_open}&status_venue=${status}`, 'GET', null).then(response => {
            // console.log('response11', response)
            if (response && response.data.status == 200) {
                dispatch({ type: Types2.LOAD_DATA_LIST_VENUE_SUCCESS, data: response.data.data, page, last_page: response.data.last_page, check: 1 });
            } else {
                dispatch({ type: Types2.LOAD_DATA_LIST_VENUE_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types2.LOAD_DATA_LIST_VENUE_FAILURE });
        });
    };
}
export const actListVenueList2 = (tags, area, page, time_open, status) => {
    if (!time_open) {
        time_open = ''
    }
    return dispatch => {
        dispatch({ type: Types2.LOAD_DATA_LIST_VENUE_REQUEST, check: 2 });

        return HTTP.callApiWithHeader(`collection/venues?tag=${tags}&area=${area}&page=${page}&open_now=${time_open}&status_venue=${status}`, 'GET', null).then(response => {
            // console.log('response12', response)
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_LIST_VENUE_SUCCESS, data: response.data.data, page, last_page: response.data.last_page, check: 2 });
            } else {
                dispatch({ type: Types2.LOAD_DATA_LIST_VENUE_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types2.LOAD_DATA_LIST_VENUE_FAILURE });
        });
    };
}
export const actSaveVenueNews = (Slug, body, navigation) => {
    return dispatch => {
        dispatch({ type: Types.SAVE_VENUE_NEWS_REQUEST });
        return HTTP.callApiWithHeader('venues/' + Slug + '/venue-news', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.SAVE_VENUE_NEWS_SUCCESS, data: response.data.data })
                return new Promise.resolve({ status: 200 });
            } else {
                dispatch({ type: Types.SAVE_VENUE_NEWS_FAILURE });
                if (response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.SAVE_VENUE_NEWS_FAILURE });
        });
    };
}

export const onUpdateVenueNews = (Id, body, navigation) => {
    return dispatch => {
        dispatch({ type: Types.UPDATE_VENUE_NEWS_REQUEST });
        return HTTP.callApiWithHeader('venue-news/' + Id + '/update', 'POST', body).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.UPDATE_VENUE_NEWS_SUCCESS, data: response.data.data, id: Id });
                dispatch({ type: Types.UPDATE_VENUE_NEWS_LIST_SUCCESS, data: response.data.data, id: Id });
                return new Promise.resolve({ status: 200 });
            } else {
                dispatch({ type: Types.UPDATE_VENUE_NEWS_FAILURE });
                if (response.data.status === -1) {
                    Toast.showWithGravity(JSON.stringify(response.data.errors), Toast.SHORT, Toast.TOP)
                } else {
                    Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                }
            }
        }).catch(function (error) {
            dispatch({ type: Types.UPDATE_VENUE_NEWS_FAILURE });
        });
    };
}

export const actGetBanners = (body, id) => {
    return dispatch => {
        return HTTP.callApiWithHeader(`banners/venue`, 'GET', body).then(response => {
            if (response && response.data.status == 200) {
                return new Promise.resolve({ status: 200, data: response.data.data })
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                return new Promise.resolve({ status: -1 })
            }
        }).catch(function (error) {
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
        });
    };
}

export const actCreateVenue = (body, id) => {
    return dispatch => {
        return HTTP.callApiWithHeader(`teams/${id}/venues/create`, 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                return new Promise.resolve({ status: 200, id: response.data.data.Id })
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data.errors : response), Toast.SHORT, Toast.TOP)
                return new Promise.resolve({ status: -1 })
            }
        }).catch(function (error) {
            dispatch({ type: Types.VENUE_LIST_FAILURE });
            Toast.showWithGravity(JSON.stringify(response.data ? response.data.errors : response), Toast.SHORT, Toast.TOP)
        });
    };
}

export const actEditVenue = (body, id) => {
    return dispatch => {
        return HTTP.callApiWithHeader(`venues/${id}/update`, 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                return new Promise.resolve({ status: 200 })
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                return new Promise.resolve({ status: -1 })
            }
        }).catch(function (error) {
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
        });
    };
}

export const actListVenueMap = (tags, area, time_open, status, North = '', South = '', East = '', West = '',) => {
    if (!time_open) {
        time_open = ''
    }

    return dispatch => {
        dispatch({ type: Types.VENUE_LIST_REQUEST });
        return HTTP.callApiWithHeader(`search/venue/in-map?tag=${tags}&area=${area}&open_now=${time_open}&status_venue=${status}&North=${North}&South=${South}&East=${East}&West=${West}`, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.VENUEMAP_LIST_SUCCESS, data: response.data.data, last_page: response.data.last_page });
            } else {
                dispatch({ type: Types.VENUEMAP_LIST_FAILURE });
                // Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.VENUEMAP_LIST_FAILURE });
        });
    };
}

export const actDeletePhoTo = (Id, index) => {
    return dispatch => {
        // dispatch({ type: Types.VENUE_PHOTO_REQUEST });
        return HTTP.callApiWithHeader(`venues/image/${Id}/delete`, 'POST', null).then(response => {
            // console.log('response', response);
            if (response && response.data.status == 200) {
                dispatch({ type: Types.VENUE_DELETE_PHOTO_SUCCESS, data: index });
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                // dispatch({ type: Types.VENUE_PHOTO_FAILURE });
            }
        }).catch(function (error) {
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            // dispatch({ type: Types.VENUE_PHOTO_FAILURE });
        });
    };
}

export const actGetFullPhoto = (Id, page) => {
    return dispatch => {
        dispatch({ type: Types.VENUE_PHOTO_REQUEST });
        return HTTP.callApiWithHeader(`venues/${Id}/photos?page=${page}?per_page=2`, 'GET', null).then(response => {
            // console.log('response22', response);
            if (response && response.data.status == 200) {
                dispatch({ type: Types.VENUE_PHOTO_SUCCESS, data: response.data.data, page: response.data.last_page });
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                dispatch({ type: Types.VENUE_PHOTO_FAILURE });
            }
        }).catch(function (error) {
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            dispatch({ type: Types.VENUE_PHOTO_FAILURE });
        });
    };
}

export const actLikeVenues1 = (Id) => {
    return dispatch => {
        return HTTP.callApiWithHeader(`likes/venue`, 'POST', { VenueId: Id }).then(response => {
            if (response && response.data.status == 200) {
                return new Promise.resolve({ status: 200 });
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                return new Promise.resolve({ status: -1 });
            }
        }).catch(function (error) {
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
        });
    };
}

export const actLikeVenues = (id, user = null) => {
    var body = {
        VenueId: id
    }
    return dispatch => {
        dispatch({ type: Types2.LIKE_VENUE_REQUEST, id });
        dispatch({ type: Types2.LIKE_VENUE_IN_TEAM_REQUEST, id });
        dispatch({ type: Types2.LIKE_VENUE_IN_SEARCH_REQUEST, id });
        return HTTP.callApiWithHeader('likes/venue', 'POST', body, true).then(response => {
            if (response && response.data.status == 200) {
                if (response.data.like === 'liked') {
                    firebase.analytics().logEvent('like_venue', { venue_id: id });
                }
                dispatch({ type: Types2.LIKE_VENUE_SUCCESS, id, user });
                return new Promise.resolve({ status: 200 });
            } else if (response && response.data.status == 401) {
                dispatch({ type: Types2.LIKE_VENUE_FAILURE, id });
                dispatch({ type: Types2.LIKE_VENUE_IN_TEAM_FAILURE, id });
                dispatch({ type: Types.SHOW_MODAL_LOGIN });
                return new Promise.resolve({ status: -1 });
            } else {
                dispatch({ type: Types2.LIKE_VENUE_FAILURE, id });
                dispatch({ type: Types2.LIKE_VENUE_IN_TEAM_FAILURE, id });
                dispatch({ type: Types2.LIKE_VENUE_IN_SEARCH_FAILURE, id });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                return new Promise.resolve({ status: -1 });
            }
        }).catch(function (error) {
            dispatch({ type: Types2.LIKE_VENUE_FAILURE, id });
            dispatch({ type: Types2.LIKE_VENUE_IN_TEAM_FAILURE, id });
            dispatch({ type: Types2.LIKE_VENUE_IN_SEARCH_FAILURE, id });
        });
    };
}

export const actLikeVenueNews = (VenueNewsId) => {
    var body = {
        VenueNewsId
    }
    return dispatch => {
        dispatch({ type: Types2.LIKE_VENUE_NEWS_REQUEST, id: VenueNewsId });
        dispatch({ type: Types2.UPDATE_LIKE_VENUE_NEWS_REQUEST, id: VenueNewsId });
        return HTTP.callApiWithHeader('likes/venue-news', 'POST', body, true).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types2.LIKE_VENUE_NEWS_SUCCESS, id: VenueNewsId });
            } else if (response && response.data.status == 401) {
                dispatch({ type: Types2.LIKE_VENUE_NEWS_FAILURE, id: VenueNewsId });
                dispatch({ type: Types2.UPDATE_LIKE_VENUE_NEWS_FAILURE, id: VenueNewsId });
                dispatch({ type: Types.SHOW_MODAL_LOGIN });
            } else {
                dispatch({ type: Types2.LIKE_VENUE_NEWS_FAILURE, id: VenueNewsId });
                dispatch({ type: Types2.UPDATE_LIKE_VENUE_NEWS_FAILURE, id: VenueNewsId });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types2.LIKE_VENUE_NEWS_FAILURE, id: VenueNewsId });
            dispatch({ type: Types2.UPDATE_LIKE_VENUE_NEWS_FAILURE, id: VenueNewsId });
        });
    };
}

export const actUploadPhoto = (Id, photo) => {
    // console.log('photo', photo)
    return dispatch => {
        dispatch({ type: Types.VENUE_UPLOAD_REQUEST })
        return HTTP.callApiWithHeader(`venues/${Id}/ajax/image`, 'POST', photo, true).then(response => {
            // console.log('response21', response);
            if (response && response.data.status == 200 && response.data.data.length > 0) {
                dispatch({ type: Types.VENUE_UPLOAD_SUCCESS, data: response.data.data })
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                dispatch({ type: Types.VENUE_UPLOAD_FAILURE })
            }
        }).catch(function (error) {
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
        });
    };
}
export const actGetVenueDetail = (Id) => {
    return dispatch => {
        dispatch({ type: Types.VENUE_DETAIL_REQUEST });
        return HTTP.callApiWithHeader(`venues/${Id}`, 'GET', null).then(response => {
            // console.log('==============================');
            // console.log('response', response);
            // console.log('==============================');

            if (response && response.data.status == 200) {
                dispatch({ type: Types.VENUE_DETAIL_SUCCESS, data: response.data.data });
            } else if (response.data.status === 404) {
                NavigationService.goBack()
                Toast.showWithGravity('404-NotFound', Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types.VENUE_DETAIL_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.VENUE_DETAIL_FAILURE });
        });
    };
}
export const actGetVenueDetailPhoto = (Id) => {
    return dispatch => {
        // dispatch({ type: Types.VENUE_PHOTO_DETAIL_REQUEST });
        return HTTP.callApiWithHeader(`venues/${Id}/photos?page=1`, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.VENUE_PHOTO_DETAIL_SUCCESS, data: response.data.data });
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                // dispatch({ type: Types.VENUE_PHOTO_DETAIL_FAILURE });
            }
        }).catch(function (error) {
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            // dispatch({ type: Types.VENUE_PHOTO_DETAIL_FAILURE });
        });
    };
}

export const actLoadDataVenueNews = (slug, page, iso_code) => {
    return dispatch => {
        // dispatch({ type: Types.LOAD_DATA_VENUE_NEWS_REQUEST });
        return HTTP.callApiWithHeader('venues/' + slug + '/venue-news?page=' + page + '&iso_code=' + iso_code, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_VENUE_NEWS_SUCCESS, data: response.data.data, total: response.data.total, page });
            } else {
                dispatch({ type: Types.LOAD_DATA_VENUE_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_VENUE_NEWS_FAILURE });
        });
    };
}

export const actEditVenueNews = (Id) => {
    return dispatch => {
        dispatch({ type: Types.EDIT_VENUE_NEWS_REQUEST });
        return HTTP.callApiWithHeader('venue-news/' + Id, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.EDIT_VENUE_NEWS_SUCCESS, data: response.data.data });
                return new Promise.resolve({ status: 200, data: response.data.data });
            } else {
                dispatch({ type: Types.EDIT_VENUE_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.EDIT_VENUE_NEWS_FAILURE });
        });
    };
}

export const actDeleteVenueNews = (Id) => {
    return dispatch => {
        dispatch({ type: Types.DELETE_VENUE_NEWS_REQUEST });
        return HTTP.callApiWithHeader('venue-news/' + Id + '/delete', 'POST', null).then(response => {
            if (response && response.data.status === 200) {
                dispatch({ type: Types.DELETE_VENUE_NEWS_SUCCESS, id: Id });
                return new Promise.resolve({ status: 200 });
            } else {
                dispatch({ type: Types.DELETE_VENUE_NEWS_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.DELETE_VENUE_NEWS_FAILURE });
        });
    };
}

export const actReportVenueNews = (id, content, navigation) => {
    return dispatch => {
        dispatch({ type: Types2.VENUE_REPORT_REQUEST });
        let body = {
            VenueNewsId: id,
            Content: content
        };
        return HTTP.callApiWithHeader('venue-news/' + id + '/report', 'post', body, null).then(response => {
            if (response && response.status == 200) {
                if (response.data.status == 200) {
                    dispatch({ type: Types2.VENUE_REPORT_SUCCESS });
                    navigation.goBack();
                    Toast.showWithGravity(response.data.messages.message, Toast.LONG, Toast.TOP)
                } else if (response.data.status == 401) {
                    dispatch({ type: Types2.VENUE_REPORT_FAILURE });
                    dispatch({ type: Types.SHOW_MODAL_LOGIN });
                } else {
                    dispatch({ type: Types2.VENUE_REPORT_FAILURE });
                    Toast.showWithGravity(response.data.errors.Error, Toast.LONG, Toast.TOP)
                }
            } else {
                dispatch({ type: Types2.VENUE_REPORT_FAILURE });
                Toast.showWithGravity(response.data.errors.Error, Toast.LONG, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types2.VENUE_REPORT_FAILURE });
        });
    };
}

export const actSetTokenIns = (venueId, token) => {
    return dispatch => {
        dispatch({ type: Types2.VENUE_SET_INSTAGRAM_REQUEST });
        let body = {
            Token: token.access_token
        };
        return HTTP.callApiWithHeader('venues/' + venueId + '/set-instagram', 'post', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types2.VENUE_SET_INSTAGRAM_SUCCESS, data: response.data.data, token: token });
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types2.VENUE_SET_INSTAGRAM_FAILURE });
                Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types2.VENUE_SET_INSTAGRAM_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types2.VENUE_SET_INSTAGRAM_FAILURE });
        });
    };
}

export const actImagesIns = (venueId) => {
    return dispatch => {
        dispatch({ type: Types2.VENUE_IMAGEINS_REQUEST });
        return HTTP.callApiWithHeader("venues/" + venueId + "/get-images-instagram", 'GET', null).then(response => {
            // console.log('resInsta', response)
            if (response && response.data.status == 200) {
                dispatch({ type: Types2.VENUE_IMAGEINS_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types2.VENUE_IMAGEINS_FAILURE });
                // Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types2.VENUE_IMAGEINS_FAILURE });
        });
    };
}

export const actRemoveTokenIns = (Slug) => {
    var body = {
        slug: Slug
    }
    return dispatch => {
        dispatch({ type: Types2.VENUE_REMOVE_INSTAGRAM_REQUEST });
        return HTTP.callApiWithHeader('venues-remove-instagram', 'post', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types2.VENUE_REMOVE_INSTAGRAM_SUCCESS });
            } else if (response && response.data.status == -1) {
                dispatch({ type: Types2.VENUE_REMOVE_INSTAGRAM_FAILURE });
                Toast.showWithGravity(response.data.errors[[Object.keys(response.data.errors)[0]]], Toast.SHORT, Toast.TOP)
            } else {
                dispatch({ type: Types2.VENUE_REMOVE_INSTAGRAM_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types2.VENUE_SET_INSTAGRAM_FAILURE });
        });
    };
}
export const actLoadDataEventByVenue = (id, page) => {
    return dispatch => {
        dispatch({ type: Types.LOAD_DATA_EVENT_BY_VENUE_REQUEST, page });
        return HTTP.callApiWithHeader('venues/' + id + '/events?page=' + page, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.LOAD_DATA_EVENT_BY_VENUE_SUCCESS, data: response.data.data, total: response.data.total });
            } else {
                dispatch({ type: Types.LOAD_DATA_EVENT_BY_VENUE_FAILURE });
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.LOAD_DATA_EVENT_BY_VENUE_FAILURE });
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
        });
    };
}
export const actDeleteVenue = (id, body) => {
    return dispatch => {
        dispatch({ type: Types.VENUE_LIST_REQUEST })
        return HTTP.callApiWithHeader(`venues/${id}/delete`, 'POST', body).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.DELETE_VENUE, Id: id })
                Toast.showWithGravity(JSON.stringify(response.data ? response.data.message.Message : response), Toast.SHORT, Toast.TOP)
                return new Promise.resolve({ status: 200 })
            } else {
                dispatch({ type: Types.VENUE_LIST_FAILURE })

                Toast.showWithGravity(JSON.stringify(response.data ? response.data.errors.Error : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {

            dispatch({ type: Types.VENUE_LIST_FAILURE })
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
        });
    };
}

export const actReportVenue = (id, body) => {
    return dispatch => {
        dispatch({ type: Types.VENUE_LIST_REQUEST })
        return HTTP.callApiWithHeader(`venues/${id}/report`, 'POST', body).then(response => {
            // console.log('report', response)
            if (response && response.data.status == 200) {
                // console.log('ok')
                dispatch({ type: Types.VENUE_LIST_FAILURE })
                Toast.showWithGravity(JSON.stringify(response.data ? response.data.messages.message : response), Toast.SHORT, Toast.TOP)
                return new Promise.resolve({ status: 200 })
            } else {
                dispatch({ type: Types.VENUE_LIST_FAILURE })
                Toast.showWithGravity(JSON.stringify(response.data ? response.data.errors.Error : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {

            dispatch({ type: Types.VENUE_LIST_FAILURE })
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
        });
    };
}


