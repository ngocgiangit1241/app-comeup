import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';


// export const actVenueLists = (id, type) => {
//     var url = '';
//     if (type === 'city') {
//         url = `venues?page=1&per_page=12&city_id=${id}`;
//     } else {
//         url = `venues?page=1&per_page=12&iso_code=${id}`;
//     }
//     return dispatch => {
//         dispatch({ type: Types.VENUE_LIST_REQUEST });
//         return HTTP.callApiWithHeader(url, 'GET', null).then(response => {
//             if (response && response.data.status == 200) {
//                 dispatch({ type: Types.VENUE_LIST_SUCCESS, data: response.data.data, page: 1 });
//             } else {
//                 dispatch({ type: Types.VENUE_LIST_FAILURE });
//                 // Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
//             }
//         }).catch(function (error) {
//             dispatch({ type: Types.VENUE_LIST_FAILURE });
//         });
//     };
// }

export const actCreateVenue = (body, id) => {
    return dispatch => {
        return HTTP.callApiWithHeader(`teams/${id}/venues/create`, 'POST', body).then(response => {
            console.log('response', response)
        }).catch(function (error) {
            dispatch({ type: Types.VENUE_LIST_FAILURE });
        });
    };
}

export const actListVenueMap = (tags, area, time_open, page, North = '', South = '', East = '', West = '',) => {
    if (!time_open) {
        time_open = ''
    }
    return dispatch => {
        dispatch({ type: Types.VENUE_LIST_REQUEST });
        return HTTP.callApiWithHeader(`search/venue/in-map?tags=${tags}&area=${area}&page=${page}&open_now=${time_open}&North=${North}&South=${South}&East=${East}&West=${West}`, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.VENUEMAP_LIST_SUCCESS, data: response.data.data, page, last_page: response.data.last_page });
            } else {
                dispatch({ type: Types.VENUEMAP_LIST_FAILURE });
                // Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            }
        }).catch(function (error) {
            dispatch({ type: Types.VENUEMAP_LIST_FAILURE });
        });
    };
}

export const actLikeVenues = (Id) => {
    return dispatch => {
        return HTTP.callApiWithHeader(`likes/venue`, 'POST', { VenueId: Id }).then(response => {
            if (response && response.data.status == 200) {
                return new Promise.resolve({ status: 200 });
            } else {
                Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
                return new Promise.resolve({ status: -1 });
            }
        }).catch(function (error) {
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
        });
    };
}
export const actUploadPhoto = (Id, photo) => {
    return dispatch => {
        return HTTP.callApiWithHeader(`venues/${Id}/ajax/image`, 'POST', { FileImg: photo }).then(response => {
            console.log('ressss', response);
            // if (response && response.data.status == 200) {
            //     return new Promise.resolve({ status: 200 });
            // } else {
            //     Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
            //     return new Promise.resolve({ status: -1 });
            // }
        }).catch(function (error) {
            Toast.showWithGravity(JSON.stringify(response.data ? response.data : response), Toast.SHORT, Toast.TOP)
        });
    };
}
export const actGetVenueDetail = (Id) => {
    return dispatch => {
        dispatch({ type: Types.VENUE_DETAIL_REQUEST });
        return HTTP.callApiWithHeader(`venues/${Id}`, 'GET', null).then(response => {
            if (response && response.data.status == 200) {
                dispatch({ type: Types.VENUE_DETAIL_SUCCESS, data: response.data.data });
            } else {
                dispatch({ type: Types.VENUE_DETAIL_FAILURE });
            }
        }).catch(function (error) {
            dispatch({ type: Types.VENUE_DETAIL_FAILURE });
        });
    };
}








