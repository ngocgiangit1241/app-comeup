import React from 'react'
import { FlatList, ActivityIndicator, Dimensions, Easing, Image, Platform, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Popover from 'react-native-popover-view';
import * as datacountry from '../utils/datacountry.json';
import * as Colors from '../constants/Colors';
import Touchable from '../screens_view/Touchable';
import Line from '../screens_view/Line';

const { width, height } = Dimensions.get('window')

function CountryCode(props) {

    const checkLanguage = (item) => {
        var { language } = props;
        // var language = 'ko'
        if (language === 'vi') {
            return ''
        } else if (language === 'ko') {
            if (item.common !== item.kor) {
                return `(${item.kor})`
            }
            return
        } else {
            return ''
        }
    }


    const renderItem = ({ item, index }) => {
        if (item !== 'default')
            return (
                <Touchable key={index} onPress={() => props.onPress('+' + datacountry[item]?.callingCode[datacountry[item]?.callingCode.length - 1], datacountry[item].flag)}
                    style={[styles.boxCountry, { flex: 1, justifyContent: 'space-between' }]}
                >
                    <View style={[styles.boxCountry, { width: '85%' }]}>
                        <Image source={{ uri: datacountry[item].flag }} style={[styles.ic_country, {}]} />
                        <Text style={[styles.txtCountry, { flexShrink: 1 }]}>{datacountry[item]?.name?.common} {checkLanguage(datacountry[item]?.name)}</Text>
                        {/* <Text style={[styles.txtCountry, { flexGrow: 1 }]}>{this.checkLanguage(datacountry[item]?.name)}</Text> */}
                    </View>
                    <Text style={[styles.txtCountry, { fontSize: 17, color: '#999', marginRight: 6, marginTop: -5 }]}>{'+' + datacountry[item]?.callingCode[datacountry[item]?.callingCode.length - 1]}</Text>
                </Touchable>
            )
        else
            return null
    }

    return (
        <>
            <Popover
                isVisible={props.isVisible}
                animationConfig={{
                    duration: 200,
                    easing: Easing.inOut(Easing.quad),
                }}
                fromView={props.touchable}
                placement='bottom'
                arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                onRequestClose={() => props.closePopover()}>

                <ScrollView style={{ flex: 1 }}>
                    <View style={styles.boxPopover}>
                        <Touchable onPress={() => props.onPress('+' + datacountry['KR']?.callingCode[datacountry['KR']?.callingCode.length - 1], datacountry['KR'].flag)} style={styles.boxCountry}>
                            <Image source={{ uri: datacountry['KR'].flag }} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>{datacountry['KR']?.name?.common} </Text>
                            <Text style={styles.txtCountry}>{checkLanguage(datacountry['KR']?.name)}</Text>
                            <Text style={[styles.txtCountry, { position: 'absolute', right: 0, fontSize: 17, color: '#999', top: 1, marginRight: 6 }]}>{'+' + datacountry['KR']?.callingCode[datacountry['KR']?.callingCode.length - 1]}</Text>
                        </Touchable>
                        <Touchable onPress={() => props.onPress('+' + datacountry['VN']?.callingCode[datacountry['VN']?.callingCode.length - 1], datacountry['VN'].flag)} style={styles.boxCountry}>
                            <Image source={{ uri: datacountry['VN'].flag }} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>{datacountry['VN']?.name?.common} </Text>
                            <Text style={styles.txtCountry}>{checkLanguage(datacountry['VN']?.name)}</Text>
                            <Text style={[styles.txtCountry, { position: 'absolute', right: 0, fontSize: 17, color: '#999', top: 1, marginRight: 6 }]}>{'+' + datacountry['VN']?.callingCode[datacountry['VN']?.callingCode.length - 1]}</Text>
                        </Touchable>
                        <Line style={{ borderWidth: 1, marginVertical: 10 }} />
                        <View>

                            <FlatList
                                data={Object.keys(datacountry)}
                                renderItem={renderItem}
                                // keyExtractor={() => Math.random().toString()}
                                initialNumToRender={4}
                            />

                            {/* {
                                                Object.keys(datacountry).map((item, index) => {
                                                    if (item !== 'default')
                                                        return (
                                                            <Touchable key={index + 'data_country'} onPress={() => { this.closePopover(); this.setState({ Zipcode: '+' + datacountry[item]?.callingCode[datacountry[item]?.callingCode.length - 1] }) }} style={styles.boxCountry}>
                                                                <Image source={{ uri: datacountry[item].flag }} style={styles.ic_country} />
                                                                <Text style={styles.txtCountry}>{datacountry[item]?.name?.common} </Text>
                                                                <Text style={styles.txtCountry}>{this.checkLanguage(datacountry[item]?.name)}</Text>
                                                                <Text style={[styles.txtCountry, { position: 'absolute', right: 0, fontSize: 17, color: '#999', top: 1 }]}>{'+' + datacountry[item]?.callingCode[datacountry[item]?.callingCode.length - 1]}</Text>
                                                            </Touchable>
                                                        )
                                                })
                                            } */}
                        </View>
                    </View>
                </ScrollView>
            </Popover>
        </>
    )
}
const styles = StyleSheet.create({
    textDes: {
        color: '#872aa5',
        fontSize: 14,
        fontStyle: 'italic'

    },
    ipContent: {
        fontSize: 15,
        color: Colors.TEXT_P,
        borderBottomWidth: 1,
        borderBottomColor: '#bcbcbc',
        flex: 2.5,
        paddingBottom: 5,
        paddingTop: 5,
        // height: 32,
        fontWeight: 'bold'
    },
    boxPicker: {
        borderBottomWidth: 1,
        borderBottomColor: '#bcbcbc',
        paddingBottom: 5,
        paddingTop: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: 72,
        marginRight: 10
    },
    txtPicker: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    boxOption: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 10
    },
    ic_dropdown: {
        width: 13,
        height: 11,
        marginRight: 5
    },
    boxInput: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 16,
    },
    boxPopover: {
        width: width - 12,
        padding: 12,
    },
    boxCountry: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 6
    },
    ic_country: {
        width: 30,
        height: 20,
        marginRight: 12
    },
    txtCountry: {
        color: '#333333',
        fontSize: 15
    },
})
export default React.memo(CountryCode)