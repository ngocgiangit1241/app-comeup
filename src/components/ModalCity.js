import React, { Component } from 'react';
import { Animated, Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../screens_view/ScrView';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalCity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: '',
            height: new Animated.Value(280)
        }
    }
    componentDidUpdate(props, state) {
        if (this.state.current === '') {
            Animated.timing(                    // Animate over time
                this.state.height,             // The animated value to drive, this would be a new Animated.Value(0) object.
                {
                    toValue: 280,                   // Animate the value
                    duration: 300,                 // Make it take a while
                }
            ).start();
        } else {
            Animated.timing(                    // Animate over time
                this.state.height,             // The animated value to drive, this would be a new Animated.Value(0) object.
                {
                    toValue: 450,                   // Animate the value
                    duration: 300,                 // Make it take a while
                }
            ).start();
        }
    }

    render() {
        var { countries } = this.props;
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <Animated.View style={[styles.modalContent, { height: this.state.height }]}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <Text style={[styles.title, { borderBottomWidth: 0 }]}>{convertLanguage(language, 'select_a_city')}</Text>

                    <SrcView style={styles.container}>
                        <View style={styles.content}>
                            {
                                countries.map((item, index) => {

                                    return <React.Fragment key={index}>
                                        <Text style={[styles.txtCountry
                                            , { borderBottomWidth: this.state.current === index ? 0 : 0, color: this.state.current === index ? '#00A9F4' : '#333333' }
                                        ]} onPress={() => this.setState({ current: this.state.current === index ? '' : index })}>{item['Name_' + language]}</Text>
                                        {
                                            this.state.current === index &&
                                            <View style={[styles.boxChildren
                                                , { borderBottomWidth: item.Cites.length > 0 ? 0 : 0 }
                                            ]}>
                                                {
                                                    item.Cites.map((city, index2) => {
                                                        return <Text style={styles.txtCity} key={index2} onPress={() => this.props.selectCity(city, item)}>{city['Name_' + language]}</Text>
                                                    })
                                                }

                                            </View>
                                        }


                                    </React.Fragment>
                                })
                            }
                        </View>
                    </SrcView>
                </Animated.View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        // height: 600,
        // marginLeft: 50,
        // marginRight: 50,
        borderRadius: 5
    },
    container: {
        flex: 1,
        marginBottom: 10,
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center'
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    title: {
        fontSize: 17,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        paddingHorizontal: 20,
        textAlign: 'center'
    },
    txtCountry: {
        fontSize: 17,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        alignSelf: 'flex-start',
        paddingHorizontal: 24,
        textAlign: 'center'
    },
    boxChildren: {
        paddingTop: 15,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        width: 250,
        marginLeft: 56
        // alignItems: 'center',
    },
    txtCity: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 15,
    }
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default React.memo(connect(mapStateToProps, null)(ModalCity));
