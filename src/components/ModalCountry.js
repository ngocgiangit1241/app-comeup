import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../screens_view/ScrView';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalCountry extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: ''
        }
    }

    checkLangCountries = (item) => {
        var { language } = this.props.language;
        switch (language) {
            case 'ko':
                return item.Name_ko;
            case 'vi':
                return item.Name_vi;;
            case 'en':
                return item.Name_en;;
            default:
                return item.Name;
        }
    }

    render() {
        var { language } = this.props.language;
        var { countries } = this.props;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <SrcView style={styles.container}>
                        <View style={styles.content}>
                            <Text style={[styles.txtSelectCountry, { borderBottomWidth: 0 }]}>{convertLanguage(language, 'select_a_country')}</Text>
                            {
                                countries.map((item, index) => {
                                    return <TouchableOpacity key={index} onPress={() => this.props.selectCountry(item)}>
                                        <Text style={[styles.txtCountry]} >{this.checkLangCountries(item)}</Text>
                                    </TouchableOpacity>
                                })
                            }
                        </View>
                    </SrcView>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        height: 300,
        marginLeft: 50,
        marginRight: 50,
        borderRadius: 5
    },
    container: {

    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center'
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    txtSelectCountry: {
        fontSize: 15,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        textAlign: 'center'
    },
    txtCountry: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15,
    },
    boxChildren: {
        paddingTop: 15,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        width: 250,
        alignItems: 'center'
    },
    txtCity: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15,
    }
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalCountry);