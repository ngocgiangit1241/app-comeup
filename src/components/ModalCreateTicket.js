import React, { useState, useRef, useEffect } from 'react';
import {
  PixelRatio,
  ScrollView,
  KeyboardAvoidingView,
  Text,
  TouchableOpacity,
  View,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
  Linking,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import Modal from 'react-native-modal';
import SrcView from '../screens_view/ScrView';
import { formatNumber, convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { useDispatch, useSelector } from 'react-redux';
import { TextField } from '../components/react-native-material-textfield';
import Popover from 'react-native-popover-view';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
// import ModalContactInfoComeup from '../components/event/ModalContactInfoComeup';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import moment from "moment";

const scale = width / 360;
const scaleFontSize = PixelRatio.getFontScale();

export default function ModalCreateTicket(props) {
  const dispatch = useDispatch()
  const refType = useRef(null);
  const ref2 = useRef(null);
  const refScroll = useRef();
  const refDes = useRef(null);
  const { language } = useSelector(state => state.language);
  // const language = 'ko';
  const [toogle, setTogle] = useState({
    showMore: false,
    checkBox1: false,
    checkBox2: false,
    Type: false,
  });
  useEffect(() => {
    if (toogle.checkBox2) {
      setTimeout(() => refScroll.current.scrollToEnd({ animated: true }), 100);
    } else {
      // setTimeout(() => refScroll.current.scrollToEnd({ animated: true }), 100);
    }
  }, [toogle.checkBox2]);
  const listTypes = [
    {
      name: convertLanguage(language, 'e_ticket2'),
      value: 'e-ticket',
    },
    {
      name: convertLanguage(language, 'delivery_printed_ticket'),
      value: 'd-p-ticket',
    },
  ];
  const [validate, setValidate] = useState({
    checkRequire: false,
    Quantity: '',
    price: '',
  });

  const [modalContact, setModalContact] = useState(false);

  useEffect(() => {
    if (props?.edit?.length > 0) {
      if (props.edit[1].Question2 && props.edit[1].Question2 !== '') {
        setTogle({ ...toogle, checkBox2: !toogle.checkBox2 });
      }
      setData({
        ...data,
        Type:
          props.edit[1].Type === 'd-p-ticket'
            ? {
              name: convertLanguage(language, 'delivery_printed_ticket'),
              value: 'd-p-ticket',
            }
            : { name: convertLanguage(language, 'e-ticket'), value: 'e-ticket' },
        NameTicket: props.edit[1].NameTicket,
        Price: format2(props.edit[1].Price),
        PriceFormat: formatNumber(format2(props.edit[1].Price)),
        Quantity: format2(props.edit[1].Quantity),
        Option: props.edit[1].Option,
        Question2: props.edit[1].Question2,
        Status: props.edit[1].Status === 1 ? false : true,
        LimitQuantity: props.edit[1].LimitQuantity,
        Description: props.edit[1].Description,
        TimeStartTicket: props.edit[1].TimeStartTicket,
        TimeEndTicket: props.edit[1].TimeEndTicket,
      });
      setShowTime({
        ...showTime,
        TimeStartTicket: moment(props.edit[1].TimeStartTicket).format(
          'YYYY-MM-DD HH:mm',
        ),
        TimeEndTicket: moment(props.edit[1].TimeEndTicket).format(
          'YYYY-MM-DD HH:mm',
        ),
      });
    }
    else {

      if (moment() <= moment(props.timeFinish)) {
        setData({
          ...data,
          TimeStartTicket: moment().format('YYYY-MM-DD HH:mm'),
          TimeEndTicket: moment(props.timeFinish).format('YYYY-MM-DD HH:mm')
        });

        setShowTime({
          ...showTime,
          TimeStartTicket: moment().format(
            'YYYY-MM-DD HH:mm',
          ),
          TimeEndTicket: moment(props.timeFinish).format(
            'YYYY-MM-DD HH:mm',
          ),
        });

      } else {
        setData({
          ...data,
          TimeStartTicket: moment(props.timeStart).format('YYYY-MM-DD HH:mm'),
          TimeEndTicket: moment(props.timeFinish).format('YYYY-MM-DD HH:mm')
        });

        setShowTime({
          ...showTime,
          TimeStartTicket: moment(props.timeStart).format(
            'YYYY-MM-DD HH:mm',
          ),
          TimeEndTicket: moment(props.timeFinish).format(
            'YYYY-MM-DD HH:mm',
          ),
        });
      }
    }
  }, [props.edit]);
  const [data, setData] = useState({
    Type: { name: convertLanguage(language, 'e-ticket'), value: 'e-ticket' },
    TimeStartTicket: '',
    TimeEndTicket: '',
    NameTicket: '',
    Price: '',
    PriceFormat: '',
    Quantity: '',
    Status: false,
    LimitQuantity: '10',
    Description: '',
    Question2: '',
    Option: [],
    TicketId: '',
  });

  const [showTime, setShowTime] = useState({
    TimeStartTicket: '',
    TimeEndTicket: '',
  });
  const [checkFocus, setFocus] = useState(false);
  useEffect(() => {
    if (
      data.NameTicket !== '' &&
      data.Price !== '' &&
      data.Type.value !== '' &&
      data.Quantity !== '' &&
      data.TimeStartTicket !== '' &&
      data.TimeEndTicket !== ''
    ) {
      setValidate({ ...validate, checkRequire: true });
    } else {
      setValidate({ ...validate, checkRequire: false });
    }
  }, [data]);

  const setPopTimeout = (value) => {
    setTimeout(() => {
      setModalContact(value)
    }, 200);
  }


  useEffect(() => {
    data.Type.value === 'd-p-ticket'
      ? setPopTimeout(true)
      : setPopTimeout(false);
  }, [data.Type]);

  const [isDatePickerVisible, setDatePickerVisibility] = useState({
    start: false,
    end: false,
  });
  const format2 = txt => {
    try {
      var numb = txt.match(/\d/g);
      numb = numb.join('');
      return numb;
    } catch (error) {
      return txt;
    }
  };
  const onSave = async () => {
    let Quantity,
      price = '';
    if (data.Quantity == 0) {
      // setValidate({ ...validate, Quantity: 'Quantity khong duoc bang 0' })
      Quantity = 'Quantity khong duoc bang 0';
    } else {
      // setValidate({ ...validate, Quantity: '' })
      Quantity = '';
    }
    //Price
    if (props.Unit === 'VND') {
      if (data.Price < 100000 && data.Price > 0) {
        // price = convertLanguage(language, 'validate_ticket_price_min', {
        //   zero: '0',
        //   min: '10,000',
        // });
        price = convertLanguage(language, 'validate_ticket_price_ticket', {
          zero: '0',
          min: '100,000',
          max: '10,000,000,000',
        })
      } else if (data.Price > 10000000000) {
        // price = convertLanguage(language, 'validate_ticket_price_max', {
        //   max: '10,000,000,000',
        // });
        price = convertLanguage(language, 'validate_ticket_price_ticket', {
          zero: '0',
          min: '100,000',
          max: '10,000,000,000',
        })
      } else {
        price = '';
      }
    } else {
      if (data.Price < 5000 && data.Price > 0) {
        // price = convertLanguage(language, 'validate_ticket_price_min', {
        //   zero: '0',
        //   min: '1,000',
        // });
        price = convertLanguage(language, 'validate_ticket_price_ticket', {
          zero: '0',
          min: '5,000',
          max: '1,000,000,000',
        })
      } else if (data.Price > 1000000000) {
        // price = convertLanguage(language, 'validate_ticket_price_max', {
        //   max: '1,000,000,000',
        // });
        price = convertLanguage(language, 'validate_ticket_price_ticket', {
          zero: '0',
          min: '5,000',
          max: '1,000,000,000',
        })
      } else {
        price = '';
      }
    }
    //

    setValidate({ ...validate, Quantity, price });
    if (price === '' && Quantity === '') {
      // console.log('allAPI', data);
      let Option = ['', ''];
      if (toogle.checkBox1 === true && toogle.checkBox2 === true) {
        Option = [1, 2];
      } else if (toogle.checkBox1 === false && toogle.checkBox2 === true) {
        Option = ['', 2];
      } else if (toogle.checkBox1 === true && toogle.checkBox2 === false) {
        Option = [1, ''];
      } else {
        Option = ['', ''];
      }

      console.log('123', props);
      if (props.edit.length > 0) {
        //edit

        await props.changeEdit(props.edit[0], {
          ...props.ticket,
          ...data,
          Unit: props.Unit,
          Option,
          Status: data.Status === true ? 0 : 1,
          Type: data.Type.value,
          TicketId: props?.ticket?.Id || null
        });
        // await dispatch()
        props.closeModal();
      } else {
        props.save({
          ...data,
          Unit: props.Unit,
          Option,
          Status: data.Status === true ? 0 : 1,
          Type: data.Type.value,
        }); //add
        props.closeModal();
      }
    }
  };
  const showDatePickerStart = () => {
    setDatePickerVisibility({ ...isDatePickerVisible, start: true });
  };

  const hideDatePickerStart = () => {
    setDatePickerVisibility({ ...isDatePickerVisible, start: false });
  };

  const handleConfirmStart = date => {
    if (data.TimeEndTicket === '') {
      setData({
        ...data,
        TimeStartTicket: moment(props.timeEvent.startTime).format('YYYY-MM-DD HH:mm'),
      });
      setShowTime({
        ...showTime,
        TimeStartTicket: moment(props.timeEvent.startTime).format('YYYY-MM-DD HH:mm'),
      });
      hideDatePickerStart();
    } else {
      if (moment(date).format('YYYY-MM-DD HH:mm') > moment(props.timeEvent.endTime).format('YYYY-MM-DD HH:mm')) {
        setData({
          ...data,
          TimeStartTicket: moment(props.timeEvent.endTime).format('YYYY-MM-DD HH:mm'),
          TimeEndTicket: moment(props.timeEvent.endTime).format('YYYY-MM-DD HH:mm'),
        });
        setShowTime({
          ...showTime,
          TimeStartTicket: moment(props.timeEvent.endTime).format('YYYY-MM-DD HH:mm'),
          TimeEndTicket: moment(props.timeEvent.endTime).format('YYYY-MM-DD HH:mm'),
        });
        hideDatePickerStart();
        return
      }
      if (
        moment(date).format('YYYY-MM-DD HH:mm') >
        moment(data.TimeEndTicket).format('YYYY-MM-DD HH:mm')
      ) {
        setData({
          ...data,
          TimeStartTicket: moment(date).format('YYYY-MM-DD HH:mm'),
          TimeEndTicket: '',
        });
        setShowTime({
          ...showTime,
          TimeStartTicket: moment(date).format('YYYY-MM-DD HH:mm'),
          TimeEndTicket: '',
        });
        hideDatePickerStart();
      } else {
        setData({
          ...data,
          TimeStartTicket: moment(date).format('YYYY-MM-DD HH:mm'),
        });
        setShowTime({
          ...showTime,
          TimeStartTicket: moment(date).format('YYYY-MM-DD HH:mm'),
        });
        hideDatePickerStart();
      }
    }
  };
  const showDatePickerEnd = () => {
    setDatePickerVisibility({ ...isDatePickerVisible, end: true });
  };

  const hideDatePickerEnd = () => {
    setDatePickerVisibility({ ...isDatePickerVisible, end: false });
  };

  const handleConfirmEnd = date => {
    if (moment(date).format('YYYY-MM-DD HH:mm') < moment(data.TimeStartTicket).format('YYYY-MM-DD HH:mm')) {
      setData({
        ...data,
        TimeEndTicket: moment(data.TimeStartTicket).format('YYYY-MM-DD HH:mm'),
      });
      setShowTime({
        ...showTime,
        TimeEndTicket: moment(data.TimeStartTicket).format('YYYY-MM-DD HH:mm'),
      });
      hideDatePickerEnd();
      return
    }

    if (moment(date).format('YYYY-MM-DD HH:mm') > moment(props.timeEvent.endTime).format('YYYY-MM-DD HH:mm')) {
      setData({
        ...data,
        TimeEndTicket: moment(props.timeEvent.endTime).format('YYYY-MM-DD HH:mm'),
      });
      setShowTime({
        ...showTime,
        TimeEndTicket: moment(props.timeEvent.endTime).format('YYYY-MM-DD HH:mm'),
      });
      hideDatePickerEnd();
      return
    }

    setData({ ...data, TimeEndTicket: moment(date).format('YYYY-MM-DD HH:mm') });
    setShowTime({
      ...showTime,
      TimeEndTicket: moment(date).format('YYYY-MM-DD HH:mm'),
    });
    hideDatePickerEnd();
  };

  useEffect(() => {
    if (data.Option.length === 2) {
      if (data.Option[0] === 1) {
        if (data.Option[1] === 2) {
          setTogle({ ...toogle, checkBox1: true, checkBox2: true });
        } else {
          setTogle({ ...toogle, checkBox1: true, checkBox2: false });
        }
      } else {
        if (data.Option[1] === 2) {
          setTogle({ ...toogle, checkBox1: false, checkBox2: true });
        } else {
          setTogle({ ...toogle, checkBox1: false, checkBox2: false });
        }
      }
    }
  }, [data.Option]);

  const checkVisible = () => {
    if (
      !props?.edit[1]?.total_ticketDetail ||
      props?.edit[1]?.total_ticketDetail === undefined
    ) {
      return true;
    }
    if (props?.edit[1]?.total_ticketDetail !== 0) {
      return false;
    }
    return true;
  };
  const checkvisibleTimeEnd = () => {
    // if (
    //   !props?.edit[1]?.total_ticketDetail ||
    //   props?.edit[1]?.total_ticketDetail === undefined
    // ) {
    if (data.TimeStartTicket !== '') {
      return false;
    }
    return true;
    // }
    // if (
    //   props?.edit[1]?.total_ticketDetail &&
    //   props?.edit[1]?.total_ticketDetail !== undefined
    // ) {
    //   if (props?.edit[1]?.total_ticketDetail !== 0) {
    //     return true;
    //   }
    //   return false;
    // }
  };
  const checkLegth = string => {
    if (string.length < 15) {
      return string;
    }
    return string.slice(0, 13) + '...';
  };


  return (
    <>
      <Modal
        isVisible={true}
        backdropOpacity={0.3}
        animationIn="zoomInDown"
        animationOut="zoomOutUp"
        animationInTiming={1}
        animationOutTiming={1}
        backdropTransitionInTiming={1}
        backdropTransitionOutTiming={1}
        deviceHeight={
          Platform.OS === 'android'
            ? ExtraDimensions.getRealWindowHeight()
            : height
        }>
        <View style={styles.modalContent}>
          <View style={styles.rowClose}>
            <TouchableOpacity
              style={styles.btnClose}
              onPress={() => {
                props.closeModal();
              }}>
              <Image
                style={styles.iconClose}
                source={require('../assets/close.png')}
              />
            </TouchableOpacity>
          </View>
          <ScrollView ref={refScroll}>
            <View style={styles.content}>
              {/* <Text style={styles.contentTitle}>
                {convertLanguage(language, 'we_are_happy_with_free')}
              </Text> */}
              {/* <TouchableOpacity style={{ marginTop: 6 * scale }}>
                <Text
                  style={{ color: '#00A9F4', textDecorationLine: 'underline' }}>
                  {convertLanguage(language, 'tickets_selling_terms')}
                </Text>
              </TouchableOpacity> */}
              {/* <Text
                style={{
                  fontStyle: 'normal',
                  fontWeight: 'normal',
                  fontSize: 12 * scale,
                  lineHeight: 15 * scale,
                  color: '#828282',
                  marginTop: 12 * scale,
                  textAlign: 'center',
                }}>
                {convertLanguage(
                  language,
                  'by_creating_tickets_you_agree_to_the_terms_of_selling_tickets',
                )}
              </Text> */}
              {/* <View
                style={{
                  borderBottomColor: '#00A9F4',
                  borderBottomWidth: 1,
                  width: '100%',
                  marginTop: 20 * scale,
                }}
              /> */}
              {props?.edit[1]?.total_ticketDetail &&
                props?.edit[1]?.total_ticketDetail !== 0 ? (
                <View
                  style={{
                    justifyContent: 'center',
                    paddingTop: 20,
                    textAlign: 'center',
                  }}>
                  <Text style={{ color: '#EB5757' }}>
                    {/* {props?.edit[1]?.total_ticketDetail + ' '} */}
                    {convertLanguage(language, 'warnning_ticket')}
                  </Text>
                </View>
              ) : null}
            </View>

            <KeyboardAwareScrollView>
              <View style={{}}>
                <View style={{ flex: 1, paddingHorizontal: 20 * scale }}>
                  <TextField
                    editable={checkVisible()}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    value={data.NameTicket}
                    onChangeText={NameTicket => setData({ ...data, NameTicket })}
                    // error={error.manager && error.manager[0]}
                    returnKeyType="next"
                    label={convertLanguage(language, 'ticket_name') + ' *'}
                    baseColor={'#828282'}
                    tintColor={'#828282'}
                    errorColor={'#EB5757'}
                    inputContainerStyle={[
                      styles.inputContainerStyle,
                      { borderColor: '#BDBDBD' },
                    ]}
                    containerStyle={styles.containerStyle}
                    labelTextStyle={{
                      paddingBottom: 15 * scale,
                      paddingLeft: 12 * scale,
                    }}
                    lineWidth={2}
                    selectionColor={'#3a3a3a'}
                    style={[styles.input, {}]}
                    errorImage={require('../assets/error.png')}

                  />
                </View>
                <View style={{ flex: 1, paddingHorizontal: 20 * scale }}>
                  <TextField
                    editable={checkVisible()}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    value={data.PriceFormat}
                    onChangeText={Price =>
                      !isNaN(Price)
                        ? setData({ ...data, Price, PriceFormat: Price })
                        : setData({
                          ...data,
                          Price: data.Price,
                          PriceFormat: data.PriceFormat,
                        })
                    }
                    error={validate.price !== '' && validate.price}
                    returnKeyType="next"
                    label={`${convertLanguage(language, 'ticket_price')}(${props.Unit
                      }) ` + convertLanguage(language, 'include_vat') + ' *'}
                    baseColor={'#828282'}
                    tintColor={'#828282'}
                    errorColor={'#EB5757'}
                    inputContainerStyle={[
                      styles.inputContainerStyle,
                      {
                        borderColor:
                          validate.price !== '' ? '#EB5757' : '#BDBDBD',
                      },
                    ]}
                    containerStyle={styles.containerStyle}

                    labelTextStyle={{
                      paddingBottom: 15 * scale,
                      paddingLeft: 12 * scale,
                    }}
                    lineWidth={2}
                    selectionColor={'#3a3a3a'}
                    style={styles.input}
                    errorImage={require('../assets/error.png')}
                    keyboardType="number-pad"
                    onFocus={() =>
                      setData({ ...data, PriceFormat: format2(data.PriceFormat) })
                    }
                    onBlur={() =>
                      data.Price !== '' &&
                      setData({
                        ...data,
                        PriceFormat: formatNumber(data.PriceFormat),
                      })
                    }
                  />
                </View>
                <View style={styles.container}>
                  <View
                    style={{
                      flex: 1 / 2,
                      marginLeft: 20 * scale,
                      // marginRight: 10 * scale,
                    }}>
                    <TouchableOpacity
                      onPress={() => setTogle({ ...toogle, Type: true })}
                      disabled={!checkVisible()}>
                      <TextField
                        ref={refType}
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        value={
                          // data.Type.name.length > 14
                          //   ? data.Type.name.slice(0, 14) + '...'
                          //   : data.Type.name
                          data.Type.name
                        }
                        editable={false}
                        returnKeyType="next"
                        label={convertLanguage(language, 'ticket_type') + ' *'}
                        baseColor={'#828282'}
                        tintColor={'#828282'}
                        errorColor={'#EB5757'}
                        // checkLeft={{ marginLeft: -4 }}
                        inputContainerStyle={[
                          styles.inputContainerStyle,
                          { borderColor: '#BDBDBD', position: 'relative' },
                        ]}
                        containerStyle={styles.containerStyle}

                        labelTextStyle={{
                          paddingBottom: 15 * scale,
                          paddingLeft: 12 * scale,
                        }}
                        lineWidth={2}
                        // labelFontSize={(12 * scale) / scaleFontSize}
                        selectionColor={'#3a3a3a'}
                        errorImage={require('../assets/error.png')}
                      />
                      <Image
                        source={require('../assets/down-st.png')}
                        style={{
                          width: 15 * scale,
                          height: 15 * scale,
                          position: 'absolute',
                          top: '32%',
                          right: 0,
                          marginRight: 8 * scale,
                          resizeMode: 'contain',
                          tintColor: '#4F4F4F',
                          transform: [
                            { rotate: toogle.Type ? '180deg' : '0deg' },
                          ],
                        }}
                      />
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      flex: 1 / 2,
                      marginRight: 20 * scale,
                      marginLeft: 10 * scale,
                    }}>
                    <TextField
                      // editable={!data.Status}
                      autoCorrect={false}
                      enablesReturnKeyAutomatically={true}
                      value={data.Quantity}
                      onChangeText={Quantity =>
                        !isNaN(Quantity) && Quantity !== '0'
                          ? setData({ ...data, Quantity })
                          : setData({ ...data, Quantity: data.Quantity })
                      }
                      error={validate.Quantity !== '' && validate.Quantity}
                      returnKeyType="next"
                      label={convertLanguage(language, 'quantity') + ' *'}
                      baseColor={'#828282'}
                      tintColor={'#828282'}
                      errorColor={'#EB5757'}
                      inputContainerStyle={[
                        styles.inputContainerStyle,
                        {
                          borderColor:
                            validate.Quantity !== '' ? '#EB5757' : '#BDBDBD',
                        },
                      ]}
                      containerStyle={styles.containerStyle}

                      labelTextStyle={{
                        paddingBottom: 15 * scale,
                        paddingLeft: 12 * scale,
                      }}
                      lineWidth={2}
                      selectionColor={'#3a3a3a'}
                      style={styles.input}
                      errorImage={require('../assets/error.png')}
                      keyboardType="number-pad"
                    />
                  </View>
                </View>
                <View>
                  {data.Type.value === 'd-p-ticket' && (
                    <>
                      {/* <ModalContactInfoComeup
                                        modalVisible={true}
                                        closeModal={() => setModalContact(false)}
                                    /> */}
                      <Popover
                        isVisible={modalContact}
                        animationConfig={{ duration: 0 }}
                        fromView={refType.current}
                        placement="center"
                        // fromRect={{ x: 32 * scale, y: 160 * scale, width: width - 64 * scale, height: 200 }}
                        arrowStyle={{
                          backgroundColor: 'transparent',
                          height: 0,
                          width: 0,
                        }}
                        backgroundStyle={{
                          backgroundColor: 'rgba(0, 0, 0, 0.1)',
                        }}
                        onRequestClose={() => setPopTimeout(false)}>
                        <View
                          style={[
                            styles.rowClose,
                            {
                              width: width - 64 * scale,
                              padding: 12 * scale,
                            },
                          ]}>
                          <TouchableOpacity
                            style={{}}
                            onPress={() => setPopTimeout(false)}>
                            <Image
                              style={{ width: 30 * scale, height: 30 * scale }}
                              source={require('../assets/close.png')}
                            />
                          </TouchableOpacity>
                        </View>
                        <View
                          style={{
                            justifyContent: 'center',
                            borderColor: 'rgba(0, 0, 0, 0.3)',
                            paddingTop: 10 * scale,
                            paddingBottom: 12 * scale,
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              paddingHorizontal: 24 * scale,
                              paddingBottom: 24 * scale,
                            }}>
                            <Image
                              source={require('../assets/contact_user.png')}
                              style={{
                                width: 64 * scale,
                                height: 64 * scale,
                                marginRight: 8 * scale,
                              }}
                            />
                            <View
                              style={{
                                flex: 1,
                              }}>
                              <Text style={styles.txtInfo}>
                                {convertLanguage(language, 'pop_info')}
                              </Text>
                              <Text style={styles.txtInfo}>
                                <Text style={{ color: '#8c8c8c' }}>{convertLanguage(language, 'email')}:</Text>
                                contact@starindex.com
                              </Text>
                              <Text style={styles.txtInfo}>
                                <Text style={{ color: '#8c8c8c' }}>{convertLanguage(language, 'tel')}:</Text>08
                                2779 2656
                              </Text>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  marginTop: 10 * scale,
                                }}>
                                <TouchableOpacity
                                  style={styles.btnPhone}
                                  onPress={() =>
                                    Linking.openURL('tel:0827792656')
                                  }>
                                  <Image
                                    source={require('../assets/contact_tel.png')}
                                    style={styles.contact_tel}
                                  />
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={styles.btnPhone}
                                  onPress={() =>
                                    Linking.openURL(
                                      'mailto:contact@starindex.com',
                                    )
                                  }>
                                  <Image
                                    source={require('../assets/contact_mail.png')}
                                    style={styles.contact_tel}
                                  />
                                </TouchableOpacity>
                              </View>
                            </View>
                          </View>
                        </View>
                      </Popover>
                      <Text style={styles.txtInfoCod}>
                        {convertLanguage(language, 'ticket_des1')}
                        <Text
                          onPress={() => setPopTimeout(true)}
                          style={{ color: '#00A9F4' }}>{convertLanguage(language, 'here')}</Text>
                        {convertLanguage(language, 'ticket_des2')}
                      </Text>
                    </>
                  )}
                </View>
                <View style={styles.container}>
                  <View
                    style={{
                      flex: 1 / 2,
                      marginLeft: 20 * scale,
                      // marginRight: 10 * scale,
                    }}>
                    <TouchableOpacity
                      // disabled={!checkVisible()}
                      onPress={() => showDatePickerStart()}
                      style={{ flex: 3 / 4 }}>
                      <TextField
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        value={showTime.TimeStartTicket}
                        // onChangeText={(companyName) => setData({ ...data, companyName })}
                        // error={error.manager && error.manager[0]}
                        editable={false}
                        returnKeyType="next"
                        label={convertLanguage(language, 'start_date')}
                        baseColor={'#828282'}
                        tintColor={'#828282'}
                        errorColor={'#EB5757'}
                        inputContainerStyle={[
                          styles.inputContainerStyle,
                          { borderColor: '#BDBDBD' },
                        ]}
                        containerStyle={[styles.containerStyle]}
                        checkLeft={{ marginLeft: -3 }}
                        labelTextStyle={{
                          paddingBottom: 15 * scale,
                          paddingLeft: 12 * scale,
                        }}
                        lineWidth={2}
                        selectionColor={'#3a3a3a'}
                        // fontSize={(12 * scale) / scaleFontSize}
                        // labelFontSize={(14 * scale) / scaleFontSize}
                        style={{ flex: 1 }}
                        errorImage={require('../assets/error.png')}
                      />
                      <Image
                        source={require('../assets/Cal.png')}
                        style={{
                          width: 12 * scale,
                          height: 12 * scale,
                          position: 'absolute',
                          top: '35%',
                          right: -3,
                          // marginTop: 30 * scale,
                          marginRight: 7 * scale,
                          flex: 1 / 4,
                          resizeMode: 'contain',
                        }}
                      />
                    </TouchableOpacity>
                    <DateTimePickerModal
                      isVisible={isDatePickerVisible.start}
                      mode="datetime"
                      cancelTextIOS={convertLanguage(language, 'cancel')}
                      confirmTextIOS={convertLanguage(language, 'confirm')}
                      locale={language}
                      format="HH:mm YYYY.MM.DD"
                      cancelTextIOS={convertLanguage(language, 'cancel')}
                      onConfirm={handleConfirmStart}
                      onCancel={hideDatePickerStart}
                      headerTextIOS={convertLanguage(language, 'pick_a_date')}
                      maximumDate={new Date(moment(props.timeEvent.endTime).format('YYYY-MM-DDTHH:mm'))}

                    />
                  </View>

                  <View
                    style={{
                      flex: 1 / 2,
                      marginRight: 20 * scale,
                      marginLeft: 10 * scale,
                    }}>
                    <TouchableOpacity
                      disabled={checkvisibleTimeEnd()}
                      onPress={() => showDatePickerEnd()}>
                      <TextField
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        value={showTime.TimeEndTicket}
                        // onChangeText={(companyName) => setData({ ...data, companyName })}
                        // error={error.manager && error.manager[0]}
                        editable={false}
                        returnKeyType="next"
                        label={convertLanguage(language, 'finish_date')}
                        baseColor={'#828282'}
                        tintColor={'#828282'}
                        errorColor={'#EB5757'}
                        inputContainerStyle={[
                          styles.inputContainerStyle,
                          {
                            borderColor:
                              data.TimeStartTicket !== ''
                                ? '#BDBDBD'
                                : '#E0E2E8',
                            backgroundColor: checkvisibleTimeEnd()
                              ? '#E0E2E8'
                              : 'white',
                          },
                        ]}
                        containerStyle={styles.containerStyle}
                        checkLeft={{ marginLeft: -3 }}
                        labelTextStyle={{
                          paddingBottom: 15 * scale,
                          paddingLeft: 12 * scale,
                        }}
                        lineWidth={2}
                        selectionColor={'#3a3a3a'}
                        style={styles.input}
                        // fontSize={(12 * scale) / scaleFontSize}
                        // labelFontSize={(14 * scale) / scaleFontSize}
                        errorImage={require('../assets/error.png')}
                      />
                      <Image
                        source={require('../assets/Cal.png')}
                        style={{
                          width: 12 * scale,
                          height: 12 * scale,
                          position: 'absolute',
                          top: '35%',
                          right: -3,
                          marginRight: 7 * scale,
                          resizeMode: 'contain',
                        }}
                      />
                    </TouchableOpacity>
                    <DateTimePickerModal
                      isVisible={isDatePickerVisible.end}
                      mode="datetime"
                      cancelTextIOS={convertLanguage(language, 'cancel')}
                      confirmTextIOS={convertLanguage(language, 'confirm')}
                      locale={language}
                      // format="HH:mm YYYY.MM.DD"
                      onConfirm={data1 =>
                        moment(data1).format('YYYY-MM-DD HH:mm') >
                          moment(data.TimeStartTicket)
                            .add(1, 'minutes')
                            .format('YYYY-MM-DD HH:mm')
                          ? handleConfirmEnd(
                            moment(data1).format('YYYY-MM-DD HH:mm'),
                          )
                          : handleConfirmEnd(
                            // moment(data.TimeStartTicket)
                            //   .add(5, 'minutes')
                            //   .format('YYYY-MM-DD HH:mm'),
                            moment(data1).format('YYYY-MM-DD HH:mm'),
                          )
                      }
                      onCancel={hideDatePickerEnd}
                      minimumDate={
                        new Date(moment(data.TimeStartTicket).format('YYYY-MM-DDTHH:mm'),)
                      }
                      // minimumDate={
                      //   new Date(
                      //     moment(data.TimeStartTicket)
                      //       .add(5, 'minutes')
                      //       .format('YYYY-MM-DD'),
                      //   )
                      // }
                      maximumDate={new Date(moment(props.timeEvent.endTime).format('YYYY-MM-DDTHH:mm'))}
                    // maximumDate={
                    //   Platform.OS === 'ios'
                    //     ? moment(props.timeEvent.endTime).format(
                    //       'YYYY-MM-DD',
                    //     ) > moment().format('YYYY-MM-DD')
                    //       ? new Date(
                    //         moment(props.timeEvent.endTime)
                    //           .subtract(1, 'minutes')
                    //           .format('YYYY-MM-DD'),
                    //       )
                    //       : new Date(moment().format('YYYY-MM-DD'))
                    //     : moment(props.timeEvent.endTime).format(
                    //       'YYYY-MM-DD HH:mm',
                    //     ) > moment().format('YYYY-MM-DD HH:mm')
                    //       ? new Date(
                    //         moment(props.timeEvent.endTime)
                    //           .subtract(1, 'minutes')
                    //           .format('YYYY-MM-DD'),
                    //       )
                    //       : new Date(moment().format('YYYY-MM-DD'))
                    // }
                    />
                  </View>
                </View>
              </View>
              <View style={[styles.container]}>
                <TouchableOpacity
                  disabled={
                    props.is_Finish === undefined || props.is_Finish === false
                      ? false
                      : true
                  }
                  onPress={() => setData({ ...data, Status: !data.Status })}
                  style={[
                    styles.inputContainerStyle,
                    {
                      marginLeft: 20 * scale,
                      // marginRight: 10 * scale,
                      flex: 1,
                      height: 36 * scale,
                      justifyContent: 'center',
                    },
                  ]}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      fontStyle: 'normal',
                      fontWeight: 'normal',
                      fontSize: (16 * scale) / scaleFontSize,
                      lineHeight: 20 * scale,
                    }}>
                    {!data.Status
                      ? convertLanguage(language, 'hide_ticket')
                      : convertLanguage(language, 'show_ticket')
                    }
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  disabled={
                    props.is_Finish === undefined || props.is_Finish === false
                      ? false
                      : true
                  }
                  onPress={() => {

                    props?.edit?.length > 0 ? props.deleteTicket() : null;
                  }}
                  style={[
                    styles.inputContainerStyle,
                    {
                      marginRight: 20 * scale,
                      marginLeft: 10 * scale,
                      flex: 1,
                      height: 36 * scale,
                      justifyContent: 'center',
                    },
                  ]}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      fontStyle: 'normal',
                      fontWeight: 'normal',
                      fontSize: (16 * scale) / scaleFontSize,
                      lineHeight: 20 * scale,
                    }}>
                    {convertLanguage(language, 'delete_ticket')}
                  </Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  paddingHorizontal: 20 * scale,
                  paddingTop: 5 * scale,
                  paddingBottom: 10 * scale,
                }}>
                <TouchableOpacity
                  style={[styles.boxMoreInfo, { borderColor: '#00A9F4' }]}
                  onPress={() => {
                    setTogle({ ...toogle, showMore: !toogle.showMore }),
                      setTimeout(
                        () => refScroll.current.scrollToEnd({ animated: true }),
                        100,
                      );
                  }}>
                  <View style={styles.boxContentMoreInfo}>
                    <Text style={[styles.txtMoreInfo, { color: '#00A9F4' }]}>
                      {toogle.showMore
                        ? convertLanguage(language, 'close')
                        : convertLanguage(language, 'more_info')}
                    </Text>
                    <Image
                      source={
                        toogle.showMore
                          ? require('../assets/up-st.png')
                          : require('../assets/down-st.png')
                      }
                      resizeMode="cover"
                      style={[styles.MoreInfo_arrow, { tintColor: '#00A9F4' }]}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              {toogle.showMore && (
                <>
                  <View
                    style={{
                      flex: 1,
                      paddingHorizontal: 20 * scale,
                      marginBottom: 10 * scale,
                    }}>
                    <TextField
                      autoCorrect={false}
                      enablesReturnKeyAutomatically={true}
                      value={data.LimitQuantity}
                      onChangeText={LimitQuantity =>
                        !isNaN(LimitQuantity)
                          ? setData({ ...data, LimitQuantity })
                          : setData({
                            ...data,
                            LimitQuantity: data.LimitQuantity,
                          })
                      }
                      returnKeyType="next"
                      label={convertLanguage(language, 'limit_per_order')}
                      baseColor={'#828282'}
                      tintColor={'#828282'}
                      errorColor={'#EB5757'}
                      inputContainerStyle={[
                        styles.inputContainerStyle,
                        { borderColor: '#BDBDBD' },
                      ]}
                      containerStyle={styles.containerStyle}

                      labelTextStyle={{
                        paddingBottom: 15 * scale,
                        paddingLeft: 12 * scale,
                      }}
                      lineWidth={2}
                      selectionColor={'#3a3a3a'}
                      style={styles.input}
                      keyboardType="number-pad"
                      errorImage={require('../assets/error.png')}
                      onBlur={() =>
                        data.LimitQuantity === '' &&
                        setData({ ...data, LimitQuantity: '10' })
                      }
                    />
                    <TextField
                      ref={refDes}
                      autoCorrect={false}
                      enablesReturnKeyAutomatically={true}
                      value={data.Description}
                      onChangeText={Description =>
                        setData({ ...data, Description })
                      }
                      returnKeyType="next"
                      label={
                        !checkFocus && data.Description.length === 0
                          ? convertLanguage(language, 'add_ticket_description')
                          : null
                      }
                      baseColor={'#828282'}
                      tintColor={'#828282'}
                      errorColor={'#EB5757'}
                      inputContainerStyle={[
                        styles.inputContainerStyle,
                        { borderColor: '#BDBDBD', height: 'auto' },
                      ]}
                      containerStyle={styles.containerStyle}
                      labelHeight={
                        !checkFocus && data.Description.length === 0
                          ? 25 * scale
                          : 10 * scale
                      }
                      labelTextStyle={{
                        paddingBottom: 15 * scale,
                        paddingLeft: 12 * scale,
                      }}
                      lineWidth={2}
                      selectionColor={'#3a3a3a'}
                      style={[styles.input, { height: 'auto' }]}
                      multiline={true}
                      errorImage={require('../assets/error.png')}
                      onFocus={() => setFocus(true)}
                      onBlur={() => setFocus(false)}
                    />
                  </View>
                  <View>
                    <TouchableOpacity
                      onPress={() =>
                        setTogle({ ...toogle, checkBox1: !toogle.checkBox1 })
                      }
                      style={{
                        marginLeft: 40 * scale,
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <Image
                        source={
                          toogle.checkBox1
                            ? require('../assets/step3CheckBox2.png')
                            : require('../assets/step3CheckBox1.png')
                        }
                        style={styles.ic_radio}
                      />
                      <Text
                        style={{
                          fontStyle: 'normal',
                          fontWeight: 'normal',
                          fontSize: 14 * scale,
                          lineHeight: 18 * scale,
                          marginRight: 10 * scale,
                          flex: 1,
                          color: '#00A9F4',
                        }}
                        numberOfLines={2}>
                        {convertLanguage(language, 'ask_a_delivery_address')}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() =>
                        setTogle({ ...toogle, checkBox2: !toogle.checkBox2 })
                      }
                      style={{
                        marginLeft: 40 * scale,
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 10 * scale,
                      }}>
                      <Image
                        source={
                          toogle.checkBox2
                            ? require('../assets/step3CheckBox2.png')
                            : require('../assets/step3CheckBox1.png')
                        }
                        style={styles.ic_radio}
                      />
                      <Text
                        style={{
                          fontStyle: 'normal',
                          fontWeight: 'normal',
                          fontSize: 14 * scale,
                          lineHeight: 18 * scale,
                          color: '#00A9F4',
                        }}>
                        {convertLanguage(language, 'ask_a_customize_question')}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  {toogle.checkBox2 && (
                    <View style={{ flex: 1, paddingHorizontal: 20 * scale }}>
                      <TextField
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        value={data.Question2}
                        onChangeText={Question2 =>
                          setData({ ...data, Question2 })
                        }
                        // error={error.manager && error.manager[0]}
                        returnKeyType="next"
                        label={convertLanguage(language, 'customize_question')}
                        baseColor={'#828282'}
                        tintColor={'#828282'}
                        errorColor={'#EB5757'}
                        inputContainerStyle={[
                          styles.inputContainerStyle,
                          { borderColor: '#BDBDBD', height: 'auto' },
                        ]}
                        containerStyle={styles.containerStyle}

                        labelTextStyle={{
                          paddingBottom: 15 * scale,
                          paddingLeft: 12 * scale,
                        }}
                        lineWidth={2}
                        selectionColor={'#3a3a3a'}
                        style={styles.input}
                        multiline={true}
                        errorImage={require('../assets/error.png')}
                      />
                    </View>
                  )}
                </>
              )}
            </KeyboardAwareScrollView>
          </ScrollView>

          <Popover
            isVisible={toogle.Type}
            animationConfig={{ duration: 0 }}
            fromView={refType.current}
            placement="bottom"
            arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0 }}
            backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)' }}
            onRequestClose={() => setTogle({ ...toogle, Type: false })}
            popoverStyle={{
              top: -10 * scale,
              position: 'absolute',
            }}>
            <View
              style={{
                width: 130 * scale,
                padding: 12 * scale,
              }}>
              {listTypes.map((item, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    onPress={() => {
                      setTogle({ ...toogle, Type: false }),
                        setData({ ...data, Type: item });
                    }}
                    style={styles.boxCountry}>
                    <Text>{item.name}</Text>
                  </TouchableOpacity>
                );
              })}
            </View>
          </Popover>

          <TouchableOpacity
            onPress={() => onSave()}
            disabled={!props.checkStatusEvent ? true : validate.checkRequire ? false : true}
            style={
              !props.checkStatusEvent ? styles.btnConfirm :
                validate.checkRequire
                  ? styles.btnConfirmActive
                  : styles.btnConfirm
            }>
            <Text
              style={
                !props.checkStatusEvent ? styles.txtConfirm :
                  validate.checkRequire
                    ? styles.txtConfirmActive
                    : styles.txtConfirm
              }>
              {convertLanguage(language, 'save2')}
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </>
  );
}
const styles = StyleSheet.create({
  modalContent: {
    backgroundColor: 'white',
    // height: 575 * scale,
    maxHeight: 600 * scale,
    borderRadius: 5,
    width: 328 * scale,
  },
  container: {
    flexDirection: 'row',
  },
  txtInfoCod: {
    fontSize: 12,
    color: '#F2994A',
    marginTop: -5,
    marginHorizontal: 20 * scale,
  },
  txtInfo: {
    fontSize: 12,
    color: '#333333',
    marginBottom: 5 * scale,
  },
  btnPhone: {
    width: 32 * scale,
    height: 32 * scale,
    marginRight: 8 * scale,
  },
  contact_tel: {
    width: 32 * scale,
    height: 32 * scale,
  },
  content: {
    padding: 22 * scale,
    justifyContent: 'center',
    borderColor: 'rgba(0, 0, 0, 0.3)',
    paddingTop: 10 * scale,
    paddingBottom: 10 * scale,
    alignItems: 'center',
  },
  inputContainerStyle: {
    borderWidth: 1,
    borderRadius: 4,
    paddingHorizontal: 12 * scale,
  },
  containerStyle: {
    marginVertical: 5 * scale,
  },
  input: {
    color: '#333333',
  },
  boxCountry: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 6 * scale,
  },
  ic_radio: {
    width: 20 * scale,
    height: 20 * scale,
    marginRight: 10 * scale,
    resizeMode: 'contain',
    tintColor: '#00A9F4',
  },
  contentTitle: {
    borderColor: 'rgba(0, 0, 0, 0.3)',
    alignItems: 'center',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14 * scale,
    lineHeight: 30 * scale,
    textAlign: 'center',
    marginHorizontal: 16 * scale,
  },
  rowClose: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  btnClose: {
    margin: 10 * scale,
    marginBottom: 5 * scale,
  },
  iconClose: {
    width: 30 * scale,
    height: 30 * scale,
  },
  boxMoreInfo: {
    marginTop: 15 * scale,
    height: 35 * scale,
    borderWidth: 2,
    borderColor: '#757575',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  boxContentMoreInfo: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtMoreInfo: {
    fontSize: 15 * scale,
    color: '#757575',
    marginRight: 10 * scale,
  },
  MoreInfo_arrow: {
    width: 18 * scale,
    height: 18 * scale,
  },
  btnConfirm: {
    backgroundColor: '#E0E2E8',
    width: 296 * scale,
    height: 36 * scale,
    borderRadius: 5 * scale,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 16 * scale,
    marginTop: 15 * scale,
  },
  btnConfirmActive: {
    backgroundColor: '#00A9F4',
    width: 296 * scale,
    height: 36 * scale,
    borderRadius: 5 * scale,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 16 * scale,
    marginTop: 15 * scale,
  },
  txtConfirm: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#B3B8BC' },
  txtConfirmActive: {
    fontSize: 16 * scale,
    lineHeight: 20 * scale,
    color: '#ffffff',
  },
});
