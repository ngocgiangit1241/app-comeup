import React, { useState, useEffect } from 'react';
import { ActivityIndicator, Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform, FlatList } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../screens_view/ScrView';
import { convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { useSelector, useDispatch } from "react-redux";
import { actImportEvent, actGetImportEvent } from '../actions/event'
const scale = width / 360
export default function ModalImportEvent(props) {
    const dispatch = useDispatch()
    const { language } = useSelector(state => state.language)
    const [data, setData] = useState([])
    const [page, setPage] = useState(1)
    const [checkEnd, setCheckEnd] = useState(false)
    useEffect(() => {
        dispatch(actImportEvent(props.HostInfoId, page)).then(res => {
            if (res?.status === 200) {
                if (res?.data) {
                    setPage(page + 1)
                    setData(res.data)
                }
                setCheckEnd(res.end)
            }
        })
    }, [])
    const onGetImport = (id, type) => {
        dispatch(actGetImportEvent(props.HostInfoId, id, type))
        props.setIdEventDtaftCallBack(id)
        props.closeModal()
    }
    const [choose, setChoose] = useState(null)
    const _renderFooter = () => {
        if (!checkEnd) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'no_data')}</Text>
        }
    }
    const loadMore = () => {
        dispatch(actImportEvent(props.HostInfoId, page)).then(res => {
            if (res?.status === 200) {
                if (res?.data) {
                    setPage(page + 1)
                    setData([...data, ...res.data])
                }
                setCheckEnd(res.end)
            }
        })
    }
    return (
        <Modal
            isVisible={true}
            backdropOpacity={0.3}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={1}
            animationOutTiming={1}
            backdropTransitionInTiming={1}
            backdropTransitionOutTiming={1}
            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
        >
            <View style={styles.modalContent}>
                <View style={styles.rowClose}>
                    <TouchableOpacity style={styles.btnClose} onPress={() => { props.closeModal() }} >
                        <Image style={styles.iconClose} source={require('../assets/close.png')} />
                    </TouchableOpacity>
                </View>
                <View style={styles.content}>
                    <Text style={styles.contentTitle}>{convertLanguage(language, 'import_events')}</Text>
                </View>
                <FlatList
                    style={{ flex: 1, width: '100%' }}
                    data={data}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity style={styles.boxEvent} onPress={() => onGetImport(item.Id, item.TypeEvent)}>
                                <Image source={{ uri: item.Posters.Small }} style={styles.imgThumb} />
                                <View style={{ flex: 1 }}>
                                    <Text style={styles.txtTitle} numberOfLines={1}>{item.Title}</Text>
                                    <Text style={styles.txtTime} numberOfLines={1}>{item.TimeStart} ~ {item.TimeFinish}</Text>
                                    <Text style={styles.txtVenue} numberOfLines={1}>{item.VenueName}</Text>
                                    <Text style={styles.txtAddress} numberOfLines={1}>{item.Address}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                    onEndReached={() => loadMore()}
                    // ListHeaderComponent={() => this.renderHeader()}
                    // ItemSeparatorComponent={this.renderSep}
                    onEndReachedThreshold={0.5}
                    // refreshing={this.props.team.loading}
                    // onRefresh={() => {this.refresh()}}
                    ListFooterComponent={() => _renderFooter()}
                />
            </View>
        </Modal>
    )
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        height: 550 * scale,
        borderRadius: 5,
        width: 328 * scale,
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 16 * scale
    },
    content: {
        padding: 10,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center',

    },
    boxEvent: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e8e8e8',
        paddingLeft: 20,
        paddingRight: 20
    },
    imgThumb: {
        width: 80,
        height: 80,
        marginRight: 15
    },
    viewScroll: {
        width: 240 * scale,
        borderWidth: 1,
        // borderColor: '#E0E0E0',
        borderRadius: 4,
        padding: 6 * scale,
        marginBottom: 12 * scale,
        lineHeight: 12 * scale

    },
    txtTitle: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        paddingTop: 5,
        paddingBottom: 5
    },
    txtTime: {
        fontSize: 13,
        color: '#333333',
        paddingBottom: 5
    },
    txtVenue: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 5
    },
    txtAddress: {
        fontSize: 13,
        color: '#333333',
        paddingBottom: 5
    },
    contentTitle: {
        borderColor: "rgba(0, 0, 0, 0.3)",
        alignItems: 'center',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 18 * scale,
        lineHeight: 20 * scale,
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10,
        marginBottom: 5
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    txtCountry: {
        fontSize: 15,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        paddingHorizontal: 20,
        textAlign: 'center'
    },
    boxChildren: {
        paddingTop: 15,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        width: 250,
        alignItems: 'center'
    },
    txtCity: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15,
    },
    btnConfirm: {
        backgroundColor: '#E0E2E8',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 16 * scale,
    },
    btnConfirmActive: {
        backgroundColor: '#00A9F4',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 16 * scale,

    },
    txtConfirm: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#B3B8BC' },
    txtConfirmActive: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#ffffff' },
});