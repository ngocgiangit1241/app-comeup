import React, { useState, useEffect } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../screens_view/ScrView';
import { convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { useSelector, useDispatch } from "react-redux";
import { actDataHostInfoByTeam } from '../actions/event'
import { lang } from 'moment';
const scale = width / 360
export default function ModalImportHost(props) {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(actDataHostInfoByTeam(props.HostInfoId, 1))
    }, [])
    const { hostsInfoByEvent } = useSelector(state => state.event)
    const { language } = useSelector(state => state.language)
    // const [state, setState] = useState([
    //     { id: 1, companyName: 'Viet1223', phone: '126545', email: 'hautran@12345', zipcode: '+84' },
    //     { id: 2, companyName: 'Viet1223', phone: '126545', email: 'hautran@12345', zipcode: '+84' },
    //     { id: 3, companyName: 'Viet1223', phone: '126545', email: 'hautran@12345', zipcode: '+84' },
    //     { id: 4, companyName: 'Viet1223', phone: '126545', email: 'hautran@12345', zipcode: '+84' },
    //     { id: 5, companyName: 'Viet1223', phone: '126545', email: 'hautran@12345', zipcode: '+84' },
    //     { id: 6, companyName: 'Viet1223', phone: '126545', email: 'hautran@12345', zipcode: '+84' },
    //     { id: 7, companyName: 'Viet1223', phone: '126545', email: 'hautran@12345', zipcode: '+84' },
    //     { id: 8, companyName: 'Viet1223', phone: '126545', email: 'hautran@12345', zipcode: '+84' },
    // ])
    const [choose, setChoose] = useState({ index: '', choose: '' })
    function importHostInfo() {
        // console.log('choose.choose', choose.choose);
        props.importHostInfo(choose.choose)
        props.closeModal()
    }
    return (
        <Modal
            isVisible={true}
            backdropOpacity={0.3}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={1}
            animationOutTiming={1}
            backdropTransitionInTiming={1}
            backdropTransitionOutTiming={1}
            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
        >
            <View style={styles.modalContent}>
                <View style={styles.rowClose}>
                    <TouchableOpacity style={styles.btnClose} onPress={() => { props.closeModal() }} >
                        <Image style={styles.iconClose} source={require('../assets/close.png')} />
                    </TouchableOpacity>
                </View>
                <View style={styles.content}>
                    <Text style={styles.contentTitle}>{convertLanguage(language, 'saved_contact_information')}</Text>
                </View>
                <SrcView >
                    <View style={styles.container}>
                        {
                            hostsInfoByEvent.map((item, index) => {
                                return (
                                    <TouchableOpacity key={index} onPress={() => { setChoose({ index, choose: item }) }}>
                                        <View style={[styles.viewScroll, { borderColor: index === choose.index ? '#00A9F4' : '#E0E0E0' }]}>
                                            <Text style={{ color: '#828282' }}>{convertLanguage(language, 'companyname')} : <Text style={{ color: '#333333' }}>{item.Name}</Text></Text>
                                            <Text style={{ color: '#828282' }}>{convertLanguage(language, 'phone')} : <Text style={{ color: '#333333' }}>{item.Zipcode + ' ' + item.Phone}</Text></Text>
                                            <Text style={{ color: '#828282' }}>{convertLanguage(language, 'email')} : <Text style={{ color: '#333333' }}>{item.Email}</Text></Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </View>
                </SrcView>
                <TouchableOpacity disabled={choose.index !== '' ? false : true} onPress={() => importHostInfo()} style={choose.index !== '' ? styles.btnConfirmActive : styles.btnConfirm}>
                    <Text style={choose ? styles.txtConfirmActive : styles.txtConfirm}>{convertLanguage(language, 'confirm')}</Text>
                </TouchableOpacity>
            </View>
        </Modal >
    )
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        height: 550 * scale,
        borderRadius: 5,
        width: 328 * scale,
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 16 * scale
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center',

    },
    viewScroll: {
        width: 240 * scale,
        borderWidth: 1,
        // borderColor: '#E0E0E0',
        borderRadius: 4,
        padding: 6 * scale,
        marginBottom: 12 * scale,
        lineHeight: 12 * scale

    },
    contentTitle: {
        borderColor: "rgba(0, 0, 0, 0.3)",
        alignItems: 'center',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 20 * scale,
        lineHeight: 30 * scale,
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10,
        marginBottom: 5
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    txtCountry: {
        fontSize: 15,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        paddingHorizontal: 20,
        textAlign: 'center'
    },
    boxChildren: {
        paddingTop: 15,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        width: 250,
        alignItems: 'center'
    },
    txtCity: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15,
    },
    btnConfirm: {
        backgroundColor: '#E0E2E8',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 16 * scale,
    },
    btnConfirmActive: {
        backgroundColor: '#00A9F4',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 16 * scale,

    },
    txtConfirm: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#B3B8BC' },
    txtConfirmActive: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#ffffff' },
});