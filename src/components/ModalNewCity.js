import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform, ScrollView } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../screens_view/ScrView';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper';
import Touchable from '../screens_view/Touchable';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import * as Config from '../constants/Config';

const scale = width / 360

class ModalNewCity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cityCurrent: '',
            current: '',
            country: '',
            city: null,
            cityId: '',
            location_loading: false
        }
    }

    async setCity(item) {
        this.setState({ cityId: item.Id, city: null, location_loading: true })
        const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${item.Name}&key=${Config.GOOGLE_API_KEY}`;
        try {
            const result = await fetch(apiUrl);
            const json = await result.json();
            if (json.results.length > 0) {
                item.Lat = json.results[0].geometry.location.lat;
                item.Lng = json.results[0].geometry.location.lng;
            }
            this.setState({ city: item, location_loading: false })
        } catch (err) {
            console.log(err)
        }
    }

    callBackCity() {
        var { city } = this.state;
        this.props.selectCity(city)
    }

    render() {
        var { countries } = this.props;
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <View style={styles.borderHori}>
                            <Text style={[styles.txtCountrySl]}>{convertLanguage(language, 'select_a_city')}</Text>
                        </View>
                        <View style={styles.borderHori}>
                            {
                                countries.map((item, index) => {
                                    return <React.Fragment key={index}>
                                        {
                                            this.state.current === '' &&
                                            <Text style={[styles.txtCountry, { borderBottomWidth: this.state.current === index ? 1 : 0 }]} onPress={() => { this.setState({ current: this.state.current === index ? '' : index }), this.fadeIn }}>{item['Name_' + language]}</Text>
                                        }
                                        {
                                            this.state.current === index &&
                                            <View style={{ width: '100%' }}>
                                                <Text style={[styles.txtCountry, { width: '100%' }]} onPress={() => this.setState({ current: this.state.current === index ? '' : index })}>{item['Name_' + language]}</Text>
                                                <View style={[styles.boxChildren, { borderBottomWidth: item.Cites.length > 0 ? 1 : 0, width: '100%', flexGrow: 1 }]}>


                                                    <ScrollView
                                                        showsVerticalScrollIndicator={false}
                                                        contentContainerStyle={styles.contentContainer}

                                                    >
                                                        {
                                                            item.Cites.map((city, index2) => {
                                                                return <Text style={this.state.cityCurrent === index2 ? styles.txtCityActive : styles.txtCity} key={index2} onPress={() => { this.setCity(city), this.setState({ cityCurrent: index2 }) }}>{city['Name_' + language]}</Text>
                                                            })
                                                        }

                                                    </ScrollView>
                                                </View>
                                            </View>
                                        }
                                    </React.Fragment>
                                })
                            }
                        </View>
                    </View>
                    <Touchable
                        onPress={() => { this.props.closeModal(), this.callBackCity() }}
                        disabled={this.state.city === null ? true : false}
                        style={this.state.city !== null ? styles.btnConfirmActive : styles.btnConfirm}
                    >
                        <Text style={this.state.city !== null ? styles.txtConfirmActive : styles.txtConfirm}>{convertLanguage(language, 'confirm')}</Text>
                    </Touchable>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    contentContainer: {
        alignItems: 'center',
        // paddingBottom: 15
        width: '100%',
        flexDirection: 'column'
    },
    modalContent: {
        backgroundColor: "white",
        height: 575 * scale,
        width: 328 * scale,
        borderRadius: 5 * scale
    },
    content: {
        paddingVertical: 22 * scale,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10 * scale,
        alignItems: 'center',
        width: '100%',
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10 * scale
    },
    iconClose: {
        width: 30 * scale,
        height: 30 * scale,
    },
    txtCountrySl: {
        fontSize: 20 * scale,
        fontWeight: '600',
        color: '#333333',
        paddingBottom: 15 * scale,
        paddingHorizontal: 20 * scale,
        textAlign: 'center'
    },
    txtCountry: {
        fontSize: 15 * scale,
        color: '#00A9F4',
        paddingVertical: 4 * scale,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    boxChildren: {
        paddingTop: 15 * scale,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        borderTopWidth: 1,
        borderTopColor: '#e5e5e5',
        width: '100%',
        height: 372 * scale,
        // alignItems: 'center'
    },
    txtCity: {
        fontSize: 14 * scale,
        color: '#333333',
        paddingBottom: 15 * scale,
        width: '100%',
        textAlign: 'center'
    },
    txtCityActive: {
        fontSize: 16 * scale,
        color: '#00A9F4',
        paddingBottom: 15 * scale,
        fontWeight: '600',
        textAlign: 'center'
    },
    btnConfirm: {
        backgroundColor: '#E0E2E8',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 16
    },
    btnConfirmActive: {
        backgroundColor: '#00A9F4',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 16 * scale
    },
    txtConfirm: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#B3B8BC' },
    txtConfirmActive: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#ffffff' },
    borderHori: { borderBottomWidth: 1, borderBottomColor: '#e5e5e5', width: '100%' }
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default React.memo(connect(mapStateToProps, null)(ModalNewCity));