import React, { useState, useRef, useEffect } from 'react';
import { Alert, Text, TouchableOpacity, View, Image, StyleSheet, StatusBar, Dimensions, SafeAreaView } from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import Swiper from 'react-native-swiper';
const { width, height } = Dimensions.get('window');
const scale = width / 360;
import Modal from "react-native-modal";
import Popover from 'react-native-popover-view';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { convertLanguage } from '../services/Helper';
import { useSelector } from "react-redux";

export default function ModalPhotoVenue(props) {
    const refOver = useRef(null)
    const { language } = useSelector(state => state.language)

    const [popover, setPopover] = useState(false)
    const [data, setData] = useState({
        index: 0
    })
    const [heightModal, setHeightModal] = useState(height)
    const [widthtModal, setWidthModal] = useState(height)

    useEffect(() => {
        setData({ ...data, index: props.index })
    }, [props.index])


    useEffect(() => {
        Image.getSize(props.images[data.index].Image?.Full, (width, height) => {
            setHeightModal(height * scale)
            setWidthModal(width * scale)
        })
    }, [data.index])
    const renderImages = () => {

        return props.images.map((image, index) => {
            return (
                <React.Fragment key={index}>
                    <Image
                        resizeMode='contain'
                        key={index}
                        style={[styles.imgZoom, { height: heightModal }]}
                        source={{ uri: image?.Image?.Full }}>
                    </Image>

                </React.Fragment>
            )
        })
    }



    const deleteImage = () => {

        Alert.alert(
            "",
            convertLanguage(language, 'do_you_want_to_delete_this_photo'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log("Cancel Pressed"),
                    style: "destructive"
                },
                {
                    text: convertLanguage(language, 'ok'),
                    onPress: () => {
                        props.deletePhoto(data.index)
                        setPopover(false)
                    }
                }
            ],
            { cancelable: false }
        )
        // 
    }
    return (
        <Modal
            onBackdropPress={props.onClose}
            isVisible={true}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={1}
            animationOutTiming={1}
            backdropTransitionInTiming={1}
            backdropTransitionOutTiming={1}
            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
        >
            {
                props.role !== 'Guest' &&
                <TouchableOpacity onPress={() => setPopover(!popover)} style={{ alignSelf: 'flex-end', height: 35 * scale, justifyContent: 'center', alignItems: 'center', marginBottom: 15 * scale }}>
                    <Image ref={refOver} source={require('../assets/more_horizontal.png')} style={{ width: 24 * scale, height: 24 * scale, tintColor: 'white' }} />
                </TouchableOpacity>}

            <View style={[styles.modalContent, { height: width * heightModal / widthtModal }]}>
                <Swiper
                    onIndexChanged={index => setData({ ...data, index })}
                    loop={false}
                    showsButtons={false}
                    index={data.index}
                    showsPagination={false}
                    loadMinimal
                    loadMinimalSize={0}>
                    {renderImages()}
                </Swiper>
                <TouchableOpacity disabled={data.index === props.length - 1 ? true : false} onPress={() => setData({ ...data, index: data.index + 1 })} style={{ opacity: data.index === props.length - 1 ? 0.3 : 1, position: 'absolute', right: 8 * scale, top: (width * heightModal / widthtModal) / 2, height: 26.67 * scale, width: 26.67 * scale, backgroundColor: 'rgba(51, 51, 51, 0.5)', borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../assets/arrowRight.png')} style={{ width: 13.33 * scale, height: 13.33 * scale }} />
                </TouchableOpacity>
                <TouchableOpacity disabled={props.length > 0 && data.index === 0 ? true : false} onPress={() => setData({ ...data, index: data.index - 1 })} style={{ opacity: props.length > 0 && data.index === 0 ? 0.3 : 1, position: 'absolute', left: 8 * scale, top: (width * heightModal / widthtModal) / 2, height: 26.67 * scale, width: 26.67 * scale, backgroundColor: 'rgba(51, 51, 51, 0.5)', borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../assets/arrowLeft.png')} style={{ width: 13.33 * scale, height: 13.33 * scale }} />
                </TouchableOpacity>
            </View>

            <Popover
                popoverStyle={{ borderRadius: 8 * scale }}
                isVisible={popover}
                animationConfig={{ duration: 0 }}
                fromView={refOver.current}
                placement='bottom'
                arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0 }}
                backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', borderRadius: 16, }}
                onRequestClose={() => setPopover(false)}>
                <TouchableOpacity style={styles.boxPopover} onPress={() => deleteImage()}>
                    <Text style={{ fontSize: 16 * scale, fontWeight: 'normal', fontStyle: 'normal' }}>{convertLanguage(language, 'delete')}</Text>
                </TouchableOpacity>
            </Popover>
        </Modal >
    )
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "transparent",
        alignSelf: 'center',
        width: width,
        // flex: 0
    },
    boxPopover: {
        flex: 1,
        width: 114 * scale,
        height: 44 * scale,
        borderRadius: 50,
        paddingLeft: 16 * scale,
        paddingVertical: 12 * scale,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    imgZoom: {
        resizeMode: 'contain',
        flex: 1,
        // height: "100%",
        // width: '100%'
    },
});
