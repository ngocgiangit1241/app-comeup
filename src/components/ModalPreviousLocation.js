import React, { Component } from 'react';
import { ActivityIndicator, Dimensions, Image, Platform, StyleSheet, Text, TextInput, TouchableOpacity, View, ScrollView } from 'react-native';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import Modal from "react-native-modal";
import { connect } from "react-redux";
import { actLoadDataVenue } from '../actions/event';
import { actLoadDataLocation } from '../actions/event'
import SrcView from '../screens_view/ScrView';
import Touchable from '../screens_view/Touchable';
import SafeView from '../screens_view/SafeView'
import { convertLanguage } from '../services/Helper';
import * as Colors from '../constants/Colors'
import Line from '../screens_view/Line'
const { width, height } = Dimensions.get('window')
const scale = width / 360

class ModalPreviousLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        this.props.onLoadDataLocation();
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    selectLocation(item) {
        this.props.selectLocation(item);
        this.props.closeModal()
    }

    render() {
        var { loading, locations } = this.props.event;
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={{ backgroundColor: Colors.BG, height: 576 * scale, borderRadius: 5 * scale }}>
                    <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>

                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                        <Touchable
                            onPress={() => {
                                this.props.closeModal()
                            }}
                            style={{
                                width: 50,
                                justifyContent: 'center', alignItems: 'center',
                            }}>
                            <Image style={styles.iconClose} source={require('../assets/close.png')} />
                        </Touchable>
                    </View>

                    <ScrollView style={{ flex: 1 }}>
                        {
                            loading ?
                                <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                                :
                                locations.length > 0
                                    ?
                                    <View style={styles.content}>
                                        {
                                            locations.map((item, index) => {
                                                return <Touchable style={styles.boxLocation} key={index} onPress={() => this.selectLocation(item)}>
                                                    <Text style={styles.txtVenue}>{item.Name}</Text>
                                                    <Text style={styles.txtAddress}>{item.Address}</Text>
                                                </Touchable>
                                            })
                                        }
                                    </View>
                                    :
                                    <Text style={{ textAlign: 'center', paddingTop: 20 * scale, paddingBottom: 20 * scale }}>{convertLanguage(language, 'data_empty')}</Text>
                        }
                    </ScrollView>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 24 * scale,
        paddingRight: 24 * scale,
    },
    boxLocation: {
        paddingVertical: 16 * scale,
        borderBottomWidth: 2,
        borderBottomColor: '#e8e8e8'
    },
    txtVenue: {
        fontSize: 16 * scale,
        color: '#333333',
        paddingBottom: 4 * scale,
        fontWeight: '600'
    },
    txtAddress: {
        fontSize: 14 * scale,
        color: '#828282'
    },
    iconClose: {
        width: 30 * scale,
        height: 30 * scale,
        marginTop: 8 * scale,
        marginRight: 8 * scale
    }
});
const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataLocation: () => {
            dispatch(actLoadDataLocation())
        },
    }
}
export default React.memo(connect(mapStateToProps, mapDispatchToProps)(ModalPreviousLocation));