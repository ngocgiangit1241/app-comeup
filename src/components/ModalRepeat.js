import React, { useState, useRef, useEffect } from 'react';
import { PixelRatio, Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
// import SrcView from '../screens_view/ScrView';
import { convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { useSelector } from "react-redux";
import { TextField } from '../components/react-native-material-textfield';
// import Popover from 'react-native-popover-view';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import { Calendar, LocaleConfig } from 'react-native-calendars';
import Moment from 'moment/min/moment-with-locales';

const scale = width / 360
const scaleFontSize = PixelRatio.getFontScale()

export default function ModalRepeat(props) {
    const calendarLocaleConfig = locale => {
        const moment_locale = Moment.localeData(locale);

        return {
            monthNames: moment_locale.months(),
            monthNamesShort: moment_locale.monthsShort(),
            dayNames: moment_locale.weekdays(),
            dayNamesShort: moment_locale.weekdaysShort(),
        };
    };
    const language = useSelector(state => state.language.language)
    Moment.locale(language);

    LocaleConfig.locales[language] = calendarLocaleConfig(language);
    LocaleConfig.defaultLocale = language;
    // LocaleConfig.locales['fr'] = {
    //     monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    //     monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
    //     dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    //     dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
    //     today: 'Aujourd\'hui'
    // };
    // // LocaleConfig.defaultLocale = 'en';
    const [data, setData] = useState({
        startTime: '',
        endTime: ''
    })
    // 
    useEffect(() => {
        if (props.edit.length > 0) {
            let editTime = props.edit[1].split(' ~ ');
            setData({
                startTime: editTime[0].split(' ')[0],
                endTime: editTime[1].split(' ')[0]
            })
        }
    }, [])
    const numberDate = (new Date(moment(props.dataRepeat.endTime).format('YYYY-MM-DD')).getTime() - new Date(moment(props.dataRepeat.startTime).format('YYYY-MM-DD')).getTime()) / (1000 * 3600 * 24)
    const [markedDates, setMarkedDates] = useState({
        data: '',
        label: {}
    })
    const [isDatePickerVisible, setDatePickerVisibility] = useState({
        start: false,
        end: false
    });

    const showDatePickerStart = () => {
        setDatePickerVisibility({ ...isDatePickerVisible, start: true });
    };

    const hideDatePickerStart = () => {
        setDatePickerVisibility({ ...isDatePickerVisible, start: false });
    };
    const handleConfirmStart = (date) => {
        setData({ ...data, startTime: moment(date).format('YYYY-MM-DD'), endTime: moment(date).add(numberDate, 'days').format('YYYY-MM-DD') })

        hideDatePickerStart();
    };
    const showDatePickerEnd = () => {
        setDatePickerVisibility({ ...isDatePickerVisible, end: true });
    };

    const hideDatePickerEnd = () => {
        setDatePickerVisibility({ ...isDatePickerVisible, end: false });
    };

    const handleConfirmEnd = (date) => {
        setData({ ...data, endTime: moment(date).format('YYYY-MM-DD') })
        hideDatePickerEnd();
    };
    useEffect(() => {
        let dateCurr = {}
        var getDaysArray = function (s, e) { for (var a = [], d = new Date(s); d <= e; d.setDate(d.getDate() + 1)) { a.push(new Date(d)); } return a; };
        var daylist = getDaysArray(new Date(data.startTime), new Date(data.endTime));
        daylist.map((v) => v.toISOString().slice(0, 10)).join("")
        daylist.forEach(element => {
            dateCurr[moment(element).format('YYYY-MM-DD')] = {
                customStyles: {
                    container: {
                        backgroundColor: '#00A9F4',
                        elevation: 2
                    },
                    text: {
                        color: 'white',
                        fontWeight: 'bold'
                    }
                }
            }
        });
        setMarkedDates({ data: data.startTime + ' ' + props.startTime + ' ~ ' + data.endTime + ' ' + props.endTime, label: dateCurr })
        // setMarkedDates({ data: data.startTime, label: dateCurr })
        // }
    }, [data, numberDate])

    const onSave = () => {
        // { props.changeDataRepeat(markedDates.data), props.closeModal() }
        if (markedDates.data !== '' && markedDates.data !== ' ~ ' && data.startTime !== '' && data.endTime !== '') {

            props.changeDataRepeat(markedDates.data, props.edit[0])
        }
    }
    // console.log('markedDates', markedDates)
    return (
        <Modal
            isVisible={true}
            backdropOpacity={0.3}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={1}
            animationOutTiming={1}
            backdropTransitionInTiming={1}
            backdropTransitionOutTiming={1}
            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
        >
            <View style={styles.modalContent}>
                <View style={styles.rowClose}>
                    <TouchableOpacity style={styles.btnClose} onPress={() => { props.closeModal() }} >
                        <Image style={styles.iconClose} source={require('../assets/close.png')} />
                    </TouchableOpacity>
                </View>

                <View style={{ paddingHorizontal: 20 * scale, alignItems: 'center', justifyContent: "flex-end" }}>
                    <Text style={{ justifyContent: 'center', alignItems: 'center', textAlign: 'center', fontSize: 20 * scale / scaleFontSize }}>{convertLanguage(language, 'choose_repeat')}</Text>
                    <View style={{ flexDirection: 'row', height: 80 * scale }}>
                        <View style={{ flex: 1 / 2, marginRight: 10 * scale }}>
                            <TouchableOpacity onPress={() => showDatePickerStart()} style={{ flex: 1 / 2 }}>
                                <TextField
                                    autoCorrect={false}
                                    enablesReturnKeyAutomatically={true}
                                    value={data.startTime}
                                    // onChangeText={(companyName) => setData({ ...data, companyName })}
                                    // error={error.manager && error.manager[0]}
                                    editable={false}
                                    returnKeyType='next'
                                    label={convertLanguage(language, 'start_date')}
                                    baseColor={'#828282'}
                                    tintColor={'#828282'}
                                    errorColor={'#EB5757'}
                                    inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD' }]}
                                    containerStyle={[styles.containerStyle]}
                                    labelHeight={25 * scale}
                                    labelTextStyle={{ paddingBottom: 15 * scale, paddingLeft: 12 * scale }}
                                    lineWidth={2}
                                    selectionColor={'#3a3a3a'}
                                    fontSize={10.5 * scale / scaleFontSize}
                                    labelFontSize={12 * scale / scaleFontSize}
                                    style={{ flex: 1 }}
                                    errorImage={require('../assets/error.png')}
                                />
                                <Image source={require('../assets/Cal.png')} style={{
                                    width: 20 * scale,
                                    height: 20 * scale,
                                    position: 'absolute',
                                    top: 0,
                                    right: 0,
                                    marginTop: 30 * scale,
                                    marginRight: 7 * scale,
                                    flex: 1 / 4,
                                    resizeMode: 'contain'
                                }} />
                            </TouchableOpacity>
                            <DateTimePickerModal
                                isVisible={isDatePickerVisible.start}
                                mode="date"
                                cancelTextIOS={convertLanguage(language, 'cancel')}
                                confirmTextIOS={convertLanguage(language, 'confirm')}
                                locale={language}
                                format="YYYY-MM-DD HH:mm"
                                onCancel={hideDatePickerStart}
                                // onConfirm={(data1) => new Date(data1).getTime() > new Date(props.dataRepeat.endTime).getTime() + 1000 * 60 * 60 * 24 ? handleConfirmStart(data1) : handleConfirmStart(new Date(props.dataRepeat.endTime).getTime() + 1000 * 60 * 60 * 24)}
                                // minimumDate={new Date(props.dataRepeat.endTime).getTime() + 1000 * 60 * 60 * 24}
                                minimumDate={
                                    Platform.OS === 'ios' ?
                                        new Date(moment(props.dataRepeat.endTime).add(1, 'days').format('YYYY-MM-DD HH:mm'))
                                        :
                                        new Date(moment(props.dataRepeat.endTime).add(1, 'days').format('YYYY-MM-DD'))
                                }
                                onConfirm={data1 => moment(data1).format('YYYY-MM-DD HH:mm') > moment(props.dataRepeat.endTime).add(1, 'days').format('YYYY-MM-DD HH:mm') ? handleConfirmStart(moment(data1).format('YYYY-MM-DD HH:mm')) : handleConfirmStart(moment(props.dataRepeat.endTime).add(1, 'days').format('YYYY-MM-DD HH:mm'))}
                            />
                        </View>
                        <View style={{ flex: 1 / 2 }}>
                            <View style={{ flex: 1 / 2, marginLeft: 10 * scale }}>
                                <TouchableOpacity onPress={() => showDatePickerEnd()} disabled={true}>
                                    <TextField
                                        autoCorrect={false}
                                        enablesReturnKeyAutomatically={true}
                                        value={data.endTime}
                                        // onChangeText={(companyName) => setData({ ...data, companyName })}
                                        // error={error.manager && error.manager[0]}
                                        editable={false}
                                        returnKeyType='next'
                                        label={convertLanguage(language, 'finish_date')}
                                        baseColor={'#828282'}
                                        tintColor={'#828282'}
                                        errorColor={'#EB5757'}
                                        inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#E0E2E8', backgroundColor: '#E0E2E8' }]}
                                        containerStyle={styles.containerStyle}
                                        labelHeight={25 * scale}
                                        labelTextStyle={{ paddingBottom: 15 * scale, paddingLeft: 12 * scale }}
                                        lineWidth={2}
                                        selectionColor={'#3a3a3a'}
                                        style={styles.input}
                                        fontSize={10.5 * scale / scaleFontSize}
                                        labelFontSize={12 * scale / scaleFontSize}
                                        errorImage={require('../assets/error.png')}
                                    />
                                    <Image source={require('../assets/Cal.png')} style={{
                                        width: 20 * scale,
                                        height: 20 * scale,
                                        // marginTop: 15,
                                        position: 'absolute',
                                        top: 0,
                                        right: 0,
                                        marginTop: 30 * scale,
                                        marginRight: 7 * scale,
                                        resizeMode: 'contain'
                                    }} />
                                </TouchableOpacity>

                            </View>

                        </View>

                    </View>
                    <View style={{ width: width - 40 * scale }}>
                        <Calendar
                            markingType={'custom'}
                            markedDates={markedDates.label}
                            theme={{
                                backgroundColor: '#424242',
                                calendarBackground: '#424242',
                                textSectionTitleColor: '#b6c1cd',
                                selectedDayBackgroundColor: '#67938E',
                                selectedDayTextColor: '#424242',
                                // todayTextColor: 'red',
                                dayTextColor: '#ffffff',
                                textDisabledColor: '#bdbdbd',
                                selectedDotColor: '#ffffff',
                                arrowColor: '#ffffff',
                                monthTextColor: '#ffffff',
                                textMonthFontWeight: 'bold',
                                textDayFontSize: 16,
                                textDayFontWeight: 'bold',
                                textMonthFontSize: 16,
                                textDayHeaderFontSize: 16
                            }}
                        />
                    </View>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    // position: 'absolute',
                    // bottom: 16,
                    alignSelf: 'center',
                    marginVertical: 10 * scale
                }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => { props.closeModal() }} style={{ borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', borderWidth: 1, marginRight: 16 * scale, width: 90 * scale, height: 30 * scale, borderColor: '#4F4F4F' }}>
                            <Text style={{
                                fontStyle: 'normal',
                                fontWeight: 'normal',
                                fontSize: 14 * scale / scaleFontSize,
                                lineHeight: 20 * scale,
                                color: '#4F4F4F'
                            }}>{convertLanguage(language, 'discard')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => onSave()} style={{ borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', width: 170 * scale, height: 30 * scale, backgroundColor: markedDates.data !== '' && markedDates.data !== ' ~ ' && data.startTime !== '' && data.endTime !== '' ? '#00A9F4' : '#BDBDBD' }}>
                            <Text style={{
                                color: 'white', fontStyle: 'normal',
                                fontWeight: 'normal',
                                fontSize: 14 * scale / scaleFontSize,
                                lineHeight: 20 * scale,
                            }}>{convertLanguage(language, 'save')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        </Modal >
    )
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        // height: 520 * scale,
        borderRadius: 5,
        width: 328 * scale,
    },
    inputContainerStyle: {
        borderWidth: 1,
        borderRadius: 4 * scale,
        paddingHorizontal: 12 * scale
    },
    containerStyle: {
        // paddingHorizontal: 20,
        // flex: 1
        marginVertical: 10 * scale,
    },
    contentTitle: {
        borderColor: "rgba(0, 0, 0, 0.3)",
        alignItems: 'center',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 20 * scale / scaleFontSize,
        lineHeight: 30 * scale,
        textAlign: 'center',
        marginHorizontal: 16 * scale
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10 * scale,
        marginBottom: 5 * scale
    },
    iconClose: {
        width: 30 * scale,
        height: 30 * scale,
    },

    btnConfirm: {
        backgroundColor: '#E0E2E8',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 16 * scale,
    },
    btnConfirmActive: {
        backgroundColor: '#00A9F4',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 16 * scale,

    },
    txtConfirm: { fontSize: 16 * scale / scaleFontSize, lineHeight: 20 * scale, color: '#B3B8BC' },
    txtConfirmActive: { fontSize: 16 * scale / scaleFontSize, lineHeight: 20 * scale, color: '#ffffff' },
});
