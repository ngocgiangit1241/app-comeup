import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../screens_view/ScrView';
import Touchable from '../screens_view/Touchable';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
// import { Colors } from 'react-native/Libraries/NewAppScreen';
const scale = width / 360
class ModalTagNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isActive: false
        }
    }

    render() {
        var { selected } = this.props;
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.container}>
                        <View style={styles.content}>
                            <Text style={[styles.txtCountry, { borderBottomWidth: 0, paddingBottom: 5 * scale, }]}>{convertLanguage(language, 'choose_category')}</Text>
                            <Text style={[styles.txtCountry, { borderBottomWidth: 0, paddingBottom: 10 * scale, }]}>{convertLanguage(language, 'select_category_event_0')}</Text>
                            <Text style={styles.borderModel} />
                            <View style={styles.boxListCategory}>
                                {

                                    this.props.tags.map((data, index) => {
                                        var backgroundColor = '#FFFFFF';
                                        var color = "#333333";
                                        var fontWeight = 'normal';
                                        var marginRight = (index + 1) % 3 === 0 ? 0 : 12 * scale;
                                        var borderWidth = 1;
                                        var borderRadius = 4;
                                        var borderColor = '#4F4F4F';
                                        // var i = selected.length > 0 ? selected.indexOf(data.value) : -1;
                                        var i = selected.some(el => el.value === data.value);
                                        if (i) {
                                            if (selected[0].value === data.value) {
                                                backgroundColor = '#00acfb';
                                                color = "#FFFFFF";
                                                fontWeight = 'bold';
                                                borderColor = '#00acfb';
                                            } else {
                                                backgroundColor = '#FFFFFF';
                                                color = "#00acfb";
                                                fontWeight = 'bold';
                                                borderWidth = 1;
                                                borderColor = '#00acfb';
                                            }
                                        }
                                        return (
                                            <Touchable style={[styles.btnCategory, { borderRadius, borderColor, backgroundColor, marginRight, borderWidth }]} key={index}
                                                onPress={() => this.props.selectCategory(data)}>
                                                <Text style={[styles.txtCategory, { color, fontWeight, textAlign: 'center' }]} numberOfLines={2}>{index === 0 ? '' : ''}{data.name}</Text>
                                            </Touchable>
                                        )
                                    })
                                }
                            </View>
                            <Text style={styles.borderModel} />
                            <Touchable
                                onPress={() => { this.props.closeModal(), this.props.onConfirm() }}
                                disabled={selected.length === 0 ? true : false}
                                style={selected.length < 6 && selected.length > 0 ? styles.btnConfirmActive : styles.btnConfirm} >
                                <Text style={selected.length < 6 && selected.length > 0 ? styles.txtConfirmActive : styles.txtConfirm}>{convertLanguage(language, 'confirm')}</Text>
                            </Touchable>
                        </View>
                    </View>
                </View>
            </Modal >
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5 * scale,
        // height: 575 * scale,
        width: 328 * scale,
        // textAlign: 'center'
    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10 * scale,
        alignItems: 'center',
        // textAlign: 'center'
        // backgroundColor: 'red'
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10 * scale
    },
    iconClose: {
        width: 30 * scale,
        height: 30 * scale,
    },
    boxListCategory: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 10 * scale,
        // justifyContent: ,
    },
    btnCategory: {
        width: 86 * scale,
        height: 35 * scale,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2 * scale,
        marginBottom: 15 * scale,
        borderColor: '#00acfb',

    },
    txtCountry: {
        fontSize: 15 * scale,
        color: '#333333',
        paddingBottom: 15 * scale,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    txtCategory: {
        fontSize: 11 * scale,
    },
    borderModel: { borderWidth: StyleSheet.hairlineWidth, width: 296 * scale, height: 0, marginVertical: 16 * scale, borderColor: '#333333', opacity: 0.25 },
    btnConfirm: {
        backgroundColor: '#E0E2E8',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnConfirmActive: {
        backgroundColor: '#00A9F4',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtConfirm: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#B3B8BC' },
    txtConfirmActive: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#ffffff' }
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default React.memo(connect(mapStateToProps, null)(ModalTagNew));