import React, { useState, useEffect } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../screens_view/ScrView';
import { useSelector } from "react-redux";
import { convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import ContentTermOfService from '../containers/ContentTermOfService'
const scale = width / 360

export default function ModalTermOfService(props) {
    return (
        <Modal
            isVisible={props.modalTermOfService}
            backdropOpacity={0.3}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={1}
            animationOutTiming={1}
            backdropTransitionInTiming={1}
            backdropTransitionOutTiming={1}
            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
        >
            <View style={styles.modalContent}>
                {/* <View style={styles.rowClose}>
            <TouchableOpacity style={styles.btnClose} onPress={() => { props.setModalTermOfService() }} >
                <Image style={styles.iconClose} source={require('../assets/close.png')} />
            </TouchableOpacity>
        </View> */}
                <View style={styles.container}>

                    <ContentTermOfService {...props} />

                </View>
            </View>
        </Modal >
    );
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        // height: 300 * scale,
        width: '100%',
        // marginLeft: 50 * scale,
        // marginRight: 50 * scale,
        borderRadius: 5 * scale,
        alignSelf: 'center',
    },
    container: {

    },
    content: {
        // padding: 22 * scale,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10 * scale,
        alignItems: 'center',
        marginTop: 30 * scale,
        textAlign: 'center',
        marginHorizontal: 15 * scale,
        marginBottom: 20 * scale
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    txtSelectCountry: {
        fontSize: 15,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        textAlign: 'center'
    },
    txtCountry: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15,
    },
    boxChildren: {
        paddingTop: 15,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        width: 250,
        alignItems: 'center'
    },
    txtCity: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15,
    }
});




