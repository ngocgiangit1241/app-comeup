import React, { Component } from 'react';
import { ActivityIndicator, Dimensions, Image, Platform, StyleSheet, Text, TextInput, TouchableOpacity, View, ScrollView, Keyboard } from 'react-native';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import Modal from "react-native-modal";
import { connect } from "react-redux";
import { actLoadDataVenue } from '../actions/event';
import SrcView from '../screens_view/ScrView';
import Touchable from '../screens_view/Touchable';
import SafeView from '../screens_view/SafeView'
import { convertLanguage } from '../services/Helper';
import * as Colors from '../constants/Colors'
import Line from '../screens_view/Line'
const { width, height } = Dimensions.get('window')
const scale = width / 360

class ModalVenue extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            selected: '',
            data: {}
        };
    }

    componentDidMount() {
        // this.getTeams();
    }

    componentDidUpdate() {

    }
    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    onChangeVenue(name) {
        this.setState({
            name
        })
        this.props.onLoadDataVenue(name, this.props.AreaId)
    }

    selectVenue(item, index) {
        this.setState({ selected: index, data: item })
        Keyboard.dismiss()
    }

    onGetVenue() {
        this.props.selectVenue(2, this.state.name);
        this.props.closeModal()
    }

    onGetDataVenue() {
        var { data, selected } = this.state;
        if (selected === -1) {
            this.props.selectVenue(2, this.state.name);
            this.props.closeModal()
        } else {
            this.props.selectVenue(1, data);
            this.props.closeModal()
        }
    }

    render() {
        var { loading, venues } = this.props.event;
        var { language } = this.props.language;
        var { selected } = this.state;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                // swipeDirection={['up', 'left', 'right', 'down']}
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <View style={styles.borderHori}>
                            <Text style={[styles.txtCountrySl]}>{convertLanguage(language, 'venue_name')}</Text>
                        </View>
                        {/* <View style={{}}> */}
                        <View style={styles.boxInput}>
                            <TextInput
                                style={styles.ipContent}
                                selectionColor="#bcbcbc"
                                value={this.state.name}
                                onChangeText={(name) => this.onChangeVenue(name)}
                            />
                        </View>
                        <SrcView style={{ flex: 1, paddingLeft: 10 * scale, paddingRight: 10 * scale }}>
                            {
                                this.state.name != '' &&
                                <Touchable style={styles.boxLocation2} onPress={() => { this.setState({ selected: -1 }), Keyboard.dismiss() }}>
                                    <View style={[styles.boxContentLocation, { flexDirection: 'row' }]}>
                                        <Image source={require('../assets/Pin-Add.png')} style={{ width: 24 * scale, height: 24 * scale, marginRight: 5 * scale }} />
                                        {/* <Text style={[styles.txtVenue, selected === -1 ? { color: '#03a9f4' } : {}]}>{convertLanguage(language, 'add')} <Text style={{ color: '#333333' }}>{this.state.name}</Text> {convertLanguage(language, 'as_new_venue')}</Text> */}
                                        <Text style={[styles.txtVenue, { color: '#00A9F4', alignSelf: 'center', lineHeight: 24 * scale, fontWeight: '600', fontSize: 16 * scale }]}>
                                            {convertLanguage(language, 'add')} {this.state.name} {convertLanguage(language, 'as_new_venue')}

                                        </Text>
                                    </View>
                                    {
                                        // item.IsLinked
                                        // &&
                                        // <Image source={require('../../assets/linked_icon.png')} style={styles.linked_icon} />
                                    }
                                </Touchable>
                            }
                            {
                                loading ?
                                    <ActivityIndicator size="large" color="#000000" style={{ padding: 20 * scale }} />
                                    :
                                    venues.map((item, index) => {
                                        return <Touchable style={[styles.boxLocation, , selected === index ? { borderColor: '#00A9F4' } : {}]} key={index} onPress={() => this.selectVenue(item, index)}>
                                            <View style={[styles.boxContentLocation]}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', height: 24 * scale }}>
                                                    <Image source={require('../assets/LOCATIONPINNEW.png')} style={{ width: 16 * scale, height: 16 * scale, marginRight: 5 * scale }} />
                                                    <Text style={[styles.txtVenue, { lineHeight: 24 * scale, fontSize: 16 * scale, fontWeight: '600' }, selected === index ? { color: '#00A9F4' } : {}]}>{item.Name}</Text>
                                                </View>
                                                <Text style={[styles.txtAddress, { fontSize: 14 * scale }]}>{item.Address}</Text>
                                            </View>
                                            {
                                                // item.IsLinked
                                                // &&
                                                // <Image source={require('../assets/linked_icon.png')} style={styles.linked_icon} />
                                            }
                                        </Touchable>
                                    })
                            }
                        </SrcView>

                        {/* </View> */}
                        {/* </SafeView> */}
                        {/* </View> */}
                    </View>
                    <Touchable disabled={selected === ''} style={[styles.btnOk, { backgroundColor: selected !== '' ? '#03a9f4' : '#e8e8e8' }]} onPress={() => this.onGetDataVenue()}>
                        <Text style={[styles.txtOk, { color: selected !== '' ? '#FFFFFF' : '#BCBCBC' }]}>{convertLanguage(language, 'confirm')}</Text>
                    </Touchable>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    contentContainer: {
        alignItems: 'center',
        // paddingBottom: 15
    },
    modalContent: {
        backgroundColor: "white",
        height: 575 * scale,
        width: 328 * scale,
        borderRadius: 5 * scale
    },
    content: {
        padding: 22 * scale,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10 * scale,
        alignItems: 'center'
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10 * scale
    },
    iconClose: {
        width: 30 * scale,
        height: 30 * scale,
    },
    txtCountrySl: {
        fontSize: 20 * scale,
        fontWeight: '600',
        color: '#333333',
        paddingBottom: 15 * scale,
        paddingHorizontal: 20 * scale,
        textAlign: 'center'
    },
    txtCountry: {
        fontSize: 15 * scale,
        color: '#00A9F4',
        paddingVertical: 4 * scale,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    boxChildren: {
        paddingTop: 15 * scale,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        borderTopWidth: 1,
        borderTopColor: '#e5e5e5',
        width: '100%',
        height: 372 * scale,
        alignItems: 'center'
    },
    txtCity: {
        fontSize: 14 * scale,
        color: '#333333',
        paddingBottom: 15 * scale,
    },
    txtCityActive: {
        fontSize: 16 * scale,
        color: '#00A9F4',
        paddingBottom: 15 * scale,
        fontWeight: '600'
    },
    btnConfirm: {
        backgroundColor: '#E0E2E8',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 16
    },
    btnConfirmActive: {
        backgroundColor: '#00A9F4',
        width: 296 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 16 * scale
    },
    txtConfirm: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#B3B8BC' },
    txtConfirmActive: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#ffffff' },
    borderHori: { borderBottomWidth: 1, borderBottomColor: '#e5e5e5', width: '100%' },
    content: {
        flex: 1,
    },
    boxInput: {
        marginLeft: 20 * scale,
        marginRight: 20 * scale,
    },
    ipContent: {
        marginTop: 15 * scale,
        fontSize: 15 * scale,
        fontWeight: 'bold',
        color: '#333333',
        paddingTop: 8 * scale,
        paddingBottom: 8 * scale,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        paddingLeft: 12 * scale,
        borderRadius: 4,
        height: 50 * scale
    },
    boxLocation: {
        paddingTop: 15 * scale,
        paddingBottom: 15 * scale,
        // borderBottomWidth: 2 * scale,
        // borderBottomColor: '#e8e8e8',
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        marginVertical: 8 * scale,
        borderColor: '#E0E0E0',
        paddingHorizontal: 8 * scale,
        borderRadius: 4
    },
    boxLocation2: {
        paddingTop: 15 * scale,
        // paddingBottom: 15 * scale,
        // borderBottomWidth: 2 * scale,
        // borderBottomColor: '#e8e8e8',
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        // borderWidth: 1,
        // marginVertical: 8 * scale
    },
    boxContentLocation: {
        flex: 1,
    },
    linked_icon: {
        marginLeft: 10 * scale,
        width: 27 * scale,
        height: 27 * scale
    },
    txtVenue: {
        fontSize: 15,
        color: '#757575',
        paddingBottom: 10 * scale,
        fontWeight: 'normal'
    },
    txtAddress: {
        fontSize: 15 * scale,
        color: '#757575'
    },
    btnOk: {
        height: 48 * scale,
        backgroundColor: '#03a9f4',
        marginTop: 10 * scale,
        marginBottom: 16 * scale,
        marginLeft: 20 * scale,
        marginRight: 20 * scale,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    },
    txtOk: {
        fontSize: 17 * scale,
        fontWeight: 'bold',
        color: '#ffffff',

    },
});
const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataVenue: (name, AreaId) => {
            dispatch(actLoadDataVenue(name, AreaId))
        },
    }
}
export default React.memo(connect(mapStateToProps, mapDispatchToProps)(ModalVenue));