import React, { PureComponent } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';
import { useSelector } from "react-redux";
import Touchable from '../../screens_view/Touchable';
import { convertDate, convertLanguage } from '../../services/Helper';
function ItemEventRequest({ data, onAcceptEvent, onDenyEvent, navigation }) {

    const language = useSelector(state => state.language)

    return <View style={[styles.boxRequest, { marginBottom: 24 }]}>
        <View style={[styles.row, { alignItems: 'stretch' }]}>
            <Touchable onPress={() => navigation.navigate({ name: 'EventDetail', params: { Slug: data.Slug }, key: data.Slug })}><FastImage source={{ uri: data.Posters ? data.Posters.Medium : data.Poster }} style={styles.avatar_event} /></Touchable>
            <View style={[styles.boxContent, { justifyContent: 'space-between' }]}>
                <View style={{ marginTop: 5 }}>
                    <Text style={styles.txtInvite}>{convertLanguage(language.language, 'you_have_been_invited_by_manager', { name: data.InviteBy && data.InviteBy.Name ? data.InviteBy.Name : 'Manager' })}</Text>
                    <Touchable onPress={() => navigation.navigate({ name: 'EventDetail', params: { Slug: data.Slug }, key: data.Slug })}>
                        <Text style={styles.txtTitle} numberOfLines={2}>{data.Title}</Text>
                        <Text style={styles.txtContent} numberOfLines={2}>{convertDate(data.TimeStart, data.TimeFinish)}</Text>
                        <Text style={styles.txtCategory} numberOfLines={2}>{data.VenueName}, {data.Address}</Text>
                    </Touchable>
                </View>
                {
                    data.isAccept &&
                    <Text style={styles.txtLabel}>{convertLanguage(language.language, 'invite_accepted_ev')}</Text>
                }
                {
                    data.isDeny &&
                    <Text style={styles.txtLabel}>{convertLanguage(language.language, 'invite_denied')}</Text>
                }
                {
                    !data.isAccept && !data.isDeny &&
                    <View style={[styles.row, { marginBottom: 5 }]}>
                        <Touchable style={styles.btnAccept} onPress={() => onAcceptEvent(data.Id)}>
                            <Text style={styles.txtAccept}>{convertLanguage(language.language, 'accept')}</Text>
                        </Touchable>
                        <Touchable style={styles.btnDeny} onPress={() => onDenyEvent(data.Id)}>
                            <Text style={styles.txtDeny}>{convertLanguage(language.language, 'deny')}</Text>
                        </Touchable>
                    </View>
                }
            </View>
        </View>
    </View>
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    boxContent: {
        flex: 1,
        paddingRight: 16
    },
    boxRequest: {
        marginBottom: 16
    },
    txtInvite: {
        color: '#00A9F4',
        fontSize: 12,
        marginBottom: 8
    },
    avatar_team: {
        width: 100,
        height: 100,
        borderRadius: 50,
        marginRight: 16
    },
    txtCategory: {
        fontSize: 12,
        color: '#4F4F4F',
        marginBottom: 5
    },
    btnAccept: {
        height: 32,
        paddingHorizontal: 16,
        borderRadius: 4,
        backgroundColor: '#00A9F4',
        marginRight: 10,
        justifyContent: 'center'
    },
    txtAccept: {
        color: '#FFFFFF',
        fontSize: 16
    },
    btnDeny: {
        height: 32,
        paddingHorizontal: 16,
        borderRadius: 4,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#4F4F4F',
        justifyContent: 'center'
    },
    txtDeny: {
        color: '#4F4F4F',
        fontSize: 16
    },
    avatar_event: {
        width: 100,
        height: 176,
        borderRadius: 4,
        marginRight: 16
    },
    txtTitle: {
        fontSize: 16,
        color: '#333333',
        marginBottom: 5,
        fontWeight: 'bold'
    },
    txtContent: {
        fontSize: 12,
        color: '#828282',
        marginBottom: 5
    },
    txtLabel: {
        fontSize: 16,
        color: '#00A9F4',
        marginBottom: 5
    }
});

export default ItemEventRequest;
