import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Picker, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import PickerItem from '../../screens_view/PickerItem';
import Modal from "react-native-modal";
import { convertLanguage } from '../../services/Helper';
import { connect } from "react-redux";
import SafeView from '../../screens_view/SafeView';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalRefundRate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Rate: 0,
        }
    }


    render() {
        var { language } = this.props.language;
        var { Rate } = this.state;
        var options = [];
        for (var i = 100; i > 0; i--) {
            options.push({ name: '' + i + '%', value: i })
        }
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <SafeView style={styles.modalContent}>
                    <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.txtTitle}>{convertLanguage(language, 'set_rate_refund')}</Text>
                        <View style={styles.boxRate}>
                            <TouchableOpacity style={[styles.btnRate, Rate == 10 ? { backgroundColor: '#FFFFFF', borderColor: '#03a9f4', borderWidth: 1 } : {}]} onPress={() => this.setState({ Rate: 10 })}>
                                <Text style={[styles.txtRate, Rate == 10 ? { color: '#03a9f4' } : {}]}>10%</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btnRate, Rate == 20 ? { backgroundColor: '#FFFFFF', borderColor: '#03a9f4', borderWidth: 1 } : {}]} onPress={() => this.setState({ Rate: 20 })}>
                                <Text style={[styles.txtRate, Rate == 20 ? { color: '#03a9f4' } : {}]}>20%</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.boxRate}>
                            <TouchableOpacity style={[styles.btnRate, Rate == 30 ? { backgroundColor: '#FFFFFF', borderColor: '#03a9f4', borderWidth: 1 } : {}]} onPress={() => this.setState({ Rate: 30 })}>
                                <Text style={[styles.txtRate, Rate == 30 ? { color: '#03a9f4' } : {}]}>30%</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btnRate, Rate == 40 ? { backgroundColor: '#FFFFFF', borderColor: '#03a9f4', borderWidth: 1 } : {}]} onPress={() => this.setState({ Rate: 40 })}>
                                <Text style={[styles.txtRate, Rate == 40 ? { color: '#03a9f4' } : {}]}>40%</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.boxRate}>
                            <TouchableOpacity style={[styles.btnRate, Rate == 50 ? { backgroundColor: '#FFFFFF', borderColor: '#03a9f4', borderWidth: 1 } : {}]} onPress={() => this.setState({ Rate: 50 })}>
                                <Text style={[styles.txtRate, Rate == 50 ? { color: '#03a9f4' } : {}]}>50%</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btnRate, Rate == 60 ? { backgroundColor: '#FFFFFF', borderColor: '#03a9f4', borderWidth: 1 } : {}]} onPress={() => this.setState({ Rate: 60 })}>
                                <Text style={[styles.txtRate, Rate == 60 ? { color: '#03a9f4' } : {}]}>60%</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.boxRate}>
                            <TouchableOpacity style={[styles.btnRate, Rate == 70 ? { backgroundColor: '#FFFFFF', borderColor: '#03a9f4', borderWidth: 1 } : {}]} onPress={() => this.setState({ Rate: 70 })}>
                                <Text style={[styles.txtRate, Rate == 70 ? { color: '#03a9f4' } : {}]}>70%</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btnRate, Rate == 80 ? { backgroundColor: '#FFFFFF', borderColor: '#03a9f4', borderWidth: 1 } : {}]} onPress={() => this.setState({ Rate: 80 })}>
                                <Text style={[styles.txtRate, Rate == 80 ? { color: '#03a9f4' } : {}]}>80%</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.boxRate}>
                            <TouchableOpacity style={[styles.btnRate, Rate == 90 ? { backgroundColor: '#FFFFFF', borderColor: '#03a9f4', borderWidth: 1 } : {}]} onPress={() => this.setState({ Rate: 90 })}>
                                <Text style={[styles.txtRate, Rate == 90 ? { color: '#03a9f4' } : {}]}>90%</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btnRate, Rate == 100 ? { backgroundColor: '#FFFFFF', borderColor: '#03a9f4', borderWidth: 1 } : {}]} onPress={() => this.setState({ Rate: 100 })}>
                                <Text style={[styles.txtRate, Rate == 100 ? { color: '#03a9f4' } : {}]}>100%</Text>
                            </TouchableOpacity>
                        </View>
                        {/* <View style={{borderRadius: 1, marginTop: 10, width:'60%',flexDirection: 'row',justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: '#333333', alignSelf: 'center', backgroundColor: 'white',paddingVertical: 10}}>
                            <Picker
                                mode="dropdown"
                                style={{ height:20, backgroundColor: 'white',width: '80%'}}
                                selectedValue={Rate}
                                onValueChange={(value) => this.setState({Rate: value})}>
                                <Picker.Item label="Select a rate" value="0" />
                                {
                                    options
                                }
                            </Picker>
                            <Image source={require('../../assets/down_arrow_active.png')} style={{ width: 13, height: 11, marginRight: 5}} />
                        </View> */}
                        <PickerItem
                            boxDisplayStyles={{ borderRadius: 1, marginTop: 10, width: '40%', borderWidth: 1, borderColor: '#333333', alignSelf: 'center', backgroundColor: 'white', paddingVertical: 10 }}
                            containerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                            textDisplayStyle={{ marginRight: 15, }}
                            options={options}
                            selectedOption={Rate}
                            isChange={false}
                            onSubmit={(value) => this.setState({ Rate: value })}
                        />
                        <TouchableOpacity onPress={() => this.props.onRefund(Rate)} style={[styles.btnRefund, Rate == 0 ? { backgroundColor: '#e8e8e8' } : {}]} disabled={Rate == 0}>
                            <Text style={[styles.txtRefund, Rate == 0 ? { color: '#bdbdbd' } : {}]}>{convertLanguage(language, 'refund')}</Text>
                        </TouchableOpacity>
                    </View>
                </SafeView>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    content: {
        padding: 15,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10
    },
    txtTitle: {
        textAlign: 'center',
        fontSize: 16,
        color: '#333333'
    },
    boxRate: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10
    },
    btnRate: {
        flex: 0.48,
        backgroundColor: '#e7e7e7',
        borderRadius: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 31
    },
    txtRate: {
        fontSize: 14,
        color: '#333333'
    },
    btnRefund: {
        height: 48,
        borderRadius: 2,
        backgroundColor: '#03a9f4',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    txtRefund: {
        fontSize: 14,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalRefundRate);