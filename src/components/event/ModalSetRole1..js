import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, Dimensions, Alert, Platform, TouchableOpacity } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import Touchable from '../../screens_view/Touchable';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper'
// import Loading from '../../screens_view/Loading';
import { actSetEventRole } from '../../actions/event';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalPaymentMethod extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Loading: false
        }
    }

    checkRole(role) {
        var authRole = this.props.EventRole;
        var memberRole = this.props.MemberRole;
        var rules = [];
        if (role == memberRole) {
            return false
        }
        // if(authRole == 'Leader'){
        //     return true
        // } 
        if (authRole == 'Leader') {
            return true;
        }
        if (role == 'Discharge') {
            if (authRole == 'Director') {
                rules = ['Director', 'Manager', 'Staff'];
            } else if (authRole == 'Manager') {
                rules = ['Manager', 'Staff'];
            }
            if (rules.indexOf(memberRole) != -1) {
                return true;
            }
            return false;
        }
        if (authRole == 'Director') {
            rules = ['Director', 'Manager', 'Staff'];
        } else if (authRole == 'Manager') {
            rules = ['Manager', 'Staff'];
        }
        if (rules.indexOf(memberRole) != -1 && rules.indexOf(role) != -1) {
            return true;
        }
        return false;
    }
    setEventRole = (newRole) => {
        var { language } = this.props.language;
        Alert.alert(
            // convertLanguage(language, 'set_role'),
            "",
            convertLanguage(language, 'are_you_sure_to_set_role'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'destructive',
                },
                {
                    text: convertLanguage(language, 'ok'), onPress: () => {
                        this.props.onSetEventRole(this.props.eventId, this.props.UserId, newRole, '', this.props.MemberRole)
                        this.props.toggleModal(false)
                    }
                },
            ],
            { cancelable: false },
        );

    }
    onLeaveEvent = () => {
        this.props.onLeaveEvent()

    }

    render() {
        // var { modalPaymentMethod } = this.props;
        var { language } = this.props.language;
        let roleLeader = this.checkRole('Leader');
        let roleDirector = this.checkRole('Director');
        let roleManager = this.checkRole('Manager');
        let roleStaff = this.checkRole('Staff');
        let roleDischarge = this.checkRole('Discharge');
        return (
            <React.Fragment>
                <Modal
                    isVisible={true}
                    backdropOpacity={0.3}
                    animationIn="zoomInDown"
                    animationOut="zoomOutUp"
                    animationInTiming={1}
                    animationOutTiming={1}
                    backdropTransitionInTiming={1}
                    backdropTransitionOutTiming={1}
                    style={{ marginLeft: 55, marginRight: 55 }}
                    deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                >

                    <View style={styles.modalContent}>
                        <View style={styles.rowClose}>
                            <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.toggleModal() }} >
                                <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.boxContentSelect}>
                            <Touchable disabled={!roleLeader} onPress={() => { this.setEventRole('leader') }} style={[styles.btnMethod, roleLeader ? {} : { backgroundColor: '#cccccc', borderColor: '#cccccc' }]}>
                                <Text style={[styles.txtMethod, roleLeader ? {} : { color: '#FFFFFF' }]}>{convertLanguage(language, 'set_as_a_leader')}</Text>
                            </Touchable>
                            <Touchable disabled={!roleDirector} onPress={() => { this.setEventRole('director') }} style={[styles.btnMethod, roleDirector ? {} : { backgroundColor: '#cccccc', borderColor: '#cccccc' }]}>
                                <Text style={[styles.txtMethod, roleDirector ? {} : { color: '#FFFFFF' }]}>{convertLanguage(language, 'set_as_a_director')}</Text>
                            </Touchable>
                            <Touchable disabled={!roleManager} onPress={() => { this.setEventRole('manager') }} style={[styles.btnMethod, roleManager ? {} : { backgroundColor: '#cccccc', borderColor: '#cccccc' }]}>
                                <Text style={[styles.txtMethod, roleManager ? {} : { color: '#FFFFFF' }]}>{convertLanguage(language, 'set_as_a_manager')}</Text>
                            </Touchable>
                            <Touchable disabled={!roleStaff} onPress={() => { this.setEventRole('staff') }} style={[styles.btnMethod, roleStaff ? {} : { backgroundColor: '#cccccc', borderColor: '#cccccc' }]}>
                                <Text style={[styles.txtMethod, roleStaff ? {} : { color: '#FFFFFF' }]}>{convertLanguage(language, 'set_as_a_staff')}</Text>
                            </Touchable>
                            <Touchable disabled={!roleDischarge} onPress={() => this.onLeaveEvent()} style={[styles.btnMethod, roleDischarge ? {} : { backgroundColor: '#cccccc', borderColor: '#cccccc' }]}>
                                <Text style={[styles.txtMethod, roleDischarge ? {} : { color: '#FFFFFF' }]}>{convertLanguage(language, 'discharge_event')}</Text>
                            </Touchable>
                        </View>
                    </View>

                </Modal>

            </React.Fragment>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    boxContentSelect: {
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 20
    },
    txtTicketPrice: {
        fontSize: 14,
        color: '#333333'
    },
    btnMethod: {
        width: 211,
        height: 49,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
    },
    txtMethod: {
        fontSize: 15,
        color: '#333333'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {

        onSetEventRole: (eventId, memberId, role, navigation, oldRole) => {
            dispatch(actSetEventRole(eventId, memberId, role, navigation, oldRole))
        },


    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ModalPaymentMethod);