import React, { Component } from 'react';
import { BackHandler, Text, TouchableOpacity, View, Image, StyleSheet, ActivityIndicator, Platform, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window')
import Touchable from '../../screens_view/Touchable'
import ScrView from '../../screens_view/ScrView'
import * as Colors from '../../constants/Colors'

import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { convertLanguage } from '../../services/Helper'
import Modal from "react-native-modal";
import { actHideModalLogin } from '../../actions/global';
import { actLogin } from '../../actions/user';
import { connect } from "react-redux";
import ExtraDimensions from 'react-native-extra-dimensions-android';
import AppleLogin from '../react-native-apple-login/Apple'
import appleAuth, {
    AppleAuthError,
    AppleAuthRequestScope,
    AppleAuthRealUserStatus,
    AppleAuthCredentialState,
    AppleAuthRequestOperation,
} from '@invertase/react-native-apple-authentication';


class ModalLogin extends Component {
    constructor(props) {
        super(props);
        this.getHideModalLogin = this.getHideModalLogin.bind(this);
        this.facebook = this.facebook.bind(this);
        this.google = this.google.bind(this);
        this.loginEmail = this.loginEmail.bind(this);

    }

    getHideModalLogin = () => {
        this.props.getHideModalLogin()
    }

    facebook = () => {
        var { navigation } = this.props;
        // Attempt a login using the Facebook login dialog asking for default permissions.
        LoginManager.logInWithPermissions(["public_profile", "email"]).then(
            (result) => {
                if (result.isCancelled) {
                    //alert("Login cancelled");
                } else {
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            const accessToken = data.accessToken.toString()
                            if (accessToken) {
                                this.props.onSubmit('Fbtoken', accessToken, false, navigation, 'modal')
                            } else alert('Error')
                        }
                    )
                }
            },
            (error) => {
                alert("Login fail with error: " + error);
            }
        );
    }

    componentDidMount() {
        this._setupGoogleSignin();
        BackHandler.addEventListener('hardwareBackPress', function () {
            console.log('a');
            return false
        });
    }

    async _setupGoogleSignin() {
        try {
            await GoogleSignin.configure({
                webClientId: '1058119835706-onr4c64vqgdt5iiqp4upsa2b2psqvrg7.apps.googleusercontent.com',
                offlineAccess: false,
                iosClientId: '1058119835706-k22o7reor4deq5tbjrpdvjorfdaksogu.apps.googleusercontent.com'
            });
        }
        catch (err) {
            console.log("Google signin error", err.code, err.message);
        }
    }

    //accessToken
    google = async () => {
        var { navigation } = this.props;
        GoogleSignin.signIn().then(() => {
            GoogleSignin.getTokens().then((token) => {
                this.props.onSubmit('Ggtoken', token.accessToken, false, navigation, 'modal')
            }).catch((err) => {
            });
        })
            .catch((err) => {
                // console.log(err);
            })
    }
    loginEmail() {
        this.getHideModalLogin();
        this.props.navigation.navigate('LoginWithMail', { type: 'login_mail' });
    }

    setTokenApple = (token) => {
        var { navigation } = this.props;
        this.props.onSubmit('Aptoken', token.id_token, false, navigation, 'modal')
    };

    appleIos = async () => {
        var { navigation } = this.props;
        console.warn('Beginning Apple Authentication1');

        // start a login request
        try {
            const appleAuthRequestResponse = await appleAuth.performRequest({
                requestedOperation: AppleAuthRequestOperation.LOGIN,
                requestedScopes: [
                    AppleAuthRequestScope.EMAIL,
                    AppleAuthRequestScope.FULL_NAME,
                ],
            });
            const {
                user: newUser,
                email,
                nonce,
                identityToken,
                realUserStatus /* etc */,
            } = appleAuthRequestResponse;

            this.user = newUser;

            this.fetchAndUpdateCredentialState()
                .then(res => this.setState({ credentialStateForUser: res }))
                .catch(error =>
                    this.setState({ credentialStateForUser: `Error: ${error.code}` }),
                );

            if (identityToken) {
                // e.g. sign in with Firebase Auth using `nonce` & `identityToken`
                this.props.onSubmit('Aptoken', identityToken, false, navigation, 'modal')
            } else {
                // no token - failed sign-in?
            }

            if (realUserStatus === AppleAuthRealUserStatus.LIKELY_REAL) {
                // console.log("I'm a real person!");
            }

            //   console.warn(`Apple Authentication Completed, ${this.user}, ${email}`);
        } catch (error) {
            if (error.code === AppleAuthError.CANCELED) {
                console.warn('User canceled Apple Sign in.');
            } else {
                console.error(error);
            }
        }
    };

    fetchAndUpdateCredentialState = async () => {
        if (this.user === null) {
            this.setState({ credentialStateForUser: 'N/A' });
        } else {
            const credentialState = await appleAuth.getCredentialStateForUser(this.user);
            if (credentialState === AppleAuthCredentialState.AUTHORIZED) {
                this.setState({ credentialStateForUser: 'AUTHORIZED' });
            } else {
                this.setState({ credentialStateForUser: credentialState });
            }
        }
    }

    render() {
        var { loading } = this.props.user;
        var { language } = this.props.language;
        return (
            <Modal
                isVisible={this.props.global.showModalLogin}
                backdropOpacity={0.3}
                // animationIn="zoomInDown"
                // animationOut="zoomOutUp"
                animationInTiming={300}
                animationOutTiming={300}
                backdropTransitionInTiming={1000}
                backdropTransitionOutTiming={1000}
                style={styles.modal}
                onRequestClose={this.getHideModalLogin}
                onBackdropPress={() => this.getHideModalLogin()}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            >
                <View style={styles.modalContent}>
                    <ScrView style={{ flex: 1 }}>
                        <Text style={{
                            alignSelf: 'center', marginTop: 20,
                            fontSize: 24, fontWeight: 'bold', color: Colors.TEXT_S
                        }}>{convertLanguage(language, 'we_like_party')}</Text>

                        <Image source={require('../../assets/logo_blue.png')} style={{ alignSelf: 'center', marginTop: 10 }} />

                        <Touchable
                            onPress={this.facebook}
                            style={{
                                alignSelf: 'stretch', height: 40, backgroundColor: '#3C5A96',
                                marginTop: 20, marginLeft: 48, marginRight: 48, justifyContent: 'center', borderRadius: 4,
                            }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image style={{ marginLeft: 16, marginRight: 8, width: 24, height: 24, }} source={require('../../assets/facebook_logo.png')} />
                                <Text style={{ color: 'white', fontSize: 16, flex: 1 }}>{convertLanguage(language, 'connect_facebook')}</Text>
                            </View>
                        </Touchable>

                        <Touchable
                            onPress={this.google}
                            style={{
                                alignSelf: 'stretch', height: 40, backgroundColor: '#EA4335',
                                marginTop: 12, marginLeft: 48, marginRight: 48, justifyContent: 'center', borderRadius: 4,
                            }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image style={{ marginLeft: 16, marginRight: 8, width: 24, height: 24, }} source={require('../../assets/G_icon.png')} />
                                <Text style={{ color: 'white', fontSize: 16, flex: 1 }}>{convertLanguage(language, 'connect_google')}</Text>
                            </View>
                        </Touchable>

                        <Touchable
                            onPress={() => appleAuth.isSupported ? this.appleIos() : this.instagramLogin.show()}
                            style={{
                                alignSelf: 'stretch', height: 40, backgroundColor: '#333333',
                                marginTop: 12, marginLeft: 48, marginRight: 48, justifyContent: 'center', borderRadius: 4,
                            }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image style={{ width: 24, height: 24, marginLeft: 16, marginRight: 8 }} source={require('../../assets/ic_apple.png')} />
                                <Text style={{ color: 'white', fontSize: 16, flex: 1 }}>{convertLanguage(language, 'connect_apple')}</Text>
                            </View>
                        </Touchable>
                        <AppleLogin
                            ref={ref => this.instagramLogin = ref}
                            clientId='831883500624176'
                            appId='com.starindex.login-apple'
                            appSecret='eyJraWQiOiI3NVdRNFUyRzM0IiwiYWxnIjoiRVMyNTYifQ.eyJpc3MiOiIzSzlHWTkySzNKIiwiaWF0IjoxNTkwMDM2MjU5LCJleHAiOjE2MDU1ODgyNTksImF1ZCI6Imh0dHBzOi8vYXBwbGVpZC5hcHBsZS5jb20iLCJzdWIiOiJjb20uc3RhcmluZGV4LmxvZ2luLWFwcGxlIn0.Tbfbpla8740SCKU2mJ-JfLfON1x4rPrcpBwEyLOriJlaM3qa0T0IfW2kNDGOaIHenoGJnH4KI33KntSaZPqO-A'
                            redirectUrl='https://www.comeup.asia/mobile-login-apple'
                            scopes={['name', 'email']}
                            modalVisible={true}
                            onLoginSuccess={this.setTokenApple}
                            onLoginFailure={(data) => console.log(data)}
                        />

                        <Touchable
                            onPress={this.loginEmail}
                            style={{
                                alignSelf: 'stretch', height: 40, backgroundColor: '#00A9F4',
                                marginTop: 12, marginLeft: 48, marginRight: 48, justifyContent: 'center', borderRadius: 4,
                            }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 16 }}>
                                <Image style={{ width: 24, height: 24, marginRight: 8 }} source={require('../../assets/ic_mail.png')} />
                                <Text style={{ color: 'white', fontSize: 16, flex: 1 }} numberOfLines={1}>{convertLanguage(language, 'login')}/{convertLanguage(language, 'sign_up')}</Text>
                            </View>
                        </Touchable>

                        <Touchable
                            onPress={this.getHideModalLogin}
                            style={{
                                alignSelf: 'stretch', height: 40,
                                marginTop: 24, marginLeft: 48, marginRight: 48, justifyContent: 'center', alignItems: 'center',
                                borderRadius: 4, borderWidth: 1, borderColor: Colors.PRIMARY,
                            }}>
                            <Text style={{ color: Colors.PRIMARY, fontSize: 16 }}>{convertLanguage(language, 'later')}</Text>
                        </Touchable>

                        <View style={{ height: 20 }}></View>
                    </ScrView>
                    {
                        loading &&
                        <View style={{ position: 'absolute', left: 0, top: 0, right: 0, bottom: 0, backgroundColor: 'rgba(0, 0, 0, 0.7)', alignItems: 'center', justifyContent: 'center' }} >
                            <ActivityIndicator size="large" color="#FFFFFF" />
                        </View>
                    }
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modal: {
        padding: 0,
        justifyContent: "flex-end",
        margin: 0
    },
    btnAction: {
        padding: 12,
        margin: 4,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "#000000",
        borderWidth: 1,
    },
    modalContent: {
        backgroundColor: "white",
        flex: 0.6,

    },
    content: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        padding: 12,
    }
});

const mapStateToProps = state => {
    return {
        global: state.global,
        user: state.user,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getHideModalLogin: () => {
            dispatch(actHideModalLogin())
        },
        onSubmit: (auKey, auValue, goToWelcome = false, navigation, type, register) => {
            dispatch(actLogin(auKey, auValue, goToWelcome, navigation, type, register))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ModalLogin);
