import React, { Component } from 'react';
import { ActivityIndicator, Linking, Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
class ModalRegisterTeam extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: '',
            widthView: 0,
            width: 0,
            height: 0
        }
    }


    onYesMethod = () => {
        let { data } = this.props
        if (data?.LinkApp !== "" && data?.LinkApp !== "#") {
            return Linking.openURL(data.LinkApp)
        } else {
            return null
        }
    }

    componentDidUpdate(props, state) {
        if (state.widthView !== this.state.widthView) {
            if (this.props.data?.ImageLink !== '') {
                Image.getSize(this.props.data?.ImageLink, (width, height) => {
                    this.setState({
                        width: this.state.widthView - 20,
                        height: height * ((this.state.widthView - 20) / (width))
                    });
                });
            }
        }
    }

    render() {
        var { language } = this.props.language;
        let { data } = this.props
        return (
            <Modal
                isVisible={this.props.visible}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            // onBackdropPress={() => this.props.closeModal()}
            >
                <View style={styles.modalContent}>

                    <View style={styles.content}
                        onLayout={(event) => {
                            var { x, y, width, height } = event.nativeEvent.layout;
                            this.setState({ widthView: width })
                        }}
                    >
                        {data?.ImageLink !== "" &&

                            <>
                                {this.state.width === 0 && this.state.height === 0 ?
                                    <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
                                    :
                                    <TouchableOpacity onPress={() => { this.props.onClose(), this.onYesMethod() }}>
                                        <Image source={{ uri: data?.ImageLink }} style={{ width: this.state.width, height: this.state.height, marginBottom: 16, alignSelf: 'center' }} resizeMode="contain" />
                                    </TouchableOpacity>
                                }

                            </>
                        }

                        {data?.Description && data?.Description !== '' && <Text style={styles.txtTitle}>
                            {data?.Description}
                        </Text>}


                        {!data?.HasYesNo ?
                            <TouchableOpacity style={styles.btnTeam} onPress={this.props.onClose}>
                                <Text style={styles.txtTeam}>{convertLanguage(language, 'ok')}</Text>
                            </TouchableOpacity>
                            :
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
                                <TouchableOpacity style={[styles.btnTeam, { width: 140, height: 36, backgroundColor: '#ffffff', borderColor: '#4F4F4F', borderWidth: 1 }]}
                                    onPress={this.props.onClose}>
                                    <Text style={[styles.txtTeam, { color: '#4F4F4F' }]}>{convertLanguage(language, 'no')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.btnTeam, { width: 140, height: 36 }]}
                                    onPress={() => { this.props.onClose(), this.onYesMethod() }}>
                                    <Text style={styles.txtTeam}>{convertLanguage(language, 'yes')}</Text>
                                </TouchableOpacity>
                            </View>
                        }
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5,
    },
    content: {
        padding: 16,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center',
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30
    },
    boxListCategory: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 10
    },
    btnCategory: {
        width: (width - 110) / 3,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginBottom: 15,
        borderColor: '#00acfb'
    },
    txtCountry: {
        fontSize: 17,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingLeft: 20,
        paddingRight: 20
    },
    txtTitle: {
        marginTop: 20,
        fontSize: 16,
        color: '#333333',
        marginBottom: 20,
        textAlign: 'center'
    },
    btnTeam: {
        width: '100%',
        backgroundColor: '#00A9F4',
        borderRadius: 4,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        // marginBottom: 10,
        height: 36
    },
    txtTeam: {
        fontSize: 16,
        color: '#FFFFFF',
    },
    arrow_right: {
        width: 18,
        height: 18,
        marginLeft: 10
    },
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalRegisterTeam);