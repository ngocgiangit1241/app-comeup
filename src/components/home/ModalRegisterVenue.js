import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, Dimensions, Platform, KeyboardAvoidingView, Animated, Keyboard, ScrollView } from 'react-native';
const { width, height } = Dimensions.get('window')
import firebase from 'react-native-firebase';
import Modal from "react-native-modal";
import axios from 'axios';
import Touchable from '../../screens_view/Touchable';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { TextField } from '../../components/react-native-material-textfield';
import Loading from '../../screens_view/Loading';
class ModalRegisterVenue extends Component {
    constructor(props) {
        super(props);
        this.state = {
            manager: '',
            mobile: '',
            email: '',
            venuename: '',
            address: '',
            homepageurl: '',
            keyboardShow: false,
            error: {},
            loading: false
        }
        this.keyboardHeight = new Animated.Value(0);
        this.viewHeight = new Animated.Value(height - 150);
    }

    onRegisterVenue = () => {
        Keyboard.dismiss();
        this.setState({ error: {}, loading: true })
        let headers = { 'Content-Type': 'multipart/form-data' }
        let formdata = new FormData();
        formdata.append("manager", this.state.manager)
        formdata.append("mobile", this.state.mobile)
        formdata.append("email", this.state.email)
        formdata.append("venuename", this.state.venuename)
        formdata.append("address", this.state.address)
        formdata.append("homepageurl", this.state.homepageurl)
        fetch('https://api.xcms.online/form/g2dNTZ0cdzbuoHpdYxnehuxIUPS935fLZx4gfJIE/5ef461d3b70d1c18c954ae02', {
            method: 'post',
            headers: headers,
            body: formdata
        }).then(async (response) => {
            this.setState({ loading: false })
            if (response.status == 200) {
                response = await response.json()
                if (response.status == 200) {
                    firebase.analytics().logEvent('create_venue', { venuename: this.state.venuename });
                    this.props.showModalRegisterSuccess()
                } else if (response.status == -1) {
                    this.setState({ error: response.error })
                }
            } else {
                alert('Cannot connect to server.')
            }
        }).catch(err => {
            this.setState({ loading: false })
            alert('Cannot connect to server.')
        })
    }

    componentDidMount() {
        this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        if (Platform.OS === 'ios') {
            Animated.parallel([
                Animated.timing(this.keyboardHeight, {
                    duration: event.duration,
                    toValue: event.endCoordinates.height,
                }),
            ]).start();
        }
        Animated.parallel([
            Animated.timing(this.viewHeight, {
                duration: event.duration,
                toValue: height - 50 - event.endCoordinates.height,
            }),
        ]).start();
        this.setState({ keyboardShow: true })
    };

    keyboardWillHide = (event) => {
        if (Platform.OS === 'ios') {
            Animated.parallel([
                Animated.timing(this.keyboardHeight, {
                    duration: event.duration,
                    toValue: 0,
                }),
            ]).start();
        }
        Animated.parallel([
            Animated.timing(this.viewHeight, {
                duration: event.duration,
                toValue: height - 150,
            }),
        ]).start();
        this.setState({ keyboardShow: false })
    };

    render() {
        var { language } = this.props.language;
        var { manager, mobile, email, venuename, address, homepageurl, error } = this.state;
        return (
            <Modal
                isVisible={true}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                style={[this.state.keyboardShow ? { justifyContent: 'flex-end', marginBottom: 0 } : { marginBottom: 0 }]}
            >
                <Animated.View style={[styles.modalContent, { marginBottom: this.keyboardHeight, height: this.viewHeight }]}>
                    {
                        this.state.loading &&
                        <Loading />
                    }
                    <Touchable style={styles.btnClose} onPress={() => this.props.closeModal()} >
                        <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                    </Touchable>
                    <Text style={styles.txtTitle}>{'Apply to register your venue in ComeUp\nComeUp team will contact you soon'}</Text>
                    <ScrollView contentContainerStyle={styles.content} style={{ marginHorizontal: 12 }} bounces={false} keyboardShouldPersistTaps='handled' ref={(ref) => this.scrollView = ref}>
                        <Text style={styles.txtLable}>{'Contact Info'}</Text>
                        <TextField
                            autoCorrect={false}
                            enablesReturnKeyAutomatically={true}
                            value={manager}
                            onChangeText={(manager) => { this.setState({ manager }) }}
                            error={error.manager && error.manager[0]}
                            returnKeyType='next'
                            label={convertLanguage(language, 'manager') + '*'}
                            baseColor={manager.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                            tintColor={'#828282'}
                            errorColor={'#EB5757'}
                            inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD' }]}
                            containerStyle={styles.containerStyle}
                            labelHeight={25}
                            labelTextStyle={{ paddingBottom: 15, paddingLeft: 12 }}
                            lineWidth={2}
                            selectionColor={'#3a3a3a'}
                            style={styles.input}
                            errorImage={require('../../assets/error.png')}
                        />
                        <TextField
                            autoCorrect={false}
                            enablesReturnKeyAutomatically={true}
                            value={mobile}
                            onChangeText={(mobile) => { this.setState({ mobile }) }}
                            error={error.mobile && error.mobile[0]}
                            returnKeyType='next'
                            label={convertLanguage(language, 'mobile') + '*'}
                            baseColor={mobile.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                            tintColor={'#828282'}
                            errorColor={'#EB5757'}
                            inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD' }]}
                            containerStyle={styles.containerStyle}
                            labelHeight={25}
                            labelTextStyle={{ paddingBottom: 15 }}
                            lineWidth={2}
                            selectionColor={'#3a3a3a'}
                            style={styles.input}
                            labelTextStyle={{ paddingLeft: 12 }}
                            errorImage={require('../../assets/error.png')}
                        />
                        <TextField
                            autoCorrect={false}
                            enablesReturnKeyAutomatically={true}
                            value={email}
                            onChangeText={(email) => { this.setState({ email }) }}
                            error={error.email && error.email[0]}
                            returnKeyType='next'
                            label={convertLanguage(language, 'email') + '*'}
                            baseColor={email.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                            tintColor={'#828282'}
                            errorColor={'#EB5757'}
                            inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD' }]}
                            containerStyle={styles.containerStyle}
                            labelHeight={25}
                            labelTextStyle={{ paddingBottom: 15 }}
                            lineWidth={2}
                            selectionColor={'#3a3a3a'}
                            style={styles.input}
                            labelTextStyle={{ paddingLeft: 12 }}
                            errorImage={require('../../assets/error.png')}
                            keyboardType="email-address"
                        />
                        <Text style={styles.txtLable}>{'Venue Info'}</Text>
                        <TextField
                            autoCorrect={false}
                            enablesReturnKeyAutomatically={true}
                            value={'' + venuename}
                            onChangeText={(venuename) => { this.setState({ venuename }) }}
                            error={error.venuename && error.venuename[0]}
                            returnKeyType='next'
                            label={convertLanguage(language, 'venuename') + '*'}
                            baseColor={venuename.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                            tintColor={'#828282'}
                            errorColor={'#EB5757'}
                            inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD' }]}
                            containerStyle={styles.containerStyle}
                            labelHeight={25}
                            labelTextStyle={{ paddingBottom: 15 }}
                            lineWidth={2}
                            selectionColor={'#3a3a3a'}
                            style={styles.input}
                            labelTextStyle={{ paddingLeft: 12 }}
                            errorImage={require('../../assets/error.png')}
                            onFocus={() => this.scrollView.scrollToEnd()}
                        />
                        <TextField
                            autoCorrect={false}
                            enablesReturnKeyAutomatically={true}
                            value={'' + address}
                            onChangeText={(address) => { this.setState({ address }) }}
                            error={error.address && error.address[0]}
                            returnKeyType='next'
                            label={convertLanguage(language, 'address') + '*'}
                            baseColor={address.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                            tintColor={'#828282'}
                            errorColor={'#EB5757'}
                            inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD' }]}
                            containerStyle={styles.containerStyle}
                            labelHeight={25}
                            labelTextStyle={{ paddingBottom: 15 }}
                            lineWidth={2}
                            selectionColor={'#3a3a3a'}
                            style={styles.input}
                            labelTextStyle={{ paddingLeft: 12 }}
                            errorImage={require('../../assets/error.png')}
                            onFocus={() => this.scrollView.scrollToEnd()}
                        />
                        <TextField
                            autoCorrect={false}
                            enablesReturnKeyAutomatically={true}
                            value={'' + homepageurl}
                            onChangeText={(homepageurl) => { this.setState({ homepageurl }) }}
                            // error={errors.Email}
                            returnKeyType='next'
                            label={convertLanguage(language, 'homepageurl')}
                            baseColor={homepageurl.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                            tintColor={'#828282'}
                            errorColor={'#EB5757'}
                            inputContainerStyle={[styles.inputContainerStyle, { borderColor: '#BDBDBD' }]}
                            containerStyle={styles.containerStyle}
                            labelHeight={25}
                            labelTextStyle={{ paddingBottom: 15 }}
                            lineWidth={2}
                            selectionColor={'#3a3a3a'}
                            style={styles.input}
                            labelTextStyle={{ paddingLeft: 12 }}
                            errorImage={require('../../assets/error.png')}
                            onFocus={() => this.scrollView.scrollToEnd()}
                        />
                        <Touchable disabled={manager === '' || mobile === '' || email === '' || venuename === '' || address === ''} style={[styles.btnSubmit, { backgroundColor: manager === '' || mobile === '' || email === '' || venuename === '' || address === '' ? '#E0E2E8' : '#00A9F4' }]} onPress={() => this.onRegisterVenue()}>
                            <Text style={[styles.txtSubmit, { color: manager === '' || mobile === '' || email === '' || venuename === '' || address === '' ? '#B3B8BC' : '#FFFFFF' }]}>{convertLanguage(language, 'submit')}</Text>
                        </Touchable>
                    </ScrollView>
                </Animated.View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 10,
        // minHeight: 500,
        height: height - 150,
    },
    container: {

    },
    content: {
        paddingHorizontal: 20,
        paddingBottom: 20,
        // marginHorizontal: 20
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        alignSelf: 'flex-end',
        padding: 10
    },
    iconClose: {
        width: 30,
        height: 30
    },
    txtTitle: {
        fontSize: 16,
        color: '#333333',
        marginBottom: 20,
        textAlign: 'center'
    },
    inputContainerStyle: {
        borderWidth: 1,
        borderRadius: 4,
        paddingHorizontal: 12,
    },
    containerStyle: {
        // paddingHorizontal: 20,
        // flex: 1
        marginVertical: 10,
    },
    input: {
        color: '#333333',
    },
    btnSubmit: {
        borderRadius: 4,
        height: 42,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtSubmit: {
        fontSize: 16
    },
    txtLable: {
        fontSize: 20,
        color: '#333333',
        fontWeight: '600'
    }
});

const mapStateToProps = state => {
    return {
        language: state.language,
        ticket: state.ticket
    };
}

export default connect(mapStateToProps, null)(ModalRegisterVenue);