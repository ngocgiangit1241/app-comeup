import React, { Component } from 'react';
import { Linking, Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
const scale = width / 360
class ModalVersion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: '',
            widthView: 0,
            width: 0,
            height: 0
        }
    }


    onYesMethod = () => {
        let { data } = this.props
        if (data?.Link !== "") {
            return Linking.openURL(data.Link)
        } else {
            return null
        }
    }



    render() {
        var { language } = this.props.language;
        // let { data } = this.props
        // console.log('versionApp', this.props.versionApp.split('.')[0].replace(/[^0-9]/g, ''));
        // console.log('versionNew', this.props.versionNew);

        return (
            <Modal
                isVisible={this.props.visible}
                backdropOpacity={0.3}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1}
                animationOutTiming={1}
                backdropTransitionInTiming={1}
                backdropTransitionOutTiming={1}
                onRequestClose={this.props.onLater}
                deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            // onBackdropPress={() => this.props.closeModal()}
            >
                <View style={styles.modalContent}>
                    {/* <View style={styles.rowClose}>
                        <TouchableOpacity style={styles.btnClose} onPress={() => { this.props.closeModal() }} >
                            <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                        </TouchableOpacity>
                    </View> */}
                    <View style={styles.content} >

                        <Text style={styles.txtTitle}>
                            {
                                this.props.versionApp.split('.')[0].replace(/[^0-9]/g, '') !== this.props.versionNew.split('.')[0].replace(/[^0-9]/g, '')
                                    ||
                                    this.props.versionApp.split('.')[1].replace(/[^0-9]/g, '') !== this.props.versionNew.split('.')[1].replace(/[^0-9]/g, '')
                                    ? convertLanguage(language, 'description_verion2') : convertLanguage(language, 'description_verion1')
                            }
                        </Text>

                        {
                            this.props.versionApp.split('.')[0].replace(/[^0-9]/g, '') !== this.props.versionNew.split('.')[0].replace(/[^0-9]/g, '')
                                ||
                                this.props.versionApp.split('.')[1].replace(/[^0-9]/g, '') !== this.props.versionNew.split('.')[1].replace(/[^0-9]/g, '')

                                ?
                                <TouchableOpacity style={[styles.btnTeam]}
                                    onPress={() => { Linking.openURL(Platform.OS === 'android' ? 'https://play.google.com/store/apps/details?id=com.creditcomeup.app' : 'https://apps.apple.com/us/app/doorhub-driver/id1580649368') }}
                                >
                                    <Text style={styles.txtTeam}>{convertLanguage(language, 'update')}</Text>
                                </TouchableOpacity>
                                :
                                <View style={{ flexDirection: 'row', justifyContent: 'center', width: '100%' }}>
                                    <TouchableOpacity style={[styles.btnTeam, { width: 140 * scale, height: 36 * scale, backgroundColor: '#ffffff', borderColor: '#4F4F4F', borderWidth: 1, marginRight: 12 * scale }]}
                                        onPress={this.props.onLater}>
                                        <Text style={[styles.txtTeam, { color: '#4F4F4F' }]}>{convertLanguage(language, 'later_version')}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.btnTeam, { width: 140 * scale, height: 36 * scale, marginLeft: 12 * scale }]}
                                        onPress={() => { Linking.openURL(Platform.OS === 'android' ? 'https://play.google.com/store/apps/details?id=com.creditcomeup.app' : 'https://apps.apple.com/us/app/doorhub-driver/id1580649368') }}
                                    >
                                        <Text style={styles.txtTeam}>{convertLanguage(language, 'update_version')}</Text>
                                    </TouchableOpacity>
                                </View>
                        }
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        borderRadius: 8,
    },
    content: {
        padding: 16,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 20,
        alignItems: 'center',
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30
    },
    boxListCategory: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 10
    },
    btnCategory: {
        width: (width - 110) / 3,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginBottom: 15,
        borderColor: '#00acfb'
    },
    txtCountry: {
        fontSize: 17,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingLeft: 20,
        paddingRight: 20
    },
    txtTitle: {
        marginTop: 20,
        fontSize: 16,
        color: '#333333',
        marginBottom: 20,
        textAlign: 'center',
    },
    btnTeam: {
        width: '100%',
        backgroundColor: '#00A9F4',
        borderRadius: 4,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        // marginBottom: 10,
        height: 36
    },
    txtTeam: {
        fontSize: 16,
        color: '#FFFFFF',
    },
    arrow_right: {
        width: 18,
        height: 18,
        marginLeft: 10
    },
});
const mapStateToProps = state => {
    return {
        language: state.language,
    };
}

export default connect(mapStateToProps, null)(ModalVersion);