import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    paddingVertical: 4,
    alignItems: 'flex-start',
    height: 80
  },

  text: {
    backgroundColor: 'transparent',
  },
});
