import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Picker } from 'react-native';
import { formatNumber, convertLanguage } from '../../services/Helper';
import PickerItem from '../../screens_view/PickerItem';
import moment from "moment";
import { connect } from "react-redux";
var options = [];
for (var i = 0; i <= 10; i++) {
    options.push(<Picker.Item key={'sx' + i} label={'' + i} value={i} style={{ fontWeight: 'bold' }} />)
}

class ItemTicket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ticket: {},
            isShowDetail: true,
            quantity: 0,
            datas: this.showDataQuantityIos()
        };
    }

    showDataQuantity(quantity) {
        var quantity = quantity ? quantity : 0;
        if (quantity > 10) {
            quantity = 10;
        }
        var result = [];
        for (var i = 0; i <= quantity; i++) {
            result.push(<Picker.Item key={'sx' + i} label={'' + i} value={i} />)
        }
        return result;
    }

    showDataQuantityIos() {
        var { ticket } = this.props;
        var quantity = ticket.LimitQuantity < ticket.Quantity ? ticket.LimitQuantity : ticket.Quantity
        quantity = quantity ? quantity : 0;
        if (quantity > 10) {
            quantity = 10;
        }
        var result = [];
        for (var i = 0; i <= quantity; i++) {
            result.push({ name: '' + i, value: i })
        }
        return result;
    }

    onSelect(Id, value) {
        this.setState({
            quantity: value
        })
        this.props.onSelect(Id, value)
    }

    convertDate = (datetime1) => {
        var { language } = this.props.language;
        if (datetime1) {
            var days = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
            var monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            var date_arr1 = datetime1.split(" ");
            var date1 = date_arr1[0];
            var time1 = date_arr1[1];
            var year1 = date1.split("-")[0];
            var month1 = date1.split("-")[1];
            var day1 = date1.split("-")[2];
            var monthAndroid1 = ''
            if (month1 < 10) {
                month1 = month1.substring(1,);
            }
            switch (language) {
                case 'vi':
                    monthAndroid1 = 'Th' + month1
                    break;
                case 'en':
                    monthAndroid1 = monthNames[month1]
                    break;
                case 'ko':
                    monthAndroid1 = month1.length === 1 ? "0" + month1 : month1
                    break;
                default:
                    break;
            }
            const result = { time: time1, date: day1, month: monthAndroid1 }
            return result
            // return year1 + '.' + monthNames[monthCheck()] + '.' + day1 + ' ' + days[new Date(year1 + '-' + month1 + '-' + day1).getDay()] + ' ' + time1
        } else {
            return '';
        }
    }
    render() {
        var { isShowDetail, quantity, datas } = this.state;
        var { ticket } = this.props;
        var { language } = this.props.language;
        var datetimenow = moment().format('YYYY-MM-DD HH:mm')
        let convertTime = this.convertDate(ticket.TimeEnd)
        return (
            <View style={styles.boxItem}>
                <Text style={styles.txtTicketName}>{ticket.Name}</Text>
                {/* <Text style={styles.txtShow}>{convertLanguage('ko', 'purchasable_by')} {ticket.TimeEnd}</Text> */}
                <Text style={styles.txtShow}>{convertLanguage(language, 'purchasable_by2', { time: convertTime.time, date: convertTime.date, day: convertTime.month })}</Text>
                {
                    ticket.Description !== '' &&
                    <React.Fragment>
                        {
                            isShowDetail &&
                            <Text style={styles.txtTicketDes}>{ticket.Description}</Text>
                        }
                        <Text style={isShowDetail ? styles.txtDetail : styles.txtDetailActive} onPress={() => this.setState({ isShowDetail: !isShowDetail })}>{isShowDetail ? "▲" : "▼"} {convertLanguage(language, 'detail_info')}</Text>
                    </React.Fragment>
                }
                <View style={styles.boxInfo}>
                    <View style={styles.boxPrice}>
                        <View style={styles.boxTicketType}>
                            {
                                ticket.Type === 'd-p-ticket'
                                    ?
                                    <Image source={require('../../assets/ic_delivery_brown.png')} style={styles.ic_delivery} />
                                    :
                                    <Image source={require('../../assets/ic_eticket.png')} style={styles.ic_eticket} />
                            }
                            <Text style={styles.txtDelivery}>{ticket.Type === 'd-p-ticket' ? convertLanguage(language, 'delivery_ticket2') : convertLanguage(language, 'e_ticket2')}</Text>
                        </View>
                        <Text style={styles.txtTicketPrice}>{ticket.Price == 0 ? convertLanguage(language, 'free_ticket') : ticket.Unit.toUpperCase() + ' ' + formatNumber(ticket.Price)}</Text>
                    </View>
                    {
                        moment(ticket.TimeEnd).isSameOrBefore(datetimenow) ?
                            <Text style={styles.txtSoldOut}>{convertLanguage(language, 'stop_selling')}</Text>
                            :
                            ticket.Quantity > 0 ?
                                <React.Fragment>
                                    <PickerItem
                                        boxDisplayStyles={styles.boxPicker}
                                        containerStyle={{ paddingLeft: 5, paddingRight: 5 }}
                                        options={datas}
                                        selectedOption={quantity}
                                        isChange={false}
                                        onSubmit={(value) => this.onSelect(ticket.Id, value)}
                                    />
                                    {/* <View style={[styles.boxPicker, { justifyContent: 'flex-end' }]}>
                                    <Picker
                                        mode="dropdown"
                                        style={{ height: 28, backgroundColor: 'white', width: '100%', position: 'absolute', left: 0, top: 0 }}
                                        selectedValue={quantity}
                                        onValueChange={(value) => this.onSelect(ticket.Id, value)}
                                    >

                                        {
                                            this.showDataQuantity(ticket.LimitQuantity < ticket.Quantity ? ticket.LimitQuantity : ticket.Quantity)
                                        }
                                    </Picker>
                                    <Image source={require('../../assets/down_arrow_active.png')} style={styles.ic_dropdown} />
                                </View> */}

                                </React.Fragment>
                                :
                                <Text style={styles.txtSoldOut}>{convertLanguage(language, 'sold_out')}</Text>
                    }

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    boxItem: {
        paddingTop: 25,
        paddingBottom: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd'
    },
    txtTicketName: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        marginBottom: 5
    },
    txtTicketDes: {
        fontSize: 13,
        color: '#757575',
        paddingBottom: 10,
    },
    txtShow: {
        fontSize: 13,
        color: '#333333',
        fontWeight: '400',
        paddingBottom: 15
    },
    txtDetailActive: {
        fontSize: 13,
        color: '#03a9f4',
        marginBottom: 5
    },
    boxPicker: {
        borderWidth: 1,
        borderColor: '#bdbdbd',
        borderRadius: 1,
        width: 72,
        height: 30,
        borderRadius: 5,
        paddingLeft: 5
    },
    txtPicker: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold'
    },
    boxOption: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ic_dropdown: {
        width: 13,
        height: 11,
        marginRight: 5
    },
    txtDetail: {
        fontSize: 13,
        color: '#bdbdbd',
        marginBottom: 5
    },
    boxInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 15
    },
    txtTicketPrice: {
        color: '#333333',
        fontSize: 16,
        fontWeight: '500',
        marginTop: 5
    },
    txtSoldOut: {
        fontSize: 16,
        color: '#bdbdbd',
        fontWeight: '500'
    },
    boxTicketType: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ic_delivery: {
        width: 16,
        height: 16,
        marginRight: 5
    },
    ic_eticket: {
        width: 10,
        height: 12,
        marginRight: 5
    },
    txtDelivery: {
        fontSize: 13,
        color: '#757575'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language
    };
}
export default connect(mapStateToProps, null)(ItemTicket);
