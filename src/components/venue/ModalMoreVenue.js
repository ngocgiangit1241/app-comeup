import React from 'react'
import { Text, View, ScrollView, StyleSheet, Dimensions, Platform, TouchableOpacity } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import moment from "moment";
import { convertLanguage } from '../../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';

export default function ModalMoreVenue(props) {


    return (
        <Modal
            isVisible={true}
            onBackdropPress={props.onClose}
            animationIn="slideInRight"
            animationOut="slideOutRight"
            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
            style={styles.rightModal}>
            <View style={styles.modalContent}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <TouchableOpacity style={styles.boxList} onPress={() => { props.onClose(), props.navigation.navigate('CreateVenue', { edit: true, loadAgain: props.loadAgain }) }}>
                        <Text style={styles.txtTicketBold}>{convertLanguage(props.language, 'edit')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.boxList} onPress={() => { props.onClose(), props.navigation.navigate('CancelVenue', { id: props.id, loadAgain: props.loadAgain }) }}>
                        <Text style={styles.txtTicketBold}>{convertLanguage(props.language, 'delete')}</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        height: height,
        width: width - 100,
        padding: 25,
        paddingTop: 40
    },
    txtCode: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 15
    },
    txtTitle: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15
    },
    boxList: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 2,
        borderBottomColor: '#e9e9e9',
        paddingTop: 20,
        paddingBottom: 20
    },
    txtTicket: {
        fontSize: 15,
        color: '#e8e8e8',
        fontWeight: 'bold',
        flex: 1
    },
    txtTicketBold: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
        flex: 1
    },
    circle: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: '#ff4081',
        margin: 10
    },
    rightModal: {
        alignItems: "flex-end",
        flex: 1,
        marginRight: 0
    }
});
