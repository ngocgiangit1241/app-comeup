import React, { useState, useRef, useEffect, memo } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  FlatList,
  PixelRatio,
  Linking,
} from 'react-native';
import Popover from 'react-native-popover-view';
import { convertLanguage } from '../../services/Helper';
import ImageView from 'react-native-image-view';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const scale = screenWidth / 360;
const VenueDetailItem = props => {
  const scaleFontSize = PixelRatio.getFontScale();
  const refPopover = useRef(null);
  const getHastag = hastag => {
    let dataTag = '';
    hastag.forEach(element => {
      dataTag += '#' + element.HashTagName + ' ';
    });
    return dataTag;
  };
  const [toogle, setToogle] = useState(false);
  const [modelImage, setModelImage] = useState(false);
  const [modelAvt, setModelAvt] = useState(false);
  return (
    <View>
      <View>
        <TouchableOpacity onPress={() => setModelImage(true)} activeOpacity={1}>
          <Image style={styles.view1} source={{ uri: props.bgr_image }} />
        </TouchableOpacity>
      </View>
      <View
        style={{
          width: 99 * scale,
          height: 99 * scale,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 4,
          marginLeft: 16 * scale,
          marginTop: -44 * scale,
        }}>
        <TouchableOpacity onPress={() => setModelAvt(true)} activeOpacity={1}>
          <Image style={styles.view2} source={{ uri: props.avt_image }} />
        </TouchableOpacity>
      </View>
      {/* <Text style={styles.txt1}>{props.name}</Text> */}
      <View style={{
        flexDirection: 'row',
        alignItems: 'flex-end',
        flex: 1, marginTop: 8 * scale,
        marginHorizontal: 16 * scale,
      }}>
        <Text style={[styles.txt1, { marginRight: 5 * scale }]}>{props.name}</Text>
        {
          props?.status === 0 &&
          <View style={{ flexDirection: "row", justifyContent: 'center', alignItems: 'center' }}>
            <Text>•</Text>
            <Image source={require('../../assets/hidden_red.png')} style={{ width: 20 * scale, height: 17 * scale, marginLeft: 6 * scale, marginRight: 10 * scale }} />
            <Text style={{
              color: '#EB5757', fontStyle: 'normal', fontWeight: '600', fontFamily: 'SourceSansPro-Regular',
            }}>{convertLanguage(props.language, 'hidden')}</Text>
          </View>}
      </View>
      <Text style={styles.txt2}>ID: {props.id}</Text>
      {props.hastTag.length > 0 && (
        <Text style={styles.txt2}>{getHastag(props.hastTag)}</Text>
      )}
      {props.detailFull && (
        <>
          <View style={{ marginHorizontal: 16 * scale, marginTop: 8 * scale }}>
            <View style={{ flexDirection: 'row', marginBottom: 2 * scale }}>
              <Image
                source={require('../../assets/Location2.png')}
                style={{
                  width: 16 * scale * scaleFontSize,
                  height: 16 * scale * scaleFontSize,
                  marginRight: 4 * scale,
                  marginTop: 3 * scale,
                }}
              />
              <Text
                style={{ fontSize: 16, width: 308 * scale, color: '#4F4F4F' }}>
                {props.address}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                // marginBottom: 8 * scale,
              }}>
              <Image
                source={require('../../assets/step4Clock.png')}
                style={{
                  width: 16 * scale * scaleFontSize,
                  height: 16 * scale * scaleFontSize,
                  marginRight: 4 * scale,
                }}
                resizeMode="cover"
              />
              <TouchableOpacity
                style={{ flexDirection: 'row', alignItems: 'center' }}
                onPress={() => setToogle(!toogle)}
                ref={refPopover}>
                <Text
                  style={{
                    color: props?.time?.IsOpen ? '#00A9F4' : 'red',
                    fontWeight: 'normal',
                    fontSize: 14,
                    marginRight: 8 * scale,
                  }}>

                  {/* {props?.time?.IsOpen
                    ? convertLanguage(props.language, 'open_now') + ' • '
                    : convertLanguage(props.language, 'closed_now')} */}
                  {
                    (props?.time?.TimeStart == "" || props?.time?.TimeEnd == "") &&
                    convertLanguage(props.language, 'today_closed')
                    // :
                    // props?.time?.IsOpen ?
                    //   convertLanguage(props.language, 'open_now') + '• '
                    //   :
                    //   convertLanguage(props.language, 'closed_now') + '• '
                  }
                </Text>
                {(props?.time?.TimeStart !== "" || props?.time?.TimeEnd !== "") && (
                  <Text style={{ fontSize: 16, color: '#4F4F4F' }}>
                    {props?.time?.TimeStart.substring(0, 5) +
                      ' ~ ' +
                      props?.time?.TimeEnd.substring(0, 5)}
                  </Text>
                )}
                <Image
                  source={require('../../assets/arrowDown.png')}
                  style={{
                    width: 12 * scale * scaleFontSize,
                    height: 12 * scale * scaleFontSize,
                    marginLeft: 4 * scale,
                  }}
                  resizeMode="cover"
                />
              </TouchableOpacity>
            </View>
            {/* <View
              style={{
                flexDirection: 'column',
              }}>
              <Text
                style={{
                  fontSize: 16,
                  color: '#4F4F4F',
                  marginBottom: 4 * scale,
                }}>
                {convertLanguage(props.language, 'manager')}:{' '}
                {props.managerName}
              </Text>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 2 * scale,
                }}
                onPress={() =>
                  Linking.openURL(`tel:${props.zipcode}${props.phone}`)
                }>
                <Image
                  source={require('../../assets/phone.png')}
                  style={{
                    width: 16 * scale * scaleFontSize,
                    height: 16 * scale * scaleFontSize,
                    marginRight: 4 * scale,
                    marginTop: 3 * scale,
                  }}
                />
                <Text
                  style={{ fontSize: 16, width: 308 * scale, color: '#4F4F4F' }}>
                  ({props.zipcode}) {props.phone}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', alignItems: 'center' }}
                onPress={() => Linking.openURL(`mailto:${props.email}`)}>
                <Image
                  source={require('../../assets/email-blue.png')}
                  style={{
                    width: 16 * scale * scaleFontSize,
                    height: 16 * scale * scaleFontSize,
                    marginRight: 4 * scale,
                    marginTop: 3 * scale,
                  }}
                />
                <Text
                  style={{ fontSize: 16, width: 308 * scale, color: '#4F4F4F' }}>
                  {props.email}
                </Text>
              </TouchableOpacity>
            </View> */}
          </View>
        </>
      )}

      <Popover
        isVisible={toogle}
        animationConfig={{ duration: 0 }}
        fromView={refPopover.current}
        placement="bottom"
        arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0 }}
        backgroundStyle={{ backgroundColor: 'transparent' }}
        onRequestClose={() => setToogle(false)}
        popoverStyle={{
          borderRadius: 8,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
          overflow: 'visible',
        }}>
        <View
          style={{
            marginHorizontal: 12 * scale,
            marginVertical: 8 * scale,
            justifyContent: 'center',
          }}>
          {props?.listTime.map((item, index) => {
            return (
              <View
                style={{ flexDirection: 'row', marginRight: 5 * scale }}
                key={index}>
                <Text style={{ lineHeight: 20 * scale, width: 90 * scale }}>
                  {convertLanguage(props.language, item.Code)}:
                </Text>
                <Text
                  style={{
                    lineHeight: 20 * scale,
                    color: item.TimeStart !== '' ? null : 'red',
                  }}>
                  {item.TimeStart !== ''
                    ? item.TimeStart.substring(0, 5) +
                    ' ~ ' +
                    item.TimeEnd.substring(0, 5)
                    : convertLanguage(props.language, 'closed')}
                </Text>
              </View>
            );
          })}
        </View>
      </Popover>
      <ImageView
        images={[
          {
            source: {
              uri: props.bgr_image_full || props.bgr_image,
            },
          },
        ]}
        imageIndex={0}
        isVisible={modelImage}
        onClose={() => setModelImage(false)}
      />
      <ImageView
        images={[
          {
            source: {
              uri: props.avt_image_full || props.avt_image,
            },
          },
        ]}
        imageIndex={0}
        isVisible={modelAvt}
        onClose={() => setModelAvt(false)}
      />
    </View>
  );
};
export default memo(VenueDetailItem);
const styles = StyleSheet.create({
  view1: {
    width: '100%',
    height: 92 * scale,
    backgroundColor: '#C4C4C4',
  },
  view2: {
    width: 96 * scale,
    height: 96 * scale,
    borderRadius: 4,
  },
  txt1: {
    color: '#333333',
    lineHeight: 25.14,
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'SourceSansPro-SemiBold',

  },
  txt2: {
    color: '#4F4F4F',
    lineHeight: 20.11,
    fontSize: 16,
    fontWeight: '400',
    fontFamily: 'SourceSansPro-Regular',
    marginTop: 4 * scale,
    marginLeft: 16 * scale,
  },
});
