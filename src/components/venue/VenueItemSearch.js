import React, { useState } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput, PixelRatio, Platform, Alert
} from 'react-native';
import { useSelector } from "react-redux";
import { convertLanguage } from '../../services/Helper';

const screenWidth = Dimensions.get('window').width;
const scale = screenWidth / 360;
const scaleFontSize = PixelRatio.getFontScale()

export default function VenueItemSearch(props) {
  const { language } = useSelector(state => state.language)
  const getHashtag = (hashtag) => {
    let dataFetch = ''
    hashtag.forEach(element => {
      dataFetch += '#' + element.HashTagName + ' '
    });
    return dataFetch
  }

  const actLike = () => {
    // props.onChangeLike(props.index, props?.item?.Id)
    if (props?.item?.IsLike) {
      Alert.alert(
        "",
        convertLanguage(language, 'unfollow'),
        [
          {
            text: convertLanguage(language, 'cancel'),
            onPress: () => console.log("Cancel Pressed"),
            style: "destructive"
          },
          { text: convertLanguage(language, 'ok'), onPress: () => props.onChangeLike(props.index, props?.item?.Id) }
        ]
      );
    } else {
      props.onChangeLike(props.index, props?.item?.Id)
    }
  }

  return (
    <TouchableOpacity style={styles.view1} onPress={() => props.navigation.navigate('VenueDetail', { Id: props?.item?.Id })}>
      <Image style={styles.view3} source={{ uri: props?.item?.Posters?.Small }} />
      <View style={styles.view2}>
        <Image source={{ uri: props?.item?.Logos?.Medium }} style={{ height: 32 * scale, width: 32 * scale, borderRadius: 4 }} />
      </View>
      {
        props?.item?.Role === "Guest" ?
          <TouchableOpacity
            onPress={() => actLike()}
            style={styles.view4}>
            <Image
              style={{
                height: 16 * scale,
                width: 16 * scale,
                marginTop: 8 * scale
              }}
              resizeMode="contain"
              source={props?.item?.IsLike ? require('../../assets/venue_red_heart.png') : require('../../assets/venue_heart.png')}
            />
          </TouchableOpacity>
          :
          <View style={styles.view4} />
      }
      <View style={{ marginBottom: 10 * scale }} />
      <View style={{
        marginHorizontal: 8 * scale,
        // justifyContent: 'space-between', flexDirection: 'column', flex: 1 
      }}>

        <View style={styles.view5}>
          <Text numberOfLines={2} style={styles.txt1}>{props?.item?.Name}</Text>
        </View>
        <View>
          {props?.item?.HashTag?.length > 0 &&
            <View style={[styles.view5_1]}>
              <Text numberOfLines={1} style={styles.txt2}>{getHashtag(props?.item?.HashTag)}</Text>
            </View>
          }
          <View style={styles.view5_1}>
            <Text numberOfLines={1} style={styles.txt2_1}>{props?.item?.Address}</Text>
          </View>

          <View style={{ flexDirection: 'row', marginTop: 2 * scale, alignItems: 'center' }}>

            {(props?.item?.TimeOpen?.TimeStart !== "" || props?.item?.TimeOpen?.TimeEnd !== "") &&
              <Text style={{ fontWeight: 'normal', fontSize: 10 * scale }}>
                {props?.item?.TimeOpen?.TimeStart.substring(0, 5) + ' - ' + props?.item?.TimeOpen?.TimeEnd.substring(0, 5)}
              </Text>}
            {<Text style={{ color: props?.item?.TimeOpen?.IsOpen ? '#00A9F4' : 'red', fontWeight: 'normal', fontSize: 11 * scale, flex: 1 }} numberOfLines={1}>
              {/* {props?.item?.TimeOpen?.IsOpen ? ' •' + convertLanguage(language, 'open_now') : convertLanguage(language, 'closed_now')} */}
              {
                (props?.item?.TimeOpen?.TimeStart == "" || props?.item?.TimeOpen?.TimeEnd == "") &&
                convertLanguage(language, 'today_closed')
                // :
                // props?.item?.TimeOpen?.IsOpen ?
                //   '  •' + convertLanguage(language, 'open_now')
                //   :
                //   '  •' + convertLanguage(language, 'closed_now')
              }
            </Text>}
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  view1: {

    // height: 500 * scale,
    // backgroundColor: 'red',
    borderRadius: 4 * scale,
    backgroundColor: '#FAFAFA',
    marginTop: 16 * scale,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 0.59,
    },
    shadowOpacity: 0.20,
    shadowRadius: 0.8,
    elevation: 2,
    width: 160 * scale,
    // height: 165 * scale * scaleFontSize,
    paddingBottom: 10
  },
  view2: {
    height: 34 * scale,
    width: 34 * scale,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    position: 'absolute',
    top: 28 * scale,
    left: 20 * scale,
    borderRadius: 4,
    marginTop: 8 * scale
  },
  view3: {
    height: 40 * scale,
    // width: '100%',
    marginHorizontal: 8 * scale,
    marginTop: 8 * scale
    // top: 68 * scale,
    // left: 32 * scale,
    // backgroundColor: 'yellow',
  },
  view4: {
    height: 20 * scale,
    width: 20 * scale,
    position: 'absolute',
    top: 46 * scale,
    right: 16 * scale,
    // backgroundColor: 'gray',
    // marginTop: 11 * scale,
    // marginLeft: 285 * scale,

  },
  view5: {
    marginTop: 10.67 * scale,
  },
  view5_1: {
    // lineHeight: 20 * scale,
    // backgroundColor: 'green',
    marginTop: 2 * scale,
  },
  txt1: {
    // lineHeight: 20 * scale,
    marginVertical: 3 * scale,
    fontSize: 12 * scale,
    fontWeight: 'bold',
    // lineHeight: 21 * scale
    // fontFamily: 'SourceSansPro-Bold',
    color: '#212121',
  },
  txt2: {
    lineHeight: 15 * scale,
    fontSize: 10 * scale,
    fontFamily: 'SourceSansPro-Regular',
    color: '#212121',
    fontStyle: 'normal',
    marginTop: -4 * scale,
    // marginBottom: 2 * scale,
  },
  txt2_1: {
    // lineHeight: 14 * scale,
    fontSize: 10 * scale,
    fontFamily: 'SourceSansPro-Regular',
    color: '#4F4F4F',
    fontStyle: 'normal',
    fontWeight: 'normal'
  },
});
