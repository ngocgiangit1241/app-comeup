import React, { useState } from 'react';
import {
    View,
    Text,
    Dimensions,
    Image,
    StyleSheet,
    TouchableOpacity,
    TextInput, PixelRatio, Alert
} from 'react-native';
import { useSelector } from "react-redux";
import { convertLanguage } from '../../services/Helper';
const screenWidth = Dimensions.get('window').width;
const scale = screenWidth / 360;
const scaleFontSize = PixelRatio.getFontScale()
import VenueItemSearch from '../../components/venue/VenueItemSearch';

export default function VenueLikeHomeItem(props) {

    const { language } = useSelector(state => state.language)


    const actLike = () => {
        if (props.data.IsLike) {
            Alert.alert(
                "",
                convertLanguage(language, 'unlikevenue'),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        onPress: () => console.log("Cancel Pressed"),
                        style: "destructive"
                    },
                    { text: convertLanguage(language, 'ok'), onPress: () => props.onLikeVenue(props.data.Id) }
                ]
            );
        } else {
            props.onLikeVenue(props.data.Id)
        }
    }

    return (
        <TouchableOpacity style={{
            marginBottom: 8 * scale,
            shadowColor: "#000000",
            shadowOffset: {
                width: 0,
                height: 0.59,
            },
            shadowOpacity: 0.20,
            shadowRadius: 0.8,
            elevation: 2,
            borderRadius: 4 * scale,
            backgroundColor: '#FAFAFA',
        }}
            onPress={() => props.navigation.navigate('VenueDetail', { Id: props?.data?.Id })}
        >
            <View
                style={styles.view1}>
                <View
                    style={{
                        justifyContent: 'center', alignItems: 'center', borderRadius: 4, backgroundColor: 'white', width: 34 * scale, height: 34 * scale,
                        position: 'absolute',
                        top: 36 * scale,
                        left: 20 * scale,
                        zIndex: 9999,
                    }}
                >
                    <Image style={styles.view3} source={{ uri: props.data.Logos.Medium }} />
                </View>
                <View style={styles.view2}>
                    <Image source={{ uri: props.data.Posters.Medium }} style={{ height: '100%', width: '100%' }} />
                </View>
                {
                    props?.data?.Role === "Guest" ?
                        <TouchableOpacity
                            onPress={() => { actLike() }}
                            style={styles.view4}>
                            <Image
                                style={{
                                    height: 16 * scale,
                                    width: 16 * scale,
                                    tintColor: !props.data.IsLike ? '#4F4F4F' : '#EB5757'
                                }}
                                resizeMode="contain"
                                source={props.data.IsLike ? require('../../assets/liked_border.png') : require('../../assets/like_border.png')}
                            />
                        </TouchableOpacity>
                        : <View style={styles.view4} />
                }

                <View style={{ marginTop: 24 * scale }}>
                    <View style={styles.viewTitle}>
                        <Text style={styles.txtTitle} numberOfLines={2}>{props.data.Name}</Text>
                    </View>

                    <View style={styles.view5}>
                        <Text style={styles.txt2_1} numberOfLines={1}>
                            {
                                props.data.HashTag &&
                                props.data.HashTag.map((Tag, index) => {
                                    return <React.Fragment key={index}>
                                        #{Tag.HashTagName + ' '}
                                    </React.Fragment>
                                })
                            }
                        </Text>
                    </View>
                    <View style={styles.view5}>
                        <Text numberOfLines={1} style={styles.txt1}>{props.data.Address}</Text>
                    </View>


                    <View style={{ marginLeft: 8 * scale, flexDirection: 'row', marginTop: 2 * scale }}>
                        {props?.data?.TimeOpen?.IsOpen && <Text style={{ fontWeight: 'normal', fontSize: 12 * scale }}>{props?.data?.TimeOpen?.TimeStart.substring(0, 5) + ' - ' + props?.data?.TimeOpen?.TimeEnd.substring(0, 5)}</Text>}
                        <Text style={{ color: props?.data?.TimeOpen?.IsOpen ? '#00A9F4' : 'red', fontWeight: 'normal', fontSize: 12 * scale }}>{props?.data?.TimeOpen?.IsOpen ? ' •' + convertLanguage(language, 'open_now') : convertLanguage(language, 'closed_now')}</Text>
                    </View>
                </View>

            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    view1: {
        width: 160 * scale,
        height: 175 * scale * scaleFontSize,
        borderRadius: 2.37 * scale,
        backgroundColor: '#FAFAFA',
        paddingBottom: 8 * scale
    },
    view2: {
        height: 40 * scale,
        marginLeft: 8 * scale,
        marginRight: 8 * scale,
        marginTop: 8 * scale,
        // backgroundColor: 'blue',
    },
    view3: {
        height: 32 * scale,
        width: 32 * scale,

        borderRadius: 4
    },
    view4: {
        padding: 5,
        position: 'absolute',
        top: 52 * scale,
        right: 3 * scale,

    },
    viewTitle: {
        marginLeft: 8 * scale,
        marginRight: 8 * scale,
    },
    view5: {
        marginLeft: 8 * scale,
        marginRight: 8 * scale,
        marginTop: 4 * scale,
    },
    txt1: {
        fontSize: 11 * scale,
        fontFamily: 'SourceSansPro-Regular',
        color: '#4F4F4F',
    },
    txt2: {
        fontSize: 9.48 * scale,
        fontFamily: 'SourceSansPro-Regular',
        color: '#4F4F4F',
    },
    txt2_1: {
        marginBottom: 2 * scale,
        fontSize: 12 * scale,
        fontFamily: 'SourceSansPro-Regular',
        color: '#333333',
    },
    txtTitle: {
        fontSize: 16 * scale,
        fontFamily: 'SourceSansPro-SemiBold',
        color: '#333333',
    },
});
