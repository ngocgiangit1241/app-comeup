import React, { useState } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput, PixelRatio, Alert
} from 'react-native';
import { useSelector } from "react-redux";
import { convertLanguage } from '../../services/Helper';
const screenWidth = Dimensions.get('window').width;
const scale = screenWidth / 360;
const scaleFontSize = PixelRatio.getFontScale()

export default function VenueListItem(props) {

  const { language } = useSelector(state => state.language)

  const getHashtag = (hashtag) => {
    let dataFetch = '';
    hashtag.forEach(element => {
      dataFetch += '#' + element.HashTagName + '  '
    })
    return dataFetch;
  }


  const actLike = () => {
    if (props?.item?.IsLike) {
      Alert.alert(
        "",
        convertLanguage(language, 'unlikevenue'),
        [
          {
            text: convertLanguage(language, 'cancel'),
            onPress: () => console.log("Cancel Pressed"),
            style: "destructive"
          },
          { text: convertLanguage(language, 'ok'), onPress: () => props.onChangeLike(props?.index, props?.item?.Id) }
        ]
      );
    } else {
      props.onChangeLike(props?.index, props?.item?.Id)
    }
  }



  return (
    <TouchableOpacity
      onPress={() => {
        props.navigation.navigate('VenueDetail', { Id: props?.item?.Id });
        // props.navigation.navigate({ name: 'VenueDetail', params: { Id: props?.item?.Id }, key: props?.item?.Id });
      }}
      style={styles.view1}>
      <View style={{
        borderRadius: 4 * scale,
        position: 'absolute',
        top: 68 * scale,
        left: 32 * scale,
        zIndex: 9999,
        height: 66 * scale,
        width: 66 * scale,
        backgroundColor: 'white',
        justifyContent: "center", alignItems: 'center'
      }}>
        <Image style={styles.view3} source={{ uri: props?.item?.Logos?.Medium }} />
      </View>
      <View style={styles.view2}>
        <Image source={{ uri: props?.item?.Posters?.Small }} style={{ height: '100%', width: '100%' }} />
      </View>

      {
        props?.item?.Role === 'Guest' ? <TouchableOpacity
          onPress={() => actLike()}
          style={styles.view4}>
          <Image
            style={{
              width: 24 * scale,
              height: 24 * scale,
              tintColor: props?.item?.IsLike ? "#EB5757" : '#4F4F4F',
            }}
            source={props?.item?.IsLike ? require('../../assets/liked_border.png') : require('../../assets/like_border.png')}
          />
        </TouchableOpacity> : <View style={styles.view4}></View>
      }


      <View style={styles.view5}>
        <Text numberOfLines={1} style={styles.txt1}>{props?.item?.Name}</Text>
      </View>
      {props?.item?.HashTag?.length > 0 &&
        <View style={styles.view5_1}>
          <Text numberOfLines={1} style={styles.txt2}>{getHashtag(props?.item?.HashTag)}</Text>
        </View>
      }

      <View style={styles.view5_1}>
        <Text numberOfLines={1} style={styles.txt2_1}>{props?.item?.Address}</Text>
      </View>
      <View style={{ marginLeft: 16 * scale, flexDirection: 'row' }}>
        {(props?.item?.TimeOpen?.TimeStart !== "" || props?.item?.TimeOpen?.TimeEnd !== "") && <Text style={{ fontWeight: 'normal', fontSize: 12 * scale }}>{props?.item?.TimeOpen?.TimeStart.substring(0, 5) + ' - ' + props?.item?.TimeOpen?.TimeEnd.substring(0, 5)}</Text>}
        <Text style={{ color: props?.item?.TimeOpen?.IsOpen ? '#00A9F4' : 'red', fontWeight: 'normal', fontSize: 12 * scale }}>
          {/* {props?.item?.TimeOpen?.IsOpen ? ' •' + convertLanguage(language, 'open_now') : convertLanguage(language, 'closed_now')} */}
          {
            (props?.item?.TimeOpen?.TimeStart == "" || props?.item?.TimeOpen?.TimeEnd == "") &&
            convertLanguage(language, 'today_closed')
            // :
            // props?.item?.TimeOpen?.IsOpen ?
            //   '  •' + convertLanguage(language, 'open_now')
            //   :
            //   '  •' + convertLanguage(language, 'closed_now')
          }
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  view1: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    // height: 235 * scale,
    borderRadius: 4 * scale,
    backgroundColor: '#FAFAFA',
    marginTop: 9 * scale,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 0.59,
    },
    shadowOpacity: 0.20,
    shadowRadius: 0.8,
    elevation: 2,
    paddingBottom: 10 * scale
  },
  view2: {
    height: 84 * scale,
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    marginTop: 16 * scale,
  },
  view3: {
    height: 64 * scale,
    width: 64 * scale,
    borderRadius: 4 * scale,
  },
  view4: {
    width: 24 * scale,
    height: 24 * scale,
    // backgroundColor: 'gray',
    marginTop: 11 * scale,
    marginLeft: 290 * scale,
  },
  view5: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    // height: 24 * scale,
    // backgroundColor: 'green',
    marginTop: 5 * scale,
  },
  view5_1: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    // height: 20 * scale,
    // backgroundColor: 'green',
    // marginTop: 2 * scale * scaleFontSize,
  },
  txt1: {
    lineHeight: 25 * scale,
    fontSize: 14 * scale,
    fontFamily: 'SourceSansPro-SemiBold',
    color: '#333333',
    fontWeight: 'bold',
  },
  txt2: {
    lineHeight: 19 * scale,
    fontSize: 12 * scale,
    fontFamily: 'SourceSansPro-Regular',
    color: '#4F4F4F',
    fontStyle: 'normal',
  },
  txt2_1: {
    lineHeight: 19 * scale,
    fontSize: 12 * scale,
    fontFamily: 'SourceSansPro-Regular',
    color: '#4F4F4F',
    fontStyle: 'normal',
  },
});
