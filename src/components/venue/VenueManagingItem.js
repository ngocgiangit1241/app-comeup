import React, { useState, useRef } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  FlatList,
  SafeAreaView, PixelRatio
} from 'react-native';
import { useSelector } from "react-redux";
import { convertLanguage } from '../../services/Helper';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const scale = screenWidth / 360;
const scaleFontSize = PixelRatio.getFontScale()

export default function VenueManagingItem(props) {
  // console.log('props', props)
  // const checkLength = (text) => {
  //   if (text.length >= 35) {
  //     return text.substring(0, 34) + '...'
  //   } else {
  //     return text
  //   }
  // }
  const { language } = useSelector(state => state.language)

  return (
    <TouchableOpacity
      onPress={() => {
        props.navigation.navigate({ name: 'VenueDetail', params: { Id: props?.item?.Id }, key: props?.item?.Id });
      }}
      style={styles.view1Host}>
      <View style={{
        justifyContent: 'center', alignItems: 'center', borderRadius: 4, backgroundColor: 'white', width: 64 * scale, height: 64 * scale, position: 'absolute',
        top: 68 * scale,
        left: 32 * scale,
        zIndex: 9999,
      }}>
        <Image style={styles.view3Host} source={{ uri: props?.item?.Logos?.Medium }} />
      </View>
      <View style={styles.view2Host}>
        <Image source={{ uri: props?.item?.Posters?.Small }} style={{ height: '100%', width: '100%' }} />
      </View>

      <View style={styles.view5Host}>
        <Text numberOfLines={1} style={styles.txt1Host}>{props?.item?.Name}</Text>
      </View>
      {props?.item?.HashTag &&
        <View style={styles.view5_1Host}>
          <Text style={styles.txt2_1Host} numberOfLines={1}>
            {
              props?.item.HashTag.map((Tag, index) => {
                return <React.Fragment key={index}>
                  #{Tag.HashTagName + ' '}
                </React.Fragment>
              })
            }
          </Text>
        </View>
      }

      {/* {
        props?.item?.TimeOpen.TimeEnd !== "" && props?.item?.TimeOpen.TimeStart !== "" &&
        <View style={styles.view5_1Host}>
          <Text style={styles.txt2_1Host}>{props?.item?.TimeOpen.TimeStart + ' - ' + props?.item?.TimeOpen.TimeEnd}</Text>
        </View>
      } */}
      <View style={styles.view5_1Host}>
        <Text numberOfLines={1} style={styles.txt2_1Host}>{props?.item?.Address}</Text>
      </View>
      <View style={{ marginLeft: 16 * scale, flexDirection: 'row' }}>
        {(props?.item?.TimeOpen?.TimeStart !== "" || props?.item?.TimeOpen?.TimeEnd !== "") &&
          <Text style={{ fontWeight: 'normal', fontSize: 16 * scale }}>{props?.item?.TimeOpen?.TimeStart.substring(0, 5) + ' - ' + props?.item?.TimeOpen?.TimeEnd.substring(0, 5)}</Text>}
        <Text style={{ color: props?.item?.TimeOpen?.IsOpen ? '#00A9F4' : 'red', fontWeight: 'normal', fontSize: 16 * scale }}>
          {/* {props?.item?.TimeOpen?.IsOpen ? ' •' + convertLanguage(language, 'open_now') : convertLanguage(language, 'closed_now')} */}
          {
            (props?.item?.TimeOpen?.TimeStart == "" || props?.item?.TimeOpen?.TimeEnd == "") &&
            convertLanguage(language, 'today_closed')
            // :
            // props?.item?.TimeOpen?.IsOpen ?
            //   '  •' + convertLanguage(language, 'open_now')
            //   :
            //   '  •' + convertLanguage(language, 'closed_now')
          }
        </Text>
      </View>
      {(props?.item?.Status === 0 || props?.item?.IsReview === false) &&
        <>
          <View style={{ backgroundColor: 'white', position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, opacity: 0.7, zIndex: 9999, justifyContent: 'center', alignItems: 'center' }} >
          </View>
          <View style={{ position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, zIndex: 9999, justifyContent: 'center', alignItems: 'center' }} >
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', opacity: 1 }}>
              <Image source={props?.item?.Status === 0 ? require('../../assets/hidden_red.png') : require('../../assets/clock_orange.png')} style={{ width: 20 * scale, height: 20 * scale, marginRight: 10 * scale }} />
              <Text style={{ color: '#DB6D39', fontSize: 20 * scale, fontStyle: 'normal', fontWeight: '600', fontFamily: 'SourceSansPro-SemiBold', }}>{convertLanguage(language, props?.item?.Status === 0 ? 'hidden' : 'reviewing')}</Text>
            </View>
          </View>
        </>
      }
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  view1Host: {
    width: 328 * scale,
    height: 240 * scale * scaleFontSize,
    borderRadius: 4 * scale,
    backgroundColor: '#FAFAFA',
    marginTop: 16 * scale,
    alignSelf: "center",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 0.59,
    },
    shadowOpacity: 0.20,
    shadowRadius: 0.8,
    elevation: 2,
    // paddingLeft: -16 * scale,
  },
  view2Host: {
    height: 84 * scale,
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    marginTop: 16 * scale,
  },
  view3Host: {
    height: 62 * scale,
    width: 62 * scale,
    borderRadius: 4 * scale,
  },
  view4Host: {
    height: 18.33 * scale,
    width: 21 * scale,
    // backgroundColor: 'gray',
    marginTop: 11 * scale,
    marginLeft: 290 * scale,
  },
  view5Host: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    // height: 24 * scale,
    // backgroundColor: 'green',
    marginTop: 40 * scale,
  },
  view5_1Host: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    // height: 20 * scale,
    // backgroundColor: 'green',
    marginTop: 2 * scale,
  },
  txt1Host: {
    lineHeight: 20.11 * scale,
    fontSize: 18 * scale,
    fontFamily: 'SourceSansPro-SemiBold',

    color: '#333333',
  },
  txt2Host: {
    lineHeight: 20.11 * scale,
    fontSize: 16 * scale,
    fontFamily: 'SourceSansPro-Regular',
    color: '#333333',
    fontStyle: 'normal',
  },
  txt2_1Host: {
    lineHeight: 20.11 * scale * scaleFontSize,
    fontSize: 16 * scale,
    fontFamily: 'SourceSansPro-Regular',
    color: '#4F4F4F',
    fontStyle: 'normal',
  },
})
