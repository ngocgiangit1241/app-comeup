import React, { useState, useRef, useEffect } from 'react';
import {
    View,
    Text,
    Dimensions,
    Image,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ScrollView,
    FlatList,
    SafeAreaView, PixelRatio
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { useSelector } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import { cos } from 'react-native-reanimated';
const scaleFontSize = PixelRatio.getFontScale()

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const scale = screenWidth / 360;
export default function VenueManagingProfile(props) {
    const listManaging = props.data;
    const { language } = useSelector(state => state.language)

    return (
        <View style={{ backgroundColor: 'white', }}>
            <View>
                <Carousel
                    data={listManaging}
                    renderItem={({ item, index }) => {
                        return (

                            <TouchableOpacity
                                onPress={() => {
                                    props.navigation.navigate({ name: 'VenueDetail', params: { Id: item?.Id }, key: item?.Id });
                                }}
                                style={styles.view1Host}>
                                <View style={{
                                    justifyContent: 'center', alignItems: 'center', borderRadius: 4, backgroundColor: 'white', width: 64 * scale, height: 64 * scale, position: 'absolute',
                                    top: 68 * scale,
                                    left: 32 * scale,
                                    zIndex: 9999,
                                }}>
                                    <Image style={styles.view3Host} source={{ uri: item?.Logos?.Medium }} />
                                </View>
                                <View style={styles.view2Host}>
                                    <Image source={{ uri: item.Posters?.Small }} style={{ height: '100%', width: '100%' }} />
                                </View>

                                <View style={styles.view5Host}>
                                    <Text numberOfLines={1} style={styles.txt1Host}>{item?.Name}</Text>
                                </View>

                                {item?.HashTag &&
                                    <View style={styles.view5_1Host}>
                                        <Text style={styles.txt2_1Host} numberOfLines={1}>
                                            {
                                                item.HashTag.map((Tag, index) => {
                                                    return <React.Fragment key={index}>
                                                        #{Tag.HashTagName + ' '}
                                                    </React.Fragment>
                                                })
                                            }
                                        </Text>
                                    </View>
                                }


                                <View style={styles.view5_1Host}>
                                    <Text numberOfLines={1} style={styles.txt2_1Host}>{item?.Address}</Text>
                                </View>
                                <View style={{ marginLeft: 16 * scale, flexDirection: 'row' }}>
                                    {(item?.TimeOpen?.TimeStart !== "" || item?.TimeOpen?.TimeEnd !== "") && <Text style={{ fontWeight: 'normal', fontSize: 16 * scale }}>{item?.TimeOpen?.TimeStart.substring(0, 5) + ' - ' + item?.TimeOpen?.TimeEnd.substring(0, 5)}</Text>}
                                    <Text style={{ color: item?.TimeOpen?.IsOpen ? '#00A9F4' : 'red', fontWeight: 'normal', fontSize: 14 * scale, marginTop: 2 * scale }}>
                                        {/* {item?.TimeOpen?.IsOpen ? ' •' + convertLanguage(language, 'open_now') : convertLanguage(language, 'closed_now')} */}
                                        {
                                            (item?.TimeOpen?.TimeStart == "" || item?.TimeOpen?.TimeEnd == "") &&
                                            convertLanguage(language, 'today_closed')
                                            // :
                                            // item?.TimeOpen?.IsOpen ?
                                            //     '  •' + convertLanguage(language, 'open_now')
                                            //     :
                                            //     '  •' + convertLanguage(language, 'closed_now')
                                        }
                                    </Text>
                                </View>
                                {(item.Status === 0 || item.IsReview === false) &&
                                    <>
                                        <View style={{ backgroundColor: 'white', position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, opacity: 0.7, zIndex: 9999, justifyContent: 'center', alignItems: 'center' }} >
                                        </View>
                                        <View style={{ position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, zIndex: 9999, justifyContent: 'center', alignItems: 'center' }} >
                                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', opacity: 1 }}>
                                                <Image source={item.Status === 0 ? require('../../assets/hidden_red.png') : require('../../assets/clock_orange.png')} style={{ width: 20 * scale, height: 20 * scale, marginRight: 10 * scale }} />
                                                <Text style={{ color: '#DB6D39', fontSize: 20 * scale, fontStyle: 'normal', fontWeight: '600', fontFamily: 'SourceSansPro-SemiBold', }}>{convertLanguage(language, item.Status === 0 ? 'hidden' : 'reviewing')}</Text>
                                            </View>
                                        </View>
                                    </>
                                }
                            </TouchableOpacity>
                        )
                    }}
                    sliderWidth={screenWidth - 15}
                    itemWidth={screenWidth - 50}
                    inactiveSlideScale={1}
                    firstItem={0}
                />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    view6: {
        marginLeft: 16 * scale,
        marginRight: 16 * scale,
        height: 25 * scale,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginTop: 24 * scale,
    },
    txt9: {
        lineHeight: 25.11,
        fontSize: 20 * scale,
        fontFamily: 'SourceSansPro-SemiBold',
        color: '#333333',
    },
    txt10: {
        lineHeight: 20.11,
        fontSize: 16 * scale,
        fontFamily: 'SourceSansPro-Regular',
        color: '#555555',
    },

    view1Host: {
        // marginLeft: -16 * scale,
        // marginRight: 'auto',
        width: 300 * scale,
        height: 245 * scale * scaleFontSize,
        borderRadius: 4 * scale,
        backgroundColor: '#FAFAFA',

        shadowColor: "#000000",
        shadowOffset: {
            width: 0,
            height: 0.59,
        },
        shadowOpacity: 0.20,
        shadowRadius: 0.8,
        elevation: 2,
        // paddingBottom: 20,
        marginBottom: 10,
        marginLeft: 1
    },
    view2Host: {
        height: 84 * scale,
        marginLeft: 16 * scale,
        marginRight: 16 * scale,
        marginTop: 16 * scale,
    },
    view3Host: {
        height: 62 * scale,
        width: 62 * scale,
        borderRadius: 4 * scale,
    },
    view4Host: {
        height: 18.33 * scale,
        width: 21 * scale,
        // backgroundColor: 'gray',
        marginTop: 11 * scale,
        marginLeft: 290 * scale,
    },
    view5Host: {
        marginLeft: 16 * scale,
        marginRight: 16 * scale,
        // height: 24 * scale,
        // backgroundColor: 'green',
        marginTop: 40 * scale,
    },
    view5_1Host: {
        marginLeft: 16 * scale,
        marginRight: 16 * scale,
        // height: 20 * scale,
        // backgroundColor: 'green',
        marginTop: 4 * scale,
    },
    txt1Host: {
        lineHeight: 20.11 * scaleFontSize * scale,
        fontSize: 18 * scale,
        fontFamily: 'SourceSansPro-SemiBold',

        color: '#333333',
    },
    txt2Host: {
        lineHeight: 20.11 * scaleFontSize * scale,
        fontSize: 16 * scale,
        fontFamily: 'SourceSansPro-Regular',
        color: '#333333',
        fontStyle: 'normal',
    },
    txt2_1Host: {
        lineHeight: 20.11 * scaleFontSize * scale,
        fontSize: 16 * scale,
        fontFamily: 'SourceSansPro-Regular',
        color: '#4F4F4F',
        fontStyle: 'normal',
    },
})