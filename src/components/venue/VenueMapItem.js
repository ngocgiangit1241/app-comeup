import React, { useState, memo } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput, PixelRatio, Alert
} from 'react-native';
import { useSelector } from "react-redux";
import { convertLanguage } from '../../services/Helper';
const screenWidth = Dimensions.get('window').width;
const scale = screenWidth / 360;
const scaleFontSize = PixelRatio.getFontScale()

function VenueMapItem(props) {
  const { language } = useSelector(state => state.language)


  const getHashtag = (hashtag) => {
    let dataFetch = ''
    hashtag.forEach(element => {
      dataFetch += '#' + element.HashTagName + ' '
    });
    return dataFetch
  }

  const actLike = () => {
    if (props?.item?.IsLike) {
      Alert.alert(
        "",
        convertLanguage(language, 'unlikevenue'),
        [
          {
            text: convertLanguage(language, 'cancel'),
            onPress: () => console.log("Cancel Pressed"),
            style: "destructive"
          },
          { text: convertLanguage(language, 'ok'), onPress: () => props.onChangeLike(props?.index, props?.item?.Id) }
        ]
      );
    } else {
      props.onChangeLike(props?.index, props?.item?.Id)
    }
  }

  return (
    <View style={styles.view1} >
      <View style={{
        height: 66 * scale,
        width: 66 * scale,
        position: 'absolute',
        top: 68 * scale,
        left: 32 * scale,
        zIndex: 9999, justifyContent: 'center', alignItems: 'center', borderRadius: 4, backgroundColor: 'white'
      }}>
        <Image style={styles.view3} source={{ uri: props?.item?.Logos?.Medium }} />
      </View>
      <View style={styles.view2}>
        <Image source={{ uri: props?.item?.Posters?.Small }} style={{ height: '100%', width: '100%' }} />
      </View>

      {
        props?.item?.Role === "Guest" ?
          <TouchableOpacity
            onPress={() => actLike()}
            style={styles.view4}>
            <Image
              style={{
                height: 18.33 * scale,
                width: 20 * scale,
              }}
              source={props?.item?.IsLike ? require('../../assets/venue_red_heart.png') : require('../../assets/venue_heart.png')}
            />
          </TouchableOpacity>
          :
          <View style={styles.view4} />
      }

      <View style={styles.view5}>
        <Text numberOfLines={1} style={styles.txt1}>{props?.item?.Name}</Text>
      </View>
      {props?.item?.HashTag?.length > 0 &&
        <View style={styles.view5_1}>
          <Text numberOfLines={1} style={styles.txt2}>{getHashtag(props?.item?.HashTag)}</Text>
        </View>
      }

      <View style={styles.view5_1}>
        <Text numberOfLines={1} style={styles.txt2_1}>{props?.item?.Address}</Text>
      </View>
      <View style={{ marginLeft: 16 * scale, flexDirection: 'row' }}>
        {(props?.item?.TimeOpen?.TimeStart !== "" || props?.item?.TimeOpen?.TimeEnd !== "") && <Text style={{ fontWeight: 'normal', fontSize: 12 * scale }}>{props?.item?.TimeOpen?.TimeStart.substring(0, 5) + ' - ' + props?.item?.TimeOpen?.TimeEnd.substring(0, 5)}</Text>}
        <Text style={{ color: props?.item?.TimeOpen?.IsOpen ? '#00A9F4' : 'red', fontWeight: 'normal', fontSize: 12 * scale }}>
          {/* {props?.item?.TimeOpen?.IsOpen ? ' •' + convertLanguage(language, 'open_now') : convertLanguage(language, 'closed_now')} */}
          {
            (props?.item?.TimeOpen?.TimeStart == "" || props?.item?.TimeOpen?.TimeEnd == "") &&
            convertLanguage(language, 'today_closed')
            // :
            // props?.item?.TimeOpen?.IsOpen ?
            //   '  •' + convertLanguage(language, 'open_now')
            //   :
            //   '  •' + convertLanguage(language, 'closed_now')
          }
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  view1: {
    paddingBottom: 10 * scale,
    // height: 235 * scale * scaleFontSize,
    borderRadius: 4 * scale,
    backgroundColor: '#FAFAFA',
    marginTop: 16 * scale,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 0.59,
    },
    shadowOpacity: 0.20,
    shadowRadius: 0.8,
    elevation: 2,

  },
  view2: {
    height: 84 * scale,
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    marginTop: 18 * scale,
  },
  view3: {
    height: 64 * scale,
    width: 64 * scale,

    borderRadius: 4 * scale,

  },
  view4: {
    height: 18.33 * scale,
    width: 20 * scale,
    // backgroundColor: 'gray',
    marginTop: 11 * scale,
    marginLeft: 285 * scale,

  },
  view5: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    // backgroundColor: 'green',
    marginTop: 10.67 * scale,
  },
  view5_1: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    // backgroundColor: 'green',
    // marginTop: 2 * scale,
  },
  txt1: {
    lineHeight: 25 * scale,
    fontSize: 14 * scale,
    fontFamily: 'SourceSansPro-Bold',
    color: '#333333',
    marginTop: 2 * scale,
    fontWeight: 'bold'
  },
  txt2: {
    lineHeight: 20.11 * scale,
    fontSize: 12 * scale,
    fontFamily: 'SourceSansPro-Regular',
    color: '#333333',
    fontStyle: 'normal',
  },
  txt2_1: {
    lineHeight: 20.11 * scale,
    fontSize: 12 * scale,
    fontFamily: 'SourceSansPro-Regular',
    color: '#4F4F4F',
    fontStyle: 'normal',
  },
});

export default memo(VenueMapItem)