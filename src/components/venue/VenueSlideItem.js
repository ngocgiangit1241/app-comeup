import React, { useState } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';

const screenWidth = Dimensions.get('window').width;
const scale = screenWidth / 360;

export default function VenueSlideItem(props) {
  const checkLength = (text) => {
    if (text.length >= 35) {
      // string(string.substring(35));
      text === text.substring(35);
      return text.substring(0, 34) + '...';
    } else {
      return text;
    }
  };


  const getHashtag = (hashtag) => {
    let dataFetch = '';
    hashtag.forEach(element => {
      dataFetch += '#' + element.HashTagName + ''
    })
    return dataFetch;
  }
  // console.log('props.item.like', props?.item?.HashTag.length)
  return (
    <TouchableOpacity
      onPress={() => {
        props.navigation.push('VenueDetail', { Id: props?.item?.Id });
      }}
      style={styles.view1}>
      <View style={{
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 58 * scale,
        left: 32 * scale,
        zIndex: 9999, backgroundColor: 'white', borderRadius: 4, width: 64 * scale, height: 64 * scale
      }}>
        <Image style={styles.view3} source={{ uri: props?.item?.Logos?.Small }} />
      </View>
      <View style={styles.view2}>
        <Image source={{ uri: props?.item?.Posters?.Small }} style={{ height: '100%', width: '100%' }} />
      </View>

      {
        props?.item?.Role === "Guest" ?
          <TouchableOpacity
            onPress={() => props.onLikeVenue(props?.item?.Id)}
            style={styles.view4}>
            <Image
              style={{
                width: 24 * scale,
                height: 24 * scale,
                tintColor: props?.item?.IsLike ? "#EB5757" : '#4F4F4F',
              }}
              source={props?.item?.IsLike ? require('../../assets/liked_border.png') : require('../../assets/like_border.png')}
            />
          </TouchableOpacity>
          : <View style={styles.view4} />
      }


      <View style={styles.view5}>
        <Text style={styles.txt1}>{checkLength(props?.item?.Name)}</Text>
      </View>
      {props?.item?.HashTag?.length > 0 &&
        <View style={styles.view5_1}>
          <Text style={styles.txt2}>{getHashtag(props?.item?.HashTag)}</Text>
        </View>
      }
      {props?.item?.TimeOpen.TimeEnd !== "" && props?.item?.TimeOpen.TimeStart !== "" &&
        <View style={styles.view5_1}>
          <Text style={styles.txt2_1}>{props?.item?.TimeOpen.TimeStart + ' - ' + props?.item?.TimeOpen.TimeEnd}</Text>
        </View>
      }
      <View style={styles.view5_1}>
        <Text style={styles.txt2_1}>{checkLength(props?.item?.Address)}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  view1: {
    marginRight: 16,
    borderRadius: 4 * scale,
    backgroundColor: '#FAFAFA',
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 0.59,
    },
    shadowOpacity: 0.20,
    shadowRadius: 0.8,
    elevation: 2,
    marginBottom: 1,
    paddingBottom: 16
  },
  view2: {
    height: 74 * scale,
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    marginTop: 16 * scale,
  },
  view3: {
    height: 62 * scale,
    width: 62 * scale,
    borderRadius: 4 * scale,

  },
  view4: {
    position: 'absolute',
    top: 98 * scale,
    right: 16 * scale,
  },
  view5: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    // height: 24 * scale,
    // backgroundColor: 'green',
    marginTop: 40 * scale,
  },
  view5_1: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    // height: 20 * scale,
    // backgroundColor: 'green',
    marginTop: 2 * scale,
  },
  txt1: {
    lineHeight: 20.11,
    fontSize: 16,
    fontFamily: 'SourceSansPro-SemiBold',

    color: '#333333',
  },
  txt2: {
    lineHeight: 20.11,
    fontSize: 16,
    fontFamily: 'SourceSansPro-Regular',
    color: '#333333',
    fontStyle: 'normal',
  },
  txt2_1: {
    lineHeight: 20.11,
    fontSize: 16,
    fontFamily: 'SourceSansPro-Regular',
    color: '#4F4F4F',
    fontStyle: 'normal',
  },
});
