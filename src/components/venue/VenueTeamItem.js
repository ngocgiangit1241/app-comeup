import React, { useState } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  FlatList,
  SafeAreaView
} from 'react-native';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const scale = screenWidth / 360;
export default function VenueTeamItem(props) {

  const checkLength = () => {
    if (props.teamNewItem.des.length >= 175) {
      props.teamNewItem.des === props.teamNewItem.des.substring(175)
      return (
        props.teamNewItem.des.substring(0, 174) + '...'
      )
    } else {
      return props.teamNewItem.des;
    }
  }

  return (
    <View style={styles.viewMain}>

      <View style={styles.viewHeader}>
        <Image
          style={{ width: 48, height: 48 }}
          source={props.teamNewItem.avt}
        />
        <View style={styles.viewHeader_1}>
          <Text style={styles.txtMain}>{props.teamNewItem.team}</Text>
          <Text style={styles.txtTime}>{props.teamNewItem.time}</Text>
        </View>

        <TouchableOpacity>
          <Image
            // resizeMode="cover"
            style={{ width: 20, height: 5, marginLeft: 99 * scale, marginTop: 7 * scale }}
            source={require('../../assets/more_horizontal.png')}
          />
        </TouchableOpacity>
      </View>

      {
        checkLength(props.teamNewItem.des)
          ?
          <View
            style={{ flexDirection: "row" }}
          >
            <Text style={styles.txtDes}>{checkLength(props.teamNewItem.des)}
              <Text onPress={() => console.log('DM')} style={styles.txtReadMore}> Read More</Text>
            </Text>
          </View>
          :
          <Text style={styles.txtDes}>{checkLength(props.teamNewItem.des)}</Text>
      }


      <Image
        style={{
          width: 312 * scale,
          height: 478 * scale,
          alignSelf: "center",
          marginTop: 8 * scale,
        }}
        source={props.teamNewItem.image}
      />

      <View style={{ flexDirection: "row", marginTop: 11 }}>
        <TouchableOpacity>
          <Image
            style={{ width: 20, height: 18.33, marginLeft: 10 }}
            source={require('../../assets/venue_heart.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <Image
            style={{ width: 20, height: 18.33, marginLeft: 12 }}
            source={require('../../assets/share.png')}
          />
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  viewMain: {
    marginLeft: 16,
    marginRight: 16,
    height: 666 * scale,
    borderRadius: 10 * scale,
    backgroundColor: '#F5F5F5',
    marginTop: 16,
  },
  viewHeader: {
    flexDirection: "row",
    height: 48 * scale,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 8,
  },
  viewHeader_1: {

    height: 48 * scale,
    marginLeft: 8,

  },
  viewMain2: {},
  viewMain3: {},
  viewMain4: {},
  txtMain: {
    lineHeight: 25.14,
    fontSize: 20,
    fontFamily: 'SourceSansPro-SemiBold',
    color: '#333333',
  },
  txtTime: {
    lineHeight: 15.08,
    fontSize: 12,
    fontFamily: 'SourceSansPro-Regular',
    color: '#333333',
  },
  txtDes: {
    lineHeight: 18,
    fontSize: 14,
    fontFamily: 'SourceSansPro-Regular',
    color: '#333333',
    marginLeft: 8 * scale,
    marginTop: 12 * scale,
  },
  txtReadMore: {
    color: '#00A9F4',
    lineHeight: 18,
    fontSize: 14,
    fontFamily: 'SourceSansPro-Regular',
  }
})
