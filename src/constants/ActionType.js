export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const CHECK_LOGIN_REQUEST = 'CHECK_LOGIN_REQUEST';
export const CHECK_LOGIN_SUCCESS = 'CHECK_LOGIN_SUCCESS';
export const CHECK_LOGIN_FAILURE = 'CHECK_LOGIN_FAILURE';
export const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS';
export const SHOW_MODAL_SETTING_TEAM = 'SHOW_MODAL_SETTING_TEAM';
export const HIDE_MODAL_SETTING_TEAM = 'HIDE_MODAL_SETTING_TEAM';
export const LOGOUT = 'LOGOUT';
export const SHOW_MODAL_LOGIN = 'SHOW_MODAL_LOGIN';
export const HIDE_MODAL_LOGIN = 'HIDE_MODAL_LOGIN';
export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';
export const LOAD_DATA_CONFIG_SUCCESS = 'LOAD_DATA_CONFIG_SUCCESS';
export const LOAD_DATA_GEOIP_SUCCESS = 'LOAD_DATA_GEOIP_SUCCESS';
export const SET_DATA_CITY_SUCCESS = 'SET_DATA_CITY_SUCCESS';
export const SET_DATA_COUNTRY_SUCCESS = 'SET_DATA_COUNTRY_SUCCESS';
export const CLEAR_TEAM_LIST = 'CLEAR_TEAM_LIST';
export const TEAM_DETAIL_REQUEST = 'TEAM_DETAIL_REQUEST';
export const TEAM_DETAIL_SUCCESS = 'TEAM_DETAIL_SUCCESS';
export const TEAM_DETAIL_FAILURE = 'TEAM_DETAIL_FAILURE';
export const TEAM_REPORT_REQUEST = 'TEAM_REPORT_REQUEST';
export const TEAM_REPORT_SUCCESS = 'TEAM_REPORT_SUCCESS';
export const TEAM_REPORT_FAILURE = 'TEAM_REPORT_FAILURE';
export const TEAM_DELETE_REQUEST = 'TEAM_DELETE_REQUEST';
export const TEAM_DELETE_SUCCESS = 'TEAM_DELETE_SUCCESS';
export const TEAM_DELETE_FAILURE = 'TEAM_DELETE_FAILURE';
export const TEAM_MEMBERSLIST_REQUEST = 'TEAM_MEMBERSLIST_REQUEST';
export const TEAM_MEMBERSLIST_SUCCESS = 'TEAM_MEMBERSLIST_SUCCESS';
export const TEAM_MEMBERSLIST_FAILURE = 'TEAM_MEMBERSLIST_FAILURE';
export const TEAM_LEAVE_REQUEST = 'TEAM_LEAVE_REQUEST';
export const TEAM_LEAVE_SUCCESS = 'TEAM_LEAVE_SUCCESS';
export const TEAM_LEAVE_FAILURE = 'TEAM_LEAVE_FAILURE';
export const TEAM_ROLE_REQUEST = 'TEAM_ROLE_REQUEST';
export const TEAM_ROLE_SUCCESS = 'TEAM_ROLE_SUCCESS';
export const TEAM_ROLE_FAILURE = 'TEAM_ROLE_FAILURE';
export const TEAM_INVITE_REQUEST = 'TEAM_INVITE_REQUEST';
export const TEAM_INVITE_SUCCESS = 'TEAM_INVITE_SUCCESS';
export const TEAM_INVITE_FAILURE = 'TEAM_INVITE_FAILURE';
export const TEAM_NEWSLIST_REQUEST = 'TEAM_NEWSLIST_REQUEST';
export const TEAM_NEWSLIST_SUCCESS = 'TEAM_NEWSLIST_SUCCESS';
export const TEAM_NEWSLIST_FAILURE = 'TEAM_NEWSLIST_FAILURE';
export const TEAM_LIST_FOLLOW_REQUEST = 'TEAM_LIST_FOLLOW_REQUEST';
export const TEAM_LIST_FOLLOW_SUCCESS = 'TEAM_LIST_FOLLOW_SUCCESS';
export const TEAM_LIST_FOLLOW_FAILURE = 'TEAM_LIST_FOLLOW_FAILURE';
export const TEAM_LIST_REQUEST = 'TEAM_LIST_REQUEST';
export const TEAM_LIST_SUCCESS = 'TEAM_LIST_SUCCESS';
export const TEAM_LIST_FAILURE = 'TEAM_LIST_FAILURE';
export const HOME_TEAM_LIST_REQUEST = 'HOME_TEAM_LIST_REQUEST';
export const HOME_TEAM_LIST_SUCCESS = 'HOME_TEAM_LIST_SUCCESS';
export const HOME_TEAM_LIST_FAILURE = 'HOME_TEAM_LIST_FAILURE';
export const FOLLOWING_TEAM_LIST_REQUEST = 'FOLLOWING_TEAM_LIST_REQUEST';
export const FOLLOWING_TEAM_LIST_SUCCESS = 'FOLLOWING_TEAM_LIST_SUCCESS';
export const FOLLOWING_TEAM_LIST_FAILURE = 'FOLLOWING_TEAM_LIST_FAILURE';
export const TEAM_FOLLOW_REQUEST = 'TEAM_FOLLOW_REQUEST';
export const TEAM_FOLLOW_SUCCESS = 'TEAM_FOLLOW_SUCCESS';
export const TEAM_VENUE_FOLLOW_SUCCESS = 'TEAM_VENUE_FOLLOW_SUCCESS';

export const TEAM_FOLLOW_FAILURE = 'TEAM_FOLLOW_FAILURE';
export const TEAM_IMAGEINS_REQUEST = 'TEAM_IMAGEINS_REQUEST';
export const TEAM_IMAGEINS_SUCCESS = 'TEAM_IMAGEINS_SUCCESS';
export const TEAM_IMAGEINS_FAILURE = 'TEAM_IMAGEINS_FAILURE';
export const TEAM_SET_INSTAGRAM_REQUEST = 'TEAM_SET_INSTAGRAM_REQUEST';
export const TEAM_SET_INSTAGRAM_SUCCESS = 'TEAM_SET_INSTAGRAM_SUCCESS';
export const TEAM_SET_INSTAGRAM_FAILURE = 'TEAM_SET_INSTAGRAM_FAILURE';
export const TEAM_REMOVE_INSTAGRAM_REQUEST = 'TEAM_REMOVE_INSTAGRAM_REQUEST';
export const TEAM_REMOVE_INSTAGRAM_SUCCESS = 'TEAM_REMOVE_INSTAGRAM_SUCCESS';
export const TEAM_REMOVE_INSTAGRAM_FAILURE = 'TEAM_REMOVE_INSTAGRAM_FAILURE';
export const TEAM_NEWS_CREATE_REQUEST = 'TEAM_NEWS_CREATE_REQUEST';
export const TEAM_NEWS_CREATE_SUCCESS = 'TEAM_NEWS_CREATE_SUCCESS';
export const TEAM_NEWS_CREATE_FAILURE = 'TEAM_NEWS_CREATE_FAILURE';
export const LIKE_TEAM_NEWS_REQUEST = 'LIKE_TEAM_NEWS_REQUEST';
export const LIKE_TEAM_NEWS_SUCCESS = 'LIKE_TEAM_NEWS_SUCCESS';
export const LIKE_TEAM_NEWS_FAILURE = 'LIKE_TEAM_NEWS_FAILURE';
export const UPDATE_LIKE_TEAM_NEWS_REQUEST = 'UPDATE_LIKE_TEAM_NEWS_REQUEST';
export const UPDATE_LIKE_TEAM_NEWS_FAILURE = 'UPDATE_LIKE_TEAM_NEWS_FAILURE';
export const LOAD_DATA_TEAM_NEWS_REQUEST = 'LOAD_DATA_TEAM_NEWS_REQUEST';
export const LOAD_DATA_TEAM_NEWS_SUCCESS = 'LOAD_DATA_TEAM_NEWS_SUCCESS';
export const LOAD_DATA_TEAM_NEWS_FAILURE = 'LOAD_DATA_TEAM_NEWS_FAILURE';
export const LOAD_DATA_TEAM_NEWS_DETAIL_REQUEST =
  'LOAD_DATA_TEAM_NEWS_DETAIL_REQUEST';
export const LOAD_DATA_TEAM_NEWS_DETAIL_SUCCESS =
  'LOAD_DATA_TEAM_NEWS_DETAIL_SUCCESS';
export const LOAD_DATA_TEAM_NEWS_DETAIL_FAILURE =
  'LOAD_DATA_TEAM_NEWS_DETAIL_FAILURE';
export const CLEAR_TEAM_INVITE_SEARCH = 'CLEAR_TEAM_INVITE_SEARCH';
export const EDIT_TEAM_NEWS_REQUEST = 'EDIT_TEAM_NEWS_REQUEST';
export const EDIT_TEAM_NEWS_SUCCESS = 'EDIT_TEAM_NEWS_SUCCESS';
export const EDIT_TEAM_NEWS_FAILURE = 'EDIT_TEAM_NEWS_FAILURE';
export const UPDATE_TEAM_NEWS_REQUEST = 'UPDATE_TEAM_NEWS_REQUEST';
export const UPDATE_TEAM_NEWS_SUCCESS = 'UPDATE_TEAM_NEWS_SUCCESS';
export const UPDATE_TEAM_NEWS_LIST_SUCCESS = 'UPDATE_TEAM_NEWS_LIST_SUCCESS';
export const UPDATE_TEAM_NEWS_FAILURE = 'UPDATE_TEAM_NEWS_FAILURE';
export const DELETE_TEAM_NEWS_REQUEST = 'DELETE_TEAM_NEWS_REQUEST';
export const DELETE_TEAM_NEWS_SUCCESS = 'DELETE_TEAM_NEWS_SUCCESS';
export const DELETE_TEAM_NEWS_FAILURE = 'DELETE_TEAM_NEWS_FAILURE';
export const USER_SEARCH_REQUEST = 'USER_SEARCH_REQUEST';
export const USER_SEARCH_SUCCESS = 'USER_SEARCH_SUCCESS';
export const USER_SEARCH_FAILURE = 'USER_SEARCH_FAILURE';
export const USER_INVITE_TEAM_SUCCESS = 'USER_INVITE_TEAM_SUCCESS';
export const CREATE_TEAM_REQUEST = 'CREATE_TEAM_REQUEST';
export const CREATE_TEAM_SUCCESS = 'CREATE_TEAM_SUCCESS';
export const CREATE_TEAM_FAILURE = 'CREATE_TEAM_FAILURE';
export const UPDATE_TEAM_SUCCESS = 'UPDATE_TEAM_SUCCESS';
export const LOAD_BANNER_TEAM_REQUEST = 'LOAD_BANNER_TEAM_REQUEST';
export const LOAD_BANNER_TEAM_SUCCESS = 'LOAD_BANNER_TEAM_SUCCESS';
export const LOAD_BANNER_TEAM_FAILURE = 'LOAD_BANNER_TEAM_FAILURE';
export const UPDATE_AVATAR_SUCCESS = 'UPDATE_AVATAR_SUCCESS';
export const CHECK_LOGIN_SOCIAL_REQUEST = 'CHECK_LOGIN_SOCIAL_REQUEST';
export const CHECK_LOGIN_SOCIAL_SUCCESS = 'CHECK_LOGIN_SOCIAL_SUCCESS';
export const CHECK_LOGIN_SOCIAL_FAILURE = 'CHECK_LOGIN_SOCIAL_FAILURE';
export const REMOVE_CONNECT_REQUEST = 'REMOVE_CONNECT_REQUEST';
export const REMOVE_CONNECT_SUCCESS = 'REMOVE_CONNECT_SUCCESS';
export const REMOVE_CONNECT_FAILURE = 'REMOVE_CONNECT_FAILURE';
export const UPDATE_PROFILE_REQUEST = 'UPDATE_PROFILE_REQUEST';
export const UPDATE_PROFILE_SUCCESS = 'UPDATE_PROFILE_SUCCESS';
export const UPDATE_PROFILE_FAILURE = 'UPDATE_PROFILE_FAILURE';
export const VERIFY_EMAIL_REQUEST = 'VERIFY_EMAIL_REQUEST';
export const VERIFY_EMAIL_SUCCESS = 'VERIFY_EMAIL_SUCCESS';
export const VERIFY_EMAIL_FAILURE = 'VERIFY_EMAIL_FAILURE';
export const UPDATE_SETTING_REQUEST = 'UPDATE_SETTING_REQUEST';
export const UPDATE_SETTING_SUCCESS = 'UPDATE_SETTING_SUCCESS';
export const UPDATE_SETTING_FAILURE = 'UPDATE_SETTING_FAILURE';

export const EVENT_LIST_REQUEST = 'EVENT_LIST_REQUEST';
export const EVENT_LIST_SUCCESS = 'EVENT_LIST_SUCCESS';
export const EVENT_LIST_FAILURE = 'EVENT_LIST_FAILURE';

export const VENUE_LIST_REQUEST = 'VENUE_LIST_REQUEST';
export const VENUE_LIST_SUCCESS = 'VENUE_LIST_SUCCESS';
export const VENUE_LIST_FAILURE = 'VENUE_LIST_FAILURE';

export const DELETE_VENUE_NEWS_REQUEST = 'DELETE_VENUE_NEWS_REQUEST';
export const DELETE_VENUE_NEWS_SUCCESS = 'DELETE_VENUE_NEWS_SUCCESS';
export const DELETE_VENUE_NEWS_FAILURE = 'DELETE_VENUE_NEWS_FAILURE';

export const EDIT_VENUE_NEWS_REQUEST = 'EDIT_VENUE_NEWS_REQUEST';
export const EDIT_VENUE_NEWS_SUCCESS = 'EDIT_VENUE_NEWS_SUCCESS';
export const EDIT_VENUE_NEWS_FAILURE = 'EDIT_VENUE_NEWS_FAILURE';

export const UPDATE_VENUE_NEWS_REQUEST = 'UPDATE_VENUE_NEWS_REQUEST';
export const UPDATE_VENUE_NEWS_SUCCESS = 'UPDATE_VENUE_NEWS_SUCCESS';
export const UPDATE_VENUE_NEWS_FAILURE = 'UPDATE_VENUE_NEWS_FAILURE';
export const UPDATE_VENUE_NEWS_LIST_SUCCESS = 'UPDATE_VENUE_NEWS_LIST_SUCCESS';

export const VENUEMAP_LIST_REQUEST = 'VENUEMAP_LIST_REQUEST';
export const VENUEMAP_LIST_SUCCESS = 'VENUEMAP_LIST_SUCCESS';
export const VENUEMAP_LIST_FAILURE = 'VENUEMAP_LIST_FAILURE';
export const VENUEMAP_CLEAR = 'VENUEMAP_CLEAR';

export const VENUE_DETAIL_REQUEST = 'VENUE_DETAIL_REQUEST';
export const VENUE_DETAIL_SUCCESS = 'VENUE_DETAIL_SUCCESS';
export const VENUE_DETAIL_FAILURE = 'VENUE_DETAIL_FAILURE';

export const VENUE_UPLOAD_REQUEST = 'VENUE_UPLOAD_REQUEST';
export const VENUE_UPLOAD_SUCCESS = 'VENUE_UPLOAD_SUCCESS';
export const VENUE_UPLOAD_FAILURE = 'VENUE_UPLOAD_FAILURE';

export const VENUE_PHOTO_REQUEST = 'VENUE_PHOTO_REQUEST';
export const VENUE_PHOTO_SUCCESS = 'VENUE_PHOTO_SUCCESS';
export const VENUE_PHOTO_CLEAR = 'VENUE_PHOTO_CLEAR';
export const VENUE_PHOTO_FAILURE = 'VENUE_PHOTO_FAILURE';

export const VENUE_PHOTO_DETAIL_REQUEST = 'VENUE_PHOTO_DETAIL_REQUEST';
export const VENUE_PHOTO_DETAIL_SUCCESS = 'VENUE_PHOTO_DETAIL_SUCCESS';
export const VENUE_PHOTO_DETAIL_FAILURE = 'VENUE_PHOTO_FAILURE';

export const VENUE_DELETE_PHOTO_REQUEST = 'VENUE_DELETE_PHOTO_REQUEST';
export const VENUE_DELETE_PHOTO_SUCCESS = 'VENUE_DELETE_PHOTO_SUCCESS';
export const VENUE_DELETE_PHOTO_FAILURE = 'VENUE_DELETE_PHOTO_FAILURE';

export const BANNER_LIST_REQUEST = 'BANNER_LIST_REQUEST';
export const BANNER_LIST_SUCCESS = 'BANNER_LIST_SUCCESS';
export const BANNER_LIST_FAILURE = 'BANNER_LIST_FAILURE';
export const CREATE_ABOUT_TEAM_REQUEST = 'CREATE_ABOUT_TEAM_REQUEST';
export const CREATE_ABOUT_TEAM_SUCCESS = 'CREATE_ABOUT_TEAM_SUCCESS';
export const CREATE_ABOUT_TEAM_FAILURE = 'CREATE_ABOUT_TEAM_FAILURE';
export const LOAD_DATA_EVENT_BY_TEAM_REQUEST =
  'LOAD_DATA_EVENT_BY_TEAM_REQUEST';
export const LOAD_DATA_EVENT_BY_TEAM_SUCCESS =
  'LOAD_DATA_EVENT_BY_TEAM_SUCCESS';
export const LOAD_DATA_EVENT_BY_TEAM_FAILURE =
  'LOAD_DATA_EVENT_BY_TEAM_FAILURE';
export const LOAD_DATA_EVENT_BY_VENUE_REQUEST =
  'LOAD_DATA_EVENT_BY_VENUE_REQUEST';
export const LOAD_DATA_EVENT_BY_VENUE_SUCCESS =
  'LOAD_DATA_EVENT_BY_VENUE_SUCCESS';
export const LOAD_DATA_EVENT_BY_VENUE_FAILURE =
  'LOAD_DATA_EVENT_BY_VENUE_FAILURE';
export const LOAD_DATA_EVENT_BY_VENUE_CLEAR = 'LOAD_DATA_EVENT_BY_VENUE_CLEAR';
export const LOAD_DATA_VENUE_BY_TEAM_REQUEST =
  'LOAD_DATA_VENUE_BY_TEAM_REQUEST';
export const LOAD_DATA_VENUE_BY_TEAM_SUCCESS =
  'LOAD_DATA_VENUE_BY_TEAM_SUCCESS';
export const LOAD_DATA_VENUE_BY_TEAM_FAILURE =
  'LOAD_DATA_VENUE_BY_TEAM_FAILURE';
export const LIKE_EVENT_IN_TEAM_REQUEST = 'LIKE_EVENT_IN_TEAM_REQUEST';
export const LIKE_EVENT_IN_TEAM_SUCCESS = 'LIKE_EVENT_IN_TEAM_SUCCESS';
export const LIKE_EVENT_IN_TEAM_FAILURE = 'LIKE_EVENT_IN_TEAM_FAILURE';
export const LOAD_DATA_SALE_REPORT_REQUEST = 'LOAD_DATA_SALE_REPORT_REQUEST';
export const LOAD_DATA_SALE_REPORT_SUCCESS = 'LOAD_DATA_SALE_REPORT_SUCCESS';
export const LOAD_DATA_SALE_REPORT_FAILURE = 'LOAD_DATA_SALE_REPORT_FAILURE';
export const LIKE_EVENT_IN_SEARCH_REQUEST = 'LIKE_EVENT_IN_SEARCH_REQUEST';
export const LIKE_EVENT_IN_SEARCH_SUCCESS = 'LIKE_EVENT_IN_SEARCH_SUCCESS';
export const LIKE_EVENT_IN_SEARCH_FAILURE = 'LIKE_EVENT_IN_SEARCH_FAILURE';

// event

export const LOAD_INVITE_TEAM_MEMBERS = 'LOAD_INVITE_TEAM_MEMBERS';
export const LOAD_INVITE_TEAM_MEMBERS_SUCCESS =
  'LOAD_INVITE_TEAM_MEMBERS_SUCCESS';
export const LOAD_INVITE_TEAM_MEMBERS_FAILURE =
  'LOAD_INVITE_TEAM_MEMBERS_FAILURE';
export const LOAD_INVITE_TEAM_FOLLOWERS = 'LOAD_INVITE_TEAM_FOLLOWERS';
export const LOAD_INVITE_TEAM_FOLLOWERS_SUCCESS =
  'LOAD_INVITE_TEAM_FOLLOWERS_SUCCESS';
export const LOAD_INVITE_TEAM_FOLLOWERS_FAILURE =
  'LOAD_INVITE_TEAM_FOLLOWERS_FAILURE';
export const LOAD_INVITE_TEAM_SEARCH = 'LOAD_INVITE_TEAM_SEARCH';
export const LOAD_INVITE_TEAM_SEARCH_SUCCESS =
  'LOAD_INVITE_TEAM_SEARCH_SUCCESS';
export const LOAD_INVITE_TEAM_SEARCH_FAILURE =
  'LOAD_INVITE_TEAM_SEARCH_FAILURE';
export const CLEAR_INVITE_EVENT = 'CLEAR_INVITE_EVENT';
export const INVITE_USER_REQUEST = 'INVITE_USER_REQUEST';
export const INVITE_USER_SUCCESS = 'INVITE_USER_SUCCESS';
export const INVITE_USER_FAILURE = 'INVITE_USER_FAILURE';
export const UNINVITE_USER_REQUEST = 'UNINVITE_USER_REQUEST';
export const UNINVITE_USER_SUCCESS = 'UNINVITE_USER_SUCCESS';
export const UNINVITE_USER_FAILURE = 'UNINVITE_USER_FAILURE';

export const LOAD_DATA_HOST_INFO_REQUEST = 'LOAD_DATA_HOST_INFO_REQUEST';
export const LOAD_DATA_HOST_INFO_SUCCESS = 'LOAD_DATA_HOST_INFO_SUCCESS';
export const LOAD_DATA_HOST_INFO_FAILURE = 'LOAD_DATA_HOST_INFO_FAILURE';
export const LOAD_DATA_LOCATION_REQUEST = 'LOAD_DATA_LOCATION_REQUEST';
export const LOAD_DATA_LOCATION_SUCCESS = 'LOAD_DATA_LOCATION_SUCCESS';
export const LOAD_DATA_LOCATION_FAILURE = 'LOAD_DATA_LOCATION_FAILURE';
export const LOAD_DATA_VENUE_REQUEST = 'LOAD_DATA_VENUE_REQUEST';
export const LOAD_DATA_VENUE_SUCCESS = 'LOAD_DATA_VENUE_SUCCESS';
export const LOAD_DATA_VENUE_FAILURE = 'LOAD_DATA_VENUE_FAILURE';
export const LOAD_DATA_CATEGORY_REQUEST = 'LOAD_DATA_CATEGORY_REQUEST';
export const LOAD_DATA_CATEGORY_SUCCESS = 'LOAD_DATA_CATEGORY_SUCCESS';
export const LOAD_DATA_CATEGORY_FAILURE = 'LOAD_DATA_CATEGORY_FAILURE';
export const LOAD_DATA_LIST_EVENT_REQUEST = 'LOAD_DATA_LIST_EVENT_REQUEST';
export const LOAD_DATA_LIST_EVENT_SUCCESS = 'LOAD_DATA_LIST_EVENT_SUCCESS';
export const LOAD_DATA_LIST_EVENT_FAILURE = 'LOAD_DATA_LIST_EVENT_FAILURE';
// 
export const LOAD_DATA_EVENT_DETAIL_REQUEST = 'LOAD_DATA_EVENT_DETAIL_REQUEST';
export const LOAD_DATA_EVENT_DETAIL_SUCCESS = 'LOAD_DATA_EVENT_DETAIL_SUCCESS';
export const LOAD_DATA_EVENT_DETAIL_FAILURE = 'LOAD_DATA_EVENT_DETAIL_FAILURE';
export const LOAD_DATA_EVENT_NEWS_DETAIL_REQUEST =
  'LOAD_DATA_EVENT_NEWS_DETAIL_REQUEST';
export const LOAD_DATA_EVENT_NEWS_DETAIL_SUCCESS =
  'LOAD_DATA_EVENT_NEWS_DETAIL_SUCCESS';
export const LOAD_DATA_EVENT_NEWS_DETAIL_FAILURE =
  'LOAD_DATA_EVENT_NEWS_DETAIL_FAILURE';
export const LOAD_DATA_EVENT_NEWS_REQUEST = 'LOAD_DATA_EVENT_NEWS_REQUEST';
export const LOAD_DATA_EVENT_NEWS_SUCCESS = 'LOAD_DATA_EVENT_NEWS_SUCCESS';
export const LOAD_DATA_EVENT_NEWS_FAILURE = 'LOAD_DATA_EVENT_NEWS_FAILURE';
export const LOAD_DATA_VENUE_NEWS_REQUEST = 'LOAD_DATA_VENUE_NEWS_REQUEST';
export const LOAD_DATA_VENUE_NEWS_SUCCESS = 'LOAD_DATA_VENUE_NEWS_SUCCESS';
export const LOAD_DATA_VENUE_NEWS_FAILURE = 'LOAD_DATA_VENUE_NEWS_FAILURE';
export const CLEAR_INVITE_SEARCH = 'CLEAR_INVITE_SEARCH';
export const LIKE_EVENT_REQUEST = 'LIKE_EVENT_REQUEST';
export const LIKE_EVENT_SUCCESS = 'LIKE_EVENT_SUCCESS';
export const LIKE_EVENT_FAILURE = 'LIKE_EVENT_FAILURE';
export const UPDATE_LIKED_VENUE = 'UPDATE_LIKED_VENUE';
export const UPDATE_LIKED_VENUE_HOME = 'UPDATE_LIKED_VENUE_HOME';

export const LOAD_DATA_LIKE_EVENT_REQUEST = 'LOAD_DATA_LIKE_EVENT_REQUEST';
export const LOAD_DATA_LIKE_EVENT_SUCCESS = 'LOAD_DATA_LIKE_EVENT_SUCCESS';
export const LOAD_DATA_LIKE_EVENT_FAILURE = 'LOAD_DATA_LIKE_EVENT_FAILURE';
export const LOAD_DATA_PAST_EVENT_LIKE_REQUEST =
  'LOAD_DATA_PAST_EVENT_LIKE_REQUEST';
export const LOAD_DATA_PAST_EVENT_LIKE_SUCCESS =
  'LOAD_DATA_PAST_EVENT_LIKE_SUCCESS';
export const LOAD_DATA_PAST_EVENT_LIKE_FAILURE =
  'LOAD_DATA_PAST_EVENT_LIKE_FAILURE';
export const LOAD_DATA_COLLABORATING_TEAM_REQUEST =
  'LOAD_DATA_COLLABORATING_TEAM_REQUEST';
export const LOAD_DATA_COLLABORATING_TEAM_SUCCESS =
  'LOAD_DATA_COLLABORATING_TEAM_SUCCESS';
export const LOAD_DATA_COLLABORATING_TEAM_FAILURE =
  'LOAD_DATA_COLLABORATING_TEAM_FAILURE';
export const CONFIRM_COLLABORATE_REQUEST = 'CONFIRM_COLLABORATE_REQUEST';
export const CONFIRM_COLLABORATE_SUCCESS = 'CONFIRM_COLLABORATE_SUCCESS';
export const CONFIRM_COLLABORATE_FAILURE = 'CONFIRM_COLLABORATE_FAILURE';
export const DISMISS_COLLABORATE_SUCCESS = 'DISMISS_COLLABORATE_SUCCESS';
export const DELETE_COLLABORATE_SUCCESS = 'DELETE_COLLABORATE_SUCCESS';
export const LOAD_DATA_TEAM_APPLY_REQUEST = 'LOAD_DATA_TEAM_APPLY_REQUEST';
export const LOAD_DATA_TEAM_APPLY_SUCCESS = 'LOAD_DATA_TEAM_APPLY_SUCCESS';
export const LOAD_DATA_TEAM_APPLY_FAILURE = 'LOAD_DATA_TEAM_APPLY_FAILURE';
export const APPLY_COLLABORATE_REQUEST = 'APPLY_COLLABORATE_REQUEST';
export const APPLY_COLLABORATE_SUCCESS = 'APPLY_COLLABORATE_SUCCESS';
export const APPLY_COLLABORATE_FAILURE = 'APPLY_COLLABORATE_FAILURE';
export const SAVE_EVENT_REQUEST = 'SAVE_EVENT_REQUEST';
export const SAVE_EVENT_SUCCESS = 'SAVE_EVENT_SUCCESS';
export const SAVE_EVENT_FAILURE = 'SAVE_EVENT_FAILURE';
export const UPDATE_EVENT_SUCCESS = 'UPDATE_EVENT_SUCCESS';
export const EVENT_DETAIL_TEAM_FOLLOW_SUCCESS =
  'EVENT_DETAIL_TEAM_FOLLOW_SUCCESS';
export const LOAD_DATA_EVENT_IMPORT_REQUEST = 'LOAD_DATA_EVENT_IMPORT_REQUEST';
export const LOAD_DATA_EVENT_IMPORT_SUCCESS = 'LOAD_DATA_EVENT_IMPORT_SUCCESS';
export const LOAD_DATA_EVENT_IMPORT_FAILURE = 'LOAD_DATA_EVENT_IMPORT_FAILURE';
export const ADD_TEAM_TO_IMPORT = 'ADD_TEAM_TO_IMPORT';
export const SAVE_EVENT_NEWS_REQUEST = 'SAVE_EVENT_NEWS_REQUEST';
export const SAVE_EVENT_NEWS_SUCCESS = 'SAVE_EVENT_NEWS_SUCCESS';
export const SAVE_EVENT_NEWS_FAILURE = 'SAVE_EVENT_NEWS_FAILURE';
export const SAVE_VENUE_NEWS_REQUEST = 'SAVE_VENUE_NEWS_REQUEST';
export const SAVE_VENUE_NEWS_SUCCESS = 'SAVE_VENUE_NEWS_SUCCESS';
export const SAVE_VENUE_NEWS_FAILURE = 'SAVE_VENUE_NEWS_FAILURE';
export const EDIT_EVENT_REQUEST = 'EDIT_EVENT_REQUEST';
export const EDIT_EVENT_SUCCESS = 'EDIT_EVENT_SUCCESS';
export const EDIT_EVENT_FAILURE = 'EDIT_EVENT_FAILURE';
export const EVENT_DELETE_REQUEST = 'EVENT_DELETE_REQUEST';
export const EVENT_DELETE_SUCCESS = 'EVENT_DELETE_SUCCESS';
export const EVENT_DELETE_FAILURE = 'EVENT_DELETE_FAILURE';
export const EVENT_CANCEL_REQUEST = 'EVENT_CANCEL_REQUEST';
export const EVENT_CANCEL_SUCCESS = 'EVENT_CANCEL_SUCCESS';
export const EVENT_CANCEL_FAILURE = 'EVENT_CANCEL_FAILURE';
export const SELECT_CITY_REQUEST = 'SELECT_CITY_REQUEST';
export const SELECT_CITY_SUCCESS = 'SELECT_CITY_SUCCESS';
export const SELECT_CITY_FAILURE = 'SELECT_CITY_FAILURE';
export const SELECT_COUNTRY_REQUEST = 'SELECT_COUNTRY_REQUEST';
export const SELECT_COUNTRY_SUCCESS = 'SELECT_COUNTRY_SUCCESS';
export const SELECT_COUNTRY_EVENT_SUCCESS = 'SELECT_COUNTRY_EVENT_SUCCESS';
export const SELECT_COUNTRY_FAILURE = 'SELECT_COUNTRY_FAILURE';
export const GET_DISTRICT_BY_CITY_REQUEST = 'GET_DISTRICT_BY_CITY_REQUEST';
export const GET_DISTRICT_BY_CITY_SUCCESS = 'GET_DISTRICT_BY_CITY_SUCCESS';
export const GET_DISTRICT_BY_CITY_FAILURE = 'GET_DISTRICT_BY_CITY_FAILURE';
export const EVENT_REPORT_REQUEST = 'EVENT_REPORT_REQUEST';
export const EVENT_REPORT_SUCCESS = 'EVENT_REPORT_SUCCESS';
export const EVENT_REPORT_FAILURE = 'EVENT_REPORT_FAILURE';
export const LIKE_EVENT_NEWS_REQUEST = 'LIKE_EVENT_NEWS_REQUEST';
export const LIKE_EVENT_NEWS_SUCCESS = 'LIKE_EVENT_NEWS_SUCCESS';
export const LIKE_EVENT_NEWS_FAILURE = 'LIKE_EVENT_NEWS_FAILURE';
export const UPDATE_LIKE_EVENT_NEWS_REQUEST = 'UPDATE_LIKE_EVENT_NEWS_REQUEST';
export const UPDATE_LIKE_EVENT_NEWS_FAILURE = 'UPDATE_LIKE_EVENT_NEWS_FAILURE';
export const EDIT_EVENT_NEWS_REQUEST = 'EDIT_EVENT_NEWS_REQUEST';
export const EDIT_EVENT_NEWS_SUCCESS = 'EDIT_EVENT_NEWS_SUCCESS';
export const EDIT_EVENT_NEWS_FAILURE = 'EDIT_EVENT_NEWS_FAILURE';
export const UPDATE_EVENT_NEWS_REQUEST = 'UPDATE_EVENT_NEWS_REQUEST';
export const UPDATE_EVENT_NEWS_SUCCESS = 'UPDATE_EVENT_NEWS_SUCCESS';
export const UPDATE_EVENT_NEWS_FAILURE = 'UPDATE_EVENT_NEWS_FAILURE';
export const UPDATE_EVENT_NEWS_LIST_SUCCESS = 'UPDATE_EVENT_NEWS_LIST_SUCCESS';
export const DELETE_EVENT_NEWS_REQUEST = 'DELETE_EVENT_NEWS_REQUEST';
export const DELETE_EVENT_NEWS_SUCCESS = 'DELETE_EVENT_NEWS_SUCCESS';
export const DELETE_EVENT_NEWS_FAILURE = 'DELETE_EVENT_NEWS_FAILURE';
export const LOAD_EVENT_BY_MAPS_REQUEST = 'LOAD_EVENT_BY_MAPS_REQUEST';
export const LOAD_EVENT_BY_MAPS_SUCCESS = 'LOAD_EVENT_BY_MAPS_SUCCESS';
export const LOAD_EVENT_BY_MAPS_FAILURE = 'LOAD_EVENT_BY_MAPS_FAILURE';
export const LOAD_BANNER_EVENT_REQUEST = 'LOAD_BANNER_EVENT_REQUEST';
export const LOAD_BANNER_EVENT_SUCCESS = 'LOAD_BANNER_EVENT_SUCCESS';
export const LOAD_BANNER_EVENT_FAILURE = 'LOAD_BANNER_EVENT_FAILURE';
export const LOAD_DATA_EVENT_TICKETS_REQUEST =
  'LOAD_DATA_EVENT_TICKETS_REQUEST';
export const LOAD_DATA_EVENT_TICKETS_SUCCESS =
  'LOAD_DATA_EVENT_TICKETS_SUCCESS';
export const LOAD_DATA_EVENT_TICKETS_FAILURE =
  'LOAD_DATA_EVENT_TICKETS_FAILURE';
export const GET_TICKET_REQUEST = 'GET_TICKET_REQUEST';
export const GET_TICKET_SUCCESS = 'GET_TICKET_SUCCESS';
export const GET_TICKET_FAILURE = 'GET_TICKET_FAILURE';
export const GET_TICKET_FAILURE_NOT_REFRESH = 'GET_TICKET_FAILURE_NOT_REFRESH';
export const LOAD_DATA_PURCHASE_REQUEST = 'LOAD_DATA_PURCHASE_REQUEST';
export const LOAD_DATA_PURCHASE_SUCCESS = 'LOAD_DATA_PURCHASE_SUCCESS';
export const LOAD_DATA_PURCHASE_FAILURE = 'LOAD_DATA_PURCHASE_FAILURE';
export const LOAD_DATA_MY_TICKET_REQUEST = 'LOAD_DATA_MY_TICKET_REQUEST';
export const LOAD_DATA_MY_TICKET_SUCCESS = 'LOAD_DATA_MY_TICKET_SUCCESS';
export const LOAD_DATA_MY_TICKET_FAILURE = 'LOAD_DATA_MY_TICKET_FAILURE';
export const LOAD_DATA_MY_PAST_TICKET_REQUEST =
  'LOAD_DATA_MY_PAST_TICKET_REQUEST';
export const LOAD_DATA_MY_PAST_TICKET_SUCCESS =
  'LOAD_DATA_MY_PAST_TICKET_SUCCESS';
export const LOAD_DATA_MY_PAST_TICKET_FAILURE =
  'LOAD_DATA_MY_PAST_TICKET_FAILURE';
export const LOAD_DATA_TICKET_PURCHASED_REQUEST =
  'LOAD_DATA_TICKET_PURCHASED_REQUEST';
export const LOAD_DATA_TICKET_PURCHASED_SUCCESS =
  'LOAD_DATA_TICKET_PURCHASED_SUCCESS';
export const LOAD_DATA_TICKET_PURCHASED_FAILURE =
  'LOAD_DATA_TICKET_PURCHASED_FAILURE';
export const LOAD_DATA_HOST_INFO_BY_TEAM_REQUEST =
  'LOAD_DATA_HOST_INFO_BY_TEAM_REQUEST';
export const LOAD_DATA_HOST_INFO_BY_TEAM_SUCCESS =
  'LOAD_DATA_HOST_INFO_BY_TEAM_SUCCESS';
export const LOAD_DATA_HOST_INFO_BY_TEAM_FAILURE =
  'LOAD_DATA_HOST_INFO_BY_TEAM_FAILURE';

export const DELETE_VENUE = 'DELETE_VENUE';

