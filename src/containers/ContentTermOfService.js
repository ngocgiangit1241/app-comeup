import React, { useState, useEffect } from 'react';
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window')
import Modal from "react-native-modal";
import SrcView from '../screens_view/ScrView';
import { useSelector } from "react-redux";
import { convertLanguage } from '../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { actCheckAllPolicy } from '../actions/user'

const scale = width / 360
export default function ContentTermOfService(props) {
    const { language } = useSelector(s => s.language)
    const [checkBoxAll, setCheckBoxAll] = useState(false)
    const [checkBox, setCheckBox] = useState({
        checkBox1: false,
        checkBox2: false,
        // checkBox3: false,
        // checkBox4: false,
    })

    const onContinue = async () => {
        await actCheckAllPolicy()
        await props.setModalTermOfService()
    }

    useEffect(() => {
        if (!checkBox.checkBox1 || !checkBox.checkBox2
            //  || !checkBox.checkBox3 || !checkBox.checkBox4
        ) {
            setCheckBoxAll(false)
        }
        if (checkBox.checkBox1 && checkBox.checkBox2
            // && checkBox.checkBox3 && checkBox.checkBox4
        ) {
            setCheckBoxAll(true)
        }
    }, [checkBox])

    const actSetCheckBoxAll = () => {
        setCheckBoxAll(!checkBoxAll)
        if (!checkBoxAll) {
            setCheckBox({
                checkBox1: true,
                checkBox2: true,
                // checkBox3: true,
                // checkBox4: true,
            })
        } else {
            setCheckBox({
                checkBox1: false,
                checkBox2: false,
                // checkBox3: false,
                // checkBox4: false,
            })
        }
    }
    useEffect(() => {
        if (props?.getCheckBox) {
            props?.getCheckBox(checkBoxAll)
        }
    }, [checkBoxAll])
    return (
        <View style={[styles.content, { marginTop: props?.setModalTermOfService ? 30 * scale : -30 * scale }]}>
            {props?.setModalTermOfService && <Text style={{ fontWeight: 'bold', color: '#333333', fontSize: 20 * scale, lineHeight: 25 * scale, textAlign: 'center' }}>{convertLanguage(language, 'welcome_to_join_us')}</Text>}
            <Text style={{ marginTop: 18 * scale, fontWeight: 'bold', color: '#333333', lineHeight: 20 * scale, textAlign: 'center', fontSize: 13 * scale }}>{convertLanguage(language, 'useComeup')}</Text>
            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginTop: 12 * scale, marginHorizontal: 2 * scale }} onPress={() => actSetCheckBoxAll()}>
                <Image source={checkBoxAll ? require('../assets/step3CheckBox2.png') : require('../assets/step3CheckBox1.png')} style={{ height: 20 * scale, width: 20 * scale, marginRight: 8 * scale }} resizeMode="contain" />
                <Text style={{ color: '#333333', fontSize: 13 * scale, flex: 1 }}>{convertLanguage(language, 'read_and_agree')}</Text>
            </TouchableOpacity>
            <View style={{ borderColor: 'rgba(51, 51, 51, 0.25)', borderWidth: 1, borderRadius: 4, paddingHorizontal: 15 * scale, paddingVertical: 10 * scale, width: '100%', alignSelf: 'flex-start', marginTop: 13 * scale }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingBottom: 4 * scale }}>
                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} onPress={() => setCheckBox({ ...checkBox, checkBox1: !checkBox.checkBox1 })}>
                        <Image source={checkBox.checkBox1 ? require('../assets/step3CheckBox2.png') : require('../assets/step3CheckBox1.png')} style={{ height: 20 * scale, width: 20 * scale, marginRight: 8 * scale, paddingBottom: 30 * scale }} resizeMode="contain" />
                        <Text style={{ color: '#333333', fontSize: 13 * scale }}>{convertLanguage(language, 'terms_of_service')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { props?.setModalTermOfService(), props?.openTOS() }}>
                        <Image source={require('../assets/right.png')} style={{ height: 18 * scale, width: 20 * scale, tintColor: 'black' }} resizeMode="contain" />
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingBottom: 4 * scale }}>
                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} onPress={() => setCheckBox({ ...checkBox, checkBox2: !checkBox.checkBox2 })}>
                        <Image source={checkBox.checkBox2 ? require('../assets/step3CheckBox2.png') : require('../assets/step3CheckBox1.png')} style={{ height: 20 * scale, width: 20 * scale, marginRight: 8 * scale, paddingBottom: 30 * scale }} resizeMode="contain" />
                        <Text style={{ color: '#333333', fontSize: 13 * scale }}>{convertLanguage(language, 'privacy_policy')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { props?.setModalTermOfService(), props?.openPP() }}>
                        <Image source={require('../assets/right.png')} style={{ height: 18 * scale, width: 20 * scale, tintColor: 'black' }} resizeMode="contain" />
                    </TouchableOpacity>
                </View>
                {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingBottom: 4 * scale }}>
                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} onPress={() => setCheckBox({ ...checkBox, checkBox3: !checkBox.checkBox3 })}>
                        <Image source={checkBox.checkBox3 ? require('../assets/step3CheckBox2.png') : require('../assets/step3CheckBox1.png')} style={{ height: 20 * scale, width: 20 * scale, marginRight: 8 * scale, paddingBottom: 30 * scale }} resizeMode="contain" />
                        <Text style={{ color: '#333333', fontSize: 13 * scale }}>Lorem ipsum dolor</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image source={require('../assets/right.png')} style={{ height: 18 * scale, width: 20 * scale, tintColor: 'black' }} resizeMode="contain" />
                    </TouchableOpacity>
                </View> */}
                {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} onPress={() => setCheckBox({ ...checkBox, checkBox4: !checkBox.checkBox4 })}>
                        <Image source={checkBox.checkBox4 ? require('../assets/step3CheckBox2.png') : require('../assets/step3CheckBox1.png')} style={{ height: 20 * scale, width: 20 * scale, marginRight: 8 * scale, paddingBottom: 30 * scale }} resizeMode="contain" />
                        <Text style={{ color: '#333333', fontSize: 13 * scale }}>Lorem ipsum dolor</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image source={require('../assets/right.png')} style={{ height: 18 * scale, width: 20 * scale, tintColor: 'black' }} resizeMode="contain" />
                    </TouchableOpacity>
                </View> */}
            </View>
            {props.setModalTermOfService &&
                <View style={{ marginTop: 22 * scale, flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
                    <TouchableOpacity style={{ borderColor: '#4F4F4F', borderRadius: 4 * scale, borderWidth: 1, width: 140 * scale, alignItems: 'center' }} onPress={props.setModalTermOfService}>
                        <Text style={{ color: '#4F4F4F', fontSize: 14 * scale, fontWeight: 'normal', lineHeight: 20 * scale, paddingVertical: 5 * scale }}>{convertLanguage(language, 'ask_me_later')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ borderRadius: 4 * scale, width: 140 * scale, alignItems: 'center', backgroundColor: !checkBoxAll ? '#DFDFDF' : '#00A9F4' }} disabled={!checkBoxAll} onPress={onContinue}>
                        <Text style={{ color: !checkBoxAll ? '#B3B8BC' : 'white', fontSize: 14 * scale, fontWeight: 'normal', lineHeight: 20 * scale, paddingVertical: 5 * scale }}>{convertLanguage(language, 'continue')}</Text>
                    </TouchableOpacity>
                </View>
            }
            {/* <Image source={state.checkbox ? require('../../assets/step3CheckBox2.png') : require('../../assets/step3CheckBox1.png')} style={styles.ic_radio} resizeMode="contain" */}
        </View>
    )
}
const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        // height: 300 * scale,
        width: '100%',
        // marginLeft: 50 * scale,
        // marginRight: 50 * scale,
        borderRadius: 5 * scale,
        alignSelf: 'center',
    },
    container: {

    },
    content: {
        // padding: 22 * scale,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10 * scale,
        alignItems: 'center',
        textAlign: 'center',
        marginHorizontal: 15 * scale,
        marginBottom: 20 * scale
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnClose: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    txtSelectCountry: {
        fontSize: 15,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold',
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        textAlign: 'center'
    },
    txtCountry: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15,
    },
    boxChildren: {
        paddingTop: 15,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        width: 250,
        alignItems: 'center'
    },
    txtCity: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 15,
    }
});
