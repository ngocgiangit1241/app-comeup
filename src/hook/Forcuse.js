import React, { useEffect } from 'react'
import { View, Text } from 'react-native'
import { useIsFocused } from '@react-navigation/native';

export default function Forcuse(props) {
    const isFocused = useIsFocused();
    useEffect(() => {
        props.focuse(isFocused)
    }, [isFocused])
    return null
}
