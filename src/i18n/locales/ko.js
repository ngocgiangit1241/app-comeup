export default {
  home: '홈',
  event: '이벤트',
  event_news: '이벤트 뉴스',
  related_event: '관련 이벤트',
  venue: '베뉴',
  team: '호스트',
  host_team: '호스트',
  collaboration_team: '협업 호스트',
  apply_collaborate: '이벤트 협업 요청',
  liked: '좋아요',
  ticket: '티켓',
  tickets: '티켓',
  news: '알림',
  profile: '프로필',
  comeup_app: '컴업을 더 편하게 즐기는 방법!',
  let_register: '즐거움을 나누세요!',
  register: '등록하기',
  we_like_party: '즐거운 일상으로 컴업!',
  connect_google: '구글 간편 로그인/회원가입',
  connect_facebook: '페이스북 간편 로그인/회원가입',
  login: '로그인',
  later: '나중에 하기',
  free_create: '누구나 자유롭게 이벤트, 베뉴, 호스트 정보를 무료로 등록하고 홍보할 수 있습니다.',
  register_event: '이벤트 등록',
  register_event_venue: '이벤트 등록하기',
  register_venue: '베뉴 등록',
  register_team: '호스트 등록',
  guide: '등록 가이드',
  select_host_team: '호스트 계정 선택',
  create_new_team: '새 호스트 계정 만들기',
  previous: '이전',
  next: '다음',
  search: '검색',
  more_team: '더 많은 팀',
  more_venue: '더 많은 장소',
  more_event: '더 많은 이벤트',
  set_your_id: 'ID 설정',
  note_login_with_id:
    '지금 바로 프로필을 완성하고 \n 컴업을 더 다양하게 경험해 보세요!',
  name: '이름',
  id: 'ID',
  check: '체크',
  email: '이메일',
  gender: '성별',
  birthday: '생년월일',
  language: '언어',
  sns: 'SNS',
  ok: '확인',
  male: '남성',
  female: '여성',
  avatar: '아바타',
  cancel: '취소',
  change: '변경',
  id_or_email: 'ID 또는 이메일',
  success: '성공',
  failed: '실패',
  save: '저장',
  save2: '저장하기',
  ticket_holder_list: '예매자 리스트',
  ticket_scanning: '티켓 스캐너',
  ticket_sales: '티켓 판매',
  refund_request: '환불 요청',
  withdraw: '정산',
  staff: '이벤트 관리자',
  staff2: '스태프',
  collaborating_team: '협업 호스트 관리',
  collaborating_team2: '협업 호스트 관리',
  collaborating_team_empty: '협업 호스트 관리이 없습니다.',
  edit: '수정',
  edit_map_pin: "핀 위치 수정",
  cancel_delete: '취소/삭제',
  report: '신고/건의',
  report2: '제출',
  edit_team_profile: '호스트 프로필 수정',
  sales_report: '판매 보고서',
  delete_this_team: '호스트 계정 삭제',
  team_member: '호스트 멤버',
  about_team: '호스트 소개',
  followers: '팔로워',
  follower: '팔로워',
  read_more: '더 보기',
  read_all_news: '모두 보기',
  review: '리뷰',
  hosting_event: '호스팅 이벤트',
  hosting_venue: '호스팅 베뉴',
  delete: '삭제',
  link_instagram_feed: '인스타그램 피드 연동하기',
  write: '작성하기',
  invite_people_and_build_your_team: '호스트 계정을 함께 관리할 멤버들을 초대해 팀을 구성해 보세요!',
  result: '결과',
  pending: '대기중',
  pending_request: '협업 요청 대기',
  pending_ss: '진행 중',
  please_check_the_event_code_on_tickets: '티켓에 표시된 이벤트 코드와 해당 코드가 일치하는지 확인하세요.',
  event_code: '이벤트 코드',
  logout: '로그아웃',
  following: '팔로잉',
  follow: '팔로우',
  register_about: '팀 소개', //them gioi hieu
  contact: '연락처',
  share: '공유하기',
  share2: '공유',
  invited: '초대됨',
  deleted_success: '삭제 성공',
  incorrect_password: '잘못된 비밀번호입니다.',
  forgot_password: '비밀번호를 잊으셨나요',
  password: '비밀번호',
  confirm: '확인',
  invite_member: '멤버 초대',
  member: '멤버',
  members: '멤버',
  'sales-report': '판매 보고서',
  error_ticket_not_enough: '죄송합니다! 티켓 잔여량이 충분하지 않습니다. 수량을 다시 선택해 주세요.',
  create_your_account: '회원가입',
  incorrect_code: '유효하지 않은 코드!',
  sns_login: '간편 로그인',
  new: '최신 등록순',
  popular: '인기순',
  famous: '유명한',
  all: '모두 보기',
  view_map: '지도로 보기',
  select_a_category: '카테고리 선택',
  choose_category: '카테고리 선택',
  select_a_city: '도시 선택',
  select_a_schedule: '스케줄 선택',
  select_a_country: '국가 선택',
  // select_a_type: 'Select a type',
  today: '오늘',
  tomorrow: '내일',
  week: '이번 주',
  weekend: '이번 주말',
  custom_period: '기간 설정',
  custom: '기간 설정',
  loading_more: '더 보기',
  by_logging: '로그인 시 컴업의 개인정보 보호정책 및 서비스 이용약관에 동의하게 됩니다.',
  terms_of_service: '서비스 이용약관',
  privacy_policy: '개인정보 보호정책',

  by_logging_full_ko_start: '로그인 시 컴업의 ',
  by_logging_full_ko_mid: ' 및 ',
  by_logging_full_ko_end: '에 동의하는 것으로 간주합니다.',

  create_new_account: '새로운 계정 만들기',
  log_in: '로그인',
  sign_up: '회원가입',
  send: '전송',
  waiting: '저장 중...',
  if_the_email_10: '10분 후에도 이메일이 도착하지 않는다면 컴업 팀에게 연락해 주세요.',
  just_input_your_comup:
    // 'ComeUp ID 또는 가입 이메일을 입력하면.\n비밀번호 재설정 링크가 포함 된 이메일을 보내드립니다.\nFacebook / Google 계정을 통해 가입 한 경우 Facebook / Google 계정 이메일을 입력하십시오.\n\n* 이메일이 10분안에 도착하지 않으면 저희에게 연락하세요.\ncs@comeup.asia',
    '컴업 ID 또는 이메일을 입력하시면, 연동된 이메일로 비밀번호 재설정 링크를 보내드립니다. 만약 페이스북/구글/애플로 간편 회원가입을 진행했을 경우. 페이스북/구글/애플과 연동되어 있는 이메일을 입력해 주세요.\n\n정확한 ID/이메일을 입력했는지 확인해 주세요. 잘못된 ID/이메일인 경우 비밀번호 재설정 링크를 받을 수 없습니다.',
  // just_input_your_comup:
  //   // 'ComeUp ID 또는 가입 이메일을 입력하면.\n비밀번호 재설정 링크가 포함 된 이메일을 보내드립니다.\nFacebook / Google 계정을 통해 가입 한 경우 Facebook / Google 계정 이메일을 입력하십시오.\n\n* 이메일이 10분안에 도착하지 않으면 저희에게 연락하세요.\ncs@comeup.asia',
  //   '컴업 ID 또는 이메일을 입력하시면, 연동된 이메일로 비밀번호 재설정 링크를 보내드립니다.\n만약 페이스북/구글/애플로 간편 회원가입을 진행했을 경우.\n페이스북/구글/애플과 연동되어 있는 이메일을 입력해 주세요.\n\n* 정확한 ID/이메일을 입력했는지 확인해 주세요. ID/이메일이 잘못되었을 경우 비밀번호 재설정 링크를 받을 수 없습니다.',
  email_comeup: 'cs@comeup.asia',
  check_your_email: '메일 전송 완료',
  check_your_email2: '비밀번호 재설정 링크가 전송되었습니다. 10분 후에도 이메일이 도착하지 않는다면 컴업 팀에 연락해 주세요.\n\n cs@comeup.asia',
  'No worry': '걱정하지 마세요!',
  you_will_receive_an_email:
    '비밀번호 재설정 링크가 전송되었습니다. 10분 후까지 이메일이 확인되지 않을 경우 입력하신 ID/이메일이 올바른지 확인해 주세요.',
  my_team: '참여 호스트',
  please_fill_all_fields: '모든 항목을 입력하세요.',
  post: '포스트',
  postNews: '등록하기',
  select_the_part_text: '협업하는 분야를 선택하세요.',
  if_the_host_confirm_it_text:
    '호스트 팀이 요청을 수락하면 팀 뉴스 페이지에 해당 내용이 업데이트됩니다.',
  sure_to_delete_this_event: '이벤트를 삭제하시겠습니까?',
  after_delete_this_event:
    '이벤트가 삭제되면 상세 내용, 뉴스, 장소 등 해당 이벤트와 관련된 모든 내용이 사라집니다.',
  input_your_password_to_confirm_to_delete:
    '삭제를 진행하시려면 비밀번호를 입력하세요.',
  event_page: '이벤트 페이지',
  show_more: '더 보기',
  show_less: '숨기기',
  no_worry: '걱정하지 마세요!',
  close_map: '닫기p',
  start: '시작',
  finish: '종료',
  end: '마감',
  start_date: '시작일',
  finish_date: '종료일',
  sorry_event_done: '죄송합니다! 이벤트가 완료되었습니다.',
  sorry_event_finished: '죄송합니다! 이벤트가 종료되었습니다.',
  build_your_team: '호스트 팀 페이지를 만들어 보세요!',
  team_create_text:
    '호스트 페이지를 팀원들과 같이 관리하고 팔로워도 만들수 있어요.',
  change_avatar: '프로필 사진 변경',
  team_name: '호스트 이름',
  specialty: '카테고리',
  country: '국가',
  team_id: '호스트 ID',
  select_your_team_specialities: '호스트 카테고리 선택',
  first_select_filed_will_be_the_primary_filed:
    '최대 10개까지 선택 가능합니다. 가장 먼저 선택한 항목이 호스트의 메인 카테고리로 설정됩니다.',
  checked: '승인',
  get_code: '인증코드 받기',
  select: '설정',
  by_signing:
    '회원가입 시 컴업의 개인정보 보호정책 및 서비스 이용약관에 동의하게 됩니다.',

  by_signing_full_ko_start: '회원가입 시 컴업의 ',
  by_signing_full_ko_mid: ' 및 ',
  by_signing_full_ko_end: '에 동의하는 것으로 간주합니다.',


  or: '또는',
  or2: '아직 계정이 없다면?',
  share_on: '공유하기',
  pending_invitation: '초대 응답 대기',
  leader: '리더',
  director: '디렉터',
  manager: '매니저',
  manager_booking: '매니저',
  manager_name: '담당자 이름',
  search_name_or_id: '이름 또는 ID 검색',
  invite: '초대하기',
  sure_to_set_role: '이 멤버의 등급을 변경하시겠습니까?',
  sure_to_kick_this_member_team: '이 멤버를 호스트 멤버에서 제외하시겠습니까?',
  yes: '확인',
  no: '취소',
  no2: '아니오',
  sure_to_leave_your_team: '호스트 멤버에서 떠나시겠습니까?',
  discharge: '멤버에서 제외',
  discharge_event: '관리자에서 제외',
  set_as_a_staff: '스태프로 설정',
  set_as_a_manager: '매니저로 설정',
  set_as_a_director: '디렉터로 설정',
  set_as_a_leader: '리더로 설정',
  sure_to_delete_invite: '초대를 삭제 하시겠습니까?',
  you_must_set_the_leader_member_before_leaving_your_team:
    '떠나시기 전에 리더를 변경해 주세요!',
  list_event: '이벤트 목록',
  team_review: '팀 리뷰',
  list_venues: '장소 목록',
  are_you_sure_you_want_to_report_this_team: '건의사항을 제출하시겠습니까?',
  are_you_sure_you_want_to_report_this_venue: '건의사항을 제출하시겠습니까?',
  are_you_sure_you_want_to_report_this_event: '건의사항을 제출하시겠습니까?',
  do_you_want_to_discard_this_draft: '이 초안을 삭제 하시겠습니까?',
  do_you_want_to_delete_this_news: '뉴스를 삭제 하시겠습니까?',
  do_you_want_to_delete_this_venue: '장소 삭제 하시겠습니까?',
  remove: '삭제',
  let_comeup_know: 'ComeUp에게 알려주세요.',
  what_you_want_to_report_about_team: '호스트에 대해 건의하고 싶은 내용이 있나요?',
  what_you_want_to_report_about_event:
    '이벤트에 대해 건의하고 싶은 내용이 있나요?',
  what_you_want_to_report_about_venue: '베뉴에 대해 건의하고 싶은 내용이 있나요?',
  write_your_messages: '내용을 입력하세요.',
  team_news: '호스트 뉴스',
  code: '인증코드',
  can_not_change_later: '추후 변경 불가',
  title: '제목',
  time: '시간',
  repeat: '반복 등록',
  repeat_date: '반복일자',
  location: '위치',
  category: '카테고리',
  create_tickets: '티켓 등록하기',
  we_are_happy_with_free: '이벤트에 따라 유료 또는 무료 티켓을 자유롭게 등록하세요!',
  terms_of_selling_tickets: '티켓판매약관',
  by_creating_tickets_you_agree_to_the_terms_of_selling_tickets:
    '티켓 등록/판매 시 컴업 티켓판매약관에 동의하게 됩니다.',
  ticket_name: '티켓 이름',
  price: '가격',
  quantity: '수량',
  limit_per_order: '1회 최대 구매 수량',
  sales_period: '판매 기간',
  description: '설명',
  options: '옵션',
  ask_a_delivery_address: '배송지 요청 (실물 티켓 이용 시)',
  ask_a_customize_question: '맞춤 질문하기',
  more_info: '상세 옵션 추가',
  hide: '비공개',
  add_a_ticket: '티켓 추가',
  detail: '상세 정보',
  introduce_how_this_event_is_awesome: '이 이벤트가 얼마나 멋진 지 소개하세요.',
  previous_location: '이전 등록 장소',
  city: '국가/도시',
  venue_name: '베뉴 이름',
  please_check_the_pin_location_is_correct_on_map:
    '핀이 올바르게 위치했는지 확인하세요.',
  import: '불러오기',
  by_creating_tickets: '티켓 등록/판매 시 티켓판매약관에 동의하게 됩니다..',
  show: '공개',
  close: '닫기',
  dismiss: '제외',
  apply: '신청.',
  apply_in_colla: '요청하기',
  event_page_vn: '이벤트 페이지',
  introduce_report_of_you: '당신의 보고서를 소개합니다.',
  send_report: '신고/건의',
  no_ticket_is_on_sales_now: '판매 중인 티켓이 없습니다.',
  continue_with_facebook: '페이스북 간편 로그인/회원가입',
  continue_with_google: '구글 간편 로그인/회원가입',
  new_password: '새 비밀번호',
  // faq: '자주 묻는 질문',
  faq: '자주 묻는 질문',
  'a_ticket_check-in_at_the_event': 'A. 티켓 확인 방법',
  comeup_ticket: '컴업 E-Ticket',
  'printed_comeup_e-ticket': '출력한 컴업 E-Ticket',
  'ticket_holder_name_&_phone_number': '예매자 이름 & 전화번호',
  delivered_ticket_from_this_event_host: '배송받은 실물 티켓',
  'no_ticket_check-in': '티켓 확인 없이 입장',
  b_restrictions: 'B. 유의사항',
  id_card_check: '신분증 필수 지참',
  no_minor: '미성년자 입장 불가',
  no_carrying_food_and_beverage: '외부 식음료 반입 불가',
  no_pet: '반려동물 동반 불가',
  custom_restriction: '유의사항 별도 입력',
  c_parking: 'C. 주차',
  no_car_park: '주차 불가',
  free: '무료',
  paid: '유료',
  d_refund_policy: 'D. 환불 규정',
  comeup_refund_policy: '컴업 기본 환불 규정',
  full_refund_before_event_start: '이벤트 시작 전까지 전액 환불',
  no_refund: '환불 불가',
  host_refund_policy: '호스트 별도 환불 규정',
  e_host_info: 'E. 호스트 정보',
  ceo: '대표',
  business_registration_number: '사업자등록번호',
  address: '주소',
  the_person_in_charge: '담당자',
  phone: '전화번호',
  select_category_event:
    '가장 먼저 선택한 카테고리가 기본 카테고리로 설정됩니다.',
  select_category_event_0: '최대 5개까지 선택 가능.',
  already_exited_id: '이미 존재하는 ID입니다.',
  'happy_team_makes_you_happy._follow_us_in_comeup':
    '행복한 팀은 당신을 행복하게 만듭니다. ComeUp에서 우리를 팔로우하세요.',
  'more_convenient _comeup_app.': '보다 편리한  ComeUp 앱',
  sure_to_delete_this_team: '호스트 계정을 삭제하시겠습니까?',
  text_after_delete_team:
    '호스트 계정을 삭제하면 멤버를 포함한 모든 사용자가 호스트 페이지에 접근할 수 없게 되며, 호스트가 등록한 이벤트와 베뉴 등의 콘텐츠도 모두 삭제됩니다.',
  input_text_delete_team: '진행하시려면 비밀번호를 입력하세요.',
  team_page: '팀 페이지',
  team_page_1: '팀 페이지',
  let_comeup_know_what_you_want_to_report_about_team_Happy:
    '팀에 대해 건의하고 싶은 내용이 있나요?',
  'do_you_want_to_discard_this_draft?': '이 초안을 삭제 하시겠습니까?',
  total_sales: '총 매출',
  fee_include: '수수료 :value(결제 수수료와 부가세 포함)',
  current_balance: '미정산 잔액',
  sales_reports: '판매 보고서',
  revenue: '수익',
  text_share_team:
    '멤버로 초대하고 싶은 친구에게 호스트 페이지 바로가기 링크를 공유해 보세요.\n친구가 호스트 계정을 팔로잉하면, 팔로워 목록을 통해 멤버로 간편하게 초대할 수 있습니다.',
  copyright_index: 'Copyright © STARINDEX-VIETNAM Inc. All Rights Reserved.',
  copyright_index_ko: 'Copyright © STARINDEX Inc. All Rights Reserved.',
  address_footer: '호치민 시티 1 군 응 웬후 68 비 엣콤 리얼 빌딩 9 층',
  update: '최신 정보',
  total: '합계',
  get_ticket: '예매하기',
  how_to_use_tickets: '티켓 사용 방법?',
  how_to_use_tickets2: '티켓 사용 방법',
  refund_policy: '환불 정책',
  detail_info: '상세 정보',
  delivery_info: '배송 정보',
  phone_number: '전화번호',
  purchase: '결제하기',
  i_have_checked_the_refund_policy_and_I_agree_to_purchase_all_the_selected_tickets:
    '환불 정책을 포함한 모든 티켓 정보를 확인했으며 위\n내용에 따라 예매를 진행합니다.',
  question: '질문',
  refund_policy_1:
    '1. 티켓 구매약관은 ComeUp 서비스 이용 약관의 부칙으로 ComeUp에서 티켓을 구매한 회원에게 추가 적용됩니다.',
  refund_policy_2:
    '2. ComeUp은 안전한 티켓 구매와 사용을 위해 이벤트가 정상적으로 진행되었는지 확인하고, 당사 규정에 따라 접수된 환불 요청이 모두 처리된 이후 이벤트 운영자에게 티켓 판매 수익금을 지급합니다.',
  refund_policy_3: '3. 이벤트가 취소되면 사용하지 않은 티켓은 전액 환불됩니다.',
  refund_policy_4:
    '4. 티켓 환불 신청은 ComeUp 모바일 앱과 웹 사이트 시스템을 통해 진행된 경우에만 정상적으로 인정됩니다.',
  refund_policy_5:
    '5. 이미 올바르게 사용 된 티켓 또는 회원의 부주의로 인해 실수로 사용한 티켓은 환불이 불가능합니다.',
  refund_policy_6:
    '6. 이벤트나 티켓 설명 내에서 별도의 환불규정이 있는 경우, 우리의 기본 환불규정보다 우선 적용됩니다.',
  refund_policy_7: '7. 기본 환불 규정',
  refund_policy_7_1:
    '7.1 이벤트 일정 및 주요 사항이 변경되어 티켓을 구매한 회원이 환불 신청을 하는 경우 전액 환불 처리됩니다.',
  refund_policy_7_2:
    '7.2 사용하지 않은 티켓을 환불 신청하는 경우 아래와 같이 최초 신청 시기에 따라 수수료를 부과한 후 환불 처리됩니다.',
  refund_policy_ofter:
    '행사 시작 10 일 전 : 구매 한 티켓 가격의 100 %가 환불됩니다. 행사 시작 7 일 전 : 구매 한 티켓 가격의 90 %가 환불됩니다.',
  refund_policy_ofter1:
    '행사 시작 3 일 전 : 구매 한 티켓 가격의 80 %가 환불됩니다. 행사 시작 1 일 전 : 구매 한 티켓 가격의 70 %가 환불됩니다.',
  refund_policy_ofter2:
    '이벤트 시작일 이후: 환불 불가. 단, 3일 전에 티켓을 구매한 경우',
  not_yet_sold: '아직 판매되지 않음',
  stop_selling: '판매 마감',
  sold_out: '매진',
  district: '구/군',
  commune: '마을',
  how_use_ticket_1: '티켓을 구입한 후 올바른 티켓을 선택했는지 확인하세요.',
  how_use_ticket_1_2:
    '. 티켓은 나중에 이메일로 전송됩니다. 게이트에서 티켓을 스캔 할 수 있도록 인쇄하거나 휴대폰으로 다운로드하세요.',
  how_use_ticket_2:
    '중요 :받은 편지함에 주문 내역이 표시되지 않을경우 정크 / 스팸 폴더를 확인하세요.',
  how_use_ticket_3:
    '주문에 기술적인 문제가있는 경우 ...을 클릭하여 ComeUp에 문의해주세요.',
  how_use_ticket_4:
    '각 주문과 결제가 성공적으로 이루어지면 어떤 형태로든 환불이나 취소가 불가능합니다.  만약 주문정보에 오류가 있는 경우 정보를 변경할 수 없습니다.',
  how_use_ticket_5:
    '이 행사들은 날씨도 좋고 비도 많이 오는 날에도 일어날 수 있다.',
  how_use_ticket_6: '아티스트는 변경 될 수 있습니다.',
  how_use_ticket_7: '** 공식 ',
  how_use_ticket_7_2:
    '사이트에서만 구매. 다른 소스에서 구매하는 경우 위조 또는 유효하지 않은 패스를 구입할 위험이 있습니다.',
  how_use_ticket_7_3:
    '은 비공식 소스에서 구매 한 패스에 대해 책임을지지 않습니다.',
  email_not_confirm: '이메일을 받지 못했을 경우',
  email_not_confirm_korea: ' 를 클릭하세요.',
  here: ' 여기',
  resend_email: '이메일 다시 보내기',
  resend_email_text: '계정 인증을 위한 이메일이 발송되었습니다. 확인해 주세요.',
  ticket_type: '유형',
  set_the_time_first: '먼저 시간을 설정해주세요.',
  set_the_location_info_first: '위치를 먼저 설정해 주세요.',
  save_pin_location: '저장',
  'e-ticket': 'E-Ticket',
  delivery_printed_ticket: '실물 티켓',
  delivery: '배달',
  purchasable_by: ':date1부터 :date2까지 구매 가능',
  purchasable_by2: ':day.:date :time까지 구매 가능 ',
  on_s_ticket: '일',
  in_process: '진행 중…',
  ordered: '성공',
  cash_on_delivery: '티켓 수령 시 결제',
  give_delivery_text_in_purchase_cod:
    '배달원에게 VND 10,000,000 + 배달비를 현금으로 지불하세요.',
  delivery_fee: '배송 수수료',
  delivery_fee2: '배송비',
  please_fill_in_the_missing_field: '누락된 필드를 입력하십시오!',
  now_ticket: '지금',
  after_delete_this_team: "팀을 삭제하면 팀 멤버를 포함한 모든 사용자가 팀 페이지를 볼 수 없으며, 해당 팀이 등록한 이벤트와 장소 등의 콘텐츠도 모두 삭제됩니다.",
  past_tickets: '만료된 티켓',
  my_ticket: '내 티켓',
  refund: '환불',
  refund2: '환불 요청',
  'it_s_not_an_e-ticket': '사용할 수 없는 티켓입니다.',
  show_your_delivery_ticket: '배송받은 실물 티켓을 보여주세요.',
  use_ticket: '티켓 사용하기',
  thanks_for_your_purchase: '이용해 주셔서 감사합니다!',
  more_people_more_fun:
    '함께할수록 커지는 즐거움!\n친구들에게 이벤트 소식을 알려주세요!',
  delivery_ticket: '실물 티켓',
  delivery_ticket2: '실물 티켓',
  select_a_payment_method: '결제 방법 선택',
  domestic_card: '국내 카드',
  input_a_receiver_information: '티켓 수령인의 정보를 입력하세요.',
  available_ticket_for_cash_on_delivery:
    '티켓은 예약었으며, 주문을 받을 때 지불됩니다.',
  'e-ticket_is_not_purchasable_with_cash_on_delivery':
    'E-티켓은 현금이나 배송으로 구입하지 않습니다.',
  back_to_the_event_page: '이벤트 페이지로 돌아가기',
  'select_a_payment_method_to_purchase_the_e-ticket':
    '전자 항공권을 구매할 결제 수단을 선택하세요.',
  depend_on_the_ticket_text:
    '* 배송지 위치나 티켓 형태에 따라 배송료가 상이하거나 배송이 불가할 수 있습니다.',
  past_ticket: '만료된 티켓',
  set: '수정',
  not_found: '찾을 수 없습니다.',
  history: '환불 내역',
  refunded: '환불',
  refunding: '환불',
  used: '사용 완료',
  event_not_found: '이벤트를 찾을 수 없습니다.',
  team_not_found: '참여 중인 팀이 없습니다.',
  more: '더 보기',
  'e-ticket_canceled': 'E-티켓 취소',
  my_profile: '프로필',
  you_have_not_tickets: '현재 사용가능한 티켓이 없어요.',
  welcome_to_join_us: '컴업에 오신 것을 환영합니다!',
  welcome_back: '다시 오셨네요, 환영합니다!',
  choose_no_more_than_10_photos: '사진은 최대 10장까지 선택 가능하며, 선택한 사진들의 총 용량은 20MB 이하여야 합니다.',
  foreign_card: '해외 카드',
  bank_transfer: '계좌 이체',
  view: '보기',
  title_used_ticket: '티켓을 사용하시겠습니까?',
  content_used_ticket:
    '현장 스태프 확인 없이 티켓을 사용할 경우 입장이 허용되지 않을 수 있습니다.',
  used_ticket: '티켓 사용',
  'used_/_refunded': '사용 / 환불',
  rejected_refund: '환불 거절',
  refund_accepted: '대기 중 - 환불 조건 확인',
  expired: '만료',
  select_a_district: '구/군 선택',
  error: '오류',
  time_used: '사용 시간',
  thanks_you_used_ticket: '이용해 주셔서 감사합니다.',
  please_check_your_ticket: '티켓을 확인해주세요.',
  give_ticket_free: '티켓 무료 제공',
  schedule: '스케줄',
  canceled: '취소내역',
  invite_people_as_your_staff: '이벤트를 함께 관리할 사람들을 초대해 보세요.',
  ticket_holder: '예매자',
  search_name_or_id_by_bolder_list: '이름, 닉네임, ID, 전화번호로 검색',
  repeat_title_1: '이 이벤트가 다시 시작되는 요일을 선택하십시오.',
  repeat_title_2: '(선택한 요일에 동일한 이벤트가 등록됩니다)',
  warning: '잠시만요!',
  cancel_event_question: '이벤트를 취소하시겠습니까?',
  cancel_event_question: '이벤트를 취소하시겠습니까?',
  cancel_event_title_1: '이벤트가 취소되면',
  cancel_event_title_2:
    '· 예매자에게 이벤트 취소 알림 메일과 메시지가 발송됩니다.',
  cancel_event_title_3: '. 예매된 모든 티켓은 자동 환불됩니다.',
  cancel_event_content_placeholder: '이벤트가 취소된 이유를 알려주세요.',
  invitations_empty: '협업을 요청한 호스트가 없습니다.',
  event_registration: '이벤트 등록',
  team_empty: '등록된 호스트가 없습니다.',
  event_empty: '등록된 이벤트가 없습니다.',
  venue_empty: '등록된 베뉴가 없습니다.',
  event_host_list: '호스트 목록',
  no_data: '받은 요청이 없습니다.',
  event_map: '이벤트 맵',
  event_news_empty: '이벤트 뉴스가 비어 있습니다!',
  team_news_empty: '팀 소식이 없습니다!',
  // delete_event_news: '이벤트 뉴스 삭제',
  delete_event_news: '',
  delete_team_news: '팀 뉴스 삭제',
  // delete_venue: 'Delete venue',
  validate_content: '내용이 필요합니다!',
  validate_ticket_name: '티켓 이름을 입력하세요.',
  validate_ticket_price: '티켓 가격을 입력하세요.',
  validate_ticket_quantity: '티켓 수량을 입력하세요.',
  validate_ticket_limit: '티켓 한도 수량을 입력하세요.',
  validate_ticket_time_start: '티켓의 시작 시간을 입력하세요.',
  validate_ticket_time_end: '티켓의 시간 종료을 입력하세요',
  validate_ticket_price_min: '가격은 :zero 또는 최소 :min와 같아야 합니다',
  validate_ticket_price_max: '최대 가격은 :max입니다',
  validate_ticket_price_ticket: '가격은 :min 이상 :max 이하로 설정 가능합니다. 무료 티켓일 경우 :zero을 입력하세요.',
  host_info_empty: '호스트 정보 목록이 비어 있습니다!',
  validate_city: '도시를 입력하세요.',
  validate_address: '주소를 입력하세요.',
  validate_location: '위치를 입력하세요.',
  validate_detail: '상세 섹션 입력하세요.',
  city_empty: '도시를 찾을 수 없습니다.',
  access_camera_title: '카메라에 접근 할 수 있습니까?',
  access_camera_content: '티켓을 스캔하려면 액세스 권한이 필요합니다.',
  open_setting: '설정 열기',
  scan_ticket: '스캐닝 티켓',
  scan_on_e_ticket: 'E-티켓에서 QR 코드 스캔',
  checkin: '체크인',
  at: 'at',
  by: 'by',
  scan_again: '스캔 재시도',
  continue: '계속하기',
  holder_list: '예매자 리스트',
  download: '다운로드',
  placeholder_keyword: '이름, 닉네임, ID, 전화번호로 검색',
  write_a_comment: '예매자에 대해 메모할 내용을 입력하세요.',
  ticket_empty: '현재 사용가능한 티켓이 없어요.',
  payment: '결제',
  select_ticket: '티켓 선택',
  ticket_list: '티켓',
  more_news: '더 많은 뉴스',
  validate_avatar: '아바타가 필요합니다.',
  validate_name: '이름이 필요합니다.',
  validate_team_id: '팀 아이디가 필요합니다.',
  validate_phone: '전화번호를 입력하세요.',
  validate_country: '국가를 입력하세요.',
  validate_specialty: '전문분야를 입력하세요.',
  validate_email: '이메일을 입력하세요.',
  loading: '검색 중...',
  logging_in: '로그인 중...',
  found: '찾기',
  people: '사람들',
  find_and_follow_us_in_comeup: '자세한 호스트 정보는 컴업에서 확인!',
  find_and_join_event_in_comeup: '자세한 이벤트 정보는 컴업에서 확인!',
  find_and_join_venue_in_comeup: '자세한 베뉴 정보는 컴업에서 확인!',
  delete_ticket: '삭제',
  are_you_sure_to_delete_ticket: '티켓을 삭제 하시겠습니까?',
  are_you_sure_to_delete_invite: '초대를 취소하시겠습니까?',
  event_finish: '이벤트 완료',
  event_finished: '종료된 이벤트',
  request_a_refund: '환불 요청 메시지',
  requested_refund: '환불 요청 관리',
  requested_refund2: '환불 요청 관리',
  requested_refund3: '환불 요청 중',
  processing: '처리중',
  domestic_foreign_card: '카드 결제',
  korean_card: '국내 카드',
  validate_poster_required: '포스터가 필요합니다.',
  validate_title_required: '제목이 필요합니다.',
  validate_time_start_required: '시작시간이 필요합니다.',
  validate_time_finish_required: '종료시간이 필요합니다.',
  validate_category_required: '카테고리가 필요합니다.',
  validate_address_required: '주소가 필요합니다.',
  validate_Unit_required: '단위가 필요합니다.',
  are_you_sure_delete_event_news: '뉴스를 삭제하시겠습니까?',
  current_sales: '판매 현황',
  final_sales: '최종 판매',
  include_vat: 'VAT 포함',
  sold: '판매',
  fee: '수수료',
  can_dowload_sales_report:
    '이벤트 종료 후 최종 판매 보고서를 다운로드할 수 있습니다.',
  can_withdraw_sales_revenue:
    "수익금 정산은 영업일 기준 이벤트 종료 7일 후부터 요청할 수 있습니다.(단, 환불 처리가 모두 완료되었을 경우에 한함)",
  refused: '거절',
  refused_status: '환불 거절',
  refunded: '환불',
  refunded_status: '환불 완료',
  refuse_all: '전체 거절',
  refund_all: '전체 승인',
  updated: '최신 요청',
  updated_status: '신규 요청',
  tax_issue: '세금 계산서 발행을 위해 필요한 정보를 입력해 주세요.',
  company_name: '회사명',
  registration_no: '사업자등록번호',
  bank_account_info:
    '정산받을 계좌 정보를 입력해 주세요.',
  account_no: '계좌번호.',
  account_holder: '예금주',
  company: '회사',
  individual: '개인',
  bank: '은행',
  withdrawal_request: '정산 요청하기',
  account: '계정',
  holder: '소유자',
  delete_event_question: '이벤트를 삭제하시겠습니까?',
  are_you_sure_to_set_role: '이 멤버의 등급을 변경하시겠습니까?',
  are_you_sure_to_leave_event: '이벤트 관리자에서 떠나시겠습니까?',
  leave_event: '이벤트를 떠나다.',
  set_role: '역할 설정',
  sure_to_kick_this_member_event: '이 멤버를 이벤트 관리자에서 제외하시겠습니까?',
  confirmation_forgot_password:
    '비밀번호 재설정 링크가 전송되었습니다. 10분 후에도 이메일이 도착하지 않는다면 컴업 팀에게 연락해 주세요.\n\n      cs@comeup.asia',
  confirmation: '확인!',
  pass_is_invalid: '비밀번호가 유효하지 않습니다!',
  text_total_detail:
    ':total 티켓이 판매되었으므로 티켓 정보를 변경할 수 없습니다.',
  data_empty: '목록 데이터가 비어 있습니다.',
  ticket_info: '티켓 정보',
  ticket_purchase: '구매한 티켓',
  ticket_used: '사용한 티켓',
  ticket_refunded: '환불한 티켓',

  how_use_ticket_1:
    'Once you purchase tickets, please ensure that you choose the right ticket:',
  how_use_ticket_2:
    '** If you have any technical issues with your order, click ',
  how_use_ticket_2_here: 'here',
  how_use_ticket_2_2: ' to contact ComeUp.',
  e_ticket: 'E-티켓',
  e_ticket2: 'E-Ticket',
  e_ticket_tut_1:
    '-  티켓 결제에 성공하면 ComeUp의 "My Ticket" 메뉴에 e-티켓이 생성됩니다.',
  e_ticket_tut_2:
    '- 체크인/스캔하거나 티켓 구매 정보 티켓을 알려주기 위해 ID/PASSPORT를 가져오고 입구에서 전자티켓을 보여주세요. 체크인 프로세스는 각 이벤트 호스트에 따라 달라집니다.',
  e_ticket_tut_3:
    '- 주의! 휴대 전화에서 "티켓 사용"버튼을 누르면 티켓이 사용되며 더 이상 이벤트에서 스캔 할 수 없습니다.',
  cod_ticket: 'Paper Ticket',
  cod_ticket_tut_1:
    '- You will receive paper tickets through your address that you have registered from 01 to 03 days. Please check the type and number of tickets you buy before receiving and paying to delivery service.',
  cod_ticket_tut_2:
    '- Please bring ID/PASSPORT and show yourpaper ticket at the entrance to check in.',
  text_in_refund:
    '환불 요청을 보내도 호스트가 환불을 수락하기 전에 이 티켓을 사용할 수 있습니다.',
  you_have_canceled_the_refund_request: '환불 요청을 취소했습니다.',
  cancel_request: '요청 취소',
  are_you_sure_cancel_request: '환불 요청을 취소하시겠습니까?',
  companyname: '회사명 / 이름',
  text_in_refund1:
    '고객이 환불을 요청하더라도 호스트가 환불을 최종 승인하기 전까지 티켓은 유효합니다.',
  text_in_refund2: '환불을 요청하더라도 호스트가 환불을 승인하기 전까지 티켓은 유효합니다.',
  cancel_this_request: '요청 취소',
  refuse: '거절',
  accept_refund: '승인',
  write_a_message: '메시지를 입력하세요.',
  no_contact: '연락처 정보가 없습니다',
  // search_location:
  //   '구글 지도에서 입력한 주소를 찾을 수 없습니다. 핀을 이용해 위치를 직접 표시해 주세요.',
  search_location:
    '찾을 수 없는 주소입니다. 다시 입력해 주세요.',
  as_new_venue: '추가',
  add: '새로운 장소로',
  write_your_reason: '환불 사유를 입력하세요. (선택 사항)',
  access_location_title: '우리는 당신의 위치에 접근 할 수 있습니까?',
  access_location_content: '주변의 이벤트를 볼 수 있도록 액세스가 필요합니다.',
  write_your_report: '내용을 입력하세요.',
  do_you_want_to_disconnect: '연동된 계정을 해제하시겠습니까?',
  please_add_a_password_before_deleting_the_link:
    '링크를 삭제하기 전에 비밀번호를 추가하세요.',
  no_password: '이 계정에는 비밀번호가 없습니다.',
  remove_connect_facebook: 'facebook 연결취소',
  remove_connect_google: 'google 연결취소',
  not_find_matched_result:
    '일치하는 항목이 없습니다. 키워드를 변경하거나 인기 키워드를 이용해 검색해 보세요.',
  popular_searched_keywords: '인기 키워드',
  my: '프로필',

  following_team: '팔로잉 호스트',
  content: '뉴스',
  liked_venue: '관심 베뉴',
  see_all: '모두 보기',
  see_more: '더 보기',
  received_request: '받은 요청',
  purchase_history: '구매 내역',
  setting: '설정',
  term_of_service: '서비스 이용약관',
  about_comeup: '컴업 소개',
  my_comeup: '나의 프로필',
  // about_comeup_1: 'ComeUp은 오픈 티켓 플랫폼이며 실제 티켓 판매자가 아닙니다',
  about_comeup_1: '컴업은 통신판매중개사로 티켓 판매 당사자가 아니며, 호스트가 제공한 정보 및 거래에 대해 보증하거나 책임지지 않습니다.',
  about_comeup_2:
    `STARINDEX VIETNAM | 대표 : Nguyen Tuan Anh | Floor 9, Vietcomreal Building, 68 Nguyen Hue, Ben Nghe Ward, District 1, Ho Chi Minh City, Vietnam

    contact@starindex.com | (+84)8-2779-2656 | 사업자등록번호 : 0315632115 - 발행일 : April 16, 2019 | 발행처 : Ho Chi Minh City Department of Planning and Investment`,
  about_comeup_3:
    `STARINDEX Inc. | 대표 : 김욱 | 서울특별시 마포구 성지길 25-11 지층 527호

    contact@starindex.com | (+82)2-6243-0771 | 사업자등록번호 : 105-87-75277 | 통신판매업신고번호 : 2013-서울마포-1355`,
  past_events: '지난 이벤트',
  liked_event: '관심 이벤트',
  liked_past_event: '지난 관심 이벤트',
  canceled_event: '취소된 이벤트',
  payment_code: '결제 코드',
  deleted_event: '이벤트 삭제 완료',
  no_notification: '새로운 알림이 없습니다.',
  no_notification2: '알림이 없습니다.',
  enter_country_name: '국가 이름을 입력하십시오.',
  commingsoon: '준비 중 (조금만 기다려주세요!)',
  view_qrcode: 'QR 코드 보기 & 티켓 사용하기',
  used_qr_code: '사용 완료',
  book_tickets: '티켓 예매',
  all_days: '전체 내역',
  all_days2: '전체 내역',
  all_categories: '모든 카테고리',
  total_amount: '합계',
  acount_incorrect: '잘못된 계정 정보',
  add_your_bank_acount: '대기 중 - 환불 정보 입력',
  view_all_ticket: '티켓 모두 보기',
  view_e_receipt: '전자 영수증 보기',
  you_have_been_invited_by_manager:
    ':name님이 이벤트 스태프로 초대했습니다. 수락하시겠습니까?',
  logout_question: '로그아웃하시겠습니까?',
  edit_profile: '프로필 수정',
  general_setting: '일반 설정',
  team_request: '호스트 초대',
  event_request: '이벤트 초대',
  free_ticket: '무료 티켓',
  payment_success: '결제 완료',
  payment_cod: '결제 대금',
  thank_for_using_service: '이용해 주셔서 감사합니다.',
  payment_method: '결제 방법',
  last_30_day: '30일 전까지',
  last_60_day: '60일 전까지',
  last_90_day: '90일 전까지',
  see_detail: '상세 보기',
  view_report: '상세 보기',
  hosted_by: '호스트',
  bookmark: '북마크',
  keyword: '키워드',
  delete_warning: '삭제 하시겠습니까?',
  refund_pending: '환불 대기 중',
  refund_pending2: '환불 진행 중',
  review_info: '정보 검토',
  create_event: '이벤트 등록하기',
  import_event: '불러오기',
  create_venue: '베뉴 등록하기',
  create_team: '호스트 등록하기',
  connect_apple: '애플 간편 로그인/회원가입',
  request_again: '재요청',
  request_refund: '환불 요청',
  host_have_refused_the_refund_request: '호스트가 환불 요청을 거부했습니다.',
  refund_request_refused: '환불 요청 거부',
  refund_request_canceled: '환불 요청이 취소되었습니다.',
  request_pending: '환불 대기 중',
  event_cancel: '취소된 이벤트',
  ticket_canceled: '티켓 취소',
  status_refund_1:
    '위 내용에 따라 환불을 진행하려면 다음 버튼을 눌러 환불받을 계좌 정보를 입력해 주세요.',
  proceed_refund: '환불 진행 ',
  status_refund_2:
    '환불 진행 중입니다. 처리가 완료될 때까지 잠시만 기다려 주세요.',
  status_refund_4:
    '환불 요청을 수락하여 제출했습니다. 요청이 처리 될 때까지 기다려 주시길 바랍니다.',
  status_refund_3: '위 내용에 따라 환불을 진행하려면 다음 버튼을 누르세요.',
  review_refund: '환불 정보 확인',
  refund_succeed: '환불 완료!',
  refund_error: '계정 정보가 잘못되었습니다. 계정 정보를 수정하세요.',
  submit: '확인',
  enter_acount_info: '환불 계좌 정보',
  bank_name: '은행명',
  card_company_name: '카드사',
  card_holder_name: '카드 소지자',
  bank_branch: '지점',
  acount_name: '예금주',
  acount_number: '계좌번호',
  refund_info: '환불 정보',
  card_number: '카드 번호',
  account_holder_name: '예금주',
  bank_note: '환불 사유를 입력하세요. (선택 사항)',
  edit_refund_info: '수정하기',
  pop_status_refund: '현재 환불 요청을 처리했습니다. 추가 알림을 기다리세요.',
  pop_status_refund_1:
    '계정 정보가 잘못되었습니다. 계정 정보를 확인 후 다시 제출하세요.',
  denie_refund1: '호스트 제안을 거부했습니다.',
  denie_refund2: '환불 요청이 다시 전송되었습니다 ',
  the_host_refuse: '호스트가 환불 요청을 거절했습니다.',
  '2': '호스트는 요청을 거부했습니다.',
  accept_proceed_refund: '확인 & 환불 진행',
  noti_update_force:
    'ComeUp에 중요한 업그레이드 버전이 있습니다. 서비스를 계속 사용하려면 새 버전으로 업데이트하십시오!',
  noti_update:
    'ComeUp에는 업그레이드 된 버전이 있으며 새로운 기능을 경험하도록 업데이트되었습니다!',
  notification: '알림!',
  report_to_comeup: '신고/건의하기',
  team_follow_empty:
    '다양한 분야의 호스트들이 흥미로운 이벤트와 베뉴를 만들어가고 있어요. 관심 있는 호스트를 찾아 팔로우해 보세요!',
  explore: '둘러보기',
  select_location_fail: '핀 위치가 잘못되었습니다.',
  pin_error: '핀 위치 오류',
  resend_code_after: ':time 코드를 재전송합니다',
  second: '초후',
  successfully_sent_request_to_join_the_group_waiting_for_approval:
    '협업 요청이 완료되었습니다. 이벤트 호스트의 승인을 기다려 주세요.',
  invited_as_a_member_by_manager:
    ':name님이 호스트 멤버로 초대했습니다. 수락하시겠습니까?',
  invite_accepted_ev: '초대를 수락했습니다.',
  invite_denied: '초대를 거절했습니다.',
  invite_accepted_te: '초대를 수락했습니다.',
  accept: '수락',
  deny: '거절',
  team_invitations: '호스트 초대',
  event_invitations: '이벤트 초대',
  apply_for_collaborating:
    '공동 주최사, 베뉴, 참여 아티스트 등으로 이벤트 협업을 요청할 수 있습니다. 어떤 호스트 계정으로 요청할지 선택해 주세요.',
  unread_news: '새 알림',
  read_news: '읽은 알림',
  mark_as_all_read: '읽은 상태로 변경',
  confirm_refund: '환불 진행 확인',
  do_you_want_confirm_refund: '환불 조건에 동의하고 환불을 진행하시겠습니까?',
  time_accepted_refund: '환불 승인 완료',
  team_accepted_refund: '호스트가 티켓 금액의 ',
  total_bill: ' 환불을 승인했습니다.',
  some_fee: '환불 수수료 :',
  total_amount_get: '최종 환불 금액 :',
  total_amount_refund: '최종 환불 금액 :',
  comeup_fee: '컴업 서비스 수수료',
  payment_fee: '은행/카드사 결제 수수료',
  discard: '취소',
  link_not_found: '링크를 찾을 수 없습니다.',
  photo: '사진',
  video: '동영상',
  replace_content_news:
    '사진 또는 비디오 중 한 가지만 게시할 수 있습니다. 첨부 파일을 변경하시겠습니까?',
  youtube_video_url: 'URL로 불러오기 (유튜브)',
  // insert_video: '첨부',
  insert_video: '동영상 첨부',
  btn_insert_video: '확인',
  title_accept_request: '티켓 금액의',
  are_you_sure_to_book_this_ticket: '이 티켓을 예약 하시겠습니까?',
  promotion_push: '프로모션 알림 푸시',
  received_refund_push: '환불 요청 알림 푸시',
  received_joining_team_request_push: '호스트 멤버 초대 알림 푸시',
  team_new_follower_push: '호스트 신규 팔로워 알림 푸시',
  hour_24_reminder_push: '이벤트 시작 24시간 전 알림 푸시',
  received_collaborating_request_push: '이벤트 협업 요청 알림 푸시',
  promotion_email: '프로모션 알림 이메일',
  received_refund_email: '환불 요청 알림 이메일',
  received_joining_team_request_email: '호스트 멤버 초대 알림 이메일',
  hour_24_reminder_email: '이벤트 시작 24시간 전 알림 이메일',
  received_collaborating_request_email: '이벤트 협업 요청 알림 이메일',
  weekly_news_letter_email: '주간 뉴스레터 이메일',
  push_notifications: '푸시 알림',
  email_sms_notifications: '이메일 알림',
  mobile: '전화번호',
  venuename: '베뉴 이름',
  homepageurl: '홈페이지, SNS 등',
  security_setting: '계정 설정',
  current_password: '현재 비밀번호',
  password_confirmation: '새 비밀번호 확인',
  change_password: '비밀번호 변경',
  update_password: '비밀번호 변경하기',
  update_password_title1: '비밀번호를 변경하시겠습니까?',
  update_password_regex:
    '비밀번호는 영문과 숫자를 조합한 6자리 이상이어야 합니다.',
  update_password_title2:
    '아직 비밀번호를 설정하지 않았습니다. 지금 설정하시겠습니까?',
  text_print_receipt: '영수증 인쇄',
  english: '영어',
  vietnamese: '베트남어',
  korean: '한국어',
  text_this_physical_ticket:
    '예매티켓은 아직 ComeUp에서 승인되지 않았으므로 자세한 내용은 당사에 문의하십시오.',
  not_linked: '연동된 계정 없음',
  google_connected: '구글 연동 완료',
  facebook_connected: '페이스북 연동 완료',
  apple_connected: '애플 연동 완료',
  link: '연동',
  unLink: '해제',
  customer_have_canceled_the_refund_request: '고객이 환불 요청을 취소했습니다.',
  all_past_ticket: '티켓 모두 보기',
  download_pdf_success: 'PDF 파일 다운로드 성공',
  download_pdf_fail: 'PDF 파일 다운로드 실패',
  storage_need: '파일을 다운로드하려면 메모리 액세스가 필요합니다.',
  basic_info: '기본 정보',
  overview: '미리 보기',
  add_event_poster: '대표 사진',
  title_category: '제목 & 카테고리',
  event_title: '이벤트 제목',
  date_and_time: '일시',
  time_start: '시간 시작',
  time_end: '시간 종료',
  repeat_event: '반복 등록',
  save_as_draft: '임시 저장',
  save_draft: '임시 저장',
  save_next_step: '저장 & 다음 단계',
  checkbox_step3: '나중에 등록하기 (컴업에는 유료 티켓뿐 아니라 무료 티켓과 할인 쿠폰도 등록할 수 있습니다.)',
  havent_create_ticket: '등록한 티켓이 없습니다.',
  customize_question: '질문을 입력하세요.',
  add_ticket_description: '티켓 상세 설명을 입력하세요.',
  tickets_selling_terms: '티켓판매약관',
  start_sale_period: '시작일',
  end_sale_period: '종료일',
  detail_info_by_step: '세부 정보',
  ticket_price: '가격',
  host_contact_information: '호스트 정보',
  venue_id: '베뉴 ID',
  unfollow: '팔로우를 취소하시겠습니까?',
  btn_edit: '수정',
  edit_poster: '대표 사진 변경',
  edit_poster_venue: '커버 사진 변경',
  edit_logo_venue: '프로필 사진 변경',
  warnning_ticket:
    '이미 판매된 티켓이 있을 경우 티켓 내용을 수정할 수 없습니다.',
  managing_venue: '호스팅 베뉴',
  open_now: ' 영업 중 ',
  closed_now: ' 영업 종료 ',
  edit_ticket: '수정',
  ticket_status: '비공개',
  show_ticket: '공개',
  hide_ticket: '비공개',

  monday: '월',
  tuesday: '화',
  tueday: '화',
  wednesday: '수',
  thursday: '목',
  friday: '금',
  saturday: '토',
  sunday: '일',

  monday1: '월',
  tuesday1: '화',
  tueday1: '화',
  wednesday1: '수',
  thursday1: '목',
  friday1: '금',
  saturday1: '토',
  sunday1: '일',

  like: '좋아요',
  liked_venue_detail: '좋아요',
  about_venue: '베뉴 소개',
  photos: '사진',
  upload_photos: '사진 추가',
  add_photo: '프로필 사진',
  // add_photo_detail: 'Add photo',
  team_venue: '팀',
  research: '현 지도에서 검색',
  add_venue: '커버 사진',
  name_category: '이름 & 카테고리',
  open_time: '영업 시간',
  start_time: '오픈',
  venue_contact: '베뉴 정보',
  managing_venue: '운영 중인 장소',
  goBackConfirm: '페이지에서 나가시겠습니까?',
  choose_repeat: '반복 날짜 선택',
  already_exited_day: '선택한 날짜', ///????
  account_deletion: '계정 삭제',
  delete_account_info:
    "계정 삭제를 원하시면 아래 버튼을 눌러주세요. 삭제가 완료되면 해당 계정에는 더 이상 접근할 수 없으며, 복원도 일체 불가합니다.",
  continue_to_account_deletion: '계정 삭제하기',
  // manage_venue: 'Manage venue',
  delete_venue_question: '베뉴를 삭제하시겠습니까?',
  delete_account_question: '계정을 삭제하시겠습니까?',
  closed: '휴무',
  book_now: '예약하기',
  pop_info:
    '실물 티켓을 이용하려면 별도 승인이 필요합니다. 컴업 팀에 연락해 주세요.',
  booking_info: '베뉴 연락처',
  hidden: "비공개",
  reviewing: "검토 중",
  this_venue_is_on_reviewing: "베뉴 등록을 위해 컴업 팀에서 리뷰를 진행하는 중입니다. 리뷰 과정은 영업일 기준 약 4일에서 7일이 소요됩니다. 잠시만 기다려 주세요.",
  modal_new: "베뉴 등록을 요청했습니다. 컴업 팀이 확인할 때까지 잠시만 기다려 주세요.",
  The_draft_has_been_saved_successfully: "임시 저장되었습니다.",
  pick_a_date: "날짜를 선택하세요",
  are_you_sure_you_want_to_delete: "티켓을 삭제하시겠습니까?",
  import_events: "불러오기",
  edit_for_ev: "이벤트 & 티켓 수정",
  'ticket_sales_&_withdrawal': '티켓 판매 보고서 & 정산',
  cancel_success: '이벤트 취소 완료',
  you_can_delete_this_event_after_7_days: '이벤트가 종료/취소된 :day일 후부터 삭제할 수 있습니다.',
  there_are_no_pending_invitation: "초대된 사람이 없습니다.",
  sales_reports_ticket: '티켓 판매 보고서',
  do_you_want_to_dismiss_this_team: "이 호스트와의 협업을 취소하시겠습니까?",
  refuse_all_reason: "환불 거절 사유를 입력하세요.",
  btn_refuse_all: '전체 거절하기',
  set_rate_refund: '환불 수수료율 설정',
  btn_refund_all: '전체 승인하기',
  earliest: "구매순",
  name_a_z: "이름순",
  empty_all: "환불 요청이 없습니다!",
  empty_updated: "새로운 환불 요청이 없습니다!",
  empty_refused: "거절된 환불이 없습니다!",
  empty_refunded: "승인된 환불이 없습니다!",
  empty_progressing: "진행 중인 환불이 없습니다!",
  error_delete: "이미 판매된 티켓이 있을 경우 티켓을 삭제할 수 없습니다.",
  select_category_event_0: "(최대 5개까지 선택 가능)",
  instagram: "인스타그램",
  venue_news: "베뉴 뉴스",
  do_you_want_to_delete_this_photo: "사진을 삭제하시겠습니까?",
  notifications: "알림",
  attach_file: "파일 첨부",
  newTeam: "최신 등록순",
  deleteInstagram: '연동 해제',
  found_people: "검색 결과 :people",
  use: '사용',
  purchase_id: "결제 ID",
  enter_acount_info: "환불 계좌 정보",
  // extra_fee: "기타 수수료 :",
  extra_fee: "추가 수수료 :",
  saved_contact_information: "저장된 호스트 정보",
  no_result_found_in_search: "결과를 찾을 수 없습니다.",
  no_ticket_holder: "티켓을 구매한 사람이 없습니다.",
  you_have_no_previously_saved_information: "저장된 정보가 없습니다.",
  cancel_payment: "결제 취소",
  are_you_sure_to_cancel_Payment: "결제를 취소하시겠습니까?",
  vietnam: "베트남",
  korea: "대한민국",
  tel: '전화번호',
  introduce: "컴업 소개",
  general_policy_and_regulation: "서비스 이용 정책",
  regulation_and_form_of_payment: "결제 관련 정책",
  shipping_delivery_policy: "배송 관련 정책",
  exchange_and_refund_policy: "교환/환불 정책",
  policy_to_protect_consumer: "개인정보보호 정책",
  phone_min: "전화번호는 8자리 이상이어야 합니다.",
  error_email: "유효한 이메일이어야 합니다.",
  text_wel_update_comeup_te: "업그레이드된 컴업에 다시 오신 것을 환영합니다! 새로워진 컴업에는 이벤트와 베뉴를 등록하고 관리할 수 있는 호스트 계정이 생겼어요. 호스트 정보를 업데이트하고 컴업의 더 다양한 기능을 이용해 보세요. 감사합니다.",
  go_to_team_settings: "호스트 설정으로 이동",
  'success!': "업데이트 완료!",
  unlikevenue: '좋아요를 취소하시겠습니까?',
  receipt: "영수증 보기",
  endTime: '종료',
  contacts_teampage: "호스트 소개",
  send_report2: '제출',
  sure_to_refuse: "환불을 취소하시겠습니까?",
  are_you_sure: "건의사항을 제출하시겠습니까?",
  btn_invited: "초대",
  btn_invite: "초대",
  no_liked_event_found: "관심 이벤트가 없습니다.",
  no_liked_venue_found: "관심 베뉴가 없습니다.",
  no_liked_news_post_found: "관심 뉴스가 없습니다.",
  event_has_deleted: '삭제된 이벤트',
  edit_email: '이메일 수정',
  red_invoice: '이 주문에 대한 빨간색 인보이스를 받으려면 이 호스트에게 문의하십시오.',
  host_is_individual: '(호스트가 개인인 경우 빨간색 인보이스를 받지 못할 수 있습니다)',
  read_and_agree: '위 내용을 모두 확인하였으며 이에 동의합니다.',
  ask_me_later: '나중에 다시 확인',
  pop_up_withdraw: '이벤트가 정상적으로 진행되었을 경우, 티켓 판매 수익금은 정산 요청일로부터 영업일 기준 3일 이내 위 계좌로 입금됩니다.',
  check_withdraw: "정산 요청 후에는 정보를 수정하거나 취소할 수 없습니다. 입력하신 정보가 정확한지 다시 한 번 확인해 주세요.",
  write_of_cumtom: "내용을 입력하세요.",
  today_closed: '오늘 휴무',
  // 
  // about
  our_joy: "놀잘러로 향하는 요즘 여가 플랫폼",
  comeup: "",
  together: "즐거운 일상으로 컴업!",
  comeup_des: "컴업은 파티, 모임, 공연 등의 이벤트부터 체험 액티비티, 엔터테인먼트 베뉴까지! 우리의 삶을 풍성하게 만들어줄 다양 한 여가 정보를 나누는 여가 특화 플랫폼입니다. 관심사, 지역, 시간에 따라 즐길거리를 편하게 예약할 수 있을 뿐 아니라 여가 생활을 함께 즐길 비슷한 취향의 친구들도 만날 수 있습니다. 또한, 원한다면 누구나 호스트가 되어 이벤트를 개설하 고 티켓도 판매할 수 있습니다.\n\n함께 즐길수록 더 커지는 행복, 여가의 가치를 발견하는 곳, 컴업에서 같이 놀아요!",
  our_vision: "비전",
  comeup_vision: "사람과 사람, 사람과 문화를 연결해 즐거운 경험을 제공함으로써 함께 행복하기 위한 사회가 되도록 기여합니다.",
  our_mission: "미션",
  about1: "여가 콘텐츠를 매개로 사람, 문화, 시간, 공간을 연결해 함께 즐기는 온오프라인 경험을 제공합니다.",
  about2: "동시대 대중의 트렌드, 호기심을 반영한 새롭고 이색적인 여가 콘텐츠를 끊임없이 공유합니다.",
  about3: "누구나 경계 없이 다양한 층위의 여가 문화를 공유할 수 있도록 열린 장을 만듭니다.",
  title1: "트렌디한 여가 생활",
  content1: "이벤트, 핫플레이스, 크리에이터! 다양하고 이색적인 놀거리",
  title2: "함께하는 즐거움",
  content2: "비슷한 취향, 관심사를 가진 사람들과 소셜라이징",
  title3: "자유로운 열린 공간",
  content3: "누구에게나, 어떤 문화에나 활짝 열린 오픈플랫폼",
  lets: "그래서 우리는 컴업해업!",
  lets2: "",
  btn_about1: '구경하기',
  btn_about2: "등록하기",
  contact_us: '고객센터',
  south_korea: "대한민국",
  contact1_ko: "   (82) 02 - 6243 - 0771",
  contact2_ko: "   cs@comeup.asia",
  contact3_ko: "   @comeup (카카오톡 채널)",
  contact4_ko: " • - 운영시간 10:00 ~ 19:00",
  contact5_ko: " • - 점심시간 13:00 ~ 14:00",
  contact6_ko: " • - 주말 및 공휴일 휴무",
  contact7_ko: "",
  // vietnam: "Viet Nam"
  contact1_vn: "   (84) 8 - 2779 - 2656",
  contact2_vn: "   cs@comeup.asia",
  contact3_vn: "",
  contact4_vn: " • - 운영시간 09:00 ~ 18:00",
  contact5_vn: " • - 점심시간 13:00 ~ 14:00",
  contact6_vn: " • - 주말 및 공휴일 휴무",
  contact7_vn: " • - 베트남 현지 시간 기준",
  title_footer: "Vertical Platform Meets Social",
  content_footer: "스타인덱스는 컴업 운영사로 소셜 기능을 접목해 일상의 경험을 확장시키는 버티컬 플랫폼을 개발, 서비스하는 ICT 스타트업입니다. 대한민국 서울, 베트남 하노이에 지사를 두고 아시아 시장에 진출하고 있습니다.",
  //
  map_area: "현재 지도",
  please_submit: "안전한 서비스 이용을 위해 호스트에 대한 추가 정보를 알려주세요.",
  you_can_edit_later: "추후 호스트 소유자가 바뀌거나 변동 사항이 발생할 경우 수정 가능합니다.",
  id_info: "ID 정보",
  a_leader_name: "호스트 소유자의 이름 또는 회사명",
  id_or_business: "주민등록번호 또는 사업자등록번호",
  later_version: "나중에",
  update_version: "업데이트",
  description_verion1: '새로운 컴업 앱이 배포 되었어요.\n업데이트 하시겠어요?',
  description_verion2: "새로운 컴업 앱이 배포 되었어요.\n앱을 업데이트 해주세요.",
  useComeup: "컴업 서비스를 계속 이용하기 위해 아래 약관과 정책을 확인해 주세요.",
  full_name_and_card: "성명 (신분증 이름)",
  id_card_passport: "주민등록번호/여권번호",
  established: '설립일',
  set_custom_address: "입력한 주소를 사용하고 핀 위치 수동 설정",
  ticket_des1: "아직 승인되지 않은 티켓입니다. 자세한 내용은 ",
  ticket_des2: "를 클릭해 주세요.",
  select_date: "날짜 선택"
};
