import * as Types from '../constants/ActionType';
import * as Types2 from '../constants/ActionType2';
const initialState = {
    loading: false,
    cities: [],
    countries: [],
    districts: [],
    event_countries: [],
    venue_countries: [],
    home_countries: [],
};

const city = (state = initialState, action) => {
    switch (action.type) {
        case Types.SELECT_CITY_REQUEST:
            state.loading = true;
            return { ...state };
        case Types.SELECT_CITY_SUCCESS:
            state.loading = false;
            state.cities = action.data;
            return { ...state };
        case Types.SELECT_CITY_FAILURE:
            state.loading = false;
            return { ...state };
        case Types.SELECT_COUNTRY_REQUEST:
            state.loading = true;
            return { ...state };
        case Types.SELECT_COUNTRY_SUCCESS:
            state.loading = false;
            state.countries = action.data;
            return { ...state };
        case Types.SELECT_COUNTRY_EVENT_SUCCESS:
            state.loading = false;
            state.event_countries = action.data;
            return { ...state };
        case Types.SELECT_COUNTRY_FAILURE:
            state.loading = false;
            return { ...state };
        case Types.GET_DISTRICT_BY_CITY_REQUEST:
            state.loading = true;
            return { ...state };
        case Types.GET_DISTRICT_BY_CITY_SUCCESS:
            state.loading = false;
            state.districts = action.data;
            return { ...state };
        case Types.GET_DISTRICT_BY_CITY_FAILURE:
            state.loading = false;
            return { ...state };

        case Types2.SELECT_COUNTRY_REQUEST_VENUE:
            state.loading = true;
            return { ...state };
        case Types2.SELECT_COUNTRY_EVENT_SUCCESS_VENUE:
            state.loading = false;
            state.venue_countries = action.data;
        case Types2.SELECT_COUNTRY_FAILURE_VENUE:
            state.loading = false;
        case Types2.SELECT_CITY_FAILURE_VENUE:
            state.loading = false;
        // 
        case Types2.SELECT_COUNTRY_REQUEST_HOME:
            state.loading = true;
            return { ...state };
        case Types2.SELECT_COUNTRY_EVENT_SUCCESS_HOME:
            state.loading = false;
            state.home_countries = action.data;
        case Types2.SELECT_COUNTRY_FAILURE_HOME:
            state.loading = false;
        case Types2.SELECT_CITY_FAILURE_HOME:
            state.loading = false;
        default: return { ...state };
    }
}

export default city;