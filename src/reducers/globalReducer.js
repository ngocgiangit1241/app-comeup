import * as Types from '../constants/ActionType';
import * as Types2 from '../constants/ActionType2';
const initialState = {
    showModalLogin: false,
    config: {},
    city: {},
    country: {},
    iso_code: 'kr',
    isUseLinking: true
};

const global = (state = initialState, action) => {
    switch (action.type) {
        case Types.SHOW_MODAL_LOGIN:
            state.showModalLogin = true;
            return { ...state };
        case Types.HIDE_MODAL_LOGIN:
            state.showModalLogin = false;
            return { ...state };
        case Types.LOAD_DATA_CONFIG_SUCCESS:
            state.config = action.data;
            return { ...state };
        case Types.SET_DATA_CITY_SUCCESS:
            state.city = action.data;
            return { ...state };
        case Types.SET_DATA_COUNTRY_SUCCESS:
            state.country = action.data;
        case Types.LOAD_DATA_GEOIP_SUCCESS:
            state.iso_code = action.data.Code;
            return { ...state };
        case Types2.USE_LINKING:
            state.isUseLinking = false;
            return { ...state };
        default: return { ...state };
    }
}

export default global;