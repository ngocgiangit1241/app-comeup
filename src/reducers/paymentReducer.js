import * as Types from '../constants/ActionType';
import * as Types2 from '../constants/ActionType2';
const initialState = {
    loading: false,
    data: {},
    is_buy_cod: false,
    loading_buy_cod: false,
    loading_e_ticket_free: false,
};

const payment = (state = initialState, action) => {
    switch (action.type) {
        case Types2.PAYMENT_REQUEST:
            state.loading = true;
            return { ...state };
        case Types2.PAYMENT_SUCCESS:
            state.loading = false;
            state.data = action.data ? action.data : {};
            return { ...state };
        case Types2.PAYMENT_FAILURE:
            state.loading = false;
            return { ...state };
        case Types2.LOAD_PURCHASE_DETAIL:
            state.is_buy_cod = false;
            return { ...state };
        case Types2.PAYMENT_COD_REQUEST:
            state.loading_buy_cod = true;
            return { ...state };
        case Types2.PAYMENT_COD_SUCCESS:
            state.loading_buy_cod = false;
            state.is_buy_cod = true;
            return { ...state };
        case Types2.PAYMENT_COD_FAILURE:
            state.loading_buy_cod = false;
            return { ...state };
        case Types2.PAYMENT_E_TICKET_FREE_REQUEST:
            state.loading_e_ticket_free = true;
            return { ...state };
        case Types2.PAYMENT_E_TICKET_FREE_SUCCESS:
            state.loading_e_ticket_free = false;
            return { ...state };
        case Types2.PAYMENT_E_TICKET_FREE_FAILURE:
            state.loading_e_ticket_free = false;
            return { ...state };
        default: return { ...state };
    }
}

export default payment;