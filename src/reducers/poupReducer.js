import * as Types2 from '../constants/ActionType2';
const initialState = {
    loading: false,
    data: []
};

const poup = (state = initialState, action) => {
    switch (action.type) {
        case Types2.POUP_LIST_SUCCESS:
            state.data = action.data
            state.loading = true;
            return { ...state };
        default: return { ...state };
    }
}

export default poup;