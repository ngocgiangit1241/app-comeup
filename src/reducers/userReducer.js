import * as Types from '../constants/ActionType';
import * as Types2 from '../constants/ActionType2';
const initialState = {
  loading: false,
  profile: {},
  errors: {},
  searchMembers: [],
  loadingSearch: false,
  loadingCheckSocial: false,
  loadingUpdateProfile: false,
  err_social_fb: '',
  err_social_gg: '',
  err_social_ap: '',
  followingTeam: {
    teams: [],
    loading: false,
    loadMore: false,
    is_empty: false,
    teamIdAction: 0,
    loadingFollowers: false,
  },
  hostingEvent: {
    past_events: [],
    past_loading: false,
    past_loadMore: false,
    past_is_empty: false,
    past_eventIdAction: 0,
    past_loadingLike: false,

    events: [],
    loading: false,
    loadMore: false,
    is_empty: false,
    eventIdAction: 0,
    loadingLike: false,
  },
  hostingVenue: {
    venue: [],
    loading: false,
    load_more: true
    // loadMore: false,
    // is_empty: false,
    // eventIdAction: 0,
    // loadingLike: false,
  },
  loadingProfile: false,
  profileData: {
    events: [],
    teams: [],
  },
  check_login_loading: false,
  error_status: '',
  is_empty: false,
  loadMore: false,
  listTeamRequest: [],
  listEventRequest: [],
  listPurchaseHistory: [],
  loadMoreSaleReport: false,
  saleReport: [],
  isVerifyEmail: false,
  modalRegisterTeam: false,
  team: {},
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case Types.LOGIN_REQUEST:
      state.loading = true;
      state.errors = {};
      return { ...state };
    case Types.LOGIN_SUCCESS:
      state.loading = false;
      state.errors = {};
      state.profile = action.data;
      return { ...state };
    case Types.LOGIN_FAILURE:
      state.loading = false;
      state.errors = action.errors;
      return { ...state };
    case Types.LOGOUT:
      return { ...state };

    case Types.USER_SEARCH_REQUEST:
      state.loadingSearch = true;
      return { ...state };
    case Types.USER_SEARCH_SUCCESS:
      state.loadingSearch = false;
      state.searchMembers = action.data;
      return { ...state };
    case Types.USER_SEARCH_FAILURE:
      state.loadingSearch = false;
      return { ...state };

    case Types.USER_INVITE_TEAM_SUCCESS:
      state.loadingInvite = false;
      var members = state.searchMembers;
      var newMembers = members.map(member => {
        if (member.Id == action.memberId) {
          member.Role = 'Invited';
        }
        return member;
      });
      state.searchMembers = newMembers;
      return { ...state };
    case Types.UPDATE_AVATAR_SUCCESS:
      state.profile = action.data;
      return { ...state };

    case Types.CHECK_LOGIN_SOCIAL_REQUEST:
      state.loadingCheckSocial = true;
      state.err_social_fb = '';
      state.err_social_gg = '';
      state.err_social_ap = '';
      return { ...state };
    case Types.CHECK_LOGIN_SOCIAL_SUCCESS:
      state.loadingCheckSocial = false;
      state.err_social_fb = '';
      state.err_social_gg = '';
      state.err_social_ap = '';
      if (action.login_type == 'fb') {
        state.profile.Is_SNS_FB = true;
      } else if (action.login_type == 'gg') {
        state.profile.Is_SNS_GG = true;
      } else {
        state.profile.Is_SNS_AP = true;
      }
      return { ...state };
    case Types.CHECK_LOGIN_SOCIAL_FAILURE:
      state.loadingCheckSocial = false;
      if (action.login_type == 'fb') {
        state.err_social_fb = action.errors.message
          ? action.errors.message
          : '';
      } else if (action.login_type == 'gg') {
        state.err_social_gg = action.errors.message
          ? action.errors.message
          : '';
      } else {
        state.err_social_ap = action.errors.message
          ? action.errors.message
          : '';
      }
      return { ...state };
    case Types.REMOVE_CONNECT_REQUEST:
      state.loadingCheckSocial = true;
      return { ...state };
    case Types.REMOVE_CONNECT_SUCCESS:
      state.loadingCheckSocial = false;
      if (action.remove_type == 'fb') {
        state.profile.Is_SNS_FB = false;
      } else if (action.remove_type == 'gg') {
        state.profile.Is_SNS_GG = false;
      } else {
        state.profile.Is_SNS_AP = false;
      }
      return { ...state };
    case Types.REMOVE_CONNECT_FAILURE:
      state.loadingCheckSocial = false;
      return { ...state };
    case Types.UPDATE_PROFILE_REQUEST:
      state.errors = {};
      state.loadingUpdateProfile = true;
      return { ...state };
    case Types.UPDATE_PROFILE_SUCCESS:
      state.loadingUpdateProfile = false;
      state.profile = action.data;
      return { ...state };
    case Types.UPDATE_PROFILE_FAILURE:
      state.errors = action.errors;
      state.loadingUpdateProfile = false;
      return { ...state };

    // followingteam

    case Types2.CLEAR_TEAM_FOLLOWING_TEAM:
      state.followingTeam.teams = [];
      return { ...state };
    case Types.FOLLOWING_TEAM_LIST_REQUEST:
      state.followingTeam.loadMore = true;
      state.followingTeam.loading = true;
      state.followingTeam.is_empty = false;
      if (action.page === 1) {
        state.followingTeam.teams = [];
      }
      return { ...state };
    case Types.FOLLOWING_TEAM_LIST_SUCCESS:
      state.followingTeam.loading = false;
      if (action.page == 1) {
        state.followingTeam.teams = action.data;
      } else {
        state.followingTeam.teams = state.followingTeam.teams.concat(
          action.data,
        );
      }
      state.followingTeam.loadMore =
        action.page < action.last_page ? true : false;
      state.followingTeam.is_empty = action.total === 0 ? true : false;
      return { ...state };
    case Types.FOLLOWING_TEAM_LIST_FAILURE:
      state.followingTeam.loadMore = false;
      state.followingTeam.loading = false;
      return { ...state };
    //
    case Types.TEAM_FOLLOW_REQUEST:
      state.followingTeam.loadingFollowers = true;
      state.followingTeam.teamIdAction = action.teamId;
      return { ...state };
    case Types.TEAM_FOLLOW_SUCCESS:
      state.followingTeam.loadingFollowers = false;
      state.followingTeam.teamIdAction = 0;
      var teams = state.followingTeam.teams;
      var newTeams = teams.map(team => {
        if (team.Id == action.teamId) {
          team.IsFollow = team.IsFollow ? false : true;
        }
        return team;
      });
      state.followingTeam.teams = newTeams;
      return { ...state };
    case Types.TEAM_FOLLOW_FAILURE:
      state.followingTeam.loadingFollowers = false;
      state.followingTeam.teamIdAction = 0;
      return { ...state };

    //

    case Types2.HOSTEAM_LIST_REQUEST:
      state.hostingEvent.loading = true;
      return { ...state };
    case Types2.HOSTEAM_LIST_SUCCESS:
      let eventList = action.data;
      if (eventList.current_page == 1) {
        state.hostingEvent.events = eventList.data;
      } else {
        state.hostingEvent.events = state.hostingEvent.events.concat(
          eventList.data,
        );
      }
      state.hostingEvent.is_empty = !eventList.total ? true : false;

      if (eventList.current_page < eventList.last_page) {
        state.hostingEvent.loadMore = true;
      } else {
        state.hostingEvent.loadMore = false;
      }
      state.hostingEvent.current_page = eventList.current_page;
      state.hostingEvent.loading = false;
      return { ...state };
    case Types2.HOSTEAM_LIST_FAILURE:
      state.hostingEvent.loading = false;
      return { ...state };
    //past

    case Types2.CLEAR_PAST_EVENT_LIST_HOST_EVENT:
      state.hostingEvent.past_loading = true;
      return { ...state };
    case Types2.PAST_HOSTEAM_LIST_REQUEST:
      if (action.page === 1) {
        state.hostingEvent.past_events = [];
      }
      return { ...state };
    case Types2.PAST_HOSTEAM_LIST_SUCCESS:
      let past_eventList = action.data;
      state.hostingEvent.past_events = state.hostingEvent.past_events.concat(
        past_eventList.data,
      );
      state.hostingEvent.past_is_empty = !past_eventList.total ? true : false;
      if (past_eventList.current_page < past_eventList.last_page) {
        state.hostingEvent.past_loadMore = true;
      } else {
        state.hostingEvent.past_loadMore = false;
      }
      state.hostingEvent.past_current_page = past_eventList.current_page;
      state.hostingEvent.past_loading = false;
      return { ...state };
    case Types2.PAST_HOSTEAM_LIST_FAILURE:
      state.hostingEvent.past_loading = false;
      return { ...state };

    //
    case Types.LIKE_EVENT_REQUEST:
    case Types.LIKE_EVENT_FAILURE:
      if (state.hostingEvent.events) {
        var newData = state.hostingEvent.events;
        newData.map((data, key) => {
          if (data.Id === action.id) {
            data.Like = !data.Like;
          }
        });
        state.hostingEvent.events = newData;
      }
      if (state.hostingEvent.past_events) {
        var newData = state.hostingEvent.past_events;
        newData.map((data, key) => {
          if (data.Id === action.id) {
            data.Like = !data.Like;
          }
        });
        state.hostingEvent.past_events = newData;
      }
      if (state.profileData.events) {
        var newData = state.profileData.events;
        newData.map((data, key) => {
          if (data.Id === action.id) {
            data.Like = !data.Like;
          }
        });
        state.profileData.events = newData;
      }

      return { ...state };
    case Types2.LOAD_DATA_MY_PROFILE_REQUEST:
      state.loadingProfile = true;
      return { ...state };
    case Types2.LOAD_DATA_MY_PROFILE_SUCCESS:
      state.loadingProfile = false;
      state.profileData = action.data;
      return { ...state };
    case Types2.CLEAR_EVENT_LIST_HOSTEAM:
      state.hostingEvent.events = [];
      return { ...state };
    case Types.CHECK_LOGIN_REQUEST:
      state.check_login_loading = true;
      state.error_status = '';
      return { ...state };
    case Types.CHECK_LOGIN_SUCCESS:
      state.check_login_loading = false;
      state.profile = action.data;
      return { ...state };
    case Types.CHECK_LOGIN_FAILURE:
      state.check_login_loading = false;
      state.error_status = action.error_status;
      return { ...state };
    case Types2.UPDATE_NEW_NOTIFICATION:
      if (!state.profile.IsNewNotification) {
        state.profile.IsNewNotification = true;
      }
      return { ...state };
    case Types2.UPDATE_OLD_NOTIFICATION:
      if (state.profile.IsNewNotification) {
        state.profile.IsNewNotification = false;
      }
      return { ...state };
    case Types2.LOAD_DATA_TEAM_REQUEST_REQUEST:
      state.loadMore = true;
      state.is_empty = false;
      if (action.page === 1) {
        state.listTeamRequest = [];
      }
      return { ...state };
    case Types2.LOAD_DATA_TEAM_REQUEST_SUCCESS:
      state.loading = false;
      state.listTeamRequest =
        action.page === 1
          ? action.data
          : state.listTeamRequest.concat(action.data);
      state.loadMore =
        state.listTeamRequest.length < action.total ? true : false;
      state.is_empty = action.total === 0 ? true : false;
      return { ...state };
    case Types2.LOAD_DATA_TEAM_REQUEST_FAILURE:
      state.loadMore = false;
      return { ...state };
    case Types2.LOAD_DATA_EVENT_REQUEST_REQUEST:
      state.loadMore = true;
      state.is_empty = false;
      if (action.page === 1) {
        state.listEventRequest = [];
      }
      return { ...state };
    case Types2.LOAD_DATA_EVENT_REQUEST_SUCCESS:
      state.loading = false;
      state.listEventRequest =
        action.page === 1
          ? action.data
          : state.listEventRequest.concat(action.data);
      state.loadMore =
        state.listEventRequest.length < action.total ? true : false;
      state.is_empty = action.total === 0 ? true : false;
      return { ...state };
    case Types2.LOAD_DATA_EVENT_REQUEST_FAILURE:
      state.loadMore = false;
      return { ...state };
    case Types2.LOAD_DATA_TEAM_REQUEST_FAILURE:
      state.loadMore = false;
      return { ...state };
    case Types2.LOAD_DATA_PURCHASE_HISTORY_REQUEST:
      state.loadMore = true;
      state.is_empty = false;
      if (action.page === 1) {
        state.listPurchaseHistory = [];
      }
      return { ...state };
    case Types2.LOAD_DATA_PURCHASE_HISTORY_SUCCESS:
      state.listPurchaseHistory =
        action.page === 1
          ? action.data
          : state.listPurchaseHistory.concat(action.data);
      state.loadMore =
        state.listPurchaseHistory.length < action.total ? true : false;
      state.is_empty = action.total === 0 ? true : false;
      return { ...state };
    case Types2.LOAD_DATA_PURCHASE_HISTORY_FAILURE:
      state.loadMore = false;
      return { ...state };
    case Types2.LOAD_DATA_USER_SALE_REPORT_REQUEST:
      state.is_empty = true;
      state.loadMoreSaleReport = true;
      if (action.page === 1) {
        state.saleReport = [];
      }
      return { ...state };
    case Types2.LOAD_DATA_USER_SALE_REPORT_SUCCESS:
      if (action.page === 1) {
        state.saleReport = action.data;
      } else {
        state.saleReport.Event = state.saleReport.Event.concat(
          action.data.Event,
        );
      }
      state.loadMoreSaleReport =
        state.saleReport.Event.length < action.total ? true : false;
      state.is_empty = action.total === 0 ? true : false;
      return { ...state };
    case Types2.LOAD_DATA_USER_SALE_REPORT_FAILURE:
      state.is_empty = false;
      state.loadMoreSaleReport = false;
      return { ...state };
    case Types2.USER_ACCEPT_EVENT_REQUEST:
      var newData = state.listEventRequest;
      newData.map((data, key) => {
        if (data.Id === action.Id) {
          data.isAccept = true;
        }
      });
      state.listEventRequest = newData;
      return { ...state };
    case Types2.USER_ACCEPT_EVENT_FAILURE:
      var newData = state.listEventRequest;
      newData.map((data, key) => {
        if (data.Id === action.Id) {
          data.isAccept = false;
        }
      });
      state.listEventRequest = newData;
      return { ...state };
    case Types2.USER_DENY_EVENT_REQUEST:
      var newData = state.listEventRequest;
      newData.map((data, key) => {
        if (data.Id === action.Id) {
          data.isDeny = true;
        }
      });
      state.listEventRequest = newData;
      return { ...state };
    case Types2.USER_DENY_EVENT_FAILURE:
      var newData = state.listEventRequest;
      newData.map((data, key) => {
        if (data.Id === action.Id) {
          data.isDeny = false;
        }
      });
      state.listEventRequest = newData;
      return { ...state };
    case Types2.USER_ACCEPT_TEAM_REQUEST:
      var newData = state.listTeamRequest;
      newData.map((data, key) => {
        if (data.Id === action.Id) {
          data.isAccept = true;
        }
      });
      state.listTeamRequest = newData;
      return { ...state };
    case Types2.USER_ACCEPT_TEAM_FAILURE:
      var newData = state.listTeamRequest;
      newData.map((data, key) => {
        if (data.Id === action.Id) {
          data.isAccept = false;
        }
      });
      state.listTeamRequest = newData;
      return { ...state };
    case Types2.USER_DENY_TEAM_REQUEST:
      var newData = state.listTeamRequest;
      newData.map((data, key) => {
        if (data.Id === action.Id) {
          data.isDeny = true;
        }
      });
      state.listTeamRequest = newData;
      return { ...state };
    case Types2.USER_DENY_TEAM_FAILURE:
      var newData = state.listTeamRequest;
      newData.map((data, key) => {
        if (data.Id === action.Id) {
          data.isDeny = false;
        }
      });
      state.listTeamRequest = newData;
      return { ...state };
    case Types.VERIFY_EMAIL_REQUEST:
      state.isVerifyEmail = true;
      return { ...state };
    case Types.VERIFY_EMAIL_SUCCESS:
      state.isVerifyEmail = false;
      return { ...state };
    case Types.VERIFY_EMAIL_FAILURE:
      state.isVerifyEmail = false;
      return { ...state };
    case Types.CLEAR_TEAM_INVITE_SEARCH:
      state.searchMembers = [];
      state.loadingSearch = false;
      return { ...state };
    case Types.UPDATE_SETTING_REQUEST:
      var key = Object.keys(action.body)[0];
      state.profile.GeneralSetting[key] = action.body[key];
      return { ...state };
    case Types.UPDATE_SETTING_FAILURE:
      var key = Object.keys(action.body)[0];
      state.profile.GeneralSetting[key] =
        state.profile.GeneralSetting[key] == 0 ? 1 : 0;
      return { ...state };
    case Types.CHANGE_PASSWORD_SUCCESS:
      state.profile.IsPassword = true;
      return { ...state };
    case Types.SHOW_MODAL_SETTING_TEAM:
      state.team = action.team;
      state.modalRegisterTeam = true;
      return { ...state };
    case Types.HIDE_MODAL_SETTING_TEAM:
      state.modalRegisterTeam = false;
      return { ...state };
    case Types2.ACCOUNT_DELETE_REQUEST:
      state.loading = true;
      return { ...state };
    case Types2.ACCOUNT_DELETE_SUCCESS:
      state.loading = false;
      state.profile = {};
      return { ...state };
    case Types2.ACCOUNT_DELETE_FAILURE:
      state.loading = false;
      return { ...state };
    case Types2.HOSTVENUE_LIST_REQUEST:
      state.hostingVenue.loading = true;
    case Types2.HOSTVENUE_LIST_SUCCESS:
      if (action.data) {
        state.hostingVenue.loading = false;
        state.hostingVenue.load_more = action.load_more
        if (action.page === 1) {
          state.hostingVenue.venue = action.data
        } else {
          state.hostingVenue.venue = state.hostingVenue.venue.concat(action.data)
        }
      }
      return { ...state };
    case Types2.HOSTVENUE_LIST_FAILURE:
      state.hostingVenue.loading = false;
      return { ...state };
    default:
      return { ...state };
  }
};

export default user;
