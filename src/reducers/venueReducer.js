import * as Types from '../constants/ActionType';
import * as Types2 from '../constants/ActionType2';
const initialState = {
  loading: false,
  loadingPageList: false,
  venue: {},
  venueHostingEvent: {
    loading: false,
    data: [],
    total: 1,
  },
  photoDetail: [],
  checkPhotoDetail: false,
  venues: [],
  liked: [],
  errors: {},
  loadMore: false,
  is_empty: false,
  last_pageList: 1,
  isFetching: false,
  last_pageMap: 1,
  venueList: [],
  venueMap: [],
  venue_news: [],
  loadingCreateVenueNews: false,
  loadingDeleteVenueNews: false,
  loadingEditVenueNews: false,
  itemVenueNewsEditing: null,
  photo: {
    loading: false,
    photo: [],
    page: 1,
  },
  venueNewsDetail: {},
  loadingReport: false,
  loadMoreList: false,
  is_venue_empty: false,
  last_page: 1,
  loadingSetIns: false,
  loadingImagesIns: false,
  InstagramImages: [],
};

var findIndex = (datas, id) => {
  var result = -1;
  datas.forEach((data, index) => {
    if (data.Id === id) {
      result = index;
      return result;
    }
  });
  return result;
};

const venue = (state = initialState, action) => {
  var index = -1;
  switch (action.type) {
    case Types.VENUE_LIST_REQUEST:
      state.loading = true;
      return { ...state };
    case Types.VENUE_LIST_SUCCESS:
      state.loading = false;
      if (action.page == 1) {
        var venues = action.data;
      } else {
        var venues = state.venues.concat(action.data);
      }
      state.venues = venues;
      return { ...state };
    case Types.VENUE_LIST_FAILURE:
      state.loading = false;
      return { ...state };

    // venue list
    case Types2.LOAD_DATA_LIST_VENUE_REQUEST:
      if (action.check === 1) {
        state.loadingPageList = true;
      } else {
        state.loading = true;
      }

      return { ...state };

    case Types2.LOAD_DATA_LIST_VENUE_SUCCESS:
      // console.log('state.venues', state.venues)
      // console.log('action.data', action.data)
      if (action.check === 1) {
        state.loadingPageList = false;
      } else {
        state.loading = false;
      }
      if (action.page === 1) {
        var venueList = action.data;
      } else {
        var venueList = state.venueList.concat(action.data);
      }
      state.venueList = venueList;
      state.last_page = action.last_page;
      // state.loadMoreList = state.venueList.length < action.last_page ? true : false;
      // state.is_venue_empty = action.last_page === 0 ? true : false;
      return { ...state };

    case Types2.LOAD_DATA_LIST_VENUE_FAILURE:
      state.loading = false;
      state.loadingPageList = false;
      return { ...state };
    case Types2.LOAD_DATA_LIST_CLEAR:
      state.loading = false;
      state.venueList = [];
      state.last_page = 1;
      return { ...state };
    case Types.LOAD_LIKED_VENUE_FAILURE:
      state.loading = false;
      return { ...state };
    // case Types.UPDATE_LIKED_VENUE_HOME:
    //     state.venues[action.data].IsLike = !state.venues[action.data].IsLike
    //     return { ...state };
    case Types.SAVE_VENUE_NEWS_REQUEST:
      state.loadingCreateVenueNews = true;
      return { ...state };
    case Types.SAVE_VENUE_NEWS_SUCCESS:
      // state.isFetching = false;
      // state.is_empty = false;
      // state.venue_news.unshift(action.data)
      // return { ...state };
      state.loadingCreateVenueNews = false;
      var newVenueNews = state.venue_news;
      newVenueNews.unshift(action.data);
      return { ...state };
    case Types.SAVE_VENUE_NEWS_FAILURE:
      state.loadingCreateVenueNews = false;
      return { ...state };
    case Types.VENUE_DETAIL_REQUEST:
      state.loading = true;
      return { ...state };
    case Types.VENUE_DETAIL_SUCCESS:
      state.loading = false;
      state.venue = action.data;
      return { ...state };
    case Types.VENUE_DETAIL_FAILURE:
      state.loading = false;
      return { ...state };
    case Types.VENUEMAP_LIST_REQUEST:
      state.loading = true;
      return { ...state };
    case Types.VENUEMAP_LIST_SUCCESS:
      state.loading = false;
      // if (action.page == 1) {
      //     var venues = action.data;
      // } else {
      //     var venues = state.venueMap.concat(action.data)
      // }
      var venues = action.data;
      state.venueMap = venues;
      // state.last_pageMap = action.last_page;
      return { ...state };
    case Types.VENUEMAP_CLEAR:
      state.loading = false;
      state.last_pageMap = 1;
      state.venueMap = [];
      return { ...state };
    case Types.VENUEMAP_LIST_FAILURE:
      state.loading = false;
      return { ...state };
    case Types.DELETE_VENUE_NEWS_REQUEST:
      state.loadingDeleteVenueNews = true;
      return { ...state };
    case Types.DELETE_VENUE_NEWS_SUCCESS:
      index = findIndex(state.venue_news, action.id);
      if (index !== -1) {
        state.venue_news.splice(index, 1);
      }
      if (state.venue.venue_news) {
        index = findIndex(state.venue.venue_news, action.id);
        if (index !== -1) {
          state.venue.venue_news.splice(index, 1);
        }
      }
      state.loadingDeleteVenueNews = false;
      return { ...state };
    case Types.DELETE_VENUE_NEWS_FAILURE:
      state.loadingDeleteVenueNews = false;
      return { ...state };
    case Types.EDIT_VENUE_NEWS_REQUEST:
      state.loadingEditVenueNews = true;
      return { ...state };
    case Types.EDIT_VENUE_NEWS_SUCCESS:
      state.loadingEditVenueNews = false;
      state.itemVenueNewsEditing = action.data;
      return { ...state };
    case Types.EDIT_VENUE_NEWS_FAILURE:
      state.loadingEditVenueNews = false;
      return { ...state };
    case Types.UPDATE_VENUE_NEWS_REQUEST:
      state.isFetching = true;
      return { ...state };
    case Types.UPDATE_VENUE_NEWS_FAILURE:
      state.isFetching = false;
      return { ...state };
    case Types.UPDATE_VENUE_NEWS_SUCCESS:
      state.isFetching = false;
      index = findIndex(state.venue_news, action.id);
      if (index !== -1) {
        state.venue_news[index].Contents = action.data.Contents;
        state.venue_news[index].ContentShort = action.data.ContentShort;
        state.venue_news[index].Images = action.data.Images;
        state.venue_news[index].Link = action.data.Link;
      }
      return { ...state };
    case Types.UPDATE_VENUE_NEWS_LIST_SUCCESS:
      state.isFetching = false;
      if (state.venue.venue_news) {
        index = findIndex(state.venue.venue_news, action.id);
        if (index !== -1) {
          state.venue.venue_news[index].Contents = action.data.Contents;
          state.venue.venue_news[index].ContentShort = action.data.ContentShort;
          state.venue.venue_news[index].Images = action.data.Images;
          state.venue.venue_news[index].Link = action.data.Link;
        }
      }
      return { ...state };
    case Types.LOAD_DATA_VENUE_NEWS_REQUEST:
      state.loadMore = true;
      state.loading = true;
      state.is_empty = false;
      return { ...state };
    case Types.LOAD_DATA_VENUE_NEWS_SUCCESS:
      state.loading = false;
      state.venue_news =
        action.page === 1 ? action.data : state.venue_news.concat(action.data);
      state.loadMore = state.venue_news.length < action.total ? true : false;
      state.is_empty = action.total === 0 ? true : false;
      return { ...state };
    case Types.LOAD_DATA_VENUE_NEWS_FAILURE:
      state.loadMore = false;
      state.loading = false;
      return { ...state };
    case Types.VENUE_UPLOAD_REQUEST:
      state.photo.loading = true;
      state.loading = true;
      return { ...state };
    case Types.VENUE_UPLOAD_SUCCESS:
      state.loading = false;
      state.photo.loading = false;
      // state.venue.Photos = [...state.venue.Photos, ...action.data]
      state.photo.photo = [...action.data, ...state.photo.photo];
      state.checkPhotoDetail = !state.checkPhotoDetail;
      return { ...state };
    case Types.VENUE_UPLOAD_FAILURE:
      state.photo.loading = false;
      state.loading = false;
      return { ...state };
    case Types.VENUE_PHOTO_REQUEST:
      state.photo.loading = false;
      return { ...state };
    case Types.VENUE_PHOTO_SUCCESS:
      state.photo.loading = false;
      state.photo.photo = [...state.photo.photo, ...action.data];
      state.photo.page = action.page;
      return { ...state };
    case Types.VENUE_PHOTO_CLEAR:
      state.photo.loading = false;
      state.photo.photo = [];
      state.photo.page = 1;
      return { ...state };
    case Types.VENUE_PHOTO_FAILURE:
      state.photo.loading = false;
      return { ...state };
    case Types.VENUE_DELETE_PHOTO_SUCCESS:
      let index = action.data;
      state.photo.photo.splice(index, 1);
      state.checkPhotoDetail = !state.checkPhotoDetail;
      return { ...state };
    case Types.VENUE_PHOTO_DETAIL_SUCCESS:
      state.photoDetail = action.data;
      return { ...state };
    case Types2.LIKE_VENUE_NEWS_REQUEST:
    case Types2.LIKE_VENUE_NEWS_FAILURE:
      var newData = state.venue_news;
      newData.map((data, key) => {
        if (data.Id === action.id) {
          data.IsLike = !data.IsLike;
        }
      });
      state.venue_news = newData;
      // if (typeof state.venueNewsDetail.IsLike !== 'undefined') {
      //     state.venueNewsDetail.IsLike = !state.venueNewsDetail.IsLike;
      // }
      return { ...state };
    case Types2.UPDATE_LIKE_VENUE_NEWS_REQUEST:
    case Types2.UPDATE_LIKE_VENUE_NEWS_FAILURE:
      if (state.venue.venue_news) {
        var newData = state.venue.venue_news;
        newData.map((data, key) => {
          if (data.Id === action.id) {
            data.IsLike = !data.IsLike;
          }
        });
        state.venue.venue_news = newData;
      }
      return { ...state };
    case Types2.VENUE_REPORT_REQUEST:
      state.loadingReport = true;
      return { ...state };
    case Types2.VENUE_REPORT_SUCCESS:
      state.loadingReport = false;
      return { ...state };
    case Types2.VENUE_REPORT_FAILURE:
      state.loadingReport = false;
      return { ...state };
    case Types2.LIKE_VENUE_REQUEST:
    case Types2.LIKE_VENUE_FAILURE:
      if (state.venueList.length > 0) {
        index = findIndex(state.venueList, action.id);
        if (index !== -1) {
          state.venueList[index].IsLike = !state.venueList[index].IsLike;
        }
      }
      if (state.venueMap.length > 0) {
        index = findIndex(state.venueMap, action.id);
        if (index !== -1) {
          state.venueMap[index].IsLike = !state.venueMap[index].IsLike;
        }
      }
      if (state.venues.length > 0) {
        index = findIndex(state.venues, action.id);
        if (index !== -1) {
          state.venues[index].IsLike = !state.venues[index].IsLike;
        }
      }
      if (state.venue.Id === action.id) {
        state.venue.IsLike = !state.venue.IsLike;
      }
      return { ...state };
    case Types2.LIKE_VENUE_SUCCESS:
      return { ...state };
    case Types.TEAM_VENUE_FOLLOW_SUCCESS:
      if (state.venue?.Team?.Id === action.teamId) {
        state.venue.Team.IsFollow = !state.venue.Team.IsFollow;
      }
      return { ...state };
    case Types2.VENUE_SET_INSTAGRAM_REQUEST:
      state.loadingSetIns = true;
      return { ...state };
    case Types2.VENUE_SET_INSTAGRAM_SUCCESS:
      state.loadingSetIns = false;
      state.InstagramImages = action.data;
      state.venue.IsInstagram = true;
      return { ...state };
    case Types2.VENUE_SET_INSTAGRAM_FAILURE:
      state.loadingSetIns = false;
      return { ...state };
    case Types2.VENUE_IMAGEINS_REQUEST:
      state.loadingImagesIns = true;
      return { ...state };
    case Types2.VENUE_IMAGEINS_SUCCESS:
      state.loadingImagesIns = false;
      state.InstagramImages = action.data;
      return { ...state };
    case Types2.VENUE_IMAGEINS_FAILURE:
      state.loadingImagesIns = false;
      return { ...state };
    case Types2.CLEAR_IMAGES_INSTAGRAM:
      state.InstagramImages = [];
      return { ...state };
    case Types2.CLEAR_VENUE_DETAIL:
      state.venue = {};
      return { ...state };
    case Types2.VENUE_REMOVE_INSTAGRAM_REQUEST:
      state.loadingSetIns = true;
      return { ...state };
    case Types2.VENUE_REMOVE_INSTAGRAM_SUCCESS:
      state.loadingSetIns = false;
      state.InstagramImages = [];
      state.venue.IsInstagram = false;
      return { ...state };
    case Types2.VENUE_REMOVE_INSTAGRAM_FAILURE:
      state.loadingSetIns = false;
      return { ...state };
    case Types.LOAD_DATA_EVENT_BY_VENUE_REQUEST:
      state.venueHostingEvent.loading = true;
      return { ...state };
    case Types.LOAD_DATA_EVENT_BY_VENUE_SUCCESS:
      state.venueHostingEvent.loading = false;
      state.venueHostingEvent.data = [
        ...state.venueHostingEvent.data,
        ...action.data,
      ];
      state.venueHostingEvent.total = action.total;
      return { ...state };
    case Types.LOAD_DATA_EVENT_BY_VENUE_FAILURE:
      state.venueHostingEvent.loading = false;
      return { ...state };
    case Types.LOAD_DATA_EVENT_BY_VENUE_CLEAR:
      state.venueHostingEvent.loading = false;
      state.venueHostingEvent.total = 1;
      state.venueHostingEvent.data = [];
      return { ...state };
    case Types.DELETE_VENUE:
      state.venues = state.venues.filter(data => data.Id !== action.Id);
      return { ...state };
    default:
      return { ...state };
  }
};

export default venue;
