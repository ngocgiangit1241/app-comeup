import * as Types from '../constants/ActionType';
const initialState = {
    loading: false,
    venue: {},
    venues: [],
    errors: {},
    venueMap: [],
    last_pageMap: 1,
    isFetching: false
};

const venue = (state = initialState, action) => {
    switch (action.type) {
        case Types.VENUEMAP_LIST_REQUEST:
            state.loading = true;
            return { ...state };
        case Types.VENUEMAP_LIST_SUCCESS:
            state.loading = false;
            // if (action.page == 1) {
            //     var venues = action.data;
            // } else {
            //     var venues = state.venueMap.concat(action.data)
            // }
            var venues = action.data;
            state.venueMap = venues;
            state.last_pageMap = action.last_page;
            return { ...state };
        case Types.VENUEMAP_LIST_FAILURE:
            state.loading = false;
            return { ...state };
        case Types.VENUE_DETAIL_REQUEST:
            state.loading = true;
            return { ...state };
        case Types.VENUE_DETAIL_SUCCESS:
            state.loading = false;
            state.venue = action.data
            return { ...state };
        case Types.VENUE_DETAIL_FAILURE:
            state.loading = false;
            return { ...state };






        // case Types.VENUE_LIST_REQUEST:
        //     state.loading = true;
        //     return { ...state };
        // case Types.VENUE_LIST_SUCCESS:
        //     state.loading = false;
        //     if (action.page == 1) {
        //         var venues = action.data;
        //     } else {
        //         var venues = state.venues.concat(action.data)
        //     }
        //     state.venues = venues;
        //     return { ...state };
        // case Types.VENUE_LIST_FAILURE:
        //     state.loading = false;
        //     return { ...state };
        // case Types.SAVE_VENUE_NEWS_REQUEST:
        //     state.isFetching = true;
        //     return { ...state };
        // case Types.SAVE_VENUE_NEWS_SUCCESS:
        //     state.isFetching = false;
        //     state.venue.VenueNews.unshift(action.data);
        //     return { ...state };
        // case Types.SAVE_VENUE_NEWS_FAILURE:
        //     state.isFetching = false;
        //     return { ...state };
        default: return { ...state };
    }
}

export default venue;