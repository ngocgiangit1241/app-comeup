import React, { Component } from 'react';
import {
    ActivityIndicator, Platform,
    View, Text, Image, TextInput, StyleSheet, Dimensions,
    ScrollView, Animated, Keyboard, Alert, KeyboardAvoidingView
} from 'react-native';
import Toast from 'react-native-simple-toast';
import Touchable from '../screens_view/Touchable'
const { width, height } = Dimensions.get('window')
import YouTube from 'react-native-youtube';
import Modal from "react-native-modal";
import ExtraDimensions from 'react-native-extra-dimensions-android';
import * as Colors from '../constants/Colors'
const base64 = require('base-64');
import ImagePicker from 'react-native-image-crop-picker';
import AndroidWebView from 'react-native-webview';
import { actCreateTeamNews, actEditTeamNews, actUpdateTeamNews } from '../actions/team';
import { connect } from "react-redux";
import ImageResizer from 'react-native-image-resizer';
import { convertLanguage } from '../services/Helper'
import * as Config from '../constants/Config';
import RNFS from 'react-native-fs';
import SafeView from '../screens_view/SafeView'
import Line from '../screens_view/Line';
import Loading from '../screens_view/Loading';
class CreateTeamNews extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            Content: '',
            PhotoId: [],
            PhotoBase: [],
            Images: [],
            isOnpress: true,
            update: true,
            Link: '',
            Type: '',
            modalWarning: false,
            modalYoutubeLink: false,
            VideoURL: '',
            LinkError: '',
            inputHeight: 0
        };
        this.keyboardHeight = new Animated.Value(0);
    }

    save = () => {
        Keyboard.dismiss();
        var { language } = this.props.language;
        var { iso_code } = this.props.global;
        this.setState({
            isOnpress: false
        })
        if (!this.validate()) {
            this.setState({
                isOnpress: true
            })
            return;
        }
        var team = this.props.team.team;
        var { Content, PhotoBase, PhotoId, Images, Link } = this.state;
        var body = {
            TeamId: team.Id,
            Content,
            PhotoBase,
            PhotoId,
            Link,
            iso_code
        }
        if (PhotoBase.length + Images.length > 10) {
            Alert.alert(
                '',
                convertLanguage(language, 'choose_no_more_than_10_photos'),
                [
                    {
                        text: convertLanguage(language, 'ok'),
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'destructive',
                    },
                ],
                { cancelable: false },
            );
            this.setState({
                isOnpress: true
            })
            return;
        }
        if (this.props.route.params['TeamNewsId']) {
            this.props.onUpdateTeamNews(this.props.route.params['TeamNewsId'], body, this.props.navigation)
        } else {
            this.props.createTeamNews(body, this.props.navigation);
        }
        setTimeout(() => {
            this.setState({
                isOnpress: true
            })
        }, 2000)
    }

    onMessage(data) {
        this.setState({ Content: data.nativeEvent.data })
    }

    sendPostMessage() {
        var data = JSON.stringify({ content: this.state.Content, placeholder: '', type: 'team_news' });
        this.webView2.postMessage(data);
    }

    renderLoading() {
        return <View style={{ width, position: 'absolute', justifyContent: 'center', alignItems: 'center', zIndex: 9, top: 20 }}>
            <ActivityIndicator size="large" color="#000000" />
        </View>

    }

    componentWillMount() {
        this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        Animated.parallel([
            Animated.timing(this.keyboardHeight, {
                duration: event.duration,
                toValue: event.endCoordinates.height,
            }),
        ]).start();
    };

    keyboardWillHide = (event) => {
        Animated.parallel([
            Animated.timing(this.keyboardHeight, {
                duration: event.duration,
                toValue: 0,
            }),
        ]).start();
    };

    componentDidMount() {
        if (this.props.route.params['TeamNewsId']) {
            this.props.onEditTeamNews(this.props.route.params['TeamNewsId'])
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.team.itemTeamNewsEditing && this.props.route.params['TeamNewsId']) {
            this.setState({
                Content: nextProps.team.itemTeamNewsEditing.Content,
                Images: nextProps.team.itemTeamNewsEditing.Images,
                PhotoId: this.getPhotoId(nextProps.team.itemTeamNewsEditing.Images),
                Link: nextProps.team.itemTeamNewsEditing.Link ? nextProps.team.itemTeamNewsEditing.Link : '',
                Type: nextProps.team.itemTeamNewsEditing.Images.length > 0 ? 'photo' : nextProps.team.itemTeamNewsEditing.Link ? 'video' : ''
            })
            // this.sendPostMessage(nextProps.team.itemTeamNewsEditing.Content)
        }
    }

    getPhotoId(Images) {
        var PhotoId = [];
        Images.forEach((Image) => {
            PhotoId.push(Image.Id)
        })
        return PhotoId;
    }

    validate() {
        var { Content, PhotoBase, Images } = this.state;
        var { language } = this.props.language;
        if (PhotoBase.length + Images.length === 0 && Content.replace(/\s/g, '').length === 0) {
            Toast.showWithGravity(convertLanguage(language, 'validate_content'), Toast.SHORT, Toast.TOP)
            return false;
        }
        return true;
    }

    checkButtonPost() {
        var { Content, PhotoBase, Images } = this.state;
        if (PhotoBase.length + Images.length === 0 && Content.replace(/\s/g, '').length === 0) {
            return false;
        }
        return true;
    }

    pickImage() {
        var { PhotoBase } = this.state;
        ImagePicker.openPicker({
            includeBase64: true,
            multiple: true,
            maxFiles: 10
        }).then((images) => {
            this.setState({ modalWarning: false })
            var data = PhotoBase;
            images.map((image, index) => {
                ImageResizer.createResizedImage(image.path, 600, 800, 'JPEG', 80).then(async (uri) => {
                    const base64image = await RNFS.readFile(uri.uri, 'base64');
                    data.push('data:image/png;base64,' + base64image)
                    this.setState({
                        PhotoBase: data,
                    });
                }).catch((er) => {
                })
            })
            setTimeout(() => {
                this.scrollView.scrollToEnd();
            }, 300);
        }).catch(e => { });
    }

    deleteImage(index) {
        var { PhotoBase, Images } = this.state;
        PhotoBase.splice(index - Images.length, 1);
        this.setState({
            PhotoBase,
        });
    }

    deleteOldImage(index) {
        var { Images, PhotoId } = this.state;
        var index2 = PhotoId.indexOf(Images[index].Id);
        if (index2 !== -1) {
            PhotoId.splice(index2, 1)
        }
        Images.splice(index, 1);
        this.setState({
            Images,
            PhotoId
        });
    }

    mergeImage() {
        var { PhotoBase, Images } = this.state;
        return Images.concat(PhotoBase)
    }

    selectVideo() {
        var { PhotoBase, Images } = this.state;
        if (PhotoBase.length > 0 || Images.length > 0) {
            this.setState({ modalWarning: true })
        } else {
            this.setState({ modalYoutubeLink: true, Type: 'video' })
        }
    }

    selectImage() {
        var { Link } = this.state;
        if (Link.length > 0) {
            this.setState({ modalWarning: true })
        } else {
            this.setState({ Type: 'photo' })
            this.pickImage()
        }
    }

    changeType() {
        var { Type } = this.state;
        if (Type === 'photo') {
            this.setState({ Images: [], PhotoBase: [], PhotoId: [], Type: 'video', modalWarning: false, modalYoutubeLink: true, })
        } else {
            this.setState({ Link: '', Type: 'photo' })
            this.pickImage()
        }
    }

    async onInsertVideo() {
        var { language } = this.props.language;
        var { VideoURL } = this.state;
        this.setState({ LinkError: '' })
        var Id = this.youtube_parser(VideoURL);
        if (Id) {
            await this.setState({ modalYoutubeLink: false, Link: Id, VideoURL: '' })
            this.scrollView.scrollToEnd();
        } else {
            this.setState({ LinkError: convertLanguage(language, 'link_not_found') });
        }
    }

    youtube_parser(url) {
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
        var match = url.match(regExp);
        return (match && match[7].length == 11) ? match[7] : false;
    }

    render() {
        var { loadingCreateTeamNews, loadingEditTeamNews, isFetching } = this.props.team;
        var { Content, Type, Link, modalWarning, modalYoutubeLink, VideoURL, LinkError, isOnpress } = this.state;
        var { language } = this.props.language;
        var arrImages = this.mergeImage();
        return (
            <>
                <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                    <View style={{ position: 'relative', zIndex: 10, justifyContent: 'space-between', flexDirection: 'row', marginRight: 20, alignItems: 'center' }}>
                        <Touchable
                            onPress={() => this.props.navigation.goBack()}
                            style={{
                                minWidth: 48, minHeight: 48, alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                            <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                        </Touchable>

                        <Touchable
                            onPress={this.save}
                            disabled={loadingCreateTeamNews || loadingEditTeamNews || isFetching || !isOnpress || !this.checkButtonPost()}
                            style={{
                                paddingTop: 8, paddingBottom: 8, paddingLeft: 12, paddingRight: 12,
                            }} >
                            <Text style={{ fontSize: 16, color: loadingCreateTeamNews || loadingEditTeamNews || isFetching || !isOnpress || !this.checkButtonPost() ? 'gray' : '#00A9F4', fontWeight: 'bold' }}>{convertLanguage(language, 'postNews')}</Text>
                        </Touchable>
                    </View>

                    <Line />

                    <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.OS === 'android' ? -444 : 0} style={{ flex: 1 }} >
                        <ScrollView style={styles.boxInput} contentContainerStyle={{ paddingBottom: 16 }} ref={(ref) => this.scrollView = ref}>
                            <TextInput
                                style={[styles.ipContent, { height: Platform.OS === 'android' ? Math.max(208, this.state.inputHeight) : 208 }]}
                                selectionColor="#bcbcbc"
                                name="Content"
                                value={Content}
                                onChangeText={(Content) => this.setState({ Content })}
                                multiline={true}
                                onContentSizeChange={(event) => {
                                    this.setState({ inputHeight: event.nativeEvent.contentSize.height })
                                }}
                            />
                            {
                                (arrImages.length > 0) &&
                                <View>
                                    <View style={styles.boxImages}>
                                        {
                                            arrImages.map((item, index) => {
                                                return <View style={[styles.boxItem, (index + 1) % 4 === 0 ? { marginRight: 0 } : {}]} key={index}>
                                                    <Image source={{ uri: item.Photos ? item.Photos.Small : item }} style={styles.image} />
                                                    <Touchable style={styles.btnClose} onPress={() => item.Photos ? this.deleteOldImage(index) : this.deleteImage(index)}>
                                                        <Image style={styles.ic_close} source={require('../assets/close_img.png')} />
                                                    </Touchable>
                                                </View>
                                            })
                                        }
                                        <Touchable onPress={() => this.pickImage()} style={[styles.boxItem, { borderWidth: 2, borderStyle: 'dashed', borderColor: '#BDBDBD', marginRight: 0, alignItems: 'center', justifyContent: 'center' }]}>
                                            <Image source={require('../assets/plus_img.png')} style={{ width: 32, height: 32 }} />
                                            <Text style={{ color: '#BDBDBD', fontSize: 16 }}>{convertLanguage(language, 'attach_file')}</Text>
                                        </Touchable>
                                    </View>
                                </View>
                            }
                            {
                                Link.length > 0 &&
                                <YouTube
                                    videoId={Link} // The YouTube video ID
                                    play={false} // control playback of video with true/false
                                    fullscreen={false} // control whether the video should play in fullscreen or inline
                                    style={{ alignSelf: 'stretch', height: (width - 16) * 9 / 16, marginHorizontal: 16, marginBottom: 16 }}
                                    apiKey={'AIzaSyASjD7n17ht9T7huOQ8V0GXbfqsz9xYWC0'}
                                />
                            }
                        </ScrollView>
                    </KeyboardAvoidingView>
                    <Animated.View style={[styles.boxBottom, { position: 'absolute', marginBottom: this.keyboardHeight, bottom: 0, left: 0, width: '100%' }]}>
                        {/* <View style={{ backgroundColor: '#E8E7E8', height: 40, justifyContent: 'center', alignItems: 'center', paddingLeft: 20, paddingRight: 20 }}> */}
                        <Touchable style={[styles.boxUploadImage]} onPress={() => this.selectImage()}>
                            <Image source={Type === 'photo' ? require('../assets/upload-img-focus.png') : require('../assets/upload-img.png')} style={styles.img_upload} />
                            <Text style={[styles.txtUpload, Type === 'photo' ? { color: '#00A9F4' } : { color: '#828282' }]}>{convertLanguage(language, 'photo')}</Text>
                        </Touchable>
                        <Touchable style={[styles.boxUploadImage]} onPress={() => this.selectVideo()}>
                            <Image source={Type === 'video' ? require('../assets/upload-video-focus.png') : require('../assets/upload-video.png')} style={styles.img_upload} />
                            <Text style={[styles.txtUpload, Type === 'video' ? { color: '#00A9F4' } : { color: '#828282' }]}>{convertLanguage(language, 'video')}</Text>
                        </Touchable>
                        {/* </View> */}
                    </Animated.View>
                    {
                        modalWarning &&
                        <Modal
                            isVisible={true}
                            backdropOpacity={0.3}
                            animationIn="zoomInDown"
                            animationOut="zoomOutUp"
                            animationInTiming={1}
                            animationOutTiming={1}
                            backdropTransitionInTiming={1}
                            backdropTransitionOutTiming={1}
                            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                        >
                            <View style={styles.modalContent}>
                                <View style={styles.rowClose}>
                                    <Touchable style={styles.btnCloseModal} onPress={() => { this.setState({ modalWarning: false }) }} >
                                        <Image style={styles.iconClose} source={require('../assets/close.png')} />
                                    </Touchable>
                                </View>
                                <View style={styles.contentModal}>
                                    <Text style={styles.txtTitle}>{convertLanguage(language, 'replace_content_news')}</Text>
                                    <View style={styles.boxButton}>
                                        <Touchable style={styles.btnNo} onPress={() => { this.setState({ modalWarning: false }) }} >
                                            <Text style={styles.txtNo}>{convertLanguage(language, 'no2')}</Text>
                                        </Touchable>
                                        <Touchable style={styles.btnNo} onPress={() => this.changeType()} >
                                            <Text style={styles.txtNo}>{convertLanguage(language, 'yes')}</Text>
                                        </Touchable>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                    }
                    {
                        modalYoutubeLink &&
                        <Modal
                            isVisible={true}
                            backdropOpacity={0.3}
                            animationIn="zoomInDown"
                            animationOut="zoomOutUp"
                            animationInTiming={1}
                            animationOutTiming={1}
                            backdropTransitionInTiming={1}
                            backdropTransitionOutTiming={1}
                            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                        >
                            <Animated.View style={[styles.modalContent, { marginBottom: this.keyboardHeight }]}>
                                <View style={[styles.rowClose, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]}>
                                    <Text style={[styles.contentModal, { fontSize: 20, fontWeight: '600', marginTop: 5 }]}>{convertLanguage(language, 'insert_video')}</Text>
                                    <Touchable style={styles.btnCloseModal} onPress={() => { this.setState({ modalYoutubeLink: false }) }} >
                                        <Image style={styles.iconClose} source={require('../assets/close.png')} />
                                    </Touchable>
                                </View>
                                <View style={[styles.contentModal, { alignItems: 'flex-start' }]}>
                                    <Text style={[styles.txtTitle, { textAlign: 'left', marginBottom: 5, fontWeight: '600' }]}>{convertLanguage(language, 'youtube_video_url')}</Text>
                                    <TextInput
                                        style={styles.ipURL}
                                        selectionColor="#bcbcbc"
                                        name="VideoURL"
                                        value={VideoURL}
                                        onChangeText={(VideoURL) => this.setState({ VideoURL })}
                                    />
                                    {
                                        LinkError.length > 0 &&
                                        <Text style={styles.txtError}>{LinkError}</Text>
                                    }
                                    <Touchable disabled={VideoURL.length === 0} style={styles.btnInsert} onPress={() => this.onInsertVideo()} >
                                        <Text style={styles.txtInsert}>{convertLanguage(language, 'btn_insert_video')}</Text>
                                    </Touchable>
                                </View>
                            </Animated.View>
                        </Modal>
                    }
                </SafeView>
                {
                    (loadingCreateTeamNews || loadingEditTeamNews || isFetching) &&
                    <Loading />
                }
            </>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        // flex: 1,
    },
    boxInput: {
        paddingTop: 24,
        marginBottom: 56,
    },
    boxImages: {
        marginBottom: 5,
        marginHorizontal: 16,
        flexDirection: 'row',
        flexWrap: 'wrap',
        flexGrow: 4,
    },
    boxItem: {
        width: (width - 80) / 4,
        height: (width - 80) / 4,
        marginRight: 16,
        borderRadius: 4,
        marginBottom: 16,
    },
    image: {
        width: (width - 80) / 4,
        height: (width - 80) / 4
    },
    btnClose: {
        position: 'absolute',
        top: -8,
        right: -8,
        width: 16,
        height: 16,
        alignItems: 'center',
        justifyContent: 'center',
    },
    ic_close: {
        width: 16,
        height: 16
    },
    boxUploadImage: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        flex: 0.5
        // marginTop: 15
    },
    img_upload: {
        width: 24,
        height: 24,
        marginRight: 4
    },
    ipContent: {
        fontSize: 15,
        color: '#333333',
        padding: 8,
        height: 208,
        textAlignVertical: 'top',
        borderWidth: 1,
        borderColor: '#E0E0E0',
        marginHorizontal: 16,
        borderRadius: 4,
        marginBottom: 16
    },
    boxControl: {
        paddingTop: 10,
        alignItems: 'flex-end',
        justifyContent: 'center',
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    boxInsert: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#03a9f4',
        backgroundColor: '#03a9f4'
    },
    txtInsert: {
        color: '#FFFFFF',
        fontSize: 15,
        fontWeight: 'bold',
        paddingLeft: 5,
        paddingRight: 5
    },
    boxCancel: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        marginRight: 5,
    },
    txtCancel: {
        color: '#333333',
        fontSize: 14,
        fontWeight: 'bold',
        paddingLeft: 5, paddingRight: 5
    },
    boxBottom: {
        // flex: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: -2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 4,
        height: 56,
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#FFFFFF'
    },
    txtUpload: {
        color: '#00A9F4',
        fontSize: 16,
        fontWeight: '600'
    },
    modalContent: {
        backgroundColor: "white",
        borderRadius: 5
    },
    contentModal: {
        padding: 22,
        justifyContent: "center",
        borderColor: "rgba(0, 0, 0, 0.3)",
        paddingTop: 10,
        alignItems: 'center'
    },
    rowClose: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    btnCloseModal: {
        margin: 10
    },
    iconClose: {
        width: 30,
        height: 30,
    },
    txtTitle: {
        fontSize: 14,
        color: '#333333',
        textAlign: 'center'
    },
    boxButton: {
        marginTop: 24,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    btnDiscard: {
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        flex: 0.45,
        borderWidth: 1,
        borderColor: '#EB5757'
    },
    txtDiscard: {
        color: '#EB5757',
        fontSize: 16
    },
    btnNo: {
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        flex: 0.45,
        borderWidth: 1,
        borderColor: '#4F4F4F'
    },
    txtNo: {
        color: '#4F4F4F',
        fontSize: 16
    },
    ipURL: {
        fontSize: 15,
        color: '#333333',
        padding: 8,
        borderWidth: 1,
        borderColor: '#E0E0E0',
        borderRadius: 4,
        width: '100%'
    },
    btnInsert: {
        alignSelf: 'flex-end',
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        backgroundColor: '#03a9f4',
        marginTop: 16,
        paddingHorizontal: 16
    },
    txtInsert: {
        color: '#FFFFFF',
        fontSize: 16,
    },
    txtError: {
        fontSize: 12,
        color: 'red',
        marginTop: 5
    }
});

const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language,
        global: state.global,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        createTeamNews: (body, navigation) => {
            dispatch(actCreateTeamNews(body, navigation))
        },
        onEditTeamNews: (Id) => {
            dispatch(actEditTeamNews(Id))
        },
        onUpdateTeamNews: (Id, body, navigation) => {
            dispatch(actUpdateTeamNews(Id, body, navigation))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CreateTeamNews);