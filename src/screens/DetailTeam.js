import React, { Component } from 'react';
import {
    Dimensions,
    View, Text, Image, StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    Alert,
    Platform,
    ScrollView
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import Modal from "react-native-modal";
import SafeView from '../screens_view/SafeView';
import Touchable from '../screens_view/Touchable';
import ScrView from '../screens_view/ScrView';
import Line from '../screens_view/Line';
import TeamNewsItem from '../components/team/TeamNewsItem';
import * as Colors from '../constants/Colors';
import ModalAction from '../components/team/ModalAction';
import ModalContact from '../components/team/ModalContact';
import ModalZoomImage from '../components/team/ModalZoomImage';
import ModalTeamNews from '../components/team/ModalTeamNews';
import ModalActionInstagram from '../components/team/ModalActionInstagram';
import InstagramLogin from 'react-native-instagram-login';
import Share from 'react-native-share';
import FastImage from 'react-native-fast-image';
import { getTag, convertLanguage } from '../services/Helper';
import TouchableLoading from '../components/view/TouchableLoading';
import { actGlobalCheckLogin } from '../actions/global';
import ItemEventSlide from '../components/event/ItemEventSlide';
import ModalDetail from '../components/team/ModalDetail';
const { width, height } = Dimensions.get('window');
import { actTeamDetail, actTeamNews, actImagesIns, actSetTokenIns, actTeamFollow, actLikeTeamNews, actDeleteTeamNews, actRemoveTokenIns } from '../actions/team';
import { actLikeEvent } from '../actions/event';
import { connect } from "react-redux";
import * as Config from '../constants/Config';
import AutoHeightWebView from 'react-native-autoheight-webview';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import ModalTeamNewsDetail from '../components/team/ModalTeamNewsDetail';
import VenueSlideItem from '../components/venue/VenueSlideItem';
import Loading from '../screens_view/Loading';
import Error404 from '../screens_view/404Error';
import { actLikeVenues } from '../actions/venue';
import Error500 from '../screens_view/500Error';
import Forcuse from '../hook/Forcuse'
import VenueManagingItem from '../components/venue/VenueManagingItem'
class DetailTeam extends Component {
    constructor(props) {
        super(props);
        var id = ''
        // const { id } = this.props.route.params
        if (this.props.route?.params?.params?.id !== undefined) {
            id = this.props.route.params.params['id']
        } else {
            id = this.props.route.params['id']
        }
        this.srcView = React.createRef();

        this.state = {
            refreshing: false,
            avatarSource: null,
            tokenInstagram: null,
            hasTokenIns: true,
            loadingData: true,
            loadingInviteOrFollow: false,
            id: id,
            arrTeamNews: [],
            arrImgInstagram: [],
            modalAction: false,
            modalContact: false,
            modalZoom: false,
            zoomeImageIndex: 0,
            isLoadTeamNew: false,
            TeamNewsId: '',
            isViewAll: true,
            modalZoomImage: false,
            zoomImageIndex2: 0,
            modelImage: false,
            modalDetail: false,
            modalTeamNewsDetail: false,
            teamnews: {},
            Role: '',
            focuse: false,
            scrolly: 0,
            scrolly2: 0
        };
    }

    componentWillUnmount() {
        // if (Platform.OS === 'android') {
        //     this._unsubscribe();
        //     this.setState({ focuse: false })
        // }
    }
    componentDidMount() {
        // if (Platform.OS === 'android') {
        //     if (this.srcView?.current !== null) {
        //         console.log('fsfsfsf', this.state.scrolly2);
        //         setTimeout(() => {
        //             this.srcView?.current?.scrollTo({ x: 0, y: this.state.scrolly2, animated: true })
        //         }, 100);
        //     }
        // }
        // this.setState({ focuse: true })
        // if (Platform.OS === 'android') {
        //     this._unsubscribe = this.props.navigation.addListener('focus', () => {
        //         // do something
        //         // this.setState({ focuse: true })
        //         if (this.srcView?.current !== null) {
        //             setTimeout(() => {
        //                 this.srcView?.current?.scrollTo({ x: 0, y: this.state.scrolly2, animated: true })
        //             }, 1);
        //         }
        //         // this.srcView?.getNode().scrollTo({ y: this.state.scrolly2, animated: false })
        //     });
        // }
        var { iso_code } = this.props.global;
        var notification = '';
        if (this.props.route.params['screen']) {
            notification = this.props.route.params.params['notification']
        } else {
            notification = this.props.route.params['notification']
        }
        this.props.getTeamDetail(this.state.id, notification, iso_code);
        if (this.props.route.params && this.props.route.params['screen'] && this.props.route.params['screen'] === 'members') {
            this.props.navigation.navigate('TeamMember', { Slug: this.state.id })
        }
    }

    // componentDidUpdate() {
    //     if (Platform.OS === 'android') {
    //         if (this.srcView?.current !== null && this.props.isShowWebview) {
    //             setTimeout(() => {
    //                 this.srcView?.current?.scrollTo({ x: 0, y: this.state.scrolly2, animated: true })
    //             }, 1);
    //         }
    //     }
    // }

    refresh() {
        var { iso_code } = this.props.global;
        this.props.getTeamDetail(this.state.id, this.props.route.params['notification'], iso_code);
    }

    share() {
        let team = this.props.team.team;
        var { language } = this.props.language;
        let options = {
            title: team.Name,
            message: team.Name + `\n${convertLanguage(language, 'find_and_follow_us_in_comeup')}\n`,
            url: Config.MAIN_URL + 'hosts/' + team.Slug,
            subject: "Follow us" //  for email
        };
        Share.open(options)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }

    // loadDataWhenScroll = () => {
    //     let team = this.props.team.team;
    //     if (!this.state.isLoadTeamNew && team) {
    //         this.setState({ isLoadTeamNew: true });
    //         this.props.getTeamNews(team.Id, this.props.navigation);
    //     }
    //     if (!this.state.isLoadImageIns && team && team.IsInstagram) {
    //         this.setState({ isLoadImageIns: true });
    //         this.props.getImageIns(team.Id);
    //     }
    // }

    setTokenIns = (token) => {
        let team = this.props.team.team;
        this.props.setTokenIns(team.Id, token);
    }

    logout = () => {
        // Cookie.clear().then(() => {
        this.setState({ tokenInstagram: null })
        // })
    }

    inviteOrFollow = (IsFollow) => {
        // this.props.onTeamFollow(this.props.team.team.Id)
        if (IsFollow) {
            Alert.alert(
                // "Alert Title",
                '',
                convertLanguage(this.props.language.language, 'unfollow'),
                [
                    {
                        text: convertLanguage(this.props.language.language, 'cancel'),
                        onPress: () => console.log("Cancel Pressed"),
                        style: "destructive"
                    },
                    { text: convertLanguage(this.props.language.language, 'ok'), onPress: () => this.props.onTeamFollow(this.props.team.team.Id) }
                ],
                { cancelable: false }
            );
        } else {
            this.props.onTeamFollow(this.props.team.team.Id);
        }
    }

    getDefaultAboutTeam() {
        const team = this.props.team.team;
        if (!team) {
            return '';
        }
        if (team.AboutTeam) {
            return team.AboutTeam;
        } else {
            return '<p style="color: #333333;"><b>Country:</b> ' + team.CountryName + '</p><p style="color: #333333;"><b>Phone:</b> ' + team.Zipcode + '' + team.Phone + '</p><p style="color: #333333;"><b>Email:</b> ' + team.Email + '</p>';
        }
    }

    renderLoading() {
        return <ActivityIndicator size="large" color="#000000" />
    }

    onLoadEnd() {
        setTimeout(() => {
            this.refs.content.measureInWindow((ox, oy, width, height) => {
                this.setState({
                    webviewHeight: height > 300 ? 300 : height,
                    isViewAll: height > 300 ? false : true,
                    webviewParentHeight: height > 300 ? 301 : 0,
                })
            });
        }, 100);
    }

    createAbout = () => {
        var { language } = this.props.language;
        var { isViewAll } = this.state;
        const team = this.props.team.team;
        if (!team) return null
        if (team.Role != 'Guest' && !team.AboutTeam) {
            return <Touchable
                onPress={() => this.props.navigation.navigate('CreateIntroTeam', {
                    slug: team.Code,
                    aboutTeam: this.getDefaultAboutTeam(),
                    callback: (team) => this.setState({ team })
                })}
                style={styles.btnRegister}>
                <Text style={styles.txtBtnRegister}>+ {convertLanguage(language, 'register_about')}</Text>
            </Touchable>
        } else if (team.AboutTeam) {
            return <>
                <View style={{ flexDirection: 'row', marginLeft: 16, marginTop: 10, marginBottom: 10, justifyContent: 'space-between', alignItems: 'flex-end' }}>
                    <Text style={{ color: '#333333', fontSize: 20, fontWeight: 'bold' }}>{convertLanguage(language, 'about_team')}</Text>
                    {
                        team.Role != 'Guest' &&
                        <TouchableOpacity
                            onPress={() => {
                                this.clearWebview(), this.props.navigation.navigate('CreateIntroTeam', {
                                    slug: team.Code,
                                    aboutTeam: team.AboutTeam,
                                    callback: (team) => this.setState({ team })
                                })
                            }}
                            style={{ marginRight: 16, alignItems: 'flex-end' }}
                        >
                            <Text>{convertLanguage(language, 'edit')}</Text>
                        </TouchableOpacity>
                    }
                </View>
                <View ref="content" style={isViewAll ? [{ overflow: 'hidden', marginHorizontal: 16 }, this.state.webviewHeight > 0 ? { height: this.state.webviewHeight } : {}] : [{ overflow: 'hidden', width: '100%', marginLeft: 20, marginRight: 20 }, this.state.webviewParentHeight > 0 ? { height: this.state.webviewParentHeight } : {}]}>
                    {
                        // this.props.isShowWebview &&
                        // <AutoHeightWebView startInLoadingState={true} scalesPageToFit={false} zoomable={false} scrollEnabled={false} renderLoading={() => this.renderLoading()} onLoadEnd={() => this.onLoadEnd()} width={width - 40} source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 40}px; overflow-wrap: break-word} iframe {width: ${width - 40}px; height: ${(width - 40) * 9 / 16}px} img { width: ${width - 40}px !important; margin-bottom: 5px; margin-top: 5px;}</style><body>` + team.AboutTeam.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') + `</body></html>` }} originWhitelist={['*']} />
                        <AutoHeightWebView
                            startInLoadingState={true}
                            scalesPageToFit={false} zoomable={false}
                            scrollEnabled={false} renderLoading={() => this.renderLoading()}
                            width={width - 32}
                            // ref={ref => this.webview = ref}
                            // androidHardwareAccelerationDisabled={true}
                            useWebkit={true}
                            // cacheMode={'LOAD_CACHE_ELSE_NETWORK'}
                            source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 32}px; overflow-wrap: break-word} iframe {width: ${width - 32}px; height: ${(width - 32) * 9 / 16}px} img {width: ${width - 32}px !important; margin-bottom: 5px; margin-top: 5px; height: auto !important;}</style><body>` + team.AboutTeam.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') + `</body></html>` }} originWhitelist={['*']} />
                    }
                    {
                        // !isViewAll &&
                        // <LinearGradient colors={['rgba(255,255,255,0)', '#FFFFFF']} style={{ height: 50, width: '100%', position: 'absolute', bottom: 0, left: 0 }}></LinearGradient>
                    }
                </View>
                <View>
                    {
                        !isViewAll &&
                        <Touchable style={{ height: 35, justifyContent: 'center', alignItems: 'center', }} onPress={() => this.setState({ modalDetail: true })}>
                            <Text style={{ fontSize: 15, color: '#03a9f4', fontWeight: 'bold' }}>+ {convertLanguage(language, 'read_more')}</Text>
                        </Touchable>
                    }
                </View>
            </>
        } else {
            return null
        }
    }

    createInstagramView = () => {
        var team = this.props.team.team
        var arrImgInstagram = team.InstagramImages ? team.InstagramImages : [];
        var { language } = this.props.language;
        if (team.Role == 'Leader' && !team.IsInstagram) {
            return <>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 30, marginBottom: 15 }}>
                    <Text style={{ marginLeft: 16, color: '#333333', fontSize: 20, fontWeight: 'bold' }}>{convertLanguage(language, 'instagram')}</Text>
                </View>
                <Touchable
                    onPress={() => {
                        if (team.IsInstagram) {
                            this.logout()
                        } else
                            this.instagramLogin.show()
                    }}
                    style={{
                        marginTop: 10, marginHorizontal: 16,
                        alignSelf: 'stretch',
                        height: 44, borderColor: Colors.PRIMARY, borderWidth: 1, borderRadius: 4,
                        justifyContent: 'center', alignItems: 'center',
                    }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image source={require('../assets/instagram_icon.png')} />
                        <Text style={{ marginLeft: 10, fontSize: 18, fontWeight: 'bold', color: '#03a9f4' }}>{convertLanguage(language, 'link_instagram_feed')}</Text>
                    </View>
                </Touchable>
                <InstagramLogin
                    ref={ref => this.instagramLogin = ref}
                    clientId='248215246620837'
                    appId='248215246620837'
                    responseType='code'
                    appSecret='2f2d389bda5275560baaa49c400630f5'
                    redirectUrl='https://mapi.comeup.asia/account/instagram'
                    scopes={['user_profile', 'user_media']}
                    modalVisible={!this.state.tokenInstagram}
                    onLoginSuccess={this.setTokenIns}
                    onLoginFailure={(data) => console.log(data)}
                />
            </>
        } else {
            let data = []
            const length = arrImgInstagram.length
            if (length > 0) {
                for (let i = 0; i < length; i += 3) {
                    let d = []
                    for (let jj = i; jj < i + 3; jj++) {
                        if (jj < length) {
                            d.push(arrImgInstagram[jj])
                        }
                    }
                    data.push(d)
                }
                return <>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 30, marginBottom: 15 }}>
                        <Text style={{ marginLeft: 16, color: '#333333', fontSize: 20, fontWeight: 'bold' }}>{convertLanguage(language, 'instagram')}</Text>
                        {
                            team.Role == 'Leader' &&
                            <Touchable style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 16 }} onPress={() => this.setState({ modelActionInstagram: true })}>
                                <Image style={{ width: 12, height: 22 }} source={require('../assets/more_1_top.png')} />
                            </Touchable>
                        }
                    </View>
                    <Carousel
                        data={data}
                        renderItem={this.createInstagramView_Child}
                        sliderWidth={width}
                        itemWidth={width - 32}
                        inactiveSlideScale={1}
                    />
                </>
            }
        }

        return null
    }

    createInstagramView_Child = ({ item, index }) => {
        const item1 = item[0]
        let item2 = (item.length >= 2) ? item2 = item[1] : null
        let item3 = (item.length >= 3) ? item3 = item[2] : null

        return (
            <View style={{ flexDirection: 'row', marginTop: 10 }} key={index}>
                {this.createInstagramView_Child_2(item1, index * 3)}
                {this.createInstagramView_Child_2(item2, index * 3 + 1)}
                {this.createInstagramView_Child_2(item3, index * 3 + 2)}
            </View>
        );
    }

    createInstagramView_Child_2 = (item, index) => {
        if (item == null) return <View key={index} style={{ flex: 1, marginRight: 4 }} />
        return <TouchableOpacity key={index} style={{ flex: 1, marginRight: 4 }} onPress={() => { this.setState({ zoomeImageIndex: index, modalZoom: true }) }} >
            <Image style={{ backgroundColor: '#bcbcbc' }} aspectRatio={1} source={{ uri: item.Thumbnail }} />
        </TouchableOpacity>
    }

    createNews = () => {
        const team = this.props.team.team;
        var { language } = this.props.language;
        if (!team) return null
        return (
            <View>
                <Text style={{ marginLeft: 16, marginTop: 30, color: '#333333', fontSize: 20, fontWeight: 'bold' }}>{convertLanguage(language, 'team_news')}</Text>
                {
                    team.Role != 'Guest' && team.Role !== 'Staff' ?
                        <Touchable
                            onPress={() => { this.clearWebview(), this.props.navigation.navigate('CreateANews', { id: team.Id }) }}
                            style={{
                                marginTop: 10, marginHorizontal: 16, marginBottom: 10,
                                alignSelf: 'stretch',
                                height: 44, borderColor: Colors.PRIMARY, borderWidth: 1, borderRadius: 4,
                                justifyContent: 'center', alignItems: 'center',
                            }}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.PRIMARY }}>{convertLanguage(language, 'write')}</Text>
                        </Touchable>
                        :
                        null
                }
                {this.createListNews()}
            </View>
        )
    }

    dataZoomImage() {
        var images = [];
        this.state.dataZoomImage.forEach(image => {
            images.push({ Image: image.Photos.Large });
        });
        return images;
    }

    onOpenModelTeamNewsDetail(item, Role) {
        this.setState({
            modalTeamNewsDetail: true,
            teamnews: item,
            Role: Role
        })
    }

    closeModalTeamNewsDetail = () => {
        this.setState({ modalTeamNewsDetail: false, teamnews: {} })
    }

    createListNews = () => {
        let team = this.props.team.team;
        var { language } = this.props.language;
        if (team && team.Id) {
            const TeamNews = this.props.team.team.TeamNews;
            return (
                <View style={{ marginHorizontal: 16 }}>
                    {
                        this.props.team.loadingTeamNews ?
                            <ActivityIndicator size="large" color="#000000" style={styles.loading} />
                            :
                            TeamNews && typeof (TeamNews) != undefined ?
                                TeamNews.map((value, index, arr) => {
                                    if (index < 2)
                                        return <TeamNewsItem key={index}
                                            clearWebview={() => this.clearWebview()}
                                            navigation={this.props.navigation}
                                            onReportTeamNews={(id) => this.onReportTeamNews(id)}
                                            data={value}
                                            onLikeTeamNews={(Id) => this.props.onLikeTeamNews(Id)}
                                            onDeleteTeamNews={(Id) => this.props.onDeleteTeamNews(Id)}
                                            zoomImage={(index, Images) => this.setState({ zoomImageIndex2: index, modalZoomImage: true, dataZoomImage: Images })}
                                            onOpenModelTeamNewsDetail={() => this.onOpenModelTeamNewsDetail(value, value.Team.Role)} />
                                    else if (index == 2) {
                                        return (
                                            <Touchable
                                                key={index}
                                                onPress={() => {
                                                    this.clearWebview(), this.props.navigation.navigate('TeamListNews', {
                                                        id: this.props.team.team.Id, team: this.props.team.team
                                                    })
                                                }}
                                                style={{
                                                    marginTop: 10,
                                                    alignSelf: 'stretch',
                                                    height: 44, borderColor: Colors.PRIMARY, borderWidth: 1, borderRadius: 4,
                                                    justifyContent: 'center', alignItems: 'center',
                                                }}>
                                                <Text style={{ fontSize: 16, fontWeight: '600', color: Colors.PRIMARY }}>{convertLanguage(language, 'more_news')}</Text>
                                            </Touchable>
                                        )
                                    } else { return (<View key={index}></View>) }
                                })
                                : null
                    }
                </View>
            )
        }
        return null;
    }

    onReportTeamNews(TeamNewsId) {
        this.props.globalCheckLogin('TeamReport')
    }

    onDeleteTeamNews(TeamNewsId) {
        var { language } = this.props.language;
        Alert.alert(
            convertLanguage(language, 'delete_team_news'),
            convertLanguage(language, 'do_you_want_to_delete_this_news'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'destructive',
                },
                { text: convertLanguage(language, 'ok'), onPress: () => { this.setState({ modalTeamNewsDetail: false, modelTeamNews: false, teamnews: {} }); this.props.onDeleteTeamNews(TeamNewsId) } },
            ],
            { cancelable: false },
        );
    }

    openModal(props) {
        if (props != 'modalAction') {
            this.setState({ modalAction: false, [props]: true });
        } else {
            this.setState({ [props]: true });
        }

    }

    closeModal(props) {
        this.setState({ [props]: false });
    }

    onReport() {
        this.setState({ modalContact: false });
        this.props.globalCheckLogin('TeamReport')
    }

    onRemoveInstagram() {
        this.setState({ modelActionInstagram: false })
        const Code = this.props.team.team.Code;
        this.props.onRemoveTokenIns(Code);
    }

    goBack = () => {
        var { index, routes } = this.props.navigation.dangerouslyGetState();
        if (routes.length > 1 && ['Login', 'LoginWithMail', 'ForgotPw', 'Splash'].includes(routes[index - 1].name)) {
            this.props.navigation.reset({ index: 0, routes: [{ name: 'Main' }], })
        } else {
            this.clearWebview()
            this.props.navigation.goBack();
        }
    }

    clearWebview = async () => {
        // if (Platform.OS === "android") {
        //     // await this.webview?.stopLoading();
        //     // await this.webview?.reload()
        //     this.setState({ focuse: false })
        // }
    }

    render() {
        var { team, loadingFollowers } = this.props.team;
        // console.log('state', this.state.focuse)
        if (!team) return null
        var { language } = this.props.language;
        // console.log('112', this.props.isShowWebview);
        // console.log('11', this.state.scrolly2);
        return (
            <>
                {/* <Forcuse focuse={async (data) => {
                    if (Platform.OS === "android") {
                        // await this.webview?.reload()
                        // 
                        if (!this.state.focuse && this?.webview !== null) {
                            // await this.webview?.reload()
                            // await this?.webview?.stopLoading();
                            setTimeout(async () => {
                                await this?.webview?.stopLoading();
                            }, 1000);
                        }
                    }
                    this.setState({ focuse: data })
                }} /> */}
                {
                    <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                        {
                            this.props.team.loadingSetIns &&
                            <Loading />
                        }
                        <View style={{}}>
                            <Touchable
                                onPress={this.goBack}
                                style={{
                                    minWidth: 48, minHeight: 48, alignSelf: 'flex-start',
                                    justifyContent: 'center', alignItems: 'center'
                                }}>
                                <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                            </Touchable>
                            {
                                !this.props.team.loading && team.status_code !== 404 && team.status_code !== 500 &&
                                <>
                                    <Touchable
                                        onPress={() => { this.openModal(team.Role !== 'Guest' ? 'modalAction' : 'modalContact') }}
                                        style={{
                                            minWidth: 50, minHeight: 50,
                                            justifyContent: 'center', alignItems: 'center',
                                            position: 'absolute', right: 0, top: 0
                                        }}>
                                        <Image source={team.Role !== 'Guest' ? require('../assets/more_horizontal.png') : require('../assets/report.png')} style={{ width: 30, height: 30, marginRight: 14 }} />
                                    </Touchable>
                                    <Touchable
                                        onPress={() => this.share()}
                                        style={{
                                            minWidth: 40, minHeight: 50,
                                            justifyContent: 'center', alignItems: 'center',
                                            position: 'absolute', right: 50, top: 0
                                        }}>
                                        <Image source={require('../assets/share_2.png')} style={{ width: 30, height: 30 }} />
                                    </Touchable>
                                </>
                            }
                        </View>

                        <Line />
                        {
                            this.props.team.loading ?
                                <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                                :
                                team.status_code === 404
                                    ?
                                    <Error404 onPress={() => this.props.navigation.goBack()} />
                                    :
                                    team.status_code === 500
                                        ?
                                        <Error500 onPress={() => this.refresh()} />
                                        :
                                        <ScrView scrollEventThrottle={16} refs={this.srcView} onScroll={(event) => { this.setState({ scrolly2: this.state.scrolly, scrolly: event.nativeEvent.contentOffset.y }) }}>
                                            <TouchableOpacity style={{ backgroundColor: 'gray', alignSelf: 'center', marginTop: 20, width: 128, height: 128, borderRadius: 128 / 2 }} onPress={() => this.setState({ modelImage: true })}>
                                                <Image
                                                    resizeMode='cover'
                                                    style={{ backgroundColor: 'gray', alignSelf: 'center', width: 128, height: 128, borderRadius: 128 / 2 }}
                                                    source={{ uri: team.Logos ? team.Logos.Medium : '' }} />
                                            </TouchableOpacity>

                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, alignSelf: 'center', marginLeft: 20, marginRight: 20 }}>
                                                <Text style={{ color: '#333333', fontSize: 24, fontWeight: 'bold' }}>{team.Name}</Text>
                                                {team.Verify == 1 ? <Image style={{ marginLeft: 10 }} source={require('../assets/team_certified.png')} /> : null}
                                            </View>

                                            <Text style={{ alignSelf: 'center', color: '#4F4F4F', fontSize: 16, marginTop: 15, fontWeight: 'bold' }}>ID: {team.Code}</Text>
                                            <Text style={{ alignSelf: 'center', color: '#333333', fontSize: 14, marginTop: 5, marginLeft: 20, marginRight: 20, textAlign: 'center' }}>{getTag(team.HashTag)}</Text>

                                            {/* followers */}
                                            <View style={{ flexDirection: 'row', margin: 16, alignItems: 'center' }}>
                                                <TouchableOpacity style={{ flex: 1 }} onPress={() => { this.clearWebview(), this.props.navigation.navigate('TeamFollowers') }}>
                                                    <Text style={{ alignSelf: 'center', color: '#333333', fontSize: 18, fontWeight: 'bold' }}>{team.FollowerCount ? team.FollowerCount : '0'} {convertLanguage(language, (team?.FollowerCount > 1 && team?.FollowerCount !== null && team?.FollowerCount !== undefined) ? 'followers' : 'follower')}</Text>
                                                </TouchableOpacity>
                                                <TouchableLoading
                                                    disabled={loadingFollowers}
                                                    activeOpacity={0.7}
                                                    loading={false}
                                                    onPress={() => { team.Role != 'Guest' ? this.props.navigation.navigate('TeamMember', { Slug: this.state.id }) : this.inviteOrFollow(team.IsFollow) }}
                                                    style={{
                                                        flex: 1, minHeight: 36,
                                                        borderRadius: 5,
                                                        backgroundColor: !team.IsFollow || team.Role != 'Guest' ? '#00A9F4' : '#FFFFFF',
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        flexDirection: 'row',
                                                        borderWidth: !team.IsFollow || team.Role != 'Guest' ? 0 : 1,
                                                        borderColor: '#333333'
                                                    }}>
                                                    <Text style={{ color: !team.IsFollow || team.Role != 'Guest' ? '#FFFFFF' : '#333333', fontSize: 16, fontWeight: !team.IsFollow || team.Role != 'Guest' ? 'bold' : 'normal' }}>{team.Role != 'Guest' ? convertLanguage(language, 'team_member') : (team.IsFollow ? convertLanguage(language, 'following') : convertLanguage(language, 'follow'))}</Text>
                                                </TouchableLoading>
                                            </View>

                                            {/* about team */}
                                            {this.createAbout()}

                                            {/* hosting event */}
                                            {
                                                ((team.Events && team.Events.length > 0) || team.Role != 'Guest') &&
                                                <React.Fragment>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 30, marginBottom: 15 }}>
                                                        <Text style={{ marginLeft: 16, color: '#333333', fontSize: 20, fontWeight: 'bold' }}>{convertLanguage(language, 'hosting_event')}</Text>
                                                        {team.Events && team.Events.length > 0 && <Touchable style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 16 }} onPress={() => { this.clearWebview(), this.props.navigation.navigate('EventByTeams', { Slug: team.Id }) }}>
                                                            {/* <Image style={{ width: 17, height: 17 }} source={require('../assets/list_icon.png')} /> */}
                                                            <Text style={styles.txtSeeMore}>{convertLanguage(language, 'see_more')}</Text>
                                                        </Touchable>}
                                                    </View>
                                                    <View style={styles.boxListEvent}>
                                                        <Carousel
                                                            data={team.Events}
                                                            renderItem={(item) => { return <ItemEventSlide language={language} data={item} navigation={this.props.navigation} onLikeEvent={(Id) => this.props.onLikeEvent(Id)} /> }}
                                                            sliderWidth={width}
                                                            itemWidth={width - 26}
                                                            inactiveSlideScale={1}
                                                        />
                                                    </View>
                                                    {
                                                        team.Role != 'Guest' &&
                                                        <Touchable style={styles.btnRegister} onPress={async () => { this.clearWebview(), this.props.navigation.navigate('CreateEventStep1', { HostInfoId: team.Id }) }}>
                                                            <Text style={styles.txtBtnRegister}>{convertLanguage(language, 'register_event_venue')}</Text>
                                                        </Touchable>
                                                    }
                                                </React.Fragment>
                                            }

                                            {/* hosting Venue */}
                                            {
                                                (team?.Venues && team.Venues.length > 0 || team.Role != 'Guest') &&
                                                <React.Fragment>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 30, marginBottom: 15 }}>
                                                        <Text style={{ marginLeft: 16, color: '#333333', fontSize: 20, fontWeight: 'bold' }}>{convertLanguage(language, 'hosting_venue')}</Text>
                                                        {team?.Venues && team.Venues.length > 0 && <Touchable style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 16 }} navigation={this.props.navigation} onPress={() => this.props.navigation.navigate('VenueByTeams', { Slug: team.Id })}>
                                                            <Text style={styles.txtSeeMore}>{convertLanguage(language, 'see_more')}</Text>
                                                        </Touchable>}
                                                    </View>
                                                    <View style={styles.boxListEvent}>
                                                        <Carousel
                                                            data={team.Venues}
                                                            // renderItem={(item) => { return <VenueSlideItem item={item.item} navigation={this.props.navigation} onLikeVenue={(id) => this.props.onLikeVenue(id)} /> }}
                                                            renderItem={(item) => {
                                                                return <VenueManagingItem item={item.item} navigation={this.props.navigation} onLikeVenue={(id) => this.props.onLikeVenue(id)} />
                                                            }}
                                                            sliderWidth={width}
                                                            itemWidth={width - 44}
                                                            inactiveSlideScale={1}
                                                        />
                                                    </View>
                                                    {
                                                        team.Role != 'Guest' &&
                                                        <Touchable style={styles.btnRegister} onPress={() => {
                                                            this.clearWebview()
                                                            this.props.navigation.navigate('CreateVenue', { HostInfoId: team.Id })
                                                        }}>
                                                            <Text style={styles.txtBtnRegister}>{convertLanguage(language, 'register_venue')}</Text>
                                                        </Touchable>
                                                    }
                                                </React.Fragment>
                                            }

                                            {/* Instagram */}
                                            {this.createInstagramView()}


                                            {/* News */}
                                            {this.createNews()}

                                        </ScrView>
                        }
                        {
                            this.state.modalAction
                                ?
                                <ModalAction
                                    modalVisible={this.state.modalAction}
                                    closeModal={(value) => this.closeModal(value)}
                                    openModal={(value) => this.openModal(value)}
                                    navigation={this.props.navigation}
                                    openViewReport={() => this.props.globalCheckLogin('TeamReport')}
                                    team={team}
                                    updateTeam={() => this.props.navigation.navigate('RegisterTeam', { data: team, callback: () => this.refresh() })}
                                />
                                : null
                        }

                        {
                            this.state.modalContact
                                ?
                                <ModalContact
                                    modalVisible={this.state.modalContact}
                                    closeModal={(value) => this.closeModal(value)}
                                    openModal={(value) => this.openModal(value)}
                                    onReport={() => this.onReport()}
                                    team={team}
                                />
                                : null
                        }

                        {
                            this.state.modalZoom
                                ?
                                <ModalZoomImage
                                    modalVisible={this.state.modalContact}
                                    closeModal={(value) => this.closeModal(value)}
                                    openModal={(value) => this.openModal(value)}
                                    team={team}
                                    images={team.InstagramImages}
                                    index={this.state.zoomeImageIndex}
                                />
                                : null
                        }

                        {
                            this.state.modelActionInstagram &&
                            <ModalActionInstagram Role={team.Role} closeModal={() => this.setState({ modelActionInstagram: false, TeamNewsId: '' })} onRemoveInstagram={() => this.onRemoveInstagram()} />
                        }

                        {
                            this.state.modalZoomImage
                                ?
                                <ModalZoomImage
                                    modalVisible={this.state.modalZoomImage}
                                    closeModal={() => this.setState({ modalZoomImage: false })}
                                    team={team}
                                    images={this.dataZoomImage()}
                                    index={this.state.zoomImageIndex2}
                                />
                                : null
                        }
                        <Modal
                            isVisible={this.state.modelImage}
                            backdropOpacity={0.7}
                            animationIn="zoomInDown"
                            animationOut="zoomOutUp"
                            animationInTiming={1}
                            animationOutTiming={1}
                            backdropTransitionInTiming={1}
                            backdropTransitionOutTiming={1}
                            onBackdropPress={() => this.setState({ modelImage: false })}
                            style={{ margin: 0 }}
                            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                        >
                            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                                <Image style={[styles.boxImageZoom]} source={{ uri: team.Logos ? team.Logos.Large : team.Logo }} />
                                <Touchable style={styles.boxIconClose} onPress={() => this.setState({ modelImage: false })}>
                                    <Image style={styles.iconClose} source={require('../assets/x_icon_white.png')} />
                                </Touchable>
                            </View>
                        </Modal>

                        {
                            this.state.modalDetail
                                ?
                                <ModalDetail
                                    modalVisible={this.state.modalDetail}
                                    closeModal={() => this.setState({ modalDetail: false })}
                                    Detail={team.AboutTeam}
                                />
                                : null
                        }

                    </SafeView>
                }
                {
                    this.state.modalTeamNewsDetail &&
                    <ModalTeamNewsDetail
                        closeModal={this.closeModalTeamNewsDetail}
                        data={this.state.teamnews}
                        onDeleteTeamNews={(Id) => this.props.onDeleteTeamNews(Id)}
                        onLikeTeamNews={(Id) => this.props.onLikeTeamNews(Id)}
                        openModalAction={(Id) => { this.onOpenModelTeamNews(Id, this.state.Role) }}
                        navigation={this.props.navigation}
                    />
                }
            </>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //   flex: 1,
        //   justifyContent: "center",
        //   alignItems: "center"
    },
    btnRegister: {
        marginTop: 10,
        marginHorizontal: 16,
        alignSelf: 'stretch',
        height: 44,
        borderColor: Colors.PRIMARY,
        borderWidth: 1,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtBtnRegister: {
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.PRIMARY
    },
    contentHtml: {
        marginLeft: 20,
        marginRight: 20
    },
    loading: {
        padding: 20
    },
    boxImageZoom: {
        width: width,
        // resizeMode: 'center',
        height: width
    },
    iconClose: {
        width: 25,
        height: 25,
        padding: 10
    },
    boxIconClose: {
        padding: 20,
        paddingTop: 30,
        position: 'absolute',
        right: 0,
        top: 0
    },
    txtSeeMore: {
        fontSize: 16,
        color: '#555555'
    },
    boxListTeam: {
        paddingHorizontal: 20
    }
});

const stylesHtml = StyleSheet.create({
    a: {
        fontWeight: '300',
        color: '#FF3366', // make links coloured pink
    },
    p: {
        // lineHeight: 20,
        height: 25,
        fontSize: 16,
        // color: '#333333'
    }
});

const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language,
        global: state.global,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getTeamDetail: (id, notification_id, iso_code) => {
            dispatch(actTeamDetail(id, notification_id, iso_code))
        },
        getTeamNews: (id) => {
            dispatch(actTeamNews(id))
        },
        getImageIns: (id) => {
            dispatch(actImagesIns(id))
        },
        setTokenIns: (id, token) => {
            dispatch(actSetTokenIns(id, token))
        },
        onTeamFollow: (TeamId) => {
            dispatch(actTeamFollow(TeamId))
        },
        onLikeTeamNews: (Id) => {
            dispatch(actLikeTeamNews(Id))
        },
        onDeleteTeamNews: (TeamNewsId) => {
            dispatch(actDeleteTeamNews(TeamNewsId))
        },
        globalCheckLogin: (nextPage) => {
            dispatch(actGlobalCheckLogin(nextPage))
        },
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
        onRemoveTokenIns: (Code) => {
            dispatch(actRemoveTokenIns(Code))
        },
        onLikeVenue: (id, index) => {
            dispatch(actLikeVenues(id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(DetailTeam);

