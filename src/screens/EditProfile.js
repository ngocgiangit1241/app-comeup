import React, { Component } from 'react'
import { PixelRatio, View, Text, Image, TextInput, ActivityIndicator, Dimensions, ScrollView, TouchableOpacity, Alert, StyleSheet, Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-community/async-storage';
import { TextField } from '../components/react-native-material-textfield';
import DatePicker from 'react-native-datepicker';
import Touchable from '../screens_view/Touchable';
import ImagePicker from 'react-native-image-crop-picker';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
const { width, height } = Dimensions.get('window');
const scale = width / 360
const scaleFontSize = PixelRatio.getFontScale()
import PickerItem from '../screens_view/PickerItem';
import { actCheckLoginSocial, actUpdateProfile, actRemoveConnectFacebook, actLogout, actVerifyEmail } from '../actions/user';
import { connect } from "react-redux";
import * as Colors from '../constants/Colors';
import { convertLanguage, dateNow } from '../services/Helper';
import * as Config from '../constants/Config';
import Line from '../screens_view/Line';
import Popover from 'react-native-popover-view';
import SafeView from '../screens_view/SafeView';
import FastImage from 'react-native-fast-image';
import CountryCode from '../components/CountryCode';
import AppleLogin from '../components/react-native-apple-login/Apple'
import appleAuth, {
    AppleAuthError,
    AppleAuthRequestScope,
    AppleAuthRealUserStatus,
    AppleAuthCredentialState,
    AppleAuthRequestOperation,
} from '@invertase/react-native-apple-authentication';
import * as datacountry from '../utils/datacountry.json';

class EditProfile extends Component {
    constructor(props) {
        super(props);

        const { UserId, Email, Name, Avatars, Avatar, Is_SNS_FB, Is_SNS_GG, IsValidEmail, Zipcode, Phone, Birthday, Gender, Language, CountryCode, AvatarCheck } = this.props.user.profile;
        this.state = {
            loading: false,
            loadingCheckId: false,
            //
            name: Name ? Name : '',
            id: UserId ? UserId : '',
            email: Email ? Email : '',
            email_draff: Email ? Email : '',
            Zipcode: Zipcode ? Zipcode : '+84',
            flag: datacountry[Object.keys(datacountry).find(key => datacountry[key].callingCode[datacountry[key].callingCode.length - 1] === (Zipcode ? Zipcode.substring(1) : '84'))].flag,
            Phone: Phone ? Phone : '',
            gender: Gender ? Gender : 0,
            birthday: Birthday ? Birthday : '',
            languages: Language ? Language : 'en',
            arrHeadPhone: [],
            avatarSource: AvatarCheck ? { uri: Avatars.Small } : require('../assets/profile_default_3.png'),
            avatarGg: Avatars.SmallGg ? { uri: Avatars.SmallGg } : require('../assets/profile_default_3.png'),
            avatarAp: Avatars.SmallAp ? { uri: Avatars.SmallAp } : require('../assets/profile_default_3.png'),
            avatarBase64: '',
            emailEditable: Email ? false : true,
            err_name: '',
            err_id: '',
            err_email: '',
            valid_id: '',
            is_change_avatar: false,
            Is_SNS_FB: Is_SNS_FB ? Is_SNS_FB : false,
            Is_SNS_GG: Is_SNS_GG ? Is_SNS_GG : false,
            IsValidEmail: IsValidEmail,
            CountryCode: CountryCode ? CountryCode : '',
            isChange: false,
            isVisible: false,
            editEmail: !IsValidEmail == 0 ? false : true
        };
        this.instagramLogin = null;

    }

    should = () => {
        return (this.state.name && this.state.id && this.state.email && this.state.languages && this.state.isChange)
    }

    shouldCheckId = () => {
        return this.state.loadingCheckId || !this.state.id
    }

    onUpdateProfile = () => {
        const { name, id, email, gender, birthday, languages, Zipcode, Phone, avatarBase64 } = this.state;
        var body = {
            "Name": name,
            "UserId": id,
            "Email": email,
            "Gender": gender,
            "Birthday": birthday,
            "Language": languages,
            "Zipcode": Zipcode,
            "Phone": Phone,
        }
        if (avatarBase64 != '') {
            body.AvatarBase = avatarBase64
        }
        this.props.onUpdateProfile(body);
        this.setState({ isChange: false })
    }

    checkId = async () => {
        if (!this.state.id) return

        const auKey = await AsyncStorage.getItem('auKey')
        const auValue = await AsyncStorage.getItem('auValue')

        let headers = { 'Content-Type': 'multipart/form-data' }
        headers[auKey] = auValue

        let formdata = new FormData();
        formdata.append("code", this.state.id)

        this.setState({ loadingCheckId: true, valid_id: 'Checking...', err_id: '' })
        fetch(Config.API_URL + '/users/check-id', {
            method: 'post',
            headers: headers,
            body: formdata
        }).then(async (response) => {
            if (response.status == 200) {
                response = await response.json()
                if (response.status == 200) {
                    this.setState({ valid_id: response.messages.code, err_id: '' })
                } else if (response.errors) {
                    this.setState({ err_id: response.errors.code, valid_id: '' })
                }
            } else {
                alert('Cannot connect to server.')
                this.setState({ valid_id: '', err_id: '' })
            }
        }).catch(err => {
            //alert(err)
            this.setState({ valid_id: '', err_id: '' })
        })
    }

    pickAvatar = () => {
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true,
            includeBase64: true
        }).then(image => {
            this.setState({
                is_change_avatar: true,
                avatarBase64: 'data:image/png;base64,' + image.data,
                isChange: true
            })
        }).catch(e => { });
    }

    facebook = () => {
        var { language } = this.props.language;
        if (this.props.user.profile.Is_SNS_FB) {
            Alert.alert(
                '',
                convertLanguage(language, 'do_you_want_to_disconnect'),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'destructive',
                    },
                    { text: convertLanguage(language, 'ok'), onPress: () => { LoginManager.logOut(); this.props.removeConnectFacebook('fb', language) } },
                ],
                { cancelable: false },
            );
        } else {
            // Attempt a login using the Facebook login dialog asking for default permissions.
            LoginManager.logInWithPermissions(["public_profile", "email"]).then(
                (result) => {
                    if (result.isCancelled) {
                        // alert("Login cancelled");
                    } else {
                        AccessToken.getCurrentAccessToken().then(
                            (data) => {
                                const accessToken = data.accessToken.toString()
                                if (accessToken) {
                                    var body = {
                                        Fbtoken: accessToken
                                    }
                                    this.props.onCheckLoginSocial(body, 'fb')
                                }
                                else alert('Error')
                            }
                        )
                    }
                },
                (error) => {
                    alert("Login fail with error: " + error);
                }
            );
        }
    }

    async _setupGoogleSignin() {
        try {
            await GoogleSignin.configure({
                webClientId: '1058119835706-onr4c64vqgdt5iiqp4upsa2b2psqvrg7.apps.googleusercontent.com',
                offlineAccess: false,
                iosClientId: '1058119835706-k22o7reor4deq5tbjrpdvjorfdaksogu.apps.googleusercontent.com'
            });
        }
        catch (err) {
            console.log("Google signin error", err.code, err.message);
        }
    }

    componentDidMount() {
        this._setupGoogleSignin();
    }

    google = async () => {
        var { language } = this.props.language;
        if (this.props.user.profile.Is_SNS_GG) {
            Alert.alert(
                '',
                convertLanguage(language, 'do_you_want_to_disconnect'),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'destructive',
                    },
                    { text: convertLanguage(language, 'ok'), onPress: () => { GoogleSignin.revokeAccess(); GoogleSignin.signOut(); this.props.removeConnectFacebook('gg', language) } },
                ],
                { cancelable: false },
            );
        } else {
            // this._setupGoogleSignin();
            GoogleSignin.signIn().then(() => {
                GoogleSignin.getTokens().then((token) => {
                    var body = {
                        Ggtoken: token.accessToken
                    }
                    this.props.onCheckLoginSocial(body, 'gg')
                }).catch((err) => {
                });
            })
                .catch((err) => {
                })
        }
    }
    apple = async () => {
        var { language } = this.props.language;
        if (this.props.user.profile.Is_SNS_AP) {
            Alert.alert(
                '',
                convertLanguage(language, 'do_you_want_to_disconnect'),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'destructive',
                    },
                    { text: convertLanguage(language, 'ok'), onPress: () => { this.logoutApple, this.props.removeConnectFacebook('ap', language) } },
                ],
                { cancelable: false },
            );
        } else {
            appleAuth.isSupported ? this.appleIos() : this.instagramLogin.show()
        }
    }
    setTokenApple = (token) => {
        // var { navigation } = this.props;
        // this.props.onSubmit('Aptoken', token.id_token, true, navigation)
        var body = {
            Aptoken: token.id_token
        }
        this.props.onCheckLoginSocial(body, 'ap')
    }

    logoutApple = async () => {
        try {
            const appleAuthRequestResponse = await appleAuth.performRequest({
                requestedOperation: AppleAuthRequestOperation.LOGOUT
            });
            const credentialState = await appleAuth.getCredentialStateForUser(
                appleAuthRequestResponse.user
            );
            if (credentialState === AppleAuthCredentialState.REVOKED) {
            }
        } catch (appleLogoutError) {
            console.warn("Apple logout error: ", appleLogoutError);
        }
    }


    appleIos = async () => {
        var { navigation } = this.props;
        console.warn('Beginning Apple Authentication');

        // start a login request
        try {
            const appleAuthRequestResponse = await appleAuth.performRequest({
                requestedOperation: AppleAuthRequestOperation.LOGIN,
                requestedScopes: [
                    AppleAuthRequestScope.EMAIL,
                    AppleAuthRequestScope.FULL_NAME,
                ],
            });


            const {
                user: newUser,
                email,
                nonce,
                identityToken,
                realUserStatus /* etc */,
            } = appleAuthRequestResponse;

            this.user = newUser;

            this.fetchAndUpdateCredentialState()
                .then(res => this.setState({ credentialStateForUser: res }))
                .catch(error =>
                    this.setState({ credentialStateForUser: `Error: ${error.code}` }),
                );

            if (identityToken) {
                // e.g. sign in with Firebase Auth using `nonce` & `identityToken`
                var body = {
                    Aptoken: identityToken
                }
                this.props.onCheckLoginSocial(body, 'ap')
            } else {
                // no token - failed sign-in?
            }

            if (realUserStatus === AppleAuthRealUserStatus.LIKELY_REAL) {
                // console.log("I'm a real person!");
            }

            // console.warn(`Apple Authentication Completed, ${this.user}, ${email}`);
        } catch (error) {
            if (error.code === AppleAuthError.CANCELED) {
                // console.warn('User canceled Apple Sign in.');
            } else {
                // console.error(error);
            }
        }
    };

    fetchAndUpdateCredentialState = async () => {
        if (this.user === null) {
            this.setState({ credentialStateForUser: 'N/A' });
        } else {
            const credentialState = await appleAuth.getCredentialStateForUser(this.user);
            if (credentialState === AppleAuthCredentialState.AUTHORIZED) {
                this.setState({ credentialStateForUser: 'AUTHORIZED' });
            } else {
                this.setState({ credentialStateForUser: credentialState });
            }
        }
    }


    logout = () => {
        LoginManager.logOut()
        GoogleSignin.revokeAccess();
        GoogleSignin.signOut()
        let arr = []
        arr.push(['auKey', ''])
        arr.push(['auValue', ''])
        AsyncStorage.multiSet(arr)
        this.props.getLogout(this.props.navigation);
    }

    convertDate = (date) => {
        const { language } = this.props.language;
        if (date) {
            if (language === 'vi') {
                var date_arr = date.split("-");
                var day = date_arr[2];
                var month = date_arr[1];
                var year = date_arr[0];
                return day + '-' + month + '-' + year;
            } else {
                return date
            }
        }
        return '';
    }

    showPopover() {
        this.setState({ isVisible: true });
    }

    closePopover() {
        this.setState({ isVisible: false });
    }

    onEditEmail = () => {
        this.setState({ editEmail: !this.state.editEmail })
        if (this.state.editEmail) {
            this.setState({ email: this.state.email_draff })
        }
    }

    render() {
        var { loadingCheckSocial, loadingUpdateProfile, errors, profile, isVerifyEmail } = this.props.user;
        var { Zipcode } = this.state;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'edit_profile')}</Text>
                    <View style={{ width: 48 }} />
                </View>
                <Line />
                <View style={{ flex: 1 }}>
                    {
                        (loadingCheckSocial || isVerifyEmail) &&
                        <View style={{ position: 'absolute', backgroundColor: 'rgba(0,0,0,0.3)', left: 0, top: 0, width, height, zIndex: 9, alignItems: 'center', justifyContent: 'center' }}>
                            <ActivityIndicator size="large" color="#000000" style={{ marginBottom: 50 }} />
                        </View>
                    }
                    <KeyboardAwareScrollView>
                        <ScrollView style={{ flex: 1, width: '100%' }}>
                            <Text style={{
                                marginTop: 20,
                                color: Colors.TEXT_P, fontSize: 14,
                                textAlign: 'center',
                            }}>
                                {convertLanguage(language, 'note_login_with_id')}
                            </Text>
                            <View style={{ alignItems: 'center', marginBottom: 20 }}>
                                <Touchable onPress={this.pickAvatar} style={{ alignSelf: 'center' }}>
                                    <FastImage
                                        resizeMode='cover'
                                        style={{ alignSelf: 'center', marginTop: 20, width: 96, height: 96, borderRadius: 48 }}
                                        source={this.state.is_change_avatar ? { uri: this.state.avatarBase64 } : this.state.avatarSource} />
                                    <Touchable onPress={this.pickAvatar} style={{ position: 'absolute', bottom: 0, right: 0, width: 32, height: 32, borderRadius: 16, alignItems: 'center', justifyContent: 'center', backgroundColor: '#FFFFFF' }}>
                                        <Image source={profile.AvatarCheck || this.state.is_change_avatar ? require('../assets/pencil.png') : require('../assets/plus2.png')} style={{ width: 24, height: 24 }} />
                                    </Touchable>
                                </Touchable>
                            </View>
                            {/* name */}
                            <TextField
                                value={this.state.name}
                                autoCorrect={false}
                                autoCapitalize={'none'}
                                enablesReturnKeyAutomatically={true}
                                onChangeText={(name) => this.setState({ name, isChange: true })}
                                returnKeyType='next'
                                label={convertLanguage(language, 'name') + '*'}
                                error={errors.Name}
                                baseColor={this.state.name.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                tintColor={'#828282'}
                                errorColor={'#EB5757'}
                                inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.firstname ? '#EB5757' : '#BDBDBD' }]}
                                containerStyle={styles.containerStyle}
                                // labelHeight={25}
                                labelTextStyle={{ paddingBottom: 15 }}
                                lineWidth={2}
                                selectionColor={'#3a3a3a'}
                                style={styles.input}
                                labelTextStyle={{ paddingLeft: 12 }}
                                errorImage={require('../assets/error.png')}
                            />

                            {/* id */}
                            <TextField
                                disabled={profile.UserId ? true : false}
                                disabledLineWidth={0}
                                autoCorrect={false}
                                enablesReturnKeyAutomatically={true}
                                value={this.state.id}
                                onChangeText={(id) => {
                                    if (id != this.state.id) {
                                        this.setState({ id, loadingCheckId: false, err_id: '', valid_id: '', isChange: true })
                                    }
                                }}
                                error={errors.UserId}
                                returnKeyType='next'
                                label={convertLanguage(language, 'id') + '*'}
                                baseColor={this.state.id.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                tintColor={'#828282'}
                                errorColor={'#EB5757'}
                                inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.firstname ? '#EB5757' : '#BDBDBD' }, profile.UserId ? { backgroundColor: '#e9ecef' } : {}]}
                                containerStyle={styles.containerStyle}
                                // labelHeight={25}
                                labelTextStyle={{ paddingBottom: 15 }}
                                lineWidth={2}
                                selectionColor={'#3a3a3a'}
                                style={styles.input}
                                labelTextStyle={{ paddingLeft: 12 }}
                                errorImage={require('../assets/error.png')}
                            />

                            {/* email */}
                            <View style={{ flexDirection: 'row' }}>
                                <TextField
                                    editable={this.state.editEmail}
                                    autoCorrect={false}
                                    enablesReturnKeyAutomatically={true}
                                    value={this.state.email}
                                    onChangeText={(email) => { this.setState({ email, isChange: true }) }}
                                    error={errors.Email}
                                    returnKeyType='next'
                                    label={convertLanguage(language, 'email') + '*'}
                                    baseColor={this.state.email.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                    tintColor={'#828282'}
                                    errorColor={'#EB5757'}
                                    inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.firstname ? '#EB5757' : '#BDBDBD', backgroundColor: this.state.editEmail ? '#ffffff' : '#e9ecef' }]}
                                    containerStyle={[styles.containerStyle, { marginBottom: 5 }]}
                                    // labelHeight={25}
                                    labelTextStyle={{ paddingBottom: 15 }}
                                    lineWidth={2}
                                    selectionColor={'#3a3a3a'}
                                    style={styles.input}
                                    labelTextStyle={{ paddingLeft: 12 }}
                                    errorImage={require('../assets/error.png')}
                                />
                                {this.state.IsValidEmail != 0 &&
                                    <TouchableOpacity onPress={this.onEditEmail}
                                        style={{ width: '25%', marginRight: 20, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: 'black', marginVertical: 13, borderRadius: 8 }}>
                                        <Text style={{ fontSize: 15 }}>
                                            {
                                                !this.state.editEmail ? convertLanguage(language, 'edit_email') : convertLanguage(language, 'cancel')
                                            }

                                        </Text>
                                    </TouchableOpacity>
                                }
                            </View>
                            {
                                this.state.IsValidEmail == 0 &&
                                <>
                                    <Text style={{ fontSize: 13, color: 'red', paddingHorizontal: 20, marginTop: -5 }}>{convertLanguage(language, 'resend_email_text')}</Text>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginTop: 5 }}>
                                        <Text style={{ fontSize: 13, color: 'red', paddingLeft: 20 }}>{convertLanguage(language, 'email_not_confirm')}</Text>
                                        <Text style={{ fontSize: 13, color: '#00adff' }}
                                            onPress={() => this.props.onVerifyEmail()}>
                                            {convertLanguage(language, 'here')}</Text>
                                        <Text style={{ fontSize: 13, color: 'red', paddingRight: 20, lineHeight: 20 }}>{convertLanguage(language, 'email_not_confirm_korea')}</Text>
                                    </View>

                                </>
                            }
                            <View style={{ flexDirection: 'row', paddingHorizontal: 20 }}>
                                <TouchableOpacity
                                    onPress={() => this.showPopover()}
                                    ref={ref => this.touchable = ref}
                                    style={{
                                        borderWidth: 1,
                                        borderColor: '#bdbdbd',
                                        // paddingBottom: 6,
                                        // paddingTop: 3,
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        marginRight: 10,
                                        height: 50.5 * scale / scaleFontSize,
                                        borderRadius: 4,
                                        paddingHorizontal: 12,
                                        marginVertical: 6,
                                        width: 130
                                    }}>
                                    <View style={{}}>
                                        <Text style={{ color: '#828282', fontSize: 10 * scale / scaleFontSize, marginTop: 3 }}>{convertLanguage(language, 'country')}</Text>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
                                            <Image source={{ uri: this.state.flag }} style={[styles.ic_country, { width: 25, height: 15, marginTop: 7 }]} />
                                            <Text style={styles.txtCountry}>{Zipcode}</Text>
                                        </View>
                                    </View>
                                    <Image source={require('../assets/down_arrow_active.png')} style={{
                                        width: 13,
                                        height: 11,
                                        alignSelf: 'center'
                                    }} />
                                </TouchableOpacity>
                                <TextField
                                    autoCorrect={false}
                                    enablesReturnKeyAutomatically={true}
                                    value={this.state.Phone}
                                    onChangeText={(Phone) => this.setState({ Phone: Phone == 0 ? '' : Phone, isChange: true })}
                                    error={errors.Phone}
                                    returnKeyType='next'
                                    label={convertLanguage(language, 'phone')}
                                    baseColor={this.state.Phone.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                    tintColor={'#828282'}
                                    errorColor={'#EB5757'}
                                    inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.firstname ? '#EB5757' : '#BDBDBD' }]}
                                    containerStyle={[styles.containerStyle, { paddingHorizontal: 0 }]}
                                    // labelHeight={25}
                                    labelTextStyle={{ paddingBottom: 15 }}
                                    lineWidth={2}
                                    selectionColor={'#3a3a3a'}
                                    style={styles.input}
                                    labelTextStyle={{ paddingLeft: 12 }}
                                    errorImage={require('../assets/error.png')}
                                    keyboardType="number-pad"
                                />
                            </View>
                            {errors.Phone &&
                                <View style={{ marginTop: 10 }}></View>
                            }
                            {/* gender */}
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20 }}>
                                <Text style={{ color: Colors.TEXT_P, fontSize: 16, flex: 1.5, textAlign: 'center' }}>{convertLanguage(language, 'gender')}:</Text>
                                <Touchable style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }} onPress={() => this.setState({ gender: 0, isChange: true })}>
                                    <Image source={this.state.gender === 0 ? require('../assets/circle_gry_dot.png') : require('../assets/circle_gry_line.png')} style={{ width: 16, height: 16 }} />
                                    <Text style={{ marginLeft: 10, fontSize: 17, color: Colors.TEXT_P }}>{convertLanguage(language, 'male')}</Text>
                                </Touchable>

                                <Touchable style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20, flex: 1 }} onPress={() => this.setState({ gender: 1, isChange: true })}>
                                    <Image source={this.state.gender === 1 ? require('../assets/circle_gry_dot.png') : require('../assets/circle_gry_line.png')} style={{ width: 16, height: 16 }} />
                                    <Text style={{ marginLeft: 10, fontSize: 17, color: Colors.TEXT_P }}>{convertLanguage(language, 'female')}</Text>
                                </Touchable>
                            </View>
                            {
                                errors.Gender &&
                                <Text style={{ color: '#EB5757', paddingHorizontal: 20, paddingTop: 5, textAlign: 'center' }}>{errors.Gender}</Text>
                            }


                            {/* birthday */}
                            <View style={{
                                alignSelf: 'stretch',
                                marginLeft: 20, marginRight: 20, marginTop: 20,
                                borderColor: '#bdbdbd',
                                borderWidth: 1,
                                height: 43.5,
                                borderRadius: 4,
                                paddingHorizontal: 12,
                                paddingTop: 3
                            }}>
                                <Text style={{ color: this.state.birthday.length > 0 ? '#828282' : 'rgba(189,189,189,1)', fontSize: 12 }}>{convertLanguage(language, 'birthday')}</Text>
                                <DatePicker
                                    style={{ width: '100%' }}
                                    locale={language}
                                    date={this.convertDate(this.state.birthday)}
                                    androidMode="spinner"
                                    mode="date"
                                    placeholder={convertLanguage(language, 'select_date')}
                                    format={language === 'vi' ? "DD-MM-YYYY" : 'YYYY-MM-DD'}
                                    minDate={language === 'vi' ? "01-01-1970" : "1970-01-01"}
                                    maxDate={dateNow()}
                                    confirmBtnText={convertLanguage(language, 'confirm')}
                                    cancelBtnText={convertLanguage(language, 'cancel')}
                                    iconComponent={<Image style={{ position: 'absolute', right: 0, width: 0, height: 0 }} source={require('../assets/calendar_icon.png')} />}
                                    customStyles={{
                                        dateInput: {
                                            borderLeftWidth: 0,
                                            borderRightWidth: 0,
                                            borderTopWidth: 0,
                                            borderBottomWidth: 0,
                                        },
                                        dateText: {
                                            color: Colors.TEXT_P, fontSize: 15, alignSelf: 'flex-start', paddingTop: 0, marginTop: -13
                                        },
                                        btnTextConfirm: {
                                            color: Colors.PRIMARY, fontSize: 17
                                        },
                                        btnTextCancel: {
                                            color: Colors.TEXT_P, fontSize: 16
                                        },
                                        dateTouchBody: {
                                            height: 32
                                        },
                                        placeholderText: {
                                            alignSelf: 'flex-start'
                                        }
                                        // ... You can check the source to find the other keys.
                                    }}
                                    onDateChange={(birthday) => this.setState({ birthday, isChange: true })}
                                />
                            </View>



                            {/* language */}
                            <View style={{
                                alignSelf: 'stretch',
                                marginLeft: 20, marginRight: 20, marginTop: 15,
                                borderColor: '#bdbdbd',
                                borderWidth: 1,
                                height: 43.6,
                                borderRadius: 4,
                                // paddingHorizontal: 12,
                                paddingTop: 3
                            }}>
                                <Text style={{ color: this.state.languages.length > 0 ? '#828282' : 'rgba(189,189,189,1)', fontSize: 12, paddingLeft: 12 }}>{convertLanguage(language, 'language')}</Text>

                                <View style={{ flex: 1, alignSelf: 'stretch' }}>
                                    <PickerItem
                                        boxDisplayStyles={{ width: '100%', justifyContent: 'space-between', height: 30 }}
                                        containerStyle={[{ height: 30, left: Platform.OS === 'android' ? 5 : 0, marginTop: -7 }, Platform.OS === 'ios' ? { paddingHorizontal: 12, marginTop: -13 } : {}]}
                                        iconStyle={{ marginRight: 0, marginTop: -13 }}
                                        options={[
                                            {
                                                name: convertLanguage(language, 'english'),
                                                value: 'en'
                                            },
                                            {
                                                name: convertLanguage(language, 'vietnamese'),
                                                value: 'vi'
                                            },
                                            {
                                                name: convertLanguage(language, 'korean'),
                                                value: 'ko'
                                            }
                                        ]}
                                        selectedOption={this.state.languages}
                                        onSubmit={(value) => { this.setState({ languages: value, isChange: true }) }}
                                    />
                                </View>
                            </View>


                            {/* sns login */}
                            <View style={{
                                marginHorizontal: 20,
                                marginTop: 24,
                                paddingHorizontal: 16,
                                paddingVertical: 24,
                                backgroundColor: '#F9F9F9',
                                borderRadius: 8
                            }}>
                                <Text style={{ color: '#00A9F4', fontSize: 16, marginBottom: 24 }}>{convertLanguage(language, 'sns_login')}</Text>
                                <View>
                                    {
                                        this.props.user.profile.Is_SNS_FB
                                            ?
                                            <Touchable
                                                onPress={this.facebook}
                                                disabled={loadingCheckSocial}
                                                style={{
                                                    width: '100%', height: 48,
                                                    justifyContent: 'center'
                                                }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image style={{ marginLeft: 10 }} source={this.state.avatarSource} style={{ width: 24, height: 24, marginRight: 10 }} />
                                                        <Text style={{ color: '#333333', fontSize: 16, flex: 1 }} numberOfLines={1}>{convertLanguage(language, 'facebook_connected')}</Text>
                                                    </View>
                                                    <Text style={{ color: '#333333', fontSize: 16, marginLeft: 10 }}>{convertLanguage(language, 'unLink')}</Text>
                                                </View>
                                            </Touchable>
                                            :
                                            <Touchable
                                                onPress={this.facebook}
                                                disabled={loadingCheckSocial}
                                                style={{
                                                    width: '100%', height: 48,
                                                    justifyContent: 'center'
                                                }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image style={{ marginRight: 10, width: 24, height: 24 }} source={require('../assets/ic_facebook.png')} />
                                                        <Text style={{ color: '#333333', fontSize: 15, }}>{convertLanguage(language, 'not_linked')}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                                                        <Text style={{ color: '#00A9F4', fontSize: 16 }}>{convertLanguage(language, 'link')}</Text>
                                                        <Image style={{ marginLeft: 6, width: 6, height: 10 }} source={require('../assets/arrow_right_blue.png')} />
                                                    </View>
                                                </View>
                                            </Touchable>
                                    }
                                    {
                                        this.props.user.err_social_fb !== '' &&
                                        <Text style={{ color: '#ff4081', fontSize: 12, marginTop: 2 }}>{this.props.user.err_social_fb}</Text>
                                    }
                                    {
                                        this.props.user.profile.Is_SNS_GG ?
                                            <Touchable
                                                onPress={this.google}
                                                disabled={loadingCheckSocial}
                                                style={{
                                                    width: '100%', height: 48,
                                                    justifyContent: 'center'
                                                }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image style={{ marginLeft: 10 }} source={this.state.avatarGg} style={{ width: 24, height: 24, marginRight: 10 }} />
                                                        <Text style={{ color: '#333333', fontSize: 16, flex: 1 }} numberOfLines={1}>{convertLanguage(language, 'google_connected')}</Text>
                                                    </View>
                                                    <Text style={{ color: '#333333', fontSize: 16, marginLeft: 10 }}>{convertLanguage(language, 'unLink')}</Text>
                                                </View>
                                            </Touchable>
                                            :
                                            <Touchable
                                                onPress={this.google}
                                                disabled={loadingCheckSocial}
                                                style={{
                                                    width: '100%', height: 48,
                                                    justifyContent: 'center'
                                                }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image style={{ marginRight: 10, width: 24, height: 24 }} source={require('../assets/ic_google.png')} />
                                                        <Text style={{ color: '#333333', fontSize: 15, }}>{convertLanguage(language, 'not_linked')}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                                                        <Text style={{ color: '#00A9F4', fontSize: 16 }}>{convertLanguage(language, 'link')}</Text>
                                                        <Image style={{ marginLeft: 6, width: 6, height: 10 }} source={require('../assets/arrow_right_blue.png')} />
                                                    </View>
                                                </View>
                                            </Touchable>
                                    }
                                    {
                                        this.props.user.err_social_gg !== '' &&
                                        <Text style={{ color: '#ff4081', fontSize: 12, marginTop: 2 }}>{this.props.user.err_social_gg}</Text>
                                    }
                                    {/* //apple */}

                                    {
                                        this.props.user.profile.Is_SNS_AP ?
                                            <Touchable
                                                onPress={this.apple}
                                                disabled={loadingCheckSocial}
                                                style={{
                                                    width: '100%', height: 48,
                                                    justifyContent: 'center'
                                                }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image source={this?.state?.avatarAp} style={{ width: 24, height: 24, marginRight: 10 }} />
                                                        <Text style={{ color: '#333333', fontSize: 16, flex: 1 }} numberOfLines={1}>{convertLanguage(language, 'apple_connected')}</Text>
                                                    </View>
                                                    <Text style={{ color: '#333333', fontSize: 16, marginLeft: 10 }}>{convertLanguage(language, 'unLink')}</Text>
                                                </View>
                                            </Touchable>
                                            :
                                            <Touchable
                                                onPress={this.apple}
                                                disabled={loadingCheckSocial}
                                                style={{
                                                    width: '100%', height: 48,
                                                    justifyContent: 'center'
                                                }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image style={{ marginRight: 10, width: 24, height: 24 }} source={require('../assets/Apple.png')} />
                                                        <Text style={{ color: '#333333', fontSize: 15, }}>{convertLanguage(language, 'not_linked')}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                                                        <Text style={{ color: '#00A9F4', fontSize: 16 }}>{convertLanguage(language, 'link')}</Text>
                                                        <Image style={{ marginLeft: 6, width: 6, height: 10 }} source={require('../assets/arrow_right_blue.png')} />
                                                    </View>
                                                </View>
                                            </Touchable>
                                    }
                                    <AppleLogin
                                        ref={ref => this.instagramLogin = ref}
                                        appId='com.starindex.login-apple'
                                        appSecret='eyJraWQiOiI3NVdRNFUyRzM0IiwiYWxnIjoiRVMyNTYifQ.eyJpc3MiOiIzSzlHWTkySzNKIiwiaWF0IjoxNTkwMDM2MjU5LCJleHAiOjE2MDU1ODgyNTksImF1ZCI6Imh0dHBzOi8vYXBwbGVpZC5hcHBsZS5jb20iLCJzdWIiOiJjb20uc3RhcmluZGV4LmxvZ2luLWFwcGxlIn0.Tbfbpla8740SCKU2mJ-JfLfON1x4rPrcpBwEyLOriJlaM3qa0T0IfW2kNDGOaIHenoGJnH4KI33KntSaZPqO-A'
                                        redirectUrl='https://www.comeup.asia/mobile-login-apple'
                                        scopes={['name', 'email']}
                                        modalVisible={true}
                                        onLoginSuccess={this.setTokenApple}
                                        onLoginFailure={(data) => console.log(data)}
                                    />

                                </View>
                            </View>

                            <Touchable
                                disabled={!this.should() || loadingUpdateProfile}
                                onPress={this.onUpdateProfile}
                                style={{
                                    marginBottom: 20, marginTop: 40,
                                    marginLeft: 20, marginRight: 20,
                                    alignSelf: 'stretch',
                                    minHeight: 50, borderColor: this.should() ? '#FFFFFF' : Colors.DISABLE, borderWidth: 1, borderRadius: 4,
                                    justifyContent: 'center', alignItems: 'center',
                                    backgroundColor: this.should() ? Colors.PRIMARY : '#FFFFFF'
                                }}>
                                <Text style={{ fontSize: 16, color: this.should() ? '#FFFFFF' : Colors.DISABLE, }}>{loadingUpdateProfile ? convertLanguage(language, 'waiting') : convertLanguage(language, 'save2')}</Text>
                            </Touchable>
                        </ScrollView>
                    </KeyboardAwareScrollView>

                </View>
                <CountryCode onPress={(data, flag) => { this.closePopover(); this.setState({ Zipcode: data, isChange: true, flag }) }}
                    isVisible={this.state.isVisible} touchable={this.touchable} language={language} closePopover={() => this.closePopover()} />
                {/* <Popover
                    isVisible={this.state.isVisible}
                    animationConfig={{ duration: 0 }}
                    fromView={this.touchable}
                    placement='bottom'
                    arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                    backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                    onRequestClose={() => this.closePopover()}>
                    <View style={styles.boxPopover}>
                        <Touchable onPress={() => { this.closePopover(); this.setState({ Zipcode: '+82', isChange: true }) }} style={styles.boxCountry}>
                            <Image source={require('../assets/flag_kr.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+82</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopover(); this.setState({ Zipcode: '+84', isChange: true }) }} style={styles.boxCountry}>
                            <Image source={require('../assets/flag_vn.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+84</Text>
                        </Touchable>
                        <Touchable onPress={() => { this.closePopover(); this.setState({ Zipcode: '+66', isChange: true }) }} style={styles.boxCountry}>
                            <Image source={require('../assets/flag_th.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>+66</Text>
                        </Touchable>
                    </View>
                </Popover> */}
            </SafeView >
        );
    }
}

const styles = StyleSheet.create({
    inputContainerStyle: {
        borderWidth: 1,
        borderRadius: 4,
        paddingHorizontal: 12,
        // height: 62,
        flex: 1,
    },
    containerStyle: {
        paddingHorizontal: 20,
        flex: 1,
        marginVertical: 6,
    },
    input: {
        color: '#333333',
    },
    boxPopover: {
        width: 130,
        padding: 12,
    },
    boxCountry: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 6
    },
    ic_country: {
        width: 24,
        height: 24,
        marginRight: 12
    },
    txtCountry: {
        color: '#333333',
        fontSize: 14,
        // paddingBottom: -5
    },
})

const mapStateToProps = state => {
    return {
        user: state.user,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onCheckLoginSocial: (body, type) => {
            dispatch(actCheckLoginSocial(body, type))
        },
        removeConnectFacebook: (type, language) => {
            dispatch(actRemoveConnectFacebook(type, language))
        },
        onUpdateProfile: (body) => {
            dispatch(actUpdateProfile(body))
        },
        getLogout: (navigation) => {
            dispatch(actLogout(navigation))
        },
        onVerifyEmail: () => {
            dispatch(actVerifyEmail())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
