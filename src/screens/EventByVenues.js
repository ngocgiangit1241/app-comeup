import React, { useState, useEffect, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet } from 'react-native'
import SafeView from '../screens_view/SafeView'
import Touchable from '../screens_view/Touchable'
import Line from '../screens_view/Line'
import ItemEvent from '../components/event/ItemEvent'
import * as Colors from '../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataEventByVenue } from '../actions/venue'
import { actLikeEvent } from '../actions/event'
import { convertLanguage } from '../services/Helper'
import * as Types from '../constants/ActionType';

export default function EventByVenues(props) {
    const dispatch = useDispatch()
    useEffect(() => {
        return () => {
            dispatch({ type: Types.LOAD_DATA_EVENT_BY_VENUE_CLEAR });
        }
    }, [])
    const [data, setData] = useState({
        page: 1,
        data: []
    })
    const { language } = useSelector(state => state.language)
    const venue = useSelector(state => state.venue.venueHostingEvent)
    console.log('venue', venue)
    useEffect(() => {
        dispatch(actLoadDataEventByVenue(props?.route?.params?.Slug, data.page))
    }, [data.page])
    const _renderFooter = () => {
        if (venue.total !== data.page) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            return <View></View>
        }
    }
    const loadMore = () => {
        if (data.page < venue.total) {
            setData({ ...data, page: data.page + 1 })
        }
    }
    return (
        <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
            <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                <Touchable
                    onPress={() => props.navigation.goBack()}
                    style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                </Touchable>
                <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                <View style={{ minHeight: 48, marginRight: 20, justifyContent: 'center', alignItems: 'center' }}></View>
            </View>

            <Line />

            <View style={styles.content}>
                <FlatList
                    style={{ paddingTop: 10 }}
                    data={venue.data}
                    renderItem={({ item }) => {
                        return <ItemEvent cancel_text={convertLanguage(language, 'canceled_event')} data={item} navigation={props.navigation} onLikeEvent={(Id) => console.log('111')} />
                    }}
                    onEndReached={() => { loadMore() }}
                    // ListHeaderComponent={() => renderHeader()}
                    // ItemSeparatorComponent={renderSep}
                    onEndReachedThreshold={0.5}
                    keyExtractor={(item, index) => index.toString()}
                    // refreshing={props.team.loading}
                    // onRefresh={() => {refresh()}}
                    ListFooterComponent={() => _renderFooter()}
                />
            </View>
        </SafeView>
    )
}
const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
});
