import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Dimensions, Animated } from 'react-native';

import Touchable from '../screens_view/Touchable'
import Line from '../screens_view/Line'
import SafeView from '../screens_view/SafeView'
import TeamSearchMember from './TeamSearchMember'
import TeamTabFollowers from './TeamTabFollowers'
import TeamShare from './TeamShare'

import { TabView } from 'react-native-tab-view';

import * as Colors from '../constants/Colors'

import { actClearTeamInviteSearch } from '../actions/team';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper'
const FirstRoute = () => (
    <View style={[styles.scene, { backgroundColor: '#ff4081' }]} />
);
const SecondRoute = () => (
    <View style={[styles.scene, { backgroundColor: '#673ab7' }]} />
);

class TeamInvite extends Component {
    constructor(props) {
        super(props);
        var { language } = this.props.language;
        this.state = {
            index: 0,
            routes: [
                { key: 'Search', title: convertLanguage(language, 'search') },
                { key: 'Followers', title: convertLanguage(language, 'followers') },
                { key: 'SNS', title: convertLanguage(language, 'sns') },
            ],
        };
    }

    _renderTabBar = props => {
        return (
            <View style={styles.tabBar}>
                {props.navigationState.routes.map((route, i) => {
                    return (
                        <TouchableOpacity
                            style={[styles.tabItem, { borderBottomColor: this.state.index === i ? '#03a9f4' : '#e8e8e8' }]}
                            onPress={() => this.setState({ index: i })}>
                            <Text style={{ color: this.state.index === i ? '#03a9f4' : '#333333', fontWeight: this.state.index === i ? 'bold' : '400', fontSize: 15 }}>{route.title}</Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };

    componentWillUnmount() {
        this.props.clearTeamInviteSearch()
    }

    render() {
        let team = this.props.team.team;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => {
                            this.props.route.params['callback']();
                            this.props.navigation.goBack()
                        }}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                </View>
                <Line />
                <View style={styles.header}>
                    <Text style={styles.txtTitle}>{convertLanguage(language, 'invite_people_and_build_your_team')}</Text>
                </View>
                <TabView
                    style={styles.container}
                    navigationState={this.state}
                    labelStyle={{ backgroundColor: 'red' }}
                    renderScene={({ route }) => {
                        switch (route.key) {
                            case 'Search':
                                return <TeamSearchMember />
                            case 'Followers':
                                return <TeamTabFollowers />
                            case 'SNS':
                                return <TeamShare />
                        }
                    }}
                    onIndexChange={index => this.setState({ index })}
                    renderTabBar={this._renderTabBar}
                />
            </SafeView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        paddingTop: 0,
    },
    header: {
        padding: 15,
        paddingTop: 5,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    txtTitle: {
        fontWeight: 'bold',
        fontSize: 17,
        color: '#333333'
    },
    content: {
        flex: 1,
        padding: 5
    },
    rowTabbar: {
        flexDirection: 'row',
        marginTop: 10
    },
    btnTabbar: {
        flex: 1,
        padding: 10,
        borderBottomWidth: 1,
        borderColor: Colors.GRAY
    },
    txtBtnTabbar: {
        fontSize: 16,
        alignSelf: 'center'
    },
    btnTabbarActive: {
        borderBottomWidth: 3,
        borderBottomColor: Colors.PRIMARY
    },
    txtBtnTabbarActive: {
        color: Colors.PRIMARY
    },
    btnTabbar1: {
        marginRight: 6,
    },
    btnTabbar2: {
        marginLeft: 6,
        marginRight: 6,
    },
    btnTabbar3: {
        marginLeft: 6,
    },
    space: {
        borderLeftWidth: 1,
        borderColor: Colors.GRAY,
        marginTop: 5,
        marginBottom: 5
    },


    tabbar: {
        backgroundColor: '#FFFFFF',
    },
    tab: {
        width: Dimensions.get('window').width / 3 - 1,
        backgroundColor: '#FFFFFF',
        padding: 5,
        borderBottomWidth: 1,
    },
    tabIndicator: {
        backgroundColor: Colors.PRIMARY,
        borderWidth: 2,
        borderColor: Colors.PRIMARY,
    },
    tabLabel: {
        color: Colors.PRIMARY,
        fontWeight: '400',
    },
    tabBar: {
        flexDirection: 'row',
        // borderWidth: 1,
        // borderColor: '#000000'
    },
    tabItem: {
        flex: 1,
        alignItems: 'center',
        padding: 16,
        borderBottomWidth: 2,
        borderBottomColor: '#000000'
    },
});
const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        clearTeamInviteSearch: () => {
            dispatch(actClearTeamInviteSearch())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamInvite);
