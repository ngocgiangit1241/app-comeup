import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, Dimensions, Linking } from 'react-native';
import Popover from 'react-native-popover-view';
import FastImage from 'react-native-fast-image';
import SafeView from '../screens_view/SafeView'
import Touchable from '../screens_view/Touchable'
import Line from '../screens_view/Line'
import Dots from 'react-native-dots-pagination';
import ItemTeam from '../components/team/ItemTeam'
import * as Colors from '../constants/Colors'
import Carousel from 'react-native-snap-carousel';
import { actTeamLists, actTeamFollow, actLoadBannerTeam, actClearListTeam } from '../actions/team';
import { actLoadDataCategory } from '../actions/category'
import { actSelectCountry } from '../actions/city'
import ModalCountry from '../components/ModalCountry'
import ModalType from '../components/ModalType'
import ModalTag from '../components/ModalTag'
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper'
const { width } = Dimensions.get('window')
class TeamList extends Component {
    constructor(props) {
        super(props);
        var { language } = this.props.language;
        this.state = {
            page: 0,
            // country: '',
            // status_team: '',
            // tag: '',
            activeSlide: 0,
            data_types: [
                {
                    name: convertLanguage(language, 'newTeam'),
                    value: 'new'
                },
                {
                    name: convertLanguage(language, 'popular'),
                    value: 'popular'
                },
                {
                    name: convertLanguage(language, 'famous'),
                    value: 'famous'
                },
            ],
            status_team: {
                name: convertLanguage(language, 'popular'),
                value: 'popular',
                icon: require('../assets/ic_popular.png')
            },
            tag: {
                name: convertLanguage(language, 'all_categories'),
                value: ''
            },
            country: this.props.global.country,
            modalCountry: false,
            modalType: false,
            modalTag: false,
            refresh: false,
        };
    }
    async componentDidMount() {
        this.getTeams();
        this.props.getBannerTeam()
        this.props.onLoadDataCategory('team');
        var { countries } = this.props.city;
        if (countries.length === 0) {
            this.props.onLoadDataCountry();
        }
    }
    componentWillUnmount() {
        this.props.onClearListTeam()
    }
    componentDidUpdate(prevProps) {
        if (this.state.tag.value === '') {
            if (this.state.tag.name != convertLanguage(prevProps.language.language, 'all_categories')) {
                this.setState({
                    tag: {
                        name: convertLanguage(prevProps.language.language, 'all_categories'),
                        value: '',
                    }

                })
            }
        }

        // 

        if (this.state.status_team.value === 'popular') {
            if (this.state.status_team.name != convertLanguage(prevProps.language.language, 'popular')) {
                this.setState({
                    status_team: {
                        name: convertLanguage(prevProps.language.language, 'popular'),
                        value: 'popular',
                        icon: require('../assets/ic_popular.png')
                    },
                })
            }

        } else if (this.state.status_team.value === 'new') {
            if (this.state.status_team.name != convertLanguage(prevProps.language.language, 'newTeam')) {
                this.setState({
                    status_team: {
                        name: convertLanguage(prevProps.language.language, 'newTeam'),
                        value: 'new',
                        icon: require('../assets/ic_new.png')
                    },
                })
            }
        } else {

        }
    }
    followTeam = (teamId) => {
        this.props.followTeam(teamId)
    }
    renderItem = (item, index) => {
        var disabled = this.props.team.teamIdAction == item.Id ? this.props.team.loadingFollowers : false;
        return <ItemTeam
            style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}
            key={'team_' + item.Id}
            data={item}
            followTeam={this.followTeam}
            disabled={disabled}
            navigation={this.props.navigation}
        />
    }

    getTeams() {
        var { country, status_team, tag } = this.state
        var page = this.state.page + 1;
        this.props.getTeams(page, country.Id, status_team.value, tag.value);
        this.setState({ page: page });
    }

    refresh() {
        var { country, status_team, tag } = this.state
        var page = 1
        this.props.getTeams(page, country.Id, status_team.value, tag.value);
        this.setState({ page: 1 });
    }

    loadMore() {
        var { loadMore } = this.props.team;
        if (loadMore) {
            this.getTeams();
        }
    }

    showDataCategory() {
        var { categories } = this.props.category;
        var result = [
            {
                name: convertLanguage(this.props.language.language, 'all_categories'),
                value: ''
            }
        ]
        categories.forEach(category => {
            result.push({
                name: category.HashTagName,
                value: category.Id
            })
        });
        return result;
    }

    async onSelect(key, value) {
        await this.setState({
            [key]: value,
            modalCountry: false,
            modalType: false,
            modalTag: false,
        })
        this.refresh()
    }

    renderHeader = () => {
        var { language } = this.props.language;
        var { country, status_team, tag, activeSlide } = this.state;
        return <View>
            {
                this.props.team.banners.length > 0 &&
                <View>
                    <Carousel
                        data={this.props.team.banners}
                        renderItem={({ item, index }) => {
                            return <Touchable onPress={() => Linking.openURL(item.LinkApp)} disabled={item.LinkApp === '#'}><FastImage style={{ backgroundColor: '#bcbcbc', aspectRatio: 2.4 }} source={{ uri: item.Images ? item.Images.Medium : item.Image }} /></Touchable>
                        }}
                        sliderWidth={width}
                        itemWidth={width}
                        inactiveSlideScale={1}
                        onSnapToItem={(index) => this.setState({ activeSlide: index })}
                        autoplay={true}
                        autoplayInterval={3000}
                        loop
                    />
                    {
                        this.props.team.banners.length > 1 &&
                        <View style={{ position: 'absolute', bottom: 0, alignSelf: 'center' }}>
                            <Dots length={this.props.team.banners.length} active={activeSlide} activeColor={'#FFFFFF'} width={100} passiveDotWidth={6} passiveDotHeight={6} activeDotWidth={9} activeDotHeight={9} paddingVertical={6} passiveColor={'rgba(255, 255, 255, 0.5)'} marginHorizontal={3} />
                        </View>
                    }
                </View>
            }
            <View style={styles.boxFilter}>
                <View style={styles.boxFilter1}>
                    <Touchable style={[styles.boxItemFilter, { flex: 0.47 }]} onPress={() => this.setState({ modalCountry: true })} ref={ref => this.touchable2 = ref}>
                        <Image source={require('../assets/ic_location_blue.png')} style={styles.ic_location} />
                        <Text style={styles.txtFilter} numberOfLines={1}>{country['Name_' + language]}</Text>
                        <Image source={require('../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                    </Touchable>
                    <Touchable style={[styles.boxItemFilter, { flex: 0.47 }]} onPress={() => this.setState({ modalType: true })} ref={ref => this.touchable = ref}>
                        <Image source={status_team.icon} style={styles.ic_location} />
                        <Text style={styles.txtFilter} numberOfLines={1}>{status_team.name}</Text>
                        <Image source={require('../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                    </Touchable>
                </View>
                <Touchable style={styles.boxItemFilter} onPress={() => this.setState({ modalTag: true })}>
                    <Image source={require('../assets/ic_category_blue.png')} style={styles.ic_location} />
                    <Text style={styles.txtFilter} numberOfLines={1}>{tag.name}</Text>
                    <Image source={require('../assets/select_arrow_black.png')} style={styles.ic_dropdown} />
                </Touchable>
            </View>
        </View>
    }
    _renderFooter() {
        var { loadMore, is_empty } = this.props.team;
        var { language } = this.props.language;
        if (loadMore) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'team_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    goBack = () => {
        var { index, routes } = this.props.navigation.dangerouslyGetState();
        if (routes.length > 1 && ['Login', 'LoginWithMail', 'ForgotPw', 'Splash'].includes(routes[index - 1].name)) {
            this.props.navigation.reset({ index: 0, routes: [{ name: 'Main' }], })
        } else {
            this.props.navigation.goBack();
        }
    }

    render() {
        var teams = this.props.team.teams;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={this.goBack}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'team')}</Text>
                    <Touchable style={{ minHeight: 48, minWidth: 48, marginRight: 5, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('SearchResult')}>
                        <Image source={require('../assets/search.png')} style={{ width: 32, height: 32 }} />
                    </Touchable>
                </View>

                <Line />

                {
                    <FlatList
                        // style={{ flex: 1, width: '100%' }}
                        contentContainerStyle={{ paddingBottom: 20 }}
                        data={teams}
                        renderItem={({ item, index }) => {
                            return this.renderItem(item, index)
                        }}
                        onEndReached={() => { this.loadMore() }}
                        ListHeaderComponent={this.renderHeader}
                        // ItemSeparatorComponent={this.renderSep}
                        onEndReachedThreshold={0.5}
                        refreshing={false}
                        onRefresh={() => { this.refresh() }}
                        ListFooterComponent={() => this._renderFooter()}
                    />


                }
                {
                    // this.state.modalCountry && this.props.city.countries.length > 0
                    //     ?
                    //     <ModalCountry
                    //         modalVisible={this.state.modalCountry}
                    //         closeModal={() => this.setState({ modalCountry: false })}
                    //         countries={this.props.city.countries}
                    //         selectCountry={(city) => this.onSelect('country', city)}
                    //     />
                    //     : null
                }
                {
                    // this.state.modalType && this.props.city.countries.length > 0
                    //     ?
                    //     <ModalType
                    //         modalVisible={this.state.modalType}
                    //         closeModal={() => this.setState({ modalType: false })}
                    //         types={this.state.data_types}
                    //         selectType={(type) => this.onSelect('status_team', type)}
                    //     />
                    //     : null
                }
                {
                    this.state.modalTag
                        ?
                        <ModalTag
                            modalVisible={this.state.modalTag}
                            closeModal={() => this.setState({ modalTag: false })}
                            tags={this.showDataCategory()}
                            selectTag={(tag) => this.onSelect('tag', tag)}
                        />
                        : null
                }
                <Popover
                    isVisible={this.state.modalType}
                    animationConfig={{ duration: 0 }}
                    fromView={this.touchable}
                    placement='bottom'
                    popoverStyle={{
                        borderRadius: 8
                    }}
                    arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                    backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.05)', }}
                    onRequestClose={() => this.setState({ modalType: false })}>
                    <View style={styles.boxPopover}>
                        <Touchable onPress={() => this.onSelect('status_team', { name: convertLanguage(language, 'popular'), value: 'popular', icon: require('../assets/ic_popular.png') })} style={styles.boxCountry}>
                            <Image source={require('../assets/ic_popular.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>{convertLanguage(language, 'popular')}</Text>
                        </Touchable>
                        <Touchable onPress={() => this.onSelect('status_team', { name: convertLanguage(language, 'newTeam'), value: 'new', icon: require('../assets/ic_new.png') })} style={styles.boxCountry}>
                            <Image source={require('../assets/ic_new.png')} style={[styles.ic_country]} />
                            <Text style={styles.txtCountry}>{convertLanguage(language, 'newTeam')}</Text>
                        </Touchable>
                        {/* <Touchable onPress={() => this.onSelect('status_team', { name: convertLanguage(language, 'famous'), value: 'famous', icon: require('../assets/ic_famous.png') })} style={styles.boxCountry}>
                            <Image source={require('../assets/ic_famous.png')} style={styles.ic_country} />
                            <Text style={styles.txtCountry}>{convertLanguage(language, 'famous')}</Text>
                        </Touchable> */}
                    </View>
                </Popover>
                <Popover
                    isVisible={this.state.modalCountry}
                    animationConfig={{ duration: 0 }}
                    fromView={this.touchable2}
                    placement='bottom'
                    popoverStyle={{
                        borderRadius: 8
                    }}
                    arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                    backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.05)', }}
                    onRequestClose={() => this.setState({ modalCountry: false })}>
                    <View style={styles.boxPopover}>
                        {
                            this.props.city.countries.map((item, index) => {
                                return <Touchable onPress={() => this.onSelect('country', item)} style={styles.boxCountry} key={index}>
                                    <Text style={styles.txtCountry}>{item[`Name_${language}`] || item.Name}</Text>
                                </Touchable>
                            })
                        }
                        {/* <Touchable onPress={() => this.onSelect('status_team', 'popular')} style={styles.boxCountry}>
                            <Text style={styles.txtCountry}>{convertLanguage(language, 'popular')}</Text>
                        </Touchable>
                        <Touchable onPress={() => this.onSelect('status_team', 'new')} style={styles.boxCountry}>
                            <Text style={styles.txtCountry}>{convertLanguage(language, 'new')}</Text>
                        </Touchable>
                        <Touchable onPress={() => this.onSelect('status_team', 'famous')} style={styles.boxCountry}>
                            <Text style={styles.txtCountry}>{convertLanguage(language, 'famous')}</Text>
                        </Touchable> */}
                    </View>
                </Popover>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    boxFilter: {
        // flexDirection: 'row',
        // justifyContent: 'space-between',
        // alignItems: 'center',
        padding: 16,
        paddingBottom: 0
    },
    boxFilter1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 10
    },
    loading: {
        padding: 20
    },
    textHint: {
        padding: 20,
        textAlign: 'center'
    },
    boxItemFilter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 36,
        borderRadius: 8,
        borderColor: '#BDBDBD',
        borderWidth: 1,
        paddingHorizontal: 10
    },
    txtFilter: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold'
    },
    ic_dropdown: {
        width: 16,
        height: 16
    },
    ic_location: {
        width: 20,
        height: 20
    },
    boxPopover: {
        width: (width - 32) * 0.47,
        padding: 12,
    },
    boxCountry: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 6
    },
    ic_country: {
        width: 24,
        height: 24,
        marginRight: 24
    },
    txtCountry: {
        color: '#4F4F4F',
        fontSize: 16,
        fontWeight: '600'
    },
});

const mapStateToProps = state => {
    return {
        team: state.team,
        city: state.city,
        category: state.category,
        language: state.language,
        global: state.global
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getTeams: (page, country, status_team, tag) => {
            dispatch(actTeamLists(page, 10, country, status_team, tag))
        },
        followTeam: (teamId) => {
            dispatch(actTeamFollow(teamId))
        },
        onLoadDataCategory: (type) => {
            dispatch(actLoadDataCategory(type))
        },
        onLoadDataCountry: () => {
            dispatch(actSelectCountry())
        },
        getBannerTeam: () => {
            dispatch(actLoadBannerTeam())
        },
        onClearListTeam: () => {
            dispatch(actClearListTeam())
        }

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamList);
