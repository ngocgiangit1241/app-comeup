import React, { Component } from 'react';
import { View, Text, Image, ScrollView, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';

import Touchable from '../screens_view/Touchable'
import SafeView from '../screens_view/SafeView'
import Line from '../screens_view/Line'
import MemberTeamItem from '../components/member/MemberTeamItem'

import * as Colors from '../constants/Colors'
import { convertLanguage } from '../services/Helper'
import { actListMembers, actLeaveTeam, actSetTeamRole, actInvitedMember } from '../actions/team';
import { connect } from "react-redux";

class TeamMember extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentWillMount() {
        this.props.listMembers(this.props.route.params['Slug']);
    }
    onRefresh() {
        this.props.listMembers(this.props.route.params['Slug']);
    }
    submitLeaveTeam(memberId) {
        var auth = this.props.user.profile;
        var isAuth = auth.Id == memberId ? true : false
        this.props.leaveTeam(this.props.team.team.Id, memberId, this.props.navigation, isAuth);
    }
    submitSetRole(memberId, role) {
        this.props.setTeamRole(this.props.team.team.Id, memberId, role, this.props.navigation);
    }
    submitInvitedMember(memberId) {
        this.props.invitedMember(this.props.team.team.Id, memberId, this.props.navigation);
    }



    renderListMember(Role = '') {
        var members = this.props.team.Members;
        var team = this.props.team.team;
        var auth = this.props.user.profile;
        var { language } = this.props.language;
        if (!members) {
            return null;
        }
        if (Role == '') {
            var membersFilters = members.filter(function (member) {
                return member.Role != 'Invited'
            });
            if (!this.props.team.loadingMembers && membersFilters.length === 0) {
                return <Text style={{ textAlign: 'center', padding: 20 }}>{convertLanguage(language, 'no_data')}</Text>
            }
        } else {
            var membersFilters = members.filter(function (member) {
                return member.Role == Role
            });
            if (!this.props.team.loadingMembers && membersFilters.length === 0) {
                return <Text style={{ textAlign: 'center', padding: 20 }}>{convertLanguage(language, 'no_data')}</Text>
            }
        }
        return membersFilters.map((member, index) => {
            return <MemberTeamItem
                key={index}
                member={member}
                team={team}
                leaveTeam={() => { this.submitLeaveTeam(member.Id) }}
                setTeamRole={(role) => { this.submitSetRole(member.Id, role) }}
                invitedMember={() => { this.submitInvitedMember(member.Id) }}
                auth={auth}
                language={language}
            />
        });
    }

    render() {
        let team = this.props.team.team;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                </View>
                <Line />
                <ScrollView style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.colThumb}>
                            <Image
                                resizeMode='cover'
                                style={styles.imgLogo}
                                source={team.Logos ? { uri: team.Logos.Medium } : null} />
                        </View>
                        <View style={styles.colTeamInfo}>
                            <View style={styles.rowTeamName}>
                                <Text style={styles.txtTeamName}>
                                    {team.Name}
                                </Text>
                                {
                                    team.Verify == 1 &&
                                    <Image style={styles.imgCertified} source={require('../assets/team_certified.png')} />
                                }
                            </View>
                            <Text style={styles.txtTeamId}>ID: {team.Code}</Text>
                        </View>
                    </View>
                    {
                        (team.Role === 'Leader' || team.Role === 'Director') &&
                        <TouchableOpacity style={styles.btnInviteMember} onPress={() => { this.props.navigation.navigate('TeamInvite', { callback: () => this.onRefresh() }) }}>
                            <Text style={styles.txtBtnInviteMember}>+ {convertLanguage(language, 'invite_member')}</Text>
                        </TouchableOpacity>
                    }
                    <View style={styles.content}>
                        <View style={styles.rowHTotalMember}>
                            <Text style={styles.txtTotalMember}>{this.props.team.team.MembersCount ? this.props.team.team.MembersCount : 0} {convertLanguage(language, this.props.team.team.MembersCount && this.props.team.team.MembersCount > 1 ? 'members' : 'member')}</Text>
                        </View>
                        {
                            this.props.team.loadingMembers ?
                                <ActivityIndicator size="large" color="#000000" style={styles.loading} />
                                : null
                        }
                        {
                            this.renderListMember()
                        }
                        <Text style={styles.txtTitlePending}>{convertLanguage(language, 'pending_invitation')}</Text>
                        {
                            this.renderListMember('Invited')
                        }
                    </View>
                </ScrollView>
            </SafeView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        padding: 20,
        paddingTop: 0,
        flex: 1,
    },
    content: {
        flex: 1,
        padding: 5
    },
    loading: {
        margin: 10
    },
    header: {
        flexDirection: 'row',
        paddingBottom: 20,
        paddingTop: 20
    },
    colThumb: {
        flex: 0.3,
        justifyContent: 'flex-start'
    },
    colTeamInfo: {
        flex: 0.7,
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10

    },
    imgLogo: {
        // alignSelf: 'center',
        width: 80,
        height: 80,
        borderRadius: 40
    },
    rowTeamName: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtTeamName: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#333333'
    },
    txtTeamId: {
        fontSize: 18,
        color: '#333333'
    },
    imgCertified: {
        maxWidth: 30,
        marginLeft: 10
    },
    rowHTotalMember: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
    },
    txtTotalMember: {
        fontWeight: 'bold',
        color: '#333333',
        fontSize: 18
    },
    btnInviteMember: {
        padding: 15,
        borderWidth: 1,
        borderColor: Colors.PRIMARY,
        borderRadius: 5,
        alignItems: 'center'
    },
    txtBtnInviteMember: {
        color: Colors.PRIMARY,
        fontWeight: 'bold'
    },
    txtTitlePending: {
        fontWeight: 'bold',
        paddingTop: 20,
        paddingBottom: 20,
        color: '#333333',
        fontSize: 18
    }

});
const mapStateToProps = state => {
    return {
        team: state.team,
        user: state.user,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        listMembers: (id) => {
            dispatch(actListMembers(id))
        },
        leaveTeam: (teamId, memberId, navigation, isAuth) => {
            dispatch(actLeaveTeam(teamId, memberId, navigation, isAuth))
        },
        setTeamRole: (teamId, memberId, role, navigation) => {
            dispatch(actSetTeamRole(teamId, memberId, role, navigation))
        },
        invitedMember: (teamId, memberId, navigation) => {
            dispatch(actInvitedMember(teamId, memberId, navigation))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamMember);
