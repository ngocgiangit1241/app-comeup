import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, TouchableOpacity, ActivityIndicator, Platform, KeyboardAvoidingView, ScrollView, Alert } from 'react-native';

import Touchable from '../screens_view/Touchable'
import SafeView from '../screens_view/SafeView'

import * as Colors from '../constants/Colors'

import { actReportTeam } from '../actions/team';
import { connect } from "react-redux";
import { convertLanguage } from '../services/Helper'
class TeamReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            content: '',
        };
    }


    submitData() {
        if (this.state.content.replace(/\s/g, '').length > 0 && !this.props.team.loadingReport) {
            this.props.reportTeam(this.props.team.team.Id, this.state.content, this.props.navigation);
        }
    }

    submitReport() {
        var { language } = this.props.language;
        Alert.alert(

            '',
            convertLanguage(language, 'are_you_sure'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log("Cancel Pressed"),
                    style: "destructive"
                },
                {
                    text: convertLanguage(language, 'ok'), onPress: () => this.submitData(),
                    // style: "cancel"
                }
            ],
            { cancelable: false }
        );
    }

    render() {
        var { language } = this.props.language;
        // var language = 'vi';
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ margin: 20, marginBottom: 0 }}>
                    <Touchable
                        onPress={() =>
                            this.props.navigation.goBack()
                        }
                        style={{
                            minWidth: 40, minHeight: 40, alignSelf: 'flex-start',
                            justifyContent: 'center',
                        }}>
                        <Image source={require('../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Touchable
                        onPress={() => { this.submitReport() }}
                        style={{
                            paddingTop: 5,
                            paddingBottom: 5,
                            paddingLeft: 10,
                            paddingRight: 10,
                            borderRadius: 4,
                            justifyContent: 'center', alignItems: 'center',
                            position: 'absolute', right: 0, top: 0, borderWidth: 1,
                            borderColor: this.state.content.replace(/\s/g, '').length == 0 ? Colors.DISABLE : Colors.PRIMARY,
                            backgroundColor: this.state.content.replace(/\s/g, '').length == 0 ? '#FFFFFF' : Colors.PRIMARY
                        }}>
                        <Text style={[styles.txtBtnReport, { color: this.state.content.replace(/\s/g, '').length == 0 ? Colors.DISABLE : '#FFFFFF' }]}>{convertLanguage(language, 'report2')}</Text>
                        {
                            this.props.team.loadingReport ?
                                <ActivityIndicator size="small" />
                                : null
                        }
                    </Touchable>
                </View>
                <View style={styles.rowTitle}>
                    {/* <Text style={styles.txtTitle}>{convertLanguage(language, 'let_comeup_know')}</Text> */}
                    {
                        language === 'ko' ?
                            <>
                                <Text style={styles.txtTitle}>{this.props.team.team.Name}</Text>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'what_you_want_to_report_about_team')}</Text>
                            </>
                            :
                            <>
                                <Text style={styles.txtTitle}>{convertLanguage(language, 'what_you_want_to_report_about_team')}</Text>
                                <Text style={styles.txtTitle}>{this.props.team.team.Name}</Text>
                            </>
                    }
                </View>
                <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.OS === 'android' ? -500 : 0} style={{ flex: 1 }} >
                    <ScrollView style={styles.rowContent}>

                        <TextInput
                            style={styles.ipContent}
                            multiline={true}
                            placeholder={convertLanguage(language, 'write_your_messages')}
                            value={this.state.content}
                            onChangeText={(content) => this.setState({ content })}
                        // onEndEditing={(e) => this.setState({content: e.nativeEvent.text})}
                        />
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeView>
        );
    }
}
const styles = StyleSheet.create({
    rowTitle: {
        alignItems: "center",
        marginBottom: 20,
        marginTop: 20
    },
    txtTitle: {
        fontSize: 18,
        color: '#333333',
        fontWeight: 'bold', textAlign: 'center'
    },
    rowContent: {
        flex: 0.7,
        margin: 20,
        marginTop: 0
    },
    ipContent: {
        height: 400,
        paddingVertical: 10,
        paddingHorizontal: 10,
        fontSize: 18,
        textAlignVertical: 'top',
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#bcbcbc'
    },
    rowAction: {
        flex: 0.3,
        alignItems: "center",
        marginTop: 10
    },
    btnReport: {
        flexDirection: 'row',
        borderWidth: 1,
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 40,
        paddingRight: 40,
        borderRadius: 4,
        borderColor: Colors.DISABLE
    },
    txtBtnReport: {
        fontSize: 18,
        color: Colors.DISABLE,
        paddingRight: 3
    },
    btnReportActive: {
        borderColor: Colors.PRIMARY
    },
    txtBtnReportActive: {
        color: Colors.PRIMARY
    },
});
const mapStateToProps = state => {
    return {
        team: state.team,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        reportTeam: (id, content, navigation) => {
            dispatch(actReportTeam(id, content, navigation))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamReport);
