import React, { Component } from 'react';
import { View } from 'react-native';

import { WebView } from "react-native-webview";

export default class Web extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'red' }}>
                <WebView
                    style={{ flex: 1, backgroundColor: 'green' }}
                    source={{ uri: 'https://www.comeup.kr/en/legal/terms' }}
                />
            </View>
        );
    }
}
