import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, RefreshControl, Alert } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import SrcView from '../../screens_view/ScrView'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper'
import { actLoadDataCollaboratingTeam, actConfirmCollaborate, actDismissCollaborate, actDeleteCollaborate } from '../../actions/event'

class CollaboratingTeam extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOnpress: true,
        };
    }

    componentDidMount() {
        this.props.onLoadDataCollaboratingTeam(this.props.route.params['Slug']);
    }

    refresh() {
        this.props.onLoadDataCollaboratingTeam(this.props.route.params['Slug']);
    }

    onConfirmCollaborate(Slug, TeamId) {
        this.setState({
            isOnpress: false
        })
        this.props.onConfirmCollaborate(Slug, TeamId)
        setTimeout(() => {
            this.setState({
                isOnpress: true
            })
        }, 3000)
    }
    onDismissCollaborate(Slug, TeamId) {

        var { language } = this.props.language;
        Alert.alert(
            "",
            convertLanguage(language, 'do_you_want_to_dismiss_this_team'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log("Cancel Pressed"),
                    style: "destructive"
                },
                {
                    text: convertLanguage(language, 'ok'), onPress: () => {
                        this.setState({
                            isOnpress: false
                        })
                        this.props.onDismissCollaborate(Slug, TeamId)
                        setTimeout(() => {
                            this.setState({
                                isOnpress: true
                            })
                        }, 3000)
                    }
                }
            ]
        )
    }
    onDeleteCollaborate(Slug, TeamId) {
        this.setState({
            isOnpress: false
        })
        this.props.onDeleteCollaborate(Slug, TeamId)
        setTimeout(() => {
            this.setState({
                isOnpress: true
            })
        }, 3000)
    }

    render() {
        var { loading, collaborates, invitations } = this.props.event;
        var { isOnpress } = this.state;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    {/* <Touchable style={{ height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 1, backgroundColor: '#03a9f4', marginRight: 20 }} onPress={() => null}>
                        <Text style={{ fontSize: 17, color: '#FFFFFF', fontWeight: '400', paddingLeft: 15, paddingRight: 15 }}>Save</Text>
                    </Touchable> */}
                </View>

                <Line />
                <SrcView
                    style={{ flex: 1 }}
                    refreshControl={
                        <RefreshControl
                            refreshing={loading}
                            onRefresh={() => this.refresh()}
                        />
                    }
                >
                    {
                        loading &&
                        <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                    }
                    <View style={[styles.content, { opacity: loading ? 0 : 1 }]}>
                        <View style={styles.boxCollaboratingTeam}>
                            <Text style={styles.txtLable}>{convertLanguage(language, 'collaborating_team2')}</Text>
                            {
                                collaborates.length > 0 ?
                                    collaborates.map((item, index) => {
                                        return <Touchable style={styles.boxInfo} key={index} onPress={() => this.props.navigation.navigate({ name: 'DetailTeam', params: { id: item.Id }, key: item.Id })}>
                                            <View style={styles.boxDataInfo}>
                                                <Image source={{ uri: item.Logos.Small }} style={styles.imgCollaboration} />
                                                <View style={styles.boxContentInfo}>
                                                    <Text style={styles.txtTitle}>{item.Name}</Text>
                                                    <Text style={styles.txtSmall}>({item.Code})</Text>
                                                    <Text style={styles.txtCategory}>
                                                        {
                                                            item.HashTagToEvent
                                                            // item.HashTag &&
                                                            // item.HashTag.map((Tag, index) => {
                                                            //     return <React.Fragment key={index}>
                                                            //         #{Tag.HashTagName + ' '}
                                                            //     </React.Fragment>
                                                            // })
                                                        }
                                                    </Text>
                                                </View>
                                            </View>
                                            <View style={styles.boxButton}>
                                                <Touchable style={styles.btnDismiss} onPress={() => isOnpress ? this.onDismissCollaborate(this.props.route.params['Slug'], item.Id) : null}>
                                                    <Text style={styles.txtDismiss}>{convertLanguage(language, 'dismiss')}</Text>
                                                </Touchable>
                                            </View>
                                        </Touchable>
                                    })
                                    :
                                    <Text style={{ textAlign: 'center', padding: 20 }}>{convertLanguage(language, 'collaborating_team_empty')}</Text>
                            }
                        </View>
                        <View style={styles.boxPendingTeam}>
                            <Text style={styles.txtLable}>{convertLanguage(language, 'pending_request')}</Text>
                            {
                                invitations.length > 0 ?
                                    invitations.map((item, index) => {
                                        return <Touchable style={styles.boxInfo} key={index} onPress={() => this.props.navigation.navigate({ name: 'DetailTeam', params: { id: item.Id }, key: item.Id })}>
                                            <View style={styles.boxDataInfo}>
                                                <Image source={{ uri: item.Logos.Small }} style={styles.imgCollaboration} />
                                                <View style={styles.boxContentInfo}>
                                                    <Text style={styles.txtTitle}>{item.Name}</Text>
                                                    <Text style={styles.txtSmall}>({item.Code})</Text>
                                                    <Text style={styles.txtCategory}>
                                                        {
                                                            item.HashTagToEvent
                                                            // item.HashTag &&
                                                            // item.HashTag.map((Tag, index) => {
                                                            //     return <React.Fragment key={index}>
                                                            //         #{Tag.HashTagName + ' '}
                                                            //     </React.Fragment>
                                                            // })
                                                        }
                                                    </Text>
                                                </View>
                                            </View>
                                            <View style={styles.boxButton}>
                                                <Touchable style={styles.btnConfirm} onPress={() => isOnpress ? this.onConfirmCollaborate(this.props.route.params['Slug'], item.Id) : null}>
                                                    <Text style={styles.txtConfirm}>{convertLanguage(language, 'accept')}</Text>
                                                </Touchable>
                                                <Touchable style={styles.btnDismiss} onPress={() => isOnpress ? this.onDeleteCollaborate(this.props.route.params['Slug'], item.Id) : null}>
                                                    <Text style={styles.txtDismiss}>{convertLanguage(language, 'deny')}</Text>
                                                </Touchable>
                                            </View>
                                        </Touchable>
                                    })
                                    :
                                    <Text style={{ textAlign: 'center', padding: 20 }}>{convertLanguage(language, 'invitations_empty')}</Text>
                            }
                        </View>
                    </View>
                </SrcView>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20
    },
    boxCollaboratingTeam: {
        borderBottomColor: '#e8e8e8',
        borderBottomWidth: 1
    },
    txtLable: {
        fontSize: 14,
        color: '#757575',
        paddingBottom: 15,
        fontWeight: 'bold',
        color: 'black',
    },
    boxInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 20
    },
    boxDataInfo: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    imgCollaboration: {
        width: 60,
        height: 60,
        borderRadius: 30,
        marginRight: 15,
    },
    boxContentInfo: {
        flex: 1
    },
    txtTitle: {
        fontSize: 15,
        color: '#333333',
        paddingBottom: 5,
        fontWeight: 'bold'
    },
    txtSmall: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 5,
    },
    txtCategory: {
        fontSize: 14,
        color: '#333333',
    },
    boxButton: {

    },
    btnDismiss: {
        width: 70,
        height: 27,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#333333',
        borderRadius: 1,
    },
    txtDismiss: {
        fontSize: 14,
        color: '#333333',
    },
    boxPendingTeam: {
        marginTop: 20
    },
    btnConfirm: {
        width: 70,
        height: 27,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#03a9f4',
        borderRadius: 1,
        marginBottom: 5
    },
    txtConfirm: {
        fontSize: 14,
        color: '#03a9f4',
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataCollaboratingTeam: (slug) => {
            dispatch(actLoadDataCollaboratingTeam(slug))
        },
        onConfirmCollaborate: (slug, id) => {
            dispatch(actConfirmCollaborate(slug, id))
        },
        onDismissCollaborate: (slug, id) => {
            dispatch(actDismissCollaborate(slug, id))
        },
        onDeleteCollaborate: (slug, id) => {
            dispatch(actDeleteCollaborate(slug, id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CollaboratingTeam);
// export default EventHostList;
