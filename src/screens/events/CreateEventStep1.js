
import Geolocation from '@react-native-community/geolocation';
import moment from "moment";
import React, { useEffect, useRef, useState } from 'react';
import { PixelRatio, KeyboardAvoidingView, ActivityIndicator, Dimensions, Image, Keyboard, PermissionsAndroid, ScrollView, StyleSheet, Text, TouchableOpacity, View, Alert, Platform } from 'react-native';
import Autocomplete from "react-native-autocomplete-input";
import ImagePicker from 'react-native-image-crop-picker';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import { useDispatch, useSelector } from "react-redux";
import RNFetchBlob from "rn-fetch-blob";
import { useDebouncedCallback } from 'use-debounce';
import { actLoadDataCategory } from '../../actions/category';
import { actSaveEventStep1, actSaveEventDraft, actUpdateStep1, actUpdateEvent2 } from '../../actions/event'
import { actSelectCountry } from '../../actions/city';
import flagPinkImg from '../../assets/pin_1_icon_active.png';
import ModalNewCity from '../../components/ModalNewCity';
import ModalRepeat from '../../components/ModalRepeat';
import ModalTagNew from '../../components/ModalTagNew';
import ModalPreviousLocation from '../../components/ModalPreviousLocation';
import ModalVenue from '../../components/ModalVenue';
import { TextField } from '../../components/react-native-material-textfield';
import * as Colors from '../../constants/Colors';
import * as Config from '../../constants/Config';
import * as Types from '../../constants/ActionType';
import * as Types2 from '../../constants/ActionType2';
import SafeView from '../../screens_view/SafeView';
// import { SafeAreaConsumer } from 'react-native-safe-area-context';
import Touchable from '../../screens_view/Touchable';
import { convertLanguage } from '../../services/Helper';
import Toast from 'react-native-simple-toast';
import EventCreateStep2 from './EventCreateStep2';
import EventCreateStep3 from './EventCreateStep3';
import EventCreateStep4 from './EventCreateStep4';
import Loading from '../../screens_view/Loading';
import ModalImportEvent from '../../components/ModalImportEvent'
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import { set } from 'react-native-reanimated';

const { width, height } = Dimensions.get('window')
const fs = RNFetchBlob.fs;
let imagePath = null;
const ASPECT_RATIO = 2;
const LATITUDE_DELTA = 0.08;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const scale = width / 360
const scaleFontSize = PixelRatio.getFontScale()

const CreateEventStep1 = (props) => {

  const map = useRef(null)
  const refAreaname = useRef(null)
  const scrollView = useRef(null)
  const dispatch = useDispatch()
  const [modal, setModal] = useState(false)
  const [modalCity, setModalCity] = useState(false)
  const [modalVenue, setModalVenue] = useState(false)
  const [modalPreviousLocation, setModalPreviousLocation] = useState(false)
  const [address, setAddress] = useState('')
  const [changeStep, setChangeStep] = useState(1)
  // useEffect(() => {
  //   if (props.route.params['Slug'] !== undefined) {
  //     setChangeStep(4)
  //   }
  // }, [])
  const [step, setStep] = useState(1)
  const [PosterBaseCrop, setPosterBaseCrop] = useState('')
  const [PosterBase, setPosterBase] = useState('')
  const [state, setState] = useState({
    modalRepeat: false,
    dataRepeat: [],
    edit: [],
    modalImportEvent: false
  })
  const [initial, setInitial] = useState({
    title: '',
    category_selected: [],
    isChange: false,
    selected: [],
    listCategory: '',
    city: '',
    Lat: '',
    Long: '',
    AreaId: '',
    AreaName: '',
    isEdit: false,
    showPink: false,
    showButtonMyLocation: true,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
    Unit: '',
    CountryCode: '',
    Address: '',
    loading: false,
    hideResults: true,
    predictions: [],
    loadingLocation: false,
    editAddress: false,
    VenueName: '',
  })

  useEffect(() => {
    dispatch(actLoadDataCategory('event'))
    dispatch(actSelectCountry())
    if (props?.route?.params['Slug'] && props?.route?.params['Slug'] !== undefined) {
      dispatch(actUpdateEvent2(props.route.params['Slug']))
      setChangeStep(4)
    }
    return () => dispatch({ type: Types2.SAVE_EVENT_STEP4_SUCCESS_FULL })
  }, [])

  useEffect(() => {
    if (initial.title === undefined) {
      if (props?.route?.params['Slug'] && props?.route?.params['Slug'] !== undefined) {
        dispatch(actUpdateEvent2(props.route.params['Slug']))
        setChangeStep(4)
      }
    }
  }, [initial.title])

  // useEffect(() => {
  //   dispatch(actLoadDataCategory('event'))
  // }, [])
  // useEffect(() => {
  //   dispatch(actSelectCountry())
  // }, [])
  const showListCategory = () => {
    var data = ''
    initial.selected.forEach(element => {
      data += '#' + element?.name + ' '
    });
    return data;
  }
  const { navigation } = props
  const language = useSelector(state => state.language.language)
  const categories = useSelector(state => state.category.categories)
  const countries = useSelector(state => state.city.countries)
  const event = useSelector(state => state.event)

  useEffect(() => {
    if ((props?.route?.params['Slug'] && props?.route?.params['Slug'] !== undefined)) {
      function fetchCategory() {
        let data = []
        if (event?.event?.HashTag !== undefined) {
          event?.event?.HashTag.forEach((element, index) => {
            data[index] = { value: element.Id, name: element.HashTagName }
          });
        }
        return data
      }
      function fetchListCategory() {
        let list = ''
        fetchCategory().forEach(element => {
          list += '#' + element.name + ' '
        });
        return list
      }

      setInitial({
        ...initial,
        title: event?.event?.Title,
        category_selected: fetchCategory(),
        selected: fetchCategory(),
        listCategory: fetchListCategory(),
        editAddress: true,
        Address: event?.event?.Address,
        AreaId: event?.event?.AreaId,
        AreaName: event?.event?.AreaName,
        CountryCode: event?.event?.CodeCountry,
        Lat: event?.event?.Lat,
        Long: event?.event?.Long,
        Unit: event?.event?.UnitTicket,
        VenueId: event?.event?.VenueId,
        VenueName: event?.event?.VenueName,
        editAddress: false,
        hideResults: false,
        isChange: true,
        isEdit: false,
        loading: false,
        loadingLocation: false,
        showButtonMyLocation: true,
        showPink: true,
      })
      setAddress(event?.event?.Address)
      setData({ ...data, startTime: event?.event?.TimeStart, endTime: event?.event?.TimeFinish })
      // setPosterBaseCrop(event?.event?.Posters?.Large)
      // setPosterBase(event?.event?.Posters?.Full)
      RNFetchBlob.config({
        fileCache: true
      })
        .fetch("GET", event?.event?.Posters?.Full)
        .then(resp => {
          imagePath = resp.path();
          return resp.readFile("base64");
        })
        .then(base64Data => {
          setPosterBase('data:image/png;base64,' + base64Data)
          return fs.unlink(imagePath);
        });
      // 

      RNFetchBlob.config({
        fileCache: true
      })
        .fetch("GET", event?.event?.Posters?.Large)
        .then(resp => {
          imagePath = resp.path();
          return resp.readFile("base64");
        })
        .then(base64Data => {
          setPosterBaseCrop('data:image/png;base64,' + base64Data)
          return fs.unlink(imagePath);
        });
      return;
    }
    if (event.checkImport === true) {
      function fetchCategory() {
        let data = []
        if (event?.eventImport?.HashTag !== undefined) {
          event?.eventImport?.HashTag.forEach((element, index) => {
            data[index] = { value: element.Id, name: element.HashTagName }
          });
        }
        return data
      }
      function fetchListCategory() {
        let list = ''
        fetchCategory().forEach(element => {
          list += '#' + element.name + ' '
        });
        return list
      }
      setInitial({
        ...initial,
        title: event?.eventImport?.Title,
        category_selected: fetchCategory(),
        selected: fetchCategory(),
        listCategory: fetchListCategory(),
        editAddress: true,
        Address: event?.eventImport?.Address,
        AreaId: event?.eventImport?.AreaId,
        AreaName: event?.eventImport?.AreaName,
        CountryCode: event?.eventImport?.CodeCountry,
        Lat: event?.eventImport?.Lat,
        Long: event?.eventImport?.Long,
        Unit: event?.eventImport?.UnitTicket,
        VenueId: event?.eventImport?.VenueId,
        VenueName: event?.eventImport?.VenueName,
        editAddress: false,
        hideResults: false,
        isChange: true,
        isEdit: false,
        loading: false,
        loadingLocation: false,
        showButtonMyLocation: true,
        showPink: true,
      })
      let dataRepeat = getEventImportRepeat(event?.eventImport?.TimeRepeatArray)
      setState({ ...state, dataRepeat: unique([...state.dataRepeat, ...dataRepeat]) })
      setAddress(event?.eventImport?.Address)
      setStep(1)
      setChangeStep(1)
      RNFetchBlob.config({
        fileCache: true
      })
        .fetch("GET", event?.eventImport?.Posters?.Full)
        // the image is now dowloaded to device's storage
        .then(resp => {
          // the image path you can use it directly with Image component
          imagePath = resp.path();
          return resp.readFile("base64");
        })
        .then(base64Data => {
          // here's base64 encoded image
          setPosterBase('data:image/png;base64,' + base64Data)
          // remove the file from storage
          return fs.unlink(imagePath);
        });
      // 

      RNFetchBlob.config({
        fileCache: true
      })
        .fetch("GET", event?.eventImport?.Posters?.Large)
        // the image is now dowloaded to device's storage
        .then(resp => {
          // the image path you can use it directly with Image component
          imagePath = resp.path();
          return resp.readFile("base64");
        })
        .then(base64Data => {
          // here's base64 encoded image
          setPosterBaseCrop('data:image/png;base64,' + base64Data)
          // remove the file from storage
          return fs.unlink(imagePath);
        });
      validateUpdateandImport(event)
    }
  }, [event])
  // console.log('==============================');
  // console.log('event.checkImport]', event.checkImport);
  // console.log('==============================');

  function validateUpdateandImport(event) {
    let PosterBaseCropVl = false
    let PosterBaseVl = false
    let title = false
    let category_selected = false
    let AreaName = false
    let VenueName = false
    let addressVl = false
    let startTime = false
    let endTime = false
    if (event?.eventImport?.Posters?.Large === '') {
      PosterBaseCropVl = true
      PosterBaseVl = true
    }
    if (event?.eventImport?.Title === '') {
      title = true
    }
    if (event?.eventImport?.HashTag.length === 0) {
      category_selected = true
    }
    if (event?.eventImport?.AreaName === '') {
      AreaName = true
    }
    if (event?.eventImport?.VenueName === '') {
      VenueName = true
    }
    if (event?.eventImport?.Address === '') {
      addressVl = true
    }
    if (event?.eventImport?.TimeStart === '') {
      startTime = true
    }
    if (event?.eventImport?.TimeFinish === '') {
      endTime = true
    }
    setErrors({ PosterBaseCropVl, PosterBaseVl, title, category_selected, AreaName, VenueName, addressVl, startTime, endTime })

  }

  function showDataCategory() {
    const result = [];
    categories.forEach(category => {
      result.push({
        name: category.HashTagName,
        value: category.Id
      })
    });
    return result;
  }
  function selectCategory(data) {
    var selected2 = initial.selected
    var index = selected2.some(el => el.value === data.value);
    if (index) {
      selected2 = selected2.filter(el => el.value !== data.value);
    } else {
      if (selected2.length < 5) {
        selected2.push(data)
      }
    }
    setInitial({ ...initial, selected: selected2 })
  }

  function onSelect(value) {
    if (value.Id) {
      var region = {
        latitude: value.Lat,
        longitude: value.Lng,
        latitudeDelta: initial.latitudeDelta,
        longitudeDelta: initial.longitudeDelta
      }
      setTimeout(() => map?.current?.animateToRegion(region), 10);
      loadDataUnit(value.Id, value)


    }
  }

  function loadDataUnit(AreaId, value) {
    fetch(Config.API_URL + '/area/' + AreaId + '/find-country', {
    }).then((response) => {
      return response.json()
    }).then((response) => {
      if (response.status == 200) {
        let nameCity = ''
        switch (language) {
          case 'ko':
            nameCity = value.Name_ko
            break;
          case 'vi':
            nameCity = value.Name_vi
            break;
          case 'en':
            nameCity = value.Name_en
            break;
          default:
            nameCity = value.Name
            break;
        }
        setInitial({
          ...initial, city: value, Lat: value.Lat, Long: value.Lng,
          AreaId: value.Id,
          AreaName: nameCity,
          Unit: response.data.Unit,
          CountryCode: response.data.Code,
          VenueName: ''
        })
        setAddress('')
        dispatch({ type: Types.LOAD_DATA_VENUE_SUCCESS, data: [] });
      }
    })
  }
  function onRegionChange(region) {
    setInitial({
      ...initial,
      Lat: region.latitude,
      Long: region.longitude,
      latitudeDelta: region.latitudeDelta,
      longitudeDelta: region.longitudeDelta,
    })
  }
  function _onMapReady() {
    if (Platform.OS === 'android') {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        .then(granted => {
        });
    }
  }
  async function onSavePin() {
    setInitial({ ...initial, loading: true })
    const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${initial.Lat},${initial.Long}&key=${Config.GOOGLE_API_KEY}&result_type=country&language=${language}`;
    try {
      const result = await fetch(apiUrl);
      const json = await result.json();
      setInitial({ ...initial, loading: false })
      if (json.results.length > 0 && json.results[0].address_components.length > 0 && json.results[0].address_components[0].short_name === initial.CountryCode.toUpperCase()) {
        setInitial({ ...initial, isEdit: false, loading: false })
      } else {
        Alert.alert(
          convertLanguage(language, 'pin_error'),
          convertLanguage(language, 'select_location_fail'),
          [
            {
              text: convertLanguage(language, 'ok'),
              style: "cancel",
              cancelable: true,
            },
          ],
        );
      }
    } catch (err) {
      setInitial({ ...initial, isEdit: false, loading: false })
    }
  }

  async function onSelectLocation(data) {
    Keyboard.dismiss();
    const apiUrl2 = `https://maps.googleapis.com/maps/api/place/details/json?key=${Config.GOOGLE_API_KEY}&placeid=${data.item.place_id}&language=${language}`;
    try {
      const result2 = await fetch(apiUrl2);
      const json2 = await result2.json();
      setInitial({
        ...initial,
        Lat: json2.result.geometry.location.lat,
        Long: json2.result.geometry.location.lng,
        Address: data.item.description,
        hideResults: true,
        showPink: true,
        predictions: [],
        isEdit: false
      })
      var region = {
        Address: data.item.description,
        hideResults: true,
        showPink: true,
        latitude: json2.result.geometry.location.lat,
        longitude: json2.result.geometry.location.lng,
        latitudeDelta: initial.latitudeDelta,
        longitudeDelta: initial.longitudeDelta
      }
      setTimeout(() => map?.current?.animateToRegion(region), 10);
    } catch (err) {
    }
    setAddress(data.item.description)

  }

  async function onChangeDestination(Address) {
    let { Lat, Long } = initial
    const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${Config.GOOGLE_API_KEY}&input=${encodeURI(Address)}&types=address&strictbounds&location=${Lat},${Long}&radius=20000&language=${language}`;
    try {
      const result = await fetch(apiUrl);
      const json = await result.json();
      if (json.predictions && json.predictions.length > 0) {
        setInitial({
          ...initial,
          loadingLocation: false,
          Address: Address,
          hideResults: Address === '' ? true : false,
          editAddress: true,
          predictions: json.predictions
        })
      } else {
        setInitial({
          ...initial,
          loadingLocation: false,
          Address: Address,
          hideResults: Address === '' ? true : false,
          editAddress: true,
          predictions: [1]
        })
      }
    } catch (err) {
      setInitial({
        ...initial,
        loadingLocation: Address === '' ? false : true,
        Address: Address,
        hideResults: Address === '' ? true : false,
        editAddress: true,
        predictions: [1]
      })
    }
  }

  const debounced = useDebouncedCallback(
    (value) => {
      onChangeDestination(value)
    },
    500
  );
  useEffect(() => {
    debounced.callback(address)
  }, [address])
  function onPressToMyLocation() {
    Geolocation.getCurrentPosition((position) => {
      setInitial({
        ...initial,
        Lat: position.coords.latitude,
        Long: position.coords.longitude,
      })
      var region = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        latitudeDelta: initial.latitudeDelta,
        longitudeDelta: initial.longitudeDelta
      }
      setTimeout(() => map?.current?.animateToRegion(region), 10);
    })
  }
  function onPressZoomIn() {
    var region = {
      latitude: parseFloat(initial.Lat),
      longitude: parseFloat(initial.Long),
      latitudeDelta: initial.latitudeDelta / 2,
      longitudeDelta: initial.longitudeDelta / 2
    }
    setInitial({
      ...initial,
      latitudeDelta: initial.latitudeDelta / 2,
      longitudeDelta: initial.longitudeDelta / 2
    })
    setTimeout(() => map?.current?.animateToRegion(region), 10);
  }
  function onPressZoomOut() {
    var region = {
      latitude: parseFloat(initial.Lat),
      longitude: parseFloat(initial.Long),
      latitudeDelta: initial.latitudeDelta * 2,
      longitudeDelta: initial.longitudeDelta * 2
    }
    setInitial({
      ...initial,
      latitudeDelta: initial.latitudeDelta * 2,
      longitudeDelta: initial.longitudeDelta * 2
    })
    setTimeout(() => map?.current?.animateToRegion(region), 10);
  }
  function onEditLocation() {
    dismissKeyboard();
    setInitial({
      ...initial,
      isEdit: true,
      showPink: true, predictions: []
    })
    // scrollView?.current?.scrollToEnd();
  }
  function onFocusAddress() {
    scrollView?.current?.scrollTo({ x: 0, y: 100, animated: true })
  }
  function setVenue(type, venue) {
    if (type === 1) {

    } else {
      setInitial({
        ...initial,
        VenueName: venue,
        VenueId: '',
        editAddress: false
      })
    }
    setTimeout(() => {
      loadDataUnit2(venue.AreaId, venue, type)
    }, 1000);
  }
  function loadDataUnit2(AreaId, venue, type) {
    fetch(Config.API_URL + '/area/' + AreaId + '/find-country', {
    }).then((response) => {
      return response.json()
    }).then((response) => {
      if (response.status == 200) {
        if (type === 1) {
          setInitial({
            ...initial,
            Lat: parseFloat(venue.Lat),
            Long: parseFloat(venue.Long),
            Address: venue.Address,
            VenueName: venue.Name,
            VenueId: venue.Id,
            AreaId: venue.AreaId,
            // AreaName: venue.AreaName,
            editAddress: false,
            showPink: true,
            Unit: response.data.Unit,
            CountryCode: response.data.Code
          })
          setAddress(venue.Address)
          let addressVl = false
          if (venue.Address === '') {
            addressVl = true
          }
          setErrors({ ...errors, addressVl })
          var region = {
            latitude: parseFloat(venue.Lat),
            longitude: parseFloat(venue.Long),
            latitudeDelta: initial.latitudeDelta,
            longitudeDelta: initial.longitudeDelta
          }
          setTimeout(() => map?.current?.animateToRegion(region), 10);
        } else {
          setInitial({
            ...initial,
            Unit: response.data.Unit,
            CountryCode: response.data.Code
          })
        }
      }
    })
  }
  const pickThumb = () => {
    ImagePicker.openPicker({
      mediaType: 'photo',
      includeBase64: true
    }).then(image => {
      ImagePicker.openCropper({
        path: image.path,
        width: image.width,
        height: image.width * 9 / 16,
        includeBase64: true
      }).then(image2 => {
        setInitial({
          ...initial,
          isChange: true,
        })
        setPosterBaseCrop('data:image/png;base64,' + image2.data);
        setPosterBase('data:image/png;base64,' + image.data);
        setErrors({ ...errors, setPosterBaseCropVl: false, setPosterBaseVl: false })
      });
    }).catch(e => { });
  }

  const [data, setData] = useState({
    startTime: moment().startOf('day').format('YYYY-MM-DD HH:mm'),
    endTime: moment().startOf('day').add(1, 'minutes').format('YYYY-MM-DD HH:mm')
  })
  const [isDatePickerVisible, setDatePickerVisibility] = useState({
    start: false,
    end: false
  });

  const showDatePickerStart = () => {
    setDatePickerVisibility({ ...isDatePickerVisible, start: true });
  };

  const hideDatePickerStart = () => {
    setDatePickerVisibility({ ...isDatePickerVisible, start: false });
  };
  const handleConfirmStart = (date) => {
    setErrors({ ...errors, startTime: false })
    if (data.startTime !== '' && moment(date).format('YYYY-MM-DD HH:mm') !== moment(data.startTime).format('YYYY-MM-DD HH:mm')) {
      setState({ ...state, dataRepeat: [] })
    }
    if (data.endTime !== '' && moment(data.endTime).format('YYYY-MM-DD HH:mm') <= moment(date).format('YYYY-MM-DD HH:mm')) {
      console.log('1');
      setData({ ...data, startTime: moment(date).format('YYYY-MM-DD HH:mm'), endTime: moment(date).add(1, 'minutes').format('YYYY-MM-DD HH:mm') })
    } else {
      console.log('2');
      setData({ ...data, startTime: moment(date).format('YYYY-MM-DD HH:mm') })

    }
    hideDatePickerStart();
  };
  const showDatePickerEnd = () => {
    setDatePickerVisibility({ ...isDatePickerVisible, end: true });
  };

  const hideDatePickerEnd = () => {
    setDatePickerVisibility({ ...isDatePickerVisible, end: false });
  };

  const handleConfirmEnd = (date) => {
    setErrors({ ...errors, endTime: false })
    setState({ ...state, dataRepeat: [] })
    setData({ ...data, endTime: moment(date).format('YYYY-MM-DD HH:mm') })
    hideDatePickerEnd();
  };
  const onRemoveRepeat = (valueToRemove) => {
    setState({ ...state, dataRepeat: [...state.dataRepeat].filter(item => item !== valueToRemove) })
  }
  const onEditRepeat = (index, item) => {
    setState({ ...state, modalRepeat: true, edit: [index, item] })
  }
  const changeDataRepeat = (data, indexItem) => {
    let dataFetch = [...state.dataRepeat]
    dataFetch[indexItem] = data
    if (indexItem !== undefined) { //Edit
      if ([...state.dataRepeat].find(item => item === data) === undefined || [...state.dataRepeat].length === 0) {
        setState({ ...state, modalRepeat: false, dataRepeat: [...dataFetch] })
      } else {
        Toast.showWithGravity(JSON.stringify(convertLanguage(language, 'already_exited_day')), Toast.SHORT, Toast.TOP)
      }
    } else { //Tao moi
      if ([...state.dataRepeat].find(item => item === data) === undefined || [...state.dataRepeat].length === 0) {
        setState({ ...state, modalRepeat: false, dataRepeat: [...dataFetch, data] })
      } else {
        Toast.showWithGravity(JSON.stringify(convertLanguage(language, 'already_exited_day')), Toast.SHORT, Toast.TOP)
      }
    }
  }

  function getDateRepeat() {
    var dateString = '';
    state.dataRepeat.forEach((item, index) => {
      dateString = index + 1 === state.dataRepeat.length ? dateString + item.slice(0, 10) : dateString + item.slice(0, 10) + ', ';
    });
    return dateString;
  }
  function getDataTag() {
    var result = [];
    initial.category_selected.forEach((category) => {
      result.push(category.value)
    })
    return result
  }
  const [IdEventDtaft, setIdEventDtaft] = useState('no')
  const [loadingDraft, setIdLoadingDraft] = useState(false)
  function onUpdate(type = '') {
    var Tags = [];
    initial.category_selected.forEach(category => {
      Tags.push(category.value);
    });
    var TagString = JSON.stringify(Tags);
    var body = {
      PosterBaseCrop,
      PosterBase,
      Title: initial.title,
      TimeStart: data.startTime,
      TimeFinish: data.endTime,
      Lat: initial.Lat,
      Long: initial.Long,
      Address: initial.Address,
      VenueName: initial.VenueName,
      VenueId: initial.VenueId,
      AreaId: initial.AreaId,
      AreaName: initial.AreaName,
      TimeRepeat: getDateRepeat(),
      Tag: getDataTag(),
      TagString
    }

    dispatch(actUpdateStep1(props.route.params['Slug'], body))
      .then((res) => {
        if (res && res.status === 200) {
          dispatch(actUpdateEvent2(props.route.params['Slug']))
          setChangeStep(4)
        }
      })

  }
  function onRegister(type = '') {
    var Tags = [];
    initial.category_selected.forEach(category => {
      Tags.push(category.value);
    });
    var TagString = JSON.stringify(Tags);
    var body = {
      PosterBaseCrop,
      PosterBase,
      Title: initial.title,
      TimeStart: data.startTime,
      TimeFinish: data.endTime,
      Lat: initial.Lat,
      Long: initial.Long,
      Address: initial.Address,
      VenueName: initial.VenueName,
      VenueId: initial.VenueId,
      AreaId: initial.AreaId,
      AreaName: initial.AreaName,
      TimeRepeat: getDateRepeat(),
      Tag: getDataTag(),
      TagString
    }

    if (type === 'draft') {
      setIdLoadingDraft(true)
      dispatch(actSaveEventDraft(props.route.params['HostInfoId'], body, IdEventDtaft, 'no', language))
        .then((res) => {
          if (res && res.status === 200) {
            setIdEventDtaft(res.Id);
            setIdLoadingDraft(false)
            setInitial({ ...initial, body })
          }
        })
      setTimeout(() => {
        setIdLoadingDraft(false)
      }, 5000);
    } else {
      dispatch(actSaveEventStep1(props.route.params['HostInfoId'], body))
        .then((res) => {
          if (res && res.status === 200) {
            setChangeStep(2)
            setStep(2)
          }
        })
    }
  }

  function setLocation(location) {
    loadDataUnitLocation(location.AreaId, location)
  }
  function loadDataUnitLocation(AreaId, location) {
    fetch(Config.API_URL + '/area/' + AreaId + '/find-country', {
    }).then((response) => {
      return response.json()
    }).then((response) => {
      if (response.status == 200) {
        setInitial({
          ...initial,
          Lat: location.Lat,
          Long: location.Long,
          Address: location.Address,
          VenueName: location.Name,
          AreaId: location.AreaId,
          AreaName: location.AreaName,
          editAddress: false,
          showPink: true,
          Unit: response.data.Unit, CountryCode: response.data.Code,
        })
        setAddress(location.Address)
        let AreaName = false
        let VenueName = false
        let addressVl = false
        if (location.AreaName === '') {
          AreaName = true
        }
        if (location.Name === '') {
          VenueName = true
        }
        if (location.Address === '') {
          addressVl = true
        }
        setErrors({ ...errors, AreaName, VenueName, addressVl })
      }
    })
  }

  const goBackAction = () => {
    Alert.alert(
      convertLanguage(language, 'goBackConfirm'),
      "",
      [
        {
          text: convertLanguage(language, 'cancel'),
          style: "destructive"
        },
        {
          text: convertLanguage(language, 'ok'), onPress: () => {
            dispatch({ type: Types2.SAVE_EVENT_STEP4_SUCCESS_FULL }),
              dispatch({ type: Types.SAVE_EVENT_SUCCESS, IdEventDtaft: 'no' }),
              props?.route?.params['Slug'] && props?.route?.params['Slug'] !== undefined && changeStep !== 4 ? setChangeStep(4) : navigation.goBack()
          }
        }
      ],
      { cancelable: false })
  }
  function unique(arr) {
    var newArr = []
    for (var i = 0; i < arr.length; i++) {
      if (!newArr.includes(arr[i])) {
        newArr.push(arr[i])
      }
    }
    return newArr
  }
  const getEventImportRepeat = (data) => {
    if (data === undefined)
      return []
    let local = []
    data.forEach(element => {
      local.push(element.Start + ' ~ ' + element.End)
    });
    return local
  }

  const setIdEventDtaftCallBack = (idEvent) => {
    setIdEventDtaft(idEvent)
  }
  const [errors, setErrors] = useState({
    PosterBaseCropVl: false,
    PosterBaseVl: false,
    title: false,
    category_selected: false,
    AreaName: false,
    VenueName: false,
    addressVl: false,
    startTime: false,
    endTime: false,
  })
  const onValidation = (type = '') => {
    let PosterBaseCropVl = false
    let PosterBaseVl = false
    let title = false
    let category_selected = false
    let AreaName = false
    let VenueName = false
    let addressVl = false
    let startTime = false
    let endTime = false
    if (type === '') {
      if (PosterBaseCrop === '' || PosterBase === '') {
        PosterBaseCropVl = true
        PosterBaseVl = true
      }
      if (initial.title === '') {
        title = true
      }
      if (initial.category_selected.length === 0) {
        category_selected = true
      }
      if (initial.AreaName === '') {
        AreaName = true
      }
      if (initial.VenueName === '') {
        VenueName = true
      }
      if (address === '') {
        addressVl = true
      }
      if (data.startTime === '') {
        startTime = true
      }
      if (data.endTime === '') {
        endTime = true
      }
    } else {
      if (initial.title === '') {
        title = true
      }
      if (initial.AreaName === '') {
        AreaName = true
      }
      if (initial.VenueName === '') {
        VenueName = true
      }
      if (address === '') {
        addressVl = true
      }
      if (data.startTime === '') {
        startTime = true
      }
      if (data.endTime === '') {
        endTime = true
      }
    }
    setErrors({ PosterBaseCropVl, PosterBaseVl, title, category_selected, AreaName, VenueName, addressVl, startTime, endTime })
    // checkScroll()
    if (props?.route?.params['Slug'] && props?.route?.params['Slug'] !== undefined) {
      if (!PosterBaseCropVl && !PosterBaseVl && !title && !category_selected && !AreaName && !VenueName && !addressVl && !startTime && !endTime) {
        onUpdate()
      }
    } else {
      if (type === '') {
        if (!PosterBaseCropVl && !PosterBaseVl && !title && !category_selected && !AreaName && !VenueName && !addressVl && !startTime && !endTime) {
          onRegister()
        }
      } else {
        if (!title && !AreaName && !VenueName && !addressVl && !startTime && !endTime) {
          onRegister('draft')
        }
      }
    }
  }
  useEffect(() => {
    if (errors.PosterBaseVl || errors.PosterBaseCropVl) {
      scrollView?.current?.scrollTo({ x: 0, y: 0 * scale, animated: true })
      return
    }
    if (errors.title || errors.category_selected) {
      scrollView?.current?.scrollTo({ x: 0, y: 200 * scale, animated: true })
      return
    }
    if (errors.AreaName || errors.VenueName || errors.addressVl) {
      scrollView?.current?.scrollTo({ x: 0, y: 400 * scale, animated: true })
      return
    }
    if (errors.startTime || errors.endTime) {
      scrollView?.current?.scrollTo({ x: 0, y: 992 * scale, animated: true })
      return
    }
  }, [errors])

  // hàm so sánh thời gian kết thhúc của sự kiện có nhỏ hơn thời gian kết thúc của vé không
  // const TimeFnEventFollowTimeFnTicket = () => {
  //   const timeTickets = ["2021-01-10 10:16", "2021-01-10 10:16", "2021-01-11 10:16", "2021-01-12 10:16"];
  //   // moment(timeEvent).format('YYYY-MM-DD HH:mm') > moment(timeticket).format('YYYY-MM-DD HH:mm')
  //   const timeEvent = event.event.TimeFinish
  // }


  // console.log(
  //   'daa', new Date(moment(data.startTime).add(1, 'minutes').format('YYYY-MM-DD HH:mm'))
  // );
  // console.log('2sds', moment(data.startTime).add(1, 'minutes').format('YYYY-MM-DD'));

  console.log('event.loadingStep1', event.loadingStep1);
  if (event.loadingStep1 && props?.route?.params['Slug'] && props?.route?.params['Slug'] !== undefined) {
    return (
      <Loading />
    )
  } else {
    return (
      <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
        {
          ((event.loadingStep1 || loadingDraft) && props?.route?.params['Slug'] === undefined) &&
          <Loading />
        }
        <View style={styles.container}>
          <View style={styles.header}>
            <Touchable
              onPress={() => goBackAction()}
              style={styles.touchAction}>
              <View style={styles.headerLeft}>
                <Image source={require('../../assets/icon_back.png')} style={styles.imgBack} resizeMode='cover' />
              </View>
            </Touchable>
            {
              (props?.route?.params['Slug'] && props?.route?.params['Slug'] !== undefined) ? null :
                <Touchable
                  onPress={() => setState({ ...state, modalImportEvent: !state.modalImportEvent })}
                  style={styles.touchAction}>
                  <Text style={styles.headerRight}>{convertLanguage(language, 'import')}</Text>
                </Touchable>
            }
          </View>
          {
            props?.route?.params['Slug'] && props?.route?.params['Slug'] !== undefined ? null :
              <View style={styles.stepContainer}>
                <Touchable style={changeStep === 1 ? styles.actionStepActive : styles.actionStep} onPress={() => setChangeStep(1)}>
                  <View style={{ alignItems: 'center' }}>
                    <Image source={require('../../assets/basic_info.png')} style={styles.imgAction} resizeMode='cover' />
                    {changeStep === 1 && <Text numberOfLines={1} style={styles.txtAction}>{convertLanguage(language, 'basic_info')}</Text>}
                  </View>
                  <View style={styles.txtBoxinfo} >
                    <Image source={require('../../assets/right.png')} style={styles.imgChevRight} resizeMode='cover' />
                  </View>
                </Touchable>
                <Touchable style={changeStep === 2 ? styles.actionStepActive : styles.actionStep} onPress={() => step > 1 ? setChangeStep(2) : null}>
                  <View style={{ alignItems: 'center', }}>
                    <Image source={require('../../assets/detail_info.png')} style={[styles.imgAction, { tintColor: step > 1 ? '#4F4F4F' : null }]} resizeMode='cover' />
                    {changeStep === 2 && <Text numberOfLines={1} style={styles.txtAction}>{convertLanguage(language, 'detail_info_by_step')}</Text>}
                  </View>
                  <View style={styles.txtBoxinfo} >
                    <Image source={require('../../assets/right.png')} style={styles.imgChevRight} resizeMode='cover' />
                  </View>
                </Touchable>

                <Touchable style={changeStep === 3 ? styles.actionStepActive : styles.actionStep} onPress={() => step > 2 ? setChangeStep(3) : null}>
                  <View style={{ alignItems: 'center', }}>
                    <Image source={require('../../assets/tickets_big.png')} style={[styles.imgAction, { tintColor: step > 2 ? '#4F4F4F' : null }]} resizeMode='cover' />
                    {changeStep === 3 && <Text style={styles.txtAction}>{convertLanguage(language, 'ticket')}</Text>}
                  </View>
                  <View style={styles.txtBoxinfo} >
                    <Image source={require('../../assets/right.png')} style={styles.imgChevRight} resizeMode='cover' />
                  </View>
                </Touchable>
                <Touchable style={changeStep === 4 ? styles.actionStepActive : styles.actionStep} onPress={() => step === 4 ? setChangeStep(4) : null}>
                  <View style={{ alignItems: 'center', }}>
                    <Image source={require('../../assets/publish.png')} style={[styles.imgAction, { tintColor: step === 4 ? '#4F4F4F' : null }]} resizeMode='cover' />
                    {changeStep === 4 && <Text numberOfLines={1} style={styles.txtAction}>{convertLanguage(language, 'overview')}</Text>}
                  </View>
                </Touchable>
              </View>

          }
          {
            changeStep === 1 &&
            <>
              <KeyboardAvoidingView keyboardVerticalOffset={20 * scale} behavior={Platform.OS == "ios" ? "padding" : "height"} style={{ flexGrow: 1 }}>
                {/* onScroll={() => Platform.OS === 'ios' ? Keyboard.dismiss() : null} */}
                <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled' ref={scrollView} style={{ flex: 1 }}>
                  {/* <KeyboardAwareScrollView> */}

                  <View style={{ paddingHorizontal: 16 * scale }}>

                    <View style={styles.boxImageThumb}>
                      {
                        PosterBaseCrop !== '' ?
                          <>
                            <Touchable onPress={() => pickThumb()} >
                              <Image source={{ uri: PosterBaseCrop }} style={styles.imgThumb} resizeMode='cover' />
                              <View style={{
                                position: 'absolute', bottom: 8 * scale, right: 8 * scale,
                                flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
                                backgroundColor: 'rgba(0, 0, 0, 0.5)',
                                borderRadius: 32,
                                // width: 130 * scale,
                                height: 34 * scale,
                                paddingHorizontal: 6 * scale
                              }}>
                                <Image source={require('../../assets/editPoster.png')} style={{ marginLeft: 3.8 * scale, marginRight: 9.5 * scale, width: 15 * scale, height: 13.5 * scale, resizeMode: 'cover' }} />
                                <Text style={{
                                  color: '#FFFFFF',
                                  fontStyle: 'normal',
                                  fontWeight: 'normal',
                                  fontSize: 12,
                                  lineHeight: 15,
                                }}>{convertLanguage(language, 'edit_poster')}</Text>
                              </View>
                            </Touchable>

                          </>
                          :
                          <>
                            <View></View>
                            <Touchable style={[styles.boxAddImage, { borderColor: errors.PosterBaseCropVl || errors.PosterBaseVl ? '#EB5757' : Colors.PRIMARY, }]} onPress={() => pickThumb()}>
                              <Image source={require('../../assets/photo_add.png')} style={[styles.imgAddThumb, { tintColor: errors.PosterBaseCropVl || errors.PosterBaseVl ? '#EB5757' : Colors.PRIMARY }]} resizeMode='contain' />
                              <Text style={[styles.txtAddImage, { color: errors.PosterBaseCropVl || errors.PosterBaseVl ? '#EB5757' : Colors.PRIMARY, }]}>{convertLanguage(language, 'add_event_poster')}</Text>
                            </Touchable>
                          </>
                      }
                    </View>
                    <View style={{}}>
                      <Text style={styles.titleContex}>{convertLanguage(language, 'title_category')}</Text>
                      <TextField
                        autoCorrect={false}
                        disabled={event?.event?.is_Finish}
                        enablesReturnKeyAutomatically={true}
                        value={initial.title}
                        onChangeText={(value) => { setInitial({ ...initial, title: value }), initial.title !== '' ? setErrors({ ...errors, title: false }) : '' }}
                        error={errors.title}
                        returnKeyType='next'
                        label={convertLanguage(language, 'title') + ' *'}
                        baseColor={'#828282'}
                        tintColor={'#828282'}
                        errorColor={'#EB5757'}
                        inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.title ? '#EB5757' : '#BDBDBD' }]}
                        containerStyle={styles.containerStyle}
                        labelHeight={20}
                        labelTextStyle={{ paddingBottom: 15, paddingLeft: 12 }}
                        lineWidth={2}
                        selectionColor={'#3a3a3a'}
                        style={styles.input}
                        errorImage={require('../../assets/error.png')}
                      />
                      <Touchable style={{}}
                        onPress={() => setModal(!modal)}
                        style={{}}
                        disabled={event?.event?.is_Finish}
                      >
                        <TextField
                          autoCorrect={false}
                          enablesReturnKeyAutomatically={true}
                          value={initial.listCategory}
                          error={errors.category_selected}
                          returnKeyType='next'
                          label={convertLanguage(language, 'category') + ' *'}
                          baseColor={'#828282'}
                          tintColor={'#828282'}
                          errorColor={'#EB5757'}
                          inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.category_selected ? '#EB5757' : '#BDBDBD' }]}
                          containerStyle={styles.containerStyle}
                          labelHeight={20}
                          labelTextStyle={{ paddingBottom: 15, paddingLeft: 12 }}
                          lineWidth={2}
                          selectionColor={'#3a3a3a'}
                          style={styles.input}
                          editable={false}
                        // errorImage={require('../../assets/error.png')}
                        />
                        <Image source={require('../../assets/ic_category_blue.png')} resizeMode='contain' style={{ width: 18 * scale, height: 18 * scale, tintColor: errors.category_selected ? '#EB5757' : '#4F4F4F', position: 'absolute', right: 8 * scale, top: '35%' }} />

                      </Touchable>

                    </View>
                    <View style={{}}>
                      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.titleContex}>{convertLanguage(language, 'location')}</Text>
                        <Touchable
                          onPress={() => setModalPreviousLocation(!modalPreviousLocation)}
                          disabled={event?.event?.is_Finish}
                          style={{}}>
                          <Text style={styles.headerRight}>{convertLanguage(language, 'previous_location')}</Text>
                        </Touchable>
                      </View>
                      <Touchable style={{}}
                        onPress={() => setModalCity(!modalCity)}
                        disabled={event?.event?.is_Finish}
                        style={{}}
                      >
                        <TextField
                          autoCorrect={false}
                          enablesReturnKeyAutomatically={true}
                          value={initial.AreaName}
                          error={errors.AreaName}
                          returnKeyType='next'
                          label={convertLanguage(language, 'city') + ' *'}
                          baseColor={'#828282'}
                          tintColor={'#828282'}
                          errorColor={'#EB5757'}
                          inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.AreaName ? '#EB5757' : '#BDBDBD' }]}
                          containerStyle={styles.containerStyle}
                          labelHeight={20}
                          labelTextStyle={{ paddingBottom: 15, paddingLeft: 12 }}
                          lineWidth={2}
                          selectionColor={'#3a3a3a'}
                          style={styles.input}
                          editable={false}
                          errorImage={require('../../assets/error.png')}
                        />

                      </Touchable>
                      <Touchable style={{}}
                        onPress={() => initial.AreaName !== '' ? setModalVenue(!modalVenue) : setErrors({ ...errors, AreaName: true })}
                        disabled={(initial.AreaId !== '' && event?.event?.is_Finish === false || event?.event?.is_Finish === undefined) ? false : true}
                      >
                        <TextField
                          autoCorrect={false}
                          enablesReturnKeyAutomatically={true}
                          value={initial.VenueName}
                          error={errors.VenueName}
                          returnKeyType='next'
                          label={convertLanguage(language, 'venue_name') + ' *'}
                          baseColor={'#828282'}
                          tintColor={'#828282'}
                          errorColor={'#EB5757'}
                          inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.VenueName ? '#EB5757' : '#BDBDBD' }]}
                          containerStyle={styles.containerStyle}
                          labelHeight={20}
                          labelTextStyle={{ paddingBottom: 15, paddingLeft: 12 }}
                          lineWidth={2}
                          selectionColor={'#3a3a3a'}
                          style={styles.input}
                          editable={false}
                          errorImage={require('../../assets/error.png')}
                        />

                      </Touchable>


                    </View>
                    {/* <ScrollView style={{ flex: 1 }} ref={scrollView} keyboardShouldPersistTaps='handled'> */}
                    <View style={styles.content}>
                      <Touchable onPress={() => setErrors({ ...errors, AreaName: true })} disabled={initial.AreaName !== '' ? true : false}>
                        <TextField
                          autoCorrect={false}
                          enablesReturnKeyAutomatically={true}
                          disabled={(initial.AreaId !== '' && initial.AreaName !== '' && (event?.event?.is_Finish === false || event?.event?.is_Finish === undefined)) ? false : true}
                          value={address}
                          ref={refAreaname}
                          onChangeText={Address => (setAddress(Address), setErrors({ ...errors, addressVl: false }))}
                          error={errors.addressVl}
                          returnKeyType='next'
                          label={convertLanguage(language, 'address') + ' *'}
                          baseColor={'#828282'}
                          tintColor={'#828282'}
                          errorColor={'#EB5757'}
                          inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.addressVl ? '#EB5757' : '#BDBDBD' }]}
                          containerStyle={styles.containerStyle}
                          labelHeight={20}
                          labelTextStyle={{ paddingBottom: 15, paddingLeft: 12 }}
                          lineWidth={2}
                          selectionColor={'#3a3a3a'}
                          style={styles.input}
                          errorImage={require('../../assets/error.png')}
                        />
                      </Touchable>
                      <View style={styles.autocompleteContainer}>
                        <Autocomplete
                          setKeyboard={() => onFocusAddress()}
                          autoCapitalize="none"
                          editable={initial.AreaName != ''}
                          autoCorrect={false}
                          hideResults={initial.hideResults}
                          containerStyle={{ marginTop: 4, padding: 0 }}
                          inputContainerStyle={{
                            // marginBottom: Platform.OS === 'ios' ? -8 * scale : -17 * scale,
                            borderColor: 'white'
                            // position: "absolute", bottom: -50 * scale
                            // marginBottom: 20 * scale
                          }}
                          listStyle={{ maxHeight: 500 * scale }}
                          style={[styles.txtContent, { padding: 0 }]}
                          data={initial.predictions}
                          underlineColorAndroid='transparent'
                          onFocus={() => setInitial({ ...initial, focus: 'suggest' })}
                          renderTextInput={() => { <View style={{}}></View> }}
                          flatListProps={{
                            keyExtractor: (_, idx) => idx,
                            renderItem: (data) => {
                              return (
                                <>
                                  {data.item !== 1 &&
                                    <Touchable onPress={() => onSelectLocation(data)} style={styles.btnSelect}>
                                      <Text>{data.item.description}</Text>
                                    </Touchable>
                                  }
                                  {
                                    data.index === initial.predictions.length - 1 &&
                                    <Touchable style={styles.btnSelect} onPress={onEditLocation}>
                                      <Text style={{ textAlign: 'center', color: Colors.PRIMARY }}>{convertLanguage(language, 'set_custom_address')}</Text>
                                    </Touchable>
                                  }
                                </>
                              )
                            }
                          }}
                        // renderItem={data => (
                        //   <Touchable onPress={() => onSelectLocation(data)} style={styles.btnSelect}>
                        //     <Text>{data.item.description}</Text>
                        //   </Touchable>
                        // )}
                        />
                      </View>
                      {
                        initial.loadingLocation &&
                        <ActivityIndicator size="large" color="#000000" />
                      }
                      {/* {
                        (!initial.loadingLocation && initial.Address != '' && address != '' && initial.predictions.length === 0 && initial.editAddress) &&
                        <Text style={{ fontSize: 12 * scale, color: '#333333', textAlign: 'center', paddingLeft: 15 * scale, paddingRight: 15 * scale, marginBottom: 10 }}>{convertLanguage(language, 'search_location')}</Text>
                      } */}
                      {/* {
                    (!loadingLocation && Address != '' && predictions.length === 0 && editAddress) &&
                    <Text style={{ fontSize: 16, color: '#333333', textAlign: 'center', paddingLeft: 15, paddingRight: 15 }}>{convertLanguage(language, 'search_location')}</Text>
                } */}
                      {
                        initial.Lat && initial.Long ?
                          <View style={styles.boxEditMap}>
                            <Text style={styles.txtEditMap}>{convertLanguage(language, 'please_check_the_pin_location_is_correct_on_map')}</Text>
                            <Touchable disabled={(event?.event?.is_Finish === false || event?.event?.is_Finish === undefined) ? false : true} style={styles.btnEdit} onPress={onEditLocation}>
                              <Text style={styles.txtEdit} numberOfLines={1}>{convertLanguage(language, 'edit_map_pin')}</Text>
                            </Touchable>
                          </View>
                          :
                          null
                      }
                      <View style={[{ flex: 1, borderRadius: 6, height: 250 * scale, marginTop: 20 * scale, backgroundColor: '#E7E7E7', position: 'relative' }, initial.isEdit ? { zIndex: 0 } : {}]}>
                        {
                          initial.Lat && initial.Long ?
                            <>
                              <MapView
                                provider={PROVIDER_GOOGLE}
                                initialRegion={{
                                  latitude: parseFloat(initial.Lat),
                                  longitude: parseFloat(initial.Long),
                                  latitudeDelta: initial.latitudeDelta,
                                  longitudeDelta: initial.longitudeDelta,
                                }}
                                style={{ height: 250 * scale, borderRadius: 6, flex: 1 }}
                                onRegionChangeComplete={(region) => onRegionChange(region)}
                                zoomEnabled={initial.isEdit}
                                pitchEnabled={initial.isEdit}
                                rotateEnabled={false}
                                scrollEnabled={initial.isEdit}

                                {...{ [!initial.isEdit ? 'pointerEvents' : null]: "none" }}
                                ref={map}
                                onMapReady={_onMapReady}
                              />
                              {
                                initial.showPink &&
                                <View style={styles.markerFixed}>
                                  <Image style={styles.marker} source={flagPinkImg} />
                                </View>
                              }

                              {
                                initial.isEdit &&
                                <Touchable style={styles.boxPinLocation} onPress={() => onSavePin()} disabled={initial.loading}>
                                  <Text style={styles.txtPinLocation}>{convertLanguage(language, 'save_pin_location')}</Text>
                                  {
                                    initial.loading &&
                                    <ActivityIndicator size='small' color='#FFFFFF' style={{ marginLeft: 5 * scale }} />
                                  }
                                </Touchable>
                              }
                              {
                                initial.isEdit &&
                                <View style={styles.boxAction}>
                                  {
                                    initial.showButtonMyLocation &&
                                    <Touchable style={styles.btnMyLocation} onPress={() => onPressToMyLocation()}>
                                      <Image source={require('../../assets/my_location.png')} style={styles.icMyLocation} />
                                    </Touchable>
                                  }
                                  <Touchable style={styles.btnPlus} onPress={() => onPressZoomIn()}>
                                    <Image source={require('../../assets/plus.png')} style={styles.icPlus} />
                                  </Touchable>
                                  <Touchable style={styles.btnMinus} onPress={() => onPressZoomOut()}>
                                    <Image source={require('../../assets/minus.png')} style={styles.icMinus} />
                                  </Touchable>
                                </View>
                              }
                            </>
                            :
                            null
                        }
                      </View>
                    </View>
                    {/* </ScrollView> */}

                    <View style={{ marginTop: 20 * scale }}>
                      <Text style={styles.titleContex}>{convertLanguage(language, 'date_and_time')}</Text>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1 / 2 }}>
                          <View style={{ flex: 1 / 2, marginRight: 10 * scale }}>
                            <TouchableOpacity disabled={(event?.event?.is_Finish === false || event?.event?.is_Finish === undefined) ? false : true} onPress={() => showDatePickerStart()} style={{ flex: 3 / 4 }}>
                              <TextField
                                autoCorrect={false}
                                enablesReturnKeyAutomatically={true}
                                value={data.startTime}
                                // onChangeText={() => setErrors({ ...errors, startTime: false })}
                                error={errors.startTime}
                                editable={false}
                                returnKeyType='next'
                                label={convertLanguage(language, 'start_date') + ' *'}
                                baseColor={'#828282'}
                                tintColor={'#828282'}
                                errorColor={'#EB5757'}
                                inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.startTime ? '#EB5757' : '#BDBDBD' }]}
                                containerStyle={[styles.containerStyle]}
                                // labelHeight={20}
                                labelTextStyle={{ marginLeft: 12 }}
                                affixTextStyle={{ backgroundColor: 'blue' }}
                                checkLeft={{ marginLeft: -2 }}
                                // labelOffset={{ x0: 0, y0: -10, x1: 0, y1: -2 }}
                                // contentInset={{ top: 0, left: 0, right: 0, label: 4, input: 8, }}
                                // lineWidth={}
                                selectionColor={'#3a3a3a'}
                              // fontSize={10 * scale / scaleFontSize}
                              // labelFontSize={12 * scale / scaleFontSize}

                              // style={{ flex: 1 }}
                              // errorImage={require('../../assets/error.png')}
                              />
                              <Image source={require('../../assets/Cal.png')} style={{
                                width: 15 * scale,
                                height: 15 * scale,
                                position: 'absolute',
                                top: -2,
                                right: 0,
                                marginTop: 25 * scale,
                                marginRight: 7 * scale,
                                flex: 1 / 4,
                                resizeMode: 'contain',
                                tintColor: errors.startTime ? '#EB5757' : null
                              }} />


                            </TouchableOpacity>
                            <DateTimePickerModal
                              isVisible={isDatePickerVisible.start}
                              mode="datetime"
                              cancelTextIOS={convertLanguage(language, 'cancel')}
                              confirmTextIOS={convertLanguage(language, 'confirm')}
                              locale={language}
                              format="YYYY-MM-DD HH:mm"
                              onConfirm={handleConfirmStart}
                              onCancel={hideDatePickerStart}
                              headerTextIOS={convertLanguage(language, 'pick_a_date')}

                            />
                          </View>
                        </View>
                        <View style={{ flex: 1 / 2 }}>
                          <View style={{ flex: 1 / 2, marginLeft: 10 * scale }}>
                            <TouchableOpacity onPress={() => showDatePickerEnd()} disabled={data.startTime !== '' && (event?.event?.is_Finish === false || event?.event?.is_Finish === undefined) ? false : true}>
                              <TextField
                                autoCorrect={false}
                                enablesReturnKeyAutomatically={true}
                                value={data.endTime}
                                // onChangeText={(companyName) => setData({ ...data, companyName })}
                                error={errors.endTime}
                                editable={false}
                                returnKeyType='next'
                                label={convertLanguage(language, 'finish_date') + ' *'}
                                baseColor={'#828282'}
                                tintColor={'#828282'}
                                errorColor={'#EB5757'}
                                inputContainerStyle={[styles.inputContainerStyle, { borderColor: data.startTime !== '' ? (errors.endTime ? '#EB5757' : '#BDBDBD') : '#E0E2E8', backgroundColor: data.startTime !== '' ? 'white' : '#E0E2E8' }]}
                                containerStyle={styles.containerStyle}
                                // labelHeight={20}
                                labelTextStyle={{ paddingBottom: 15, paddingLeft: 12 }}
                                lineWidth={2}
                                checkLeft={{ marginLeft: -2 }}
                                selectionColor={'#3a3a3a'}
                                style={styles.input}
                              // fontSize={14 * scale / scaleFontSize}
                              // labelFontSize={12 * scale / scaleFontSize}

                              // errorImage={require('../../assets/error.png')}
                              />
                              <Image source={require('../../assets/Cal.png')} style={{
                                width: 15 * scale,
                                height: 15 * scale,
                                // marginTop: 15,
                                position: 'absolute',
                                top: -2,
                                right: 0,
                                marginTop: 25 * scale,
                                marginRight: 7 * scale,
                                resizeMode: 'contain',
                                tintColor: errors.endTime ? '#EB5757' : null
                              }} />
                            </TouchableOpacity>
                            <DateTimePickerModal
                              isVisible={isDatePickerVisible.end}
                              mode="datetime"
                              cancelTextIOS={convertLanguage(language, 'cancel')}
                              confirmTextIOS={convertLanguage(language, 'confirm')}
                              locale={language}
                              format="YYYY-MM-DD HH:mm"
                              onConfirm={(data1) => moment(data1).format('YYYY-MM-DD HH:mm') > moment(data.startTime).add(1, 'minutes').format('YYYY-MM-DD HH:mm') ? handleConfirmEnd(moment(data1).format('YYYY-MM-DD HH:mm')) : handleConfirmEnd(moment(data.startTime).add(1, 'minutes').format('YYYY-MM-DD HH:mm'))}
                              // onConfirm={(data1) => new Date(data1).getTime() > new Date(data.startTime).getTime() + 1000 * 60 ? handleConfirmEnd(data1) : handleConfirmEnd(new Date(data.startTime).setTime(new Date(data.startTime).getTime() + 1000 * 60 * 5))}
                              onCancel={hideDatePickerEnd}
                              headerTextIOS={convertLanguage(language, 'pick_a_date')}
                              minimumDate={Platform.OS === 'ios' ?
                                new Date(moment(data.startTime).add(1, 'minutes').format('YYYY-MM-DDTHH:mm'))
                                :
                                new Date(moment(data.startTime).add(1, 'minutes').format('YYYY-MM-DD'))
                              }
                            />
                          </View>
                        </View>
                      </View>

                      <TouchableOpacity onPress={() => setState({ ...state, modalRepeat: !state.modalRepeat, edit: [] })} style={{ width: 110 * scale, height: 36 * scale, borderWidth: 1, borderRadius: 4 * scale, backgroundColor: data.endTime !== '' ? 'white' : '#BDBDBD', borderWidth: data.endTime !== '' ? 1 : 0 }} disabled={data.endTime !== '' && (event?.event?.is_Finish === false || event?.event?.is_Finish === undefined) ? false : true}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', height: '100%', alignItems: 'center' }}>
                          <Image source={require('../../assets/Loop.png')} style={{ resizeMode: 'cover', width: 12 * scale, height: 16 * scale, marginRight: 7 * scale }} />
                          <Text numberOfLines={1} style={{ fontWeight: 'normal', fontSize: 14 * scale / scaleFontSize, color: '#1B1D1F' }}>{convertLanguage(language, 'repeat_event')}</Text>
                        </View>
                      </TouchableOpacity>


                      < View
                        style={{
                          borderBottomColor: '#00A9F4',
                          borderBottomWidth: 1,
                          width: width - 32 * scale,
                          marginVertical: 20 * scale
                        }}
                      />
                      {state.dataRepeat.length > 0 &&
                        <>
                          {/*  */}
                          {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 14 * scale }}>
                    <Text>2019.Nov.20 03:36 ~ 2020.Dec.31 03:36</Text>
                    <View style={{ flexDirection: 'row' }}>
                      <TouchableOpacity style={{ marginRight: 20 * scale }}>
                        <Image source={require('../../assets/EditRepeat.png')} style={{ resizeMode: 'cover', width: 20 * scale, height: 20 * scale }} />
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <Image source={require('../../assets/DeleteRepeat.png')} style={{ resizeMode: 'cover', width: 20 * scale, height: 20 * scale }} />
                      </TouchableOpacity>
                    </View>
                  </View> */}
                          {/*  */}
                          {state.dataRepeat.map((item, index) => {
                            return (
                              <View key={index} style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 14 * scale }}>
                                <Text>{item}</Text>
                                <View style={{ flexDirection: 'row' }}>
                                  <TouchableOpacity onPress={() => onEditRepeat(index, item)} style={{ marginRight: 20 * scale }}>
                                    <Image source={require('../../assets/EditRepeat.png')} style={{ resizeMode: 'cover', width: 20 * scale, height: 20 * scale }} />
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={() => onRemoveRepeat(item)}>
                                    <Image source={require('../../assets/DeleteRepeat.png')} style={{ resizeMode: 'cover', width: 20 * scale, height: 20 * scale }} />
                                  </TouchableOpacity>
                                </View>
                              </View>
                            )
                          })}
                          <View
                            style={{
                              borderBottomColor: '#00A9F4',
                              borderBottomWidth: 1,
                              width: width - 32 * scale,
                              marginBottom: 20 * scale
                            }}
                          />
                        </>
                      }
                    </View  >
                  </View>


                  {
                    props?.route?.params['Slug'] && props?.route?.params['Slug'] !== undefined ?
                      <View style={styles.boxSubmit}>
                        <View style={styles.containerSubmit}>
                          <Touchable style={styles.draftActive} onPress={() => setChangeStep(4)}>
                            <Text style={styles.txtDreftActive}>{convertLanguage(language, 'cancel')}</Text>
                          </Touchable>
                          <Touchable style={styles.saveActive} onPress={() => onValidation()}>
                            <Text style={styles.txtActive} >{convertLanguage(language, 'save')}</Text>
                          </Touchable>
                        </View>
                      </View>
                      :
                      <View style={styles.boxSubmit}>
                        <View style={styles.containerSubmit}>
                          <Touchable style={styles.draftActive} onPress={() => onValidation('draft')}>
                            <Text style={styles.txtDreftActive}>{convertLanguage(language, 'save_as_draft')}</Text>
                          </Touchable>
                          <Touchable style={styles.saveActive} onPress={() => onValidation()}>
                            <Text style={styles.txtActive} >{convertLanguage(language, 'save_next_step')}</Text>
                          </Touchable>
                        </View>
                      </View>

                  }
                  {/* </KeyboardAwareScrollView> */}

                </ScrollView>
              </KeyboardAvoidingView>
            </>
          }
          {/* step2 */}
          {
            changeStep === 2 && <EventCreateStep2 checkImport={event.checkImport} IdEventDtaft={IdEventDtaft} load={() => dispatch(actUpdateEvent2(props.route.params['Slug']))} changeStep4={() => setChangeStep(4)} update={props.route.params['Slug']} setChangeStep={() => { setChangeStep(3), setStep(3) }} navigation={navigation} HostInfoId={props.route.params['HostInfoId']} />
          }
          {/*  */}
          {/* step23 */}
          {
            changeStep === 3 && <EventCreateStep3 event={event.event} dEventDtaft={IdEventDtaft} load={() => dispatch(actUpdateEvent2(props.route.params['Slug']))} changeStep4={() => setChangeStep(4)} update={props.route.params['Slug']} Unit={initial.Unit} navigation={navigation} timeEvent={data} HostInfoId={props.route.params['HostInfoId']} setChangeStep={() => { setChangeStep(4), setStep(4) }} />
          }
          {
            changeStep === 4 && <EventCreateStep4
              changeStep1={() => setChangeStep(1)}
              changeStep2={() => setChangeStep(2)}
              changeStep3={() => setChangeStep(3)}
              IdEventDtaft={IdEventDtaft}
              navigation={navigation} HostInfoId={props.route.params['HostInfoId']} update={props.route.params['Slug']}
            />
          }
          {/*  */}

          {
            modal &&
            <ModalTagNew
              closeModal={() => setModal(false)}
              tags={showDataCategory()}
              selectCategory={(category) => selectCategory(category)}
              selected={initial.selected}
              onConfirm={() => { setInitial({ ...initial, category_selected: initial.selected, listCategory: showListCategory() }), setErrors({ ...errors, category_selected: false }) }}
            />
          }
          {
            modalCity &&
            <ModalNewCity
              modalVisible={modalCity}
              closeModal={() => setModalCity(false)}
              countries={countries}
              selectCity={(city) => { onSelect(city), setErrors({ ...errors, AreaName: false }) }}
            // onConfirm={() => setInitial({ ...initial, category_selected: initial.selected })}
            />
          }
          {
            modalVenue &&
            <ModalVenue
              modalVisible={modalVenue}
              AreaId={initial.AreaId}
              closeModal={() => setModalVenue(false)}
              selectVenue={(type, venue) => { setVenue(type, venue), setErrors({ ...errors, VenueName: false }) }}
            // onConfirm={() => setInitial({ ...initial, category_selected: initial.selected })}
            />
          }
          {
            state.modalRepeat &&
            <ModalRepeat
              startTime={data.startTime.split(" ")[1]}
              endTime={data.endTime.split(" ")[1]}
              edit={state.edit}
              dataRepeat={data}
              modalVisible={state.modalRepeat}
              closeModal={() => setState({ ...state, modalRepeat: false })}
              changeDataRepeat={(data, indexEdit) => changeDataRepeat(data, indexEdit)}
            />
          }
          {
            modalPreviousLocation &&
            <ModalPreviousLocation
              selectLocation={(location) => setLocation(location)}
              modalVisible={modalPreviousLocation}
              closeModal={() => setModalPreviousLocation(false)}
            />
          }
          {
            state.modalImportEvent &&
            <ModalImportEvent
              HostInfoId={props.route.params['HostInfoId']}
              modalVisible={state.modalImportEvent}
              setIdEventDtaftCallBack={(idEvent) => setIdEventDtaftCallBack(idEvent)}
              closeModal={() => setState({ ...state, modalImportEvent: false })}
            />
          }
        </View >
      </SafeView>
    )
  }
}

// CreateEventStep1.propTypes = {
//   provider: ProviderPropType,
// };

const styles = StyleSheet.create({
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: Platform.OS === 'android' ? 55 * scale : 47 * scale,
    zIndex: 1
  },
  container: {
    backgroundColor: Colors.BG,
    flex: 1
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: 16 * scale,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.1)',
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 1 * scale,
  },
  imgBack: {
    width: 18 * scale,
    height: 18 * scale
  },
  titleContex: {
    fontSize: 14 * scale,
    fontFamily: 'SourceSansPro-SemiBold',
    color: '#333333',
    paddingBottom: 16 * scale
  },
  touchAction: {
    minWidth: 48 * scale,
    minHeight: 48 * scale,
    alignSelf: 'flex-start',
    justifyContent: 'center', alignItems: 'center',
  },
  headerLeft: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerRight: { color: Colors.PRIMARY, fontSize: 12 * scale },
  txtLeft: {
    paddingLeft: 4 * scale,
    fontSize: 18 * scale,
    fontFamily: 'SourceSansPro-SemiBold',
    color: '#4f4f4f',
    lineHeight: 23 * scale
  },

  stepContainer: { flexDirection: 'row', justifyContent: 'space-between', paddingTop: 16 * scale, borderBottomWidth: 1, borderBottomColor: '#F2F2F2', marginHorizontal: 8 * scale },
  actionStepActive: { width: '25%', flexDirection: 'row', justifyContent: 'center', borderBottomColor: Colors.PRIMARY, borderBottomWidth: 1 * scale },
  actionStep: { width: '25%', flexDirection: 'row', justifyContent: 'center' },
  txtAction: { paddingTop: 4 * scale, fontSize: 12 * scale, lineHeight: 18 * scale, color: '#4F4F4F', textAlign: 'center' },
  imgChevRight: { width: 16 * scale, height: 16 * scale, tintColor: '#BDBDBD' },
  imgAction: { width: 32 * scale, height: 32 * scale },
  txtBoxinfo: { top: 8 * scale, position: 'absolute', right: -8 * scale },
  boxImageThumb: {
    marginBottom: 30 * scale,
    paddingTop: 16 * scale
  },
  boxAddImage: {
    height: 184 * scale,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderWidth: 4 * scale,
    borderStyle: 'dashed',
    borderRadius: 16 * scale
  },
  txtAddImage: {
    fontSize: 16 * scale,
    fontFamily: 'SourceSansPro-SemiBold'
  },
  imgAddThumb: { width: 32 * scale, height: 32 * scale, marginRight: 5 * scale, tintColor: Colors.PRIMARY },
  ipContent: {
    fontSize: 15 * scale,
    fontWeight: 'bold',
    color: '#333333',
    paddingTop: 8 * scale,
    paddingBottom: 8 * scale,
    borderBottomWidth: 1,
    borderBottomColor: '#bdbdbd'
  },
  txtContent: {
    fontSize: 15 * scale,
    color: '#333333',
    fontWeight: 'bold',
    // position: "absolute", bottom: -50 * scale
  },
  inputContainerStyle: {
    borderWidth: 1,
    borderRadius: 4 * scale,
    paddingHorizontal: 12 * scale
  },
  containerStyle: {
    // paddingHorizontal: 20,
    // flex: 1
    marginVertical: 10 * scale,
  },
  input: {
    color: '#333333',
    // fontSize: 5
  },
  markerFixed: {
    left: '50%',
    marginLeft: -16 * scale,
    marginTop: -45 * scale,
    position: 'absolute',
    top: '50%'
  },
  marker: {
    height: 45 * scale,
    width: 32 * scale
  },
  boxPinLocation: {
    position: 'absolute',
    alignSelf: 'center',
    minWidth: 150 * scale,
    // height: 36 * scale,
    paddingHorizontal: 5 * scale,
    paddingVertical: 3 * scale,
    backgroundColor: '#03a9f4',
    borderRadius: 4 * scale,
    alignItems: 'center',
    justifyContent: 'center',
    top: 20 * scale,
    flexDirection: 'row',
  },
  txtPinLocation: {
    color: '#FFFFFF',
    fontSize: 14 * scale,
    fontWeight: 'bold'
  },
  boxAction: {
    position: 'absolute',
    bottom: 15 * scale,
    right: 15 * scale
  },
  btnMyLocation: {
    width: 30 * scale,
    height: 30 * scale,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 10 * scale
  },
  icMyLocation: {
    width: 20 * scale,
    height: 20 * scale
  },
  btnPlus: {
    width: 30 * scale,
    height: 30 * scale,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 10 * scale
  },
  icPlus: {
    width: 18 * scale,
    height: 18 * scale
  },
  btnMinus: {
    width: 30 * scale,
    height: 30 * scale,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 10 * scale
  },
  icMinus: {
    width: 18 * scale,
    height: 18 * scale
  },
  content: {
    flex: 1,
    position: 'relative'
  },
  btnSelect: {
    backgroundColor: '#FFFFFF',
    paddingBottom: 10 * scale,
    paddingTop: 10 * scale,
    borderColor: '#bdbdbd',
    borderWidth: 1,
    paddingLeft: 5 * scale,
    paddingRight: 5 * scale, zIndex: 99999,
  },
  boxEditMap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtEditMap: {
    fontSize: 12 * scale,
    color: '#333333',
    flex: 1,
    lineHeight: 20 * scale,
    fontWeight: 'normal'
  },
  btnEdit: {
    // width: 113 * scale,
    height: 36 * scale,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#bdbdbd',
    paddingLeft: 10 * scale,
    paddingRight: 10 * scale,
    marginLeft: 10 * scale,
    borderRadius: 4,
    borderColor: '#4F4F4F'
  },
  txtEdit: {
    fontSize: 12 * scale,
    fontWeight: 'normal',
    color: '#4F4F4F'
  },
  imgThumb: {
    // width: width,
    // resizeMode: 'contain',
    height: 184 * scale,
    borderRadius: 4 * scale
  },
  boxInput: {
    paddingBottom: 24,
  },
  txtInput: {
    fontSize: 14,
    color: '#757575',
    marginBottom: 7,
    fontWeight: 'bold'
  },
  boxDate: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },
  boxSubmit: {
    backgroundColor: 'white',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: -6,
    },
    shadowOpacity: 0.10,
    shadowRadius: 16.00,
    elevation: 30,
    marginTop: 36 * scale,
    width: width,
    paddingHorizontal: 16 * scale,
    paddingVertical: 16 * scale
  },
  containerSubmit: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
  draftActive: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', borderWidth: 1, width: 114 * scale, height: 36 * scale, borderColor: '#4F4F4F' },
  txtDreftActive: {
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16 * scale / scaleFontSize,
    lineHeight: 20 * scale,
    color: '#4F4F4F'
  },
  draftDefault: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', marginRight: 16 * scale, width: 115 * scale, height: 36 * scale, backgroundColor: '#E0E2E8' },
  txtDreft: {
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: 20 * scale,
    color: '#B3B8BC'
  },
  saveActive: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', width: 198 * scale, height: 36 * scale, backgroundColor: '#00A9F4' },
  txtActive: {
    color: 'white', fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16 * scale / scaleFontSize,
    lineHeight: 20 * scale,
  },
  saveDefault: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', width: 180 * scale, height: 36 * scale, backgroundColor: '#E0E2E8' },
  txtDefault: {
    color: 'white', fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: 20 * scale,
    color: '#B3B8BC'
  }
})

export default CreateEventStep1