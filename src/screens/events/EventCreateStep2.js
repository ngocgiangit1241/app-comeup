import React, { memo, useEffect, useRef, useState } from 'react';
import { ActivityIndicator, Dimensions, Image, KeyboardAvoidingView, PixelRatio, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import Popover from 'react-native-popover-view';
import { useDispatch, useSelector } from "react-redux";
import { actSaveEventDraft, actSaveEventStep2, actUpdateStep2 } from '../../actions/event';
import ModalImportHost from '../../components/ModalImportHost';
import { TextField } from '../../components/react-native-material-textfield';
import * as Colors from '../../constants/Colors';
import Touchable from '../../screens_view/Touchable';
import { convertLanguage } from '../../services/Helper';
const { width, height } = Dimensions.get('window')
const scale = width / 360
const scaleFontSize = PixelRatio.getFontScale()

const EventCreateStep2Component = ({ navigation, HostInfoId, setChangeStep, changeStep4, update, load, IdEventDtaft }) => {
    const { language } = useSelector(state => state.language)
    const event = useSelector(state => state.event)
    const dispatch = useDispatch()
    const [loadingDraft, setIdLoadingDraft] = useState(false)
    const [state, setState] = useState(false)
    const [modalImport, setModalImport] = useState(false)
    const scrollValidate = useRef(null)
    const [data, setData] = useState({
        companyName: '',
        Zipcode: '+84',
        phone: '',
        err: false,
        errEmail: false,
        Data: [[], [], [], []],
        Detail: '',
        email: '',
    })
    const rerturnFAQ = (data) => {
        let fetch = []
        data.forEach(element => {
            fetch.push({ key: element })
        });
        return fetch
    }
    const converFAQ1 = () => {
        let dataFetch = [[], [], [], []]
        if (event?.event?.FAQ) {
            dataFetch[0] = event?.event?.FAQ.A !== undefined ? rerturnFAQ(event?.event?.FAQ.A) : []
            dataFetch[1] = event?.event?.FAQ.B !== undefined ? rerturnFAQ(event?.event?.FAQ.B) : []
            dataFetch[2] = event?.event?.FAQ.C !== undefined ? rerturnFAQ(event?.event?.FAQ.C) : []
            dataFetch[3] = event?.event?.FAQ.D !== undefined ? rerturnFAQ(event?.event?.FAQ.D) : []
        }
        return dataFetch
    }
    const converFAQ2 = () => {
        let dataFetch = [[], [], [], []]
        if (event?.eventImport?.FAQ) {
            dataFetch[0] = event?.eventImport?.FAQ.A !== undefined ? rerturnFAQ(event?.eventImport?.FAQ.A) : []
            dataFetch[1] = event?.eventImport?.FAQ.B !== undefined ? rerturnFAQ(event?.eventImport?.FAQ.B) : []
            dataFetch[2] = event?.eventImport?.FAQ.C !== undefined ? rerturnFAQ(event?.eventImport?.FAQ.C) : []
            dataFetch[3] = event?.eventImport?.FAQ.D !== undefined ? rerturnFAQ(event?.eventImport?.FAQ.D) : []
        }
        return dataFetch
    }
    useEffect(() => {
        if (event?.event?.HostInfo && Object.keys(event?.event?.HostInfo).length !== 0) {
            let dataFetch = {}
            dataFetch.Data = converFAQ1()
            dataFetch.Detail = event?.event?.Detail === null ? '' : event?.event?.Detail
            dataFetch.Zipcode = event?.event?.HostInfo.Zipcode === "" ? '+84' : event?.event?.HostInfo.Zipcode
            dataFetch.companyName = event?.event?.HostInfo.Name === null ? '' : event?.event?.HostInfo.Name
            dataFetch.phone = event?.event?.HostInfo.Phone === null ? '' : event?.event?.HostInfo.Phone
            dataFetch.email = event?.event?.HostInfo.Email === null ? '' : event?.event?.HostInfo.Email
            setData({ ...data, ...dataFetch })
            return;
        }
        if (event?.eventImport?.HostInfo && Object.keys(event?.eventImport?.HostInfo).length !== 0) {
            let dataFetch = {}
            dataFetch.Data = converFAQ2()
            dataFetch.Detail = event?.eventImport?.Detail === null ? '' : event?.eventImport?.Detail
            dataFetch.Zipcode = event?.eventImport?.HostInfo.Zipcode === "" || event?.eventImport?.HostInfo.Zipcode === null ? '+84' : event?.eventImport?.HostInfo.Zipcode
            dataFetch.companyName = event?.eventImport?.HostInfo.Name === null ? '' : event?.eventImport?.HostInfo.Name
            dataFetch.phone = event?.eventImport?.HostInfo.Phone === null ? '' : event?.eventImport?.HostInfo.Phone
            dataFetch.email = event?.eventImport?.HostInfo.Email === null ? '' : event?.eventImport?.HostInfo.Email
            setData({ ...data, ...dataFetch })
        }
    }, [event])



    const touchable = useRef(null)

    const showPopover = () => {
        setState(true);
    }

    const closePopover = () => {
        setState(false);
    }
    const renderLoading = () => {
        return <ActivityIndicator size="large" color="#000000" />
    }
    var FAQS = [];
    FAQS.push(
        {
            type: 'checkbox',
            name: convertLanguage(language, 'a_ticket_check-in_at_the_event'),
            data: [
                {
                    key: 'comeup-ticket',
                    value: convertLanguage(language, 'comeup_ticket')
                },
                {
                    key: 'printed-comeup-e-ticket',
                    value: convertLanguage(language, 'printed_comeup_e-ticket')
                },
                {
                    key: 'ticket-holder-name-&-phone-number',
                    value: convertLanguage(language, 'ticket_holder_name_&_phone_number')
                },
                {
                    key: 'delivered-ticket-from-this-event-host',
                    value: convertLanguage(language, 'delivered_ticket_from_this_event_host')
                },
                // {
                //     key: 'no-ticket-check-in',
                //     value: convertLanguage(language, 'no_ticket_check-in')
                // }
            ],
        })
    FAQS.push({
        type: 'checkbox',
        name: convertLanguage(language, 'b_restrictions'),
        data: [
            {
                key: 'id-card-check',
                value: convertLanguage(language, 'id_card_check')
            },
            {
                key: 'no-minor',
                value: convertLanguage(language, 'no_minor')
            },
            {
                key: 'no-carrying-food-and-beverage',
                value: convertLanguage(language, 'no_carrying_food_and_beverage')
            },
            {
                key: 'no-pet',
                value: convertLanguage(language, 'no_pet')
            },
            // {
            //     key: 'custom-restriction',
            //     value: convertLanguage(language, 'custom_restriction')
            // }
        ]
    })
    FAQS.push({
        type: 'radio',
        name: convertLanguage(language, 'c_parking') + ' *',
        data: [
            {
                key: 'no-car-park',
                value: convertLanguage(language, 'no_car_park')
            },
            {
                key: 'free',
                value: convertLanguage(language, 'free')
            },
            {
                key: 'paid',
                value: convertLanguage(language, 'paid')
            }
        ]
    })
    FAQS.push({
        type: 'radio',
        name: convertLanguage(language, 'd_refund_policy') + ' *',
        data: [
            {
                key: 'comeup-refund-policy',
                value: convertLanguage(language, 'comeup_refund_policy')
            },
            {
                key: 'full-refund-before-event-start',
                value: convertLanguage(language, 'full_refund_before_event_start')
            },
            {
                key: 'no-refund',
                value: convertLanguage(language, 'no_refund')
            },
            {
                key: 'host-refund-policy',
                value: convertLanguage(language, 'host_refund_policy')
            }
        ]
    })

    const onChangeFAQ = (faq, type, index, index2) => {
        var { Data } = data;
        var i = Data[index].some(el => el.key === faq.key);
        if (!i) {
            if (type === 'radio') {
                Data[index] = [];
            }
            Data[index].unshift({ key: faq.key });
        } else {
            Data[index] = Data[index].filter(el => el.key !== faq.key);
        }
        setData({ ...data, Data })
    }

    function importHostInfo(hostinfo) {
        setData({ ...data, companyName: hostinfo.Name, Zipcode: hostinfo.Zipcode !== '' ? hostinfo.Zipcode : '+84', phone: hostinfo.Phone, email: hostinfo.Email })
    }
    function getFAQ() {
        var result = {};
        var a = [];
        var b = [];
        var c = [];
        var d = [];
        data.Data[0].forEach((item) => {
            a.push(item.key)
        })
        data.Data[1].forEach((item) => {
            b.push(item.key)
        })
        data.Data[2].forEach((item) => {
            c.push(item.key)
        })
        data.Data[3].forEach((item) => {
            d.push(item.key)
        })
        result['A'] = a;
        result['B'] = b;
        result['C'] = c;
        result['D'] = d;
        return result;
    }
    function onUpdate() {
        var body = {
            NameHost: data.companyName,
            PhoneHost: data.phone,
            EmailHost: data.email,
            Zipcode: data.Zipcode,
            FAQ: getFAQ(),
            Detail: data.Detail
        }
        dispatch(actUpdateStep2(update, body))
            .then((res) => {
                if (res && res.status === 200) {
                    load()
                    changeStep4()
                }
            })

    }

    function onRegister(type = '') {
        var body = {
            NameHost: data.companyName,
            PhoneHost: data.phone,
            EmailHost: data.email,
            Zipcode: data.Zipcode,
            FAQ: getFAQ(),
            Detail: data.Detail
        }

        if (type === 'draft') {
            setIdLoadingDraft(true)
            dispatch(actSaveEventDraft(HostInfoId, body, IdEventDtaft, event.event.Id, language))
                .then((res) => {
                    if (res && res.status === 200) {
                        setIdLoadingDraft(false)
                        setData(...data, body)
                        // setIdEventDtaft(res.Id);
                    }
                })
            setTimeout(() => {
                setIdLoadingDraft(false)
            }, 5000);
        } else {
            dispatch(actSaveEventStep2(HostInfoId, body, event.event.Id))
                .then((res) => {
                    if (res && res.status === 200) {
                        setChangeStep()
                    }
                })
        }

    }
    const [error, setError] = useState({
        companyName: false,
        phone: false,
        email: false,
        Data1: false,
        Data2: false,
        phoneString: '',
        emailString: ''
    })
    function onValidate() {
        let email = false
        let companyName = false
        let phone = false
        let Data1 = false
        let Data2 = false
        let phoneString = ''
        let emailString = ''
        var mailformat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (data.email !== '') {
            if (data.email.match(mailformat)) {
                email = false
                emailString = ''
            } else {
                emailString = convertLanguage(language, 'error_email')
                email = true
            }
        } else {
            emailString = ''
            email = true
        }

        if (data.companyName !== '') {
            companyName = false
        } else {
            companyName = true
        }

        if (data.phone !== '') {
            if (data.phone.length > 8 && data.phone.length < 12) {
                phone = false
                phoneString = ''
            } else {
                phoneString = convertLanguage(language, 'phone_min')
                phone = true
            }
        } else {
            phoneString = ''
            phone = true
        }

        if (data.Data[2].length > 0) {
            Data1 = false
        } else {
            Data1 = true
        }


        if (data.Data[3].length > 0) {
            Data2 = false
        } else {
            Data2 = true
        }
        setError({
            companyName,
            phone,
            email,
            Data1,
            Data2,
            phoneString, emailString
        })

        if (companyName || phone || email) {
            scrollValidate.current.scrollTo({ y: 350 * scale, animated: true })
        }
        if (!companyName && !phone && !email && !Data1 && !Data2) {
            if (update !== undefined) {
                onUpdate()
            } else {
                onRegister()
            }
        }
    }

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={styles.container}
        >
            <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between' }} showsVerticalScrollIndicator={false} ref={scrollValidate}>
                <View style={{ paddingHorizontal: 12 * scale, paddingTop: 24 * scale }}>
                    <Text style={{
                        color: '#333333',
                        fontFamily: 'SourceSansPro-SemiBold',
                        fontSize: 14 * scale,
                        paddingBottom: 8 * scale
                    }}>{convertLanguage(language, 'description')}</Text>
                    <View style={{
                        height: 320 * scale,
                    }}>
                        {/* <WebViewEditDetail /> */}
                        <View style={styles.boxInput}>
                            <Touchable style={styles.ipContent} onPress={() => navigation.navigate('WebViewEditDetail', { Detail: data.Detail, callback: (data2) => setData({ ...data, ...data2 }) })}>
                                <View style={{ height: 262 * scale, padding: 8 * scale, overflow: 'hidden' }}>
                                    <AutoHeightWebView startInLoadingState={true} scalesPageToFit={false}
                                        customStyle={`*{font-family: 'SourceSansPro-SemiBold';}`}
                                        zoomable={false} scrollEnabled={false} renderLoading={renderLoading}
                                        source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 66}px; overflow-wrap: break-word} iframe {width: ${width - 66}px; height: ${(width - 66) * 9 / 16}px} img {width: ${width - 66}px !important; margin-bottom: 5px; margin-top: 5px; height: auto !important;}</style><body>` + data.Detail.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') + `</body></html>` }}
                                        originWhitelist={['*']} />
                                </View>
                                {
                                    data.Detail.length > 0 && data.Detail !== '<p><br></p>' && data.Detail !== '<br>' &&
                                    <Text style={styles.txtEdit} onPress={() => navigation.navigate('WebViewEditDetail', { Detail: data.Detail, callback: (data2) => setData({ ...data, ...data2 }) })}>+ {convertLanguage(language, 'edit')}</Text>

                                }
                            </Touchable>
                        </View>


                    </View>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        fontStyle: 'normal',
                        fontFamily: 'SourceSansPro-SemiBold',
                        // fontSize: 20,

                    }}>
                        <Text style={{
                            color: '#333333',
                            fontStyle: 'normal',
                            fontFamily: 'SourceSansPro-SemiBold',
                            fontSize: 14 * scale,
                        }}>
                            {convertLanguage(language, 'host_contact_information')}
                        </Text>
                        <Touchable
                            onPress={() => setModalImport(!modalImport)}
                        >
                            <Text style={styles.headerRight}>{convertLanguage(language, 'import')}</Text>
                        </Touchable>
                    </View>

                    <View>
                        <TextField
                            autoCorrect={false}
                            enablesReturnKeyAutomatically={true}
                            value={data.companyName}
                            onChangeText={(companyName) => setData({ ...data, companyName })}
                            error={error.companyName}
                            returnKeyType='next'
                            label={convertLanguage(language, 'companyname') + ' *'}
                            baseColor={'#828282'}
                            tintColor={'#828282'}
                            errorColor={'#EB5757'}
                            inputContainerStyle={[styles.inputContainerStyle, { borderColor: error.companyName ? '#EB5757' : '#BDBDBD' }]}
                            containerStyle={styles.containerStyle}
                            // labelHeight={25}
                            labelTextStyle={{ paddingBottom: 15, paddingLeft: 12 }}
                            lineWidth={2}
                            selectionColor={'#3a3a3a'}
                            style={styles.input}
                            errorImage={require('../../assets/error.png')}
                        />
                        <View style={{ flexDirection: 'row', flex: 1 }}>
                            <TouchableOpacity
                                onPress={() => showPopover()}
                                ref={touchable}
                                style={{
                                    borderWidth: 1,
                                    borderColor: '#bdbdbd',
                                    paddingBottom: 6 * scale, paddingTop: 6 * scale,
                                    flexDirection: 'row',
                                    // alignItems: 'center',
                                    justifyContent: 'space-between',
                                    marginRight: 10 * scale,
                                    height: 37.5 * scale,
                                    borderRadius: 4 * scale,
                                    paddingHorizontal: 12 * scale,
                                    // marginVertical: 10 * scale,
                                    marginTop: 5 * scale,
                                    width: 100 * scale, flex: 1 / 4,
                                    alignItems: 'center'
                                }}>
                                <View style={{ justifyContent: 'flex-start' }}>
                                    <Text style={{ color: '#828282', fontSize: 10 * scale / scaleFontSize, }}>{convertLanguage(language, 'country')}</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
                                        <Image resizeMode="contain" source={data.Zipcode === '+84' ? require('../../assets/flag_vn.png') : data.Zipcode === '+66' ? require('../../assets/flag_th.png') : require('../../assets/flag_kr.png')} style={styles.ic_country} />
                                        <Text style={styles.txtCountry}>{data.Zipcode}</Text>
                                    </View>
                                </View>
                                <Image source={require('../../assets/down_arrow_active2.png')} style={{
                                    width: 15 * scale,
                                    height: 15 * scale,
                                    marginTop: 10 * scale,

                                }} resizeMode="contain" />
                            </TouchableOpacity>
                            <View style={{ flex: 3 / 4 }}>
                                <TextField
                                    autoCorrect={false}
                                    enablesReturnKeyAutomatically={true}
                                    value={data.phone}
                                    onChangeText={(phone) => !isNaN(phone) ? setData({ ...data, phone: phone, err: false }) : setData({ ...data, err: true })}
                                    // onChangeText={(Phone) => this.setState({ Phone: Phone == 0 ? '' : Phone, isChange: true })}
                                    error={error.phone}
                                    returnKeyType='next'
                                    label={convertLanguage(language, 'phone_number') + ' *'}
                                    baseColor={'#828282'}

                                    // baseColor={this.state.Phone.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                    tintColor={'#828282'}
                                    errorColor={'#EB5757'}
                                    inputContainerStyle={[styles.inputContainerStyle, { borderColor: error.phone ? '#EB5757' : '#BDBDBD' }]}
                                    // inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.firstname ? '#EB5757' : '#BDBDBD' }]}
                                    containerStyle={[styles.containerStyle, { paddingHorizontal: 0 }]}
                                    // labelHeight={25}
                                    labelTextStyle={{ paddingBottom: 15 }}
                                    lineWidth={2}
                                    selectionColor={'#3a3a3a'}
                                    style={styles.input}
                                    labelTextStyle={{ paddingLeft: 12 }}
                                    errorImage={require('../../assets/error.png')}
                                    keyboardType="number-pad"
                                    maxLength={11}
                                    minLength={9}
                                />
                            </View>
                        </View>
                        {error.phoneString !== '' &&
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 3 / 5, }}>
                                </View>
                                <View style={{ flex: 4 / 5 }}>
                                    <Text style={{ fontSize: 12 * scale, color: 'red' }}>{error.phoneString}</Text>
                                </View>
                            </View>
                        }
                        <TextField
                            autoCorrect={false}
                            enablesReturnKeyAutomatically={true}
                            value={data.email}
                            onChangeText={(email) => setData({ ...data, email })}
                            error={error.email}
                            returnKeyType='next'
                            label={convertLanguage(language, 'email') + ' *'}
                            baseColor={'#828282'}
                            tintColor={'#828282'}
                            errorColor={'#EB5757'}
                            inputContainerStyle={[styles.inputContainerStyle, { borderColor: error.email ? '#EB5757' : '#BDBDBD', marginTop: 5 * scale }]}
                            containerStyle={styles.containerStyle}
                            // labelHeight={25}
                            labelTextStyle={{ paddingBottom: 15, paddingLeft: 12 }}
                            lineWidth={2}
                            selectionColor={'#3a3a3a'}
                            style={styles.input}
                            errorImage={require('../../assets/error.png')}
                        />
                    </View>
                    {error.emailString !== '' &&
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 12 * scale, color: 'red' }}>{error.emailString}</Text>
                        </View>
                    }
                    <View style={{ marginTop: 10 * scale }}>
                        <Text style={[styles.txtBold, { color: '#333333', marginTop: 10 * scale, marginBottom: -15 }]}>{convertLanguage(language, 'faq')}</Text>
                        {
                            FAQS.map((item, index) => {
                                return <View style={styles.boxQuestion} key={index}>
                                    {(index === 0 || index === 1) &&
                                        <Text style={[styles.txtTitleQuestion, { color: '#333333' }]}>{item.name}</Text>
                                    }
                                    {index === 2 && <Text style={[styles.txtTitleQuestion, { color: error.Data1 === false ? '#333333' : '#EB5757' }]}>{item.name}</Text>}
                                    {index === 3 && <Text style={[styles.txtTitleQuestion, { color: error.Data2 === false ? '#333333' : '#EB5757' }]}>{item.name}</Text>}
                                    {
                                        item.data.map((faq, index2) => {

                                            return <Touchable style={styles.boxItemOptions} onPress={() => onChangeFAQ(faq, item.type, index, index2)} key={index2}>
                                                {
                                                    item.type === 'checkbox'
                                                        ?
                                                        <Image source={data.Data.length > 0 && data.Data[index].some(el => el.key === faq.key) ? require('../../assets/box_checked.png') : require('../../assets/box_check.png')} style={styles.ic_radio} />
                                                        :
                                                        <Image source={data.Data.length > 0 && data.Data[index].some(el => el.key === faq.key) ? require('../../assets/radio_active_icon.png') : require('../../assets/radio_icon.png')} style={styles.ic_radio} />
                                                }
                                                {/* <Text>{faq.value}</Text> */}
                                                <Text style={[styles.txtOptionsActive, { fontWeight: data.Data.length > 0 && data.Data[index].some(el => el.key === faq.key) ? 'bold' : 'normal' }]}>{faq.value}</Text>
                                            </Touchable>
                                        })
                                    }
                                </View>
                            })
                        }
                    </View>
                    <Popover
                        isVisible={state}
                        animationConfig={{ duration: 0 }}
                        fromView={touchable.current}
                        placement='bottom'
                        arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                        backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                        onRequestClose={() => closePopover()}>
                        <View style={styles.boxPopover}>
                            <Touchable onPress={() => { closePopover(); setData({ ...data, Zipcode: '+82' }) }} style={styles.boxCountry}>
                                <Image source={require('../../assets/flag_kr.png')} style={styles.ic_country} />
                                <Text style={styles.txtCountry}>+82</Text>
                            </Touchable>
                            <Touchable onPress={() => { closePopover(); setData({ ...data, Zipcode: '+84' }) }} style={styles.boxCountry}>
                                <Image source={require('../../assets/flag_vn.png')} style={styles.ic_country} />
                                <Text style={styles.txtCountry}>+84</Text>
                            </Touchable>
                            <Touchable onPress={() => { closePopover(); setData({ ...data, Zipcode: '+66' }) }} style={styles.boxCountry}>
                                <Image source={require('../../assets/flag_th.png')} style={styles.ic_country} />
                                <Text style={styles.txtCountry}>+66</Text>
                            </Touchable>
                        </View>
                    </Popover>
                </View>


                {
                    update !== undefined ?
                        <View style={styles.boxSubmit}>
                            <View style={styles.containerSubmit}>
                                <Touchable style={styles.draftActive} onPress={() => changeStep4()}>
                                    <Text style={styles.txtDreftActive}>{convertLanguage(language, 'cancel')}</Text>
                                </Touchable>
                                <Touchable style={styles.saveActive} onPress={() => onValidate()}>
                                    <Text style={styles.txtActive} >{convertLanguage(language, 'save')}</Text>
                                </Touchable>
                            </View>
                        </View>

                        :
                        <View style={styles.boxSubmit}>
                            <View style={styles.containerSubmit}>
                                <Touchable style={styles.draftActive} onPress={() => onRegister('draft')}>
                                    <Text style={styles.txtDreftActive}>{convertLanguage(language, 'save_as_draft')}</Text>
                                </Touchable>
                                <Touchable style={styles.saveActive} onPress={() => onValidate()}>
                                    <Text style={styles.txtActive}>{convertLanguage(language, 'save_next_step')}</Text>
                                </Touchable>
                            </View>
                        </View>
                }

            </ScrollView >
            {
                modalImport &&
                <ModalImportHost
                    modalVisible={modalImport}
                    closeModal={() => setModalImport(!modalImport)}
                    HostInfoId={HostInfoId}
                    importHostInfo={(hostinfo) => importHostInfo(hostinfo)}
                // countries={this.props.city.event_countries}
                // selectCity={(city) => this.onSelect('area', city)}
                />
            }
            {/* {
                (event.loadingStep2 || loadingDraft) &&
                <Loading />
            } */}
        </KeyboardAvoidingView >
    )
}
const styles = StyleSheet.create({
    container: { backgroundColor: Colors.BG, flex: 1 },
    inputContainerStyle: {
        borderWidth: 1,
        borderRadius: 4 * scale,
        paddingHorizontal: 12 * scale,
        // height: 59 * scale
    },
    ic_radio: {
        width: 16 * scale,
        height: 16 * scale,
        marginRight: 10 * scale
    },
    txtBold: {
        fontSize: 14 * scale,
        color: '#333333',
        fontFamily: 'SourceSansPro-SemiBold'
    },
    boxQuestion: {
        paddingTop: 20 * scale,
        // paddingBottom: 20,
        // borderBottomWidth: 1,
        // borderBottomColor: '#bdbdbd'
    },
    txtEdit: {
        textAlign: 'center',
        color: '#03a9f4',
        fontSize: 14,
        fontFamily: 'SourceSansPro-SemiBold'
    },
    ipContent: {
        fontSize: 15,
        color: '#333333',
        height: 300 * scale,
        overflow: 'hidden',
        borderWidth: 1,
        borderColor: '#bdbdbd',
        justifyContent: 'space-between',
        overflow: 'hidden',
        paddingHorizontal: 8 * scale,
        paddingVertical: 12 * scale
        // textAlignVertical: 'top'
    },
    txtTitleQuestion: {
        fontSize: 15,
        fontFamily: 'SourceSansPro-SemiBold'
    },
    boxItemOptions: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10 * scale
    },
    txtOptionsActive: {
        color: '#333333',
        fontSize: 15,
        fontFamily: 'SourceSansPro-SemiBold'
    },
    boxPopover: {
        width: 100 * scale,
        padding: 12 * scale,
        alignItems: 'center'
    },
    boxCountry: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 6 * scale
    },
    ic_country: {
        width: 18 * scale,
        height: 16 * scale,
        marginRight: 8 * scale
    },
    txtCountry: {
        color: '#333333',
        fontSize: 14 * scale / scaleFontSize
    },
    containerStyle: {
        // paddingHorizontal: 20,
        // flex: 1
        // marginVertical: 10 * scale,
        marginTop: 5 * scale
    },
    input: {
        color: '#333333',
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 12 * scale,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0, 0, 0, 0.1)',
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.1,
        shadowRadius: 1,
    },
    touchAction: {
        minWidth: 48 * scale, minHeight: 48 * scale, alignSelf: 'flex-start',
        justifyContent: 'center', alignItems: 'center',
    },
    headerLeft: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerRight: { color: Colors.PRIMARY, fontSize: 14 },
    txtLeft: { paddingLeft: 4 * scale, fontSize: 18, fontFamily: 'SourceSansPro-SemiBold', color: '#4f4f4f', lineHeight: 23 * scale },
    boxInput: {
        // paddingBottom: 24,
    },
    txtDetail: {
        fontSize: 14,
        color: '#bdbdbd',
    },
    stepContainer: { flexDirection: 'row', justifyContent: 'space-between', paddingTop: 16 * scale, marginBottom: 24 * scale, borderBottomWidth: 1, borderBottomColor: '#F2F2F2', marginHorizontal: 12 * scale },
    boxSubmit: {
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: -6,
        },
        shadowOpacity: 0.10,
        shadowRadius: 16.00,
        elevation: 30,
        marginTop: 36 * scale,
        width: width,
        paddingHorizontal: 16 * scale,
        paddingVertical: 16 * scale
    },
    containerSubmit: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    draftActive: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', borderWidth: 1, width: 114 * scale, height: 36 * scale, borderColor: '#4F4F4F' },
    txtDreftActive: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16 * scale / scaleFontSize,
        lineHeight: 20 * scale,
        color: '#4F4F4F'
    },
    draftDefault: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', marginRight: 16 * scale, width: 115 * scale, height: 36 * scale, backgroundColor: '#E0E2E8' },
    txtDreft: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 20 * scale,
        color: '#B3B8BC'
    },
    saveActive: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', width: 198 * scale, height: 36 * scale, backgroundColor: '#00A9F4' },
    txtActive: {
        color: 'white', fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16 * scale / scaleFontSize,
        lineHeight: 20 * scale,
    },
    saveDefault: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', width: 180 * scale, height: 36 * scale, backgroundColor: '#E0E2E8' },
    txtDefault: {
        color: 'white', fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 20 * scale,
        color: '#B3B8BC'
    }
})
export default EventCreateStep2 = memo(EventCreateStep2Component)