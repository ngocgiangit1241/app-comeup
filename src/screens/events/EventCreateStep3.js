import moment from "moment";
import React, { useEffect, useRef, useState } from 'react';
import { Alert, Dimensions, Image, PixelRatio, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Popover from 'react-native-popover-view';
import { useDispatch, useSelector } from "react-redux";
import { actSaveEventDraft, actSaveEventStep3, actUpdateStep3 } from "../../actions/event";
import ModalCreateTicket from '../../components/ModalCreateTicket';
import { convertLanguage, formatNumber } from '../../services/Helper';
const { width, height } = Dimensions.get('window')
const scale = width / 360
const scaleFontSize = PixelRatio.getFontScale()

function EventCreateStep3(props) {

    const dispatch = useDispatch()
    const event = useSelector(state => state.event)
    const { language } = useSelector(state => state.language)

    const [toogle, setToogle] = useState({
        dot3: false,
        index: -1,
        hide: [],
        statusHide: false
    })
    const [state, setState] = useState({
        checkbox: false,
        modalCreateTicket: false,
        modalRepeat: false,
        edit: []
    })
    useEffect(() => {
        if (event?.event?.Tickets !== undefined && event?.event?.Tickets.length === 0) {
            setState({ ...state, checkbox: true })
        } else {
            setState({ ...state, checkbox: false })
        }
        if (event?.event?.Tickets && event?.event?.Tickets.length > 0) {
            let dataFetch = [...event.event.Tickets]
            dataFetch.forEach(element => {
                element.NameTicket = element.Name
                element.TimeStartTicket = element.TimeStart
                element.TimeEndTicket = element.TimeEnd
                element.LimitQuantity = element.TotalQuantity.toString()
                element.Quantity = element.Quantity.toString()
                element.TicketId = element.Id
            });
            setData(dataFetch)
            return;
        }

        // 
        if (event?.eventImport?.Tickets !== undefined && event?.eventImport?.Tickets.length === 0) {
            setState({ ...state, checkbox: true })
        } else {
            setState({ ...state, checkbox: false })
        }
        if (event?.eventImport?.Tickets && event?.eventImport?.Tickets.length > 0) {
            let dataFetch = [...event.eventImport.Tickets]
            dataFetch.forEach(element => {
                element.NameTicket = element.Name
                element.TimeStartTicket = props.timeEvent.startTime
                element.TimeEndTicket = props.timeEvent.endTime
                element.LimitQuantity = element.TotalQuantity.toString()
                element.Quantity = element.Quantity.toString()
            });
            setData(dataFetch)
        }

    }, [event])



    const inputRef = useRef([]);
    const [data, setData] = useState([]);
    const [poup, setPoup] = useState(<Text></Text>)

    // var formatter = new Intl.NumberFormat('vi-VI', {
    //     style: 'currency',
    //     currency: props.Unit,
    // });
    if (inputRef.current.length !== data.length) {
        inputRef.current = Array(data.length).fill().map((_, i) => inputRef.current[i] || React.createRef())

    }
    const changeToogle = (x, statusHide) => {
        setToogle({ ...toogle, dot3: true, index: x, statusHide })
    }
    useEffect(() => {
        if (toogle.dot3 == true) {
            let fetchPoup = (
                <Popover
                    isVisible={toogle.dot3}
                    animationConfig={{ duration: 0 }}
                    fromView={inputRef?.current[toogle.index]?.current}
                    placement='bottom'
                    arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0 }}
                    backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                    onRequestClose={() => setToogle({ ...toogle, dot3: false })}
                    popoverStyle={{
                        borderRadius: 8, width: 111 * scale, top: 3 * scale, left: -15 * scale, position: 'absolute'
                    }}
                >
                    <View style={{
                        // width: 111 * scale,
                        padding: 12 * scale,

                    }} >
                        <TouchableOpacity onPress={() => showEdit()}>
                            <Text style={{ fontWeight: 'normal', fontSize: 16 * scale / scaleFontSize, marginBottom: 5 * scale }}>{convertLanguage(language, 'edit_ticket')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity disabled={!checkStatusEvent()}
                            onPress={() => changeHide()}>
                            <Text style={{
                                fontWeight: 'normal',
                                fontSize: 16 * scale / scaleFontSize,
                                marginBottom: 5 * scale,
                                opacity: !checkStatusEvent() ? 0.3 : data[toogle.index].total_ticketDetail === 0 || data[toogle.index].total_ticketDetail === undefined ? 1 : 0.3
                            }}
                            >{toogle.statusHide ? convertLanguage(language, 'hide') : convertLanguage(language, 'show')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity disabled={event?.event?.is_Finish === undefined || !event?.event?.is_Finish ? false : true} onPress={() => deleteTicket(toogle.index)}>
                            <Text style={{ fontWeight: 'normal', fontSize: 16 * scale / scaleFontSize, opacity: event?.event?.is_Finish === undefined || !event?.event?.is_Finish ? 1 : 0.3 }}>
                                {convertLanguage(language, 'delete')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </Popover >
            )
            setPoup(fetchPoup)
        } else {
            setPoup(<Text></Text>)
        }

    }, [toogle])
    // console.log('==============================');
    // console.log('data', data);
    // console.log('==============================');

    const arrowUp = (index) => {
        if (index > 0) {
            let fetchData = [...data]
            var data1 = fetchData[index];
            var data2 = fetchData[index - 1];

            fetchData[index - 1] = data1;
            fetchData[index] = data2;
            setData(fetchData)
        }
    }
    const arrowDown = (index) => {
        if (index < data.length - 1) {
            let fetchData = [...data]
            var data1 = fetchData[index];
            var data2 = fetchData[index + 1];
            fetchData[index + 1] = data1;
            fetchData[index] = data2;
            setData(fetchData)
        }
    }
    const deleteTicket = (index) => {
        let fetchData = [...data]
        if ((fetchData[index]?.total_ticketDetail === undefined || fetchData[index]?.total_ticketLock === undefined) || (fetchData[index]?.total_ticketDetail === 0 && fetchData[index]?.total_ticketLock === 0)) {
            Alert.alert(
                "",
                convertLanguage(language, "are_you_sure_you_want_to_delete"),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        onPress: () => console.log("Cancel Pressed"),
                        style: "destructive"
                    },
                    {
                        text: convertLanguage(language, 'ok'), onPress: () => {

                            fetchData.splice(index, 1)
                            setData(fetchData)
                            setPoup(<Text></Text>)
                            setState({ ...state, modalCreateTicket: false })
                        }
                    }
                ]
            );
        } else {
            Alert.alert(
                "",
                convertLanguage(language, "error_delete"),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        onPress: () => console.log("Cancel Pressed"),
                        style: "destructive"
                    }
                ]
            );
        }

    }
    const showEdit = () => {
        setToogle({ ...toogle, dot3: false })
        setState({ ...state, modalCreateTicket: !state.modalCreateTicket })
    }
    const changeEdit = (index, item) => {
        let dataFetch = [...data]
        dataFetch[index] = item
        setData(dataFetch)
    }
    const changeHide = () => {
        let dataFetch = [...data]
        if (dataFetch[toogle.index].total_ticketDetail === 0 || dataFetch[toogle.index].total_ticketDetail === undefined) {
            if (dataFetch[toogle.index].Status === 0) {
                dataFetch[toogle.index].Status = 1
            } else {
                dataFetch[toogle.index].Status = 0
            }
            setData(dataFetch)
            setToogle({ ...toogle, dot3: !toogle.dot3, index: '' })
        }
    }
    const [loadingDraft, setIdLoadingDraft] = useState(false)
    const [validate, setValidate] = useState(true)
    const onValidate = () => {
        if (data.length > 0) {
            setValidate(true)
            return true
        }
        if (state.checkbox) {
            setValidate(true)
            return true
        }
        setValidate(false)
        return false
    }
    const onRegister = (type = '') => {
        if (type === 'draft') {
            setIdLoadingDraft(true)
            dispatch(actSaveEventDraft(props.HostInfoId, { tickets: data }, props.IdEventDtaft, event.event.Id, language))
                .then((res) => {
                    if (res && res.status === 200) {
                        setIdLoadingDraft(false)
                        // setIdEventDtaft(res.Id);
                    }
                })
            setTimeout(() => {
                setIdLoadingDraft(false)
            }, 5000);
        } else {
            console.log('123');
            dispatch(actSaveEventStep3(props.HostInfoId, { tickets: data }, event.event.Id))
                .then(res => {
                    if (res.status === 200) {
                        props.setChangeStep()
                    }
                })

        }
    }

    const onUpdate = () => {
        dispatch(actUpdateStep3(props.update, { tickets: data }))
            .then(async res => {
                if (res.status === 200) {
                    await props.load()
                }
            })
    }


    const checkStatusEvent = () => {
        if (props?.event?.Status == 0) {
            return false
        } else {
            if (props?.event?.IsFinish == true) {
                return false
            } else {
                return true
            }
        }
    }


    console.log('w', checkStatusEvent());
    console.log('state', state);
    console.log('toogle', toogle);
    return (
        <>
            {/* {
                (event.loadingStep3 || loadingDraft) &&
                <Loading />
            } */}
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }} >
                    <View style={{ flex: 1 }}>
                        {
                            data.length > 0 &&
                            <View
                                style={{
                                    borderBottomColor: '#00A9F4',
                                    borderBottomWidth: 1,
                                    width: width - 32 * scale,
                                    // paddingHorizontal: 16 * scale,
                                    alignSelf: 'center',
                                    paddingTop: 24
                                }}

                            />
                        }

                        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between', marginTop: 15 * scale }}>
                            <View style={{ paddingHorizontal: 16 * scale, }}>
                                {
                                    data.length > 0 ?
                                        data.map((item, index) => {
                                            return (
                                                <React.Fragment key={index}>
                                                    <View style={{ borderRadius: 5, padding: 11 * scale, backgroundColor: "#F2F2F2", flexDirection: 'row', justifyContent: 'space-between', marginBottom: 16 * scale }}>
                                                        <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                                                            <TouchableOpacity onPress={() => arrowUp(index)} >
                                                                <Image source={require('../../assets/up.png')} style={{ marginVertical: 10 * scale }} />
                                                            </TouchableOpacity>
                                                            <TouchableOpacity onPress={() => arrowDown(index)} >
                                                                <Image source={require('../../assets/down.png')} style={{ marginVertical: 10 * scale }} />
                                                            </TouchableOpacity>
                                                        </View>
                                                        <View style={{ flexDirection: 'column', flex: 1, marginLeft: 8 * scale, justifyContent: 'space-around' }}>
                                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                                <View style={{ flex: 1 }}>
                                                                    {
                                                                        (item.Status === 0 && (item.total_ticketDetail === 0 || item.total_ticketDetail === undefined)) && <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 16, color: 'red' }}>({convertLanguage(language, 'ticket_status')}) </Text>
                                                                    }
                                                                    <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 16, color: (item.Status === 0 && (item.total_ticketDetail === 0 || item.total_ticketDetail === undefined)) ? '#828282' : '#333333' }}>{item.NameTicket}</Text>
                                                                </View>
                                                                <TouchableOpacity onPress={() => { changeToogle(index, item.Status), setState({ ...state, edit: [index, item] }) }}  >
                                                                    <Image ref={inputRef?.current[index]} source={require('../../assets/MenuVert.png')} style={{ alignSelf: 'flex-end' }} />
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={{ flexDirection: 'column', opacity: (item.Status === 0 && (item.total_ticketDetail === 0 || item.total_ticketDetail === undefined)) ? 0.3 : 1 }}>
                                                                <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 12, color: '#828282' }}>
                                                                    {convertLanguage(language, 'purchasable_by', { date1: moment(item.TimeStartTicket).format('HH:mm YYYY.MM.DD'), date2: moment(item.TimeEndTicket).format('HH:mm YYYY.MM.DD') })}
                                                                </Text>
                                                                {item.Description !== '' && <Text style={{ fontWeight: 'normal', fontSize: 12, color: '#4F4F4F', marginVertical: 4 * scale }}>{item.Description}</Text>}
                                                            </View>
                                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', opacity: (item.Status === 0 && (item.total_ticketDetail === 0 || item.total_ticketDetail === undefined)) ? 0.3 : 1 }}>
                                                                <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 15 }}>{formatNumber(item.Price) + ' ' + props.Unit}</Text>
                                                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                                                    {
                                                                        item.Type === 'd-p-ticket' ?
                                                                            <Image source={require('../../assets/ic_delivery_brown.png')}
                                                                                style={{ width: 20 * scale, height: 10 * scale, marginRight: 3 * scale }} />
                                                                            :
                                                                            <Image source={require('../../assets/ic_eticket.png')}
                                                                                style={{ width: 11 * scale, height: 14 * scale, marginRight: 5 * scale }} />
                                                                    }
                                                                    <Text style={{
                                                                        fontStyle: 'normal',
                                                                        fontSize: 10,
                                                                    }}>{item.Type === 'd-p-ticket' ? convertLanguage(language, 'delivery_ticket') : convertLanguage(language, 'e-ticket')}</Text>
                                                                </View>
                                                            </View>
                                                        </View>

                                                    </View>

                                                </React.Fragment>
                                            )
                                        })

                                        :
                                        <Image source={require('../../assets/step3Image.png')} style={{ height: 292 * scale, width: 256 * scale, resizeMode: 'contain', alignSelf: 'center' }} />
                                }
                            </View>

                            <View style={{ alignItems: 'center' }}>
                                {data.length < 1 &&
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{
                                            marginTop: 10 * scale,
                                            fontStyle: 'normal',
                                            fontFamily: 'SourceSansPro-SemiBold',
                                            fontSize: 16,
                                            lineHeight: 20 * scale,
                                            textAlign: 'center',
                                            color: '#E0E0E0'
                                        }}>{convertLanguage(language, 'havent_create_ticket')}</Text>
                                    </View>
                                }

                                {
                                    checkStatusEvent() &&
                                    <View style={{ marginTop: 16 * scale, marginBottom: 10 * scale }}>
                                        <TouchableOpacity disabled={state.checkbox} onPress={() => setState({ ...state, modalCreateTicket: !state.modalCreateTicket, edit: [] })}
                                            style={{
                                                flexDirection: 'row', borderRadius: 4, justifyContent: 'center', alignItems: 'center',
                                                width: 160 * scale, height: 48 * scale, backgroundColor: state.checkbox === false ? '#00A9F4' : '#E0E2E8'
                                            }}>
                                            <Image source={require('../../assets/step3ButtonCreate.png')} style={{ marginRight: 9 * scale, tintColor: state.checkbox === false ? 'white' : '#B3B8BC' }} />
                                            <Text style={{
                                                color: state.checkbox === false ? 'white' : '#B3B8BC',
                                                fontStyle: 'normal',
                                                fontWeight: 'normal',
                                                fontSize: 16 * scale / scaleFontSize,
                                                lineHeight: 20 * scale,
                                                fontStyle: 'normal',
                                            }}>{convertLanguage(language, 'create_tickets')}</Text>
                                        </TouchableOpacity>
                                    </View>}
                                {
                                    data.length < 1 &&
                                    <View style={{ flex: 1, marginHorizontal: 16 * scale, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 15 * scale }}>
                                        <TouchableOpacity onPress={() => setState({ ...state, checkbox: !state.checkbox })} style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 27 * scale }}>
                                            <Image source={state.checkbox ? require('../../assets/step3CheckBox2.png') : require('../../assets/step3CheckBox1.png')} style={styles.ic_radio} resizeMode="contain"
                                            />
                                            <Text style={{
                                                fontStyle: 'normal',
                                                fontWeight: 'normal',
                                                fontSize: 14 * scale,
                                                lineHeight: 18 * scale,
                                                color: !validate && !state.checkbox ? '#EB5757' : '#4F4F4F'
                                            }}>{convertLanguage(language, 'checkbox_step3')}</Text>
                                        </TouchableOpacity>
                                    </View>
                                }

                                <View style={{ marginTop: 20 * scale, justifyContent: 'space-between', alignItems: 'center' }}>
                                    <TouchableOpacity style={{ marginTop: 6 * scale }}>
                                        <Text
                                            style={{ color: '#00A9F4', textDecorationLine: 'underline', fontSize: 16, lineHeight: 20, textAlign: 'center' }}>
                                            {convertLanguage(language, 'tickets_selling_terms')}
                                        </Text>
                                    </TouchableOpacity>
                                    <Text
                                        style={{
                                            fontStyle: 'normal',
                                            fontWeight: 'normal',
                                            fontSize: 12 * scale,
                                            lineHeight: 15 * scale,
                                            color: '#828282',
                                            marginVertical: 16 * scale,
                                            textAlign: 'center'
                                        }}>
                                        {convertLanguage(
                                            language,
                                            'by_creating_tickets_you_agree_to_the_terms_of_selling_tickets',
                                        )}
                                    </Text>
                                </View>

                                {
                                    props.update !== undefined ?
                                        <View style={styles.boxSubmit}>
                                            <View style={styles.containerSubmit}>
                                                <TouchableOpacity style={styles.draftActive} onPress={() => props.changeStep4()}>
                                                    <Text style={styles.txtDreftActive}>{convertLanguage(language, 'cancel')}</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={styles.saveActive} onPress={() => onUpdate()}>
                                                    <Text style={styles.txtActive} >{convertLanguage(language, 'save')}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>

                                        :

                                        <View style={styles.boxSubmit}>
                                            <View style={styles.containerSubmit}>
                                                <TouchableOpacity onPress={() => onRegister('draft')}
                                                    style={styles.draftActive}>
                                                    <Text style={styles.txtDreftActive}>{convertLanguage(language, 'save_as_draft')}</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity

                                                    style={styles.saveActive} onPress={() => onValidate() && onRegister()}>
                                                    <Text style={styles.txtActive}>{convertLanguage(language, 'save_next_step')}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>

                                }
                            </View>

                        </ScrollView>
                    </View>
                </View>
            </View>
            {
                state.modalCreateTicket &&
                <ModalCreateTicket
                    Unit={props.Unit}
                    edit={state.edit}
                    changeEdit={(index, item) => changeEdit(index, item)}
                    save={(dataTicket) => { setData([...data, dataTicket]) }}
                    timeEvent={props?.timeEvent}
                    modalVisible={state?.modalCreateTicket}
                    closeModal={() => setState({ ...state, modalCreateTicket: false })}
                    deleteTicket={() => deleteTicket(toogle?.index)}
                    is_Finish={event?.event?.is_Finish}
                    timeStart={event?.event?.TimeStart}
                    timeFinish={event?.event?.TimeFinish}
                    checkStatusEvent={checkStatusEvent()}
                    ticket={props.update !== undefined && toogle.index > 0 ? event?.event?.Tickets[toogle.index] : false}
                />
            }
            {poup}
        </>
    )
}
export default React.memo(EventCreateStep3)
const styles = StyleSheet.create({
    ic_radio: {
        width: 20 * scale,
        height: 20 * scale,
        marginRight: 10 * scale,
    },
    boxSubmit: {
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: -6,
        },
        shadowOpacity: 0.10,
        shadowRadius: 16.00,
        elevation: 30,
        marginTop: 36 * scale,
        width: width,
        paddingHorizontal: 16 * scale,
        paddingVertical: 16 * scale
    },
    containerSubmit: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    draftActive: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', borderWidth: 1, width: 114 * scale, height: 36 * scale, borderColor: '#4F4F4F' },
    txtDreftActive: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16 * scale / scaleFontSize,
        lineHeight: 20 * scale,
        color: '#4F4F4F'
    },
    draftDefault: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', marginRight: 16 * scale, width: 115 * scale, height: 36 * scale, backgroundColor: '#E0E2E8' },
    txtDreft: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 20 * scale,
        color: '#B3B8BC'
    },
    saveActive: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', width: 198 * scale, height: 36 * scale, backgroundColor: '#00A9F4' },
    txtActive: {
        color: 'white', fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16 * scale / scaleFontSize,
        lineHeight: 20 * scale,
    },
    saveDefault: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', width: 180 * scale, height: 36 * scale, backgroundColor: '#E0E2E8' },
    txtDefault: {
        color: 'white', fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 20 * scale,
        color: '#B3B8BC'
    }
})