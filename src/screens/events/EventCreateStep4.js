import moment from "moment";
import React, { useEffect, useRef, useState } from 'react';
import { ActivityIndicator, Dimensions, Image, PixelRatio, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import LinearGradient from 'react-native-linear-gradient';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { useDispatch, useSelector } from "react-redux";
import { actLoadEventStep4, actSaveEventDraft, actSaveEventStep4 } from "../../actions/event";
import Touchable from '../../screens_view/Touchable';
import { convertLanguage } from '../../services/Helper';
import "intl"
import "intl/locale-data/jsonp/vi";

const { width, height } = Dimensions.get('window')
const scale = width / 360
const LATITUDE_DELTA = 0.08;
const ASPECT_RATIO = 2;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const scaleFontSize = PixelRatio.getFontScale()

export default function EventCreateStep4(props) {
    const dispatch = useDispatch()
    const map = useRef(null)
    const webview = useRef(null)
    const event = useSelector(state => state.event)
    const { language } = useSelector(state => state.language)
    const [data, setData] = useState([]);


    useEffect(() => {
        if (props.update === undefined) {
            dispatch(actLoadEventStep4(props.HostInfoId, event.event.Id))
        }
    }, [])
    const renderLoading = () => {
        return <ActivityIndicator size="large" color="#000000" />
    }
    var FAQS = [];
    FAQS.push(
        {
            type: 'checkbox',
            name: convertLanguage(language, 'a_ticket_check-in_at_the_event'),
            data: [
                {
                    key: 'comeup-ticket',
                    value: convertLanguage(language, 'comeup_ticket')
                },
                {
                    key: 'printed-comeup-e-ticket',
                    value: convertLanguage(language, 'printed_comeup_e-ticket')
                },
                {
                    key: 'ticket-holder-name-&-phone-number',
                    value: convertLanguage(language, 'ticket_holder_name_&_phone_number')
                },
                {
                    key: 'delivered-ticket-from-this-event-host',
                    value: convertLanguage(language, 'delivered_ticket_from_this_event_host')
                },
                // {
                //     key: 'no-ticket-check-in',
                //     value: convertLanguage(language, 'no_ticket_check-in')
                // }
            ],
        })
    FAQS.push({
        type: 'checkbox',
        name: convertLanguage(language, 'b_restrictions'),
        data: [
            {
                key: 'id-card-check',
                value: convertLanguage(language, 'id_card_check')
            },
            {
                key: 'no-minor',
                value: convertLanguage(language, 'no_minor')
            },
            {
                key: 'no-carrying-food-and-beverage',
                value: convertLanguage(language, 'no_carrying_food_and_beverage')
            },
            {
                key: 'no-pet',
                value: convertLanguage(language, 'no_pet')
            },
            // {
            //     key: 'custom-restriction',
            //     value: convertLanguage(language, 'custom_restriction')
            // }
        ]
    })
    FAQS.push({
        type: 'radio',
        name: convertLanguage(language, 'c_parking') + '*',
        data: [
            {
                key: 'no-car-park',
                value: convertLanguage(language, 'no_car_park')
            },
            {
                key: 'free',
                value: convertLanguage(language, 'free')
            },
            {
                key: 'paid',
                value: convertLanguage(language, 'paid')
            }
        ]
    })
    FAQS.push({
        type: 'radio',
        name: convertLanguage(language, 'd_refund_policy') + '*',
        data: [
            {
                key: 'comeup-refund-policy',
                value: convertLanguage(language, 'comeup_refund_policy')
            },
            {
                key: 'full-refund-before-event-start',
                value: convertLanguage(language, 'full_refund_before_event_start')
            },
            {
                key: 'no-refund',
                value: convertLanguage(language, 'no_refund')
            },
            {
                key: 'host-refund-policy',
                value: convertLanguage(language, 'host_refund_policy')
            }
        ]
    })
    const inputRef = useRef([]);
    var formatter = (unit) => new Intl.NumberFormat('vi-VI', {
        style: 'currency',
        currency: unit,
    });
    if (inputRef.current.length !== data.length) {
        inputRef.current = Array(data.length).fill().map((_, i) => inputRef.current[i] || React.createRef())

    }
    const [loadingDraft, setIdLoadingDraft] = useState(false)
    const onRegister = (type = '') => {
        if (type === 'draft') {
            setIdLoadingDraft(true)
            dispatch(actSaveEventDraft(props.HostInfoId, {}, props.IdEventDtaft, event.event.Id, language))
                .then((res) => {
                    if (res && res.status === 200) {
                        setIdLoadingDraft(false)
                        // setIdEventDtaft(res.Id);
                    }
                })
            setTimeout(() => {
                setIdLoadingDraft(false)
            }, 5000);
        } else {
            dispatch(actSaveEventStep4(props.HostInfoId, event.event.Id, props.navigation, event?.event?.Slug))
        }
    }
    const [moreDetail, setMoreDetail] = useState({
        display: true,
        textDisplay: "Show more",

    });
    return (
        <>
            {
                // (event.loadingStep1 || loadingDraft) ?
                //     <Loading />
                //     :
                <>
                    <ScrollView>
                        <View style={styles.container}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingTop: props.update !== undefined ? 32 * scale : 0 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                                    <Image source={require('../../assets/basic_info.png')} style={{ marginRight: 8 * scale, width: 24 * scale, height: 24 * scale }} />
                                    <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 18, color: '#333333' }}>{convertLanguage(language, 'basic_info')}</Text>
                                </View>
                                {
                                    props.update !== undefined && event?.event?.Status !== 0 &&
                                    <TouchableOpacity onPress={() => props.changeStep1()} style={{ height: 36 * scale, width: 63 * scale, backgroundColor: '#00A9F4', borderRadius: 4, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white', fontSize: 16, fontFamily: 'SourceSansPro-SemiBold', fontStyle: 'normal' }}>{convertLanguage(language, 'btn_edit')}</Text>
                                    </TouchableOpacity>
                                }
                            </View>
                            <Image
                                source={{ uri: event?.event?.Posters?.Small }}
                                style={{ width: 328 * scale, height: 210 * scale, resizeMode: 'cover', marginVertical: 24 * scale }}
                            />
                            <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 16 }}>{event?.event?.Title}</Text>
                            <View style={{ flexDirection: 'row', flexWrap: "wrap" }}>
                                {
                                    event?.event?.HashTag !== undefined && event?.event?.HashTag.map((item, index) => {
                                        return (
                                            <Text style={{ overflow: 'hidden', paddingRight: 8 * scale, fontSize: 12 * scale, paddingTop: 4 * scale, paddingBottom: 8 * scale }} key={item.Id}>{'#' + item.HashTagName}</Text>
                                        )
                                    })
                                }
                            </View>
                            <View style={{}}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../assets/step4Clock.png')} style={{ width: 16 * scale * scaleFontSize, height: 16 * scale * scaleFontSize, marginRight: 4 * scale }} />
                                    <Text style={{ fontSize: 14, width: 308 * scale, fontFamily: 'SourceSansPro-SemiBold', color: '#4F4F4F' }}>{event?.event?.TimeStart + ' ~ ' + event?.event?.TimeFinish}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../assets/Location2.png')} style={{ width: 16 * scale * scaleFontSize, height: 16 * scale * scaleFontSize, marginRight: 4 * scale }} />
                                    <Text style={{ fontSize: 14, width: 308 * scale, fontFamily: 'SourceSansPro-SemiBold', color: '#4F4F4F' }}>{event?.event?.Address}</Text>
                                </View>

                            </View>
                            <View style={{ height: 147 * scale, marginTop: 20 * scale, backgroundColor: '#E7E7E7', position: 'relative' }}>
                                {
                                    (event?.event?.Lat !== undefined && event?.event?.Long !== undefined && event?.event?.Lat !== "" && event?.event?.Long !== "") &&
                                    <View style={{ height: 80 }} onPress={() => props.navigation.navigate('MapViews', { Lat: parseFloat(event.event.Lat), Long: parseFloat(event.event.Long) })}>
                                        <MapView
                                            provider={PROVIDER_GOOGLE}
                                            initialRegion={{
                                                latitude: parseFloat(event.event.Lat),
                                                longitude: parseFloat(event.event.Long),
                                                latitudeDelta: LATITUDE_DELTA,
                                                longitudeDelta: LONGITUDE_DELTA,
                                            }}
                                            zoomEnabled={false}
                                            enableZoomControl={false}
                                            scrollEnabled={false}
                                            style={{ width: 328 * scale, height: 147 * scale }}
                                            ref={map}
                                            liteMode={true}
                                        >
                                            <Marker
                                                coordinate={{ latitude: parseFloat(event.event.Lat), longitude: parseFloat(event.event.Long) }}
                                            // image={flagPinkImg}
                                            >
                                                <Image source={require('../../assets/pin_icon.png')} />
                                            </Marker>
                                        </MapView>
                                        <Touchable style={{ position: 'absolute' }} onPress={() => props.navigation.navigate('MapViews', { Lat: parseFloat(event.event.Lat), Long: parseFloat(event.event.Long) })}></Touchable>
                                        {/* <Image source={require('../../assets/map_view.png')} style={{ width: width - 40, height: 80 }} /> */}
                                    </View>
                                }
                            </View>
                            {
                                event?.event?.TimeRepeatArray !== undefined && event?.event?.TimeRepeatArray.length > 0 ?
                                    <View View style={{ marginTop: 24 * scale }}>
                                        <Text style={styles.titleContex}>{convertLanguage(language, 'repeat_event')}</Text>
                                        <View
                                            style={{
                                                borderBottomColor: '#00A9F4',
                                                borderBottomWidth: 1,
                                                width: width - 40 * scale,
                                                marginVertical: 20 * scale
                                            }}
                                        />
                                        {
                                            event?.event?.TimeRepeatArray.map((item, index) => {
                                                return (
                                                    <View key={index} style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 14 * scale }}>
                                                        <Text style={{ fontSize: 14 * scale }}>{item.Start + ' ~ ' + item.End}</Text>
                                                    </View>
                                                )
                                            })
                                        }
                                        <View
                                            style={{
                                                borderBottomColor: '#00A9F4',
                                                borderBottomWidth: 1,
                                                width: width - 40 * scale,
                                                marginBottom: 20 * scale
                                            }}
                                        />



                                    </View>
                                    :
                                    <View View style={{ marginTop: 24 * scale }} />
                            }


                            <View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={require('../../assets/detail_info.png')} style={{ marginRight: 12 * scale, width: 28 * scale, height: 28 * scale, tintColor: '#4F4F4F' }} />
                                        <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 18, color: '#333333' }}>{convertLanguage(language, 'detail_info_by_step')}</Text>
                                    </View>
                                    {
                                        props.update !== undefined && event?.event?.Status !== 0 &&
                                        <TouchableOpacity onPress={() => props.changeStep2()} style={{ height: 36 * scale, width: 63 * scale, backgroundColor: '#00A9F4', borderRadius: 4, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: 'white', fontSize: 16, fontFamily: 'SourceSansPro-SemiBold', fontStyle: 'normal' }}>{convertLanguage(language, 'btn_edit')}</Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                                {event?.event?.Detail !== undefined && event?.event?.Detail !== '' &&
                                    <>
                                        <Text style={[styles.titleContex, { marginTop: 24 * scale, marginBottom: 16 * scale }]}>{convertLanguage(language, 'description')}</Text>
                                        <View style={{ overflow: 'hidden', width: '100%', height: event?.event?.Detail.length > 500 && moreDetail.display ? 300 : null, marginBottom: 20 }}>
                                            <AutoHeightWebView
                                                startInLoadingState={true}
                                                scalesPageToFit={false}
                                                scrollEnabled={false}
                                                showsVerticalScrollIndicator={false}
                                                renderLoading={() => renderLoading()}
                                                style={{ flex: 1 }}
                                                source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 32}px; overflow-wrap: break-word} iframe {width: ${width - 32}px; height: ${(width - 32) * 9 / 16}px} img {width: ${width - 32}px !important; margin-bottom: 5px; margin-top: 5px; height: auto !important;}</style><body>` + event.event.Detail.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') + `</body></html>` }}
                                                originWhitelist={['*']}
                                            />
                                            {
                                                event?.event?.Detail.length > 500 && moreDetail.display &&
                                                <LinearGradient colors={['rgba(255,255,255,0)', '#FFFFFF']} style={{ height: 200, width: '100%', position: 'absolute', bottom: 0, left: 0 }}></LinearGradient>
                                            }
                                        </View>
                                        {
                                            event?.event?.Detail.length > 500 &&
                                            // moreDetail.display &&
                                            <TouchableOpacity onPress={() => setMoreDetail({ display: !moreDetail.display, textDisplay: moreDetail.display ? 'Show less' : "Show more" })} style={{ flex: 1 }} >
                                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ color: '#00A9F4', fontSize: 18, fontWeight: '600', textAlign: 'center', paddingRight: 4 }}>{moreDetail.textDisplay === 'Show more' ? convertLanguage(language, 'show_more') : convertLanguage(language, 'show_less')}</Text>
                                                    <Image source={require('../../assets/show-more.png')} style={{ width: 16, height: 16, transform: moreDetail.display ? [{ rotate: '0deg' }] : [{ rotate: '180deg' }] }} />
                                                </View>
                                            </TouchableOpacity>
                                        }


                                    </>
                                }
                                <Text style={[styles.titleContex, { marginTop: 24 * scale, marginBottom: 16 * scale, color: '#333333' }]}>{convertLanguage(language, 'faq')}</Text>
                                <View>
                                    {event?.event?.FAQ?.A !== undefined && event?.event?.FAQ?.A.length > 0 &&
                                        <>
                                            <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 18, color: '#333333' }}>{convertLanguage(language, 'a_ticket_check-in_at_the_event')}</Text>
                                            {event?.event?.FAQ.A.map((item, index) => {
                                                return <Text key={index} style={{ paddingLeft: 32 * scale, paddingTop: 14 * scale, paddingBottom: 16 * scale }}>{FAQS[0].data.filter(a => a.key === item)[0].value}</Text>

                                            })}
                                        </>
                                    }
                                    {event?.event?.FAQ?.B !== undefined && event?.event?.FAQ?.B.length > 0 &&
                                        <>
                                            <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 18, color: '#333333' }}>{convertLanguage(language, 'b_restrictions')}</Text>
                                            {event?.event?.FAQ.B.map((item, index) => {
                                                return <Text key={index} style={{ paddingLeft: 32 * scale, paddingTop: 14 * scale, paddingBottom: 16 * scale }}>{FAQS[1].data.filter(a => a.key === item)[0].value}</Text>

                                            })}
                                        </>
                                    }
                                    {event?.event?.FAQ?.C !== undefined && event?.event?.FAQ?.C.length > 0 &&
                                        <>
                                            <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 18, color: '#333333' }}>{convertLanguage(language, 'c_parking') + '*'}</Text>
                                            {event?.event?.FAQ.C.map((item, index) => {
                                                return <Text key={index} style={{ paddingLeft: 32 * scale, paddingTop: 14 * scale, paddingBottom: 16 * scale }}>{FAQS[2].data.filter(a => a.key === item)[0].value}</Text>

                                            })}
                                        </>
                                    }
                                    {event?.event?.FAQ?.D !== undefined && event?.event?.FAQ?.D.length > 0 &&
                                        <>
                                            <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 18, color: '#333333' }}>{convertLanguage(language, 'd_refund_policy') + '*'}</Text>
                                            {event?.event?.FAQ.D.map((item, index) => {
                                                return <Text key={index} style={{ paddingLeft: 32 * scale, paddingTop: 14 * scale, paddingBottom: 16 * scale }}>{FAQS[3].data.filter(a => a.key === item)[0].value}</Text>

                                            })}
                                        </>
                                    }
                                </View>
                                <View>
                                    <Text style={[styles.titleContex, { marginTop: 24 * scale, marginBottom: 16 * scale, color: '#333333' }]}>{convertLanguage(language, 'host_contact_information')}</Text>
                                    <Text style={{ fontSize: 16, color: '#333333', paddingVertical: 8 * scale }}>{convertLanguage(language, 'companyname')} : {event?.event?.HostInfo?.Name}</Text>
                                    <Text style={{ fontSize: 16, color: '#333333', paddingVertical: 8 * scale }}>{convertLanguage(language, 'phone')} : {event?.event?.HostInfo?.Phone}</Text>
                                    <Text style={{ fontSize: 16, color: '#333333', paddingVertical: 8 * scale }}>{convertLanguage(language, 'email')} : {event?.event?.HostInfo?.Email}  </Text>
                                </View>

                            </View>

                            <View style={{ paddingTop: 25, paddingBottom: 23 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', paddingBottom: 23, justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={require('../../assets/tickets_big.png')} style={{ width: 28 * scale, height: 28 * scale, tintColor: '#4F4F4F', marginRight: 8 * scale }} />
                                        <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 18, color: '#333333' }}>{convertLanguage(language, 'tickets')}</Text>
                                    </View>
                                    {
                                        props.update !== undefined && event?.event?.Status !== 0 &&
                                        <TouchableOpacity onPress={() => props.changeStep3()} style={{ height: 36 * scale, width: 63 * scale, backgroundColor: '#00A9F4', borderRadius: 4, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: 'white', fontSize: 16, fontFamily: 'SourceSansPro-SemiBold', fontStyle: 'normal' }}>{convertLanguage(language, 'btn_edit')}</Text>
                                        </TouchableOpacity>
                                    }
                                </View>

                                {
                                    event?.event?.Tickets !== undefined && event?.event?.Tickets.length > 0 &&
                                    event?.event?.Tickets.map((item, index) => {
                                        return (
                                            <React.Fragment key={index}>
                                                <View style={{ borderRadius: 5, padding: 12 * scale, backgroundColor: "#F2F2F2", marginBottom: 16 * scale, opacity: item.hide ? 0.3 : 1 }}>
                                                    <View style={{ flexDirection: 'column', flex: 1 }}>
                                                        {/* <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 18 }}>{item.Name}</Text> */}
                                                        <View style={{ flex: 1 }}>
                                                            {
                                                                item.Status === 0 && <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 16, color: 'red' }}>({convertLanguage(language, 'ticket_status')}) </Text>
                                                            }
                                                            <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 16, color: item.Status === 0 ? '#828282' : '#333333' }}>{item.Name}</Text>
                                                        </View>
                                                    </View>

                                                    <View style={{ justifyContent: 'center', opacity: item.Status === 0 ? 0.3 : 1 }}>
                                                        <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 12, color: '#828282', paddingTop: 4 * scale }}>
                                                            {/* {convertLanguage(language, 'purchasable_by')} {moment(item.TimeStart).format('HH:mm YYYY.MM.DD') + ' ~ ' + moment(item.TimeEnd).format('HH:mm YYYY.MM.DD')} */}
                                                            {convertLanguage(language, 'purchasable_by', { date1: moment(item.TimeStart).format('HH:mm YYYY.MM.DD'), date2: moment(item.TimeEnd).format('HH:mm YYYY.MM.DD') })}

                                                        </Text>
                                                        {item.Description !== '' && <Text style={{ fontWeight: 'normal', fontSize: 12, color: '#4F4F4F', marginVertical: 4 * scale }}>{item.Description}</Text>}
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingTop: 8 * scale }}>
                                                            <Text style={{ fontFamily: 'SourceSansPro-SemiBold', fontSize: 18 }}>{formatter(item.Unit).format(item.Price)}</Text>
                                                            <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
                                                                {
                                                                    item.Type === 'd-p-ticket' ?
                                                                        <Image source={require('../../assets/ic_delivery_brown.png')}
                                                                            style={{ width: 20 * scale, height: 10 * scale, marginRight: 3 * scale }} />
                                                                        :
                                                                        <Image source={require('../../assets/ic_eticket.png')}
                                                                            style={{ width: 11 * scale, height: 14 * scale, marginRight: 5 * scale }} />
                                                                }
                                                                <Text style={{
                                                                    fontStyle: 'normal',
                                                                    fontSize: 10,
                                                                }}>{item.Type === 'd-p-ticket' ? convertLanguage(language, 'delivery_ticket') : convertLanguage(language, 'e-ticket')}</Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>

                                            </React.Fragment>
                                        )
                                    })
                                }
                            </View>
                        </View >
                        {props.update === undefined &&
                            <View style={styles.boxSubmit}>
                                <View style={styles.containerSubmit}>
                                    <TouchableOpacity style={styles.draftActive} onPress={() => onRegister('draft')}>
                                        <Text style={styles.txtDreftActive}>{convertLanguage(language, 'save_as_draft')}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.saveActive} onPress={() => onRegister()}>
                                        <Text style={styles.txtActive} >{convertLanguage(language, 'register')}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }
                    </ScrollView >
                </>
            }
        </>
    )
}

const styles = StyleSheet.create({
    container: { paddingHorizontal: 16 * scale, flex: 1 },
    titleContex: { fontSize: 16, fontFamily: 'SourceSansPro-SemiBold', color: '#333333' },
    boxSubmit: {
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: -6,
        },
        shadowOpacity: 0.10,
        shadowRadius: 16.00,
        elevation: 30,
        marginTop: 36 * scale,
        width: width,
        paddingHorizontal: 16 * scale,
        paddingVertical: 16 * scale
    },
    containerSubmit: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    draftActive: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', borderWidth: 1, width: 114 * scale, height: 36 * scale, borderColor: '#4F4F4F' },
    txtDreftActive: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16 * scale / scaleFontSize,
        lineHeight: 20 * scale,
        color: '#4F4F4F'
    },
    draftDefault: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', marginRight: 16 * scale, width: 115 * scale, height: 36 * scale, backgroundColor: '#E0E2E8' },
    txtDreft: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 20 * scale,
        color: '#B3B8BC'
    },
    saveActive: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', width: 198 * scale, height: 36 * scale, backgroundColor: '#00A9F4' },
    txtActive: {
        color: 'white', fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16 * scale / scaleFontSize,
        lineHeight: 20 * scale,
    },
    saveDefault: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', width: 180 * scale, height: 36 * scale, backgroundColor: '#E0E2E8' },
    txtDefault: {
        color: 'white', fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 20 * scale,
        color: '#B3B8BC'
    }
})