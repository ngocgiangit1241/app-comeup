import moment from "moment";
import React, { Component } from 'react';
import { ActivityIndicator, Alert, Dimensions, Image, PermissionsAndroid, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import FastImage from 'react-native-fast-image';
import firebase from 'react-native-firebase';
import ImageView from 'react-native-image-view';
import LinearGradient from 'react-native-linear-gradient';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { SafeAreaConsumer } from 'react-native-safe-area-context';
import Share from 'react-native-share';
import Carousel from 'react-native-snap-carousel';
import { connect } from "react-redux";
import { actClearItemEvent, actDeleteEventNews, actLikeEvent, actLikeEventNews, actLoadDataEventDetail } from '../../actions/event';
import { actGlobalCheckLogin, actShowModalLogin } from '../../actions/global';
import { actTeamFollow } from '../../actions/team';
import ItemEventNews from '../../components/event/ItemEventNews';
import ItemEventSlide from '../../components/event/ItemEventSlide';
import ModalContact from '../../components/event/ModalContact';
import ModalEventNews from '../../components/event/ModalEventNews';
import ModalEventNewsDetail from '../../components/event/ModalEventNewsDetail';
import ModalFAQ from '../../components/event/ModalFAQ';
import ModalMore from '../../components/event/ModalMore';
import ModalReport from '../../components/event/ModalReport';
import ModalZoomImage from '../../components/event/ModalZoomImage';
import TouchableLoading from '../../components/view/TouchableLoading';
import * as Types2 from '../../constants/ActionType2';
import * as Colors from '../../constants/Colors';
import * as Config from '../../constants/Config';
import Error404 from '../../screens_view/404Error';
import Error500 from '../../screens_view/500Error';
import Line from '../../screens_view/Line';
import SrcView from '../../screens_view/ScrView';
import Touchable from '../../screens_view/Touchable';
import { convertDate3, convertLanguage, getTag } from '../../services/Helper';
import EventApplyColla from './EventApplyColla';
import ModalEventMemberLiked from './EventMemberLiked';
const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = 2;
const LATITUDE_DELTA = 0.002;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
// import { WebView } from 'react-native-webview';
// import HTMLView from 'react-native-htmlview';
// import AutoHeightWebView from '../../components/view/AutoHeightWebView'
class EventDetail extends Component {
    constructor(props) {
        super(props);
        this.srcView = React.createRef();
        this.state = {
            check2: true,
            Id: '',
            check1: false,
            checkLoad: false,
            modalContact: false,
            modelMore: false,
            modelReport: false,
            modelImage: false,
            heightImage: 0,
            modelEventNews: false,
            EventNewsId: '',
            isViewAll: true,
            zoomeImageIndex: 0,
            modalZoom: false,
            dataZoom: [],
            isOnpressLike: true,
            webviewHeight: 0,
            webviewParentHeight: 0,
            event: {},
            status_code: '',
            modalEventNewsDetail: false,
            eventnews: {},
            Role: '',
            modalEventMemberLiked: false,
            modalEventApplyColla: false,
            checkNexprop: false,
            Slug: '',
            checkHolder_list: false,
            focuse: false,
            scrolly: 0,
            scrolly2: 0
        };
    }

    // htmlContent = () => {
    //     return (
    //         `
    //         <h1>This HTML snippet is now rendered with native components !</h1>
    //         <h2>Enjoy a webview-free and blazing fast application</h2>
    //         <img src="https://i.imgur.com/dHLmxfO.jpg?2" />
    //         <em style="textAlign: center;">Look at how happy this native cat is</em>
    //     `
    //     )
    // };

    // renderNode = (node, index, siblings, parent, defaultRenderer) => {
    //     if (node.name == 'iframe') {
    //         const a = node.attribs;
    //         const iframeHtml = `<iframe src="${a.src}" frameborder="1"></iframe>`;
    //         return (
    //             <View key={index} style={{ width: 300, height: 300 }}>
    //                 <WebView
    //                     source={{ html: iframeHtml }}
    //                     // androidHardwareAccelerationDisabled={true}
    //                     // mixedContentMode='always'
    //                     // style={{ flex: -1, backgroundColor: '#444444', width: 300, height: 300 }}
    //                     startInLoadingState={true}
    //                     // injectedJavaScript={this.makeOverrideJs()}
    //                     originWhitelist={['*']}
    //                     javaScriptEnabled={true}
    //                     androidHardwareAccelerationDisabled={true}

    //                 />
    //             </View>
    //         );
    //     }
    //     if (node.name === 'img') {
    //         const data = node.attribs;
    //         return (
    //             <Image
    //                 key={index}
    //                 source={{ uri: data.src }}
    //                 resizeMode="contain"
    //                 style={{ height: 500, width: 500 }}
    //             />
    //         );

    //     }

    // }
    componentDidMount() {
        this.setState({ focuse: true })
        if (Platform.OS === 'android') {
            this._unsubscribe = this.props.navigation.addListener('focus', () => {
                // do something
                this.setState({ focuse: true })
                if (this.srcView?.current !== null) {
                    setTimeout(() => {
                        this.srcView?.current?.scrollTo({ x: 0, y: this.state.scrolly2, animated: true })
                    }, 10);
                }
            });
        }
        var { iso_code } = this.props.global;
        var notification = '';
        let Slug = '';
        if (this.props.route.params && this.props.route.params['screen'] && this.props.route.params.params) {
            notification = this.props.route.params.params['notification']
        } else {
            notification = this.props.route.params['notification']
        }
        if (this.props.route?.params?.params?.Slug !== undefined) {
            Slug = this.props.route.params.params['Slug']
            this.setState({ Slug })
        } else {
            Slug = this.props.route.params['Slug']
            this.setState({ Slug })

        }
        this.props.onLoadDataEventDetail(Slug, notification, iso_code);
        if (this.props.route.params && this.props.route.params['screen'] && this.props.route.params['screen'] === 'ticket-sales') {
            this.props.navigation.navigate('EventSaleReport', { Slug: Slug })
        }
        if (this.props.route.params && this.props.route.params['screen'] && this.props.route.params['screen'] === 'collaborates') {
            this.props.navigation.navigate('CollaboratingTeam', { Slug: Slug })
        }
        if (this.props.route.params && this.props.route.params['screen'] && this.props.route.params['screen'] === 'refund-request') {
            this.props.navigation.navigate('RefundList', { Slug: Slug })
        }
        if (this.props.route.params && this.props.route.params['screen'] && this.props.route.params['screen'] === 'holder-list') {
            this.setState({ checkHolder_list: true })
        }
    }

    refresh() {
        var { iso_code } = this.props.global;
        this.props.onLoadDataEventDetail(this.state.Slug, this.props.route.params['notification'], iso_code);
    }

    componentWillUnmount() {
        this.props.clearItemEvent()
        if (Platform.OS === 'android') {
            this._unsubscribe();
            this.setState({ focuse: false })
        }
        // this.setState({ checkLoad: false })
    }

    showDataFAQ(item) {
        var { language } = this.props.language
        switch (item) {
            case 'comeup-ticket':
                return convertLanguage(language, 'comeup_ticket');
            case 'printed-comeup-e-ticket':
                return convertLanguage(language, 'printed_comeup_e-ticket');
            case 'ticket-holder-name-&-phone-number':
                return convertLanguage(language, 'ticket_holder_name_&_phone_number');
            case 'delivered-ticket-from-this-event-host':
                return convertLanguage(language, 'delivered_ticket_from_this_event_host');
            case 'no-ticket-check-in':
                return convertLanguage(language, 'no_ticket_check-in');
            case 'id-card-check':
                return convertLanguage(language, 'id_card_check');
            case 'no-minor':
                return convertLanguage(language, 'no_minor');
            case 'no-carrying-food-and-beverage':
                return convertLanguage(language, 'no_carrying_food_and_beverage');
            case 'no-pet':
                return convertLanguage(language, 'no_pet');
            case 'custom-restriction':
                return convertLanguage(language, 'custom_restriction');
            case 'no-car-park':
                return convertLanguage(language, 'no_car_park');
            case 'free':
                return convertLanguage(language, 'free');
            case 'paid':
                return convertLanguage(language, 'paid');
            case 'comeup-refund-policy':
                return convertLanguage(language, 'comeup_refund_policy');
            case 'full-refund-before-event-start':
                return convertLanguage(language, 'full_refund_before_event_start');
            case 'no-refund':
                return convertLanguage(language, 'no_refund');
            case 'host-refund-policy':
                return convertLanguage(language, 'host_refund_policy');
            default:
                return '';
        }
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps?.event?.event?.Id !== this.state.Id) {
            if (nextProps?.event?.event?.Id !== null) {
                this.setState({ Id: nextProps?.event?.event?.Id })
            }

            if (nextProps.event.event.Posters) {
                Image.getSize(nextProps.event.event.Posters.Full, (imgWidth, imgHeight) => {
                    var heightImage2 = width * imgHeight / imgWidth;
                    this.setState({ heightImage: heightImage2 })
                });
            }
            if (nextProps.event.event.Slug == this.state.Slug || nextProps.event.event?.Id == this.state.Slug && !this.state.checkNexprop) {
                this.setState({
                    event: nextProps.event.event,
                    checkNexprop: true

                })
                firebase.analytics().setCurrentScreen(nextProps.event.event.Title, nextProps.event.event.Title);
            }

            if (nextProps.event.event.status_code) {
                this.setState({
                    status_code: nextProps.event.event.status_code
                })
            }
            if (this.state.checkHolder_list) {
                this.props.navigation.navigate('HolderList', { event: nextProps.event.event })

            }
        } else {
        }
    }

    onEditEvent(Id) {
        this.setState({ modelMore: false });
        this.props.navigation.navigate('CreateEvent', { EventId: Id })
    }
    onEditEvent2(Id, Slug, TeamId) {
        this.setState({ checkLoad: false })
        this.props.navigation.navigate('CreateEventStep1', { EventId: Id, Slug, HostInfoId: TeamId })
        this.setState({ modelMore: false });
    }
    convertDateToMoment(dateString) {
        var datetime = dateString.split(" ");
        var date = datetime[0].split("-");
        var time = datetime[1].split(":");
        return moment([date[0], date[1] - 1, date[2], time[0], time[1]]);
    }

    onDeleteEvent(event) {
        var { language } = this.props.language
        this.setState({ modelMore: false });
        var datetimenow = moment().format('YYYY-MM-DD HH:mm')
        if (moment(event.TimeFinish).isSameOrAfter(datetimenow)) {
            if (event.Status == 0) {
                var count_days = moment().diff(this.convertDateToMoment(event.CanceledAt), 'days');
                if (count_days >= 7) {
                    this.props.navigation.navigate('DeleteEvent', { event })
                } else {
                    setTimeout(() => {
                        Alert.alert(
                            convertLanguage(language, 'warning'),
                            // 'You can delete this event after ' + (7 - count_days) + ' days!',
                            convertLanguage(language, 'you_can_delete_this_event_after_7_days', { day: 7 - count_days }),
                            [
                                {
                                    text: convertLanguage(language, 'ok'),
                                    onPress: () => console.log('Cancel Pressed'),
                                    style: 'cancel',
                                },
                            ],
                            { cancelable: false },
                        );
                    }, 100);

                }
            } else {
                this.props.navigation.navigate('CancelEvent', { event })
            }
        } else {
            this.props.navigation.navigate('DeleteEvent', { event })
        }
    }

    onShowCollaborating() {
        var { event } = this.state;
        this.setState({ modelMore: false });
        this.props.navigation.navigate('CollaboratingTeam', { Slug: event.Slug })
    }

    scanTicket() {
        this.setState({ modelMore: false });
        var { event } = this.state;
        this.props.navigation.navigate('EventScanTicket', { event: event })
    }

    refundList() {
        var { event } = this.state;
        this.setState({ modelMore: false });
        this.props.navigation.navigate('RefundList', { Slug: event.Slug })
    }
    withdrawRequest() {
        var { event } = this.state;
        this.setState({ modelMore: false });
        this.props.navigation.navigate('WithdrawRequest', { event: event })
    }
    eventStaff() {
        var { event } = this.state;
        this.setState({ modelMore: false });
        this.props.navigation.navigate('EventStaff', { Slug: event.Slug })
    }
    eventSaleReport() {
        var { event } = this.state;
        this.setState({ modelMore: false });
        this.props.navigation.navigate('EventSaleReport', { Slug: event.Slug })
    }
    holderList() {
        var { event } = this.state;
        this.setState({ modelMore: false });
        this.props.navigation.navigate('HolderList', { event: event })
    }

    share() {
        var { event } = this.state;
        var { language } = this.props.language;
        // console.log('getTag(event.HashTag)', getTag(event.HashTag));

        let options = {
            title: event.Title,
            message: event.Title + `\n${convertLanguage(language, 'find_and_join_event_in_comeup')}\n` + getTag(event.HashTag),
            url: Config.MAIN_URL + 'events/' + event.Slug,
            subject: "Jois with us" //  for email
        };
        Share.open(options)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }

    showModelReport() {
        this.setState({
            modelMore: false,
            modalContact: true,
        })
    }

    onReport() {
        var { event } = this.state;
        this.setState({
            modelMore: false,
            modalContact: false,
        })
        this.props.globalCheckLogin('EventReport', { Slug: event.Slug, Title: event.Title })
    }

    onLikeEventNews(EventNewsId) {
        this.props.onLikeEventNews(EventNewsId);
    }

    onOpenModelEventNews(Id) {
        this.setState({
            modelEventNews: true,
            EventNewsId: Id,
        })
    }

    onEditEventNews() {
        var { EventNewsId } = this.state;
        this.setState({
            modelEventNews: false,
        })
        this.props.navigation.navigate('WriteEventNews', { Id: EventNewsId })
    }

    onDeleteEventNews() {
        var { language } = this.props.language
        var { EventNewsId } = this.state;
        setTimeout(() => {
            Alert.alert(
                convertLanguage(language, 'delete_event_news'),
                convertLanguage(language, 'are_you_sure_delete_event_news'),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'destructive',
                    },
                    { text: 'OK', onPress: () => { this.setState({ modalEventNewsDetail: false, modelEventNews: false, eventnews: {} }); this.props.onDeleteEventNews(EventNewsId) } },
                ],
                { cancelable: false },
            );
        }, 100)
    }
    _onMapReady() {
        if (Platform.OS === 'android') {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                .then(granted => {
                    // this.setState({ paddingTop: 0 });
                });
        }
    }
    onReportEventNews() {
        var { EventNewsId } = this.state;
        this.setState({
            modelEventNews: false,
        })
        this.props.navigation.navigate('EventNewsReport', { Id: EventNewsId })
    }

    onLikeEvent(Id) {
        var { event } = this.state;
        if (this.props.event.event.Slug !== this.state.Slug) {
            event.Like = !event.Like;
        }
        this.setState({
            isOnpressLike: false,
            event
        })
        this.props.onLikeEvent(Id, this.props.user.profile);
        setTimeout(() => {
            this.setState({
                isOnpressLike: true
            })
        }, 2000)
    }

    onGetEventByTag(Tag) {
        var data = {
            name: Tag.HashTagName,
            value: Tag.Id
        }
        this.props.navigation.navigate('EventList', { tag: data });
    }

    renderLoading() {
        return <ActivityIndicator size="large" color="#000000" />
    }

    onLoadEnd() {
        setTimeout(() => {
            this.refs.content.measureInWindow((ox, oy, width, height) => {
                this.setState({
                    webviewHeight: height > 300 ? 300 : height,
                    isViewAll: height > 300 ? false : true,
                    webviewParentHeight: height > 300 ? 301 : 0,
                })
            });
        }, 100);
    }

    onOpenModelEventNewsDetail(item, Role) {
        item.Event = this.state.event;
        this.setState({
            modalEventNewsDetail: true,
            eventnews: item,
            Role: Role
        })
    }

    closeModalEventNewsDetail = () => {
        this.setState({ modalEventNewsDetail: false, eventnews: {} })
    }

    onShowModalTeam() {
        var { profile } = this.props.user;
        if (profile.Id) {
            this.setState({ modalEventApplyColla: true })
        } else {
            this.props.onShowModalLogin();
        }
        // this.props.globalCheckLogin('EventApplyColla', { Slug: event.Slug })
    }

    goBack = () => {
        var { index, routes } = this.props.navigation.dangerouslyGetState();
        if (routes.length > 1 && ['Login', 'LoginWithMail', 'ForgotPw', 'Splash'].includes(routes[index - 1].name)) {
            this.props.navigation.reset({ index: 0, routes: [{ name: 'Main' }], })
        } else {
            this.setState({ checkLoad: false })
            this.props.navigation.goBack();
        }
    }
    stopWebView = async (event) => {
        if (Platform.OS === "android") {
            await this.webview?.stopLoading();
            await this.webview?.reload()
            this.setState({ focuse: false })
        }
        await this.props.navigation.navigate('PurchaseTicket', { Slug: event.Slug, callBack: () => this.callBackTicket() })
    }
    callBackTicket = async () => {
        if (Platform.OS === "android") {
            await this.webview?.reload()
            this.setState({ focuse: true })
        }


    }
    render() {
        var { event, status_code, modalEventMemberLiked, modalEventApplyColla } = this.state;
        var { loadingEventDetail } = this.props.event;
        var { modelImage, heightImage, isViewAll } = this.state;
        var disabled = false;
        if (event.Team) {
            disabled = this.props.team.teamIdAction == event.Team.Id ? this.props.team.loadingFollowers : false;
        }
        var { language } = this.props.language;
        // console.log('event', event.is_Finish);
        return (

            <>
                <View style={{ backgroundColor: Colors.BG, flex: 1 }}>
                    <SafeAreaConsumer>
                        {insets => <View style={{ paddingTop: insets.top }} />}
                    </SafeAreaConsumer>
                    <View style={{ backgroundColor: Colors.BG, flex: 1 }}>
                        <View style={{}}>
                            <Touchable
                                onPress={() => this.goBack()}
                                style={{
                                    minWidth: 48, minHeight: 48, alignSelf: 'flex-start',
                                    justifyContent: 'center', alignItems: 'center'
                                }}>
                                <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                            </Touchable>
                            {
                                event?.Id && status_code !== 404 && status_code !== 500 &&
                                <>
                                    <Touchable
                                        onPress={() => event?.Role !== 'Guest' ? this.setState({ modelMore: true }) : this.setState({ modalContact: true })}
                                        style={{
                                            minWidth: 40, minHeight: 50,
                                            justifyContent: 'center', alignItems: 'center',
                                            position: 'absolute', right: 0, top: 0
                                        }}>
                                        <Image source={event.Role !== 'Guest' ? require('../../assets/more_horizontal.png') : require('../../assets/report.png')} style={{ width: 30, height: 30, marginRight: 14 }} />
                                    </Touchable>
                                    <Touchable
                                        onPress={() => { this.share() }}
                                        style={{
                                            minWidth: 40, minHeight: 50,
                                            justifyContent: 'center', alignItems: 'center',
                                            position: 'absolute', right: 45, top: 0
                                        }}>
                                        <Image source={require('../../assets/share_2.png')} style={{ width: 30, height: 30 }} />
                                    </Touchable>
                                    {
                                        event.Role === 'Guest' &&
                                        <Touchable
                                            onPress={() => this.onLikeEvent(event?.Id)}
                                            disabled={!this.state.isOnpressLike}
                                            style={{
                                                minWidth: 50, minHeight: 50,
                                                justifyContent: 'center', alignItems: 'center',
                                                position: 'absolute', right: 80, top: 0
                                            }}>
                                            <Image source={event.Like ? require('../../assets/liked_red.png') : require('../../assets/like_border_black.png')} style={{ width: 30, height: 30 }} resizeMode="contain" />
                                        </Touchable>
                                    }
                                </>
                            }
                        </View>
                        <Line />
                        {
                            loadingEventDetail &&
                            <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                        }
                        <View style={{ opacity: loadingEventDetail ? 0 : 1, flex: 1 }}>
                            {
                                status_code === 404
                                    ?
                                    <Error404 onPress={() => this.props.navigation.goBack()} />
                                    :
                                    status_code === 500
                                        ?
                                        <Error500 onPress={() => this.refresh()} />
                                        :
                                        <>
                                            <SrcView scrollEventThrottle={16} style={styles.container} refs={this.srcView} onScroll={(event) => { this.setState({ scrolly2: this.state.scrolly, scrolly: event.nativeEvent.contentOffset.y }) }}>
                                                <TouchableOpacity onPress={() => this.setState({ modelImage: true })} activeOpacity={1}>
                                                    <FastImage style={styles.boxImageThumb} source={{ uri: event.Posters ? event.Posters.Medium : event.Poster }} />
                                                </TouchableOpacity>
                                                {
                                                    event.Status == 0
                                                    &&
                                                    <>
                                                        <View style={styles.imgCancel} >
                                                            <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#ff4081', height: 60, width: '115%', transform: [{ rotateZ: '-0.3rad' }] }}>
                                                                <Text style={{ textAlign: 'center', color: '#FFFF', fontSize: 30, fontWeight: 'bold' }}>{convertLanguage(language, 'canceled_event')}</Text>
                                                            </View>
                                                        </View>
                                                    </>
                                                }
                                                <View style={styles.content}>
                                                    <Text style={styles.txtNameEvent}>{event.Title}</Text>
                                                    <Text style={[styles.txtCategory, { fontWeight: 'normal', flexDirection: 'row', fontSize: 14, color: '#333333', }]}>
                                                        {
                                                            event.HashTag &&
                                                            event.HashTag.map((Tag, index) => {
                                                                return <Text key={index} onPress={() => this.onGetEventByTag(Tag)} style={{ fontSize: 14, fontWeight: '400' }}>
                                                                    #{Tag.HashTagName + '   '}
                                                                </Text>
                                                            })
                                                        }
                                                    </Text>
                                                    <Text style={[styles.txtCategory]}>{convertDate3(event.TimeStart, event.TimeFinish, language)}</Text>
                                                    <Text style={[styles.txtCategory]}>{event.VenueName} - {event.Address}</Text>
                                                    {
                                                        event?.Lat !== undefined && event?.Long !== undefined && event?.Lat !== "" && event?.Long !== "" &&
                                                        <View style={{ marginTop: 15 }}>
                                                            <MapView
                                                                provider={PROVIDER_GOOGLE}
                                                                initialRegion={{
                                                                    latitude: parseFloat(event?.Lat),
                                                                    longitude: parseFloat(event?.Long),
                                                                    latitudeDelta: LATITUDE_DELTA,
                                                                    longitudeDelta: LONGITUDE_DELTA,
                                                                }}
                                                                // zoomEnabled={true}
                                                                // enableZoomControl={true}
                                                                style={{ width: width - 30, height: 80 }}
                                                                ref={ref => this.map = ref}
                                                                liteMode={true}
                                                                androidHardwareAccelerationDisabled={true}

                                                            // onMapReady={() => this._onMapReady()}
                                                            >
                                                                <Marker
                                                                    coordinate={{ latitude: parseFloat(event?.Lat), longitude: parseFloat(event?.Long) }}
                                                                // image={flagPinkImg}
                                                                >
                                                                    <Image source={require('../../assets/pin_icon.png')} />
                                                                </Marker>
                                                            </MapView>
                                                            {/* <Image source={require('../../assets/map_view.png')} style={{ width: width - 40, height: 80 }} /> */}

                                                            <TouchableOpacity style={{ width: width - 40, height: 80, position: 'absolute' }} onPress={() => { this.props.navigation.navigate('MapViews', { Lat: parseFloat(event.Lat), Long: parseFloat(event.Long) }) }}></TouchableOpacity>
                                                        </View>
                                                    }
                                                    <View style={styles.boxLiked}>
                                                        <View style={styles.boxLikeLeft}>
                                                            <Touchable onPress={() => this.onLikeEvent(event?.Id)} disabled={!this.state.isOnpressLike || event.Role !== 'Guest'}>
                                                                <Image source={event?.Like ? require('../../assets/liked_red.png') : require('../../assets/like_border_black.png')} style={styles.icLike} />
                                                            </Touchable>
                                                            <Touchable style={{ flex: 1 }} onPress={() => this.setState({ modalEventMemberLiked: true })}>
                                                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ flex: 1 }}>
                                                                    {
                                                                        (event?.ListLikes && event?.ListLikes.length > 0) &&
                                                                        event?.ListLikes.map((Like, index) => {
                                                                            return <Image key={index} source={{ uri: Like.Avatars.Small + '' }} style={styles.avatar} />
                                                                        })
                                                                    }
                                                                </ScrollView>
                                                            </Touchable>
                                                        </View>
                                                        {/* {
                                                            (event?.ListLikes && event?.ListLikes.length * 54 > width - 77) &&
                                                            <Touchable style={{ padding: 10, paddingRight: 0 }} onPress={() => this.setState({ modalEventMemberLiked: true })}>
                                                                <Image source={require('../../assets/like_pop_icon.png')} style={styles.icLikePop} />
                                                            </Touchable>
                                                        } */}
                                                    </View>

                                                    <View style={styles.boxDetail}>
                                                        {/* <View ref="content" style={isViewAll ? [{ overflow: 'hidden' }, this.state.webviewHeight > 0 ? { height: this.state.webviewHeight } : {}] : [{ overflow: 'hidden', width: '100%' }, this.state.webviewParentHeight > 0 ? { height: this.state.webviewParentHeight } : {}]}> */}
                                                        <View>
                                                            {
                                                                typeof event.Detail != 'undefined' && event.Detail !== '' &&
                                                                // <AutoHeightWebView startInLoadingState={true} scalesPageToFit={false} zoomable={false} scrollEnabled={false} renderLoading={() => this.renderLoading()} onLoadEnd={() => this.onLoadEnd()} width={width - 40} source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 40}px; overflow-wrap: break-word} iframe {width: ${width - 40}px; height: ${(width - 40) * 9 / 16}px} img {width: ${width - 40}px !important; margin-bottom: 5px; margin-top: 5px}</style><body>` + event.Detail.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') + `</body></html>` }} originWhitelist={['*']} />
                                                                <View style={{ flex: 1 }}>
                                                                    {this.state.focuse && <AutoHeightWebView
                                                                        style={{ overflow: 'hidden' }}
                                                                        // style={{ flex: 1, minHeight: 200, height: 500, opacity: 0.99 }}
                                                                        // mediaPlaybackRequiresUserAction={true}
                                                                        // automaticallyAdjustContentInsets={false}
                                                                        startInLoadingState={true}
                                                                        scalesPageToFit={true}
                                                                        zoomable={false}
                                                                        scrollEnabled={false}
                                                                        renderLoading={() => this.renderLoading()}
                                                                        ref={ref => this.webview = ref}
                                                                        // onNavigationStateChange={(event) => {
                                                                        //     console.log('aa', event)
                                                                        //     this.webview.stopLoading();
                                                                        // }}
                                                                        // onError={() => console.log('on error')}
                                                                        // onLoad={() => console.log('on load')}
                                                                        // onLoadStart={() => this.setState({ check2: true })}
                                                                        // onLoadEnd={() => this.setState({ check2: false })}
                                                                        // useWebkit={true}
                                                                        // width={width - 32}
                                                                        source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 32}px; overflow-wrap: break-word} iframe {width: ${width - 32}px; height: ${(width - 32) * 9 / 16}px} img {width: ${width - 32}px !important; margin-bottom: 5px; margin-top: 5px; height: auto !important;}</style><body>` + event.Detail.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') + `</body></html>` }}
                                                                        // source={{ html: event.Detail }}
                                                                        originWhitelist={['*']}
                                                                    // androidHardwareAccelerationDisabled={true}
                                                                    // cacheMode={'LOAD_CACHE_ELSE_NETWORK'}

                                                                    />}

                                                                </View >
                                                            }

                                                            {
                                                                (event.FAQ && ((event.FAQ['A'] && event.FAQ['A'].length > 0) || (event.FAQ['B'] && event.FAQ['B'].length > 0) || (event.FAQ['C'] && event.FAQ['C'].length > 0) || (event.FAQ['D'] && event.FAQ['D'].length > 0))) &&
                                                                <React.Fragment>
                                                                    <Text style={styles.txtFAQ}>{convertLanguage(language, 'faq')}</Text>
                                                                    {
                                                                        event.FAQ['A'] && event.FAQ['A'].length > 0
                                                                        &&
                                                                        <React.Fragment>
                                                                            <Text style={styles.txtLable}>{convertLanguage(language, 'a_ticket_check-in_at_the_event')}</Text>
                                                                            {
                                                                                event.FAQ['A'].map((item, index) => {
                                                                                    return <Text style={styles.value} key={index}>
                                                                                        {
                                                                                            '- ' + this.showDataFAQ(item)
                                                                                        }
                                                                                    </Text>
                                                                                })
                                                                            }
                                                                        </React.Fragment>
                                                                    }
                                                                    {
                                                                        event.FAQ['B'] && event.FAQ['B'].length > 0
                                                                        &&
                                                                        <React.Fragment>
                                                                            <Text style={styles.txtLable}>{convertLanguage(language, 'b_restrictions')}</Text>
                                                                            {
                                                                                event.FAQ['B'].map((item, index) => {
                                                                                    return <Text style={styles.value} key={index}>
                                                                                        {
                                                                                            '- ' + this.showDataFAQ(item)
                                                                                        }
                                                                                    </Text>
                                                                                })
                                                                            }
                                                                        </React.Fragment>
                                                                    }
                                                                    {
                                                                        event.FAQ['C'] && event.FAQ['C'].length > 0
                                                                        &&
                                                                        <React.Fragment>
                                                                            <Text style={styles.txtLable}>{convertLanguage(language, 'c_parking')}</Text>
                                                                            {
                                                                                event.FAQ['C'].map((item, index) => {
                                                                                    return <Text style={styles.value} key={index}>
                                                                                        {
                                                                                            '- ' + this.showDataFAQ(item)
                                                                                        }
                                                                                    </Text>
                                                                                })
                                                                            }
                                                                        </React.Fragment>
                                                                    }
                                                                    {
                                                                        event.FAQ['D'] && event.FAQ['D'].length > 0
                                                                        &&
                                                                        <React.Fragment>
                                                                            <Text style={styles.txtLable}>{convertLanguage(language, 'd_refund_policy')}</Text>
                                                                            {
                                                                                event.FAQ['D'].map((item, index) => {
                                                                                    return <Text style={styles.value} key={index}>
                                                                                        {
                                                                                            '- ' + this.showDataFAQ(item)
                                                                                        }
                                                                                    </Text>
                                                                                })
                                                                            }
                                                                        </React.Fragment>
                                                                    }
                                                                </React.Fragment>
                                                            }
                                                            {
                                                                !isViewAll &&
                                                                <LinearGradient colors={['rgba(255,255,255,0)', '#FFFFFF']} style={{ height: 50, width: '100%', position: 'absolute', bottom: 0, left: 0 }}></LinearGradient>
                                                            }
                                                        </View>
                                                        {
                                                            !isViewAll &&
                                                            <Touchable style={styles.btnReadAllNew} onPress={() => this.setState({ modalFAQ: true })}>
                                                                <Text style={styles.txtReadAllNew}>+ {convertLanguage(language, 'read_more')}</Text>
                                                            </Touchable>
                                                        }
                                                    </View>

                                                    {
                                                        (event.Role !== 'Guest' && event.Role !== 'Staff') &&
                                                        <View style={styles.boxEventNews}>
                                                            <Text style={styles.txtEventNews}>{convertLanguage(language, 'event_news')}</Text>
                                                            <Touchable style={styles.btnWrite} onPress={() => this.props.navigation.navigate('WriteEventNews', { Slug: event.Slug })}>
                                                                <Text style={styles.txtWrite}>{convertLanguage(language, 'write')}</Text>
                                                            </Touchable>
                                                        </View>
                                                    }
                                                    {
                                                        event.EventNews && event.EventNews.length > 0
                                                        &&
                                                        <View style={styles.boxNews}>
                                                            {
                                                                event.EventNews.map((item, index) => {
                                                                    return <ItemEventNews
                                                                        data={item}
                                                                        key={index}
                                                                        onDeleteEventNews={(Id) => this.props.onDeleteEventNews(Id)}
                                                                        zoomImage={(index, dataZoom) => this.setState({ zoomeImageIndex: index, modalZoom: true, dataZoom: dataZoom })}
                                                                        Role={event.Role} Poster={event.Posters ? event.Posters.Medium : event.Poster}
                                                                        Title={event.Title}
                                                                        navigation={this.props.navigation}
                                                                        onLikeEventNews={(Id) => this.onLikeEventNews(Id)}
                                                                        share={() => this.share()}
                                                                        onOpenModelEventNews={(Id) => this.onOpenModelEventNews(Id)}
                                                                        onOpenModelEventNewsDetail={() => this.onOpenModelEventNewsDetail(item, event.Role)} />
                                                                })
                                                            }
                                                            <Touchable style={styles.btnReadAllNew} onPress={() => this.props.navigation.navigate('EventNews', { Slug: event.Slug, Event: event })}>
                                                                <Text style={styles.txtReadAllNew}>{convertLanguage(language, 'read_all_news')} </Text>
                                                                <Image source={require('../../assets/right_blue.png')} style={{ width: 12, height: 12, marginTop: 3 }} resizeMode="contain" />
                                                            </Touchable>
                                                        </View>
                                                    }
                                                    {
                                                        event.Team &&
                                                        <View style={styles.boxHostTeam}>
                                                            <Text style={[styles.txtEventNews, { fontSize: 16 }]}>{convertLanguage(language, 'host_team')}</Text>
                                                            <Touchable style={styles.boxInfo} onPress={() => this.props.navigation.navigate({ name: 'DetailTeam', params: { id: event.Team.Id }, key: event.Team.Id })}>
                                                                <Image source={{ uri: event.Team.Logos ? event.Team.Logos.Small : '' }} style={styles.imgHostTeam} />
                                                                <View style={styles.boxInfoRight}>
                                                                    <View style={styles.boxEventMoreLeft}>
                                                                        <Text style={[styles.txtNameInfo, { paddingBottom: 3 }]}>{event.Team.Name}</Text>
                                                                        {
                                                                            event.Team.Verify == 1 &&
                                                                            <Image style={styles.icCertified} source={require('../../assets/team_certified.png')} />
                                                                        }
                                                                    </View>
                                                                    <Text style={[styles.txtDetail, { paddingBottom: 3 }]}>({event.Team.Code})</Text>
                                                                    <Text style={[styles.txtCategory, { paddingBottom: 3, fontWeight: '600' }]}>
                                                                        {
                                                                            event.Team.HashTag &&
                                                                            event.Team.HashTag.map((Tag, index) => {
                                                                                return <React.Fragment key={index}>
                                                                                    #{Tag.HashTagName + ' '}
                                                                                </React.Fragment>
                                                                            })
                                                                        }
                                                                    </Text>
                                                                    <View style={[styles.boxEventActions, { marginTop: 5 }]}>
                                                                        {
                                                                            event.Team.Role === 'Guest'
                                                                                ?
                                                                                <TouchableLoading activeOpacity={0.7} style={event.Team.IsFollow ? styles.btnFollowing : styles.btnFollow} onPress={() => this.props.followTeam(event.Team.Id)} disabled={disabled} loading={disabled}>
                                                                                    <Text style={event.Team.IsFollow ? styles.txtFollowing : styles.txtFollow}>{event.Team.IsFollow ? convertLanguage(language, 'following') : convertLanguage(language, 'follow')}</Text>
                                                                                </TouchableLoading>
                                                                                :
                                                                                <View style={{ flex: 0.6 }}></View>
                                                                        }
                                                                        <Touchable style={styles.btnContact} onPress={() => this.props.navigation.navigate({ name: 'DetailTeam', params: { id: event.Team.Id }, key: event.Team.Id })}>
                                                                            <Text style={styles.txtContact}>{convertLanguage(language, 'contacts_teampage')}</Text>
                                                                        </Touchable>
                                                                    </View>
                                                                </View>
                                                            </Touchable>

                                                        </View>
                                                    }
                                                    {/* <View style={styles.boxHostTeam}>
                                                <Text style={styles.txtEventNews}>Venue</Text>
                                                <View style={styles.boxInfo}>
                                                    <Image source={{ uri: 'https://comeup.techup.vn/uploads/teams/8ff7e5beca36a75e8558dab42148057c/171_CqageR_200x200.png' }} style={styles.imgVenue} />
                                                    <View style={styles.boxInfoRight}>
                                                        <Text style={[styles.txtNameInfo, { paddingBottom: 3 }]}>Seoul Pub Crawl</Text>
                                                        <Text style={[styles.txtDetail, { paddingBottom: 3 }]}>(seoulpc1)</Text>
                                                        <Text style={[styles.txtCategory, { paddingBottom: 3 }]}>#Gathering  #Party</Text>
                                                        <View style={styles.boxEventMoreLeft}>
                                                            <Image source={require('../../assets/review_star_blue.png')} style={styles.icStar} />
                                                            <Image source={require('../../assets/review_star_blue.png')} style={styles.icStar} />
                                                            <Image source={require('../../assets/review_star_blue.png')} style={styles.icStar} />
                                                            <Image source={require('../../assets/review_star_gry.png')} style={styles.icStar} />
                                                            <Image source={require('../../assets/review_star_gry.png')} style={styles.icStar} />
                                                            <Text style={[styles.txtNameInfo, { paddingBottom: 0 }]}>294</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View> */}
                                                    <View style={styles.boxHostTeam}>
                                                        <View style={styles.boxCollaboration}>
                                                            <Text style={[styles.txtEventNews, { paddingBottom: 0 }]}>{convertLanguage(language, 'collaboration_team')}</Text>
                                                            {
                                                                // (event.Role === 'Leader' || event.Role === 'Director') &&
                                                                // <Touchable onPress={() => this.props.navigation.navigate('CollaboratingTeam', { Slug: event.Slug })}
                                                                //     style={{
                                                                //         minWidth: 40, minHeight: 50,
                                                                //         justifyContent: 'center', alignItems: 'flex-end',
                                                                //     }}>
                                                                //     <Image source={require('../../assets/more_1_top.png')} />
                                                                // </Touchable>
                                                            }
                                                        </View>
                                                        {
                                                            event.CollaborationTeams && event.CollaborationTeams.length > 0
                                                            &&
                                                            event.CollaborationTeams.map((item, index) => {
                                                                var disabled2 = this.props.team.teamIdAction == item.Id ? this.props.team.loadingFollowers : false;
                                                                return <Touchable style={styles.boxEventMore} key={index} onPress={() => this.props.navigation.navigate({ name: 'DetailTeam', params: { id: item.Id }, key: item.Id })}>
                                                                    <View style={[styles.boxEventActions, { flex: 1 }]}>
                                                                        <Image source={{ uri: item.Logos.Small + '' }} style={styles.imgCollaboration} />
                                                                        <View style={styles.boxInfoRight}>
                                                                            <Text style={[styles.txtNameInfo, { paddingBottom: 3 }]} numberOfLines={2}>{item.Name}</Text>
                                                                            <Text style={[styles.txtDetail, { paddingBottom: 3 }]} numberOfLines={2}>({item.Code})</Text>
                                                                            <Text style={[styles.txtCategory, { paddingBottom: 3 }]} numberOfLines={2}>
                                                                                {
                                                                                    item.HashTagToEvent
                                                                                }
                                                                            </Text>
                                                                        </View>
                                                                    </View>
                                                                    {
                                                                        item.Role === 'Guest' &&
                                                                        <TouchableLoading style={item.IsFollow ? styles.btnFollowingCollaboration : styles.btnFollowCollaboration} onPress={() => this.props.followTeam(item.Id)} disabled={disabled2} loading={disabled2}>
                                                                            <Text numberOfLines={1} style={item.IsFollow ? styles.txtFollowing : styles.txtFollow}>{item.IsFollow ? convertLanguage(language, 'following') : convertLanguage(language, 'follow')}</Text>
                                                                        </TouchableLoading>
                                                                    }
                                                                </Touchable>
                                                            })
                                                        }
                                                        <Touchable style={styles.boxItemList} onPress={() => this.onShowModalTeam()}>
                                                            <View style={styles.boxEventActions}>
                                                                <Image source={require('../../assets/icon-add-team.png')} style={[styles.imgCollaboration, { backgroundColor: 'transparent', }]} />
                                                                <View style={styles.boxContent}>
                                                                    <Text style={styles.txtAddNew}>{convertLanguage(language, 'apply_collaborate')}</Text>
                                                                </View>
                                                            </View>
                                                        </Touchable>
                                                    </View>
                                                </View>
                                                {
                                                    (event.RelatedEvent && event.RelatedEvent.length > 0) &&
                                                    <View style={styles.boxRelated}>
                                                        <View style={[styles.boxEventMore, { paddingLeft: 20, paddingRight: 20 }]}>
                                                            <Text style={[styles.txtEventNews, { paddingBottom: 0 }]}>{convertLanguage(language, 'related_event')}</Text>
                                                            <Image source={require('../../assets/more3.png')} />
                                                        </View>
                                                        <View style={styles.boxListEvent}>
                                                            <Carousel
                                                                data={event.RelatedEvent}
                                                                renderItem={(item, index) => { return <ItemEventSlide cancel_text={convertLanguage(language, 'canceled_event')} from={'detail'} data={item} key={index} onLikeEvent={(Id) => this.props.onLikeEvent(Id)} navigation={this.props.navigation} language={language} /> }}
                                                                sliderWidth={width}
                                                                itemWidth={width - 26}
                                                                inactiveSlideScale={1}
                                                            // activeSlideAlignment={'start'}
                                                            />
                                                        </View>
                                                    </View>
                                                }
                                                {
                                                    event.Status != 0 &&
                                                    <SafeAreaConsumer>
                                                        {insets => <View style={{ height: insets.bottom + 62 }} />}
                                                    </SafeAreaConsumer>
                                                }
                                            </SrcView>
                                            {
                                                event.Status != 0 &&
                                                <SafeAreaConsumer>
                                                    {insets => <View style={[styles.boxTicket, { bottom: insets.bottom }]}>
                                                        <Touchable
                                                            style={[styles.btnTicket, event.is_Finish ? { backgroundColor: '#f2f2f2' } : {}]} onPress={() => {
                                                                // firebase.analytics().logEvent("book_ticket", { Title: event.Title });
                                                                // this.setState({ checkLoad: false })
                                                                // this.webview.stopLoading();
                                                                // this.props.navigation.navigate('PurchaseTicket', { Slug: event.Slug })
                                                                // this.props.onClearEvent()
                                                                this?.stopWebView(event)

                                                            }}
                                                            disabled={event.is_Finish}
                                                        >
                                                            <View style={styles.boxEventMoreLeft}>
                                                                {!event.is_Finish && <Image source={require('../../assets/ticket_white.png')} style={{ marginRight: 10, width: 30, height: 30 }} />}
                                                                <Text style={[styles.txtFollow, { fontWeight: 'bold', fontSize: 17 }, event.is_Finish ? { color: '#BDBDBD' } : {}]}>

                                                                    {convertLanguage(language, event.is_Finish ? 'event_finish' : 'book_tickets')}
                                                                </Text>
                                                            </View>
                                                        </Touchable>
                                                    </View>
                                                    }
                                                </SafeAreaConsumer>
                                            }
                                        </>
                            }
                        </View>
                        {
                            this.state.modalContact
                                ?
                                <ModalContact
                                    modalVisible={this.state.modalContact}
                                    closeModal={() => this.setState({ modalContact: false })}
                                    onReport={() => this.onReport()}
                                    team={{
                                        name: event.HostInfo.Name,
                                        ceo: event.HostInfo.Ceo,
                                        business_number: event.HostInfo.BusinessRegistrationNumber,
                                        Address: event.HostInfo.Address,
                                        person_charge: event.HostInfo.ThePersonInCharge,
                                        Phone: event.HostInfo.Phone,
                                        Email: event.HostInfo.Email,
                                        Code: event.Team.Code,
                                        TeamName: event.Team.Name,
                                    }}
                                />
                                : null
                        }
                        {
                            this.state.modelMore &&
                            <ModalMore closeModal={() => this.setState({ modelMore: false })}
                                showModelReport={() => this.showModelReport()}
                                edit={() => this.onEditEvent(event?.Id)}
                                edit2={() => this.onEditEvent2(event?.Id, this.state.event.Slug, event?.TeamId)}
                                delete={() => this.onDeleteEvent(event)}
                                collaborating={() => this.onShowCollaborating()}
                                scanTicket={() => this.scanTicket()}
                                Role={event.Role}
                                Code={event.Code}
                                Status={event.Status}
                                language={language}
                                TimeFinish={event.TimeFinish}
                                refundList={() => this.refundList()}
                                withdrawRequest={() => this.withdrawRequest()}
                                eventStaff={() => this.eventStaff()}
                                eventSaleReport={() => this.eventSaleReport()}
                                holderList={() => this.holderList()}
                            />
                        }
                        {
                            this.state.modelReport &&
                            <ModalReport closeModal={() => this.setState({ modelReport: false })} />
                        }
                        {
                            this.state.modelEventNews &&
                            <ModalEventNews
                                closeModal={() => this.setState({ modelEventNews: false, EventNewsId: '' })}
                                onEditEventNews={() => this.onEditEventNews()}
                                onDeleteEventNews={() => this.onDeleteEventNews()}
                                Role={event.Role}
                                onReportEventNews={() => this.onReportEventNews()}
                            />
                        }
                        {
                            this.state.modalZoom
                                ?
                                <ModalZoomImage
                                    modalVisible={this.state.modalZoom}
                                    closeModal={() => this.setState({ modalZoom: false })}
                                    event={event}
                                    dataZoom={this.state.dataZoom}
                                    index={this.state.zoomeImageIndex}
                                />
                                : null
                        }

                        {
                            this.state.modalFAQ
                                ?
                                <ModalFAQ
                                    modalVisible={this.state.modalFAQ}
                                    closeModal={() => this.setState({ modalFAQ: false })}
                                    Detail={event.Detail}
                                    FAQ={event.FAQ}
                                />
                                : null
                        }
                        <ImageView
                            images={[
                                {
                                    source: {
                                        uri: event.Posters ? event.Posters.Full : event.Poster
                                    }
                                }
                            ]}
                            imageIndex={0}
                            isVisible={modelImage}
                            onClose={() => this.setState({ modelImage: false })}
                        />
                    </View>
                    {
                        this.state.modalEventNewsDetail &&
                        <ModalEventNewsDetail
                            closeModal={this.closeModalEventNewsDetail}
                            data={this.state.eventnews}
                            onDeleteEventNews={(Id) => this.props.onDeleteEventNews(Id)}
                            onLikeEventNews={(Id) => this.onLikeEventNews(Id)}
                            onOpenModelEventNews={(Id) => this.onOpenModelEventNews(Id, this.state.Role)}
                            navigation={this.props.navigation}
                        />
                    }
                    {
                        modalEventMemberLiked &&
                        <ModalEventMemberLiked Slug={event.Slug} Role={event.Role} closeModal={() => this.setState({ modalEventMemberLiked: false })} />
                    }
                    {
                        modalEventApplyColla &&
                        <EventApplyColla Slug={event.Slug} closeModal={() => this.setState({ modalEventApplyColla: false })} navigation={this.props.navigation} />
                    }
                </View>
            </>
        );
    }
}

// EventDetail.propTypes = {
//     provider: ProviderPropType,
// };
const stylesHtml = StyleSheet.create({
    p: {
        marginTop: 3,
        marginBottom: 3
    }
});
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    boxImageThumb: {
        marginBottom: 25,
        width: width,
        resizeMode: 'cover',
        height: 210,
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    boxIcon: {
        padding: 20
    },
    imgThumb: {
        width: 31,
        height: 31,
    },
    boxCancel: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        height: 210,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    imgCancel: {
        width: width,
        height: 210,
        position: 'absolute',
        top: 0,
        left: 0,
        backgroundColor: 'rgba(0,0,0,0.5)',
        resizeMode: 'cover',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    content: {
        flex: 1,
        paddingLeft: 16,
        paddingRight: 16
    },
    txtNameEvent: {
        fontSize: 18,
        color: '#333333',
        paddingBottom: 15,
        fontWeight: 'bold'
    },
    txtCategory: {
        fontSize: 13,
        color: '#4F4F4F',
        paddingBottom: 5,
        fontWeight: '400'
    },
    boxLiked: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    boxLikeLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomWidth: 2,
        borderBottomColor: '#e8e8e8',
        flex: 1
    },
    icLike: {
        width: 22.5,
        height: 18,
        marginRight: 10
    },
    icLikePop: {
        width: 25,
        height: 25,
        padding: 10
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 23,
        marginRight: 8
    },
    boxDetail: {
        paddingTop: 30,
        paddingBottom: 30,
        borderBottomWidth: 2,
        borderBottomColor: '#e8e8e8'
    },
    txtDetail: {
        fontSize: 14,
        color: '#333333'
    },
    txtFAQ: {
        paddingTop: 30,
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 15,
    },
    txtLable: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 10,
    },
    value: {
        fontSize: 14,
        color: '#333333',
        paddingBottom: 7,
        paddingLeft: 15,
    },
    boxEventNews: {
        paddingTop: 30,
        paddingBottom: 30,
        borderBottomWidth: 2,
        borderBottomColor: '#e8e8e8'
    },
    txtEventNews: {
        fontSize: 16,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 15,
    },
    btnWrite: {
        height: 36,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#03a9f4',
        marginBottom: 15,
        borderRadius: 5
    },
    txtWrite: {
        fontSize: 15,
        color: '#03a9f4'
    },
    boxNews: {
        paddingTop: 30,
        paddingBottom: 30,
        borderBottomWidth: 2,
        borderBottomColor: '#e8e8e8'
    },
    boxInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 15
    },
    boxInfoRight: {
        flex: 1
    },
    txtNameInfo: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 5
    },
    txtTimeAgo: {
        fontSize: 14,
        color: '#757575',
    },
    imgThumbNews: {
        width: width - 40,
        height: 190,
        marginBottom: 15
    },
    txtEventContent: {
        fontSize: 14,
        color: '#333333',
        marginBottom: 15
    },
    boxEventMore: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 15
    },
    boxEventMoreLeft: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    btnReadAllNew: {
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    txtReadAllNew: {
        fontSize: 15,
        color: '#03a9f4',
        fontWeight: 'bold'
    },
    boxHostTeam: {
        paddingTop: 30,
    },
    imgHostTeam: {
        width: 72,
        height: 72,
        borderRadius: 36,
        marginRight: 10
    },
    icCertified: {
        width: 22,
        height: 22,
        marginLeft: 10
    },
    icStar: {
        width: 18,
        height: 17,
        marginRight: 3
    },
    boxEventActions: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    btnFollow: {
        flex: 0.6,
        height: 31,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#03a9f4',
        marginRight: 15,
        borderRadius: 5,
        flexDirection: 'row'
    },
    btnFollowCollaboration: {
        height: 31,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#03a9f4',
        borderRadius: 2,
        width: 100,
        flexDirection: 'row',
    },
    txtFollow: {
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    btnFollowing: {
        flex: 0.6,
        height: 31,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 15,
        borderRadius: 5,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: Colors.GRAY,
        flexDirection: 'row'
    },
    btnFollowingCollaboration: {
        height: 31,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: Colors.GRAY,
        flexDirection: 'row',
        borderRadius: 2,
        width: 100
    },
    txtFollowing: {
        fontSize: 15,
        color: Colors.GRAY,
        fontWeight: 'bold'
    },
    btnContact: {
        flex: 0.4,
        height: 31,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        // backgroundColor: '#00a9f4',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#00a9f4',
    },
    txtContact: {
        fontSize: 15,
        color: '#00a9f4',
        fontWeight: 'bold'
    },
    imgVenue: {
        width: 130,
        height: 80,
        marginRight: 10
    },
    imgCollaboration: {
        width: 60,
        height: 60,
        marginRight: 10,
        borderRadius: 30,
        backgroundColor: '#f2f2f2',
    },
    boxItemList: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 15
    },
    image_thumb: {
        width: 70,
        height: 70,
        borderRadius: 35,
        borderWidth: 1,
        borderColor: '#d7d7d7',
        marginRight: 15
    },
    ic_plus: {
        width: 70,
        height: 70,
        marginRight: 15
    },
    boxContent: {
        flex: 1
    },
    txtAddNew: {
        fontSize: 16,
        color: '#00A9F4'
    },
    boxRelated: {
        paddingTop: 30
    },
    boxTicket: {
        height: 62,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        position: 'absolute',
        width
        // alignItems: 'center',
    },
    btnTicket: {
        height: 46,
        backgroundColor: '#03a9f4',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 8,
        borderRadius: 4,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.17,
        shadowRadius: 7.49,
        elevation: 9,
    },
    boxCollaboration: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 15
    },
    boxImageZoom: {
        width: width,
        resizeMode: 'cover',
        // alignItems: 'flex-end',
    },
    iconClose: {
        width: 25,
        height: 25,
        padding: 10
    },
    boxIconClose: {
        padding: 20,
        paddingTop: 30,
        position: 'absolute',
        right: 0,
        top: 0
    },
    boxImages: {
        marginBottom: 15
    },
    boxItem: {
        marginRight: 15,
    },
    image: {
        width: 100,
        height: 100
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        team: state.team,
        user: state.user,
        language: state.language,
        global: state.global,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataEventDetail: (slug, notification_id, iso_code) => {
            dispatch(actLoadDataEventDetail(slug, notification_id, iso_code))
        },
        followTeam: (teamId) => {
            dispatch(actTeamFollow(teamId))
        },
        onLikeEventNews: (EventNewsId) => {
            dispatch(actLikeEventNews(EventNewsId))
        },
        onDeleteEventNews: (EventNewsId) => {
            dispatch(actDeleteEventNews(EventNewsId))
        },
        globalCheckLogin: (nextPage, params) => {
            dispatch(actGlobalCheckLogin(nextPage, params))
        },
        onLikeEvent: (id, user) => {
            dispatch(actLikeEvent(id, user))
        },
        clearItemEvent: () => {
            dispatch(actClearItemEvent())
        },
        onShowModalLogin: () => {
            dispatch(actShowModalLogin())
        },
        onClearEvent: () => {
            dispatch({ type: Types2.SAVE_EVENT_STEP4_SUCCESS_FULL })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventDetail);

