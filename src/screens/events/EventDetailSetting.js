import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Dimensions, ActivityIndicator } from 'react-native';

import Toast from 'react-native-simple-toast';
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import SrcView from '../../screens_view/ScrView'
import * as Colors from '../../constants/Colors'
const { width, height } = Dimensions.get('window')
import { connect } from "react-redux";
// import HTMLView from 'react-native-htmlview'
import AutoHeightWebView from 'react-native-autoheight-webview';
import { convertLanguage } from '../../services/Helper'

class HolderList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Detail: this.props.route.params['Detail'],
            Data: this.props.route.params['FAQ'],
            NameHost: this.props.route.params['NameHost'],
            Ceo: this.props.route.params['Ceo'],
            BusinessRegistrationNumber: this.props.route.params['BusinessRegistrationNumber'],
            AddressHost: this.props.route.params['AddressHost'],
            ThePersonInCharge: this.props.route.params['ThePersonInCharge'],
            PhoneHost: this.props.route.params['PhoneHost'],
            EmailHost: this.props.route.params['EmailHost'],
            modalLinkVideo: false,
            Link: '',
            FAQS: []
        };
    }

    componentWillMount() {
        var FAQS = [];
        var { language } = this.props.language
        FAQS.push(
            {
                type: 'checkbox',
                name: convertLanguage(language, 'a_ticket_check-in_at_the_event'),
                data: [
                    {
                        key: 'comeup-ticket',
                        value: convertLanguage(language, 'comeup_ticket')
                    },
                    {
                        key: 'printed-comeup-e-ticket',
                        value: convertLanguage(language, 'printed_comeup_e-ticket')
                    },
                    {
                        key: 'ticket-holder-name-&-phone-number',
                        value: convertLanguage(language, 'ticket_holder_name_&_phone_number')
                    },
                    {
                        key: 'delivered-ticket-from-this-event-host',
                        value: convertLanguage(language, 'delivered_ticket_from_this_event_host')
                    },
                    // {
                    //     key: 'no-ticket-check-in',
                    //     value: convertLanguage(language, 'no_ticket_check-in')
                    // }
                ],
            })
        FAQS.push({
            type: 'checkbox',
            name: convertLanguage(language, 'b_restrictions'),
            data: [
                {
                    key: 'id-card-check',
                    value: convertLanguage(language, 'id_card_check')
                },
                {
                    key: 'no-minor',
                    value: convertLanguage(language, 'no_minor')
                },
                {
                    key: 'no-carrying-food-and-beverage',
                    value: convertLanguage(language, 'no_carrying_food_and_beverage')
                },
                {
                    key: 'no-pet',
                    value: convertLanguage(language, 'no_pet')
                },
                // {
                //     key: 'custom-restriction',
                //     value: convertLanguage(language, 'custom_restriction')
                // }
            ]
        })
        FAQS.push({
            type: 'radio',
            name: convertLanguage(language, 'c_parking') + '*',
            data: [
                {
                    key: 'no-car-park',
                    value: convertLanguage(language, 'no_car_park')
                },
                {
                    key: 'free',
                    value: convertLanguage(language, 'free')
                },
                {
                    key: 'paid',
                    value: convertLanguage(language, 'paid')
                }
            ]
        })
        FAQS.push({
            type: 'radio',
            name: convertLanguage(language, 'd_refund_policy') + '*',
            data: [
                {
                    key: 'comeup-refund-policy',
                    value: convertLanguage(language, 'comeup_refund_policy')
                },
                {
                    key: 'full-refund-before-event-start',
                    value: convertLanguage(language, 'full_refund_before_event_start')
                },
                {
                    key: 'no-refund',
                    value: convertLanguage(language, 'no_refund')
                },
                {
                    key: 'host-refund-policy',
                    value: convertLanguage(language, 'host_refund_policy')
                }
            ]
        })
        this.setState({
            FAQS
        })
    }

    onChangeFAQ(faq, type, index, index2) {
        var { Data } = this.state;
        var i = Data[index].some(el => el.key === faq.key);
        if (!i) {
            if (type === 'radio') {
                Data[index] = [];
            }
            Data[index].unshift(faq);
        } else {
            Data[index] = Data[index].filter(el => el.key !== faq.key);
        }
        this.setState({ Data })
    }

    renderLoading() {
        return <ActivityIndicator size="large" color="#000000" />
    }

    validate() {
        var { Data, NameHost, PhoneHost, EmailHost } = this.state;
        var { language } = this.props.language
        if (Data[2].length === 0) {
            Toast.showWithGravity(convertLanguage(language, 'validate_faq2_required'), Toast.SHORT, Toast.BOTTOM)
            return true;
        }
        if (Data[3].length === 0) {
            Toast.showWithGravity(convertLanguage(language, 'validate_faq3_required'), Toast.SHORT, Toast.BOTTOM)
            return true;
        }
        if (NameHost === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_name_host_required'), Toast.SHORT, Toast.BOTTOM)
            return true;
        }
        if (PhoneHost === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_phone_host_required'), Toast.SHORT, Toast.BOTTOM)
            return true;
        }
        if (EmailHost === '') {
            Toast.showWithGravity(convertLanguage(language, 'validate_email_host_required'), Toast.SHORT, Toast.BOTTOM)
            return true;
        }
        return false;
    }

    onSaveDetail() {
        if (this.validate()) {
            return;
        }
        this.props.route.params['callback'](this.state);
        this.props.navigation.goBack()
    }

    updateDataHostInfo(HostInfo) {
        this.setState({
            NameHost: HostInfo.Name,
            Ceo: HostInfo.Ceo,
            BusinessRegistrationNumber: HostInfo.BusinessRegistrationNumber,
            AddressHost: HostInfo.Address,
            ThePersonInCharge: HostInfo.ThePersonInCharge,
            PhoneHost: HostInfo.Phone,
            EmailHost: HostInfo.Email,
        })
    }

    render() {
        var { language } = this.props.language;
        var { Data, NameHost, PhoneHost, EmailHost, Detail, FAQS } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <Touchable style={{ height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 1, backgroundColor: '#03a9f4', marginRight: 20 }} onPress={() => this.onSaveDetail()}>
                        <Text style={{ fontSize: 17, color: '#FFFFFF', fontWeight: '400', paddingLeft: 15, paddingRight: 15 }}>{convertLanguage(language, 'save')}</Text>
                    </Touchable>
                </View>

                <Line />
                <SrcView style={{ flex: 1 }}>
                    <View style={styles.content}>
                        <View style={styles.boxInput}>
                            <Touchable style={styles.ipContent} onPress={() => this.props.navigation.navigate('WebViewEditDetail', { Detail, callback: (data) => this.setState({ Detail: data.Detail }) })}>
                                <View style={{ height: 262, padding: 8, overflow: 'hidden' }}>
                                    <AutoHeightWebView startInLoadingState={true} scalesPageToFit={false} zoomable={false} scrollEnabled={false} renderLoading={this.renderLoading} source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 66}px; overflow-wrap: break-word} iframe {width: ${width - 66}px; height: ${(width - 66) * 9 / 16}px} img {width: ${width - 66}px !important; margin-bottom: 5px; margin-top: 5px; height: auto !important;}</style><body>` + Detail.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') + `</body></html>` }} originWhitelist={['*']} />
                                </View>
                                <Text style={styles.txtEdit} onPress={() => this.props.navigation.navigate('WebViewEditDetail', { Detail, callback: (data) => this.setState({ Detail: data.Detail }) })}>+ {convertLanguage(language, 'edit')}</Text>
                            </Touchable>
                        </View>
                        <Text style={styles.txtBold}>{convertLanguage(language, 'faq')}</Text>
                        {
                            FAQS.map((item, index) => {
                                return <View style={styles.boxQuestion} key={index}>
                                    <Text style={styles.txtTitleQuestion}>{item.name}</Text>
                                    {
                                        item.data.map((faq, index2) => {
                                            return <Touchable style={styles.boxItemOptions} onPress={() => this.onChangeFAQ(faq, item.type, index, index2)} key={index2}>
                                                {
                                                    item.type === 'checkbox'
                                                        ?
                                                        <Image source={Data[index].some(el => el.key === faq.key) ? require('../../assets/box_checked.png') : require('../../assets/box_check.png')} style={styles.ic_radio} />
                                                        :
                                                        <Image source={Data[index].some(el => el.key === faq.key) ? require('../../assets/radio_active_icon.png') : require('../../assets/radio_icon.png')} style={styles.ic_radio} />
                                                }
                                                <Text style={[styles.txtOptionsActive, { fontWeight: Data[index].some(el => el.key === faq.key) ? 'bold' : 'normal' }]}>{faq.value}</Text>
                                            </Touchable>
                                        })
                                    }
                                </View>
                            })
                        }
                        <View style={[styles.boxQuestion, { borderBottomWidth: 0 }]}>
                            <View style={styles.boxImport}>
                                <Text style={styles.txtTitleQuestion}>{convertLanguage(language, 'e_host_info') + '*'}</Text>
                                <Touchable style={styles.btnImport} onPress={() => this.props.navigation.navigate('ImportHostInfo', { Slug: this.props.route.params['HostInfoId'], callback: (HostInfo) => this.updateDataHostInfo(HostInfo) })}>
                                    <Text style={styles.txtImport}>{convertLanguage(language, 'import')}</Text>
                                </Touchable>
                            </View>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'companyname')}</Text>
                                <TextInput
                                    style={styles.ipContent2}
                                    selectionColor="#bcbcbc"
                                    value={NameHost}
                                    onChangeText={(NameHost) => this.setState({ NameHost })}
                                />
                            </View>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'phone')}</Text>
                                <TextInput
                                    style={styles.ipContent2}
                                    selectionColor="#bcbcbc"
                                    value={PhoneHost}
                                    onChangeText={(PhoneHost) => this.setState({ PhoneHost })}
                                />
                            </View>
                            <View style={styles.boxInput}>
                                <Text style={styles.txtInput}>{convertLanguage(language, 'email')}</Text>
                                <TextInput
                                    style={styles.ipContent2}
                                    selectionColor="#bcbcbc"
                                    value={EmailHost}
                                    onChangeText={(EmailHost) => this.setState({ EmailHost })}
                                />
                            </View>
                        </View>
                    </View>
                </SrcView>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    boxInput: {
        paddingBottom: 24,
    },
    ipContent: {
        fontSize: 15,
        color: '#333333',
        height: 300,
        overflow: 'hidden',
        borderWidth: 1,
        borderColor: '#bdbdbd',
        justifyContent: 'space-between'
        // textAlignVertical: 'top'
    },
    txtInput: {
        fontSize: 14,
        color: '#757575',
        marginBottom: 7,
        fontWeight: 'bold'
    },
    ipContent2: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333',
        paddingTop: 8,
        paddingBottom: 8,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd'
    },
    txtBold: {
        fontSize: 17,
        color: '#333333',
        fontWeight: 'bold'
    },
    boxQuestion: {
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd'
    },
    txtTitleQuestion: {
        color: '#333333',
        fontSize: 15,
        fontWeight: 'bold'
    },
    boxItemOptions: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    },
    txtOptionsActive: {
        color: '#333333',
        fontSize: 15,
        fontWeight: 'bold'
    },
    ic_radio: {
        width: 16,
        height: 16,
        marginRight: 10
    },
    txtOptions: {
        color: '#333333',
        fontSize: 15,
    },
    boxImport: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    btnImport: {
        width: 60,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333'
    },
    txtImport: {
        fontSize: 13,
        color: '#333333'
    },
    ipContent3: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333',
        paddingTop: 8,
        paddingBottom: 8,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        height: 200,
        textAlignVertical: 'top'
    },
    boxControl: {
        paddingTop: 10,
        alignItems: 'flex-end',
        justifyContent: 'center',
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    boxInsert: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#03a9f4',
        backgroundColor: '#03a9f4'
    },
    txtInsert: {
        color: '#FFFFFF',
        fontSize: 15,
        fontWeight: 'bold',
        paddingLeft: 5,
        paddingRight: 5
    },
    boxCancel: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
        marginRight: 5,
    },
    txtCancel: {
        color: '#333333',
        fontSize: 14,
        fontWeight: 'bold',
        paddingLeft: 5, paddingRight: 5
    },
    txtEdit: {
        textAlign: 'center',
        color: '#03a9f4',
        fontSize: 14,
        fontWeight: 'bold'
    },
});

const stylesHtml = StyleSheet.create({
    a: {
        fontWeight: '300',
        color: '#FF3366', // make links coloured pink
    },
    p: {
        marginTop: 3,
        marginBottom: 3
    }
});

const mapStateToProps = state => {
    return {
        language: state.language
    };
}
// const mapDispatchToProps = (dispatch, props) => {
//     return {


//     }
// }
export default connect(mapStateToProps, null)(HolderList);
// export default EventHostList;
