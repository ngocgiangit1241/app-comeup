import React, { memo, useState, useEffect, useRef } from 'react'
import { Dimensions, Image, SafeAreaView, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, Platform, TouchableOpacity, ActivityIndicator } from 'react-native';
import Touchable from '../../screens_view/Touchable';
import { useDispatch, useSelector } from "react-redux";
import flagPinkImg from '../../assets/pin_icon.png';
import * as Colors from '../../constants/Colors';
import AutoHeightWebView from 'react-native-autoheight-webview';
const { width, height } = Dimensions.get('window')
const scale = width / 360
const LATITUDE_DELTA = 0.08;
const ASPECT_RATIO = 2;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import SafeView from '../../screens_view/SafeView';
import { actEditEventStep } from "../../actions/event";


export default function EventEdit(props) {
  const dispatch = useDispatch()
  const map = useRef(null)
  const webview = useRef(null)
  const event = useSelector(state => state.event)
  const [data, setData] = useState([]);
  useEffect(() => {
    dispatch(actEditEventStep())
      .then((res) => {
        if (res && res.status === 200) {
          // setChangeStep()
        }
      })
  }, [])
  useEffect(() => {
    let data = [
      {
        ticketName: '1Vip Ticket Front Rows, Sit Next To Rihann, Vip Ticket Front Rows, Sit Next To Rihanna,Vip Ticket Front Rows, Sit Next To Rihanna... ',
        startSale: 'Purchasable by 14:14 on 10 Sunday.',
        endSale: 202,
        ticketPrice: '420.000 VND',
        ticketType: 'd-p-ticket',
        quantity: '100',
        askDeliver: true,
        question: 'fafa',
        hide: false,
        limitPer: '11',
        ticketDes: '222',

      },
      {
        ticketName: '2Vip Ticket Front Rows, Sit Next To Rihann, Vip Ticket Front Rows, Sit Next To Rihanna,Vip Ticket Front Rows, Sit Next To Rihanna... ',
        startSale: 'Purchasable by 14:14 on 10 Sunday.',
        ticketPrice: '420.000 VND',
        ticketType: 'd-p-ticket',
        quantity: '100',
        askDeliver: false,
        question: '',
        hide: false,
        limitPer: '',
        ticketDes: '',

      },
      {
        ticketName: '3Vip Ticket Front Rows, Sit Next To Rihann, Vip Ticket Front Rows, Sit Next To Rihanna,Vip Ticket Front Rows, Sit Next To Rihanna... ',
        startSale: 'Purchasable by 14:14 on 10 Sunday.',
        ticketPrice: '420.000 VND',
        ticketType: 'e-ticket',
        quantity: '100',
        askDeliver: false,
        question: '',
        hide: false,
        limitPer: '',
        ticketDes: '',

      },
    ];

    setData(data);
  }, []);
  const renderLoading = () => {
    return <ActivityIndicator size="large" color="#000000" />
  }

  const inputRef = useRef([]);
  var formatter = new Intl.NumberFormat('vi-VI', {
    style: 'currency',
    currency: 'VND',
  });
  if (inputRef.current.length !== data.length) {
    inputRef.current = Array(data.length).fill().map((_, i) => inputRef.current[i] || React.createRef())

  }
  const onRegister = () => {

  }
  return (
    <SafeView style={styles.container}>
      <ScrollView>
        <View style={styles.container2}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image source={require('../../assets/basic_info.png')} style={{ marginRight: 8 * scale, width: 32 * scale, height: 32 * scale }} />
            <Text style={{ fontWeight: '600', fontSize: 24 }}>Basic info</Text>
          </View>
          <Image
            source={{ uri: event?.event?.Posters?.Small }}
            style={{ width: 328 * scale, height: 210 * scale, resizeMode: 'contain', marginVertical: 24 }}
          />
          <Text style={{ fontWeight: '600', fontSize: 24 * scale }}>{event?.event?.Title}</Text>
          <View style={{ flexDirection: 'row' }}>
            {
              event?.event?.HashTag !== undefined && event?.event?.HashTag.map((item, index) => {
                return (
                  <Text style={{ paddingRight: 8 * scale }} key={item.Id}>{item.HashTagName}</Text>
                )
              })
            }
          </View>
          <View style={{ marginTop: 8 * scale }}>
            <View style={{ flexDirection: 'row' }}>
              <Image source={require('../../assets/step4Clock.png')} style={{ width: 16 * scale, height: 16 * scale, marginRight: 4 * scale }} />
              <Text>{event?.event?.TimeStartUTC}</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Image source={require('../../assets/Location2.png')} style={{ width: 16 * scale, height: 16 * scale, marginRight: 4 * scale }} />
              <Text>{event?.event?.TimeFinishUTC}</Text>
            </View>

          </View>
          {/* <View style={{ height: 147 * scale, marginTop: 20 * scale, backgroundColor: '#E7E7E7', position: 'relative' }}>
          {
            (event?.event?.Lat != 'undefined' && event?.event?.Long != 'undefined' && event?.event?.Lat !== "" && event?.event?.Long !== "") &&
            <View style={{ height: 80 }} onPress={() => props.navigation.navigate('MapViews', { Lat: parseFloat(event.event.Lat), Long: parseFloat(event.event.Long) })}>
              <MapView
                provider={PROVIDER_GOOGLE}
                initialRegion={{
                  latitude: parseFloat(event.event.Lat),
                  longitude: parseFloat(event.event.Long),
                  latitudeDelta: LATITUDE_DELTA,
                  longitudeDelta: LONGITUDE_DELTA,
                }}
                zoomEnabled={true}
                enableZoomControl={true}
                style={{ width: 328 * scale, height: 147 * scale }}
                ref={map}
              >
                <Marker
                  coordinate={{ latitude: parseFloat(event.event.Lat), longitude: parseFloat(event.event.Long) }}
                  image={flagPinkImg}
                />
              </MapView>
              <Touchable style={{ position: 'absolute' }} onPress={() => props.navigation.navigate('MapViews', { Lat: parseFloat(event.event.Lat), Long: parseFloat(event.event.Long) })}></Touchable>
            </View>
          }
        </View> */}
          <View View style={{ marginTop: 24 * scale }
          }>
            <Text style={styles.titleContex}>Repeat event</Text>
            <View
              style={{
                borderBottomColor: '#00A9F4',
                borderBottomWidth: 1,
                width: width - 40 * scale,
                marginVertical: 20 * scale
              }}
            />
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 14 * scale }}>
              <Text>2019.Nov.20 03:36 ~ 2020.Dec.31 03:36</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 14 * scale }}>
              <Text>2019.Nov.20 03:36 ~ 2020.Dec.31 03:36</Text>
            </View>
            <View
              style={{
                borderBottomColor: '#00A9F4',
                borderBottomWidth: 1,
                width: width - 40 * scale,
                marginBottom: 20 * scale
              }}
            />
          </View >


          <View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={require('../../assets/detail_info.png')} style={{ marginRight: 12 * scale, width: 24 * scale, height: 24 * scale, tintColor: '#4F4F4F' }} />
              <Text style={{ fontWeight: '600', fontSize: 24 * scale, color: '#333333' }}>Detail info</Text>
            </View>
            <Text style={[styles.titleContex, { marginTop: 24 * scale, marginBottom: 16 * scale }]}>Description</Text>
            <View>
              {
                // typeof event.event.Detail != 'undefined' && event.event.Detail !== '' &&
                <AutoHeightWebView
                  startInLoadingState={true}
                  scalesPageToFit={false}
                  zoomable={false}
                  scrollEnabled={false}
                  renderLoading={() => renderLoading()}
                  ref={webview}
                  width={width - 32}
                  // source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 32}px; overflow-wrap: break-word} iframe {width: ${width - 32}px; height: ${(width - 32) * 9 / 16}px} img {width: ${width - 32}px !important; margin-bottom: 5px; margin-top: 5px; height: auto !important;}</style><body>` + event.event.Detail.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') + `</body></html>` }}
                  source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 32}px; overflow-wrap: break-word} iframe {width: ${width - 32}px; height: ${(width - 32) * 9 / 16}px} img {width: ${width - 32}px !important; margin-bottom: 5px; margin-top: 5px; height: auto !important;}</style><body>` + 'jhjhjhjhjhjhjhj jhjhjh jhjhsdlivja asdjvhkajlsdjv aslkdjajksdhjklvaklsdj' + `</body></html>` }}
                  originWhitelist={['*']} />
              }
            </View>
            <Text style={[styles.titleContex, { marginTop: 24 * scale, marginBottom: 16 * scale, color: '#333333' }]}>FAQ</Text>
            <View>
              <Text style={{ fontWeight: '600', fontSize: 18 * scale, color: '#333333' }}>A. Ticket</Text>
              <Text style={{ paddingLeft: 32 * scale, paddingTop: 14 * scale, paddingBottom: 16 * scale }}>Ticket holder name & phone number</Text>
              <Text style={{ fontWeight: '600', fontSize: 18 * scale, color: '#333333' }}>B. Restrictions</Text>
              <Text style={{ paddingLeft: 32 * scale, paddingTop: 14 * scale, paddingBottom: 16 * scale }}>No carrying food and beverage</Text>
              <Text style={{ fontWeight: '600', fontSize: 18 * scale, color: '#333333' }}>C. Parking *</Text>
              <Text style={{ paddingLeft: 32 * scale, paddingTop: 14 * scale, paddingBottom: 16 * scale }}>No carrying food and beverage</Text>
              <Text style={{ fontWeight: '600', fontSize: 18 * scale, color: '#333333' }}>D. Refund *</Text>
              <Text style={{ paddingLeft: 32 * scale, paddingTop: 14 * scale }}>No refund</Text>
            </View>
            <View>
              {/* <Text style={[styles.titleContex, { marginTop: 24 * scale, marginBottom: 16 * scale, color: '#333333' }]}>Host contact information</Text>
            <Text style={{ fontSize: 16 * scale, color: '#333333', paddingVertical: 8 * scale }}>Công Ty/ Tên: {event?.event?.HostInfo.Company}</Text>
            <Text style={{ fontSize: 16 * scale, color: '#333333', paddingVertical: 8 * scale }}>Số điện thoại: {event?.event?.HostInfo?.Phone}</Text>
            <Text style={{ fontSize: 16 * scale, color: '#333333', paddingVertical: 8 * scale }}>Email: {event?.event?.HostInfo?.Email}  </Text> */}
            </View>

          </View>

          <View style={{ paddingTop: 25, paddingBottom: 23 }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', paddingBottom: 23 }}>
              <Image source={require('../../assets/tickets_big.png')} style={{ width: 32 * scale, height: 32 * scale, tintColor: '#4F4F4F', marginRight: 8 * scale }} />
              <Text style={{ fontWeight: '600', fontSize: 24 * scale, color: '#333333' }}>Detail info</Text>
            </View>

            {
              data.length > 0 ?
                data.map((item, index) => {
                  return (
                    <React.Fragment key={index}>
                      <View style={{ borderRadius: 5, padding: 12 * scale, backgroundColor: "#F2F2F2", marginBottom: 16 * scale, opacity: item.hide ? 0.3 : 1 }}>
                        <View style={{ flexDirection: 'column', flex: 1 }}>
                          <Text style={{ fontWeight: '600', fontSize: 18 * scale }}>{item.ticketName}</Text>

                        </View>
                        <View style={{ justifyContent: 'center' }}>
                          <Text style={{ fontWeight: '600', fontSize: 14 * scale, color: '#828282', paddingTop: 4 * scale }}>{item.startSale + ' ~ ' + item.endSale}</Text>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingTop: 8 * scale }}>

                            <Text style={{ fontWeight: '600', fontSize: 18 * scale }}>{formatter.format(item.ticketPrice)}</Text>
                            <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
                              {
                                item.ticketType.value === 'd-p-ticket' ?
                                  <Image source={require('../../assets/ic_delivery_brown.png')}
                                    style={{ width: 20 * scale, height: 10 * scale, marginRight: 3 * scale }} />
                                  :
                                  <Image source={require('../../assets/ic_eticket.png')}
                                    style={{ width: 11 * scale, height: 14 * scale, marginRight: 5 * scale }} />
                              }
                              <Text style={{
                                fontStyle: 'normal',
                                fontSize: 10 * scale,
                              }}>{item.ticketType.value === 'd-p-ticket' ? 'Delivery Ticket' : 'E-Ticket'}</Text>
                            </View>
                          </View>
                        </View>
                      </View>

                    </React.Fragment>
                  )
                }) :
                null
            }
          </View>


        </View >
        <View style={styles.boxSubmit}>
          <View style={styles.containerSubmit}>
            <TouchableOpacity style={styles.draftActive}>
              <Text style={styles.txtDreftActive}>Save as draft</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.saveActive} onPress={() => onRegister()}>
              <Text style={styles.txtActive} >Save & Next step</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView >
    </SafeView>
  )
}

const styles = StyleSheet.create({
  container: { backgroundColor: Colors.BG, flex: 1 },
  container2: { paddingHorizontal: 16 * scale, flex: 1 },
  titleContex: { fontSize: 20 * scale, fontWeight: '600', color: '#333333' },
  boxSubmit: {
    backgroundColor: 'white',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 8 * scale,
    },
    shadowOpacity: 0.16,
    shadowRadius: 16.00,
    elevation: 24 * scale,
    height: 68 * scale,
    width: width,
    justifyContent: 'center',
    alignItems: 'center',
    // overflow: 'hidden',

  },
  containerSubmit: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center' },
  draftActive: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', borderWidth: 1, marginRight: 16 * scale, width: 96 * scale, height: 36 * scale, borderColor: '#4F4F4F' },
  txtDreftActive: {
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16 * scale,
    lineHeight: 20 * scale,
    color: '#4F4F4F'
  },
  draftDefault: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', marginRight: 16 * scale, width: 96 * scale, height: 36 * scale, backgroundColor: '#E0E2E8' },
  txtDreft: {
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16 * scale,
    lineHeight: 20 * scale,
    color: '#B3B8BC'
  },
  saveActive: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', width: 180 * scale, height: 36 * scale, backgroundColor: '#00A9F4' },
  txtActive: {
    color: 'white', fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16 * scale,
    lineHeight: 20 * scale,
  },
  saveDefault: { borderRadius: 4 * scale, justifyContent: 'center', alignItems: 'center', width: 180 * scale, height: 36 * scale, backgroundColor: '#E0E2E8' },
  txtDefault: {
    color: 'white', fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16 * scale,
    lineHeight: 20 * scale,
    color: '#B3B8BC'
  }
})