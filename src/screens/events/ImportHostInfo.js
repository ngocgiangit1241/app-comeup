import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import SrcView from '../../screens_view/ScrView'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actDataHostInfoByTeam } from '../../actions/event'
import { convertLanguage } from '../../services/Helper'

class ImportHostInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            index: '',
            item: ''
        };
    }

    componentDidMount() {
        this.props.getDataHostInfo(this.props.route.params['Slug'], 1)
    }

    selectHostInfo(index, item) {
        this.setState({
            index,
            item
        })
    }

    apply() {
        var { item } = this.state;
        this.props.route.params['callback'](item);
        this.props.navigation.goBack()
    }

    _renderFooter() {
        var { loadMore, is_empty } = this.props.event;
        var { language } = this.props.language;
        if (loadMore) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty) {
                return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'host_info_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    loadMore() {
        var { loadMore } = this.props.event;
        var { page } = this.state;
        if (loadMore) {
            this.props.onLoadDataListEvent(this.props.route.params['Slug'], page + 1);
            this.setState({ page: page + 1 })
        }
    }

    render() {
        var { hostsInfoByEvent } = this.props.event;
        var { language } = this.props.language;
        var { index } = this.state;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <Touchable style={{ height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 1, backgroundColor: index == '' ? '#e8e8e8' : '#03a9f4', marginRight: 20 }} onPress={() => this.apply()}>
                        <Text style={{ fontSize: 17, color: index == '' ? '#bdbdbd' : '#FFFFFF', fontWeight: '400', paddingLeft: 15, paddingRight: 15 }}>{convertLanguage(language, 'apply')}</Text>
                    </Touchable>
                </View>

                <Line />
                <View style={styles.content}>
                    <FlatList
                        style={{ flex: 1, width: '100%', padding: 10 }}
                        data={hostsInfoByEvent}
                        renderItem={({ item }) => {
                            return <Touchable style={[styles.boxInfo]} onPress={() => this.selectHostInfo(item.Id, item)}>
                                <View style={[styles.boxDataInfo, { borderWidth: item.Id === index ? 2 : 1, borderColor: item.Id === index ? '#03a9f4' : '#bdbdbd' }]}>
                                    <View style={styles.boxInput}>
                                        <Text style={styles.txtInput}>{convertLanguage(language, 'name')}</Text>
                                        <View style={styles.boxInputContent}>
                                            <Text style={styles.txtContent}>{item.Name}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.boxInput}>
                                        <Text style={styles.txtInput}>{convertLanguage(language, 'phone')}</Text>
                                        <View style={styles.boxInputContent}>
                                            <Text style={styles.txtContent}>{item.Phone}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.boxInput}>
                                        <Text style={styles.txtInput}>{convertLanguage(language, 'email')}</Text>
                                        <View style={styles.boxInputContent}>
                                            <Text style={styles.txtContent}>{item.Email}</Text>
                                        </View>
                                    </View>
                                </View>
                            </Touchable>
                        }}
                        onEndReached={() => { this.loadMore() }}
                        // ListHeaderComponent={() => this.renderHeader()}
                        // ItemSeparatorComponent={this.renderSep}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(item, index) => index.toString()}
                        // refreshing={this.props.team.loading}
                        // onRefresh={() => {this.refresh()}}
                        ListFooterComponent={() => this._renderFooter()}
                    />
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#e8e8e8'
    },
    boxInput: {
        paddingBottom: 20,
    },
    txtInput: {
        fontSize: 14,
        color: '#757575',
        marginBottom: 7,
        fontWeight: 'bold'
    },
    txtContent: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
    },
    boxInputContent: {
        justifyContent: 'center',
    },
    boxInfo: {
        marginBottom: 15,
        // borderBottomWidth: 1,
        // borderBottomColor: '#e8e8e8',
    },
    btnImport: {
        width: 60,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 1,
        borderWidth: 1,
        borderColor: '#333333',
    },
    boxDataInfo: {
        flex: 1,
        padding: 20,
        backgroundColor: '#FFFFFF',
        borderRadius: 2
    },
    txtImport: {
        fontSize: 13,
        color: '#333333'
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getDataHostInfo: (slug, page) => {
            dispatch(actDataHostInfoByTeam(slug, page))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ImportHostInfo);
// export default EventHostList;
