import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import SrcView from '../../screens_view/ScrView';
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataLocation } from '../../actions/event'

class PreviousLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        this.props.onLoadDataLocation();
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    selectLocation(item) {
        this.props.route.params['callback'](item);
        this.props.navigation.goBack()
    }

    render() {
        var { loading, locations } = this.props.event;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <View style={{ width: 50 }} />
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                    <Touchable
                        onPress={() => {
                            this.props.navigation.goBack()
                        }}
                        style={{
                            width: 50, minHeight: 50,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                        <Image source={require('../../assets/X_icon.png')} />
                    </Touchable>
                </View>

                <Line />

                <SrcView style={{ flex: 1 }}>
                    {
                        loading ?
                            <ActivityIndicator size="large" color="#000000" style={{ padding: 20 }} />
                            :
                            locations.length > 0
                                ?
                                <View style={styles.content}>
                                    {
                                        locations.map((item, index) => {
                                            return <Touchable style={styles.boxLocation} key={index} onPress={() => this.selectLocation(item)}>
                                                <Text style={styles.txtVenue}>{item.Name}</Text>
                                                <Text style={styles.txtAddress}>{item.Address}</Text>
                                            </Touchable>
                                        })
                                    }
                                    {/* <View style={styles.boxLocation}>
                                <Text style={styles.txtVenueBold}>Bitexco</Text>
                                <Text style={styles.txtAddressBold}>5, Fast and furious, D1, Ho Chi Minh</Text>
                            </View> */}
                                </View>
                                :
                                <Text style={{ textAlign: 'center', paddingTop: 20, paddingBottom: 20 }}>{convertLanguage(language, 'data_empty')}</Text>
                    }
                </SrcView>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    boxLocation: {
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomWidth: 2,
        borderBottomColor: '#e8e8e8'
    },
    txtVenue: {
        fontSize: 15,
        color: '#757575',
        paddingBottom: 15
    },
    txtAddress: {
        fontSize: 15,
        color: '#757575'
    },
    txtVenueBold: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
        paddingBottom: 15
    },
    txtAddressBold: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataLocation: () => {
            dispatch(actLoadDataLocation())
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PreviousLocation);
