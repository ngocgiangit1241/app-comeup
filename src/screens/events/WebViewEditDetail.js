// import React, { useState, useEffect, useRef } from 'react';
// import { ActivityIndicator, View, Text, Image, Dimensions, StyleSheet, Platform, Keyboard } from 'react-native';

// import { useSelector } from "react-redux";
// import SafeView from '../../screens_view/SafeView'
// import Touchable from '../../screens_view/Touchable'
// import Line from '../../screens_view/Line'
// import * as Colors from '../../constants/Colors'
// import AndroidWebView from 'react-native-webview';
// const { width, height } = Dimensions.get('window')
// import { convertLanguage } from '../../services/Helper'
// import * as Config from '../../constants/Config';

// function WebViewEditDetail({ navigation, route }) {

//     const webView2 = useRef();

//     const [Detail, setDetail] = React.useState('');

//     const language = useSelector(state => state.language)

//     const sendPostMessage = () => {
//         var data = JSON.stringify({ content: Detail, placeholder: convertLanguage(language.language, 'introduce_how_this_event_is_awesome'), type: 'event', height: height - 160 })
//         webView2.current && webView2.current.postMessage(data);
//     }

//     const onMessage = (data) => {
//         setDetail(data.nativeEvent.data)
//     }

//     useEffect(() => {
//         Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
//         Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

//         // cleanup function
//         return () => {
//             Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
//             Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
//         };
//     }, []);

//     const _keyboardDidShow = (event) => {
//         var data = JSON.stringify({ height: height - 160 - event.endCoordinates.height, type: 'height', isShowKeyboard: true })
//         webView2.current && webView2.current.postMessage(data);
//     };

//     const _keyboardDidHide = () => {
//         var data = JSON.stringify({ height: height - 160, type: 'height', isShowKeyboard: false })
//         webView2.current && webView2.current.postMessage(data);
//     };

//     const renderLoading = () => {
//         return <View style={{ width, height: height - 100, position: 'absolute', justifyContent: 'center', alignItems: 'center', zIndex: 9 }}>
//             <ActivityIndicator size="large" color="#000000" />
//         </View>
//     }

//     const onSaveDetail = () => {
//         route.params['callback']({ Detail });
//         navigation.goBack()
//     }

//     return (
//         <View style={[styles.content, { backgroundColor: Colors.BG }]}>
//             <AndroidWebView
//                 ref={webView2}
//                 style={{ position: 'absolute' }}
//                 source={{ uri: Platform.OS === 'ios' ? Config.MAIN_URL + "/editor-mobile" : Config.MAIN_URL + "/editor-mobile-android" }}
//                 onMessage={onMessage}
//                 onLoadEnd={sendPostMessage}
//                 startInLoadingState
//                 renderLoading={renderLoading}
//                 scalesPageToFit={true}
//                 zoomable={false}
//                 scrollEnabled={false}
//             />
//         </View>
//     );

// }

// const styles = StyleSheet.create({
//     content: {
//         justifyContent: 'center',
//         alignItems:'center\'
//     },
// });
// export default WebViewEditDetail;



import React, { useState, useEffect, useRef } from 'react';
import { ActivityIndicator, View, Text, Image, Dimensions, StyleSheet, Platform, Keyboard } from 'react-native';

import { useSelector } from "react-redux";
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import * as Colors from '../../constants/Colors'
import AndroidWebView from 'react-native-webview';
const { width, height } = Dimensions.get('window')
import { convertLanguage } from '../../services/Helper'
import * as Config from '../../constants/Config';

function WebViewEditDetail({ navigation, route }) {

    const webView2 = useRef();

    const [Detail, setDetail] = React.useState(route.params['Detail']);
    const language = useSelector(state => state.language)

    const sendPostMessage = () => {
        var data = JSON.stringify({ content: Detail, placeholder: convertLanguage(language.language, 'introduce_how_this_event_is_awesome'), type: 'event', height: height - 160 })
        console.log('data1', data)
        webView2.current && webView2.current.postMessage(data);
    }

    const onMessage = (data) => {
        console.log('data2', data.nativeEvent.data);
        setDetail(data.nativeEvent.data)
    }

    useEffect(() => {
        Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

        // cleanup function
        return () => {
            Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
            Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
        };
    }, []);

    const _keyboardDidShow = (event) => {
        var data = JSON.stringify({ height: height - 160 - event.endCoordinates.height, type: 'height', isShowKeyboard: true })
        webView2.current && webView2.current.postMessage(data);
    };

    const _keyboardDidHide = () => {
        var data = JSON.stringify({ height: height - 160, type: 'height', isShowKeyboard: false })
        webView2.current && webView2.current.postMessage(data);
    };

    const renderLoading = () => {
        return <View style={{ width, height: height - 100, position: 'absolute', justifyContent: 'center', alignItems: 'center', zIndex: 9 }}>
            <ActivityIndicator size="large" color="#000000" />
        </View>
    }

    const onSaveDetail = () => {
        route.params['callback']({ Detail });
        navigation.goBack()
    }
    console.log('url', Platform.OS === 'ios' ? Config.MAIN_URL + "editor-mobile?lang=" + language.language : Config.MAIN_URL + "editor-mobile-android?lang=" + language.language)
    // // console.log('RETAL', route.params['Detail'])
    // console.log('lan', language.language)

    return (
        <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
            <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0, position: 'relative', zIndex: 10 }}>
                <Touchable
                    onPress={() => navigation.goBack()}
                    style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                </Touchable>
                <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
                <Touchable style={{ height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 1, backgroundColor: '#03a9f4', marginRight: 20 }} onPress={onSaveDetail}>
                    <Text style={{ fontSize: 17, color: '#FFFFFF', fontWeight: '400', paddingLeft: 15, paddingRight: 15 }}>{convertLanguage(language.language, 'save')}</Text>
                </Touchable>
            </View>

            <Line />
            <View style={styles.content}>
                <AndroidWebView
                    ref={webView2}
                    // style={{ width: 300, height: 600}}
                    source={{ uri: Platform.OS === 'ios' ? Config.MAIN_URL + "editor-mobile?lang=" + language.language : Config.MAIN_URL + "editor-mobile-android?lang=" + language.language }}
                    onMessage={onMessage}
                    onLoadEnd={sendPostMessage}
                    startInLoadingState
                    renderLoading={renderLoading}
                    scalesPageToFit={true}
                    zoomable={false}
                    scrollEnabled={false}
                />
            </View>
        </SafeView>
    );

}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
});
export default WebViewEditDetail;
