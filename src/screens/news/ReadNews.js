import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, ScrollView, Linking, StatusBar, Platform, TouchableOpacity } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import { convertDateTime } from '../../services/Helper'
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { actLoadDataReadNews } from '../../actions/news';
import moment from "moment";
import TimeAgo from 'react-native-timeago';
class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
        };
    }

    componentDidMount() {
        if (this.props.language.language === 'vi') {
            require('moment/locale/vi')
        }
        if (this.props.language.language === 'en') {
            require('moment/locale/en-au')
        }
        if (this.props.language.language === 'ko') {
            require('moment/locale/ko')
        }
        this.props.onLoadData()
    }

    _renderFooter = () => {
        var { dataReadNews } = this.props.news
        var { language } = this.props.language;


        if (dataReadNews.loadMore) { return <ActivityIndicator size="large" color="#000000" style={styles.loading} /> } else {
            if (dataReadNews.is_empty) {
                return <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                    <View style={{ alignSelf: 'center' }}>
                        <Text style={{ color: '#757575', paddingTop: 36, textAlign: 'center', fontSize: 15 }}>{convertLanguage(language, 'no_notification2')}</Text>
                    </View>
                </View>
            } else {
                return null
            }
        }
    }


    renderItem = (item) => {
        var url = 'comeup:/' + item.Notification.Url;
        return <Touchable style={{ flexDirection: 'row', paddingTop: 15 }} onPress={() => Linking.openURL(url)}>
            <Image source={{ uri: item.Notification.Thumbnail }} style={{ borderRadius: 50, width: 112 / 2, height: 112 / 2, backgroundColor: '#bdbdbd' }} />
            <View style={{ justifyContent: 'center', paddingHorizontal: 10, flex: 1 }}>
                <Text style={{ fontSize: 14, color: '#333333' }} >{item.Notification.Context}</Text>
                <Text style={{ fontSize: 12 }} >
                    {/* <TimeAgo time={convertDateTime(item.Notification.CreatedAt)} /> */}
                    {item.CreatedAt}
                </Text>
            </View>
        </Touchable>
    }
    refresh = () => {
        this.props.onLoadData()
        this.setState({ page: 1 })
    }
    loadMore = () => {
        var { dataReadNews } = this.props.news
        if (dataReadNews.loadMore) {
            this.props.onLoadData(this.state.page + 1)
            this.setState({ page: this.state.page + 1 })
        }
    }

    render() {
        StatusBar.setBarStyle(Platform.OS === 'android' ? 'light-content' : 'dark-content', true);
        var { language } = this.props.language;
        var { dataReadNews } = this.props.news;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack()}
                        style={{
                            minWidth: 40, minHeight: 50, marginLeft: 20, alignSelf: 'flex-start',
                            justifyContent: 'center'
                        }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Text style={{ alignItems: 'center', fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'read_news')}</Text>
                    <View style={{ minHeight: 50, minWidth: 50, marginRight: 5, justifyContent: 'center', alignItems: 'center' }}></View>
                </View>

                <Line />
                <View style={styles.content}>
                    <FlatList
                        contentContainerStyle={{ paddingLeft: 20, paddingRight: 20, paddingBottom: 30 }}
                        data={dataReadNews.data}
                        renderItem={({ item }) => {
                            return this.renderItem(item)
                        }}
                        onEndReached={this.loadMore}
                        // ListHeaderComponent={this.renderHeader}
                        // ItemSeparatorComponent={this.renderSep}
                        onEndReachedThreshold={0.2}
                        keyExtractor={(item, index) => index.toString()}
                        refreshing={false}
                        onRefresh={this.refresh}
                        ListFooterComponent={this._renderFooter}
                    />
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({

    new1: {
        paddingTop: 20,
        // paddingBottom:12,
        borderBottomColor: '#bdbdbd',
        borderBottomWidth: 1
    },
    content: {
        flex: 1,
    },
    txtTitle: {
        color: Colors.TEXT_P,
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 15,
    },
    loading: {
        padding: 20
    },
    boxPopover: {
        paddingVertical: 8,
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 4,
    },
    boxNewRead: {
        paddingVertical: 4,
        paddingHorizontal: 24
    },
    txtNewRead: {
        color: '#212529',
        fontSize: 16
    },
});

const mapStateToProps = state => {
    return {
        news: state.news,
        language: state.language,
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadData: (page) => {
            dispatch(actLoadDataReadNews(page))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(News);
// export default EventHostList;
