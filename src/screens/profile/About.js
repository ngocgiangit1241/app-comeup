import React from 'react'
import { PixelRatio, StyleSheet, Text, View, Image, Dimensions, ScrollView, ImageBackground, TouchableOpacity } from 'react-native'
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import { convertLanguage } from '../../services/Helper';
import * as Colors from '../../constants/Colors'
import * as Config from '../../constants/Config';
import { useSelector } from 'react-redux';
import SafeView from '../../screens_view/SafeView';
const screenWidth = Dimensions.get('window').width;
const scaleFontSize = PixelRatio.getFontScale()
const scale = screenWidth / 360 / scaleFontSize;
const scaleImage = screenWidth / 360;

const About = (props) => {
    const { language } = useSelector(s => s.language)
    return (
        <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
            <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: "space-between", marginTop: 0 }}>
                <Touchable
                    onPress={() => props.navigation.goBack()}
                    style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                </Touchable>
                <Text
                    style={{ fontSize: 18 * scale, fontWeight: 'bold', color: Colors.TEXT_P }}>
                    {convertLanguage(language, 'introduce')}
                </Text>
                <View style={{ minWidth: 48, minHeight: 48, }}>
                </View>
            </View>

            <Line />

            <ScrollView
                style={styles.content} disableScrollViewPanResponder={true} >
                <ImageBackground source={require('../../assets/banner_about.png')} style={{ width: 360 * scaleImage, height: 570 * scaleImage }} resizeMode="cover">
                    <Text style={{ color: '#fff', fontSize: 36 * scale, lineHeight: 45 * scale, fontWeight: '600', marginTop: 339 * scale, marginLeft: 16 * scale, marginRight: 50 * scale }}>
                        {convertLanguage(language, 'our_joy')}
                    </Text>
                    <Text style={{ color: '#fff', fontSize: 36 * scale, lineHeight: 45 * scale, fontWeight: 'bold', marginLeft: 16 * scale }}>
                        {convertLanguage(language, 'lets2')}
                        {language !== 'ko' && <Text style={{ color: '#00A9F4' }}>{convertLanguage(language, 'comeup')} </Text>}
                        {convertLanguage(language, 'together')}
                    </Text>
                </ImageBackground>
                <ImageBackground
                    source={require('../../assets/banner_about2.png')} style={{ width: 360 * scaleImage, height: 139 * scaleImage }} resizeMode="cover"
                >
                    <Image
                        source={require('../../assets/banner_about3.png')} style={{ width: 96 * scaleImage, height: 96 * scaleImage, alignSelf: 'center', marginTop: 33 * scale }} resizeMode="contain"
                    />
                </ImageBackground>

                <View style={styles.boxText}>
                    <Text style={{ color: '#333333', fontSize: 16 * scale, fontWeight: '600' }}>
                        {convertLanguage(language, 'comeup_des')}
                    </Text>
                </View>

                <View>
                    <Image source={require('../../assets/banner_about4.png')} style={{ width: 203 * scaleImage, height: 202 * scaleImage, alignSelf: 'flex-end', position: 'absolute', right: -40 * scaleImage }} resizeMode="contain" />
                    <Text style={{ fontSize: 24 * scale, fontWeight: '600', lineHeight: 30 * scale, color: '#00A9F4', alignSelf: 'center', marginTop: 80 * scale }}>
                        {convertLanguage(language, 'our_vision')}
                    </Text>
                </View>
                <Text style={[styles.boxText, { marginTop: 40 * scale, textAlign: 'center', color: '#333333', fontSize: 16 * scale, fontWeight: '600', lineHeight: 20 * scale }]}>
                    {convertLanguage(language, 'comeup_vision')}
                </Text>

                <Text style={{ fontSize: 24 * scale, fontWeight: '600', lineHeight: 30 * scale, color: '#00A9F4', alignSelf: 'center', marginTop: 50 * scale }}>
                    {convertLanguage(language, 'our_mission')}
                </Text>
                <View style={[styles.boxText, { marginBottom: 50 * scale }]}>
                    <View style={{ position: 'relative' }}>
                        <Image source={require('../../assets/1..png')} style={{ width: 50 * scaleImage, height: 80 * scaleImage }} resizeMode="contain" />
                        <Text style={{ position: 'absolute', top: 50 * scale, marginLeft: 33 * scale, fontSize: 16 * scale, lineHeight: 20 * scale, fontWeight: '600', color: '#333333' }}>  {convertLanguage(language, 'about1')}</Text>
                    </View>
                    <View style={{ position: 'relative', marginTop: 28 * scale }}>
                        <Image source={require('../../assets/2..png')} style={{ width: 50 * scaleImage, height: 80 * scaleImage }} resizeMode="contain" />
                        <Text style={{ position: 'absolute', top: 50 * scale, marginLeft: 33 * scale, fontSize: 16 * scale, lineHeight: 20 * scale, fontWeight: '600', color: '#333333' }}>  {convertLanguage(language, 'about2')}</Text>
                    </View>
                    <View style={{ position: 'relative', marginTop: 28 * scale }}>
                        <Image source={require('../../assets/3..png')} style={{ width: 50 * scaleImage, height: 80 * scaleImage }} resizeMode="contain" />
                        <Text style={{ position: 'absolute', top: 50 * scale, marginLeft: 33 * scale, fontSize: 16 * scale, lineHeight: 20 * scale, fontWeight: '600', color: '#333333' }}>  {convertLanguage(language, 'about3')}</Text>
                    </View>
                </View>
                <View>
                    <View style={[styles.boxText, { paddingLeft: 32 * scale, paddingRight: 32, zIndex: 20 }]}>
                        <Text style={{ fontSize: 24 * scale, fontWeight: '600', lineHeight: 30 * scale, color: '#00A9F4' }}>{convertLanguage(language, 'title1')}</Text>
                        <Text style={{ fontSize: 16 * scale, lineHeight: 20 * scale, fontWeight: '600', color: '#333333' }}>{convertLanguage(language, 'content1')}</Text>
                    </View>
                    <View style={[styles.boxText, { paddingLeft: 32 * scale, paddingRight: 32, zIndex: 20, marginTop: 20 * scale }]}>
                        <Text style={{ fontSize: 24 * scale, fontWeight: '600', lineHeight: 30 * scale, color: '#00A9F4' }}>{convertLanguage(language, 'title2')}</Text>
                        <Text style={{ fontSize: 16 * scale, lineHeight: 20 * scale, fontWeight: '600', color: '#333333' }}>{convertLanguage(language, 'content2')}</Text>
                    </View>
                    <View style={[styles.boxText, { paddingLeft: 32 * scale, paddingRight: 32, zIndex: 20, marginTop: 20 * scale }]}>
                        <Text style={{ fontSize: 24 * scale, fontWeight: '600', lineHeight: 30 * scale, color: '#00A9F4' }}>{convertLanguage(language, 'title3')}</Text>
                        <Text style={{ fontSize: 16 * scale, lineHeight: 20 * scale, fontWeight: '600', color: '#333333' }}>{convertLanguage(language, 'content3')}</Text>
                    </View>
                    <Image
                        source={require('../../assets/banner_about5.png')}
                        style={{ width: 120 * scaleImage, height: 300 * scaleImage, position: 'absolute', right: -8 * scale, top: 80 * scale, zIndex: 10 }} resizeMode="contain"
                    />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginTop: 80 * scale, marginBottom: 32 * scale }}>
                    <Text style={[styles.letComeUp, { color: "#333333" }]}>{convertLanguage(language, 'lets')} </Text>
                    {language !== 'ko' && <Text style={[styles.letComeUp, { color: "#00A9F4" }]}>{convertLanguage(language, 'comeup')}!</Text>}
                </View>
                <View>
                    <Touchable
                        onPress={() => props.navigation.navigate('TeamList')}
                        style={{
                            backgroundColor: '#00A9F4', width: 312 * scale, alignSelf: 'center',
                            paddingVertical: 8 * scale, borderRadius: 8 * scale,
                            // marginTop: 32 * scale
                        }}>
                        <Text style={{ flex: 1, color: '#FFFFFF', fontSize: 24 * scale, lineHeight: 30 * scale, fontWeight: '600', alignSelf: 'center' }}>{convertLanguage(language, 'btn_about1')}</Text>
                    </Touchable>
                    <View style={{ marginTop: 16 * scale }} />
                    <Touchable
                        onPress={() => props.navigation.navigate('RegisterOpt')}
                        style={{
                            backgroundColor: '#00A9F4', zIndex: 20,
                            width: 312 * scale,
                            alignSelf: 'center', paddingVertical: 8 * scale, borderRadius: 8 * scale
                        }}>
                        <Text style={{ color: '#FFFFFF', fontSize: 24 * scale, lineHeight: 30 * scale, fontWeight: '600', alignSelf: 'center' }}>{convertLanguage(language, 'btn_about2')}</Text>
                    </Touchable>
                    <Image
                        source={require('../../assets/banner_about6.png')}
                        style={{ width: screenWidth, position: 'absolute', top: 40 * scaleImage }} resizeMode="cover"
                    />
                    <View style={{ paddingHorizontal: 32 * scale }}>
                        <Text style={[styles.contactUs, { fontSize: 24 * scale, lineHeight: 30 * scale, fontWeight: '600', alignSelf: 'center', marginTop: 150 * scale }]}>{convertLanguage(language, 'contact_us')}</Text>
                        <Text style={[styles.contactUs, { fontSize: 20 * scale, lineHeight: 25 * scale, fontWeight: '600', marginTop: 32 * scale, marginBottom: 24 * scale }]}>{convertLanguage(language, 'south_korea')}</Text>
                        <Text style={styles.contactUs}>{convertLanguage(language, 'contact1_ko')}</Text>
                        <Text style={styles.contactUs}>{convertLanguage(language, 'contact2_ko')}</Text>
                        <Text style={styles.contactUs}>{convertLanguage(language, 'contact3_ko')}</Text>
                        <View style={{ marginTop: 16 * scale }} />
                        <Text style={styles.contactUs}>{convertLanguage(language, 'contact4_ko')}</Text>
                        <Text style={styles.contactUs}>{convertLanguage(language, 'contact5_ko')}</Text>
                        <Text style={styles.contactUs}>{convertLanguage(language, 'contact6_ko')}</Text>
                        <Text style={styles.contactUs}>{convertLanguage(language, 'contact7_ko')}</Text>
                    </View>
                    <View style={{ backgroundColor: '#00A9F4' }}>
                        <View style={{ paddingHorizontal: 32 * scale }}>
                            <Text style={[styles.contactUs, { fontSize: 20 * scale, lineHeight: 25 * scale, fontWeight: '600', marginTop: 32 * scale, marginBottom: 24 * scale }]}>{convertLanguage(language, 'vietnam')}</Text>
                            <Text style={styles.contactUs}>{convertLanguage(language, 'contact1_vn')}</Text>
                            <Text style={styles.contactUs}>{convertLanguage(language, 'contact2_vn')}</Text>
                            <View style={{ marginTop: 16 * scale }} />
                            <Text style={styles.contactUs}>{convertLanguage(language, 'contact4_vn')}</Text>
                            <Text style={styles.contactUs}>{convertLanguage(language, 'contact5_vn')}</Text>
                            <Text style={styles.contactUs}>{convertLanguage(language, 'contact6_vn')}</Text>
                            <Text style={styles.contactUs}>{convertLanguage(language, 'contact7_vn')}</Text>
                        </View>
                        <Image source={require('../../assets/starindex_CI_NAV_RGB.png')} style={{ width: 328 * scaleImage, height: 41 * scaleImage, alignSelf: 'center', marginBottom: 33 * scaleImage, marginTop: 48 * scaleImage }} />
                        <View style={{ paddingHorizontal: 16 * scale, marginBottom: 25 * scale }}>
                            <Text style={[styles.contactUs, { fontSize: 20 * scale, lineHeight: 25 * scale, fontWeight: '600' }]}>{convertLanguage(language, 'title_footer')}</Text>
                            <Text style={styles.contactUs}>{convertLanguage(language, 'content_footer')}</Text>
                        </View>
                    </View>
                </View>

            </ScrollView>
        </SafeView>
    )
}

export default About

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxText: {
        marginTop: 35 * scale,
        paddingHorizontal: 16 * scale
    },
    letComeUp: {
        fontSize: 32 * scale,
        lineHeight: 40 * scale,
        fontWeight: 'bold'
    },
    contactUs: {
        color: '#fff',
        fontSize: 16 * scale,
        lineHeight: 20 * scale,
        fontWeight: "normal"
    }
})
