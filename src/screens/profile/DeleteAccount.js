import React, {Component} from 'react';
import {
  ActivityIndicator,
  View,
  Text,
  Image,
  KeyboardAvoidingView,
  StyleSheet,
  ScrollView,
  TextInput,
  Keyboard,
  Platform,
} from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import {convertLanguage} from '../../services/Helper';
import * as Colors from '../../constants/Colors';
import {connect} from 'react-redux';
import {actCancelEvent} from '../../actions/event';
import Loading from '../../screens_view/Loading';
import {actDeleteAccount} from '../../actions/user';
import {LoginManager} from 'react-native-fbsdk';
import {GoogleSignin} from '@react-native-community/google-signin';
import AsyncStorage from '@react-native-community/async-storage';

class DeleteAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Password: '',
    };
  }

  onDeleteAccount() {
    Keyboard.dismiss();
    var {Password} = this.state;
    var body = {
      Password,
    };
    this.props.onDeleteAccount(body, this.props.navigation);
  }

  logout = () => {
    LoginManager.logOut();
    GoogleSignin.revokeAccess();
    GoogleSignin.signOut();
    let arr = [];
    arr.push(['auKey', '']);
    arr.push(['auValue', '']);
    AsyncStorage.multiSet(arr);
    this.props.getLogout(this.props.navigation);
  };

  render() {
    var {language} = this.props.language;
    var {Password} = this.state;
    var {loading} = this.props.profile;
    console.log('==============================');
    console.log('this.props.profile', this.props.profile);
    console.log('==============================');

    return (
      <SafeView style={{backgroundColor: Colors.BG, flex: 1}}>
        <View
          style={{
            alignSelf: 'stretch',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 0,
          }}>
          <Touchable
            onPress={() => this.props.navigation.goBack()}
            style={{
              minWidth: 48,
              minHeight: 48,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../../assets/icon_back.png')}
              style={{width: 24, height: 24}}
            />
          </Touchable>
          <Text
            style={{fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P}}
          />
          <View style={{width: 48}} />
        </View>

        <Line />
        <KeyboardAvoidingView
          behavior="padding"
          keyboardVerticalOffset={Platform.OS === 'android' ? -500 : 0}
          style={{flex: 1}}>
          <View style={{flex: 1}}>
            {loading && <Loading />}
            <ScrollView
              style={styles.content}
              keyboardShouldPersistTaps="handle">
              <Text style={styles.txtWarning}>
                {convertLanguage(language, 'warning')}
              </Text>
              <Text style={styles.txtQuestion}>
                {convertLanguage(language, 'delete_account_question')}
              </Text>
              <View style={styles.boxPassword}>
                <Text style={styles.txtPhone}>
                  {convertLanguage(language, 'password')}
                </Text>
                <TextInput
                  style={styles.ipContent}
                  selectionColor="#bcbcbc"
                  value={Password}
                  onChangeText={Password => this.setState({Password})}
                  secureTextEntry={true}
                />
              </View>
              <Touchable
                style={{marginTop: 0, alignSelf: 'flex-end'}}
                onPress={() => this.props.navigation.navigate('ForgotPw')}>
                <Text
                  style={{
                    textDecorationLine: 'underline',
                    alignSelf: 'center',
                    fontWeight: 'normal',
                    fontSize: 15,
                    color: Colors.PRIMARY,
                  }}>
                  {convertLanguage(language, 'forgot_password')}?
                </Text>
              </Touchable>
              <Touchable
                disabled={Password.length < 6 || loading}
                onPress={() => this.onDeleteAccount()}
                style={[
                  styles.btnConfirm,
                  {
                    backgroundColor:
                      Password.length < 6 ? '#e8e8e8' : '#ff4081',
                  },
                ]}>
                <Text
                  style={[
                    styles.txtConfirm,
                    {color: Password.length < 6 ? '#bdbdbd' : '#FFFFFF'},
                  ]}>
                  {convertLanguage(language, 'confirm')}
                </Text>
              </Touchable>
            </ScrollView>
          </View>
        </KeyboardAvoidingView>
      </SafeView>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
  },
  txtWarning: {
    textAlign: 'center',
    fontSize: 26,
    color: '#ff4081',
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 20,
  },
  txtQuestion: {
    fontSize: 17,
    color: '#333333',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  boxInfo: {
    marginTop: 25,
    marginBottom: 25,
  },
  txtInfo: {
    fontSize: 15,
    color: '#333333',
    marginBottom: 5,
  },
  ipMessage: {
    height: 160,
    borderRadius: 1,
    borderWidth: 1,
    borderColor: '#bdbdbd',
    fontSize: 13,
    color: '#333333',
    textAlignVertical: 'top',
    padding: 10,
    marginBottom: 20,
  },
  boxPassword: {
    marginVertical: 15,
  },
  txtPhone: {
    fontSize: 13,
    color: '#757575',
  },
  ipContent: {
    fontSize: 15,
    color: '#333333',
    borderBottomWidth: 1,
    borderBottomColor: '#bdbdbd',
    paddingBottom: 8,
    paddingTop: 8,
    fontWeight: 'bold',
  },
  btnConfirm: {
    alignSelf: 'center',
    width: 160,
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    marginTop: 20,
  },
  txtConfirm: {
    fontSize: 17,
    fontWeight: 'bold',
  },
});

const mapStateToProps = state => {
  return {
    language: state.language,
    profile: state.user,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onDeleteAccount: (body, navigation) => {
      dispatch(actDeleteAccount(body, navigation)).then(data => {
        if (data.status === 200) {
          logout();
        }
      });
    },
    getLogout: navigation => {
      dispatch(actLogout(navigation));
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DeleteAccount);
