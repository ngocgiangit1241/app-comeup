import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  RefreshControl,
  StatusBar,
  Platform,
} from 'react-native';

import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import Carousel from 'react-native-snap-carousel';
import * as Colors from '../../constants/Colors';
import { connect } from 'react-redux';
import ItemEventHalf from '../../components/event/ItemEventHalf';
import { actLikeEvent } from '../../actions/event';
import { convertLanguage } from '../../services/Helper';
import { actLoadDataMyProfile } from '../../actions/user';
import FastImage from 'react-native-fast-image';
import { actGlobalCheckLogin } from '../../actions/global';
import VenueManagingProfile from '../../components/venue/VenueManagingProfile';
const { width } = Dimensions.get('window');
class MyPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    var { profile } = this.props.user;
    if (profile.Id) {
      this.props.onLoadDataMyProfile();
    }
  }

  _renderFooter() {
    // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
  }

  getData(datas) {
    let array = [];
    for (let i = 0; i < datas.length; i += 2) {
      if (i > 9) break;
      let d = [];
      d.push(datas[i]);
      if (i + 1 < datas.length) d.push(datas[i + 1]);
      array.push(d);
    }
    return array;
  }
  showFollowTeam(teams) {
    var r = '';
    if (teams) {
      r = teams.map((team, index) => {
        return (
          <Touchable
            onPress={() =>
              this.props.navigation.navigate({
                name: 'DetailTeam',
                params: { id: team.Id },
                key: team.Id,
              })
            }
            key={index}
            style={styles.boxTeam}>
            <FastImage source={{ uri: team.Logo }} style={styles.avatarTeam} />
            <Text style={styles.txtTeamName} numberOfLines={1}>
              {team.Name}
            </Text>
          </Touchable>
        );
      });
    }
    return r;
  }
  showEvent(events) {
    var { language } = this.props.language;
    return events.length > 0 ? (
      <View style={styles.boxData}>
        <View style={styles.boxTitle}>
          <Text style={styles.txtTitle}>
            {convertLanguage(language, 'hosting_event')}
          </Text>
          <Touchable
            onPress={() => this.props.navigation.navigate('HostingEvent')}>
            <Image
              style={styles.icListIcon}
              source={require('../../assets/list_icon_2.png')}
            />
          </Touchable>
        </View>
        <View style={styles.boxDataEvent}>
          {events.length === 0 ? (
            <Text style={{ paddingHorizontal: 16, textAlign: 'center' }}>
              {convertLanguage(language, 'data_empty')}
            </Text>
          ) : (
            <Carousel
              data={this.getData(events)}
              renderItem={(item, index) => {
                const item1 = item.item[0];
                const item2 = item.item.length >= 2 ? item.item[1] : null;
                return (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      paddingRight:
                        this.getData(events).length === index + 1 ? 0 : 15,
                    }}
                    key={index}>
                    {item1 && (
                      <ItemEventHalf
                        numberOfLines={1}
                        cancel_text={convertLanguage(
                          language,
                          'canceled_event',
                        )}
                        data={item1}
                        navigation={this.props.navigation}
                        onLikeEvent={id => this.props.onLikeEvent(id)}
                      />
                    )}
                    {item2 && (
                      <ItemEventHalf
                        numberOfLines={1}
                        cancel_text={convertLanguage(
                          language,
                          'canceled_event',
                        )}
                        data={item2}
                        navigation={this.props.navigation}
                        onLikeEvent={id => this.props.onLikeEvent(id)}
                      />
                    )}
                  </View>
                );
              }}
              sliderWidth={width}
              itemWidth={width - 32}
              inactiveSlideScale={1}
            />
          )}
        </View>
      </View>
    ) : null;
  }
  showVenues(venues) {
    var { language } = this.props.language;
    return venues.length > 0 ? (
      <View style={styles.boxData}>
        <View style={styles.boxTitle}>
          <Text style={styles.txtTitle}>
            {convertLanguage(language, 'hosting_venue')}
          </Text>
          <Touchable
            onPress={() =>
              this.props.navigation.navigate('VenueManaging', { language })
            }>
            <Image
              style={styles.icListIcon}
              source={require('../../assets/list_icon_2.png')}
            />
          </Touchable>
        </View>
        <View style={styles.boxDataEvent}>
          {venues.length === 0 ? (
            <Text style={{ paddingHorizontal: 16, textAlign: 'center' }}>
              {convertLanguage(language, 'data_empty')}
            </Text>
          ) : (
            <VenueManagingProfile
              data={venues}
              navigation={this.props.navigation}
            />
          )}
        </View>
      </View>
    ) : null;
  }

  refresh() {
    this.props.onLoadDataMyProfile();
  }

  render() {

    StatusBar.setBarStyle(
      Platform.OS === 'android' ? 'light-content' : 'dark-content',
      true,
    );
    var { profile, profileData, loadingProfile } = this.props.user;
    var { language } = this.props.language;
    return (
      <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={{ width: 85 }} />
          <Text
            style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>
            {convertLanguage(language, 'my_profile')}
          </Text>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Touchable
              style={{
                minHeight: 50,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => this.props.globalCheckLogin('News')}>
              <Image
                source={
                  profile.IsNewNotification
                    ? require('../../assets/notification_active.png')
                    : require('../../assets/notification.png')
                }
                style={{ width: 24, height: 24 }}
              />
            </Touchable>
            <Touchable
              style={{
                minHeight: 50,
                minWidth: 50,
                marginRight: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => this.props.navigation.navigate('SearchResult')}>
              <Image
                source={require('../../assets/search.png')}
                style={{ width: 24, height: 24 }}
              />
            </Touchable>
          </View>
        </View>

        <Line />
        {profile.Id ? (
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={false}
                onRefresh={() => {
                  this.refresh();
                }}
              />
            }
            contentContainerStyle={styles.content}
            style={{ flex: 1 }}>
            <View style={styles.boxInfo}>
              <View style={styles.boxInfoLeft}>
                <Image
                  source={{ uri: profile.Avatars.Small }}
                  style={styles.avatar}
                />
                <View style={styles.boxInfoDetail}>
                  <Text style={styles.txtName} numberOfLines={2}>
                    {profile.Name}
                  </Text>
                  <Text style={styles.txtDes} numberOfLines={1}>
                    {profile.UserId}
                  </Text>
                  <Text style={styles.txtDes} numberOfLines={1}>
                    {profile.Email}
                  </Text>
                  <Text style={styles.txtDes} numberOfLines={2}>
                    {profile.Phone && profile.Zipcode + profile.Phone}
                  </Text>
                </View>
              </View>
              <Touchable
                style={styles.boxEdit}
                onPress={() => this.props.navigation.navigate('Setting')}>
                <Image
                  source={require('../../assets/setting.png')}
                  style={styles.ic_setting}
                />
              </Touchable>
            </View>
            <Touchable
              onPress={() => this.props.navigation.navigate('RegisterOpt')}
              style={styles.boxRegister}>
              <Text style={styles.txtRegister} numberOfLines={1}>
                {
                  language === 'ko' ?
                    <>
                      +{' '}
                      {convertLanguage(language, 'event')} /{' '}
                      {convertLanguage(language, 'venue')} /{' '}
                      {convertLanguage(language, 'team')}{' '}
                      {convertLanguage(language, 'register')}
                    </>
                    :
                    <>
                      + {convertLanguage(language, 'register')}{' '}
                      {convertLanguage(language, 'event')} /{' '}
                      {convertLanguage(language, 'venue')} /{' '}
                      {convertLanguage(language, 'team')}
                    </>
                }
              </Text>
            </Touchable>
            {loadingProfile ? (
              <ActivityIndicator
                size="large"
                color="#000000"
                style={styles.loading}
              />
            ) : (
              <>
                <View style={styles.boxData}>
                  <View style={styles.boxTitle}>
                    <Text style={styles.txtTitle}>
                      {convertLanguage(language, 'following_team')}
                    </Text>
                    {profileData.teams.length > 0 && (
                      <Touchable
                        onPress={() =>
                          this.props.navigation.navigate('FollowingTeam')
                        }>
                        <Image
                          style={styles.icListIcon}
                          source={require('../../assets/list_icon_2.png')}
                        />
                      </Touchable>
                    )}
                  </View>
                  {profileData.teams.length === 0 ? (
                    <View style={styles.boxEmpty}>
                      <Text style={styles.txtEmpty}>
                        {convertLanguage(language, 'team_follow_empty')}
                      </Text>
                      <Touchable
                        style={styles.btnExplore}
                        onPress={() =>
                          this.props.navigation.navigate('TeamList')
                        }>
                        <Text style={styles.txtExplore}>
                          {convertLanguage(language, 'explore')}
                        </Text>
                        <Image
                          style={styles.icRight}
                          source={require('../../assets/right_blue.png')}
                        />
                      </Touchable>
                    </View>
                  ) : (
                    <ScrollView
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                      contentContainerStyle={styles.boxListTeam}>
                      {this.showFollowTeam(profileData.teams)}
                    </ScrollView>
                  )}
                </View>
                {profileData.events.length > 0 &&
                  this.showEvent(profileData.events)}
                {profileData?.venues?.length > 0 &&
                  this.showVenues(profileData.venues)}
                <Touchable
                  style={[styles.row]}
                  onPress={() =>
                    this.props.navigation.navigate('ReceivedRequest')
                  }>
                  <Image
                    source={require('../../assets/inbox.png')}
                    style={styles.icon}
                  />
                  <Text style={[styles.text]}>
                    {convertLanguage(language, 'received_request')}
                  </Text>
                </Touchable>
                <Touchable
                  style={styles.row}
                  onPress={() =>
                    this.props.navigation.navigate('PurchaseHistory')
                  }>
                  <Image
                    source={require('../../assets/purchase_history.png')}
                    style={styles.icon}
                  />
                  <Text style={styles.text}>
                    {convertLanguage(language, 'purchase_history')}
                  </Text>
                </Touchable>
                <Touchable
                  style={styles.row}
                  onPress={() =>
                    this.props.navigation.navigate('UserSaleReport')
                  }>
                  <Image
                    source={require('../../assets/sale_report.png')}
                    style={styles.icon}
                  />
                  <Text style={styles.text}>
                    {convertLanguage(language, 'sales_report')}
                  </Text>
                </Touchable>

                {/* {
                                        <View style={styles.boxData}>
                                            <View style={styles.boxTitle}>
                                                <Text style={styles.txtTitle}>{convertLanguage(language, 'hosting_venue')}</Text>
                                                <Touchable onPress={() => this.props.navigation.navigate('HostingEvent')}>
                                                    <Image style={styles.icListIcon} source={require('../../assets/list_icon_2.png')} />
                                                </Touchable>
                                            </View>
                                            <View style={styles.boxDataEvent}>

                                                {this.props.event.events.length === 0 ? <Text style={{ paddingHorizontal: 20, textAlign: 'center' }} >{convertLanguage(language, 'data_empty')}</Text>
                                                    : <Carousel
                                                        data={this.getData(this.props.event.events)}
                                                        renderItem={((item, index) => {
                                                            const item1 = item.item[0]
                                                            const item2 = item.item.length >= 2 ? item.item[1] : null
                                                            return (
                                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 5, paddingRight: 5 }} key={index}>
                                                                    {
                                                                        item1 &&
                                                                        <ItemEventHalf cancel_text={convertLanguage(language, 'canceled_event')} data={item1} navigation={this.props.navigation} onLikeEvent={(id) => this.props.onLikeEvent(id)} />
                                                                    }
                                                                    {
                                                                        item2 &&
                                                                        <ItemEventHalf cancel_text={convertLanguage(language, 'canceled_event')} data={item2} navigation={this.props.navigation} onLikeEvent={(id) => this.props.onLikeEvent(id)} />
                                                                    }
                                                                </View>
                                                            );
                                                        })}
                                                        sliderWidth={width}
                                                        itemWidth={width - 30}
                                                        inactiveSlideScale={1}
                                                    />
                                                }
                                            </View>
                                        </View>
                                    }
                                    <View style={styles.boxListSetting}>
                                        <View style={styles.boxSetting}>
                                            <Text style={styles.txtSetting}>{convertLanguage(language, 'received_request')}</Text>
                                        </View>
                                        <View style={styles.boxSetting}>
                                            <Text style={styles.txtSetting}>{convertLanguage(language, 'purchase_history')}</Text>
                                        </View>
                                        <View style={styles.boxSetting}>
                                            <Text style={styles.txtSetting}>{convertLanguage(language, 'sales_report')}</Text>
                                        </View>
                                        <View style={styles.boxSetting}>
                                            <Text style={styles.txtSetting}>{convertLanguage(language, 'setting')}</Text>
                                        </View>
                                        <View style={styles.boxSetting}>
                                            <Text style={styles.txtSetting}>{convertLanguage(language, 'term_of_service')}</Text>
                                        </View>
                                        <View style={styles.boxSetting}>
                                            <Text style={styles.txtSetting}>{convertLanguage(language, 'privacy_policy')}</Text>
                                        </View>
                                        <View style={styles.boxSetting}>
                                            <Text style={styles.txtSetting}>{convertLanguage(language, 'about_comeUp')}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.boxDetail}>
                                        <Text style={styles.txtDetail}>{convertLanguage(language, 'about_comeup_1')}</Text>
                                        <Text style={styles.txtDetail}>{convertLanguage(language, 'about_comeup_2')}</Text>
                                        <Text style={[styles.txtDetail, { marginTop: 15 }]}>{convertLanguage(language, 'copyright_index')}</Text>
                                    </View> */}
              </>
            )}
          </ScrollView>
        ) : (
          <View
            style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
            <Touchable
              onPress={() => this.props.navigation.navigate('Login')}
              style={{
                alignSelf: 'center',
                borderRadius: 4,
                marginTop: 20,
                marginBottom: 20,
                paddingLeft: 40,
                paddingRight: 40,
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: Colors.PRIMARY,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: 'white',
                  fontSize: 20,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                {convertLanguage(language, 'login')}
              </Text>
            </Touchable>
          </View>
        )}
      </SafeView>
    );
  }
}

const styles = StyleSheet.create({
  loading: {
    padding: 20,
  },
  content: {
    paddingBottom: 16,
  },
  boxInfo: {
    paddingHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 32,
  },
  boxInfoLeft: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    width: 96,
    height: 96,
    borderRadius: 48,
    borderColor: '#bdbdbd',
    borderWidth: 1,
    marginRight: 13,
  },
  boxInfoDetail: {
    flex: 1,
  },
  txtName: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
  },
  txtDes: {
    fontSize: 12,
    color: '#4F4F4F',
    marginTop: 3,
  },
  boxEdit: {
    paddingTop: 10,
    paddingLeft: 10,
    paddingBottom: 10,
  },
  txtEdit: {
    fontSize: 13,
    color: '#757575',
  },
  boxRegister: {
    backgroundColor: '#03a9f4',
    height: 36,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 16,
    marginTop: 32,
    paddingLeft: 16,
    paddingRight: 16,
  },
  txtRegister: {
    fontSize: 15,
    color: '#FFFFFF',
    fontWeight: '600',
  },
  boxData: {
    marginTop: 24,
  },
  boxTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 16,
    paddingRight: 16,
  },
  txtTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
  },
  icListIcon: {
    width: 24,
    height: 24,
    paddingLeft: 10,
  },
  boxListTeam: {
    marginTop: 16,
    paddingLeft: 10,
    paddingRight: 10,
  },
  boxTeam: {
    width: 76,
    alignItems: 'center',
  },
  avatarTeam: {
    width: 64,
    height: 64,
    borderRadius: 32,
    marginBottom: 5,
    backgroundColor: '#bdbdbd',
  },
  txtTeamName: {
    fontSize: 14,
    color: '#4F4F4F',
    marginLeft: 6,
  },
  boxDataEvent: {
    marginTop: 15,
  },
  boxListSetting: {
    marginTop: 15,
    marginLeft: 20,
    marginRight: 20,
    borderBottomColor: '#e8e8e8',
    borderBottomWidth: 1,
  },
  boxSetting: {
    borderTopColor: '#e8e8e8',
    borderTopWidth: 1,
    paddingTop: 20,
    paddingBottom: 20,
  },
  txtSetting: {
    fontSize: 15,
    color: '#333333',
  },
  boxDetail: {
    paddingTop: 40,
    paddingBottom: 40,
    width: '60%',
    alignSelf: 'center',
  },
  txtDetail: {
    fontSize: 10,
    color: '#bdbdbd',
    textAlign: 'center',
  },
  ic_setting: {
    width: 26,
    height: 26,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 48,
    marginTop: 8,
    paddingHorizontal: 16,
  },
  icon: {
    width: 32,
    height: 32,
    marginRight: 8,
  },
  text: {
    flex: 1,
    color: '#4F4F4F',
    fontSize: 16,
    fontWeight: '600',
  },
  boxEmpty: {
    padding: 16,
  },
  txtEmpty: {
    fontSize: 16,
    color: '#333333',
    textAlign: 'center',
  },
  btnExplore: {
    height: 36,
    width: 172,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#00A9F4',
    marginTop: 8,
    alignSelf: 'center',
  },
  txtExplore: {
    color: '#00A9F4',
    fontSize: 16,
  },
  icRight: {
    width: 16,
    height: 16,
    marginLeft: 4,
  },
});

const mapStateToProps = state => {
  return {
    user: state.user,
    language: state.language,
    event: state.event,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onLikeEvent: id => {
      dispatch(actLikeEvent(id));
    },
    // onLikeVenue: (id) => {
    //     dispatch(actLikeVenue(id))
    // },
    onLoadDataMyProfile: () => {
      dispatch(actLoadDataMyProfile());
    },
    globalCheckLogin: nextPage => {
      dispatch(actGlobalCheckLogin(nextPage));
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyPage);
// export default EventHostList;
