import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  ScrollView,
  Keyboard,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import SafeView from '../../screens_view/SafeView';
import Touchable from '../../screens_view/Touchable';
import Line from '../../screens_view/Line';
import { convertLanguage } from '../../services/Helper';
import * as Colors from '../../constants/Colors';
import { connect } from 'react-redux';
import { TextField } from '../../components/react-native-material-textfield';
import HTTP from '../../services/HTTP';
import Loading from '../../screens_view/Loading';
import { changePasswordSuccess } from '../../actions/user';

class UpdatePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current_password: '',
      password: '',
      password_confirmation: '',
      loading: false,
      error: {},
    };
  }

  componentDidMount() {
    // this.getTeams();
  }

  _renderFooter() {
    // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
  }

  validate() {
    var { profile } = this.props.user;
    var { current_password, password, password_confirmation } = this.state;
    if (profile.IsPassword) {
      return (
        current_password === '' ||
        password === '' ||
        password_confirmation === ''
      );
    } else {
      return password === '' || password_confirmation === '';
    }
  }

  onUpdatePassword() {
    var { current_password, password, password_confirmation } = this.state;
    var body = {
      current_password,
      password,
      password_confirmation,
    };
    Keyboard.dismiss();
    this.setState({ error: {}, loading: true });
    return HTTP.callApiWithHeader('settings/update-password', 'POST', body)
      .then(response => {
        this.setState({ loading: false });
        if (response && response.data.status == 200) {
          this.props.changePasswordSuccess();
          Toast.showWithGravity(
            response.data.messages.Messages,
            Toast.LONG,
            Toast.TOP,
          );
          this.props.navigation.goBack();
        } else if (response && response.data.status == -1) {
          this.setState({ error: response.data.errors });
        } else {
        }
      })
      .catch(function (error) {
        this.setState({ loading: false });
      });
  }

  onDeleteAccount() {
    this.props.navigation.navigate('DeleteAccount');
  }

  render() {
    var {
      current_password,
      password,
      password_confirmation,
      loading,
      error,
    } = this.state;
    var { language } = this.props.language;
    var { profile } = this.props.user;
    return (
      <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
        <View
          style={{
            alignSelf: 'stretch',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 0,
          }}>
          <Touchable
            onPress={() => this.props.navigation.goBack()}
            style={{
              minWidth: 48,
              minHeight: 48,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../../assets/icon_back.png')}
              style={{ width: 24, height: 24 }}
            />
          </Touchable>
          <Text
            style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>
            {convertLanguage(language, 'security_setting')}
          </Text>
          <View style={{ width: 48 }} />
        </View>

        <Line />
        {loading && <Loading />}

        <View style={styles.content}>
          <ScrollView
            style={{ flex: 1, paddingHorizontal: 16, marginTop: 16 }}
            keyboardShouldPersistTaps="handled">
            <Text style={styles.txtLable}>
              {convertLanguage(language, 'change_password')}
            </Text>
            {profile.IsPassword ? (
              <>
                <Text style={styles.txtTitle}>
                  {convertLanguage(language, 'update_password_title1')}
                </Text>
                <TextField
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  value={current_password}
                  onChangeText={current_password => {
                    this.setState({ current_password });
                  }}
                  error={error.Error || error.current_password}
                  returnKeyType="next"
                  label={convertLanguage(language, 'current_password')}
                  baseColor={
                    current_password.length > 0
                      ? '#828282'
                      : 'rgba(189,189,189,1)'
                  }
                  tintColor={'#828282'}
                  errorColor={'#EB5757'}
                  inputContainerStyle={[
                    styles.inputContainerStyle,
                    { borderColor: '#BDBDBD' },
                  ]}
                  containerStyle={styles.containerStyle}
                  labelHeight={25}
                  labelTextStyle={{ paddingBottom: 15, paddingLeft: 12 }}
                  lineWidth={2}
                  selectionColor={'#3a3a3a'}
                  style={styles.input}
                  errorImage={require('../../assets/error.png')}
                  secureTextEntry={true}
                />
              </>
            ) : (
              <Text style={styles.txtTitle}>
                {convertLanguage(language, 'update_password_title2')}
              </Text>
            )}
            <TextField
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              value={password}
              onChangeText={password => {
                this.setState({ password });
              }}
              error={error.password}
              returnKeyType="next"
              label={convertLanguage(language, 'new_password')}
              baseColor={
                password.length > 0 ? '#828282' : 'rgba(189,189,189,1)'
              }
              tintColor={'#828282'}
              errorColor={'#EB5757'}
              inputContainerStyle={[
                styles.inputContainerStyle,
                { borderColor: '#BDBDBD' },
              ]}
              containerStyle={styles.containerStyle}
              labelHeight={25}
              labelTextStyle={{ paddingBottom: 15 }}
              lineWidth={2}
              selectionColor={'#3a3a3a'}
              style={styles.input}
              labelTextStyle={{ paddingLeft: 12 }}
              errorImage={require('../../assets/error.png')}
              secureTextEntry={true}
            />
            <TextField
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              value={password_confirmation}
              onChangeText={password_confirmation => {
                this.setState({ password_confirmation });
              }}
              error={error.password_confirmation}
              returnKeyType="next"
              label={convertLanguage(language, 'password_confirmation')}
              baseColor={
                password_confirmation.length > 0
                  ? '#828282'
                  : 'rgba(189,189,189,1)'
              }
              tintColor={'#828282'}
              errorColor={'#EB5757'}
              inputContainerStyle={[
                styles.inputContainerStyle,
                { borderColor: '#BDBDBD' },
              ]}
              containerStyle={styles.containerStyle}
              labelHeight={25}
              labelTextStyle={{ paddingBottom: 15 }}
              lineWidth={2}
              selectionColor={'#3a3a3a'}
              style={styles.input}
              labelTextStyle={{ paddingLeft: 12 }}
              errorImage={require('../../assets/error.png')}
              secureTextEntry={true}
            />
            <Text style={[styles.txtTitle, { color: '#828282' }]}>
              {convertLanguage(language, 'update_password_regex')}
            </Text>
            <Touchable
              disabled={this.validate()}
              style={[
                styles.btnSubmit,
                { backgroundColor: this.validate() ? '#E0E2E8' : '#00A9F4' },
              ]}
              onPress={() => this.onUpdatePassword()}>
              <Text
                style={[
                  styles.txtSubmit,
                  { color: this.validate() ? '#B3B8BC' : '#FFFFFF' },
                ]}>
                {convertLanguage(language, 'update_password')}
              </Text>
            </Touchable>
            <Text style={styles.txtLable}>
              {convertLanguage(language, 'account_deletion')}
            </Text>
            <Text style={[styles.txtTitle]}>
              {convertLanguage(language, 'delete_account_info')}
            </Text>
            {/* <Touchable
              style={[styles.btnSubmit, { backgroundColor: '#EB5757' }]}
              onPress={() => this.onDeleteAccount()}>
              <Text style={[styles.txtSubmit, { color: '#FFFFFF' }]}>
                {convertLanguage(language, 'continue_to_account_deletion')}
              </Text>
            </Touchable> */}
            <Touchable
              style={[styles.btnSubmit, { backgroundColor: '#ffff', borderColor: '#4F4F4F', borderWidth: 1 }]}
              onPress={() => this.onDeleteAccount()}>
              <Text style={[styles.txtSubmit, { color: '#4F4F4F' }]}>
                {convertLanguage(language, 'continue_to_account_deletion')}
              </Text>
            </Touchable>
          </ScrollView>
        </View>
      </SafeView>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
  txtTitle: {
    fontSize: 16,
    color: '#333333',
    marginBottom: 20,
    textAlign: 'center',
    marginTop: 16,
  },
  inputContainerStyle: {
    borderWidth: 1,
    borderRadius: 4,
    paddingHorizontal: 12,
  },
  containerStyle: {
    // paddingHorizontal: 20,
    // flex: 1
    marginVertical: 10,
  },
  input: {
    color: '#333333',
  },
  btnSubmit: {
    borderRadius: 4,
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 16,
  },
  txtSubmit: {
    fontSize: 16,
  },
  txtLable: {
    fontSize: 20,
    color: '#333333',
    fontWeight: '600',
  },
});

const mapStateToProps = state => {
  return {
    language: state.language,
    user: state.user,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    changePasswordSuccess: () => {
      dispatch(changePasswordSuccess());
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UpdatePassword);
// export default EventHostList;
