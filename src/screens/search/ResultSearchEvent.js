import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, Dimensions, ScrollView } from 'react-native';
import ItemEventHalf from '../../components/event/ItemEventHalf'
import { connect } from "react-redux";
import { actLikeEvent } from '../../actions/event'
const { width, height } = Dimensions.get('window');
import { convertLanguage } from '../../services/Helper'
import { actLoadMoreEvent } from '../../actions/search';
class ResultSearchEvent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
        };
    }

    componentDidMount() {
        // this.refresh()
    }

    _renderFooter() {
        var { loadMoreEvent } = this.props.search;
        var { is_empty } = this.props.search.dataSearch.eventList
        var { language } = this.props.language;
        if (loadMoreEvent) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty) {
                return <ScrollView>
                    <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                        <View style={{ alignItems: 'center', paddingTop: 48 }}>
                            <Image source={require('../../assets/search_empty.png')} style={{ width: 64, height: 60 }} />
                            <Text style={{ color: '#757575', textAlign: 'center', fontSize: 15 }}>{convertLanguage(language, 'not_find_matched_result')}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap' }}>
                            {this.props.showSuggestKeyword}
                        </View>
                    </View>
                </ScrollView>
                // return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'event_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    loadMore() {
        var { search } = this.props;
        if (search.loadMoreEvent) {
            this.props.onLoadMoreEvent(this.props.searchValue, this.props.city_id, search.dataSearch.eventList.current_page + 1, search.dataSearch.eventList.scroll_id)
        }
    }

    render() {
        var { search } = this.props;
        var { dataSearch } = search;
        console.log('dataSearch', dataSearch)
        var list_events = dataSearch.eventList.events;
        var { language } = this.props.language;
        return (
            search.loading ?
                <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
                :
                list_events.length === 0 ?
                    <ScrollView>
                        <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                            <View style={{ alignItems: 'center', paddingTop: 48 }}>
                                <Image source={require('../../assets/search_empty.png')} style={{ width: 64, height: 60 }} />
                                <Text style={{ color: '#757575', textAlign: 'center', fontSize: 15 }}>{convertLanguage(language, 'not_find_matched_result')}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap' }}>
                                {this.props.showSuggestKeyword}
                            </View>
                        </View>
                    </ScrollView>
                    :
                    <View style={styles.content}>
                        <FlatList
                            contentContainerStyle={{ paddingHorizontal: 20 }}
                            columnWrapperStyle={{ justifyContent: 'space-between' }}
                            data={list_events}
                            numColumns={2}
                            renderItem={({ item }) => {
                                return <ItemEventHalf cancel_text={convertLanguage(language, 'canceled_event')} data={item} navigation={this.props.navigation} onLikeEvent={(Id) => this.props.onLikeEvent(Id)} />
                            }}
                            onEndReached={() => { this.loadMore() }}
                            // ListHeaderComponent={() => this.renderHeader()}
                            // ItemSeparatorComponent={this.renderSep}
                            onEndReachedThreshold={0.6}
                            keyExtractor={(item, index) => index.toString()}
                            // refreshing={this.props.search.loadingEvent}
                            // onRefresh={() => { this.refresh() }}
                            ListFooterComponent={() => this._renderFooter()}
                        />
                    </View>

        );
    }
}

const styles = StyleSheet.create({
    tagSuggest: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#bebebe',
        borderRadius: 20,
        padding: 10,
        borderWidth: 1,
        borderColor: '#bebebe',

    },
    boxInput: {
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        margin: 20,
        marginTop: 10
    },
    content: {
        flex: 1,
    },
    imgBanner: {
        width: width,
        height: width * 9 / 16,
        marginBottom: 20,
    },
    boxFilter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 20,
    },
    boxViewMaps: {
        width: 54,
        height: 54,
        borderRadius: 27,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'black',
        shadowRadius: 5,
        shadowOpacity: 0.3,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        elevation: 5,
        bottom: 20,
        right: 20,
        backgroundColor: '#FFFFFF'
    },
    pinIcon: {
        width: 37,
        height: 45
    },
    boxItemFilter: {
        flex: 0.3333,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    txtFilter: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold'
    },
    ic_dropdown: {
        width: 13,
        height: 11
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        category: state.category,
        city: state.city,
        language: state.language,
        search: state.search
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
        onLoadMoreEvent: (searchValue, Id, page, scroll_id) => {
            dispatch(actLoadMoreEvent(searchValue, Id, page, scroll_id))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ResultSearchEvent);
// export default EventHostList;
