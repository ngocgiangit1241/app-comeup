import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, FlatList, StyleSheet, Dimensions, TextInput, TouchableOpacity } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import ItemEvent from '../../components/event/ItemEvent'
import { connect } from "react-redux";
import { actLikeEvent } from '../../actions/event'
const { width, height } = Dimensions.get('window');
import Carousel from 'react-native-snap-carousel';
import { convertLanguage } from '../../services/Helper'
import { ScrollView } from 'react-native-gesture-handler';
import { actLoadMoreEvent } from '../../actions/search';
import VenueMapItem from '../../components/venue/VenueMapItem';
import { actLikeVenues } from '../../actions/venue';

class EventList extends Component {
    constructor(props) {
        super(props);
        var { language } = this.props.language;
        this.state = {
            country: {
                Id: 1,
                Name: "Viet Nam",
                Slug: "viet-nam",
                Zipcode: "+84",
            },
            modalCountry: false,
            page: 1,
        };
    }

    componentDidMount() {

    }

    _renderFooter() {
        var { loadMoreEvent } = this.props.search;
        var { is_empty } = this.props.search.dataSearch.eventList
        var { language } = this.props.language;
        if (loadMoreEvent) {
            return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        } else {
            if (is_empty) {
                return <ScrollView>
                    <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                        <View style={{ alignItems: 'center', paddingTop: 48 }}>
                            <Image source={require('../../assets/search_empty.png')} style={{ width: 64, height: 60 }} />
                            <Text style={{ color: '#757575', textAlign: 'center', fontSize: 15 }}>{convertLanguage(language, 'not_find_matched_result')}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap' }}>
                            {this.props.showSuggestKeyword}
                        </View>
                    </View>
                </ScrollView>
                // return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'event_empty')}</Text>
            } else {
                return null;
            }
        }
    }

    refresh() {
        var { search } = this.props
        var page = 1
        this.props.onLoadMoreEvent(search.searchValue, this.props.city_id, page)
        this.setState({ page: 1 });
    }

    loadMore() {
        var { search } = this.props;
        if (search.loadMoreEvent) {
            this.props.onLoadMoreEvent(search.searchValue, this.props.city_id, search.dataSearch.eventList.current_page + 1)
        }
    }

    render() {

        var { search } = this.props;
        var { dataSearch } = search;
        var venueList = dataSearch.venueList.venues
        console.log('dataSearch', venueList);

        var { language } = this.props.language;
        return (
            search.loadingEvent ?
                <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
                :
                <View style={styles.content}>
                    {
                        venueList.length === 0 ?
                            <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                                <View style={{ alignItems: 'center', paddingTop: 48 }}>
                                    <Image source={require('../../assets/search_empty.png')} style={{ width: 64, height: 60 }} />
                                    <Text style={{ color: '#757575', textAlign: 'center', fontSize: 15 }}>{convertLanguage(language, 'not_find_matched_result')}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap' }}>
                                    {this.props.showSuggestKeyword}
                                </View>
                            </View>
                            :
                            <View style={{ marginHorizontal: 16 }}>
                                <FlatList
                                    // style={{ flex: 1, width: '100%' }}
                                    data={venueList}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <TouchableOpacity onPress={() => this.props.navigation.navigate('VenueDetail', { Id: item?.Id })}>
                                                <VenueMapItem
                                                    item={item}
                                                    navigation={this.props.navigation}
                                                    index={index}
                                                    onChangeLike={(index, id) => this.props.onLikeVenues(id)}
                                                />
                                            </TouchableOpacity>
                                        )
                                    }}
                                    // onEndReached={() => { this.loadMore() }}
                                    // ListHeaderComponent={() => this.renderHeader()}
                                    // ItemSeparatorComponent={this.renderSep}
                                    onEndReachedThreshold={0.5}
                                    keyExtractor={(item, index) => index.toString()}
                                    refreshing={this.props.search.loadingEvent}
                                // onRefresh={() => { this.refresh() }}
                                // ListFooterComponent={() => this._renderFooter()}
                                />
                            </View>
                    }
                </View>

        );
    }
}

const styles = StyleSheet.create({
    tagSuggest: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#bebebe',
        borderRadius: 20,
        padding: 10,
        borderWidth: 1,
        borderColor: '#bebebe',

    },
    boxInput: {
        borderBottomWidth: 1,
        borderBottomColor: '#bdbdbd',
        margin: 20,
        marginTop: 10
    },
    content: {
        flex: 1,
    },
    imgBanner: {
        width: width,
        height: width * 9 / 16,
        marginBottom: 20,
    },
    boxFilter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 20,
    },
    boxViewMaps: {
        width: 54,
        height: 54,
        borderRadius: 27,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'black',
        shadowRadius: 5,
        shadowOpacity: 0.3,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        elevation: 5,
        bottom: 20,
        right: 20,
        backgroundColor: '#FFFFFF'
    },
    pinIcon: {
        width: 37,
        height: 45
    },
    boxItemFilter: {
        flex: 0.3333,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    txtFilter: {
        fontSize: 14,
        color: '#333333',
        fontWeight: 'bold'
    },
    ic_dropdown: {
        width: 13,
        height: 11
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        category: state.category,
        city: state.city,
        language: state.language,
        search: state.search
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLikeEvent: (id) => {
            dispatch(actLikeEvent(id))
        },
        onLoadMoreEvent: (searchValue, Id, page, per_page) => {
            dispatch(actLoadMoreEvent(searchValue, Id, page, per_page))
        },
        onLikeVenues: (id) => {
            dispatch(actLikeVenues(id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventList);
// export default EventHostList;
