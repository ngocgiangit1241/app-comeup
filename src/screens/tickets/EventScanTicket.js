import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, Alert, StyleSheet, Platform, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window')
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
import QRCodeScanner from 'react-native-qrcode-scanner';
import * as Colors from '../../constants/Colors'
import { actLoadDataTicket, actCloseModalDataTicket, actScanTicket, actCloseModalScanTicket } from '../../actions/ticket'
import { connect } from "react-redux";
// import Permissions from 'react-native-permissions';
import { check, PERMISSIONS, RESULTS, openSettings, request } from 'react-native-permissions';
import AndroidOpenSettings from 'react-native-android-open-settings';
import Modal from "react-native-modal";
import { convertLanguage } from '../../services/Helper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { SafeAreaConsumer } from 'react-native-safe-area-context';
let scanner;
const startScan = () => {
    if (scanner) {
        scanner._setScanning(false);
    }
};

class EventScanTicket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
        };
    }

    componentDidMount() {
        this.checkCamera()
        // var CodeScan = 'ECUSANUZ';
        // var CodeEvent = 'ECUS';
        // this.props.onLoadDataTicket(CodeEvent, CodeScan);
    }

    checkCamera() {
        var permission = Platform.OS === 'android' ? PERMISSIONS.ANDROID.CAMERA : PERMISSIONS.IOS.CAMERA;
        request(permission).then(result => {
            switch (result) {
                case RESULTS.UNAVAILABLE:
                    alert(
                        'This feature is not available (on this device / in this context)',
                    );
                    this.props.navigation.goBack();
                    break;
                case RESULTS.DENIED:
                    this._alertForCameraPermission();
                    break;
                case RESULTS.GRANTED:
                    this.setState({ show: true })
                    break;
                case RESULTS.BLOCKED:
                    this._alertForCameraPermission();
                    break;
            }
        })
            .catch(error => {
                // …
            });
    }

    openSettings() {
        Platform.OS === 'ios' ? openSettings() : AndroidOpenSettings.appDetailsSettings();
        this.props.navigation.goBack();
    }

    _alertForCameraPermission() {
        var { language } = this.props.language;
        setTimeout(() => {
            Alert.alert(
                convertLanguage(language, 'access_camera_title'),
                convertLanguage(language, 'access_camera_content'),
                [
                    {
                        text: convertLanguage(language, 'cancel'),
                        onPress: () => this.props.navigation.goBack(),
                        style: 'destructive',
                    },
                    { text: convertLanguage(language, 'open_setting'), onPress: () => this.openSettings() },
                ],
            );
        }, 100)
    }

    onSuccess = (e) => {
        var event = this.props.route.params['event'];
        this.setState({
            CodeScan: e.data,
        })
        var CodeScan = e.data;
        var CodeEvent = event.Code;
        this.props.onLoadDataTicket(CodeEvent, CodeScan);
    }

    onScanTicket() {
        var event = this.props.route.params['event'];
        var body = {
            CodeScan: this.state.CodeScan,
            CodeEvent: event.Code
        }
        this.props.onScanTicket(body);
    }

    continue() {
        this.props.onCloseModalScanTicket();
        startScan()
    }

    onCloseModalDataTicket() {
        this.props.onCloseModalDataTicket();
        startScan()
    }

    render() {
        var { ticket, isShowModalTicket, isShowModalScanTicket, errors, isLoadTicketSuccess, loadingTicket, isScanTicketSuccess, error_scan } = this.props.ticket;
        var event = this.props.route.params['event'];
        var { language } = this.props.language;
        return (
            <SafeAreaConsumer>
                {insets => <View style={{ paddingTop: insets.top, backgroundColor: Colors.BG, flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
                        <Touchable
                            onPress={() => this.props.navigation.goBack()}
                            style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                        </Touchable>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'scan_ticket')}</Text>
                        <View style={{ width: 48 }} />
                    </View>

                    <Line />
                    {
                        loadingTicket &&
                        <View style={{ position: 'absolute', backgroundColor: 'rgba(0,0,0,0.1)', left: 0, top: 0, width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size="large" color="#000000" style={{ marginBottom: 50 }} />
                        </View>
                    }
                    <View style={styles.content}>
                        <View style={styles.boxTopContent}>
                            <Text style={styles.centerText}>{convertLanguage(language, 'scan_on_e_ticket')}</Text>
                            <Text style={styles.textBold}>{convertLanguage(language, 'event_code')} : {event.Code}</Text>
                        </View>
                        {
                            this.state.show &&
                            <QRCodeScanner
                                ref={(camera) => scanner = camera}
                                onRead={this.onSuccess}
                                // reactivate={this.state.isScan}
                                showMarker={true}
                                customMarker={
                                    <Image source={require('../../assets/scan_marker.png')} style={{ width: 180, height: 180, top: 150 }} />
                                }
                                cameraStyle={{
                                    height: '100%',
                                    width: window.width,
                                    opacity: loadingTicket || isShowModalTicket || isShowModalScanTicket ? 0 : 1,
                                    justifyContent: 'flex-start'
                                    // overflow: 'hidden'
                                }}
                            />
                        }
                    </View>
                    <Modal
                        isVisible={isShowModalTicket || isShowModalScanTicket}
                        animationIn="zoomInDown"
                        animationOut="zoomOutUp"
                        animationInTiming={1}
                        animationOutTiming={1}
                        backdropTransitionInTiming={1}
                        backdropTransitionOutTiming={1}
                        backdropOpacity={0.3}
                        deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                        style={styles.boxModel}>
                        {
                            isShowModalScanTicket ?
                                <View style={styles.boxScanTicket}>
                                    <Image source={isScanTicketSuccess ? require('../../assets/success.gif') : require('../../assets/close-icon.png')} style={isScanTicketSuccess ? styles.ic_success : styles.ic_fail} />
                                    <Text style={isScanTicketSuccess ? styles.txtSuccess : styles.txtFail}>{isScanTicketSuccess ? 'Success!' : error_scan}</Text>
                                    <Touchable style={styles.btnContinue} onPress={() => this.continue()}>
                                        <Text style={styles.txtContinue}>{convertLanguage(language, 'continue')}</Text>
                                    </Touchable>
                                </View>
                                :
                                <View style={styles.boxShowAnswer}>
                                    {
                                        isLoadTicketSuccess
                                            ?
                                            <React.Fragment>
                                                <View style={styles.boxData}>
                                                    <View style={styles.boxHoderName}>
                                                        <Image source={require('../../assets/username.png')} style={styles.imgUsername} />
                                                        <Text style={styles.txtUsername}>{ticket.NameHolder}</Text>
                                                    </View>
                                                    <Text style={styles.txtTicketName}>{ticket.NameTicket}</Text>
                                                </View>
                                                <View style={styles.boxButton}>
                                                    <Touchable style={styles.btnLater} onPress={() => this.onCloseModalDataTicket()}>
                                                        <Text style={styles.txtLater}>{convertLanguage(language, 'later')}</Text>
                                                    </Touchable>
                                                    <Touchable style={styles.btnCheckin} onPress={() => this.onScanTicket()}>
                                                        <Text style={styles.txtCheckin}>{convertLanguage(language, 'checkin')}</Text>
                                                    </Touchable>
                                                </View>
                                            </React.Fragment>
                                            :
                                            <React.Fragment>
                                                <Text style={styles.txtError}>{errors.CodeScan}</Text>
                                                {
                                                    errors.Type == 'used' &&
                                                    <>
                                                        <Text style={styles.txtTime}>{convertLanguage(language, 'at')} {errors.Time}</Text>
                                                        <Text style={styles.txtTime}>{convertLanguage(language, 'by')} {errors.By}</Text>
                                                    </>
                                                }
                                                <Touchable style={styles.btnScanAgain} onPress={() => this.onCloseModalDataTicket()}>
                                                    <Text style={styles.txtScanAgain}>{convertLanguage(language, 'scan_again')}</Text>
                                                </Touchable>
                                            </React.Fragment>
                                    }

                                </View>

                        }
                    </Modal>
                    <Modal
                        isVisible={isShowModalScanTicket}
                        animationIn="zoomInDown"
                        animationOut="zoomOutUp"
                        onBackdropPress={() => this.continue()}
                        animationInTiming={1}
                        animationOutTiming={1}
                        backdropTransitionInTiming={1}
                        backdropTransitionOutTiming={1}
                        backdropOpacity={0.3}
                        deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
                        style={styles.boxModel}>
                        <View style={styles.boxScanTicket}>
                            <Image source={isScanTicketSuccess ? require('../../assets/success.gif') : require('../../assets/close-icon.png')} style={isScanTicketSuccess ? styles.ic_success : styles.ic_fail} />
                            <Text style={isScanTicketSuccess ? styles.txtSuccess : styles.txtFail}>{isScanTicketSuccess ? 'Success!' : error_scan}</Text>
                            <Touchable style={styles.btnContinue} onPress={() => this.continue()}>
                                <Text style={styles.txtContinue}>{convertLanguage(language, 'continue')}</Text>
                            </Touchable>
                        </View>
                    </Modal>
                </View>}
            </SafeAreaConsumer>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    boxTopContent: {
        padding: 32,
    },
    txtFollow: {
        fontSize: 17,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    centerText: {
        fontSize: 18,
        color: '#262626',
        textAlign: 'center',
        lineHeight: 30
    },
    textBold: {
        fontWeight: 'bold',
        color: '#333333',
        fontSize: 18,
        textAlign: 'center'
    },
    boxShowAnswer: {
        padding: 25,
        backgroundColor: 'white',
        // alignItems: 'center',
        justifyContent: 'center',
        width: 260,
        alignSelf: 'center'
    },
    boxData: {
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center'
    },
    boxHoderName: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    imgUsername: {
        width: 15,
        height: 17,
        marginRight: 5
    },
    txtUsername: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333',
        // flex: 1
    },
    txtTicketName: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#333333'
    },
    boxButton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 20
    },
    btnLater: {
        width: 65,
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
        borderWidth: 1,
        borderColor: '#333333'
    },
    txtLater: {
        color: '#333333',
        fontSize: 15,
    },
    btnCheckin: {
        width: 130,
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#03a9f4',
        borderRadius: 2,
    },
    txtCheckin: {
        color: '#FFFFFF',
        fontSize: 15,
        fontWeight: 'bold',
    },
    txtError: {
        marginTop: 10,
        marginBottom: 10,
        color: '#ff4081',
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    btnScanAgain: {
        width: 210,
        backgroundColor: '#ff4081',
        borderRadius: 4,
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    txtScanAgain: {
        color: '#FFFFFF',
        fontSize: 15,
        fontWeight: 'bold',
    },
    txtTime: {
        marginBottom: 10,
        color: '#333333',
        fontSize: 15,
        textAlign: 'center'
    },
    ic_success: {
        width: 150,
        height: 150 * 261 / 257,
        padding: 20
    },
    ic_fail: {
        width: 150,
        height: 150,
        padding: 20
    },
    txtSuccess: {
        fontSize: 20,
        color: '#78B348',
        padding: 20,
        textAlign: 'center'
    },
    txtFail: {
        fontSize: 20,
        color: 'red',
        padding: 20,
        textAlign: 'center'
    },
    btnContinue: {
        width: 150,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        backgroundColor: '#78B348',
        marginTop: 20,
        marginBottom: 20
    },
    txtContinue: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#FFFFFF'
    },
    boxScanTicket: {
        height: 350,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
});

const mapStateToProps = state => {
    return {
        ticket: state.ticket,
        language: state.language
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoadDataTicket: (CodeEvent, CodeScan) => {
            dispatch(actLoadDataTicket(CodeEvent, CodeScan))
        },
        onCloseModalDataTicket: () => {
            dispatch(actCloseModalDataTicket())
        },
        onScanTicket: (body) => {
            dispatch(actScanTicket(body))
        },
        onCloseModalScanTicket: () => {
            dispatch(actCloseModalScanTicket())
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EventScanTicket);
// export default EventHostList;
