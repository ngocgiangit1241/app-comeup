import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, TextInput, StyleSheet, Dimensions, Keyboard } from 'react-native';
const { width, height } = Dimensions.get('window')
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'

import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper';
import { actCommentHolder } from '../../actions/ticket'
class HolderListComment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: ''
        };
    }

    componentDidMount() {
        var { item } = this.props.route.params;
        this.setState({
            comment: item.commentHolderList ? item.commentHolderList : ''
        })
    }

    onCommentHolder() {
        Keyboard.dismiss();
        var { item } = this.props.route.params;
        var { comment } = this.state;
        var body = {
            comment,
            paymentId: item.PaymentId
        }
        this.props.onCommentHolder(body);
    }

    render() {
        var { item } = this.props.route.params;
        var { language } = this.props.language;
        var { comment } = this.state;
        var { loadingCommentHolder } = this.props.ticket;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-end', marginTop: 0 }}>
                    <Touchable
                        onPress={() => {
                            this.props.navigation.goBack()
                        }}
                        style={{
                            width: 50, minHeight: 50,
                            justifyContent: 'center', alignItems: 'flex-end',
                            marginRight: 20
                        }}>
                        <Image source={require('../../assets/X_icon.png')} />
                    </Touchable>
                </View>

                <Line />

                <View style={styles.content}>
                    {
                        loadingCommentHolder &&
                        <View style={{ flex: 1, bottom: height / 2 - 30, width, position: 'absolute', backgroundColor: 'rgba(0,0,0,0.1)', left: 0, top: 0, height: '100%', justifyContent: 'center', alignItems: 'center', zIndex: 999999 }}>
                            <ActivityIndicator size="large" color="#000000" />
                        </View>
                    }
                    <View style={styles.boxInfo}>
                        <Image source={{ uri: item.Avatars.Small }} style={styles.imgAvatar} />
                        <View style={styles.boxInfoDetail}>
                            <Text style={styles.txtName}>{item.NameHolder}</Text>
                            <Text style={styles.txtId}>{item.Name}{item.UserId && ' / ID :' + item.UserId}</Text>
                        </View>
                    </View>
                    <TextInput
                        style={styles.ipMessage}
                        selectionColor="#bdbdbd"
                        name="comment"
                        placeholder={convertLanguage(language, 'write_a_comment')}
                        multiline={true}
                        value={comment}
                        onChangeText={(comment) => this.setState({ comment })}
                    />
                    <Touchable
                        disabled={!item.commentHolderList && comment.length === 0}
                        onPress={() => this.onCommentHolder()}
                        style={[styles.btnConfirm, { backgroundColor: !item.commentHolderList && comment.length === 0 ? '#e8e8e8' : '#03a9f4' }]}>
                        <Text style={[styles.txtConfirm, { color: !item.commentHolderList && comment.length === 0 ? '#bdbdbd' : '#FFFFFF' }]}>{convertLanguage(language, 'save')}</Text>
                    </Touchable>
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        paddingLeft: 20,
        paddingRight: 20,
        flex: 1
    },
    boxInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 20,
        paddingTop: 20
    },
    imgAvatar: {
        width: 60,
        height: 60,
        marginRight: 10,
        borderRadius: 30
    },
    boxInfoDetail: {
        flex: 1,
    },
    txtName: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
    },
    txtId: {
        fontSize: 14,
        color: '#757575',
    },
    ipMessage: {
        height: 160,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        fontSize: 13,
        color: '#333333',
        textAlignVertical: 'top',
        padding: 10,
        marginBottom: 20
    },
    btnConfirm: {
        alignSelf: 'center',
        width: 160,
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
        marginTop: 20
    },
    txtConfirm: {
        fontSize: 17,
        fontWeight: 'bold'
    },
});

const mapStateToProps = state => {
    return {
        language: state.language,
        ticket: state.ticket
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onCommentHolder: (body) => {
            dispatch(actCommentHolder(body))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HolderListComment);
