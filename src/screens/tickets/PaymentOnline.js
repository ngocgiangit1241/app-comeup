import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Alert, ActivityIndicator, Dimensions, Linking, IntentAndroid } from 'react-native';
import { WebView } from "react-native-webview";
import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
const { width, height } = Dimensions.get('window')
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import * as Config from '../../constants/Config';
var SendIntentAndroid = require("react-native-send-intent");
import firebase from 'react-native-firebase';
import { convertLanguage } from '../../services/Helper'

class PaymentOnline extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    _renderFooter() {
        // return <ActivityIndicator size="large" color="#000000" style={styles.loading}/>
    }

    onMessage = (data) => {
        var rels = JSON.parse(data.nativeEvent.data);
        if (rels.status == 200) {
            var purchaseInfo = rels.purchaseInfo;
            purchaseInfo.value = parseInt(purchaseInfo.value);
            firebase.analytics().logEvent('purchase', purchaseInfo);
            this.props.navigation.navigate('PaymentSuccess');
        } else {
            this.props.navigation.goBack();
        }
    }

    goBack() {
        const { language } = this.props.language
        Alert.alert(
            convertLanguage(language, 'cancel_payment'),
            convertLanguage(language, 'are_you_sure_to_cancel_Payment'),
            [
                {
                    text: convertLanguage(language, 'no'),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'destructive',
                },
                { text: convertLanguage(language, 'yes'), onPress: () => this.props.navigation.goBack() },
            ],
            { cancelable: false },
        );
    }

    renderLoading() {
        return <View style={{ width, height: height - 100, position: 'absolute', justifyContent: 'center', alignItems: 'center', zIndex: 10 }}>
            <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
        </View>
    }

    convertIntentURL(url) {
        var n1 = url.indexOf(";package=");
        var end_n1 = url.substring(n1);
        var n2 = end_n1.indexOf(";", 2);
        var res = end_n1.substring(9, n2);
        return res;
    }

    render() {
        var { CodePayment } = this.props.route.params;
        const { language } = this.props.language
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0, position: 'relative', zIndex: 10 }}>
                    <View style={{ width: 50 }} />
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}>{convertLanguage(language, 'payment')}</Text>
                    <Touchable
                        onPress={() => {
                            this.goBack()
                        }}
                        style={{
                            width: 50, minHeight: 50,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                        <Image source={require('../../assets/X_icon.png')} />
                    </Touchable>
                </View>

                <Line />

                <View style={styles.content}>
                    {
                        Platform.OS === 'android' ?
                            <WebView
                                source={{ uri: Config.MAIN_URL + '/api' + '/payment/' + CodePayment + '/pay' }}
                                onMessage={(data) => this.onMessage(data)}
                                startInLoadingState
                                renderLoading={this.renderLoading}
                                originWhitelist={['*']}
                                thirdPartyCookiesEnabled={true}
                                onShouldStartLoadWithRequest={event => {
                                    const { url } = event;
                                    if (url.startsWith('http://') || url.startsWith('https://')) {
                                        return true;
                                    }
                                    if (url.startsWith('intent://')) {
                                        SendIntentAndroid.openChromeIntent(url).then((wasOpen) => {
                                            if (!wasOpen) {
                                                console.log('object', url);
                                                console.log('object2', this.convertIntentURL(url));
                                                Linking.openURL(`market://details?id=${this.convertIntentURL(url)}`)
                                            }
                                        })
                                        return false;
                                    }
                                    Linking.openURL(url)
                                    return false;
                                }}
                            />
                            :
                            <WebView
                                source={{ uri: Config.API_URL + '/payment/' + CodePayment + '/pay' }}
                                onMessage={(data) => this.onMessage(data)}
                                startInLoadingState
                                renderLoading={this.renderLoading}
                                onShouldStartLoadWithRequest={event => {
                                    const { url } = event;
                                    return true;
                                }}
                            />
                    }
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        zIndex: 9
    },
});

const mapStateToProps = state => {
    return {
        // team: state.team
        language: state.language,
    };
}
// const mapDispatchToProps = (dispatch, props) => {
//     return {


//     }
// }
export default connect(mapStateToProps, null)(PaymentOnline);
// export default EventHostList;
