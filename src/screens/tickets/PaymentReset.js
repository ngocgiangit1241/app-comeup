import React, { Component } from 'react';
import { ActivityIndicator, View, Text, StyleSheet } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import { actLogout } from '../../actions/user';

import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";

class Logout extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        this.props.navigation.reset({
            index: 1,
            routes: [
              {
                params: {
                  screen: "ticket",
                },
                name: "Main",
              }
            ],
          })
    }

    render() {
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                {/* <ActivityIndicator size="large" color="#000000" />
                <Text style={{fontSize: 14, color: '#333333', marginTop: 5}}>Logging out</Text> */}
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
});

const mapStateToProps = state => {
    return {
        // team: state.team
    };
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        getLogout: (navigation) => {
            dispatch(actLogout(navigation))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Logout);
// export default EventHostList;
