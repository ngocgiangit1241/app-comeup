import React, { Component } from 'react';
import { Dimensions, View, Text, Image, StyleSheet, BackHandler } from 'react-native';

import SafeView from '../../screens_view/SafeView'
import Touchable from '../../screens_view/Touchable'
import Line from '../../screens_view/Line'
const { width } = Dimensions.get('window');
import Share from 'react-native-share';
import * as Colors from '../../constants/Colors'
import { connect } from "react-redux";
import { getTag, convertLanguage } from '../../services/Helper';
import * as Config from '../../constants/Config';
class PaymentSuccess extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            // this.goBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    share() {
        var { event } = this.props.event;
        var { language } = this.props.language;
        let options = {
            title: event.Title,
            message: event.Title + `\n${convertLanguage(language, 'find_and_join_event_in_comeup')}\n` + getTag(event.HashTag),
            url: Config.MAIN_URL + 'events/' + event.Slug,
            subject: "Jois with us" //  for email
        };
        Share.open(options)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }

    render() {
        var { event } = this.props.event;
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.PRIMARY, flex: 1 }}>
                <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-end', marginTop: 0 }}>
                    <Touchable
                        onPress={() => {
                            // this.props.navigation.popToTop();
                            this.props.navigation.navigate('PaymentReset')
                        }}
                        style={{
                            width: 50, minHeight: 50,
                            justifyContent: 'center', alignItems: 'center',
                        }}>
                        <Image source={require('../../assets/x_icon_white.png')} style={{ width: 20, height: 20 }} />
                    </Touchable>
                </View>

                <View style={styles.content}>
                    <Text style={styles.txtThank}>{convertLanguage(language, 'thanks_for_your_purchase')}</Text>
                    <View style={styles.boxCenter}>
                        <Image style={styles.thumb} source={{ uri: event.Posters ? event.Posters.Medium : event.Poster }} />
                        <Text style={styles.txtContent}>{convertLanguage(language, 'more_people_more_fun')}</Text>
                    </View>
                    <Touchable style={styles.btnShare} onPress={() => { this.share() }}>
                        <Text style={styles.txtShare}>{convertLanguage(language, 'share')}</Text>
                    </Touchable>
                </View>
            </SafeView>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    txtThank: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#FFFFFF', textAlign: 'center',
    },
    boxCenter: {
        alignItems: 'center'
    },
    thumb: {
        width: width - 80,
        height: 170,
        marginBottom: 25
    },
    txtContent: {
        fontSize: 16,
        color: '#FFFFFF',
        textAlign: 'center'
    },
    btnShare: {
        backgroundColor: '#FFFFFF',
        width: 250,
        height: 40,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtShare: {
        color: Colors.PRIMARY,
        fontWeight: 'bold',
        fontSize: 15
    },
});

const mapStateToProps = state => {
    return {
        event: state.event,
        language: state.language
    };
}
// const mapDispatchToProps = (dispatch, props) => {
//     return {


//     }
// }
export default connect(mapStateToProps, null)(PaymentSuccess);
// export default EventHostList;
