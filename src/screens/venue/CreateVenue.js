import moment from "moment";
import React, { useEffect, useRef, useState } from 'react';
import { ActivityIndicator, Alert, Dimensions, Image, Keyboard, PermissionsAndroid, PixelRatio, Platform, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Autocomplete from "react-native-autocomplete-input";
import AutoHeightWebView from 'react-native-autoheight-webview';
import ImagePicker from 'react-native-image-crop-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Popover from 'react-native-popover-view';
import { log } from "react-native-reanimated";
import { SafeAreaConsumer } from 'react-native-safe-area-context';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import { useDispatch, useSelector } from "react-redux";
import RNFetchBlob from "rn-fetch-blob";
import { useDebouncedCallback } from 'use-debounce';
import { actLoadDataCategory } from '../../actions/category';
import { actSelectCountry } from '../../actions/city';
import { actCreateVenue, actEditVenue } from '../../actions/venue';
import flagPinkImg from '../../assets/pin_1_icon_active.png';
import ModalNewCity from '../../components/ModalNewCity';
import ModalTagNew from '../../components/ModalTagNew';
import { TextField } from '../../components/react-native-material-textfield';
import * as Colors from '../../constants/Colors';
import * as Config from '../../constants/Config';
import Line from '../../screens_view/Line';
import Touchable from '../../screens_view/Touchable';
import { convertLanguage } from '../../services/Helper';

const { width, height } = Dimensions.get('window')
const scale = width / 360
const fs = RNFetchBlob.fs;
let imagePath = null;
const ASPECT_RATIO = 2;
const LATITUDE_DELTA = 0.08;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const scaleFontSize = PixelRatio.getFontScale()

export default function CreateVenue(props) {
    const touchable = useRef(null)

    const dispatch = useDispatch()
    const map = useRef(null)
    const scrollView = useRef(null)

    const categories = useSelector(state => state.category.categories)
    const countries = useSelector(state => state.city.countries)

    const { language } = useSelector(state => state.language)
    const [modal, setModal] = useState({
        category: false,
        city: false
    })
    const [photo, setPhoto] = useState([])
    const [loading, setLoading] = useState(false)
    const [contactInfor, setContacInfor] = useState({
        manageName: '',
        zipcode: '+84',
        phone: '',
        email: '',
        toogle: false,
        Detail: ''
    })
    const [data, setData] = useState({
        name: '',
        venueId: '',
        category_selected: [],
        listCategory: '',
        selected: [],
        AreaName: '',
        city: '',
        Lat: '',
        Long: '',
        AreaId: '',
        Unit: '',
        Address: '',
        CountryCode: '',
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
        hideResults: true,
        predictions: [],
        isEdit: false,
        showPink: false,
        loading: false,
        editAddress: false,
        loadingLocation: false,

    })
    const [errors, setErrors] = useState({
        poster: false,
        logo: false,
        name: false,
        venueId: false,
        category: false,
        AreaName: false,
        address: false,
        manageName: false,
        phone: false,
        email: false,
        openTime: false,
        phoneString: '',
        emailString: ''
    })
    const [image, setImage] = useState({
        poster: '',
        posterCrop: '',
        logo: '',
        logoCrop: ''
    })
    const [openTime, setOpenTime] = useState([
        {
            value: 'monday',
            startTime: 'start',
            endTime: 'end',
            toogle: false
        },
        {
            value: 'tuesday',
            startTime: 'start',
            endTime: 'end',
            toogle: false
        },
        {
            value: 'wednesday',
            startTime: 'start',
            endTime: 'end',
            toogle: false
        },
        {
            value: 'thursday',
            startTime: 'start',
            endTime: 'end',
            toogle: false
        },
        {
            value: 'friday',
            startTime: 'start',
            endTime: 'end',
            toogle: false
        },
        {
            value: 'saturday',
            startTime: 'start',
            endTime: 'end',
            toogle: false
        },
        {
            value: 'sunday',
            startTime: 'start',
            endTime: 'end',
            toogle: false
        },
    ])
    const [address, setAddress] = useState('')

    // const venue = useSelector(state => state.venue2)
    const venue = useSelector(state => state.venue)
    useEffect(() => {
        dispatch(actLoadDataCategory('venue'))
        dispatch(actSelectCountry())

        if (props?.route?.params?.edit !== undefined && props?.route?.params?.edit) {
            setContacInfor({
                manageName: venue.venue?.ManagerName,
                zipcode: venue.venue?.Zipcode,
                phone: venue.venue?.Phone,
                email: venue.venue?.Email,
                toogle: false,
                Detail: venue.venue?.Description !== null ? venue.venue?.Description : ''
            })

            // 
            let poster = '', posterCrop = '', logo = '', logoCrop = ''
            let dataFetIMG = async () => {
                RNFetchBlob.config({
                    fileCache: true
                })
                    .fetch("GET", venue.venue?.Posters?.Full)
                    .then(resp => {
                        imagePath = resp.path();
                        return resp.readFile("base64");
                    })
                    .then(base64Data => {
                        poster = 'data:image/png;base64,' + base64Data
                        return fs.unlink(imagePath);
                    });
                // 
                await RNFetchBlob.config({
                    fileCache: true
                })
                    .fetch("GET", venue.venue?.Posters?.Medium)
                    .then(resp => {
                        imagePath = resp.path();
                        return resp.readFile("base64");
                    })
                    .then(base64Data => {
                        posterCrop = 'data:image/png;base64,' + base64Data
                        return fs.unlink(imagePath);
                    });

                await RNFetchBlob.config({
                    fileCache: true
                })
                    .fetch("GET", venue.venue?.Logos?.Full)
                    .then(resp => {
                        imagePath = resp.path();
                        return resp.readFile("base64");
                    })
                    .then(base64Data => {
                        logo = 'data:image/png;base64,' + base64Data
                        return fs.unlink(imagePath);
                    });
                // 
                await RNFetchBlob.config({
                    fileCache: true
                })
                    .fetch("GET", venue.venue?.Logos?.Medium)
                    .then(resp => {
                        imagePath = resp.path();
                        return resp.readFile("base64");
                    })
                    .then(base64Data => {
                        logoCrop = 'data:image/png;base64,' + base64Data
                        return fs.unlink(imagePath);
                    });
                setImage({ poster, posterCrop, logo, logoCrop })
            }
            dataFetIMG()
            let listCategory = '',
                selected = []
            venue.venue?.HashTag.forEach(element => {
                listCategory += '#' + element.HashTagName + ' '
                selected.push({ value: element.Id, name: element.HashTagName })
            });
            setData({
                name: venue.venue?.Name,
                venueId: venue.venue?.Code,
                category_selected: venue.venue?.HashTag,
                listCategory,
                selected,
                AreaName: venue.venue?.AreaName,
                city: venue.venue?.AreaName,
                Lat: venue.venue?.Lat,
                Long: venue.venue?.Long,
                AreaId: venue.venue?.AreaId,
                Unit: '',
                Address: venue.venue?.Address,
                CountryCode: '',
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
                hideResults: true,
                predictions: [],
                isEdit: false,
                showPink: false,
                loading: false,
                editAddress: false,
                loadingLocation: false,
            })
            //
            setAddress(venue.venue?.Address)


            //Time
            let ListTimeOpen = [...openTime]
            if (venue.venue?.ListTimeOpen?.length > 0) {
                venue.venue?.ListTimeOpen.forEach(element => {
                    switch (element?.Code) {
                        case 'monday':
                            ListTimeOpen[0].startTime = element.TimeStart.substring(0, 5)
                            ListTimeOpen[0].endTime = element.TimeEnd.substring(0, 5)
                            ListTimeOpen[0].toogle = true
                            break;
                        case 'tuesday':
                            ListTimeOpen[1].startTime = element.TimeStart.substring(0, 5)
                            ListTimeOpen[1].endTime = element.TimeEnd.substring(0, 5)
                            ListTimeOpen[1].toogle = true
                            break;
                        case 'wednesday':
                            ListTimeOpen[2].startTime = element.TimeStart.substring(0, 5)
                            ListTimeOpen[2].endTime = element.TimeEnd.substring(0, 5)
                            ListTimeOpen[2].toogle = true
                            break;
                        case 'thursday':
                            ListTimeOpen[3].startTime = element.TimeStart.substring(0, 5)
                            ListTimeOpen[3].endTime = element.TimeEnd.substring(0, 5)
                            ListTimeOpen[3].toogle = true
                            break;
                        case 'friday':
                            ListTimeOpen[4].startTime = element.TimeStart.substring(0, 5)
                            ListTimeOpen[4].endTime = element.TimeEnd.substring(0, 5)
                            ListTimeOpen[4].toogle = true
                            break;
                        case 'saturday':
                            ListTimeOpen[5].startTime = element.TimeStart.substring(0, 5)
                            ListTimeOpen[5].endTime = element.TimeEnd.substring(0, 5)
                            ListTimeOpen[5].toogle = true
                            break;
                        case 'sunday':
                            ListTimeOpen[6].startTime = element.TimeStart.substring(0, 5)
                            ListTimeOpen[6].endTime = element.TimeEnd.substring(0, 5)
                            ListTimeOpen[6].toogle = true
                            break;
                        default:
                            break;
                    }
                });
            }
        }
    }, [])
    // 
    const showListCategory = () => {
        let dataFetch = ''
        data.selected.forEach(element => {
            dataFetch += '#' + element?.name + ' '
        });
        return dataFetch;
    }
    function onFocusAddress() {
        scrollView?.current?.scrollTo({ x: 0, y: 100, animated: true })
    }
    function onEditLocation() {
        dismissKeyboard();
        setData({ ...data, isEdit: true, showPink: true });
    }
    function showDataCategory() {
        const result = [];
        categories.forEach(category => {
            result.push({
                name: category.HashTagName,
                value: category.Id
            })
        });
        return result;
    }
    function selectCategory(data1) {
        var selected2 = data.selected
        var index = selected2.some(el => el.value === data1.value);
        if (index) {
            selected2 = selected2.filter(el => el.value !== data1.value);
        } else {
            if (selected2.length < 5) {
                selected2.push(data1)
            }
        }
        setData({ ...data, selected: selected2 })
    }

    function onSelect(value) {
        if (value.Id) {
            // setInitial({ ...initial, city: value, Lat: value.Lat, Long: value.Lng, AreaId: value.Id, AreaName: value.Name })
            var region = {
                latitude: value.Lat,
                longitude: value.Lng,
                latitudeDelta: data.latitudeDelta,
                longitudeDelta: data.longitudeDelta
            }
            setTimeout(() => map?.current?.animateToRegion(region), 10);
            loadDataUnit(value.Id, value)
        }
    }
    function loadDataUnit(AreaId, value) {
        fetch(Config.API_URL + '/area/' + AreaId + '/find-country', {
        }).then((response) => {
            return response.json()
        }).then((response) => {
            if (response.status == 200) {
                let nameCity = ''
                switch (language) {
                    case 'ko':
                        nameCity = value.Name_ko
                        break;
                    case 'vi':
                        nameCity = value.Name_vi
                        break;
                    case 'en':
                        nameCity = value.Name_en
                        break;
                    default:
                        nameCity = value.Name
                        break;
                }

                setData({
                    ...data,
                    city: value,
                    Lat: value.Lat,
                    Long: value.Lng,
                    AreaId: value.Id,
                    AreaName: nameCity,
                    Unit: response.data.Unit,
                    CountryCode: response.data.Code
                })

                setAddress('')
            }
        })
    }
    async function onSelectLocation(dataFetch) {
        Keyboard.dismiss();
        setAddress(dataFetch.item.description)

        const apiUrl2 = `https://maps.googleapis.com/maps/api/place/details/json?key=${Config.GOOGLE_API_KEY}&placeid=${dataFetch.item.place_id}`;
        try {
            const result2 = await fetch(apiUrl2);
            const json2 = await result2.json();
            setData({
                ...data,
                Lat: json2.result.geometry.location.lat,
                Long: json2.result.geometry.location.lng,
                Address: dataFetch.item.description,
                hideResults: true,
                showPink: true
            })
            var region = {
                Address: dataFetch.item.description,
                hideResults: true,
                showPink: true,
                latitude: json2.result.geometry.location.lat,
                longitude: json2.result.geometry.location.lng,
                latitudeDelta: data.latitudeDelta,
                longitudeDelta: data.longitudeDelta
            }
            setTimeout(() => map?.current?.animateToRegion(region), 10);
        } catch (err) {
        }
    }

    function _onMapReady() {
        if (Platform.OS === 'android') {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                .then(granted => {
                    // setState({ paddingTop: 0 });
                });
        }
    }
    const debounced = useDebouncedCallback(
        (value) => {
            onChangeDestination(value)
        },
        500
    );
    useEffect(() => {
        debounced.callback(address)
    }, [address])

    async function onChangeDestination(Address) {
        let { Lat, Long } = data
        const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${Config.GOOGLE_API_KEY}&input=${encodeURI(Address)}&types=address&strictbounds&location=${Lat},${Long}&radius=12000`;
        try {
            const result = await fetch(apiUrl);
            const json = await result.json();
            setData({
                ...data,
                loadingLocation: false,
                Address: Address,
                hideResults: Address === '' ? true : false,
                editAddress: true,
                predictions: json.predictions
            })
            // setModalCity(true)
        } catch (err) {
            setData({
                ...data,
                loadingLocation: Address === '' ? false : true,
                Address: Address,
                hideResults: Address === '' ? true : false,
                editAddress: true,
                predictions: [1]
            })
            // setModalCity(false)
        }
    }

    function onRegionChange(region) {
        setData({
            ...data,
            Lat: region.latitude,
            Long: region.longitude,
            latitudeDelta: region.latitudeDelta,
            longitudeDelta: region.longitudeDelta,
        })
    }
    async function onSavePin() {
        setData({ ...data, loading: true })
        const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${data.Lat},${data.Long}&key=${Config.GOOGLE_API_KEY}&result_type=country`;
        try {
            const result = await fetch(apiUrl);
            const json = await result.json();
            setData({ ...data, loading: false })
            if (json.results.length > 0 && json.results[0].address_components.length > 0 && json.results[0].address_components[0].short_name === data.CountryCode.toUpperCase()) {
                setData({ ...data, isEdit: false, loading: false })
            } else {
                Alert.alert(
                    convertLanguage(language, 'pin_error'),
                    convertLanguage(language, 'select_location_fail'),
                    [
                        {
                            text: convertLanguage(language, 'ok'),
                            style: "cancel",
                            cancelable: true,
                        },
                    ],
                );
                // alert(convertLanguage(language, 'select_location_fail'))
            }
        } catch (err) {
            setData({ ...data, isEdit: false, loading: false })
        }
    }
    function onPressToMyLocation() {
        Geolocation.getCurrentPosition((position) => {
            setData({
                ...data,
                Lat: position.coords.latitude,
                Long: position.coords.longitude,
            })
            var region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: data.latitudeDelta,
                longitudeDelta: data.longitudeDelta
            }
            setTimeout(() => map?.current?.animateToRegion(region), 10);
        })
    }
    function onPressZoomIn() {
        var region = {
            latitude: parseFloat(data.Lat),
            longitude: parseFloat(data.Long),
            latitudeDelta: data.latitudeDelta / 2,
            longitudeDelta: data.longitudeDelta / 2
        }
        setData({
            ...data,
            latitudeDelta: data.latitudeDelta / 2,
            longitudeDelta: data.longitudeDelta / 2
        })
        setTimeout(() => map?.current?.animateToRegion(region), 10);
    }
    function onPressZoomOut() {
        var region = {
            latitude: parseFloat(data.Lat),
            longitude: parseFloat(data.Long),
            latitudeDelta: data.latitudeDelta * 2,
            longitudeDelta: data.longitudeDelta * 2
        }
        setData({
            ...data,
            latitudeDelta: data.latitudeDelta * 2,
            longitudeDelta: data.longitudeDelta * 2
        })
        // map.animateToRegion(region, 1000);
        setTimeout(() => map?.current?.animateToRegion(region), 10);
    }
    const changeOpenTime = (index) => {
        let dataFetch = [...openTime]
        dataFetch[index].toogle = !dataFetch[index].toogle
        setOpenTime(dataFetch)
    }
    const [modalDateAndTime, setModalDateAndTime] = useState({
        index: [0, 'start'],
        toogle: false
    })
    const handleConfirmStart = (date) => {
        // setErrors({ ...errors, startTime: false })
        // if (data.startTime !== '' && new Date(moment(date).format('YYYY-MM-DD HH:mm')).getTime() !== new Date(moment(data.startTime).format('YYYY-MM-DD HH:mm')).getTime()) {
        //   setState({ ...state, dataRepeat: [] })
        // }
        // if (data.endTime !== '' && new Date(moment(data.endTime).format('YYYY-MM-DD HH:mm')).getTime() < new Date(moment(date).format('YYYY-MM-DD HH:mm')).getTime()) {
        //   setData({ ...data, startTime: moment(date).format('YYYY-MM-DD HH:mm'), endTime: '' })
        // } else {
        //   setData({ ...data, startTime: moment(date).format('YYYY-MM-DD HH:mm') })

        // }
        let dataFetch = [...openTime]
        if (modalDateAndTime.index[1] === 'start') {
            dataFetch[modalDateAndTime.index[0]].startTime = moment(date).format('HH:mm')
        } else {
            dataFetch[modalDateAndTime.index[0]].endTime = moment(date).format('HH:mm')
        }
        setOpenTime(dataFetch)
        hideDatePickerStart();
    };
    const hideDatePickerStart = () => {
        setModalDateAndTime({ ...modalDateAndTime, toogle: false })
    };
    const showPopover = () => {
        setContacInfor({ ...contactInfor, toogle: true });
    }

    const closePopover = () => {
        setContacInfor({ ...contactInfor, toogle: false });
    }
    const renderLoading = () => {
        return <ActivityIndicator size="large" color="#000000" />
    }

    const pickThumbPoster = () => {
        ImagePicker.openPicker({
            mediaType: 'photo',
            includeBase64: true,
        }).then(image1 => {
            ImagePicker.openCropper({
                path: image1.path,
                width: image1.width,
                height: image1.width * 9 / 32,
                includeBase64: true,
                cropping: true
            }).then(image2 => {
                setImage({ ...image, posterCrop: 'data:image/png;base64,' + image2.data }) // poster: 'data:image/png;base64,' + image1.data,

            });
        }).catch(e => { });
    }

    const pickThumbLogo = () => {
        ImagePicker.openPicker({
            mediaType: 'photo',
            includeBase64: true,
        }).then(image1 => {
            ImagePicker.openCropper({
                path: image1.path,
                width: image1.width,
                height: image1.width,
                includeBase64: true,
                cropping: true
            }).then(image2 => {
                // setInitial({
                //     ...initial,
                //     isChange: true,
                // })
                // setPosterBaseCrop('data:image/png;base64,' + image2.data);
                // setPosterBase('data:image/png;base64,' + image.data);
                // setErrors({ ...errors, setPosterBaseCropVl: false, setPosterBaseVl: false })
                setImage({ ...image, logo: 'data:image/png;base64,' + image1.data, logoCrop: 'data:image/png;base64,' + image2.data })
            });
        }).catch(e => { });
    }

    const onApply = () => {
        let TagString = [],
            TimeDayOpen = []
        data.selected.forEach(element => {
            TagString.push(element.value)
        });

        openTime.forEach(element => {
            if (element.toogle === true) {
                TimeDayOpen.push({
                    Code: element.value.toLowerCase(),
                    TimeStart: element.startTime,
                    TimeEnd: element.endTime
                })
            }
        });
        var body = {
            PosterBase: image.posterCrop,
            LogoBase: image.logoCrop,
            Name: data.name,
            Phone: contactInfor.phone,
            Zipcode: contactInfor.zipcode,
            Email: contactInfor.email,
            Address: data.Address,
            AreaId: data.AreaId,
            Lat: data.Lat,
            Long: data.Long,
            TagString: JSON.stringify(TagString),
            ManagerName: contactInfor.manageName,
            Code: data.venueId,
            TimeDayOpen,
            Description: contactInfor.Detail
        }
        // HostInfoId
        // if (props?.route?.params?.HostInfoId)
        if (props?.route?.params?.edit !== undefined && props?.route?.params?.edit) {
            dispatch(actEditVenue(body, venue?.venue?.Id)).then(data => {
                if (data?.status === 200) {
                    props?.route?.params?.loadAgain()
                    props.navigation.goBack()
                }
            })
        } else {
            setLoading(true)
            dispatch(actCreateVenue(body, props?.route?.params?.HostInfoId)).then(data => {
                if (data?.status === 200) {
                    props?.navigation.replace('VenueDetail', { Id: data?.id, New: true })
                }
                setLoading(false)
            })
        }


    }

    const onValidate = () => {
        let mailformat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        let poster = false
        let logo = false
        let name = false
        let venueId = false
        let category = false
        let city = false
        let address = false
        let manageName = false
        let phone = false
        let email = false
        let open_time = false
        let phoneString = ''
        let emailString = ''
        if (contactInfor.manageName !== '') {
            manageName = false
        } else {
            manageName = true
            scrollView?.current?.scrollTo({ x: 0, y: 1250 * scale, animated: true })
        }

        if (contactInfor.phone !== '') {
            if (contactInfor.phone.length > 8 && contactInfor.phone.length < 12) {
                phone = false
                phoneString = ''
            } else {
                phone = true
                phoneString = convertLanguage(language, 'phone_min')
                scrollView?.current?.scrollTo({ x: 0, y: 1250 * scale, animated: true })
            }

        } else {
            phone = true
            phoneString = ''
            scrollView?.current?.scrollTo({ x: 0, y: 1250 * scale, animated: true })
        }

        if (contactInfor.email !== '') {
            if (contactInfor.email.match(mailformat)) {
                email = false
                emailString = ''
            } else {
                email = true
                emailString = convertLanguage(language, 'error_email')
                scrollView?.current?.scrollTo({ x: 0, y: 1250 * scale, animated: true })
            }
        } else {
            // setError({ ...error, email: true })
            email = true
            emailString = ''
            scrollView?.current?.scrollTo({ x: 0, y: 1250 * scale, animated: true })
        }


        //TimeOpen
        var openTimeFetch = openTime.filter(data => data.toogle === true)
        if (openTimeFetch.length > 0) {
            var openTimeCheck = openTimeFetch.filter(data => data.startTime === "start" || data.endTime === "finish")
            if (openTimeCheck.length > 0) {
                open_time = true
                scrollView?.current?.scrollTo({ x: 0, y: 880 * scale, animated: true })
            } else {
                open_time = false
            }
        } else {
            open_time = false
        }
        // 



        if (data.city !== '') {
            city = false
        } else {
            city = true
            scrollView?.current?.scrollTo({ x: 0, y: 490 * scale, animated: true })
        }

        if (data.Address !== '') {
            address = false
        } else {
            address = true
            scrollView?.current?.scrollTo({ x: 0, y: 490 * scale, animated: true })
        }

        if (data.name === '') {
            name = true
            scrollView?.current?.scrollTo({ x: 0, y: 230 * scale, animated: true })
        } else {
            name = false
        }

        if (image.posterCrop !== '') {
            poster = false
        } else {
            poster = true
            scrollView?.current?.scrollTo({ x: 0, y: 0 * scale, animated: true })
        }

        if (image.logoCrop !== '') {
            logo = false
        } else {
            logo = true
            scrollView?.current?.scrollTo({ x: 0, y: 0 * scale, animated: true })
        }

        if (data.venueId === '') {
            venueId = true
            scrollView?.current?.scrollTo({ x: 0, y: 0 * scale, animated: true })
        } else {
            venueId = false
        }

        if (data.listCategory !== '') {
            category = false
        } else {
            category = true
            scrollView?.current?.scrollTo({ x: 0, y: 0 * scale, animated: true })
        }



        setErrors({
            ...errors,
            poster,
            logo,
            name,
            venueId,
            category,
            AreaName: city,
            address,
            manageName,
            phone,
            email,
            openTime: open_time,
            phoneString, emailString
        })
        if (open_time === false && poster === false && logo === false && name === false && venueId === false && category === false && city === false && address === false && manageName === false && phone === false && email === false) {
            onApply()
        }
    }

    const goBackAction = () => {
        Alert.alert(
            convertLanguage(language, 'goBackConfirm'),
            "",
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    style: "destructive"
                },
                {
                    text: convertLanguage(language, 'ok'), onPress: () => {
                        props.navigation.goBack()
                    }
                }
            ],
            { cancelable: false })
    }
    return (
        <View style={{ backgroundColor: Colors.BG, flex: 1 }}>

            <SafeAreaConsumer>
                {insets => <View style={{ paddingTop: insets.top }} />}
            </SafeAreaConsumer>

            <View style={{ backgroundColor: Colors.BG, flex: 1, marginHorizontal: 16 * scale }}>
                {loading &&
                    <View style={{ backgroundColor: 'rgba(255, 255, 255, 0.6)', zIndex: 999, width: width, height: height, marginLeft: - 16 * scale, position: 'absolute' }}>
                        <ActivityIndicator size="large" color="#000000" style={{ alignSelf: 'center', marginTop: height / 2 }} />
                    </View>
                }
                <View style={{}}>
                    <Touchable
                        onPress={() => goBackAction()}
                        style={{
                            minWidth: 48 * scale, minHeight: 48 * scale, alignSelf: 'flex-start',
                            justifyContent: 'center'
                        }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 18 * scale, height: 18 * scale }} />
                    </Touchable>
                </View>

                <Line />
                {/* <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={{ flexGrow: 1 }}> */}
                {/* onScroll={() => Platform.OS === 'ios' ? Keyboard.dismiss() : null} */}
                <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled' ref={scrollView} style={{ flex: 1 }}>
                    <KeyboardAwareScrollView>
                        <View style={styles.boxImageThumb}>
                            {
                                image.posterCrop === '' ?
                                    <>
                                        <View></View>
                                        <Touchable style={[styles.boxAddImage, { borderColor: errors.poster ? 'red' : Colors.PRIMARY, height: 96 * scale }]} onPress={() => pickThumbPoster()}>
                                            <Image source={require('../../assets/photo_add.png')} style={[styles.imgAddThumb, { tintColor: errors.poster ? 'red' : Colors.PRIMARY, marginRight: 5 * scale, marginLeft: 35 * scale }]} resizeMode='contain' />
                                            <Text style={[styles.txtAddImage, {
                                                color: errors.poster ? 'red' : Colors.PRIMARY, fontStyle: 'normal',
                                                fontWeight: 'normal',
                                                fontSize: 12 * scale / scaleFontSize
                                            }]}>{convertLanguage(language, 'add_venue')}</Text>
                                        </Touchable>
                                    </>
                                    :
                                    <Touchable onPress={() => pickThumbPoster()}>
                                        <Image source={{ uri: image.posterCrop }} style={{
                                            height: 92 * scale,
                                            borderRadius: 4 * scale
                                        }} />
                                        <View style={{
                                            position: 'absolute', bottom: 8 * scale, right: 8 * scale,
                                            flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
                                            backgroundColor: 'rgba(0, 0, 0, 0.5)',
                                            borderRadius: 32,
                                            height: 30 * scale,
                                            width: 130 * scale,
                                            // paddingHorizontal: 6 * scale
                                        }}>
                                            <Image source={require('../../assets/EditImage2.png')} style={{ marginLeft: 3.8 * scale, marginRight: 9.5 * scale, width: 15 * scale, height: 13.5 * scale, resizeMode: 'cover' }} />
                                            <Text style={{
                                                color: '#FFFFFF',
                                                fontStyle: 'normal',
                                                fontWeight: 'normal',
                                                fontSize: 12 * scale / scaleFontSize,
                                                lineHeight: 15,
                                            }}>{convertLanguage(language, 'edit_poster_venue')}</Text>
                                        </View>


                                    </Touchable>
                            }
                            <View style={[{ flex: 1, flexDirection: 'column', position: 'absolute', height: 96 * scale, width: 96 * scale, bottom: -40 * scale, left: 16 * scale }]}>
                                <View style={{ backgroundColor: '#FFFFFF', width: '100%' }}>
                                    {
                                        image.logoCrop === '' ?
                                            <>
                                                <View></View>
                                                <Touchable style={[styles.boxAddImage, { borderColor: errors.poster ? 'red' : Colors.PRIMARY, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: '100%', height: '100%', padding: 10 * scale, borderRadius: 4 * scale }]} onPress={() => pickThumbLogo()}>
                                                    <Image source={require('../../assets/photo_add.png')} style={[styles.imgAddThumb, { tintColor: errors.poster ? 'red' : Colors.PRIMARY, marginBottom: 2 * scale }]} resizeMode='contain' />
                                                    <Text style={[styles.txtAddImage, {
                                                        color: errors.poster ? 'red' : Colors.PRIMARY, fontStyle: 'normal',
                                                        fontWeight: 'normal',
                                                        fontSize: 12 * scale / scaleFontSize,
                                                        textAlign: 'center'
                                                    }]}>{convertLanguage(language, 'add_photo')}</Text>
                                                </Touchable>
                                            </>
                                            :
                                            <Touchable style={{ height: 96 * scale, width: 96 * scale, borderRadius: 4, backgroundColor: '#FFFFFF', justifyContent: 'center', alignItems: 'center' }} onPress={() => pickThumbLogo()}>
                                                <Image source={{ uri: image.logoCrop }} style={{
                                                    height: 94 * scale,
                                                    width: 94 * scale,
                                                    // borderRadius: 4 * scale
                                                }} resizeMode='cover' />
                                                <View style={{
                                                    position: 'absolute',
                                                    bottom: 0 * scale, right: -135 * scale,
                                                    flexDirection: 'row',
                                                    justifyContent: 'center', alignItems: 'center',
                                                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                                                    borderRadius: 32,
                                                    height: 30 * scale,
                                                    width: 130 * scale,
                                                    paddingHorizontal: 6 * scale
                                                }}>
                                                    <Image source={require('../../assets/EditImage2.png')} style={{ marginLeft: 8 * scale, marginRight: 9.5 * scale, width: 15 * scale, height: 13.5 * scale }} />
                                                    <Text style={{
                                                        color: '#FFFFFF',
                                                        fontStyle: 'normal',
                                                        fontWeight: 'normal',
                                                        fontSize: 12 * scale / scaleFontSize,
                                                        marginRight: 10 * scale,
                                                        lineHeight: 15 * scale,
                                                    }}>{convertLanguage(language, 'edit_logo_venue')}</Text>
                                                </View>

                                            </Touchable>
                                    }
                                </View>
                            </View>
                        </View>

                        <View style={{ marginTop: 24 * scale }}>
                            <View style={{}}>
                                <Text style={styles.titleContex}>{convertLanguage(language, 'name_category')}</Text>
                                <TextField
                                    autoCorrect={false}
                                    enablesReturnKeyAutomatically={true}
                                    value={data.name}
                                    onChangeText={(value) => { setData({ ...data, name: value }), data.name !== '' ? setErrors({ ...errors, name: false }) : "" }}
                                    error={errors.name}
                                    returnKeyType='next'
                                    label={convertLanguage(language, 'venue_name')}
                                    baseColor={'#828282'}
                                    tintColor={'#828282'}
                                    errorColor={'#EB5757'}
                                    inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.name ? '#EB5757' : '#BDBDBD' }]}
                                    containerStyle={styles.containerStyle}
                                    // labelHeight={25 * scale}
                                    labelTextStyle={{ paddingBottom: 15 * scale, paddingLeft: 12 * scale }}
                                    lineWidth={2 * scale}
                                    selectionColor={'#3a3a3a'}
                                    style={styles.input}
                                    // fontSize={16 * scale}
                                    errorImage={require('../../assets/error.png')}

                                />

                                <TextField
                                    disabled={props?.route?.params?.edit !== undefined && props?.route?.params?.edit ? true : false}
                                    autoCorrect={false}
                                    enablesReturnKeyAutomatically={true}
                                    value={data.venueId}
                                    onChangeText={(value) => { setData({ ...data, venueId: value }), data.venueId !== '' ? setErrors({ ...errors, venueId: false }) : '' }}
                                    error={errors.venueId}
                                    returnKeyType='next'
                                    label={convertLanguage(language, 'venue_id')}
                                    baseColor={'#828282'}
                                    tintColor={'#828282'}
                                    errorColor={'#EB5757'}
                                    inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.venueId ? '#EB5757' : '#BDBDBD' }]}
                                    containerStyle={styles.containerStyle}
                                    // labelHeight={25 * scale}
                                    labelTextStyle={{ paddingBottom: 15 * scale, paddingLeft: 12 * scale }}
                                    lineWidth={2 * scale}
                                    selectionColor={'#3a3a3a'}
                                    style={styles.input}
                                    // fontSize={16 * scale}
                                    errorImage={require('../../assets/error.png')}

                                />


                                <Touchable style={{}}
                                    onPress={() => setModal({ ...modal, category: !modal.category })}
                                    style={{}}
                                >
                                    <TextField
                                        autoCorrect={false}
                                        enablesReturnKeyAutomatically={true}
                                        value={data.listCategory}
                                        error={errors.category}
                                        returnKeyType='next'
                                        label={convertLanguage(language, 'category')}
                                        baseColor={'#828282'}
                                        tintColor={'#828282'}
                                        errorColor={'#EB5757'}
                                        inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.category ? '#EB5757' : '#BDBDBD' }]}
                                        containerStyle={styles.containerStyle}
                                        // labelHeight={25 * scale}
                                        labelTextStyle={{ paddingBottom: 15 * scale, paddingLeft: 12 * scale }}
                                        lineWidth={2 * scale}
                                        selectionColor={'#3a3a3a'}
                                        style={styles.input}
                                        // fontSize={16 * scale}
                                        editable={false}
                                    // errorImage={require('../../assets/error.png')}
                                    />
                                    <Image source={require('../../assets/ic_category_blue.png')} resizeMode='contain' style={{ width: 16 * scale, height: 16 * scale, tintColor: errors.category ? '#EB5757' : '#4F4F4F', position: 'absolute', right: 8 * scale, top: '23%' }} />

                                </Touchable>
                            </View>
                            <View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={styles.titleContex}>{convertLanguage(language, 'location')}</Text>
                                </View>
                                <Touchable
                                    onPress={() => setModal({ ...modal, city: !modal.city })}
                                >
                                    <TextField
                                        autoCorrect={false}
                                        enablesReturnKeyAutomatically={true}
                                        value={data.AreaName}
                                        error={errors.AreaName}
                                        returnKeyType='next'
                                        label={convertLanguage(language, 'city')}
                                        baseColor={'#828282'}
                                        tintColor={'#828282'}
                                        errorColor={'#EB5757'}
                                        inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.AreaName ? '#EB5757' : '#BDBDBD' }]}
                                        containerStyle={styles.containerStyle}
                                        // labelHeight={25 * scale}
                                        labelTextStyle={{ paddingBottom: 15 * scale, paddingLeft: 12 * scale }}
                                        lineWidth={2 * scale}
                                        // fontSize={16 * scale}
                                        selectionColor={'#3a3a3a'}
                                        style={styles.input}
                                        editable={false}
                                        errorImage={require('../../assets/error.png')}
                                    />

                                </Touchable>
                                <TextField
                                    autoCorrect={false}
                                    enablesReturnKeyAutomatically={true}
                                    disabled={data.AreaId !== '' ? false : true}
                                    disabledLineWidth={0}
                                    // disabledLineType="solid"
                                    value={address}
                                    onChangeText={Address => { setAddress(Address), setErrors({ ...errors, address: false }) }}
                                    error={errors.address}
                                    returnKeyType='next'
                                    label={convertLanguage(language, 'address')}
                                    baseColor={data.AreaId !== '' ? '#828282' : '#5F666F'}
                                    tintColor={'#828282'}
                                    errorColor={'#EB5757'}
                                    inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.address ? '#EB5757' : '#BDBDBD', backgroundColor: data.AreaId !== '' ? '#FFFFFF' : '#F2F2F2' }]}
                                    containerStyle={[styles.containerStyle]}
                                    // labelHeight={25 * scale}
                                    labelTextStyle={{ paddingBottom: 15 * scale, paddingLeft: 12 * scale }}
                                    lineWidth={2 * scale}
                                    // fontSize={16 * scale}
                                    selectionColor={'#3a3a3a'}
                                    style={styles.input}
                                    errorImage={require('../../assets/error.png')}
                                />
                                <Autocomplete
                                    setKeyboard={() => onFocusAddress()}
                                    autoCapitalize="none"
                                    // editable={data.AreaName != ''}
                                    autoCorrect={false}
                                    hideResults={data.hideResults}
                                    containerStyle={{ marginTop: -17, padding: 0 }}
                                    inputContainerStyle={{ margin: 0 * scale, borderColor: 'white' }}
                                    style={[styles.txtContent, { padding: 0 }]}
                                    data={data.predictions}
                                    underlineColorAndroid='transparent'
                                    onFocus={() => setData({ ...data, focus: 'suggest' })}
                                    renderTextInput={() => { <View style={{}}></View> }}
                                    renderItem={dataFetch => (
                                        <Touchable onPress={() => onSelectLocation(dataFetch)} style={[styles.btnSelect, { zIndex: 999 }]}>
                                            <Text>{dataFetch.item.description}</Text>
                                        </Touchable>
                                    )}
                                />
                                {
                                    data.loadingLocation &&
                                    <ActivityIndicator size="large" color="#000000" />
                                }
                                {/* {
                                    (!data.loadingLocation && data.Address != '' && address != '' && data.predictions.length === 0 && data.editAddress) &&
                                    <Text style={{ fontSize: 16, color: '#333333', textAlign: 'center', paddingLeft: 15 * scale, paddingRight: 15 * scale }}>{convertLanguage(language, 'search_location')}</Text>
                                } */}
                                {
                                    data.Lat && data.Long ?
                                        <View style={styles.boxEditMap}>
                                            <Text style={styles.txtEditMap}>{convertLanguage(language, 'please_check_the_pin_location_is_correct_on_map')}</Text>
                                            <Touchable style={styles.btnEdit} onPress={() => onEditLocation()}>
                                                <Text style={styles.txtEdit2}>{convertLanguage(language, 'edit_map_pin')}</Text>
                                            </Touchable>
                                        </View>
                                        :
                                        null
                                }
                                <View style={[{ height: 300 * scale, marginTop: 20 * scale, backgroundColor: '#E7E7E7', position: 'relative' }, data.isEdit ? { zIndex: 10 } : {}]}>
                                    {
                                        data.Lat && data.Long ?
                                            <React.Fragment>
                                                <MapView
                                                    provider={PROVIDER_GOOGLE}
                                                    initialRegion={{
                                                        latitude: parseFloat(data.Lat),
                                                        longitude: parseFloat(data.Long),
                                                        latitudeDelta: data.latitudeDelta,
                                                        longitudeDelta: data.longitudeDelta,
                                                    }}
                                                    style={{ height: 300 * scale, borderRadius: 6, }}
                                                    onRegionChangeComplete={(region) => onRegionChange(region)}
                                                    zoomEnabled={data.isEdit}
                                                    pitchEnabled={data.isEdit}
                                                    rotateEnabled={false}
                                                    scrollEnabled={data.isEdit}
                                                    ref={map}
                                                    {...{ [!data.isEdit ? 'pointerEvents' : null]: "none" }}
                                                    onMapReady={_onMapReady}
                                                />
                                                {
                                                    data.showPink &&
                                                    <View style={styles.markerFixed}>
                                                        <Image style={styles.marker} source={flagPinkImg} />
                                                    </View>
                                                }

                                                {
                                                    data.isEdit &&
                                                    <TouchableOpacity style={styles.boxPinLocation} onPress={() => onSavePin()} disabled={data.loading}>
                                                        <Text style={styles.txtPinLocation}>{convertLanguage(language, 'save_pin_location')}</Text>
                                                        {
                                                            data.loading &&
                                                            <ActivityIndicator size='small' color='#FFFFFF' style={{ marginLeft: 5 * scale }} />
                                                        }
                                                    </TouchableOpacity>
                                                }
                                                {
                                                    data.isEdit &&
                                                    <View style={styles.boxAction}>
                                                        {
                                                            data.showButtonMyLocation &&
                                                            <Touchable style={styles.btnMyLocation} onPress={() => onPressToMyLocation()}>
                                                                <Image source={require('../../assets/my_location.png')} style={styles.icMyLocation} />
                                                            </Touchable>
                                                        }
                                                        <TouchableOpacity style={styles.btnPlus} onPress={() => onPressZoomIn()}>
                                                            <Image source={require('../../assets/plus.png')} style={styles.icPlus} />
                                                        </TouchableOpacity>
                                                        <TouchableOpacity style={styles.btnMinus} onPress={() => onPressZoomOut()}>
                                                            <Image source={require('../../assets/minus.png')} style={styles.icMinus} />
                                                        </TouchableOpacity>
                                                    </View>
                                                }
                                            </React.Fragment>
                                            :
                                            null
                                    }
                                </View>
                            </View>
                            <View style={{ marginVertical: 21 * scale }}>
                                {/* 333333 */}
                                <Text style={[styles.titleContex, { marginBottom: 10 * scale, color: errors.openTime ? '#EB5757' : '#333333' }]}>{convertLanguage(language, 'open_time')}</Text>
                                {
                                    openTime.map((item, index) => {
                                        return (
                                            <React.Fragment key={index}>
                                                <View style={{ flexDirection: 'row', marginBottom: 16 * scale }}>
                                                    <TouchableOpacity onPress={() => changeOpenTime(index)} style={[styles.btnOpenTime, { borderColor: item.toogle ? '#00A9F4' : '#FFFFFF', backgroundColor: item.toogle ? '#FFFFFF' : '#F2F2F2' }]}>
                                                        <Text numberOfLines={1} style={[styles.txtOpenTime, { color: item.toogle ? '#00A9F4' : '#5F666F' }]}>{convertLanguage(language, item.value)}</Text>
                                                    </TouchableOpacity>
                                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1 }}>
                                                        <TouchableOpacity style={{ opacity: item.toogle ? 1 : 0.4 }} onPress={() => setModalDateAndTime({ index: [index, 'start'], toogle: true })} disabled={!item.toogle}>
                                                            <View pointerEvents='none'>
                                                                <TextInput
                                                                    style={[styles.inputOpenTimeActive]}
                                                                    editable={false}
                                                                    // onChangeText={text => onChangeText(text)}
                                                                    value={item.startTime === 'start' ? convertLanguage(language, item.startTime) : item.startTime}
                                                                />
                                                                <Image source={require('../../assets/Icon.png')} style={{ width: 16 * scale, height: 16 * scale, position: 'absolute', right: 10 * scale, top: 8 * scale }} />
                                                            </View>
                                                        </TouchableOpacity>
                                                        <TouchableOpacity style={{ opacity: item.toogle ? 1 : 0.4 }} onPress={() => setModalDateAndTime({ index: [index, 'end'], toogle: true })} disabled={!item.toogle}>
                                                            <View pointerEvents='none'>
                                                                <TextInput
                                                                    style={styles.inputOpenTimeActive}
                                                                    editable={false}
                                                                    // onChangeText={text => onChangeText(text)}
                                                                    value={item.endTime === "end" ? convertLanguage(language, item.endTime) : item.endTime}
                                                                />
                                                                <Image source={require('../../assets/Icon.png')} style={{ width: 16 * scale, height: 16 * scale, position: 'absolute', right: 10 * scale, top: 8 * scale }} />
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>

                                            </React.Fragment>
                                        )
                                    })
                                }
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={styles.titleContex}>{convertLanguage(language, 'venue_contact')}</Text>
                                </View>
                                <View>
                                    <TextField
                                        autoCorrect={false}
                                        enablesReturnKeyAutomatically={true}
                                        value={contactInfor.manageName}
                                        onChangeText={(manageName) => { setContacInfor({ ...contactInfor, manageName }), setErrors({ ...errors, manageName: false }) }}
                                        error={errors.manageName}
                                        returnKeyType='next'
                                        label={convertLanguage(language, 'manager_name') + '*'}
                                        baseColor={'#828282'}
                                        tintColor={'#828282'}
                                        errorColor={'#EB5757'}
                                        inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.manageName ? '#EB5757' : '#BDBDBD' }]}
                                        containerStyle={styles.containerStyle}
                                        // labelHeight={25 * scale}
                                        labelTextStyle={{ paddingBottom: 15 * scale, paddingLeft: 12 * scale }}
                                        lineWidth={2 * scale}
                                        selectionColor={'#3a3a3a'}
                                        style={styles.input}
                                        errorImage={require('../../assets/error.png')}
                                    />
                                    <View style={{ flexDirection: 'row', flex: 1 }}>
                                        <TouchableOpacity
                                            onPress={() => showPopover()}
                                            ref={touchable}
                                            style={{
                                                borderWidth: 1,
                                                borderColor: '#bdbdbd',
                                                // paddingBottom: 6 * scale, paddingTop: 6 * scale,
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                marginRight: 10 * scale,
                                                height: 43.4 * scale,
                                                borderRadius: 4 * scale,
                                                paddingHorizontal: 12 * scale,
                                                // marginVertical: 5 * scale,
                                                width: 100 * scale, flex: 1 / 4,
                                                alignItems: 'center'
                                            }}>
                                            <View style={{ justifyContent: 'flex-start' }}>
                                                <Text style={{ color: '#828282', fontSize: 10 * scale / scaleFontSize, marginTop: 0 * scale }}>{convertLanguage(language, 'country')}</Text>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', marginTop: -3 * scale }}>
                                                    <Image resizeMode="contain" style={[styles.ic_country, { width: 18 * scale, height: 18 * scale, marginTop: 2 * scale }]} source={contactInfor.zipcode === '+84' ? require('../../assets/flag_vn.png') : contactInfor.zipcode === '+66' ? require('../../assets/flag_th.png') : require('../../assets/flag_kr.png')} />
                                                    <Text style={[styles.txtCountry, { marginTop: 2 * scale }]}>{contactInfor.zipcode}</Text>
                                                </View>
                                            </View>
                                            <Image source={require('../../assets/down_arrow_active2.png')} style={{
                                                width: 12 * scale,
                                                height: 12 * scale,
                                                marginTop: 8 * scale
                                            }} resizeMode="contain" />
                                        </TouchableOpacity>
                                        <View style={{ flex: 3 / 4 }}>
                                            <TextField
                                                autoCorrect={false}
                                                enablesReturnKeyAutomatically={true}
                                                value={contactInfor.phone}
                                                onChangeText={(phone) => {
                                                    if (!isNaN(phone)) {
                                                        setContacInfor({ ...contactInfor, phone })
                                                        setErrors({ ...errors, phone: false })
                                                    } else {
                                                        setErrors({ ...errors, phone: true })
                                                    }
                                                }}
                                                // onChangeText={(Phone) => this.setState({ Phone: Phone == 0 ? '' : Phone, isChange: true })}
                                                error={errors.phone}
                                                returnKeyType='next'
                                                label={convertLanguage(language, 'phone_number') + '*'}
                                                baseColor={'#828282'}

                                                // baseColor={this.state.Phone.length > 0 ? '#828282' : 'rgba(189,189,189,1)'}
                                                tintColor={'#828282'}
                                                errorColor={'#EB5757'}
                                                inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.phone ? '#EB5757' : '#BDBDBD' }]}
                                                // inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.firstname ? '#EB5757' : '#BDBDBD' }]}
                                                containerStyle={[styles.containerStyle]}
                                                // labelHeight={25 * scale}
                                                labelTextStyle={{ paddingBottom: 15 * scale }}
                                                lineWidth={2 * scale}
                                                selectionColor={'#3a3a3a'}
                                                style={styles.input}
                                                labelTextStyle={{ paddingLeft: 12 * scale }}
                                                errorImage={require('../../assets/error.png')}
                                                keyboardType="number-pad"
                                                maxLength={11}
                                                minLength={9}
                                            />
                                        </View>
                                    </View>
                                    {errors.phoneString !== '' &&
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{ flex: 3 / 5, }}>
                                            </View>
                                            <View style={{ flex: 4 / 5 }}>
                                                <Text style={{ fontSize: 12 * scale, color: 'red' }}>{errors.phoneString}</Text>
                                            </View>
                                        </View>
                                    }
                                    <TextField
                                        autoCorrect={false}
                                        enablesReturnKeyAutomatically={true}
                                        value={contactInfor.email}
                                        onChangeText={(email) => { setContacInfor({ ...contactInfor, email }), setErrors({ ...errors, email: false }) }}
                                        error={errors.email}
                                        returnKeyType='next'
                                        label={convertLanguage(language, 'email') + '*'}
                                        baseColor={'#828282'}
                                        tintColor={'#828282'}
                                        errorColor={'#EB5757'}
                                        inputContainerStyle={[styles.inputContainerStyle, { borderColor: errors.email ? '#EB5757' : '#BDBDBD' }]}
                                        containerStyle={styles.containerStyle}
                                        // labelHeight={25 * scale}
                                        labelTextStyle={{ paddingBottom: 15 * scale, paddingLeft: 12 * scale }}
                                        lineWidth={2 * scale}
                                        selectionColor={'#3a3a3a'}
                                        style={styles.input}
                                        errorImage={require('../../assets/error.png')}
                                    />
                                </View>
                                {errors.emailString !== '' &&
                                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 12 * scale, color: 'red' }}>{errors.emailString}</Text>
                                    </View>
                                }
                                <Text style={[styles.titleContex, { marginBottom: 10 * scale }]}>{convertLanguage(language, 'description')}</Text>
                                <View style={{
                                    height: 320 * scale,
                                }}>
                                    {/* <WebViewEditDetail /> */}
                                    <View style={styles.boxInput}>
                                        <TouchableOpacity style={styles.ipContent} onPress={() => props.navigation.navigate('WebViewEditDetail', { Detail: contactInfor.Detail, callback: (data2) => setContacInfor({ ...contactInfor, ...data2 }) })}>
                                            <View style={{ height: 262 * scale, padding: 8 * scale, overflow: 'hidden' }}>
                                                <AutoHeightWebView startInLoadingState={true} scalesPageToFit={false}
                                                    zoomable={false} scrollEnabled={false} renderLoading={renderLoading}
                                                    source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width - 66}px; overflow-wrap: break-word} iframe {width: ${width - 66}px; height: ${(width - 66) * 9 / 16}px} img {width: ${width - 66}px !important;height: auto !important;}</style><body>` + contactInfor.Detail.replace(/"\/\/\www.youtube.com/g, '"https://www.youtube.com') + `</body></html>` }}
                                                    originWhitelist={['*']} />
                                            </View>
                                            {
                                                contactInfor.Detail.length > 0 && contactInfor.Detail !== '<p><br></p>' && contactInfor.Detail !== '<br>' &&
                                                <Text style={styles.txtEdit} onPress={() => props.navigation.navigate('WebViewEditDetail', { Detail: contactInfor.Detail, callback: (data2) => setContacInfor({ ...contactInfor, ...data2 }) })}>+ {convertLanguage(language, 'edit')}</Text>

                                            }
                                        </TouchableOpacity>
                                    </View>


                                </View>

                                <TouchableOpacity style={styles.btnConfirmActive} onPress={() => onValidate()}>
                                    <Text style={styles.txtConfirmActive}>{props?.route?.params?.edit !== undefined && props?.route?.params?.edit ? convertLanguage(language, 'save') : convertLanguage(language, 'register')}</Text>
                                </TouchableOpacity>

                                <DateTimePickerModal
                                    isVisible={modalDateAndTime.toogle}
                                    mode="time"
                                    cancelTextIOS={convertLanguage(language, 'cancel')}
                                    confirmTextIOS={convertLanguage(language, 'confirm')}
                                    locale={language}
                                    format="HH:mm"
                                    onConfirm={handleConfirmStart}
                                    onCancel={hideDatePickerStart}
                                    headerTextIOS={''}
                                />
                                <Popover
                                    isVisible={contactInfor.toogle}
                                    animationConfig={{ duration: 0 }}
                                    fromView={touchable.current}
                                    placement='bottom'
                                    arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
                                    backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', }}
                                    onRequestClose={() => closePopover()}>
                                    <View style={styles.boxPopover}>
                                        <Touchable onPress={() => { setContacInfor({ ...contactInfor, zipcode: '+82', toogle: false }) }} style={styles.boxCountry}>
                                            <Image source={require('../../assets/flag_kr.png')} style={styles.ic_country} />
                                            <Text style={styles.txtCountry}>+82</Text>
                                        </Touchable>
                                        <Touchable onPress={() => { setContacInfor({ ...contactInfor, zipcode: '+84', toogle: false }) }} style={styles.boxCountry}>
                                            <Image source={require('../../assets/flag_vn.png')} style={styles.ic_country} />
                                            <Text style={styles.txtCountry}>+84</Text>
                                        </Touchable>
                                        <Touchable onPress={() => { setContacInfor({ ...contactInfor, zipcode: '+66', toogle: false }) }} style={styles.boxCountry}>
                                            <Image source={require('../../assets/flag_th.png')} style={styles.ic_country} />
                                            <Text style={styles.txtCountry}>+66</Text>
                                        </Touchable>
                                    </View>
                                </Popover>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </ScrollView>
                {/* </KeyboardAvoidingView> */}
            </View>
            {
                modal.category &&
                <ModalTagNew
                    closeModal={() => setModal({ ...modal, category: false })}
                    tags={showDataCategory()}
                    selectCategory={(category) => selectCategory(category)}
                    selected={data.selected}
                    onConfirm={() => { setData({ ...data, category_selected: data.selected, listCategory: showListCategory() }), setErrors({ ...errors, category: false }) }}
                />
            }
            {
                modal.city &&
                <ModalNewCity
                    modalVisible={modal.city}
                    closeModal={() => setModal({ ...modal, city: false })}
                    countries={countries}
                    selectCity={(city) => { onSelect(city), setErrors({ ...errors, AreaName: false }) }}
                // onConfirm={() => setdata({ ...initial, category_selected: initial.selected })}
                />
            }
        </View >
    )
}

const styles = StyleSheet.create({
    boxImageThumb: {
        marginBottom: 30 * scale,
    },
    boxAddImage: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderWidth: 2 * scale,
        borderStyle: 'dashed',
        borderRadius: 0.1,

    },
    txtAddImage: {
        fontSize: 16 * scale,
        fontWeight: '600'
    },
    imgAddThumb: { width: 15 * scale, height: 15 * scale, marginRight: 5 * scale, tintColor: Colors.PRIMARY },
    inputContainerStyle: {
        borderWidth: 1,
        borderRadius: 4 * scale,
        paddingHorizontal: 12 * scale,
        // height: 59 * scale
    },
    containerStyle: {
        // paddingHorizontal: 20,
        // flex: 1
        marginBottom: 10 * scale,
    },
    input: {
        color: '#333333',
        // fontSize: 5
        fontSize: 16 * scale / scaleFontSize
    },
    titleContex: {
        fontSize: 18 * scale,
        // fontWeight: '00',
        marginBottom: 10,
        color: '#333333',
        fontFamily: 'SourceSansPro-SemiBold',
        paddingBottom: 1 * scale
    },
    btnSelect: {
        backgroundColor: '#FFFFFF',
        paddingBottom: 10 * scale,
        paddingTop: 10 * scale,
        borderColor: '#bdbdbd',
        borderWidth: 1,
        paddingLeft: 5 * scale,
        paddingRight: 5 * scale,
        zIndex: 99999
    },
    txtContent: {
        fontSize: 15,
        color: '#333333',
        fontWeight: 'bold',
    },
    markerFixed: {
        left: '50%',
        marginLeft: -16 * scale,
        marginTop: -45 * scale,
        position: 'absolute',
        top: '50%'
    },
    marker: {
        height: 45 * scale,
        width: 32 * scale
    },
    boxPinLocation: {
        position: 'absolute',
        alignSelf: 'center',
        minWidth: 150 * scale,
        height: 36 * scale,
        backgroundColor: '#03a9f4',
        borderRadius: 4 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        top: 20 * scale,
        flexDirection: 'row',
        paddingHorizontal: 5 * scale
    },
    txtPinLocation: {
        color: '#FFFFFF',
        fontSize: 14,
        fontWeight: 'bold'
    },
    boxAction: {
        position: 'absolute',
        bottom: 15 * scale,
        right: 15 * scale
    },
    btnMyLocation: {
        width: 30 * scale,
        height: 30 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10 * scale
    },
    icMyLocation: {
        width: 20 * scale,
        height: 20 * scale
    },
    btnPlus: {
        width: 30 * scale,
        height: 30 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10 * scale
    },
    icPlus: {
        width: 18 * scale,
        height: 18 * scale
    },
    btnMinus: {
        width: 30 * scale,
        height: 30 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        marginTop: 10 * scale
    },
    icMinus: {
        width: 18 * scale,
        height: 18 * scale
    },
    boxEditMap: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10 * scale
    },
    txtEditMap: {
        fontSize: 14,
        color: '#757575',
        flex: 1,
        lineHeight: 20 * scale
    },
    btnEdit: {
        // width: 113 * scale,
        // height: 36 * scale,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#bdbdbd',
        paddingLeft: 10 * scale,
        paddingRight: 10 * scale,
        marginLeft: 10 * scale,
        paddingVertical: 5 * scale,
        borderRadius: 4,
        borderColor: '#4F4F4F'
    },
    txtEdit2: {
        fontSize: 13 * scale,
        // color: '#757575',
        fontWeight: 'normal',
        fontSize: 12 * scale,
        color: '#4F4F4F'
    },
    btnOpenTime: {
        width: 70 * scale,
        height: 32 * scale,
        borderWidth: 1,
        justifyContent: 'center',
        borderRadius: 4, alignItems: 'center',
        marginRight: 28 * scale
    },
    txtOpenTime: {
        fontWeight: 'normal', fontSize: 12 * scale / scaleFontSize, fontStyle: 'normal'
    },
    inputOpenTimeActive: {
        color: '#828282',
        paddingLeft: 10 * scale,
        borderRadius: 4, borderColor: '#BDBDBD',
        borderWidth: 1, width: 110 * scale,
        height: 32 * scale,
        fontSize: 15 * scale / scaleFontSize,
        paddingVertical: 1 * scale
    },
    boxPopover: {
        width: 100 * scale,
        padding: 12 * scale,
        alignItems: 'center'
    },
    boxCountry: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 6 * scale
    },
    ic_country: {
        width: 18 * scale,
        height: 18 * scale,
        marginRight: 8 * scale
    },
    txtCountry: {
        color: '#333333',
        fontSize: 16 * scale / scaleFontSize
    },
    txtEdit: {
        textAlign: 'center',
        color: '#03a9f4',
        fontSize: 14,
        fontFamily: 'SourceSansPro-SemiBold'
    },
    boxInput: {
        // paddingBottom: 24,
    },
    txtDetail: {
        fontSize: 14,
        color: '#bdbdbd',
    },
    ipContent: {
        fontSize: 15,
        color: '#333333',
        height: 300 * scale,
        overflow: 'hidden',
        borderWidth: 1,
        borderColor: '#bdbdbd',
        borderRadius: 4,
        justifyContent: 'space-between',
        overflow: 'hidden',
        paddingHorizontal: 8 * scale,
        paddingVertical: 12 * scale
        // textAlignVertical: 'top'
    },
    btnConfirmActive: {
        backgroundColor: '#00A9F4',
        width: 328 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 16 * scale,

    },
    txtConfirmActive: { fontSize: 16 * scale, lineHeight: 20 * scale, color: '#ffffff' },
})