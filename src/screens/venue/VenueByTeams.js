import React, { useEffect, useState } from 'react'
import {
  View, Text, TouchableOpacity, StyleSheet, SafeAreaView, Image, ActivityIndicator,
  FlatList
} from 'react-native'
import * as Colors from '../../constants/Colors'
import Line from '../../screens_view/Line'
import { useSelector, useDispatch } from 'react-redux';
import { actLoadDataVenueByTeam } from '../../actions/team'
import VenueListItem from '../../components/venue/VenueListItem'
import { actLikeVenues } from '../../actions/venue';

const VenueByTeams = (props) => {
  const { language } = useSelector(state => state.language)
  const [page, setPage] = useState(1)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(actLoadDataVenueByTeam(props.route.params['Slug'], 1))
  }, [])

  const venues = useSelector(state => state.team.venues)
  var { loadMoreVenue, is_empty } = useSelector(state => state.team);

  const _renderFooter = () => {
    if (loadMoreVenue) {
      return <ActivityIndicator size="large" color="#000000" style={{ padding: 20, flex: 1 }} />
    } else {
      if (is_empty) {
        return <Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'venue_empty')}</Text>
      } else {
        return null;
      }
    }
  }

  const loadMore = () => {
    if (loadMoreVenue) {
      dispatch(actLoadDataVenueByTeam(props.route.params['Slug'], page + 1))
      setPage(page + 1)
    }
  }

  const onChangeLike = (index = '', Id) => {
    dispatch(actLikeVenues(Id)).then(data => {
    })
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
      <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0 }}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
          <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
        </TouchableOpacity>
        <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
        <View style={{ minHeight: 48, marginRight: 20, justifyContent: 'center', alignItems: 'center' }}></View>
      </View>

      <Line />

      <View style={styles.content}>
        <FlatList
          style={{ paddingTop: 10 }}
          data={venues}
          renderItem={({ item, index }) => {
            return <VenueListItem item={item} navigation={props.navigation} index={index} onChangeLike={(index, Id) => onChangeLike(index, Id)} />
          }}
          onEndReached={() => { loadMore() }}
          onEndReachedThreshold={0.5}
          keyExtractor={(item, index) => index.toString()}
          ListFooterComponent={() => _renderFooter()}
        />
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
})

export default VenueByTeams