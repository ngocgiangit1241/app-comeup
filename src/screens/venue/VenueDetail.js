import React, { memo, useEffect, useRef, useState } from 'react';
import {
  ActivityIndicator, Alert,
  Animated, Dimensions, FlatList, Image, Linking, PixelRatio, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View
} from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import ImagePicker from 'react-native-image-crop-picker';
import ImageResizer from 'react-native-image-resizer';
import InsetShadow from 'react-native-inset-shadow';
import InstagramLogin from 'react-native-instagram-login';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Popover from 'react-native-popover-view';
import { SafeAreaConsumer } from 'react-native-safe-area-context';
import Share from 'react-native-share';
import { useDispatch, useSelector } from 'react-redux';
import { actGlobalCheckLogin } from '../../actions/global';
import { actTeamFollow } from '../../actions/team';
import {
  actDeleteVenueNews, actGetVenueDetail, actGetVenueDetailPhoto, actImagesIns, actLikeVenueNews, actLikeVenues, actLoadDataVenueNews, actRemoveTokenIns, actSetTokenIns, actUploadPhoto
} from '../../actions/venue';
import ModalActionInstagram from '../../components/team/ModalActionInstagram';
import ModalMoreVenue from '../../components/venue/ModalMoreVenue';
import ModalNew from '../../components/venue/ModalNew';
import ModalVenueNewsDetail from '../../components/venue/ModalVenueNewsDetail';
import ModalZoomImage from '../../components/venue/ModalZoomImage';
import VenueDetailItem from '../../components/venue/VenueDetailItem';
import VenueNewsItem from '../../components/venue/VenueNewsItem';
import * as Types from '../../constants/ActionType';
import * as Types2 from '../../constants/ActionType2';
import * as Colors from '../../constants/Colors';
import * as Config from '../../constants/Config';
import Line from '../../screens_view/Line';
import Loading from '../../screens_view/Loading';
import Touchable from '../../screens_view/Touchable';
import { convertDate3, convertLanguage } from '../../services/Helper';
const screenWidth = Dimensions.get('window').width;
const { width, height } = Dimensions.get('window');
// const screenHeight = Dimensions.get('window').height;
const scale = screenWidth / 360;

const LATITUDE_DELTA = 0.08;
const ASPECT_RATIO = 2;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const scaleFontSize = PixelRatio.getFontScale();

const VenueDetail = props => {
  const [firstTime, setFirstTime] = useState(false);
  useEffect(() => {
    return () => {
      dispatch({ type: Types2.CLEAR_IMAGES_INSTAGRAM });
      dispatch({ type: Types2.CLEAR_VENUE_DETAIL });
    };
  }, []);
  const { navigation } = props;
  // const fadeAnim = useRef(new Animated.Value(0)).current;
  const [fadeAnim, setFadeAnim] = useState(new Animated.Value(0));
  const likeRef = useRef('');
  const webviewRef = useRef(null)
  const { language } = useSelector(state => state.language);
  const dispatch = useDispatch();
  const refPopover = useRef(null);
  const [toogle, setToogle] = useState({
    popoverPhoto: false,
    modalMore: false,
  });
  const [checkLoadAgain, setCheckLoadAgain] = useState(false);
  const [modelVenueNews, setModelVenueNews] = useState(false);
  const [modalZoomImage, setModalZoomImage] = useState(false);
  const [zoomImageIndex2, setZoomImageIndex2] = useState(0);
  const [activeSlide, setActiveSlide] = useState(0);
  const [dataZoomImage, setDataZoomImage] = useState([]);
  const [modelActionInstagram, setModelActionInstagram] = useState(false);
  const [page, setPage] = useState(1);
  const [Role, setRole] = useState('');
  const [venue_news, setVenuenews] = useState({});
  const [VenueNewsId, setVenueNewsId] = useState({});
  const [modalVenueNewsDetail, setModalVenueNewsDetail] = useState(false);
  const [tokenInstagram, setTokenInstagram] = useState(null);
  // const [arrImgInstagram, setArrImgInstagram] = useState([]);
  const [zoomeImageIndex, setZoomeImageIndex] = useState(0);
  const [modalZoom, setModalZoom] = useState(false);
  const instagramLogin = useRef(null);
  const { iso_code } = useSelector(state => state.global);
  const carousel = useRef();
  const [showBottom, setShowBottom] = useState({
    value: 0,
    toogle: false,
  });
  var slides = [];
  useEffect(() => {
    if (props?.route?.params?.Id) {
      dispatch(actGetVenueDetail(props?.route?.params?.Id));
      // dispatch(actLoadDataVenueNews(props?.route?.params?.Id, page, iso_code))
    }
  }, [checkLoadAgain, props?.route?.params?.Id]);

  useEffect(() => {
    setFirstTime(true);
    if (props?.route?.params?.Id) {
      dispatch(actLoadDataVenueNews(props?.route?.params?.Id, page, iso_code));
    }
  }, []);

  //     const venue = useSelector(state => state.venue2)
  const venue = useSelector(state => state.venue);

  useEffect(() => {
    dispatch(actGetVenueDetailPhoto(props.route?.params?.Id));
  }, [venue?.checkPhotoDetail]);

  useEffect(() => {
    if (venue.venue.Id !== undefined) {
      dispatch(actImagesIns(venue.venue.Id));
    }
  }, [venue.venue]);
  const loadMore = venue.loadMore;
  const is_empty = venue.is_empty;
  const arrImgInstagram = venue.InstagramImages;
  const entriesSplitter = () => {
    const length = arrImgInstagram?.length || 0;
    if (length > 0) {
      for (let i = 0; i < length; i += 3) {
        let d = [];
        for (let jj = i; jj < i + 3; jj++) {
          if (jj < length) {
            d.push(arrImgInstagram[jj]);
          }
        }
        slides.push(d);
      }
    }
  };
  // render every single slide
  const _renderItem = ({ item, index }) => {
    const item1 = item[0];
    let item2 = item?.length >= 2 ? (item2 = item[1]) : null;
    let item3 = item?.length >= 3 ? (item3 = item[2]) : null;

    return (
      <View style={{ flexDirection: 'row', marginTop: 10 }} key={index}>
        {createInstagramView_Child_2(item1, index * 3)}
        {createInstagramView_Child_2(item2, index * 3 + 1)}
        {createInstagramView_Child_2(item3, index * 3 + 2)}
      </View>
    );
  };

  const createInstagramView_Child_2 = (item, index) => {
    if (item == null)
      return <View key={index} style={{ flex: 1, marginRight: 4 }} />;
    return (
      <TouchableOpacity
        key={index}
        style={{ flex: 1, marginRight: 4 }}
        onPress={() => {
          setZoomeImageIndex(index), setModalZoom(true);
        }}>
        <Image
          style={{
            width: 100 * scale,
            height: 100 * scale,
            marginLeft: 5 * scale,
          }}
          aspectRatio={1}
          source={{ uri: item.Thumbnail }}
        />
      </TouchableOpacity>
    );
  };

  const _renderFooter = () => {
    if (loadMore) {
      return (
        <ActivityIndicator size="large" color="#000000" style={{ padding: 20 * scale }} />
      );
    } else {
      if (is_empty) {
        return (
          <Text style={{ textAlign: 'center', paddingTop: 20 * scale }}>
            {convertLanguage(language, 'team_news_empty')}
          </Text>
        );
      } else {
        return null;
      }
    }
  };

  const loadMoreNews = () => {
    if (loadMore) {
      dispatch(
        actLoadDataVenueNews(props?.route?.params?.Id, page + 1, iso_code),
      );
      setPage(page + 1);
    }
  };

  const dataZoomImageFcn = () => {
    var images = [];
    dataZoomImage.forEach(image => {
      images.push({ Image: image.Photos.Large });
    });
    return images;
  };

  const onDeleteVenueNews = Id => {
    dispatch(actDeleteVenueNews(Id)).then(res => {
      if (res && res.status === 200) {
      }
    });
  };

  const onLikeVenueNews = Id => {
    dispatch(actLikeVenueNews(Id)).then(res => {
      if (res && res.status === 200) {
      }
    });
  };

  const onDeleteVenueNewsModal = () => {
    dispatch(actDeleteVenueNews(VenueNewsId)).then(res => {
      if (res && res.status === 200) {
        setModalVenueNewsDetail(false);
        setModelVenueNews(false);
        setVenuenews({});
      }
    });
  };

  const onOpenModelVenueNews = Id => {
    setModelVenueNews(true);
    setVenueNewsId(Id);
  };

  const onEditVenueNews = () => {
    setModelVenueNews(false);
    props.navigation.navigate('WriteVenueNews', { VenueNewsId: VenueNewsId });
  };

  const onOpenModelVenueNewsDetail = (item, Role) => {
    setModalVenueNewsDetail(true);
    setVenuenews(item);
    setRole(Role);
  };

  const closeModalVenueNewsDetail = () => {
    setModalVenueNewsDetail(false);
    setVenuenews({});
  };

  const setTokenIns = token => {
    dispatch(actSetTokenIns(venue.venue.Id, token));
  };

  const onRemoveInstagram = () => {
    setModelActionInstagram(false);
    const Slug = venue.venue.Slug;
    dispatch(actRemoveTokenIns(Slug));
  };

  const createNews = () => {
    return (
      <View style={{ marginBottom: 20 * scale }}>
        {((venue.venue_news && venue.venue_news.length > 0) ||
          (venue.venue.Role !== 'Guest' && venue.venue.Role !== 'Staff')) && (
            <Text
              style={{
                marginLeft: 16 * scale,
                marginTop: 30 * scale,
                color: '#333333',
                fontSize: 20 * scale,
                fontWeight: 'bold',
              }}>
              {convertLanguage(language, 'venue_news')}
            </Text>
          )}

        {venue.venue.Role !== 'Guest' && venue.venue.Role !== 'Staff' && (
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate('WriteVenueNews', {
                Slug: venue?.venue?.Id,
              })
            }
            style={{
              marginTop: 10 * scale,
              marginHorizontal: 16 * scale,
              marginBottom: 10 * scale,
              alignSelf: 'stretch',
              height: 36 * scale,
              borderColor: Colors.PRIMARY,
              borderWidth: 1,
              borderRadius: 4,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{ fontSize: 18 * scale, color: Colors.PRIMARY }}>
              {convertLanguage(language, 'write')}
            </Text>
          </TouchableOpacity>
        )}
        {venue.venue_news && venue.venue_news.length > 0 && (
          <FlatList
            style={{
              paddingLeft: 16 * scale,
              paddingRight: 16 * scale,
            }}
            data={venue.venue_news}
            renderItem={({ item, index }) => {
              return (
                <VenueNewsItem
                  navigation={props.navigation}
                  onDeleteVenueNews={Id => onDeleteVenueNews(Id)}
                  data={item}
                  onLikeVenueNews={Id => onLikeVenueNews(Id)}
                  // openModalAction={(Id) => onOpenModelVenueNews(Id)}
                  // zoomImage={(index, Images) => { setZoomImageIndex2(index), setModalZoomImage(true), setDataZoomImage(Images) }}
                  onOpenModelVenueNewsDetail={() =>
                    onOpenModelVenueNewsDetail(item, item.Venue.Role)
                  }
                />
              );
            }}
            // onEndReached={() => loadMoreNews()}
            onEndReachedThreshold={0.1}
            ListFooterComponent={() => _renderFooter()}
          />
        )}
      </View>
    );
  };

  const getPhotos = () => {
    if (venue?.photoDetail.length < 1) {
      return (
        <>
          {venue.venue.Role !== 'Guest' && (
            <>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginTop: 16 * scale,
                }}>
                <Text
                  style={{
                    color: '#4F4F4F',
                    fontSize: 16 * scale,
                    fontFamily: 'SourceSansPro-SemiBold',
                  }}>
                  {convertLanguage(language, 'photos')}
                </Text>
              </View>

              <TouchableOpacity
                onPress={() => uploadPhoto()}
                style={{
                  marginTop: 16 * scale,
                  width: '100%',
                  height: 36 * scale,
                  borderWidth: 1,
                  borderColor: '#00A9F4',
                  borderRadius: 4,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../../assets/photo_add.png')}
                  style={{
                    width: 18 * scale,
                    height: 18 * scale,
                    tintColor: Colors.PRIMARY,
                    marginRight: 8 * scale,
                  }}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    color: '#00A9F4',
                    fontSize: 16 * scale,
                    fontStyle: 'normal',
                    fontWeight: 'normal',
                  }}>
                  {convertLanguage(language, 'upload_photos')}
                </Text>
              </TouchableOpacity>
            </>
          )}
        </>
      );
    } else {
      return (
        <>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: 16 * scale,
            }}>
            <Text
              style={{
                fontFamily: 'SourceSansPro-SemiBold',
                fontSize: 16 * scale,
                color: '#4F4F4F',
              }}>
              {convertLanguage(language, 'photos')}
            </Text>
            <TouchableOpacity
              ref={refPopover}
              onPress={() =>
                setToogle({ ...toogle, popoverPhoto: !toogle.popoverPhoto })
              }>
              <Image
                source={require('../../assets/detail_dot.png')}
                style={{
                  alignSelf: 'flex-end',
                  width: 24 * scale,
                  height: 24 * scale,
                }}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <View>
            <FlatList
              contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
              data={venue?.photoDetail}
              renderItem={({ item, index }) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      props.navigation.navigate('VenueDetailPhoto', {
                        detailFull: true,
                        venueId: venue?.venue?.Id,
                        role: venue.venue.Role,
                        isLike: venue.venue.IsLike,
                        actLike: actLike,
                        actShare: () => share(),
                        upload: () => uploadPhoto(),
                      });
                    }}
                    style={{ paddingRight: 16 * scale, marginTop: 16 * scale }}>
                    <Image
                      style={{
                        backgroundColor: '#bcbcbc',
                        width:
                          venue?.photoDetail?.length === 1
                            ? (screenWidth - 50) * scale
                            : 254 * scale,
                        height: 142 * scale,
                        borderRadius: 4,
                      }}
                      source={{ uri: item?.Image?.Small }}
                    />
                  </TouchableOpacity>
                );
              }}
              keyExtractor={item => item.Id.toString()}
              horizontal={true}
              onEndReachedThreshold={0.1}
              showsHorizontalScrollIndicator={false}
              snapToInterval={(254 + 16) * scale} // Adjust to your content width
              snapToAlignment={'start'}
              decelerationRate={'fast'}
              pagingEnabled={Platform.OS === 'android'}
            />
          </View>
        </>
      );
    }
  };
  const getHostingEvents = () => {
    if (venue?.venue?.Events?.length < 1) {
      return (
        <>
          {venue.venue.Role !== 'Guest' && (
            <>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginTop: 16 * scale,
                }}>
                <Text
                  style={{
                    color: '#4F4F4F',
                    fontSize: (16 * scale) / scaleFontSize,
                    fontFamily: 'SourceSansPro-SemiBold',
                  }}>
                  {convertLanguage(language, 'hosting_event')}
                </Text>
              </View>
              <TouchableOpacity
                //  onPress={() => props.navigation.navigate('EventHostList')}
                onPress={() =>
                  props.navigation.navigate('CreateEventStep1', {
                    HostInfoId: venue.venue?.Team?.Id,
                  })
                }
                style={{
                  marginTop: 16 * scale,
                  width: '100%',
                  height: 36 * scale,
                  borderWidth: 1,
                  borderColor: '#00A9F4',
                  borderRadius: 4,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#00A9F4',
                    fontSize: 16 * scale,
                    fontStyle: 'normal',
                    fontWeight: 'normal',
                  }}>
                  {convertLanguage(language, 'register_event')}
                </Text>
              </TouchableOpacity>
            </>
          )}
        </>
      );
    } else {
      return (
        <>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: 16 * scale,
            }}>
            <Text
              style={{
                color: '#4F4F4F',
                fontSize: (16 * scale) / scaleFontSize,
                fontFamily: 'SourceSansPro-SemiBold',
                marginVertical: 16 * scale,
              }}>
              {convertLanguage(language, 'hosting_event')}
            </Text>
            <TouchableOpacity>
              <Text
                onPress={() =>
                  props.navigation.navigate('EventByVenues', {
                    Slug: venue?.venue?.Slug,
                  })
                }
                style={{
                  color: '#00A9F4',
                  fontSize: 16 * scale,
                  fontWeight: 'normal',
                }}>
                {convertLanguage(language, 'see_more')}
              </Text>
            </TouchableOpacity>
          </View>
          <View>
            <FlatList
              contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
              data={venue?.venue?.Events}
              renderItem={({ item, index }) => {
                return (
                  <TouchableOpacity
                    onPress={() =>
                      props.navigation.navigate({
                        name: 'EventDetail',
                        params: { Slug: item.Slug },
                        key: item.Slug,
                      })
                    }
                    style={{
                      marginTop: 10 * scale,
                      marginRight: 16 * scale,
                      width: 268 * scale,
                    }}>
                    <Image
                      style={{
                        backgroundColor: '#bcbcbc',
                        width: 268 * scale,
                        height: 150 * scale,
                        borderRadius: 4,
                      }}
                      source={{ uri: item.Posters.Small }}
                    />
                    <Text
                      style={{
                        fontSize: (20 * scale) / scaleFontSize,
                        fontFamily: 'SourceSansPro-SemiBold',
                        color: '#333333',
                      }}
                      numberOfLines={2}>
                      {item.VenueName}
                    </Text>
                    <Text
                      style={{
                        fontSize: (14 * scale) / scaleFontSize,
                        fontWeight: 'normal',
                        fontStyle: 'normal',
                        color: '#4F4F4F',
                      }}>
                      {convertDate3(item.TimeStart, item.TimeFinish, language)}
                    </Text>
                    <Text
                      style={{
                        fontSize: (14 * scale) / scaleFontSize,
                        fontWeight: 'normal',
                        fontStyle: 'normal',
                        color: '#4F4F4F',
                      }}
                      numberOfLines={1}>
                      {item.Address}
                    </Text>
                    <Text
                      style={{
                        fontSize: (12 * scale) / scaleFontSize,
                        fontWeight: 'normal',
                        fontStyle: 'normal',
                      }}
                      numberOfLines={1}>
                      {getHashTag(item.HashTag)}
                    </Text>
                  </TouchableOpacity>
                );
              }}
              keyExtractor={item => item.Id}
              horizontal={true}
              onEndReachedThreshold={0.1}
              showsHorizontalScrollIndicator={false}
              snapToInterval={(268 + 16) * scale} // Adjust to your content width
              snapToAlignment={'start'}
              decelerationRate={'fast'}
              pagingEnabled={Platform.OS === 'android'}
            />
            {venue.venue.Role !== 'Guest' && (
              <TouchableOpacity
                // onPress={() => props.navigation.navigate('EventHostList')}
                onPress={() =>
                  props.navigation.navigate('CreateEventStep1', {
                    HostInfoId: venue.venue?.Team?.Id,
                  })
                }
                style={{
                  marginTop: 16 * scale,
                  width: '100%',
                  height: 36 * scale,
                  borderWidth: 1,
                  borderColor: '#00A9F4',
                  borderRadius: 4,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#00A9F4',
                    fontSize: 16 * scale,
                    fontStyle: 'normal',
                    fontWeight: 'normal',
                  }}>
                  {convertLanguage(language, 'register_event')}
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </>
      );
    }
  };

  const uploadPhoto = () => {
    ImagePicker.openPicker({
      mediaType: 'photo',
      // includeBase64: true,
      // compressImageQuality: 0.8,
      multiple: true,
      maxFiles: 10,
    })
      .then(images => {
        setToogle({ ...toogle, popoverPhoto: false });
        let dataPhotoFetch = [];
        images.forEach(image => {
          let maxSize = 5242880;
          if (image.size > maxSize) {
            //5242880
            ImageResizer.createResizedImage(
              image.path,
              Math.floor(image.width / (image.size / maxSize)),
              Math.floor(image.height / (image.size / maxSize)),
              'JPEG',
              Math.floor(80 / (image.size / maxSize)),
            )
              .then(async uri => {
                dataPhotoFetch.push(uri);
              })
              .catch(er => { });
          } else {
            dataPhotoFetch.push(image);
          }
        });
        let formData = new FormData();
        dataPhotoFetch.forEach(image => {
          const file = {
            // uri: 'file://' + image.path,
            uri: Platform.OS === 'ios' ? `file://${image.path}` : image.path,
            name:
              image.filename ||
              Math.floor(Math.random() * Math.floor(999999999)) + '.jpg',
            type: image.mime || 'image/jpeg',
          };
          formData.append('FileImg[]', file);
        });
        dispatch(actUploadPhoto(venue?.venue?.Id, formData));
      })
      .catch(e => { });
  };

  const getHashTag = arr => {
    let hastag = '';
    arr.forEach(element => {
      hastag += '#' + element.HashTagName + ' ';
    });
    return hastag;
  };

  const inviteOrFollow = IsFollow => {
    // this.props.onTeamFollow(this.props.team.team.Id)
    if (IsFollow) {
      Alert.alert(
        // "Alert Title",
        '',
        convertLanguage(language, 'unfollow'),
        [
          {
            text: convertLanguage(language, 'cancel'),
            onPress: () => console.log('Cancel Pressed'),
            style: 'destructive',
          },
          {
            text: convertLanguage(language, 'ok'),
            onPress: () => dispatch(actTeamFollow(venue?.venue?.Team?.Id)),
          },
        ],
        { cancelable: false },
      );
    } else {
      dispatch(actTeamFollow(venue?.venue?.Team?.Id));
    }
  };

  const getTeam = () => {
    return (
      <>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 16 * scale,
          }}>
          <Text
            style={{
              color: '#4F4F4F',
              fontSize: 16 * scale,
              fontFamily: 'SourceSansPro-SemiBold',
              marginTop: 16 * scale,
            }}>
            {convertLanguage(language, 'host_team')}
          </Text>
        </View>
        <TouchableOpacity
          style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}
          onPress={() =>
            props.navigation.navigate('DetailTeam', {
              id: venue?.venue?.Team?.Id,
            })
          }>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 16 * scale,
              flex: 1 / 3,
            }}>
            <Image
              source={{ uri: venue?.venue?.Team?.Logos?.Medium }}
              style={{ width: 99 * scale, height: 99 * scale, borderRadius: 100 * scale }}
              resizeMode="contain"
            />
          </View>
          <View
            style={{ marginLeft: 16 * scale, overflow: 'hidden', flex: 2 / 3 }}>
            <Text
              style={{
                lineHeight: 20 * scale,
                fontSize: 14 * scale,
                fontFamily: 'SourceSansPro-SemiBold',
                color: '#333333',
              }}>
              {venue?.venue?.Team?.Name}
            </Text>
            <Text
              style={{
                lineHeight: 20 * scale,
                fontSize: 14 * scale,
                fontStyle: 'normal',
                fontWeight: 'normal',
                color: '#828282',
              }}>
              ({venue?.venue?.Team?.Code})
            </Text>
            <Text
              style={{
                lineHeight: 20 * scale,
                fontSize: 12 * scale,
                color: '#4F4F4F',
              }}>
              {getHashTag(venue?.venue?.Team?.HashTag)}
            </Text>
            {venue?.venue?.Team?.TeamRole === 'Guest' && (
              <TouchableOpacity
                onPress={() => inviteOrFollow(venue?.venue?.Team?.IsFollow)}
                style={{
                  width: 98 * scale,
                  height: 32 * scale,
                  borderRadius: 4 * scale,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 8 * scale,
                  borderWidth:
                    !venue?.venue?.Team?.IsFollow ||
                      venue?.venue?.Team?.Role != 'Guest'
                      ? 0
                      : 1,
                  backgroundColor:
                    !venue?.venue?.Team?.IsFollow ||
                      venue?.venue?.Team?.Role != 'Guest'
                      ? '#00A9F4'
                      : '#FFFFFF',
                }}>
                <Text
                  style={{
                    fontSize: 16 * scale,
                    fontStyle: 'normal',
                    fontWeight: 'normal',
                    color:
                      !venue?.venue?.Team?.IsFollow ||
                        venue?.venue?.Team?.Role != 'Guest'
                        ? '#FFFFFF'
                        : '#333333',
                  }}>
                  {venue?.venue?.Team?.IsFollow
                    ? convertLanguage(language, 'following')
                    : convertLanguage(language, 'follow')}
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </TouchableOpacity>
      </>
    );
  };
  const getInstagram = () => {
    if (venue.venue.Role == 'Leader' && !venue.venue.IsInstagram) {
      return (
        <>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: 30 * scale,
              marginBottom: 15 * scale,
            }}>
            <Text
              style={{
                color: '#333333',
                fontSize: 20 * scale,
                fontFamily: 'SourceSansPro-SemiBold',
              }}>
              {convertLanguage(language, 'instagram')}
            </Text>
          </View>
          <Touchable
            onPress={() => {
              if (venue.venue.IsInstagram) {
                this.logout();
              } else instagramLogin?.current?.show();
            }}
            style={{
              marginTop: 10 * scale,
              alignSelf: 'stretch',
              height: 44 * scale,
              borderColor: Colors.PRIMARY,
              borderWidth: 1,
              borderRadius: 4 * scale,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={require('../../assets/instagram_icon.png')} />
              <Text
                style={{
                  marginLeft: 10 * scale,
                  fontSize: 18 * scale,
                  fontFamily: 'SourceSansPro-SemiBold',
                  color: '#03a9f4',
                }}>
                {convertLanguage(language, 'link_instagram_feed')}
              </Text>
            </View>
          </Touchable>
          <InstagramLogin
            ref={instagramLogin}
            clientId="248215246620837"
            appId="248215246620837"
            responseType="code"
            appSecret="2f2d389bda5275560baaa49c400630f5"
            redirectUrl="https://mapi.comeup.asia/account/instagram"
            scopes={['user_profile', 'user_media']}
            modalVisible={!tokenInstagram}
            onLoginSuccess={setTokenIns}
            onLoginFailure={data => console.log(data)}
          />
        </>
      );
    } else {
      if (arrImgInstagram.length > 0) {
        return (
          <>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 30 * scale,
                marginBottom: 15 * scale,
              }}>
              <Text
                style={{
                  color: '#4F4F4F',
                  fontSize: 20 * scale,
                  fontFamily: 'SourceSansPro-SemiBold',
                }}>
                {convertLanguage(language, 'instagram')}
              </Text>
              {venue.venue.Role == 'Leader' && (
                <Touchable
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingRight: 10 * scale,
                  }}
                  onPress={() => setModelActionInstagram(true)}>
                  <Image
                    style={{ width: 12 * scale, height: 22 * scale }}
                    source={require('../../assets/more_1_top.png')}
                  />
                </Touchable>
              )}
            </View>
            {entriesSplitter()}

            <FlatList
              //    contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
              data={slides}
              renderItem={_renderItem}
              extraData={slides}
              keyExtractor={item => item[0].Id || Math.random()}
              horizontal={true}
              onEndReachedThreshold={0.3}
              showsHorizontalScrollIndicator={false}
              snapToAlignment={'start'}
              snapToInterval={(width - 100) * scale} // Adjust to your content width
              pagingEnabled={Platform.OS === 'android'}
              decelerationRate={'fast'}
            />
            {modalZoom ? (
              <ModalZoomImage
                modalVisible={modalZoom}
                closeModal={() => setModalZoom(false)}
                openModal={vlue => setModalZoom(true)}
                venue={venue.venue}
                images={venue.InstagramImages}
                index={zoomeImageIndex}
              />
            ) : null}
          </>
        );
      }
    }
    return null;
  };
  const getHastag = hastag => {
    let dataTag = '';
    hastag.forEach(element => {
      dataTag += '#' + element.HashTagName + ' ';
    });
    return dataTag;
  };
  const renderLoading = () => {
    return <ActivityIndicator size="large" color="#000000" />;
  };
  const share = () => {
    let options = {
      title: venue?.venue?.Name,
      message:
        venue?.venue?.Name +
        `\n${convertLanguage(language, 'find_and_join_venue_in_comeup')}\n` +
        getHastag(venue?.venue?.HashTag),
      url: Config.MAIN_URL + 'venues/' + venue?.venue?.Slug,
      subject: 'Jois with us', //  for email
    };
    Share.open(options)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        err && console.log(err);
      });
  };
  useEffect(() => {
    if (showBottom.toogle) {
      Animated.timing(fadeAnim, {
        toValue: 56 * scale,
        duration: 300,
      }).start();
    } else {
      Animated.timing(fadeAnim, {
        toValue: 0,
        duration: 300,
      }).start();
    }
  }, [showBottom.toogle]);
  const actLike = () => {
    // dispatch(actLikeVenues(venue?.venue?.Id));
    if (venue.venue.IsLike) {
      Alert.alert(
        "",
        convertLanguage(language, 'unlikevenue'),
        [
          {
            text: convertLanguage(language, 'cancel'),
            onPress: () => console.log("Cancel Pressed"),
            style: "destructive"
          },
          { text: convertLanguage(language, 'ok'), onPress: () => dispatch(actLikeVenues(venue?.venue?.Id)) }
        ]
      );
    } else {
      dispatch(actLikeVenues(venue?.venue?.Id))
    }

  };

  const [modalContact, setModalContact] = useState(false);
  const refType = useRef(null);


  const [modalNew, setModalNew] = useState(false)
  useEffect(() => {
    if (props?.route?.params?.New) {
      setModalNew(true)
    }
  }, [props?.route?.params?.New])

  // console.log('venue.venue', venue.venue)
  return (
    <>
      <View style={{ backgroundColor: Colors.BG, flex: 1 }}>
        <SafeAreaConsumer>
          {insets => <View style={{ paddingTop: insets.top }} />}
        </SafeAreaConsumer>
        {venue.loadingSetIns && <Loading />}
        {Object.keys(venue.venue).length < 1 || venue.loading || !firstTime ? (
          <ActivityIndicator size="large" color="#000000" style={{ flex: 1 }} />
        ) : (
          <>
            <View style={{ flex: 1, backgroundColor: 'white' }}>
              <View style={styles.view1}>
                <TouchableOpacity
                  onPress={() => {
                    navigation.goBack();
                  }}
                  style={styles.view2}>
                  <Image
                    style={{ width: 24 * scale, height: 24 * scale }}
                    source={require('../../assets/back_icon.png')}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                  {venue.venue.Role === 'Guest' && (
                    <TouchableOpacity onPress={actLike}>
                      <Image
                        style={{ width: 20 * scale, height: 18.5 * scale, marginRight: 12 * scale }}
                        // source={require('../../assets/detail_heart_active.png')}
                        source={
                          venue.venue.IsLike
                            ? require('../../assets/venue_red_heart.png')
                            : require('../../assets/venue_heart.png')
                        }
                      />
                      {/* <Text style={styles.txt2_1}>
                          {venue.venue.IsLike
                            ? convertLanguage(language, 'liked_venue_detail')
                            : convertLanguage(language, 'like')}
                        </Text> */}
                    </TouchableOpacity>
                  )}
                  <TouchableOpacity
                    // style={[
                    //   styles.view5,
                    //   {
                    //     marginLeft:
                    //       venue.venue.Role === 'Guest' ? 16 * scale : 0,
                    //   },
                    // ]}
                    onPress={share}>
                    <Image
                      style={{ width: 22 * scale, height: 22 * scale, marginRight: 12 * scale }}
                      source={require('../../assets/share.png')}
                    />
                    {/* <Text style={styles.txt2_2}>
                        {convertLanguage(language, 'share')}
                      </Text> */}
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() =>
                      venue?.venue?.Role !== 'Guest'
                        ? setToogle({ ...toogle, modalMore: !toogle.modalMore })
                        : dispatch(
                          actGlobalCheckLogin('VenueReport', {
                            Id: venue.venue?.Id,
                            Title: venue.venue?.Name,
                          }),
                        )
                    }>
                    {venue?.venue?.Role !== 'Guest' ? (
                      <Image
                        source={require('../../assets/more_horizontal.png')}
                        style={{
                          width: 25 * scale,
                          height: 25 * scale,
                          marginRight: 16 * scale,
                        }}
                      />
                    ) : (
                      <Image
                        source={require('../../assets/report.png')}
                        style={{
                          width: 25 * scale,
                          height: 25 * scale,
                          marginRight: 16 * scale,
                        }}
                      />
                    )}
                  </TouchableOpacity>
                </View>
              </View>
              <Line />
              <ScrollView
                onMomentumScrollEnd={() => loadMoreNews()}
                scrollEventThrottle={16}
                contentContainerStyle={{}}
                onScroll={event => {
                  Math.floor(event.nativeEvent.contentOffset.y) >=
                    Math.floor(showBottom.value)
                    ? !showBottom.toogle
                      ? setShowBottom({ ...showBottom, toogle: true })
                      : null
                    : showBottom.toogle
                      ? setShowBottom({ ...showBottom, toogle: false })
                      : null;
                }}>
                {/* {console.log('venue.venue.Posters', venue.venue)} */}
                <VenueDetailItem
                  bgr_image={venue.venue.Posters?.Small}
                  bgr_image_full={venue.venue.Posters?.Full}
                  avt_image={venue.venue.Logos?.Medium}
                  avt_image_full={venue.venue.Logos?.Full}
                  name={venue.venue?.Name}
                  id={venue.venue?.Code}
                  hastTag={venue.venue?.HashTag}
                  navigation={navigation}
                  detailFull={true}
                  address={venue.venue?.Address}
                  time={venue.venue?.TimeOpen}
                  listTime={venue?.venue?.ListTimeOpen}
                  language={language}
                  zipcode={venue?.venue?.Zipcode}
                  phone={venue?.venue?.Phone}
                  email={venue?.venue?.Email}
                  managerName={venue?.venue?.ManagerName}
                  status={venue?.venue?.Status}
                />

                <View
                  style={{
                    height: 1 * scale,
                    width: '100%',
                    backgroundColor: 'white',
                  }}
                  ref={likeRef}
                  onLayout={event => {
                    setShowBottom({
                      ...showBottom,
                      value: event.nativeEvent.layout.y,
                    });
                  }}
                />
                <View
                  style={{
                    width: screenWidth,
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingHorizontal: 16 * scale,
                  }}>



                  {
                    !venue?.venue?.IsReview &&
                    // <View style={{
                    //   flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#F2F2F2', marginHorizontal: 16 * scale, paddingHorizontal: 10 * scale, flex: 1, paddingVertical: 8 * scale, borderRadius: 4 * scale,
                    // }}>
                    <InsetShadow
                      shadowOpacity={0.25}
                      shadowRadius={4}
                      shadowOffset={0.5}
                      bottom={false}
                      // shadowColor="rgba(0, 0, 0, 0.08)"
                      containerStyle={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#F2F2F2', paddingHorizontal: 10 * scale, flex: 1, paddingVertical: 8 * scale, borderRadius: 4 * scale, marginTop: 16 * scale }}>
                      <View style={{ flex: 1 / 9 }}>
                        <Image source={require('../../assets/clock_orange.png')} style={{ width: 20 * scale, height: 20 * scale, alignSelf: 'center' }}></Image>
                      </View>
                      <Text style={{ flex: 8 / 9, color: '#F2994A', fontSize: 16 * scale, lineHeight: 20 * scale, fontWeight: 'normal', letterSpacing: -0.4 * scale }}>{convertLanguage(language, 'this_venue_is_on_reviewing')}</Text>
                    </InsetShadow>
                    //  {/* </View> */}
                  }
                  <View
                    style={{
                      borderWidth: 1,
                      borderColor: '#D0D0D0',
                      flex: 1,
                      margin: 16 * scale,
                      width: '100%',
                    }}
                  />
                </View>

                <View style={{ marginHorizontal: 16 * scale }}>
                  {venue?.venue?.Description !== '' &&
                    venue?.venue?.Description !== undefined &&
                    venue?.venue?.Description !== null && (
                      <>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              color: '#4F4F4F',
                              fontSize: 16 * scale,
                              fontFamily: 'SourceSansPro-SemiBold',
                            }}>
                            {convertLanguage(language, 'about_venue')}
                          </Text>
                        </View>
                        <AutoHeightWebView
                          startInLoadingState={true}
                          scalesPageToFit={false}
                          scrollEnabled={false}
                          showsVerticalScrollIndicator={false}
                          renderLoading={() => renderLoading()}
                          ref={webviewRef}
                          onNavigationStateChange={(event) => {
                            if (event.url !== 'about:blank') {
                              webviewRef.current.stopLoading();
                              Linking.openURL(event.url);
                            }
                          }}
                          style={{ flex: 1, marginVertical: 10 * scale }}
                          // width={width - 32}
                          source={{
                            html:
                              `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><style> * {width: ${width -
                              32}px; overflow-wrap: break-word} iframe {width: ${width -
                              32}px; height: ${((width - 32) * 9) /
                              16}px} img {width: ${width -
                              32}px !important; margin-bottom: 5px; margin-top: 5px; height: auto !important;}</style><body>` +
                              venue?.venue?.Description.replace(
                                /"\/\/\www.youtube.com/g,
                                '"https://www.youtube.com',
                              ) +
                              `</body></html>`,
                          }}
                          originWhitelist={['*']}
                        />
                      </>
                    )}

                  <Text
                    style={{
                      color: '#4F4F4F',
                      fontSize: 16 * scale,
                      fontFamily: 'SourceSansPro-SemiBold',
                      marginVertical: 8 * scale,
                    }}>
                    {convertLanguage(language, 'manager')}:{' '}
                    <Text style={{ fontSize: 14 * scale, fontWeight: 'normal' }}>
                      {venue.venue.ManagerName}
                    </Text>
                  </Text>
                  <Text
                    style={{
                      color: '#4F4F4F',
                      fontSize: 16 * scale,
                      fontFamily: 'SourceSansPro-SemiBold',
                      marginVertical: 8 * scale,
                    }}>
                    {convertLanguage(language, 'phone')}:{' '}
                    <Text style={{ fontSize: 14 * scale, fontWeight: 'normal' }}>
                      {
                        venue?.venue?.Zipcode !== null && venue?.venue?.Zipcode !== undefined ?
                          '(' + venue?.venue?.Zipcode + ')' + ' ' + venue.venue.Phone
                          :
                          venue.venue.Phone
                      }

                    </Text>
                  </Text>
                  <Text
                    style={{
                      color: '#4F4F4F',
                      fontSize: 16 * scale,
                      fontFamily: 'SourceSansPro-SemiBold',
                      marginVertical: 8 * scale,
                    }}>
                    {convertLanguage(language, 'email')}:{' '}
                    <Text style={{ fontSize: 14 * scale, fontWeight: 'normal' }}>
                      {venue.venue.Email}
                    </Text>
                  </Text>

                  <Text
                    style={{
                      color: '#4F4F4F',
                      fontSize: 16 * scale,
                      fontFamily: 'SourceSansPro-SemiBold',
                      marginVertical: 16 * scale,
                    }}>
                    {convertLanguage(language, 'location')}
                  </Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Image
                      source={require('../../assets/Location2.png')}
                      style={{
                        width: 16 * scale * scaleFontSize,
                        height: 16 * scale * scaleFontSize,
                        marginRight: 4 * scale,
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 12 * scale,
                        width: 308 * scale,
                        fontFamily: 'SourceSansPro-SemiBold',
                        color: '#4F4F4F',
                      }}>
                      {venue.venue.Address}
                    </Text>
                  </View>

                  <View
                    style={{
                      width: 328 * scale,
                      height: 140 * scale,
                      borderRadius: 4,
                      backgroundColor: '#BDBDBD',
                      marginVertical: 8 * scale,
                    }}>
                    {venue?.venue?.Lat !== undefined &&
                      venue?.venue?.Long !== undefined &&
                      venue?.venue?.Lat !== '' &&
                      venue?.venue?.Long !== '' && (
                        <TouchableOpacity
                          onPress={() =>
                            props.navigation.navigate('MapViews', {
                              Lat: parseFloat(venue?.venue?.Lat),
                              Long: parseFloat(venue?.venue?.Long),
                            })
                          }>
                          <MapView
                            provider={PROVIDER_GOOGLE}
                            initialRegion={{
                              latitude: parseFloat(venue?.venue.Lat),
                              longitude: parseFloat(venue?.venue.Long),
                              latitudeDelta: LATITUDE_DELTA,
                              longitudeDelta: LONGITUDE_DELTA,
                            }}
                            zoomEnabled={true}
                            enableZoomControl={true}
                            style={{ width: 328 * scale, height: 147 * scale }}
                            // ref={map}
                            liteMode={true}>
                            <Marker
                              coordinate={{
                                latitude: parseFloat(venue?.venue.Lat),
                                longitude: parseFloat(venue?.venue.Long),
                              }}
                            // image={flagPinkImg}
                            >
                              <Image
                                source={require('../../assets/pin_icon.png')}
                              />
                            </Marker>
                          </MapView>
                          <TouchableOpacity
                            style={{ position: 'absolute' }}
                            onPress={() =>
                              props.navigation.navigate('MapViews', {
                                Lat: parseFloat(venue?.venue.Lat),
                                Long: parseFloat(venue?.venue.Long),
                              })
                            }
                          />
                          {/* <Image source={require('../../assets/map_view.png')} style={{ width: width - 40, height: 80 }} /> */}
                        </TouchableOpacity>
                      )}
                  </View>

                  {getPhotos()}

                  {getHostingEvents()}

                  {!Array.isArray(venue?.venue?.Team) && getTeam()}

                  {getInstagram()}
                </View>
                {createNews()}

                <View style={{ height: 60 * scale }} />
              </ScrollView>
              {/* <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    color: '#FAFAFA',
                    marginTop: 39 * scale,
                    position: 'absolute',
                    bottom: 0 * scale,
                    backgroundColor: 'white',
                    flex: 1,
                    height: 56 * scale,
                    justifyContent: 'center',
                    backgroundColor: 'white',
                    shadowColor: 'rgba(0, 0, 0, 0.2))',
                    shadowOffset: {
                      width: 0,
                      height: -8,
                    },
                    shadowOpacity: 0.2,
                    shadowRadius: 2,

                    elevation: 4,
                  }}>
                  <TouchableOpacity
                    style={[
                      styles.view4,
                      {
                        borderWidth: 1,
                        backgroundColor: '#00A9F4',
                      },
                    ]}
                    onPress={() => setModalContact(true)}>
                    <Text
                      style={[
                        styles.txt2_1,
                        { color: '#FFFFFF', fontSize: 16 * scale },
                      ]}>
                      {convertLanguage(language, 'book_now')}
                    </Text>
                  </TouchableOpacity>
                </View> */}
              {

                <SafeAreaConsumer>
                  {insets => <View style={[styles.boxTicket, { bottom: insets.bottom }]}>
                    <Touchable style={styles.btnTicket} onPress={() => setModalContact(true)}>
                      <View style={styles.boxEventMoreLeft}>
                        {/* <Image source={require('../../assets/ticket_white.png')} style={{ marginRight: 10, width: 30, height: 30 }} /> */}
                        <Text style={[styles.txtFollow, { fontWeight: 'bold', fontSize: 17 }]}>{convertLanguage(language, 'book_now')}</Text>
                      </View>
                    </Touchable>
                  </View>
                  }
                </SafeAreaConsumer>
              }
            </View>

            <Popover
              isVisible={toogle.popoverPhoto}
              animationConfig={{ duration: 0 }}
              fromView={refPopover.current}
              placement="bottom"
              arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0 }}
              backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.1)' }}
              onRequestClose={() => setToogle({ ...toogle, popoverPhoto: false })}
              popoverStyle={{
                borderRadius: 8,
                // width: 111 * scale,
                top: 3 * scale,
                left: -5 * scale,
                position: 'absolute',
              }}>
              <View
                style={{
                  // width: 111 * scale,
                  padding: 12 * scale,
                  justifyContent: 'center',
                  // alignItems: 'center'
                }}>
                <TouchableOpacity
                  onPress={() => {
                    uploadPhoto();
                  }}>
                  {venue.venue.Role !== 'Guest' && (
                    <Text
                      style={{
                        fontWeight: 'normal',
                        fontSize: (16 * scale) / scaleFontSize,
                        marginBottom: 5 * scale,
                      }}>
                      {convertLanguage(language, 'upload_photos')}
                    </Text>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate('VenueDetailPhoto', {
                      detailFull: true,
                      venueId: venue?.venue?.Id,
                      role: venue.venue.Role,
                      isLike: venue.venue.IsLike,
                      actLike: actLike,
                      actShare: () => share(),
                      upload: () => uploadPhoto(),
                    });
                    setToogle({ ...toogle, popoverPhoto: false });
                  }}>
                  <Text
                    style={{
                      fontWeight: 'normal',
                      fontSize: (16 * scale) / scaleFontSize,
                    }}>
                    {convertLanguage(language, 'see_more')}
                  </Text>
                </TouchableOpacity>
              </View>
            </Popover>
          </>
        )}
        {toogle.modalMore && (
          <ModalMoreVenue
            role={venue?.venue?.Role}
            id={venue?.venue?.Id}
            loadAgain={() => setCheckLoadAgain(!checkLoadAgain)}
            onClose={() => setToogle({ ...toogle, modalMore: false })}
            navigation={props.navigation}
            language={language}
          />
        )}

        {
          <ModalNew
            isVisible={modalNew}
            closeModal={() => setModalNew(false)}
          />
        }

      </View>
      {
        modalVenueNewsDetail && (
          <ModalVenueNewsDetail
            closeModal={closeModalVenueNewsDetail}
            data={venue_news}
            onLikeVenueNews={Id => onLikeVenueNews(Id)}
            onDeleteVenueNews={Id => onDeleteVenueNews(Id)}
            // openModalAction={(Id) => { onOpenModelVenueNews(Id, Role) }}
            navigation={props.navigation}
          />
        )
      }
      {
        modelActionInstagram && (
          <ModalActionInstagram
            Role={venue.venue.Role}
            closeModal={() => {
              setModelActionInstagram(false), setVenueNewsId('');
            }}
            onRemoveInstagram={() => onRemoveInstagram()}
          />
        )
      }
      {
        modalContact && (
          <>
            <Popover
              isVisible={modalContact}
              animationConfig={{ duration: 0 }}
              fromView={refType.current}
              placement="center"
              // fromRect={{ x: 32 * scale, y: 160 * scale, width: width - 64 * scale, height: 200 }}
              arrowStyle={{
                backgroundColor: 'transparent',
                height: 0,
                width: 0,
              }}
              backgroundStyle={{
                backgroundColor: 'rgba(0, 0, 0, 0.1)',
              }}
              onRequestClose={() => setModalContact(false)}>
              <View
                style={[
                  styles.rowClose,
                  {
                    width: width - 64 * scale,
                    padding: 12 * scale,
                  },
                ]}>
                <TouchableOpacity
                  style={{
                    position: 'absolute',
                    right: 8 * scale,
                    top: 8 * scale,
                    width: 30 * scale,
                    height: 30 * scale,
                  }}
                  onPress={() => setModalContact(false)}>
                  <Image
                    resizeMode="contain"
                    style={{ width: 30 * scale, height: 30 * scale }}
                    source={require('../../assets/close.png')}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  justifyContent: 'center',
                  borderColor: 'rgba(0, 0, 0, 0.3)',
                  paddingTop: 10 * scale,
                  paddingBottom: 12 * scale,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingHorizontal: 24 * scale,
                    paddingBottom: 24 * scale,
                  }}>
                  <View
                    style={{
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        color: '#333333',
                        fontSize: 20 * scale,
                        fontWeight: '600',
                        paddingBottom: 4,
                      }}>
                      {convertLanguage(language, 'booking_info')}
                    </Text>
                    <View style={{ paddingLeft: 16 * scale }}>
                      <Text style={styles.txtInfo}>
                        <Text style={{ color: '#8c8c8c' }}>{convertLanguage(language, 'manager_booking')} : </Text>
                        <Text style={{ fontSize: 15 * scale, color: 'black' }}> {venue?.venue?.ManagerName}</Text>
                      </Text>
                      <Text style={styles.txtInfo}>
                        <Text style={{ color: '#8c8c8c' }}>{convertLanguage(language, 'phone_number')} : </Text>
                        <Text style={{ fontSize: 15 * scale, color: 'black' }}> {'(' + venue?.venue?.Zipcode + ')'} {venue?.venue?.Phone}</Text>
                      </Text>
                      <Text style={styles.txtInfo}>
                        <Text style={{ color: '#8c8c8c' }}>{convertLanguage(language, 'email')} : </Text>
                        <Text style={{ fontSize: 15 * scale, color: 'black' }}> {venue?.venue?.Email}</Text>
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: 10 * scale,
                      }}>
                      <TouchableOpacity
                        style={styles.btnPhone}
                        onPress={() =>
                          Linking.openURL(
                            'tel:' + venue?.venue?.Zipcode + venue?.venue?.Phone,
                          )
                        }>
                        <Image
                          source={require('../../assets/contact_tel.png')}
                          style={styles.contact_tel}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={styles.btnPhone}
                        onPress={() =>
                          Linking.openURL('mailto:' + venue?.venue?.Email)
                        }>
                        <Image
                          source={require('../../assets/contact_mail.png')}
                          style={styles.contact_tel}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </Popover>
          </>
        )
      }
    </>
  );
};
export default memo(VenueDetail);
const styles = StyleSheet.create({
  view1: {
    width: 360 * scale,
    height: 48 * scale,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  view2: {
    marginLeft: 16 * scale,
    width: 65 * scale,
    height: 23 * scale,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  view3: {
    flexDirection: 'row',
    marginTop: 16 * scale,
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
  },
  view4: {
    width: 220 * scale,
    height: 36 * scale,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 4 * scale,
    borderWidth: 1,
    borderColor: '#BDBDBD',
    justifyContent: 'center',
  },
  view5: {
    width: 92 * scale,
    height: 36 * scale,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 4 * scale,
    borderWidth: 1,
    borderColor: '#00A9F4',
    marginLeft: 16 * scale,
    justifyContent: 'center',
  },
  view6: {
    //  marginTop: 16,
    width: '90%',
    paddingBottom: '10%',
    backgroundColor: 'white',
    alignSelf: 'center',
  },
  view6_1: {
    width: '100%',
    height: 184 * scale,
    marginTop: 16 * scale,
  },
  view7: {},
  txt1: {
    color: '#4F4F4F',
    lineHeight: 22.63,
    fontSize: 18 * scale,
    fontFamily: 'SourceSansPro-SemiBold',
  },
  txt2: {
    color: '#333333',
    lineHeight: 25.14,
    fontSize: 20 * scale,
    fontFamily: 'SourceSansPro-SemiBold',
    marginTop: 24 * scale,
    marginLeft: 16 * scale,
  },
  txt2_1: {
    color: '#828282',
    lineHeight: 20.11,
    fontSize: 15 * scale,
    fontFamily: 'SourceSansPro-Regular',
    marginLeft: 10 * scale,
  },
  txt2_2: {
    color: '#00A9F4',
    lineHeight: 20.11,
    fontSize: 15 * scale,
    fontFamily: 'SourceSansPro-Regular',
    marginLeft: 10 * scale,
  },
  boxPopover: {
    flex: 1,
    width: 132 * scale,
    height: 85 * scale,
    borderRadius: 50 * scale,
    paddingLeft: 16 * scale,
    paddingVertical: 12 * scale,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24,
  },
  txt3: {},
  txtInfo: {
    fontSize: 16 * scale,
    color: '#333333',
    marginBottom: 5 * scale,
  },
  btnPhone: {
    width: 32 * scale,
    height: 32 * scale,
    marginRight: 8 * scale,
  },
  contact_tel: {
    width: 32 * scale,
    height: 32 * scale,
  },
  boxTicket: {
    height: 62,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    position: 'absolute',
    width
    // alignItems: 'center',
  },
  btnTicket: {
    height: 46,
    backgroundColor: '#03a9f4',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
    borderRadius: 4,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.17,
    shadowRadius: 7.49,
    elevation: 9,
  },
  boxEventMoreLeft: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtFollow: {
    fontSize: 15,
    color: '#FFFFFF',
    fontWeight: 'bold'
  },
});
