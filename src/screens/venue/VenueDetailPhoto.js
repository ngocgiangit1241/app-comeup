import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Dimensions,
    Image,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ScrollView,
    FlatList, ActivityIndicator,
    SafeAreaView,
} from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import VenueDetailItem from '../../components/venue/VenueDetailItem';
import SafeView from '../../screens_view/SafeView';
import * as Colors from '../../constants/Colors';
import * as Types from '../../constants/ActionType';
import ModalPhotoVenue from '../../components/ModalPhotoVenue'
import { actGetFullPhoto, actDeletePhoTo } from '../../actions/venue'
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const scale = screenWidth / 360;
import { convertLanguage } from '../../services/Helper';

export default function VenueDetail(props) {
    const dispatch = useDispatch()
    const [page, setPage] = useState(1)
    useEffect(() => {
        return () => dispatch({ type: Types.VENUE_PHOTO_CLEAR })
    }, [])

    useEffect(() => {
        dispatch(actGetFullPhoto(props.route?.params?.venueId, page))
    }, [page])

    const photo = useSelector(state => state.venue)
    const venue = useSelector(state => state.venue)
    const { language } = useSelector(state => state.language)

    const [toogle, setToogle] = useState({
        display: false,
        index: 0
    })
    useEffect(() => {
        if (photo?.photo?.photo.length === 0) {
            setToogle({ ...toogle, display: false })
        }
    }, [photo])
    const deletePhoto = (index) => {
        dispatch(actDeletePhoTo(photo?.photo?.photo[index]?.Id, index))

    }
    const _renderHeader = () => (
        <>
            <View>
                <VenueDetailItem
                    bgr_image={venue.venue.Posters?.Small}
                    bgr_image_full={venue.venue.Posters?.Full}
                    avt_image={venue.venue.Logos?.Medium}
                    avt_image_full={venue.venue.Logos?.Full}
                    name={venue.venue?.Name}
                    id={venue.venue?.Code}
                    hastTag={venue.venue?.HashTag}
                    navigation={props.navigation}
                    detailFull={props.route?.params?.detailFull}
                    address={venue.venue?.Address}
                    time={venue.venue?.TimeOpen}
                    listTime={venue?.venue?.ListTimeOpen}
                    language={language}
                    zipcode={venue?.venue?.Zipcode}
                    phone={venue?.venue?.Phone}
                    email={venue?.venue?.Email}
                    managerName={venue?.venue?.ManagerName}
                    status={venue?.venue?.Status}

                />

                <View style={styles.view3}>
                    {props.route?.params?.role === 'Guest' &&
                        <TouchableOpacity style={styles.view4} onPress={props.route?.params?.actLike}>
                            <Image style={{ width: 15, height: 13.5 }} source={venue.venue.IsLike ? require('../../assets/venue_red_heart.png') : require('../../assets/venue_heart.png')} />
                            <Text style={styles.txt2_1}>{venue.venue?.IsLike ? convertLanguage(language, 'liked_venue_detail') : convertLanguage(language, 'like')}</Text>
                        </TouchableOpacity>
                    }
                    <TouchableOpacity style={styles.view5} onPress={() => props.route?.params?.actShare()}>
                        <Image style={{ width: 13.5, height: 15 }} source={require('../../assets/detail_share_blue.png')} />
                        <Text style={styles.txt2_2}>{convertLanguage(language, 'share')}</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 20 * scale }}>
                    <Text style={styles.txt2}>{convertLanguage(language, 'photo')}</Text>
                    {props.route?.params?.role !== 'Guest' && <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} onPress={() => props?.route?.params?.upload()}>
                        <Image source={require('../../assets/Camera.png')} style={{ width: 18 * scale, height: 18 * scale, marginRight: 9.5 * scale }} />
                        <Text style={{ color: '#00A9F4', fontWeight: 'normal', fontSize: 16 * scale }}>{convertLanguage(language, 'upload_photos')}</Text>
                    </TouchableOpacity>}
                </View>
            </View>
        </>
    )
    const _renderFooter = () => {
        if (page !== photo.photo.page) {
            return (
                <ActivityIndicator
                    size="large"
                    color="#000000"
                    style={{ padding: 20, flex: 1 }}
                />
            );
        } else {
            return (
                // <Text style={{ textAlign: 'center', paddingTop: 20 }}>
                //     {convertLanguage(language, 'event_empty')}
                // </Text>
                <View></View>
            );
        }


    };
    const loadMore = () => {
        if (page !== photo.photo.page)
            setPage(page + 1)
    }
    return (
        <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                {photo.photo.loading ?

                    <ActivityIndicator size="large" color="#000000" style={{ flex: 1 }} />
                    :
                    <>
                        <View style={styles.view1}>
                            <TouchableOpacity onPress={() => { props.navigation.goBack() }} style={styles.view2}>
                                <Image style={{ width: 24, height: 24 }} source={require('../../assets/back_icon.png')} />
                            </TouchableOpacity>
                            {/* <TouchableOpacity>
                                <Image style={{ width: 5, height: 20, marginRight: 16 * scale }} source={require('../../assets/detail_dot.png')} resizeMode="contain" />
                            </TouchableOpacity> */}
                        </View>


                        <FlatList
                            style={{ paddingBottom: '5%', marginHorizontal: 16 * scale }}
                            keyExtractor={item => item.Id.toString() || Math.random().toString()}
                            data={photo.photoDetail}
                            showsVerticalScrollIndicator={false}
                            ListFooterComponent={() => _renderFooter()}
                            onEndReached={() => loadMore()}
                            ListHeaderComponent={() => _renderHeader()}
                            onEndReachedThreshold={0.5}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity onPress={() => setToogle({ display: true, index })} key={item.Id}>
                                        <Image style={styles.view6_1} source={{ uri: item?.Image?.Full }} />
                                    </TouchableOpacity>
                                );
                            }}
                        />
                    </>
                }
            </View>

            {
                toogle.display &&
                <ModalPhotoVenue
                    visible={toogle.display}
                    onClose={() => setToogle({ display: false, index: 0 })}
                    images={photo.photoDetail}
                    index={toogle.index}
                    length={photo.photo.photo.length}
                    deletePhoto={(index) => deletePhoto(index)}
                    role={venue?.venue?.Role}
                />
            }


        </SafeView>
    )
}
const styles = StyleSheet.create({
    view1: {
        width: 360 * scale,
        height: 48,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'space-between'
    },
    view2: {
        marginLeft: 16 * scale,
        width: 65 * scale,
        height: 23 * scale,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    view3: {
        flexDirection: "row",
        marginTop: 24 * scale,
    },
    view4: {
        width: 220 * scale,
        height: 36 * scale,
        flexDirection: "row",
        alignItems: "center",
        borderRadius: 4 * scale,
        borderWidth: 1,
        borderColor: '#BDBDBD',
        justifyContent: 'center'
    },
    view5: {
        width: 92 * scale,
        height: 36 * scale,
        flexDirection: "row",
        alignItems: "center",
        borderRadius: 4 * scale,
        borderWidth: 1,
        borderColor: '#00A9F4',
        marginLeft: 16 * scale,
        justifyContent: "center"
    },
    view6: {
        //  marginTop: 16, 
        width: '90%',
        paddingBottom: '10%',
        backgroundColor: 'white',
        alignSelf: "center"
    },
    view6_1: {
        width: '100%',
        height: 184 * scale,
        marginVertical: 8 * scale,
        borderRadius: 4 * scale
    },
    view7: {},
    txt1: {
        color: '#4F4F4F',
        lineHeight: 22.63,
        fontSize: 18,
        fontFamily: 'SourceSansPro-SemiBold'
    },
    txt2: {
        color: '#333333',
        lineHeight: 25.14,
        fontSize: 20,
        fontFamily: 'SourceSansPro-SemiBold',

    },
    txt2_1: {
        color: '#828282',
        lineHeight: 20.11,
        fontSize: 15,
        fontFamily: 'SourceSansPro-Regular',
        marginLeft: 10 * scale
    },
    txt2_2: {
        color: '#00A9F4',
        lineHeight: 20.11,
        fontSize: 15,
        fontFamily: 'SourceSansPro-Regular',
        marginLeft: 10 * scale
    },
    txt3: {},
})
