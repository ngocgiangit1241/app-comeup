import React, { useState, useEffect } from 'react';
import Touchable from '../../screens_view/Touchable';
import { convertLanguage } from '../../services/Helper';
import ModalCity from '../../components/ModalCity';
import ModalTime from '../../components/ModalTime';
import ModalTag from '../../components/ModalTag';
import ToggleSwitch from 'toggle-switch-react-native';
import VenueLikeItem from '../../components/venue/VenueLikeItem';
import { actLoadDataVenueLiked } from '../../actions/like'
import { actLikeVenues } from '../../actions/venue'
import { useSelector, useDispatch } from 'react-redux';
import {
  View,
  Text,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  FlatList,
  SafeAreaView,
  ActivityIndicator
} from 'react-native';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const scale = screenWidth / 360;
export default function VenueLike({ navigation }) {

  const { language } = useSelector(state => state.language)
  const [data, setData] = useState({
    page: 1
  })
  const dispatch = useDispatch();
  useEffect(() => {
    return () => setData({ ...data, page: 1 })
  }, [])
  useEffect(() => {
    dispatch(actLoadDataVenueLiked(data.page))

  }, [data.page])
  const venueLiked = useSelector(state => state.like.venues)
  const venueLiked22 = useSelector(state => state.like)

  useEffect(() => {
    let dataFetch = [...venueLiked]
    dataFetch.forEach(element => {
      element.IsLike === true
    })
    setItemList(dataFetch)
  }, [venueLiked])

  const _renderFooter = () => {
    let is_loadMore = venueLiked22.loadMoreVenue;
    if (!is_loadMore) {
      return (

        <Text style={{ textAlign: 'center', paddingTop: 20 }}>
          {/* {convertLanguage(language, 'event_empty')} */}
        </Text>
      );
    } else {
      return (

        <ActivityIndicator
          size="large"
          color="#000000"
          style={{ padding: 20, flex: 1 }}
        />
      );
    }
  };
  const onLoadMore = () => {
    if (venueLiked22.loadMoreVenue) {
      setData({ ...data, page: data.page + 1 })
    }
  }

  const [itemList, setItemList] = useState([
    {
      Poster: '',
      Logos: '',
      Name: '',
      HashTag: [],
      TimeOpen: {},
      Address: '',
      like: false,
    },
  ]);
  const onChangeLike = (index, Id) => {
    dispatch(actLikeVenues(Id)).then(data => {
      if (data.status === 200) {
        let dataFetch = [...itemList]
        dataFetch[index].IsLike = !dataFetch[index].IsLike
        setItemList(dataFetch)
      }
    })
  }
  return (

    <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
      <TouchableOpacity onPress={() => { navigation.goBack() }} style={styles.view1}>
        {/* <TouchableOpacity> */}
        <Image
          style={{
            width: 15 * scale,
            height: 9 * scale,
          }}
          source={require('../../assets/back_icon.png')}
        />
        {/* </TouchableOpacity> */}
        <Text style={styles.txt1}>{convertLanguage(language, 'liked_venue')}</Text>
      </TouchableOpacity>
      <ScrollView>
        <View style={{
          borderBottomColor: '#F2F2F2',
          borderBottomWidth: 1,
          width: '100%'
        }}>

        </View>
        <FlatList
          style={{ paddingBottom: "5%" }}
          data={itemList}
          keyExtractor={item => item?.id}
          ListFooterComponent={() => _renderFooter()}
          onEndReached={() => onLoadMore()}
          renderItem={({ item, index }) => {
            return (
              <VenueLikeItem
                key={'VenueLikeItem' + index}
                navigation={navigation}
                item={item}
                index={index}
                onChangeLike={(index, id) => onChangeLike(index, id)}
                venueLiked={venueLiked}

              />
            )
          }}
        />
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  view1: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 120 * scale,
    height: 48 * scale,
    marginLeft: 16 * scale,

  },
  txt1: {
    lineHeight: 22.63,
    fontSize: 18,
    fontFamily: 'SourceSansPro-SemiBold',
    color: '#4F4F4F',
    marginLeft: 12 * scale,
  },
});
