import React, { useState } from 'react';
import VenueLikeHomeItem from '../../components/venue/VenueLikeHomeItem';
import {
    View,
    Text,
    Dimensions,
    Image,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ScrollView,
    FlatList,
    SafeAreaView
} from 'react-native';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const scale = screenWidth / 360;
export default function VenueLikeHome({ navigation }) {
    const renderItem = ({ item, index }) => {
        return (
            <VenueLikeHomeItem
                key={index}
                imgBgr={item.imgBgr}
                imgMain={item.imgMain}
                text1={item.text1}
                text2={item.text2}
                text3={item.text3}
                text4={item.text4}
                navigation={navigation}
                index={index}
                like={item.like}
                onChangeLike={(index) => onChangeLike(index)}
            />
        );
    };

    const [itemList, setItemList] = useState([

        {
            imgBgr: require('../../assets/venueimg.png'),
            imgMain: require('../../assets/girl_venue_list.png'),
            text1: '1900 LE THÉÂTRE 1900 LE THÉÂTRE',
            text2: 'Sân vận động Starindex, Số 12 ngõ 9 Liễu Giai Ba Đình Hà Nội',
            text3: '18:00 - 02:00',
            text4: '#Lounge #Party #Gathering',
            like: false,
            id: 1
        },
        {
            imgBgr: require('../../assets/venueimg.png'),
            imgMain: require('../../assets/girl_venue_list.png'),
            text1: '1900 LE THÉÂTRE 1900 LE THÉÂTRE',
            text2: 'Sân vận động Starindex, Số 12 ngõ 9 Liễu Giai Ba Đình Hà Nội',
            text3: '18:00 - 02:00',
            text4: '#Lounge #Party #Gathering',
            like: false,
            id: 2
        },
        {
            imgBgr: require('../../assets/venueimg.png'),
            imgMain: require('../../assets/girl_venue_list.png'),
            text1: '1900 LE THÉÂTRE 1900 LE THÉÂTRE',
            text2: 'Sân vận động Starindex, Số 12 ngõ 9 Liễu Giai Ba Đình Hà Nội',
            text3: '18:00 - 02:00',
            text4: '#Lounge #Party #Gathering',
            like: false,
            id: 3
        },
        {
            imgBgr: require('../../assets/venueimg.png'),
            imgMain: require('../../assets/girl_venue_list.png'),
            text1: '1900 LE THÉÂTRE 1900 LE THÉÂTRE',
            text2: 'Sân vận động Starindex, Số 12 ngõ 9 Liễu Giai Ba Đình Hà Nội',
            text3: '18:00 - 02:00',
            text4: '#Lounge #Party #Gathering',
            like: false,
            id: 4
        },
        {
            imgBgr: require('../../assets/venueimg.png'),
            imgMain: require('../../assets/girl_venue_list.png'),
            text1: '1900 LE THÉÂTRE 1900 LE THÉÂTRE',
            text2: 'Sân vận động Starindex, Số 12 ngõ 9 Liễu Giai Ba Đình Hà Nội',
            text3: '18:00 - 02:00',
            text4: '#Lounge #Party #Gathering',
            like: false,
            id: 5
        },
        {
            imgBgr: require('../../assets/venueimg.png'),
            imgMain: require('../../assets/girl_venue_list.png'),
            text1: '1900 LE THÉÂTRE 1900 LE THÉÂTRE',
            text2: 'Sân vận động Starindex, Số 12 ngõ 9 Liễu Giai Ba Đình Hà Nội',
            text3: '18:00 - 02:00',
            text4: '#Lounge #Party #Gathering',
            like: false,
            id: 6
        },
    ])
    const onChangeLike = (index) => {
        let dataFetch = [...itemList]
        dataFetch[index].like = !dataFetch[index].like
        setItemList(dataFetch)
    }
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
            <View style={{
                // paddingBottom: '100%',
                backgroundColor: 'white',
                // flex: 1
            }}>
                <Text style={styles.txt1}>Venue</Text>

                <FlatList
                    maxToRenderPerBatch={6}
                    horizontal={false}
                    numColumns={2}
                    data={itemList}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                // style={{ paddingBottom: '100%' }}
                />

                <TouchableOpacity
                    onPress={() => { navigation.navigate('VenueLike') }}
                    style={styles.view1}>
                    <Text style={styles.txt2}>See more</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    view1: {
        height: 36 * scale,
        width: 328 * scale,
        alignItems: "center",
        alignSelf: "center",
        justifyContent: "center",
        borderWidth: 1,
        borderColor: "#4F4F4F",
        borderRadius: 4 * scale,
        marginTop: 16 * scale
    },
    txt1: {
        lineHeight: 25.14,
        fontSize: 20,
        fontFamily: 'SourceSansPro-SemiBold',
        color: '#333333',
        marginTop: 32 * scale,
        marginLeft: 16 * scale
    },
    txt2: {
        lineHeight: 20.11,
        fontSize: 16,
        fontFamily: 'SourceSansPro-Regular',
        color: '#4F4F4F',
    },
})
