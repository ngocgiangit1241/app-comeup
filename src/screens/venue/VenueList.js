import React, { useState, useEffect, useRef } from 'react';
import Touchable from '../../screens_view/Touchable';
import { convertLanguage } from '../../services/Helper';
import ModalCity from '../../components/ModalCity';
import ModalTag from '../../components/ModalTag';
import ToggleSwitch from 'toggle-switch-react-native';
import Carousel from 'react-native-snap-carousel';
import { actListVenueList, actLikeVenues, actGetBanners, actListVenueList2 } from '../../actions/venue';
import { useSelector, useDispatch } from 'react-redux';
import {
  View,
  Text,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  FlatList,
  SafeAreaView,
  ActivityIndicator, Platform, Linking, PixelRatio, RefreshControl
} from 'react-native';
import VenueListItem from '../../components/venue/VenueListItem';
import AnimatedHideView from '../../components/react-native-animated-hide-view';
import Dots from 'react-native-dots-pagination';
import FastImage from 'react-native-fast-image';
import * as Types2 from '../../constants/ActionType2';
import { actSelectCountryByVenue } from '../../actions/city';
import { actLoadDataCategory } from '../../actions/category';
import Popover from 'react-native-popover-view';



const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const scale = screenWidth / 360;
const scaleFontSize = PixelRatio.getFontScale()

export default function VenueList({ navigation, route }) {
  const statusRef = useRef({})
  const dispatch = useDispatch();
  const { language } = useSelector(state => state.language)
  const venueListLL = useSelector(state => state.venue)
  const city = useSelector(state => state.city)
  const global = useSelector(state => state.global)
  const category = useSelector(state => state.category)
  const [scroll, setScroll] = useState(true)
  const [timeOpen, setTimeOpen] = useState(false);
  const [refreshing, setRefreshing] = useState(false)
  const [filler, setFiller] = useState({
    tag: global?.city?.Id ?
      route.params && route.params['tag'] ? route.params['tag'] : { name: convertLanguage(language, 'all_categories'), value: '' }
      :
      ''
    ,
    area: global?.city?.Id ?
      global.city
      :
      global.country
    ,
    status_team: {
      name: convertLanguage(language, 'popular'),
      value: 'popular',
      icon: require('../../assets/ic_popular.png')
    },
    type: global?.city?.Id ?
      'city'
      :
      'country'
    ,
    modalCity: false,
    modalTag: false,
    modalType: false,
  })
  const [data, setData] = useState({
    tags: '',
    area: 1,
    page: 1,
    lat: global.city?.Lat,
    long: global.city?.Lng
  })
  const [banner, setBanner] = useState([])
  const [itemList, setItemList] = useState([]);

  useEffect(() => {
    dispatch(actGetBanners()).then(data => {
      setBanner(data.data)
    })
    dispatch(actSelectCountryByVenue())
    dispatch(actLoadDataCategory('venue'))
    return () => dispatch({ type: Types2.LOAD_DATA_LIST_CLEAR })
  }, [])

  useEffect(() => {
    if (data.page !== 1) {
      dispatch(actListVenueList(filler?.tag?.value, filler.type === 'city' ? filler.area.Id : filler.area.Code, data.page, timeOpen, filler?.status_team?.value))
    }
  }, [data.page])

  useEffect(() => {
    dispatch(actListVenueList2(filler?.tag?.value, filler.type === 'city' ? filler.area.Id : filler.area.Code, data.page, timeOpen, filler?.status_team?.value))
  }, [timeOpen, filler?.tag?.value, filler?.area, filler?.status_team?.value])

  useEffect(() => {
    let dataFetch = [...venueListLL.venueList]
    setItemList(dataFetch)
  }, [venueListLL.venueList])

  const onChangeLike = (index, Id) => {
    dispatch(actLikeVenues(Id)).then(data => {
    })
  }

  const showDataCategory = () => {
    var result = [
      {
        name: convertLanguage(language, 'all_categories'),
        value: '',
      },
    ];
    category.categories.forEach(category => {
      result.push({
        name: category.HashTagName,
        value: category.Id,
      });
    });
    return result;
  }
  //screens
  const _renderFooter = () => {
    if (data.page !== venueListLL.last_page) {
      return (
        <ActivityIndicator
          size="large"
          color="#000000"
          style={{ padding: 20, flex: 1, marginTop: 12 * scale }}
        />
      );
    }
    else {
      return (
        <View style={{ marginTop: 12 * scale }} />

      );
    }
  };
  const onLoadMore = () => {
    if (data.page !== venueListLL.last_page) {
      setData({ ...data, page: data.page + 1 })
    }
  }

  const onSelect = async (key, value) => {
    await setFiller({ ...filler, [key]: value, modalCity: false, type: key === 'area' ? 'city' : filler.type, modalTag: false, modalType: false })
    if (key === 'area') {
      await setData({ ...data, page: 1, lat: value.Lat, long: value.Lng })
    } else {
      await setData({ ...data, page: 1 })
    }

  }

  const onRefresh = async () => {
    setScroll(true)
    setTimeOpen(false);
    setRefreshing(false)

    await setFiller({
      tag: global?.city?.Id ?
        route.params && route.params['tag'] ? route.params['tag'] : { name: convertLanguage(language, 'all_categories'), value: '' }
        :
        ''
      ,
      area: global?.city?.Id ?
        global.city
        :
        global.country
      ,
      status_team: {
        name: convertLanguage(language, 'popular'),
        value: 'popular',
        icon: require('../../assets/ic_popular.png')
      },
      type: global?.city?.Id ?
        'city'
        :
        'country'
      ,
      modalCity: false,
      modalTag: false,
      modalType: false,
    })
    await setData({
      tags: '',
      area: 1,
      page: 1,
    })

    dispatch(actGetBanners()).then(data => {
      setBanner(data.data)
    })
    dispatch(actSelectCountryByVenue())
    dispatch(actLoadDataCategory('venue'))
  }


  useEffect(() => {
    let checkTag = false
    let checkStatusTeam = 'popular'
    if (filler.tag.value === '') {
      checkTag = true
    }
    if (filler.status_team.value === 'popular') {
      checkStatusTeam = 'popular'

    } else if (filler.status_team.value === 'new') {
      checkStatusTeam = 'new'

    } else {

    }
    setFiller({
      ...filler, status_team: {
        name: convertLanguage(language, checkStatusTeam),
        value: checkStatusTeam,
        icon: checkStatusTeam === 'new' ? require('../../assets/ic_new.png') : require('../../assets/ic_popular.png')
      },
      tag: checkTag ? { name: convertLanguage(language, 'all_categories'), value: '' } : { ...filler.tag }
    })
  }, [language])

  const _renderHeader = () => {
    return (
      <View style={{ flex: 1 }}>
        <Carousel
          data={banner}
          renderItem={({ item, index }) => {
            return (
              <TouchableOpacity key={index} onPress={() => Linking.openURL(item.LinkApp)} disabled={item.LinkApp === '#'}><FastImage style={{ backgroundColor: '#bcbcbc', aspectRatio: 2.4, }} source={{ uri: item.Images ? item.Images.Medium : item.Image }} /></TouchableOpacity>
            )
          }}
          sliderWidth={screenWidth}
          itemWidth={screenWidth}
          inactiveSlideScale={1}
          // onSnapToItem={(index) => this.setState({ activeSlide: index })}
          autoplay={true}
          autoplayInterval={3000}
          loop
        />

        <View style={styles.boxFilter}>
          <View style={styles.boxFilter1}>
            <Touchable
              style={[styles.boxItemFilter, { flex: 0.47 }]}
              // onPress={toggleModal}
              onPress={() => setFiller({ ...filler, modalCity: true })}
            >
              <Image
                source={require('../../assets/ic_location_blue.png')}
                style={styles.ic_location}
              />

              <Text style={styles.txtFilter} numberOfLines={1}>
                {filler.area['Name_' + language]}
              </Text>
              <Image
                source={require('../../assets/select_arrow_black.png')}
                style={styles.ic_dropdown}
              />
            </Touchable>
            <TouchableOpacity
              ref={statusRef}
              style={[styles.boxItemFilter, { flex: 0.47 }]}
              onPress={() => setFiller({ ...filler, modalType: true })}
            >
              <Image
                source={filler.status_team.icon}
                style={styles.ic_location}
              />
              <Text style={styles.txtFilter} numberOfLines={1}>
                {filler.status_team.name}
              </Text>
              <Image
                source={require('../../assets/select_arrow_black.png')}
                style={styles.ic_dropdown}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.boxItemFilter1}

            onPress={() => setFiller({ ...filler, modalTag: true })}
          >
            <Image
              source={require('../../assets/ic_category_blue.png')}
              style={styles.ic_location}
            />
            <Text style={styles.txtFilter} numberOfLines={1}>
              {filler?.tag?.name}
            </Text>
            <Image
              source={require('../../assets/select_arrow_black.png')}
              style={styles.ic_dropdown}
            />
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() => {
            setTimeOpen(!timeOpen);
            setData({ ...data, page: 1 })
          }}
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginRight: 16 * scale,
            justifyContent: 'flex-end'
          }}>
          <Text style={styles.txt3}>{convertLanguage(language, 'open_now')}</Text>
          <ToggleSwitch
            isOn={timeOpen}
            onColor="#00A9F4"
            offColor="gray"
            size="small"
            onToggle={isOn => {
              setTimeOpen(isOn);
              setData({ ...data, page: 1 })
            }}
          />
        </TouchableOpacity>
      </View>
    )
  }
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View
          style={{
            alignSelf: 'stretch',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 5 * scale,
          }}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{
              minWidth: 32 * scale,
              minHeight: 32 * scale,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../../assets/icon_back.png')}
              style={{ width: 22 * scale, height: 22 * scale }}
            />
          </TouchableOpacity>
          <Text style={{ fontSize: 16 * scale, color: '#333333', fontFamily: 'SourceSansPro-Bold', }}>
            {convertLanguage(language, 'venue')}
          </Text>
          <TouchableOpacity
            style={{
              minHeight: 32 * scale,
              minWidth: 32 * scale,
              marginRight: 5 * scale,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => navigation.navigate('SearchResult')}>
            <Image
              source={require('../../assets/search.png')}
              style={{ width: 28 * scale, height: 28 * scale }}
            />
          </TouchableOpacity>
        </View>
        {/* <ScrollView> */}
        {
          venueListLL.loading &&
          <ActivityIndicator
            size="large"
            color="#000000"
            style={{ position: 'absolute', zIndex: 999, height: '100%', width: '100%', backgroundColor: 'rgba(105, 105, 105, 0.3)' }}
          />
        }
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
            />
          }
          style={{ paddingBottom: '5%' }}
          data={itemList}
          ListHeaderComponent={_renderHeader()}
          ListFooterComponent={_renderFooter}
          ListEmptyComponent={<Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'venue_empty')}</Text>}
          keyExtractor={(item) => item.Id.toString() || Math.random().toString()}
          onEndReached={() => onLoadMore()}
          onEndReachedThreshold={0.5}
          onScrollBeginDrag={event => {
            setScroll(false);
          }}
          onScrollEndDrag={event => {
            if (Platform.OS === 'ios')
              setScroll(true);
          }}
          onMomentumScrollBegin={event => {
            if (Platform.OS === 'ios')
              setScroll(false);
          }}
          onMomentumScrollEnd={event => {
            setScroll(true);

          }}
          renderItem={({ item, index }) => {
            return (
              <VenueListItem
                item={item}
                navigation={navigation}
                onChangeLike={(index, id) => onChangeLike(index, id)}
                index={index}
                key={index}
              />
            )
          }}
        />
        {/* </ScrollView> */}
      </View>
      {itemList?.length > 0 &&
        <AnimatedHideView
          visible={scroll}
          style={styles.view6}
          duration={300}
          onPress={() => {
            navigation.navigate('VenueMap', { filler, city, global, category, lat: data.lat, long: data.long });
          }}
        >
          <View

            style={{
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row',
            }}
          >
            <Image
              style={{ width: 15, height: 20 }}
              source={require('../../assets/venue_list_position.png')}
            />
            <Text style={styles.txt4}>{convertLanguage(language, 'view_map')}</Text>
          </View>
        </AnimatedHideView>
      }
      {/*  */}
      <Popover
        isVisible={filler.modalType}
        animationConfig={{ duration: 0 }}
        fromView={statusRef.current}
        placement='bottom'
        popoverStyle={{
          borderRadius: 8,
          width: 156 * scale,
          padding: 12 * scale
        }}
        arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
        backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.05)', }}
        onRequestClose={() => setFiller({ ...filler, modalType: false })}>
        <View style={styles.boxPopover}>
          <TouchableOpacity onPress={() => onSelect('status_team', { name: convertLanguage(language, 'popular'), value: 'popular', icon: require('../../assets/ic_popular.png') })} style={styles.boxCountry}>
            <Image source={require('../../assets/ic_popular.png')} style={styles.ic_country} />
            <Text style={styles.txtCountry}>{convertLanguage(language, 'popular')}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => onSelect('status_team', { name: convertLanguage(language, 'newTeam'), value: 'new', icon: require('../../assets/ic_new.png') })} style={styles.boxCountry}>
            <Image source={require('../../assets/ic_new.png')} style={[styles.ic_country]} />
            <Text style={styles.txtCountry}>{convertLanguage(language, 'newTeam')}</Text>
          </TouchableOpacity>
        </View>
      </Popover>
      {/*  */}
      {
        filler.modalCity && city.venue_countries.length > 0 ? (
          <ModalCity
            modalVisible={filler.modalCity}
            closeModal={() => setFiller({ ...filler, modalCity: false })}
            countries={city.venue_countries}
            selectCity={city => onSelect('area', city)}
          />
        ) : null
      }
      {
        filler.modalTag ? (
          <ModalTag
            modalVisible={filler.modalTag}
            closeModal={() => setFiller({ ...filler, modalTag: false })}
            tags={showDataCategory()}
            selectTag={tag => onSelect('tag', tag)}
          />
        ) : null
      }

    </SafeAreaView >
  );
}

const styles = StyleSheet.create({
  view1: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    height: 240 * scale,
    borderRadius: 4 * scale,
    backgroundColor: '#FAFAFA',
    marginTop: 16 * scale,
  },
  view2: {
    height: 84 * scale,
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    marginTop: 16 * scale,
    backgroundColor: 'blue',
  },
  view3: {
    height: 64 * scale,
    width: 64 * scale,
    position: 'absolute',
    top: 68 * scale,
    left: 32 * scale,
    // borderRadius: 4 * scale,
    backgroundColor: 'yellow',
    zIndex: 100,
  },
  view4: {
    height: 18.33 * scale,
    width: 20 * scale,
    // backgroundColor: 'gray',
    marginTop: 11 * scale,
    marginLeft: 290 * scale,
  },
  view5: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    height: 24 * scale,
    // backgroundColor: 'green',
    marginTop: 10.67 * scale,
  },
  view5_1: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    height: 20 * scale,
    // backgroundColor: 'green',
    marginTop: 2 * scale,
  },

  view6: {
    borderRadius: 32,
    shadowOffset: { width: 0, height: 6 },
    shadowColor: 'black',
    shadowRadius: 9,
    shadowOpacity: 0.3,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 5,
    position: 'absolute',
    bottom: 20,
    alignSelf: 'center',
    backgroundColor: '#00A9F4',
    flexDirection: 'row',
    height: 48,
    paddingHorizontal: 12,
    zIndex: 100
  },
  view7: {},
  view8: {},
  view9: {},
  txt1: {
    lineHeight: 20.11,
    fontSize: 16,
    fontWeight: '600',
    fontFamily: 'SourceSansPro-Black',
    color: '#333333',
  },
  txt2: {
    lineHeight: 20.11,
    fontSize: 16,

    fontFamily: 'SourceSansPro-Regular',
    color: '#333333',
    fontStyle: 'normal',
  },
  txt2_1: {
    lineHeight: 20.11,
    fontSize: 16,

    fontFamily: 'SourceSansPro-Regular',
    color: '#4F4F4F',
    fontStyle: 'normal',
  },
  txt3: {
    // lineHeight: 20.11,
    fontSize: 12 * scale,
    fontWeight: '400',
    fontFamily: 'SourceSansPro-Regular',
    color: '#00A9F4', marginRight: 3 * scale
  },
  txt4: {
    lineHeight: 20.11,
    fontSize: 16,
    fontWeight: '400',
    fontFamily: 'SourceSansPro-Regular',
    color: '#FFFFFF',
    marginLeft: 12 * scale,

  },
  boxFilter: {
    padding: 16 * scale,
    paddingBottom: 8 * scale

  },
  boxFilter1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 10 * scale,
  },
  boxItemFilter: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 36,
    borderRadius: 8,
    borderColor: '#BDBDBD',
    borderWidth: 1,
    paddingHorizontal: 10,
  },
  boxItemFilter1: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 36,
    // width: 328,
    borderRadius: 8,
    borderColor: '#BDBDBD',
    borderWidth: 1,
    paddingHorizontal: 10,
    width: '100%',
  },
  ic_location: {
    width: 20,
    height: 20,
  },
  ic_dropdown: {
    width: 16,
    height: 16,
  },
  txtFilter: {
    fontSize: 14,
    color: '#333333',
    fontWeight: 'bold',
  },
  boxCountry: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 6
  },
  ic_country: {
    width: 24,
    height: 24,
    marginRight: 24
  },
  txtCountry: {
    color: '#4F4F4F',
    fontSize: 16 * scale / scaleFontSize,
    fontWeight: '600'
  },
})
