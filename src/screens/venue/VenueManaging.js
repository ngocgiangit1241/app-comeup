import React, { useState, useEffect } from 'react';
import VenueManagingItem from "../../components/venue/VenueManagingItem"
import { actLoadDataMyProfile, actHostingVenue } from '../../actions/user'
import { useDispatch, useSelector } from 'react-redux';
import {
  View,
  Text,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  FlatList,
  SafeAreaView, ActivityIndicator
} from 'react-native';
import { convertLanguage } from '../../services/Helper'

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const scale = screenWidth / 360;
export default function VenueManaging({ navigation, route }) {
  const dispatch = useDispatch();

  const [page, setPage] = useState(1)
  const [perPage, setPerPage] = useState(12)
  useEffect(() => {
    dispatch(actHostingVenue(page, perPage))
  }, [page])
  const venueManaging = useSelector(state => state.user.hostingVenue.venue)
  const load_more = useSelector(state => state.user.hostingVenue.load_more)
  const language = useSelector(state => state.language.language)

  const refresh = () => {
    setPage(1)
  }
  console.log('page', page)
  console.log('load_more', load_more)
  console.log('venueManaging', venueManaging)
  const _renderEmpty = () => {
    return <Text style={{ padding: 20 * scale, textAlign: 'center', color: '#4F4F4F', fontSize: 16 * scale }} > {convertLanguage(language, 'venue_empty')}</Text>
  }

  const _renderFooter = () => {
    if (load_more) {
      return <ActivityIndicator size="large" color="#000000" style={styles.loading} />
    } else {
      return <Text style={{ padding: 20 * scale, textAlign: 'center', color: '#4F4F4F', fontSize: 16 * scale }} ></Text>
    }
    // {convertLanguage(language, 'venue_empty')}
  }

  const loadMore = () => {
    if (load_more) {
      setPage(page + 1)
    }
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>

      <View style={{
        borderBottomColor: '#F2F2F2',
        borderBottomWidth: 1,
        // width: screenWidth - 32 * scale,
        height: 48 * scale,
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
      }}>
        <TouchableOpacity onPress={() => { navigation.goBack() }} style={styles.view1}>
          {/* <TouchableOpacity> */}
          <Image
            style={{
              width: 18 * scale,
              height: 18 * scale,
            }}
            source={require('../../assets/back_icon.png')}
          />
          {/* </TouchableOpacity> */}

        </TouchableOpacity>
        <View style={{ alignItems: 'center', flex: 1, marginRight: 16 * scale }}>
          <Text style={styles.txt1}>{convertLanguage(route.params?.language, 'hosting_venue')}</Text>
        </View>

      </View >

      <FlatList
        style={{ paddingBottom: '5%' }}
        data={venueManaging}
        keyExtractor={item => item.Id.toString()}
        renderItem={({ item, index }) => {
          return (
            <VenueManagingItem
              key={index}
              navigation={navigation}
              item={item}
              index={index}
            // manaGingList={manaGingList}
            />
          )
        }}
        onEndReached={() => loadMore()}
        onEndReachedThreshold={0.5}
        refreshing={false}
        onRefresh={() => refresh()}
        ListFooterComponent={() => _renderFooter()}
        ListEmptyComponent={() => _renderEmpty()}
      />
    </SafeAreaView >
  )
}

const styles = StyleSheet.create({
  view1: {
    // backgroundColor: 'red',
    paddingLeft: 16 * scale
  },
  txt1: {
    lineHeight: 22.63 * scale,
    fontSize: 18 * scale,
    fontWeight: 'bold',
    fontFamily: 'SourceSansPro-SemiBold',
    color: '#4F4F4F',
  },
  loading: {
    padding: 20 * scale
  },
});