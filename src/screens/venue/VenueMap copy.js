import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import ToggleSwitch from 'toggle-switch-react-native';
import Touchable from '../../screens_view/Touchable';
import VenueMapItem from '../../components/venue/VenueMapItem';
import { SafeAreaConsumer } from 'react-native-safe-area-context';
import pin_1_icon from '../../assets/pin_1_icon.png';
import pin_1_icon_active from '../../assets/pin_1_icon_active.png';
import pin_menu_icon from '../../assets/pin_menu_icon.png';
import pin_menu_icon_active from '../../assets/pin_menu_icon_active.png';
import pin_icon_transparent from '../../assets/pin_icon_transparent.png';
import { check, PERMISSIONS, RESULTS, openSettings, request } from 'react-native-permissions';
import {
  ActivityIndicator,
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  Dimensions,
  Linking,
  ScrollView,
  SafeAreaView,
  Platform, PermissionsAndroid,
  TouchableOpacity, PixelRatio
} from 'react-native';
import ModalCity from '../../components/ModalCity';
// import { actListVenueMap, actLikeVenues } from '../../actions/venue2'
import { actListVenueMap, actLikeVenues, actListVenueMap2 } from '../../actions/venue'
import ClusteredMapView from 'react-native-maps-super-cluster';
import { Marker, ProviderPropType, Callout, PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import * as Types from '../../constants/ActionType';
import Popover from 'react-native-popover-view';
import ModalTag from '../../components/ModalTag';
import { convertLanguage } from '../../services/Helper';
import * as Config from '../../constants/Config';

const screenWidth = Dimensions.get('window').width;
const { width, height } = Dimensions.get('window');
const scale = screenWidth / 360;
const ASPECT_RATIO = width / height;
const scaleFontSize = PixelRatio.getFontScale()

export default function VenueMap({ navigation, route }) {
  const statusRef = useRef({})
  const map = useRef('')
  const dispatch = useDispatch();
  // const venues = useSelector(state => state.venue2)
  const venues = useSelector(state => state.venue)
  const { language } = useSelector(state => state.language)
  const global = useSelector(state => state.global)
  const [timeOpen, setTimeOpen] = useState(false);
  const [filler, setFiller] = useState(route.params.filler)
  const city = route.params.city
  const category = route.params.category
  const [data, setData] = useState({
    tags: '',
    area: 1,
    page: 1,
    area: { Id: 1 },
    lat: 21.027765,
    lng: 105.834160,
    latitudeDelta: 0.2,
    longitudeDelta: 0.2 * ASPECT_RATIO,
    markers: [],
    sliderActiveSlide: 0,
    venues: [],
    showButtonReSearch: true,
    isEdit: false,
    type: 'country'
  })




  useEffect(() => {
    return () => {
      dispatch({ type: Types.VENUEMAP_CLEAR })
    }
  }, [])




  useEffect(() => {
    setData({
      ...data,
      area: global.city.Id
        ? global.city
        : global.country,
      lat: global.city.Id
        ? global.city.Lat
        : global.country.Lat,
      lng: global.city.Id
        ? global.city.Lng
        : global.country.Lng,
    })
  }, [global])

  useEffect(() => {
    dispatch(actListVenueMap(filler?.tag?.value, filler.type === 'city' ? filler.area.Id : filler.area.Code, timeOpen, filler?.status_team?.value)) //data.page,, filler?.status_team?.value
    onSelect('area', filler?.area)
  }, [timeOpen, filler?.tag?.value, filler?.area, filler?.status_team?.value])

  const onSelect = async (key, value) => {
    await setFiller({ ...filler, [key]: value, modalCity: false, type: key === 'area' ? 'city' : filler.type, modalTag: false, modalType: false })
    await setData({ ...data, page: 1 })
    if (key === 'area') {
      const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${value.Name}&key=${Config.GOOGLE_API_KEY}`;
      try {
        const result = await fetch(apiUrl);
        const json = await result.json();
        if (json.results.length > 0) {
          var region = {
            latitude: parseFloat(json.results[0].geometry.location.lat),
            longitude: (json.results[0].geometry.location.lng),
            latitudeDelta: 0.2,
            longitudeDelta: 0.2 * ASPECT_RATIO
          }
          map?.current?.getMapRef()?.animateToRegion(region), 10
        }
      } catch (err) {
      }
    }
  }

  const showDataCategory = () => {
    var result = [
      {
        name: convertLanguage(language, 'all_categories'),
        value: '',
      },
    ];
    category.categories.forEach(category => {
      result.push({
        name: category.HashTagName,
        value: category.Id,
      });
    });
    return result;
  }

  useEffect(() => {
    let dataFetch = [...venues.venueMap]
    setItemList(dataFetch)
    //marker
    if (venues.venueMap.length > 0) {
      var { sliderActiveSlide } = data;
      var markers = getDataMarkers(venues.venueMap)
      var region = {
        latitude: markers[sliderActiveSlide].location.latitude,
        longitude: markers[sliderActiveSlide].location.longitude,
        latitudeDelta: data.latitudeDelta,
        longitudeDelta: data.longitudeDelta
      }
      let check = false
      if (data.type === 'country') {
        // setTimeout(() => {
        //   if (map?.current !== '' || map?.current?.getMapRef() !== null && map?.current !== null)
        //     map?.current?.getMapRef()?.animateToRegion(region), 10
        // });

        check = true
      }
      if (!check) {

        setData({
          ...data,
          venues: venues.venueMap,
          markers: getDataMarkers(venues.venueMap),
          showButtonReSearch: false
        })
      } else {

        setData({
          ...data,
          venues: venues.venueMap,
          markers: getDataMarkers(venues.venueMap),
          showButtonReSearch: false,
          lat: markers[sliderActiveSlide].location.latitude,
          lng: markers[sliderActiveSlide].location.longitude,
        })
      }
    } else {

      // setData({
      //   ...data,
      //   venues: venues.venueMap,
      //   markers: [],
      //   showButtonReSearch: true,
      //   lat: 21.027765,
      //   lng: 105.834160,
      //   latitudeDelta: 0.2,
      //   longitudeDelta: 0.2 * ASPECT_RATIO,
      // })
    }


  }, [venues])



  const [itemList, setItemList] = useState([]);

  const onChangeLike = (index, Id) => {
    dispatch(actLikeVenues(Id)).then(data => {
      if (data.status === 200) {
        // let dataFetch = [...itemList]
        // dataFetch[index].IsLike = !dataFetch[index].IsLike
        // console.log('123', dataFetch);
        // setItemList(dataFetch)
      }
    })

  }

  const _renderFooter = () => {
    if (data.page !== venues.last_pageMap) {
      return (
        <ActivityIndicator
          size="large"
          color="#000000"
          style={{ padding: 20, flex: 1 }}
        />
      );
    } else {
      return (
        <Text style={{ textAlign: 'center' }}>
          {/* {convertLanguage(language, 'event_empty')} */}
        </Text>
      );
    }

  };
  const loadMore = () => {
    if (data.page !== venues.last_pageMap)
      setData({ ...data, page: data.page + 1 })
  }

  const getDataMarkers = (venue_maps) => {
    var data_markers = [];
    var i = 0;
    venue_maps.forEach((venue) => {
      if (typeof venue.Lat != 'undefined' && typeof venue.Long != 'undefined' && venue.Lat !== "" && venue.Long !== "") {
        let index = data_markers.findIndex(marker => marker.location.latitude == venue.Lat && marker.location.longitude == venue.Long);
        if (index !== -1) {
          data_markers[index].venues.push(venue);
          data_markers[index].keys.push(i)
          data_markers.push({
            location: {
              latitude: parseFloat(venue.Lat),
              longitude: parseFloat(venue.Long)
            },
            venues: [],
            keys: data_markers[index].keys,
            showImage: false,
            index: i
          });
          i++;
        } else {
          data_markers.push({
            location: {
              latitude: parseFloat(venue.Lat),
              longitude: parseFloat(venue.Long)
            },
            venues: [venue],
            keys: [i],
            showImage: true,
            index: i
          });
          i++;
        }
      }
    })
    return data_markers;
  }
  const onPanDrag = () => {
    // let showButtonReSearch = true,
    // if (data.showButtonReSearch) {
    //   showButtonReSearch = false
    // }
    setData({ ...data, isEdit: false, showButtonReSearch: false })
    console.log('ss')
  }
  const onRegionChangeComplete = (center) => {
    var { isEdit } = data;
    setData({
      ...data,
      lat: center.latitude && center.latitude === 0 ? data.lat : center.latitude,
      lng: center.longitude && center.longitude === 0 ? data.lng : center.longitude,
      latitudeDelta: center.latitudeDelta,
      longitudeDelta: center.longitudeDelta,
      showButtonReSearch: true,
      isEdit: false
    })

    console.log('aa')
  }

  const renderCluster = (cluster, onPress) => {
    const pointCount = cluster.pointCount,
      coordinate = cluster.coordinate,
      clusterId = cluster.clusterId
    const clusteringEngine = map.current.getClusteringEngine(),
      clusteredPoints = clusteringEngine.getLeaves(clusterId, 100)

    return (
      <Marker coordinate={coordinate} onPress={(onPress)} key={cluster.clusterId}>
        <View style={styles.myClusterStyle}>
          <Text style={styles.myClusterTextStyle}>
            {pointCount}
          </Text>
        </View>
      </Marker>
    )
  }
  const renderMarker = (data) => (
    <Marker coordinate={data.location}
      // onPress={() => onOpen(data)}
      mapMarker={data}
      key={data.index}
    >
      {

        data.showImage ?

          <View style={{ width: data.venues.length === 1 ? 32 : 40, height: 45 }}>
            <Image source={showImageMarker(data)} style={{ width: 32, height: 45 }} />
            {
              showNumberEvent(data)
            }
            {
              data.keys.length === 1 ?
                <Callout style={styles.plainView} >
                  <Text>{data.venues[0].Name}</Text>
                </Callout>
                :
                <Callout style={styles.plainView} >
                  <Text>{data.venues[0].Address}</Text>
                </Callout>

            }
          </View>

          :
          <View style={{ width: 0, height: 0, backgroundColor: '#000000' }}></View>
      }
    </Marker >
  )


  const searchEventByLocation = () => {
    var { lat, lng, latitudeDelta, longitudeDelta } = data;
    // setState({ sliderActiveSlide: 0, modal_events: [], showButtonReSearch: false })
    var north = lat + latitudeDelta / 2;
    var east = lng + longitudeDelta / 2;
    var south = lat - latitudeDelta / 2;
    var west = lng - longitudeDelta / 2;

    // props.onLoadEventByMaps(north, east, south, west, '', tag.value, time_event.value, start, finish, type);
    dispatch(actListVenueMap(filler?.tag?.value, filler.type === 'city' ? filler.area.Id : filler.area.Code, timeOpen, filler?.status_team?.value, north, south, east, west))

    setData({
      ...data,
      area: {
        Name: 'Map Area',
        Id: '',
        Name_vi: 'Khu vực',
        Name_en: 'Map Area',
        Name_ko: '지도 영역'
      },
      sliderActiveSlide: 0,
      showButtonReSearch: false
    })
  }


  const onPressToMyLocation = () => {
    var permission = Platform.OS === 'ios' ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
    check(permission).then(response => {
      if (response === RESULTS.BLOCKED) {
        _alertForLocationPermission()
      } else if (response === RESULTS.GRANTED) {
        Geolocation.getCurrentPosition((position) => {
          setData({
            ...data,
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          })
          var region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: data.latitudeDelta,
            longitudeDelta: data.longitudeDelta
          }
          setTimeout(() => {
            if (map?.current !== '' || map?.current?.getMapRef() !== null && map?.current !== null)
              map?.current?.getMapRef()?.animateToRegion(region), 10
          });
        })
      } else if (response === RESULTS.DENIED) {
        request(permission).then(response2 => {
          if (response2 === RESULTS.GRANTED) {
            Geolocation.getCurrentPosition((position) => {
              setData({
                ...data,
                lat: position.coords.latitude,
                lng: position.coords.longitude,
              })
              var region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: data.latitudeDelta,
                longitudeDelta: data.longitudeDelta
              }
              setTimeout(() => {
                if (map?.current !== '' || map?.current?.getMapRef() !== null && map?.current !== null)
                  map?.current?.getMapRef()?.animateToRegion(region), 10
              });
            })
          }
        })
      } else {
        _alertForLocationPermission()
      }
    });
  }
  const onPressZoomIn = () => {
    var region = {
      latitude: data.lat,
      longitude: data.lng,
      latitudeDelta: data.latitudeDelta / 2,
      longitudeDelta: data.longitudeDelta / 2
    }
    setData({
      ...data,
      latitudeDelta: data.latitudeDelta / 2,
      longitudeDelta: data.longitudeDelta / 2
    })
    setTimeout(() => {
      if (map?.current !== '' && map?.current?.getMapRef() !== null && map?.current !== null)
        map?.current?.getMapRef()?.animateToRegion(region), 10
    });

  }
  const onPressZoomOut = () => {
    var region = {
      latitude: data.lat,
      longitude: data.lng,
      latitudeDelta: data.latitudeDelta * 2,
      longitudeDelta: data.longitudeDelta * 2
    }
    setData({
      ...data,
      latitudeDelta: data.latitudeDelta * 2,
      longitudeDelta: data.longitudeDelta * 2
    })

    setTimeout(() => {
      if (map?.current !== '' || map?.current?.getMapRef() !== null && map?.current !== null)
        map?.current?.getMapRef()?.animateToRegion(region), 10
    });
  }

  const _alertForLocationPermission = () => {
    setTimeout(() => {
      Alert.alert(
        convertLanguage(language, 'access_location_title'),
        convertLanguage(language, 'access_location_content'),
        [
          {
            text: convertLanguage(language, 'cancel'),
            style: 'destructive',
          },
          { text: convertLanguage(language, 'open_setting'), onPress: () => openSettings() },
        ],
      );
    }, 100)
  }
  const openSettings = () => {
    Platform.OS === 'ios' ? openSettings() : AndroidOpenSettings.appDetailsSettings();
    // props.navigation.goBack();
    navigation.goBack()
  }
  const showImageMarker = (data) => {
    var { sliderActiveSlide } = data;
    if (data.showImage) {
      var index = data.keys.indexOf(sliderActiveSlide);
      if (index !== -1) {
        switch (data.venues.length) {
          case 1:
            return pin_1_icon_active;
          default:
            return pin_menu_icon_active;
        }

      } else {
        switch (data.venues.length) {
          case 1:
            return pin_1_icon;
          default:
            return pin_menu_icon;
        }
      }
    } else {
      return pin_icon_transparent;
    }
  }

  const showNumberEvent = (data) => {
    if (data.showImage) {
      switch (data.venues.length) {
        case 1:
          return null;
        default:
          return <View style={{ width: 16, height: 16, borderRadius: 8, backgroundColor: '#EB5757', position: 'absolute', top: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ color: '#FFFFFF', fontSize: 11 }}>{data.venues.length}</Text>
          </View>
      }
    } else {
      return null;
    }
  }
  const _onMapReady = () => {
    if (Platform.OS === 'android') {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        .then(granted => {
          // this.setState({ paddingTop: 0 });
        });
    }
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        {
          venues.loading &&
          <ActivityIndicator
            size="large"
            color="#000000"
            style={{ position: 'absolute', zIndex: 999, height: '100%', width: '100%', backgroundColor: 'rgba(105, 105, 105, 0.3)' }}
          />
        }

        <View style={styles.viewHeader}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}>
            <Image
              style={{ height: 20 * scale, width: 20 * scale }}
              source={require('../../assets/arrow_back.png')}
            />
          </TouchableOpacity>
          <Touchable
            style={{
              flexDirection: 'row',
              // width: 122 * scale,
              height: '100%',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <Text style={styles.txt3}>{convertLanguage(language, 'open_now')}</Text>
            <ToggleSwitch
              isOn={timeOpen}
              onColor="#00A9F4"
              offColor="gray"
              size="normal"
              onToggle={isOn => {
                setTimeOpen(isOn);
              }}
            />
          </Touchable>
        </View>

        <ScrollView>
          <View style={styles.boxFilter}>
            <View style={styles.boxFilter1}>
              <Touchable
                style={[styles.boxItemFilter, { flex: 0.47 }]}
                // onPress={toggleModal}
                onPress={() => setFiller({ ...filler, modalCity: true })}
              >
                <Image
                  source={require('../../assets/ic_location_blue.png')}
                  style={styles.ic_location}
                />

                <Text style={styles.txtFilter} numberOfLines={1}>
                  {filler.area['Name_' + language]}
                </Text>
                <Image
                  source={require('../../assets/select_arrow_black.png')}
                  style={styles.ic_dropdown}
                />
              </Touchable>
              <TouchableOpacity
                ref={statusRef}
                style={[styles.boxItemFilter, { flex: 0.47 }]}
                onPress={() => setFiller({ ...filler, modalType: true })}
              >
                <Image
                  source={filler.status_team.icon}
                  style={styles.ic_location}
                />
                <Text style={styles.txtFilter} numberOfLines={1}>
                  {filler.status_team.name}
                </Text>
                <Image
                  source={require('../../assets/select_arrow_black.png')}
                  style={styles.ic_dropdown}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={styles.boxItemFilter1}

              onPress={() => setFiller({ ...filler, modalTag: true })}
            >
              <Image
                source={require('../../assets/ic_category_blue.png')}
                style={styles.ic_location}
              />
              <Text style={styles.txtFilter} numberOfLines={1}>
                {filler?.tag?.name}
              </Text>
              <Image
                source={require('../../assets/select_arrow_black.png')}
                style={styles.ic_dropdown}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.viewMap}>
            <ClusteredMapView
              provider={PROVIDER_GOOGLE}
              style={{ flex: 1 }}
              data={data.markers}
              initialRegion={{
                latitude: data.lat,
                longitude: data.lng,
                latitudeDelta: data.latitudeDelta,
                longitudeDelta: data.longitudeDelta,
              }}
              zoomEnabled={true}
              enableZoomControl={true}
              // style={{ height: 400, width: width }}
              height={256 * scale}
              // showsUserLocation={true}
              showsMyLocationButton={false}
              onRegionChange={onPanDrag}
              // onRegionChangeComplete={region => setRegion(region)}
              onRegionChangeComplete={onRegionChangeComplete}
              // onPress={(e) => console.log(e.nativeEvent.coordinate)}
              // onLongPress={(e) => setState({panMap: true})}
              ref={map}
              // onMapReady={_onMapReady}
              renderMarker={renderMarker}
              renderCluster={renderCluster}
            // scrollEnabled={panMap}
            />
            <View style={styles.boxAction}>
              {
                <Touchable style={styles.btnMyLocation} onPress={onPressToMyLocation}>
                  <Image source={require('../../assets/my_location.png')} style={styles.icMyLocation} />
                </Touchable>
              }
              <Touchable style={styles.btnPlus} onPress={() => onPressZoomIn()}>
                <Image source={require('../../assets/plus.png')} style={styles.icPlus} />
              </Touchable>
              <Touchable style={styles.btnMinus} onPress={onPressZoomOut}>
                <Image source={require('../../assets/minus.png')} style={styles.icMinus} />
              </Touchable>
            </View>
            {
              data.showButtonReSearch &&
              <Touchable style={styles.boxResearch} onPress={searchEventByLocation}>
                <Text style={styles.txtResearch}>{convertLanguage(language, 'research')}</Text>
              </Touchable>
            }
          </View>
          <FlatList
            style={{ paddingBottom: '5%' }}
            keyExtractor={item => item.Id.toString() || Math.random().toString()}
            data={itemList}
            ListFooterComponent={() => _renderFooter()}
            ListEmptyComponent={<Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'venue_empty')}</Text>}
            // onEndReached={() => loadMore()}
            onEndReachedThreshold={0.5}
            renderItem={({ item, index }) => {
              return (
                <View style={{ marginHorizontal: 16 * scale }}>
                  <TouchableOpacity onPress={() => navigation.navigate('VenueDetail', { Id: item?.Id })}>
                    <VenueMapItem
                      item={item}
                      navigation={navigation}
                      index={index}
                      onChangeLike={(index, id) => onChangeLike(index, id)}
                    />
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </ScrollView>

      </View>


      <Popover
        isVisible={filler.modalType}
        animationConfig={{ duration: 0 }}
        fromView={statusRef.current}
        placement='bottom'
        popoverStyle={{
          borderRadius: 8,
          width: 156 * scale,
          padding: 12 * scale
        }}
        arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
        backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.05)', }}
        onRequestClose={() => setFiller({ ...filler, modalType: false })}>
        <View style={styles.boxPopover}>
          <TouchableOpacity onPress={() => onSelect('status_team', { name: convertLanguage(language, 'popular'), value: 'popular', icon: require('../../assets/ic_popular.png') })} style={styles.boxCountry}>
            <Image source={require('../../assets/ic_popular.png')} style={styles.ic_country} />
            <Text style={styles.txtCountry}>{convertLanguage(language, 'popular')}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => onSelect('status_team', { name: convertLanguage(language, 'new'), value: 'new', icon: require('../../assets/ic_new.png') })} style={styles.boxCountry}>
            <Image source={require('../../assets/ic_new.png')} style={[styles.ic_country, { marginRight: 35 }]} />
            <Text style={styles.txtCountry}>{convertLanguage(language, 'new')}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => onSelect('status_team', { name: convertLanguage(language, 'famous'), value: 'famous', icon: require('../../assets/ic_famous.png') })} style={styles.boxCountry}>
            <Image source={require('../../assets/ic_famous.png')} style={styles.ic_country} />
            <Text style={styles.txtCountry}>{convertLanguage(language, 'famous')}</Text>
          </TouchableOpacity>
        </View>
      </Popover>

      {/*  */}
      {filler.modalCity && city.venue_countries.length > 0 ? (
        <ModalCity
          modalVisible={filler.modalCity}
          closeModal={() => setFiller({ ...filler, modalCity: false })}
          countries={city.venue_countries}
          selectCity={city => onSelect('area', city)}
        />
      ) : null}

      {filler.modalTag ? (
        <ModalTag
          modalVisible={filler.modalTag}
          closeModal={() => setFiller({ ...filler, modalTag: false })}
          tags={showDataCategory()}
          selectTag={tag => onSelect('tag', tag)}
        />
      ) : null}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  view1: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    height: 240 * scale,
    borderRadius: 4 * scale,
    backgroundColor: '#FAFAFA',
    marginTop: 16 * scale,
  },
  view2: {
    height: 84 * scale,
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    marginTop: 16 * scale,
    backgroundColor: 'blue',
  },
  view3: {
    height: 64 * scale,
    width: 64 * scale,
    position: 'absolute',
    top: 68 * scale,
    left: 32 * scale,
    borderRadius: 4 * scale,
    backgroundColor: 'yellow',
    zIndex: 9999,
  },
  view4: {
    height: 18.33 * scale,
    width: 20 * scale,
    // backgroundColor: 'gray',
    marginTop: 11 * scale,
    marginLeft: 290 * scale,
  },
  view5: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    height: 24 * scale,
    marginTop: 10.67 * scale,
  },
  view5_1: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    height: 20 * scale,
    // backgroundColor: 'green',
    marginTop: 2 * scale,
  },
  viewHeader: {
    flexDirection: 'row',
    marginLeft: 19,
    marginRight: 19,
    height: 48 * scale,
    justifyContent: 'space-between',
    alignItems: "center",
  },
  viewMap: {
    width: '100%',
    height: 256 * scale,
    marginTop: 6 * scale,
    backgroundColor: 'gray',
  },
  txt1: {
    lineHeight: 20.11,
    fontSize: 16,

    fontFamily: 'SourceSansPro-SemiBold',
    color: '#333333',
  },
  txt2: {
    lineHeight: 20.11,
    fontSize: 16,

    fontFamily: 'SourceSansPro-Regular',
    color: '#333333',
    fontStyle: 'normal',
  },
  txt2_1: {
    lineHeight: 20.11,
    fontSize: 16,

    fontFamily: 'SourceSansPro-Regular',
    color: '#4F4F4F',
    fontStyle: 'normal',
  },
  txt3: {
    lineHeight: 15 * scale,
    fontSize: 16 * scale,
    marginRight: 3 * scale,
    fontFamily: 'SourceSansPro-Regular',
    color: '#00A9F4',
  },
  boxFilter: {
    padding: 16,
  },
  boxFilter1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 10,
  },
  boxItemFilter: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 36,
    borderRadius: 8,
    borderColor: '#BDBDBD',
    borderWidth: 1,
    paddingHorizontal: 10,
  },
  boxItemFilter1: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 36,
    // width: 328,
    borderRadius: 8,
    borderColor: '#BDBDBD',
    borderWidth: 1,
    paddingHorizontal: 10,
    width: '100%',
  },
  ic_location: {
    width: 20,
    height: 20,
  },
  ic_dropdown: {
    width: 16,
    height: 16,
  },
  txtFilter: {
    fontSize: 14,
    color: '#333333',
    fontWeight: 'bold',
  },
  boxAction: {
    position: 'absolute',
    bottom: 15,
    right: 15
  },
  btnMyLocation: {
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 10
  },
  icMyLocation: {
    width: 20,
    height: 20
  },
  btnPlus: {
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 10
  },
  icPlus: {
    width: 18,
    height: 18
  },
  btnMinus: {
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 10
  },
  icMinus: {
    width: 18,
    height: 18
  },
  boxResearch: {
    position: 'absolute',
    top: 20 * scale,
    // right: width / 3 - 20 * scale,
    paddingTop: 10 * scale,
    paddingBottom: 10 * scale,
    paddingLeft: 20 * scale,
    paddingRight: 20 * scale,
    borderRadius: 10,
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#03A9F4',
    alignSelf: 'center'
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  txtResearch: {
    color: '#03A9F4',
    fontSize: 17 * scale / scaleFontSize,
  },
  myClusterStyle: {
    // position: 'absolute',
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#03a9f4',
    opacity: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  myClusterTextStyle: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 15
  },
  plainView: {
    padding: 5,
    position: 'relative',
    minWidth: 100,
    maxWidth: 500,
    alignItems: 'center'
    // backgroundColor: 'red',

  },
  boxCountry: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 6
  },
  ic_country: {
    width: 24,
    height: 24,
    marginRight: 24
  },
  txtCountry: {
    color: '#4F4F4F',
    fontSize: 16 * scale / scaleFontSize,
    fontWeight: '600'
  }
});
