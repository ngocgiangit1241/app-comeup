import Geolocation from '@react-native-community/geolocation';
import React, { useEffect, useRef, useState, useCallback } from 'react';
import {
  ActivityIndicator, Animated, Dimensions, Easing, FlatList, Image, PermissionsAndroid, PixelRatio, Platform, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View
} from 'react-native';
import { PanGestureHandler, State } from 'react-native-gesture-handler';
import { Callout, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import ClusteredMapView from 'react-native-maps-super-cluster';
import { check, PERMISSIONS, request, RESULTS } from 'react-native-permissions';
import Popover from 'react-native-popover-view';
import Carousel from 'react-native-snap-carousel';
import { useDispatch, useSelector } from 'react-redux';
import ToggleSwitch from 'toggle-switch-react-native';
// import { actListVenueMap, actLikeVenues } from '../../actions/venue2'
import { actLikeVenues, actListVenueMap } from '../../actions/venue';
import pin_1_icon from '../../assets/pin_1_icon.png';
import pin_1_icon_active from '../../assets/pin_1_icon_active.png';
import pin_icon_transparent from '../../assets/pin_icon_transparent.png';
import pin_menu_icon from '../../assets/pin_menu_icon.png';
import pin_menu_icon_active from '../../assets/pin_menu_icon_active.png';
import ModalCity from '../../components/ModalCity';
import ModalTag from '../../components/ModalTag';
import VenueMapItem from '../../components/venue/VenueMapItem';
import * as Types from '../../constants/ActionType';
import * as Config from '../../constants/Config';
import Touchable from '../../screens_view/Touchable';
import { convertLanguage } from '../../services/Helper';

const screenWidth = Dimensions.get('window').width;
const { width, height } = Dimensions.get('window');
const scale = screenWidth / 360;
const ASPECT_RATIO = width / height;
const scaleFontSize = PixelRatio.getFontScale()

export default function VenueMap({ navigation, route }) {

  const statusRef = useRef({})
  const map = useRef('')
  const _carousel = useRef(null)
  const dispatch = useDispatch();
  const venues = useSelector(state => state.venue)
  const { language } = useSelector(state => state.language)
  const global = useSelector(state => state.global)
  const [timeOpen, setTimeOpen] = useState(false);
  const [filler, setFiller] = useState(route.params.filler)
  const city = route.params.city
  const category = route.params.category

  const [position, setPotision] = useState({
    lat: route.params.lat || 21.027765,
    lng: route.params.long || 105.834160,
    latitudeDelta: 0.2,
    longitudeDelta: 0.2 * ASPECT_RATIO,
  })
  const [checkMapArea, setCheckMapArea] = useState(false)
  const [data, setData] = useState({
    tags: '',
    area: 1,
    page: 1,
    area: { Id: 1 },
    markers: [],
    sliderActiveSlide: 0,
    venues: [],
    showButtonReSearch: true,
    isEdit: false,
    type: 'country'
  })

  const [heightFlatList] = useState(new Animated.Value(0))
  const [toogleFlatList, setToogleFlatList] = useState(false)
  const [dataFlatList, setDataFlatList] = useState({
    scrollEnabled: true,
    checkFlatList: '',
    data: []
  })
  const [isDrag, setIsDag] = useState(true)


  useEffect(() => {
    const willFocusSubscription = navigation.addListener('focus', () => {
      setPotision({
        lat: route.params.lat || 21.027765,
        lng: route.params.long || 105.834160,
        latitudeDelta: 0.2,
        longitudeDelta: 0.2 * ASPECT_RATIO,
      })
    });


    return () => {
      willFocusSubscription
      dispatch({ type: Types.VENUEMAP_CLEAR })
    }
  }, [])
  useEffect(() => {
    setData({
      ...data,
      area: global.city.Id
        ? global.city
        : global.country,

    })
    // setPotision({
    //   ...position,
    //   lat: global.city.Id
    //     ? global.city.Lat
    //     : global.country.Lat,
    //   lng: global.city.Id
    //     ? global.city.Lng
    //     : global.country.Lng,
    // })
  }, [global])

  useEffect(() => {
    dispatch(actListVenueMap(filler?.tag?.value, filler.type === 'city' ? filler.area.Id : filler.area.Code, timeOpen, filler?.status_team?.value)) //data.page,, filler?.status_team?.value
    // onSelect('area', filler?.area)
  }, [timeOpen, filler?.tag?.value, filler?.area, filler?.status_team?.value])


  const onSelect = async (key, value) => {
    await setFiller({ ...filler, [key]: value, modalCity: false, type: key === 'area' ? 'city' : filler.type, modalTag: false, modalType: false })
    await setData({ ...data, page: 1 })
    if (key === 'area') {
      const apiUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${value.Name}&key=${Config.GOOGLE_API_KEY}`;
      try {
        const result = await fetch(apiUrl);
        const json = await result.json();
        if (json.results.length > 0) {
          var region = {
            latitude: parseFloat(json.results[0].geometry.location.lat),
            longitude: json.results[0].geometry.location.lng,
            latitudeDelta: 0.2,
            longitudeDelta: 0.2 * ASPECT_RATIO
          }
          map?.current?.getMapRef()?.animateToRegion(region), 10
        }
      } catch (err) {
      }
    }
  }

  const showDataCategory = () => {
    var result = [
      {
        name: convertLanguage(language, 'all_categories'),
        value: '',
      },
    ];
    category.categories.forEach(category => {
      result.push({
        name: category.HashTagName,
        value: category.Id,
      });
    });
    return result;
  }

  useEffect(() => {
    let dataFetch = [...venues.venueMap]
    setItemList(dataFetch)
    //marker
    if (venues.venueMap.length > 0) {
      var { sliderActiveSlide } = data;
      var markers = getDataMarkers(venues.venueMap)
      let check = false
      if (data.type === 'country') {
        check = true
      }
      if (!check) {

        setData({
          ...data,
          venues: venues.venueMap,
          markers: getDataMarkers(venues.venueMap),
          showButtonReSearch: false
        })
      } else {

        setData({
          ...data,
          venues: venues.venueMap,
          markers: getDataMarkers(venues.venueMap),
          showButtonReSearch: false,

        })
        setPotision({
          ...position,
          lat: markers[sliderActiveSlide].location.latitude,
          lng: markers[sliderActiveSlide].location.longitude,
        })
      }
    }
  }, [venues])



  const [itemList, setItemList] = useState([]);

  const onChangeLike = (index, Id) => {
    dispatch(actLikeVenues(Id)).then(data => {
      if (data.status === 200) {
        // let dataFetch = [...itemList]
        // dataFetch[index].IsLike = !dataFetch[index].IsLike
        // console.log('123', dataFetch);
        // setItemList(dataFetch)
      }
    })

  }

  const _renderFooter = () => {
    if (data.page !== venues.last_pageMap) {
      return (
        <ActivityIndicator
          size="large"
          color="#000000"
          style={{ padding: 20, flex: 1 }}
        />
      );
    } else {
      return (
        <Text style={{ textAlign: 'center' }}>
          {/* {convertLanguage(language, 'event_empty')} */}
        </Text>
      );
    }

  };
  const loadMore = () => {
    if (data.page !== venues.last_pageMap)
      setData({ ...data, page: data.page + 1 })
  }

  const getDataMarkers = useCallback((venue_maps) => {
    var data_markers = [];
    var i = 0;
    venue_maps.forEach((venue) => {
      if (typeof venue.Lat != 'undefined' && typeof venue.Long != 'undefined' && venue.Lat !== "" && venue.Long !== "") {
        let index = data_markers.findIndex(marker => marker.location.latitude == venue.Lat && marker.location.longitude == venue.Long);
        if (index !== -1) {
          data_markers[index].venues.push(venue);
          data_markers[index].keys.push(i)
          data_markers.push({
            location: {
              latitude: parseFloat(venue.Lat),
              longitude: parseFloat(venue.Long)
            },
            venues: [],
            keys: data_markers[index].keys,
            showImage: false,
            index: i
          });
          i++;
        } else {
          data_markers.push({
            location: {
              latitude: parseFloat(venue.Lat),
              longitude: parseFloat(venue.Long)
            },
            venues: [venue],
            keys: [i],
            showImage: true,
            index: i
          });
          i++;
        }
      }
    })
    return data_markers;
  }, [venues])

  const onPanDrag = () => {

    if (!venues.loading) {
      if (!checkMapArea) {
        setCheckMapArea(true)
      }
    }
    if (data.showButtonReSearch) {
      setData({ ...data, showButtonReSearch: false }) //isEdit: false, 
    }

  }
  const onRegionChangeComplete = (center) => {
    setData({
      ...data,
      showButtonReSearch: true,
      isEdit: false
    })

    setPotision({
      ...position,
      lat: center.latitude && center.latitude === 0 ? position.lat : center.latitude,
      lng: center.longitude && center.longitude === 0 ? position.lng : center.longitude,
      latitudeDelta: center.latitudeDelta,
      longitudeDelta: center.longitudeDelta,
    })
  }

  const renderCluster = useCallback((cluster, onPress) => {
    const pointCount = cluster.pointCount,
      coordinate = cluster.coordinate,
      clusterId = cluster.clusterId
    const clusteringEngine = map.current.getClusteringEngine(),
      clusteredPoints = clusteringEngine.getLeaves(clusterId, 100)

    return (
      <Marker coordinate={coordinate} onPress={(onPress)} key={cluster.clusterId} tracksViewChanges={false}>
        <View style={styles.myClusterStyle}>
          <Text style={styles.myClusterTextStyle}>
            {pointCount}
          </Text>
        </View>
      </Marker>
    )
  }, [venues])

  const onOpen = async (data) => {
    if (data.venues !== dataFlatList.data && data.venues.length > 0) {
      await setDataFlatList({ ...dataFlatList, data: data.venues })
    }

    // setTimeout(() => {
    await setToogleFlatList(true)
    await Animated.timing(heightFlatList, {
      toValue: 310 * scale,
      duration: 750,
      easing: Easing.linear,
      // isInteraction: false
    }).start();

    await _carousel?.current?.snapToItem(0)
    // }, 250);
  };
  const renderMarker = useCallback((data) => (
    <Marker coordinate={data.location}
      mapMarker={data}
      key={data.index}
      // key={Math.random()}
      // tracksViewChanges={false}
      // title="This is a native view"
      // description={data.keys.length === 1 ? data.venues[0].Name : data.venues[0].Address}
      title={data.keys.length === 1 ? data.venues[0].Name : data?.venues[0]?.Address}
      // pointerEvents='auto'
      onPress={() => onOpen(data)}
    //   onSelect={() => {
    //     this.props.handlePress();
    // }}

    >
      {
        data.showImage ?
          <View style={{ width: data.venues.length === 1 ? 32 * scale : 40, height: 45 * scale }}>
            <Image source={showImageMarker(data)} style={{ width: 32 * scale, height: 45 * scale }} />
            {
              showNumberEvent(data)
            }
            {/* {

              Platform.OS == "android" ?
                data.keys.length === 1 ?
                  <Callout style={styles.plainView}
                  >
                    <Text style={{ alignSelf: 'flex-start', }}>{data?.venues[0].Name}</Text>
                  </Callout>
                  :
                  <Callout style={styles.plainView} >
                    <Text style={{ alignSelf: 'flex-start', }}>{data?.venues[0].Address}</Text>
                  </Callout>
                : null
            } */}
          </View>

          :
          <View style={{ width: 0, height: 0, backgroundColor: '#000000' }}></View>
      }
    </Marker >
  ), [venues])


  const searchEventByLocation = () => {
    var { lat, lng, latitudeDelta, longitudeDelta } = position;
    // setState({ sliderActiveSlide: 0, modal_events: [], showButtonReSearch: false })
    var north = lat + latitudeDelta / 2;
    var east = lng + longitudeDelta / 2;
    var south = lat - latitudeDelta / 2;
    var west = lng - longitudeDelta / 2;

    // props.onLoadEventByMaps(north, east, south, west, '', tag.value, time_event.value, start, finish, type);
    dispatch(actListVenueMap(filler?.tag?.value, filler.type === 'city' ? filler.area.Id : filler.area.Code, timeOpen, filler?.status_team?.value, north, south, east, west))

    setData({
      ...data,
      area: {
        Name: 'Map Area',
        Id: '',
        Name_vi: 'Khu vực',
        Name_en: 'Map Area',
        Name_ko: '지도 영역'
      },
      sliderActiveSlide: 0,
      showButtonReSearch: false
    })
  }


  const onPressToMyLocation = () => {
    var permission = Platform.OS === 'ios' ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
    check(permission).then(response => {
      if (response === RESULTS.BLOCKED) {
        _alertForLocationPermission()
      } else if (response === RESULTS.GRANTED) {
        Geolocation.getCurrentPosition((position1) => {
          setPotision({
            ...position,
            lat: position1.coords.latitude,
            lng: position1.coords.longitude,
          })
          var region = {
            latitude: position1.coords.latitude,
            longitude: position1.coords.longitude,
            latitudeDelta: position.latitudeDelta,
            longitudeDelta: position.longitudeDelta
          }
          setTimeout(() => {
            if (map?.current !== '' || map?.current?.getMapRef() !== null && map?.current !== null)
              map?.current?.getMapRef()?.animateToRegion(region), 10
          });
        })
      } else if (response === RESULTS.DENIED) {
        request(permission).then(response2 => {
          if (response2 === RESULTS.GRANTED) {
            Geolocation.getCurrentPosition((position1) => {
              setPotision({
                ...position,
                lat: position1.coords.latitude,
                lng: position1.coords.longitude,
              })
              var region = {
                latitude: position1.coords.latitude,
                longitude: position1.coords.longitude,
                latitudeDelta: position.latitudeDelta,
                longitudeDelta: position.longitudeDelta
              }
              setTimeout(() => {
                if (map?.current !== '' || map?.current?.getMapRef() !== null && map?.current !== null)
                  map?.current?.getMapRef()?.animateToRegion(region), 10
              });
            })
          }
        })
      } else {
        _alertForLocationPermission()
      }
    });
  }
  const onPressZoomIn = () => {
    var region = {
      latitude: position.lat,
      longitude: position.lng,
      latitudeDelta: position.latitudeDelta / 2,
      longitudeDelta: position.longitudeDelta / 2
    }
    setPotision({
      ...position,
      latitudeDelta: position.latitudeDelta / 2,
      longitudeDelta: position.longitudeDelta / 2
    })
    setTimeout(() => {
      if (map?.current !== '' && map?.current?.getMapRef() !== null && map?.current !== null)
        map?.current?.getMapRef()?.animateToRegion(region), 10
    });

  }
  const onPressZoomOut = () => {
    var region = {
      latitude: position.lat,
      longitude: position.lng,
      latitudeDelta: position.latitudeDelta * 2,
      longitudeDelta: position.longitudeDelta * 2
    }
    setPotision({
      ...position,
      latitudeDelta: position.latitudeDelta * 2,
      longitudeDelta: position.longitudeDelta * 2
    })

    setTimeout(() => {
      if (map?.current !== '' || map?.current?.getMapRef() !== null && map?.current !== null)
        map?.current?.getMapRef()?.animateToRegion(region), 10
    });
  }

  const _alertForLocationPermission = () => {
    setTimeout(() => {
      Alert.alert(
        convertLanguage(language, 'access_location_title'),
        convertLanguage(language, 'access_location_content'),
        [
          {
            text: convertLanguage(language, 'cancel'),
            style: 'destructive',
          },
          { text: convertLanguage(language, 'open_setting'), onPress: () => openSettings() },
        ],
      );
    }, 100)
  }
  const openSettings = () => {
    Platform.OS === 'ios' ? openSettings() : AndroidOpenSettings.appDetailsSettings();
    // props.navigation.goBack();
    navigation.goBack()
  }
  const showImageMarker = (data) => {
    var { sliderActiveSlide } = data;
    if (data.showImage) {
      var index = data.keys.indexOf(sliderActiveSlide);
      if (index !== -1) {
        switch (data.venues.length) {
          case 1:
            return pin_1_icon_active;
          default:
            return pin_menu_icon_active;
        }

      } else {
        switch (data.venues.length) {
          case 1:
            return pin_1_icon;
          default:
            return pin_menu_icon;
        }
      }
    } else {
      return pin_icon_transparent;
    }
  }

  const showNumberEvent = useCallback((data) => {
    if (data.showImage) {
      switch (data.venues.length) {
        case 1:
          return null;
        default:
          return <View style={{ width: 16 * scale, height: 16 * scale, borderRadius: 8, backgroundColor: '#EB5757', position: 'absolute', top: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ color: '#FFFFFF', fontSize: 11 * scale }}>{data.venues.length}</Text>
          </View>
      }
    } else {
      return null;
    }
  }, [venues])
  const _onMapReady = () => {
    if (Platform.OS === 'android') {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        .then(granted => {
          // this.setState({ paddingTop: 0 });
        });
    }
  }


  const onHandlerStateChange = async ({ nativeEvent }) => {
    if (nativeEvent.state === State.END) {
      if (nativeEvent.translationY <= 0) {

        Animated.spring(heightFlatList, {
          toValue: 0,
          velocity: nativeEvent.velocityY,
          tension: 2,
          friction: 8,
          useNativeDriver: false
        }).start(); //step 2
        // setTimeout(() => {
        setToogleFlatList(false)
        // }, 200);
      } else {

        Animated.timing(heightFlatList, {
          toValue: 310 * scale,
          duration: 250,
          easing: Easing.linear,
          // isInteraction: false
        }).start();

      }
      setIsDag(true)
    }
  }
  const onPanGestureEvent = ({ nativeEvent }) => {
    if (isDrag) {
      setIsDag(false)
    }
    Animated.spring(heightFlatList, {
      toValue: 310 * scale + nativeEvent.translationY,
      velocity: nativeEvent.velocityY,
      tension: 2,
      friction: 8,
      useNativeDriver: false
    }).start()
  };

  if (position == null) {
    return (
      <ActivityIndicator
        size="large"
        color="#000000"
        style={{ position: 'absolute', zIndex: 999, height: '100%', width: '100%', backgroundColor: 'rgba(105, 105, 105, 0.3)' }}
      />
    )
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        {
          venues.loading &&
          <ActivityIndicator
            size="large"
            color="#000000"
            style={{ position: 'absolute', zIndex: 999, height: '100%', width: '100%', backgroundColor: 'rgba(105, 105, 105, 0.3)' }}
          />
        }

        <View style={styles.viewHeader}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}>
            <Image
              style={{ height: 20 * scale, width: 20 * scale }}
              source={require('../../assets/arrow_back.png')}
            />
          </TouchableOpacity>
          <Touchable
            style={{
              flexDirection: 'row',
              // width: 122 * scale,
              height: '100%',
              alignItems: 'center',
              justifyContent: 'flex-end',

            }}>
            <Text style={styles.txt3}>{convertLanguage(language, 'open_now')}</Text>
            <ToggleSwitch
              isOn={timeOpen}
              onColor="#00A9F4"
              offColor="gray"
              size="normal"
              onToggle={isOn => {
                setTimeOpen(isOn);
              }}
            />
          </Touchable>
        </View>

        <ScrollView scrollEnabled={isDrag} nestedScrollEnabled={true}>
          <View style={styles.boxFilter}>
            <View style={styles.boxFilter1}>
              <Touchable
                style={[styles.boxItemFilter, { flex: 0.47 }]}
                // onPress={toggleModal}
                onPress={() => setFiller({ ...filler, modalCity: true })}
              >
                <Image
                  source={require('../../assets/ic_location_blue.png')}
                  style={styles.ic_location}
                />

                <Text style={styles.txtFilter} numberOfLines={1}>
                  {checkMapArea ? convertLanguage(language, 'map_area') : filler.area['Name_' + language]}
                </Text>
                <Image
                  source={require('../../assets/select_arrow_black.png')}
                  style={styles.ic_dropdown}
                />
              </Touchable>
              <TouchableOpacity
                ref={statusRef}
                style={[styles.boxItemFilter, { flex: 0.47 }]}
                onPress={() => setFiller({ ...filler, modalType: true })}
              >
                <Image
                  source={filler.status_team.icon}
                  style={styles.ic_location}
                />
                <Text style={styles.txtFilter} numberOfLines={1}>
                  {filler.status_team.name}
                </Text>
                <Image
                  source={require('../../assets/select_arrow_black.png')}
                  style={styles.ic_dropdown}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={styles.boxItemFilter1}

              onPress={() => setFiller({ ...filler, modalTag: true })}
            >
              <Image
                source={require('../../assets/ic_category_blue.png')}
                style={styles.ic_location}
              />
              <Text style={styles.txtFilter} numberOfLines={1}>
                {filler?.tag?.name}
              </Text>
              <Image
                source={require('../../assets/select_arrow_black.png')}
                style={styles.ic_dropdown}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.viewMap}>
            <ClusteredMapView
              provider={PROVIDER_GOOGLE}
              style={{ flex: 1 }}
              data={data.markers}
              initialRegion={{
                latitude: position?.lat,
                longitude: position.lng,
                latitudeDelta: position.latitudeDelta,
                longitudeDelta: position.longitudeDelta,
              }}
              zoomEnabled={true}
              enableZoomControl={true}
              rotateEnabled={false}
              height={256 * scale}
              // showsUserLocation={true}
              showsMyLocationButton={false}
              onRegionChange={onPanDrag}
              // onRegionChangeComplete={region => setRegion(region)}
              onRegionChangeComplete={onRegionChangeComplete}
              // onPress={(e) => console.log(e.nativeEvent.coordinate)}
              // onLongPress={(e) => setState({panMap: true})}
              ref={map}
              // onMapReady={_onMapReady}
              renderMarker={renderMarker}
              renderCluster={renderCluster}
              maxZoom={14}
            // scrollEnabled={panMap}
            />
            <View style={styles.boxAction}>
              {
                <Touchable style={styles.btnMyLocation} onPress={onPressToMyLocation}>
                  <Image source={require('../../assets/my_location.png')} style={styles.icMyLocation} />
                </Touchable>
              }
              <Touchable style={styles.btnPlus} onPress={() => onPressZoomIn()}>
                <Image source={require('../../assets/plus.png')} style={styles.icPlus} />
              </Touchable>
              <Touchable style={styles.btnMinus} onPress={onPressZoomOut}>
                <Image source={require('../../assets/minus.png')} style={styles.icMinus} />
              </Touchable>
            </View>
            {
              data.showButtonReSearch &&
              <Touchable style={styles.boxResearch} onPress={searchEventByLocation}>
                <Text style={styles.txtResearch}>{convertLanguage(language, 'research')}</Text>
              </Touchable>
            }
          </View>
          <View>
            {
              // modal_events.length > 0 &&
              toogleFlatList &&
              <Animated.View
                // pointerEvents='auto'
                style={[{
                  height: heightFlatList,
                  // transform: [{ translateY: heightFlatList }]
                }, {
                  marginBottom: 20 * scale

                }]}>
                <Carousel
                  ref={_carousel}
                  data={dataFlatList.data}
                  renderItem={({ item, index }) => {
                    return (
                      <View style={{ marginHorizontal: -5 * scale }} key={'cc' + index}>
                        <TouchableOpacity onPress={() => navigation.navigate('VenueDetail', { Id: item?.Id })}>
                          <VenueMapItem
                            item={item}
                            navigation={navigation}
                            index={index}
                            onChangeLike={(index, id) => onChangeLike(index, id)}
                          />
                        </TouchableOpacity>
                      </View>
                    )
                  }}
                  // layout={'stack'}
                  // layoutCardOffset={`18`}
                  removeClippedSubviews={true}
                  sliderWidth={width}
                  itemWidth={width - 45 * scale}
                  // inactiveSlideScale={1}
                  // firstItem={data.sliderActiveSlide}
                  slideStyle={{ paddingRight: 12 * scale, }}
                // onSnapToItem={(index) => this.moveMarker(index)}
                />
                <PanGestureHandler onGestureEvent={onPanGestureEvent}
                  onHandlerStateChange={onHandlerStateChange}
                >
                  <View
                    style={[{
                      height: 20 * scale,
                      // paddingTop: 20,
                      // marginTop: 20 * scale,
                      // flex: 3 / 5,
                      alignItems: 'center',
                      backgroundColor: '#FFFFFF',
                      // backgroundColor: 'red',
                      justifyContent: 'center',
                      shadowColor: "#000",
                      shadowOffset: {
                        width: 0,
                        height: 2 * scale,
                      },
                      shadowOpacity: 0.18,
                      shadowRadius: 1.00,

                      elevation: 3,
                    }]}
                  >
                    <View style={{ backgroundColor: '#C4C4C4', borderRadius: 4, height: 4, width: 86, flex: 1 / 5 }} />
                  </View>
                </PanGestureHandler>
              </Animated.View>
            }
          </View>
          <FlatList
            scrollEnabled={isDrag}
            style={{ paddingBottom: '5%' }}
            keyExtractor={item => item.Id.toString() || Math.random().toString()}
            data={itemList}
            ListFooterComponent={() => _renderFooter()}
            ListEmptyComponent={<Text style={{ textAlign: 'center', paddingTop: 20 }}>{convertLanguage(language, 'venue_empty')}</Text>}
            // onEndReached={() => loadMore()}
            onEndReachedThreshold={0.5}
            renderItem={({ item, index }) => {
              return (
                <View style={{ marginHorizontal: 16 * scale }}>
                  <TouchableOpacity onPress={() => navigation.navigate('VenueDetail', { Id: item?.Id })}>
                    <VenueMapItem
                      item={item}
                      navigation={navigation}
                      index={index}
                      onChangeLike={(index, id) => onChangeLike(index, id)}
                    />
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </ScrollView>

      </View>


      <Popover
        isVisible={filler.modalType}
        animationConfig={{ duration: 0 }}
        fromView={statusRef.current}
        placement='bottom'
        popoverStyle={{
          borderRadius: 8,
          width: 156 * scale,
          padding: 12 * scale
        }}
        arrowStyle={{ backgroundColor: 'transparent', height: 0, width: 0, }}
        backgroundStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.05)', }}
        onRequestClose={() => setFiller({ ...filler, modalType: false })}>
        <View style={styles.boxPopover}>
          <TouchableOpacity onPress={() => onSelect('status_team', { name: convertLanguage(language, 'popular'), value: 'popular', icon: require('../../assets/ic_popular.png') })} style={styles.boxCountry}>
            <Image source={require('../../assets/ic_popular.png')} style={styles.ic_country} />
            <Text style={styles.txtCountry}>{convertLanguage(language, 'popular')}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => onSelect('status_team', { name: convertLanguage(language, 'newTeam'), value: 'new', icon: require('../../assets/ic_new.png') })} style={styles.boxCountry}>
            <Image source={require('../../assets/ic_new.png')} style={[styles.ic_country]} />
            <Text style={styles.txtCountry}>{convertLanguage(language, 'newTeam')}</Text>
          </TouchableOpacity>
          {/* <TouchableOpacity onPress={() => onSelect('status_team', { name: convertLanguage(language, 'famous'), value: 'famous', icon: require('../../assets/ic_famous.png') })} style={styles.boxCountry}>
            <Image source={require('../../assets/ic_famous.png')} style={styles.ic_country} />
            <Text style={styles.txtCountry}>{convertLanguage(language, 'famous')}</Text>
          </TouchableOpacity> */}
        </View>
      </Popover>

      {/*  */}
      {filler.modalCity && city.venue_countries.length > 0 ? (
        <ModalCity
          modalVisible={filler.modalCity}
          closeModal={() => setFiller({ ...filler, modalCity: false })}
          countries={city.venue_countries}
          selectCity={city => { onSelect('area', city), setCheckMapArea(false) }}
        />
      ) : null}

      {filler.modalTag ? (
        <ModalTag
          modalVisible={filler.modalTag}
          closeModal={() => setFiller({ ...filler, modalTag: false })}
          tags={showDataCategory()}
          selectTag={tag => onSelect('tag', tag)}
        />
      ) : null}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  view1: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    height: 240 * scale,
    borderRadius: 4 * scale,
    backgroundColor: '#FAFAFA',
    marginTop: 16 * scale,
  },
  view2: {
    height: 84 * scale,
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    marginTop: 16 * scale,
    backgroundColor: 'blue',
  },
  view3: {
    height: 64 * scale,
    width: 64 * scale,
    position: 'absolute',
    top: 68 * scale,
    left: 32 * scale,
    borderRadius: 4 * scale,
    backgroundColor: 'yellow',
    zIndex: 9999,
  },
  view4: {
    height: 18.33 * scale,
    width: 20 * scale,
    // backgroundColor: 'gray',
    marginTop: 11 * scale,
    marginLeft: 290 * scale,
  },
  view5: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    height: 24 * scale,
    marginTop: 10.67 * scale,
  },
  view5_1: {
    marginLeft: 16 * scale,
    marginRight: 16 * scale,
    height: 20 * scale,
    // backgroundColor: 'green',
    marginTop: 2 * scale,
  },
  viewHeader: {
    flexDirection: 'row',
    marginLeft: 19,
    marginRight: 19,
    height: 48 * scale,
    justifyContent: 'space-between',
    alignItems: "center",
  },
  viewMap: {
    width: '100%',
    height: 350 * scale,
    marginTop: 6 * scale,
    backgroundColor: 'gray',
  },
  txt1: {
    lineHeight: 20.11,
    fontSize: 16,

    fontFamily: 'SourceSansPro-SemiBold',
    color: '#333333',
  },
  txt2: {
    lineHeight: 20.11,
    fontSize: 16,

    fontFamily: 'SourceSansPro-Regular',
    color: '#333333',
    fontStyle: 'normal',
  },
  txt2_1: {
    lineHeight: 20.11,
    fontSize: 16,

    fontFamily: 'SourceSansPro-Regular',
    color: '#4F4F4F',
    fontStyle: 'normal',
  },
  txt3: {
    lineHeight: 21 * scale,
    fontSize: 16 * scale,
    marginRight: 3 * scale,
    fontFamily: 'SourceSansPro-Regular',
    color: '#00A9F4',
  },
  boxFilter: {
    padding: 16,
  },
  boxFilter1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 10,
  },
  boxItemFilter: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 36,
    borderRadius: 8,
    borderColor: '#BDBDBD',
    borderWidth: 1,
    paddingHorizontal: 10,
  },
  boxItemFilter1: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 36,
    // width: 328,
    borderRadius: 8,
    borderColor: '#BDBDBD',
    borderWidth: 1,
    paddingHorizontal: 10,
    width: '100%',
  },
  ic_location: {
    width: 20,
    height: 20,
  },
  ic_dropdown: {
    width: 16,
    height: 16,
  },
  txtFilter: {
    fontSize: 14,
    color: '#333333',
    fontWeight: 'bold',
  },
  boxAction: {
    position: 'absolute',
    bottom: 15,
    right: 15
  },
  btnMyLocation: {
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 10
  },
  icMyLocation: {
    width: 20,
    height: 20
  },
  btnPlus: {
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 10
  },
  icPlus: {
    width: 18,
    height: 18
  },
  btnMinus: {
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 10
  },
  icMinus: {
    width: 18,
    height: 18
  },
  boxResearch: {
    position: 'absolute',
    top: 20 * scale,
    // right: width / 3 - 20 * scale,
    paddingTop: 10 * scale,
    paddingBottom: 10 * scale,
    paddingLeft: 20 * scale,
    paddingRight: 20 * scale,
    borderRadius: 10,
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#03A9F4',
    alignSelf: 'center'
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  txtResearch: {
    color: '#03A9F4',
    fontSize: 17 * scale / scaleFontSize,
  },
  myClusterStyle: {
    // position: 'absolute',
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#03a9f4',
    opacity: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  myClusterTextStyle: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 15
  },
  plainView: {
    // padding: 5,
    // position: 'relative',
    // minWidth: 100,
    // maxWidth: 500,
    // alignItems: 'center'
    // backgroundColor: 'blue',
    // position: 'absolute',
    // top: -10 * scale
    // width: 140,
    // width: 60,
    // padding: 5,
    // position: 'relative',
    // minWidth: 100,
    // maxWidth: 500,
    // alignItems: 'center'
  },
  boxCountry: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 6
  },
  ic_country: {
    width: 24,
    height: 24,
    marginRight: 24
  },
  txtCountry: {
    color: '#4F4F4F',
    fontSize: 16 * scale / scaleFontSize,
    fontWeight: '600'
  },
  boxSlide: {
    flex: 1,
    // flexDirection: 'row'
    marginBottom: 8
  },
  boxThumb: {
    width: 312,
    height: 170,
    borderRadius: 5,
    backgroundColor: '#bdbdbd',
    marginRight: 10,
    marginBottom: 10
  },
  boxContent: {
    flex: 1
  },
  txtName: {
    fontSize: 17,
    color: '#333333',
    fontWeight: 'bold',
    marginBottom: 10
  },
  txtTime: {
    fontSize: 16,
    color: '#4F4F4F'
  },
  txtVenue: {
    fontSize: 14,
    color: '#4F4F4F'
  },
  container: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
  },
  // Callout bubble
  bubble: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    backgroundColor: '#fff',
    borderRadius: 6,
    borderColor: '#ccc',
    borderWidth: 0.5,
    padding: 15,
    width: 150,
  },
  // Arrow below the bubble
  arrow: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#fff',
    borderWidth: 16,
    alignSelf: 'center',
    marginTop: -32,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#007a87',
    borderWidth: 16,
    alignSelf: 'center',
    marginTop: -0.5,
  },
  // Character name
  name: {
    fontSize: 16,
    marginBottom: 5,
  },
  // Character image

});
