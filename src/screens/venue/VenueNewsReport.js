import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, TouchableOpacity, ActivityIndicator, Platform, Keyboard, ScrollView } from 'react-native';

import Touchable from '../../screens_view/Touchable'
import SafeView from '../../screens_view/SafeView'

import * as Colors from '../../constants/Colors'

import { actReportVenueNews } from '../../actions/venue';
import { connect } from "react-redux";
import { convertLanguage } from '../../services/Helper'
class VenueNewsReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      content: '',
    };
  }

  submitReport() {
    Keyboard.dismiss();
    if (this.state.content.replace(/\s/g, '').length > 0 && !this.props.venue.loadingReport) {
      this.props.reportVenue(this.props.route.params['VenueNewsId'], this.state.content, this.props.navigation);
    }
  }

  render() {
    var { language } = this.props.language;

    return (
      <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
        <View style={{ margin: 20, marginBottom: 0 }}>
          <Touchable
            onPress={() =>
              this.props.navigation.goBack()
            }
            style={{
              minWidth: 40, minHeight: 40, alignSelf: 'flex-start',
              justifyContent: 'center',
            }}>
            <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
          </Touchable>
          <Touchable
            onPress={() => { this.submitReport() }}
            style={{
              paddingTop: 5,
              paddingBottom: 5,
              paddingLeft: 10,
              paddingRight: 10,
              borderRadius: 4,
              justifyContent: 'center', alignItems: 'center',
              position: 'absolute', right: 0, top: 0, borderWidth: 1,
              borderColor: this.state.content.replace(/\s/g, '').length == 0 ? Colors.DISABLE : Colors.PRIMARY,
              backgroundColor: this.state.content.replace(/\s/g, '').length == 0 ? '#FFFFFF' : Colors.PRIMARY,
              flexDirection: 'row'
            }}>
            <Text style={[styles.txtBtnReport, { color: this.state.content.replace(/\s/g, '').length == 0 ? Colors.DISABLE : '#FFFFFF' }]}>{convertLanguage(language, 'report')}</Text>
            {
              this.props.venue.loadingReport ?
                <ActivityIndicator size="small" color={'#FFFFFF'} />
                : null
            }
          </Touchable>
        </View>
        <View style={styles.rowTitle}>
          <Text style={styles.txtTitle}>{convertLanguage(language, 'write_your_report')}</Text>
        </View>
        <ScrollView style={styles.rowContent}>

          <TextInput
            style={styles.ipContent}
            multiline={true}
            placeholder={'Write your message'}
            value={this.state.content}
            onChangeText={(content) => this.setState({ content })}
          // onEndEditing={(e) => this.setState({content: e.nativeEvent.text})}
          />
        </ScrollView>
      </SafeView>
    );
  }
}
const styles = StyleSheet.create({
  rowTitle: {
    alignItems: "center",
    marginBottom: 20,
    marginTop: 20
  },
  txtTitle: {
    fontSize: 18,
    color: '#333333',
    fontWeight: 'bold'
  },
  rowContent: {
    flex: 0.7,
    margin: 20,
    marginTop: 0
  },
  ipContent: {
    flex: 1,
    padding: 10,
    fontSize: 18,
    textAlignVertical: 'top',
    borderColor: '#bdbdbd',
    borderWidth: 1,
    height: 300
  },
  rowAction: {
    flex: 0.3,
    alignItems: "center",
    marginTop: 10
  },
  btnReport: {
    flexDirection: 'row',
    borderWidth: 1,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 40,
    paddingRight: 40,
    borderRadius: 4,
    borderColor: Colors.DISABLE
  },
  txtBtnReport: {
    fontSize: 18,
    color: Colors.DISABLE,
    paddingRight: 3
  },
  btnReportActive: {
    borderColor: Colors.PRIMARY
  },
  txtBtnReportActive: {
    color: Colors.PRIMARY
  },
});
const mapStateToProps = state => {
  return {
    venue: state.venue,
    language: state.language
  };
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    reportVenue: (id, content, navigation) => {
      dispatch(actReportVenueNews(id, content, navigation))
    },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(VenueNewsReport);
