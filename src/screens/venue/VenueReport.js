import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, Alert, TouchableOpacity, ActivityIndicator, Platform, KeyboardAvoidingView, ScrollView } from 'react-native';

import Touchable from '../../screens_view/Touchable'
import SafeView from '../../screens_view/SafeView'

import * as Colors from '../../constants/Colors'

import { actReportVenue } from '../../actions/venue';
import { connect } from "react-redux";
import Toast from 'react-native-simple-toast';
import { convertLanguage } from '../../services/Helper';
import Loading from '../../screens_view/Loading';
import Line from '../../screens_view/Line';
class VenueReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            content: '',
        };
    }


    submitData() {
        var { language } = this.props.language;
        if (this.state.content.replace(/\s/g, '').length === 0) {
            Toast.showWithGravity(convertLanguage(language, 'validate_content'), Toast.SHORT, Toast.TOP)
            return;
        }
        if (!this.props.venue?.loading) {
            this.props.reportVenue(this.props.route?.params['Id'], { Content: this.state.content }, this.props.navigation);
        }

    }

    submitReport() {
        var { language } = this.props.language;
        Alert.alert(
            '',
            convertLanguage(language, 'are_you_sure'),
            [
                {
                    text: convertLanguage(language, 'cancel'),
                    onPress: () => console.log("Cancel Pressed"),
                    style: "destructive"
                },
                {
                    text: convertLanguage(language, 'ok'), onPress: () => this.submitData(),
                    // style: "cancel"
                }
            ],
            { cancelable: false }
        );
    }

    render() {
        var { language } = this.props.language;
        return (
            <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
                <View style={{ margin: 20, marginBottom: 0 }}>
                    <Touchable
                        onPress={() =>
                            this.props.navigation.goBack()
                        }
                        style={{
                            minWidth: 40, minHeight: 40, alignSelf: 'flex-start',
                            justifyContent: 'center',
                        }}>
                        <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
                    </Touchable>
                    <Touchable
                        onPress={() => { this.submitReport() }}
                        style={[{
                            paddingTop: 5,
                            paddingBottom: 5,
                            paddingLeft: 20,
                            paddingRight: 20,
                            borderRadius: 4,
                            justifyContent: 'center', alignItems: 'center',
                            position: 'absolute', right: 0, top: 0, borderWidth: 1,
                            borderColor: Colors.DISABLE,
                        }, this.state.content.replace(/\s/g, '').length == 0 || this.props.venue?.loading ? {} : styles.btnReportActive]}>
                        <Text style={[styles.txtBtnReport, this.state.content.replace(/\s/g, '').length == 0 || this.props.venue?.loading ? {} : styles.txtBtnReportActive]}>{convertLanguage(language, 'report2')}</Text>
                    </Touchable>
                </View>
                <Line />
                <View style={{ flex: 1 }}>
                    {
                        this.props.venue?.loading &&
                        <Loading />
                    }
                    <View style={styles.rowTitle}>
                        {/* <Text style={styles.txtTitle}>{convertLanguage(language, 'let_comeup_know')}</Text> */}
                        {
                            language === 'ko' ?
                                <>
                                    <Text style={styles.txtTitle}>{this.props.route.params['Title']}</Text>
                                    <Text style={styles.txtTitle}>{convertLanguage(language, 'what_you_want_to_report_about_venue')}</Text>
                                </>
                                :
                                <>
                                    <Text style={styles.txtTitle}>{convertLanguage(language, 'what_you_want_to_report_about_venue')}</Text>
                                    <Text style={styles.txtTitle}>{this.props.route.params['Title']}</Text>
                                </>
                        }
                    </View>
                    <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.OS === 'android' ? -500 : 0} style={{ flex: 1 }} >
                        <ScrollView style={styles.rowContent}>

                            <TextInput
                                style={styles.ipContent}
                                multiline={true}
                                placeholder={convertLanguage(language, 'write_your_messages')}
                                value={this.state.content}
                                onChangeText={(content) => this.setState({ content })}
                            // onEndEditing={(e) => this.setState({content: e.nativeEvent.text})}
                            />
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </SafeView>
        );
    }
}
const styles = StyleSheet.create({
    rowTitle: {
        alignItems: "center",
        paddingHorizontal: 20
    },
    txtTitle: {
        fontSize: 18,
        color: '#333333',
        fontWeight: 'bold',
        textAlign: 'center'
    },
    rowContent: {
        flex: 0.7,
        margin: 20,
    },
    ipContent: {
        height: 400,
        paddingVertical: 10,
        paddingHorizontal: 10,
        fontSize: 18,
        textAlignVertical: 'top',
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#bcbcbc'
    },
    rowAction: {
        flex: 0.3,
        alignItems: "center",
        marginTop: 10
    },
    btnReport: {
        flexDirection: 'row',
        borderWidth: 1,
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 40,
        paddingRight: 40,
        borderRadius: 4,
        borderColor: Colors.DISABLE
    },
    txtBtnReport: {
        fontSize: 18,
        color: Colors.DISABLE,
        paddingRight: 3
    },
    btnReportActive: {
        borderColor: Colors.PRIMARY
    },
    txtBtnReportActive: {
        color: Colors.PRIMARY
    }
});
const mapStateToProps = state => {
    return {
        venue: state.venue,
        language: state.language
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        reportVenue: (id, content, navigation) => {
            dispatch(actReportVenue(id, content)).then(data => {
                if (data.status === 200) {
                    navigation.goBack()
                }
            })
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(VenueReport);
