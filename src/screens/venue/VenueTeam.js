import React, { useState, useRef } from 'react';
import {
    View,
    Text,
    Dimensions,
    Image,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ScrollView,
    FlatList,
    SafeAreaView
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import VenueTeamItem from '../../components/venue/VenueTeamItem';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const scale = screenWidth / 360;
export default function VenueTeam({ navigation }) {
    const carousel = useRef();
    let slides = [];
    const entriesSplitter = () => {
        const dataArr = [`https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/220px-Image_created_with_a_mobile_phone.png`, `https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/220px-Image_created_with_a_mobile_phone.png`, `https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/220px-Image_created_with_a_mobile_phone.png`, `https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/220px-Image_created_with_a_mobile_phone.png`, `https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/220px-Image_created_with_a_mobile_phone.png`, `https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/220px-Image_created_with_a_mobile_phone.png`, `https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/220px-Image_created_with_a_mobile_phone.png`, `https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/220px-Image_created_with_a_mobile_phone.png`]
        let size = 3; //Based on the size you want
        while (dataArr.length > 0) {
            let slide2 = dataArr.splice(0, size)
            slides.push(slide2);
        }
    };
    const [instagramItem, setInstagramItem] = useState(require('../../assets/venueimg.png'))

    const itemWidth = useState([
        {
            item: require('../../assets/venueimg.png')
        }
    ])

    const [teamNewItem, setTeamNewItem] = useState(
        {
            avt: require('../../assets/venueimg.png'),
            team: 'Starindex\'s Team ',
            time: '20.10.2019 17:15',
            des: 'Special Solution for Entertainment Space & Party. DFC believes in Happiness and Satisfaction of Customers.Giải pháp cho Không gian và Sự kiện Giái trí hàng đầu. DFC tin tưởng Việt Nam. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old',
            image: require('../../assets/newItemTeam.png')
        }
    )

    const [text1, setText1] = useState('1900 LE THÉÂTRE NHÀ HÁT KỊCH TÂN TRÀO HÀ NỘI VIỆT NAM ĐÔNG LÀO')
    const checkLength = () => {
        if (text1.length >= 35) {
            // string(string.substring(35));
            text1 === text1.substring(35);
            return text1.substring(0, 34) + '...';
        } else {
            return text1;
        }
    };

    const [text4, setText4] = useState('Sân vận động Starindex, Số 12 ngõ 9 Liễu Giai Ba Đình Hà Nội')
    const checkLength1 = () => {
        if (text4.length >= 35) {
            // string(string.substring(35));
            text4 === text4.substring(35);
            return text4.substring(0, 34) + '...';
        } else {
            return text4;
        }
    };

    // const [like, setLike] = useState(false)
    // const [count, setCount] = useState(0)
    // const onChangeLike = (index) => {
    //     if (index === count) {
    //         setLike(true)
    //         setCount(0)
    //     } else {
    //         setLike(false)
    //         setCount(index)
    //     }
    // }
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>

            <View style={{ flex: 1, backgroundColor: 'white', paddingBottom: '1.2%' }}>

                <View style={styles.view1}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ flexDirection: "row", alignItems: "center" }}>
                        <Image style={{ width: 13 * scale, height: 8.34 * scale, marginLeft: 12.38 * scale }}
                            source={require('../../assets/back_icon.png')} />
                        <Text style={styles.txt1}>Back</Text>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <Image style={{ width: 16.27 * scale, height: 18 * scale, marginLeft: 225.5 * scale }}
                            source={require('../../assets/share.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image style={{ width: 24 * scale, height: 24 * scale, marginLeft: 14.23 * scale }}
                            source={require('../../assets/Info.png')} />
                    </TouchableOpacity>
                </View>
                <ScrollView >

                    <View style={styles.view2}>

                    </View>

                    <Text style={styles.txt2}>
                        Viet Nam Grand Prix LLC
            </Text>
                    <Text style={styles.txt3}>
                        ID: vietnamgrandprixllc
            </Text>
                    <Text style={styles.txt4}>
                        #Mixology # Firm
            </Text>

                    <View style={styles.view3}>
                        <Text style={styles.txtFollow}>
                            256 Followers
                </Text>
                        <TouchableOpacity style={styles.view4}>
                            <Text style={styles.txt5}>
                                Follow
                </Text>
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.txt6}>About Team</Text>
                    <View style={styles.view5}>
                        <Text style={styles.txt7}>Special Solution for Entertainment Space & Party</Text>
                        <Text style={styles.txt8}>
                            Giải pháp hàng đầu cho Không gian và Sự kiện
                            Công ty DFC là một công ty giải trí chuyên thiết kế địa điểm, sáng tạo tổ chức sự kiện party và festival, tiếp thị và quản lý dịch vụ lounge, bar.
                    </Text>
                        <View style={{ flexDirection: "row", marginTop: 16 }}>
                            <Text style={styles.txt7}>Country:</Text>
                            <Text style={styles.txt8}> Viet Nam</Text>
                        </View>
                        <View style={{ flexDirection: "row", marginTop: 16 }}>
                            <Text style={styles.txt7}>Phone:</Text>
                            <Text style={styles.txt8}>(+84) 024 62952986</Text>
                        </View>
                        <View style={{ flexDirection: "row", marginTop: 16 }}>
                            <Text style={styles.txt7}>Email:</Text>
                            <Text style={styles.txt8}>contact@dfcparty.com</Text>
                        </View>
                    </View>

                    <View style={styles.view6}>
                        <Text style={styles.txt9}>Hosting Event</Text>
                        <TouchableOpacity>
                            <Text style={styles.txt10}>See more</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ marginTop: 16, marginLeft: 16 }}>
                        <Carousel
                            data={[instagramItem, instagramItem, instagramItem]}
                            renderItem={({ item }) => {
                                // console.log(item);
                                return (
                                    <View style={{ marginLeft: 16 }}>
                                        <Image style={{ backgroundColor: '#bcbcbc', width: 300 * scale, height: 168 * scale, borderRadius: 4 }} source={item} />
                                        <Text style={styles.txt1Host}>Starindex's Chrismast Party</Text>
                                        <Text style={styles.txt2_1Host}>Thu 12 Deck 13:00 ~ 14 Dec 23:59 </Text>
                                        <Text style={styles.txt2_1Host}>No. 12 Alley 9 - Liễu Giai - Ba Đình - Hà Nội</Text>
                                        <Text style={styles.txt2_2}>#Festival</Text>
                                    </View>
                                )
                            }}
                            sliderWidth={screenWidth}
                            itemWidth={screenWidth}
                            inactiveSlideScale={1}
                            firstItem={0}
                        />

                    </View>

                    <View style={styles.view6}>
                        <Text style={styles.txt9}>Hosting Venue</Text>
                        <TouchableOpacity>
                            <Text style={styles.txt10}>See more</Text>
                        </TouchableOpacity>
                    </View>

                    <View>
                        <Carousel
                            data={[instagramItem, instagramItem, instagramItem]}
                            renderItem={({ item }) => {
                                // console.log(item);
                                return (
                                    <TouchableOpacity
                                        onPress={() => {
                                            // props.navigation.navigate('VenueMap');
                                        }}
                                        style={styles.view1Host}>
                                        <Image style={styles.view3Host} source={require('../../assets/man_venue_list.png')} />
                                        <View style={styles.view2Host}>
                                            <Image source={require('../../assets/venueimg.png')} style={{ height: '100%', width: '100%' }} />
                                        </View>

                                        <TouchableOpacity
                                        //  onPress={() => { onChangeLike(1) }}
                                        >
                                            <Image
                                                style={styles.view4Host}
                                                source={require('../../assets/venue_heart.png')}
                                            // source={like ? require('../../assets/venue_red_heart.png') : require('../../assets/venue_heart.png')}
                                            />
                                        </TouchableOpacity>

                                        <View style={styles.view5Host}>
                                            <Text style={styles.txt1Host}>{checkLength(text1)}</Text>
                                        </View>
                                        <View style={styles.view5_1Host}>
                                            <Text style={styles.txt2Host}>#Lounge #Party #Gathering</Text>
                                        </View>
                                        <View style={styles.view5_1Host}>
                                            <Text style={styles.txt2_1Host}>18:00 - 02:00</Text>
                                        </View>
                                        <View style={styles.view5_1Host}>
                                            <Text style={styles.txt2_1Host}>{checkLength1(text4)}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                            sliderWidth={screenWidth}
                            itemWidth={screenWidth}
                            inactiveSlideScale={1}
                            firstItem={0}
                        />
                    </View>

                    <Text style={styles.txt9_1}>Instagram</Text>

                    <View style={{
                        marginTop: 16
                    }}>
                        {entriesSplitter()}
                        <Carousel
                            ref={carousel}
                            data={slides}
                            renderItem={({ item, index }) => {
                                return (
                                    <View style={{
                                        flexDirection: "row",
                                        marginLeft: -10,
                                    }} key={index}>
                                        {item.map((item2, index2) => {
                                            return <Image
                                                key={index2}
                                                aspectRatio={1}
                                                source={{ uri: item2 }}
                                                style={{ width: 100 * scale, height: 100 * scale, marginLeft: 10 }} />;
                                        })}
                                    </View>
                                )
                            }}
                            sliderWidth={screenWidth}
                            itemWidth={screenWidth - 40}
                            inactiveSlideScale={1}
                            firstItem={0}
                        />
                    </View>

                    <Text style={styles.txt9_2}>New</Text>
                    <VenueTeamItem
                        teamNewItem={teamNewItem}
                        navigation={navigation}
                    />

                </ScrollView>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    view1: {
        flexDirection: "row",
        width: '100%',
        height: 48 * scale,
        alignItems: "center",
        backgroundColor: '#FFFFFF',
        borderBottomWidth: 1,
        borderBottomColor: '#F2F2F2',
    },
    view2: {
        width: 128 * scale,
        height: 128 * scale,
        borderRadius: 64 * scale,
        marginTop: 16,
        alignSelf: "center",
        backgroundColor: '#BB6BD9',
        // shadowColor: '',
    },
    view3: {
        height: 36 * scale,
        marginRight: 16,
        marginLeft: 37,
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 16,
        alignItems: "center"
    },
    view4: {
        width: 156 * scale,
        height: 36 * scale,
        borderRadius: 5 * scale,
        backgroundColor: '#00A9F4',
        alignItems: "center",
        justifyContent: "center",
    },
    view5: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 16,
        backgroundColor: '#FFFFFF',
    },
    view6: {
        marginLeft: 16,
        marginRight: 16,
        height: 25,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginTop: 24 * scale,
    },
    view7: {},
    view8: {},
    txt1: {
        fontSize: 18,
        fontFamily: 'SourceSansPro-SemiBold',
        lineHeight: 22.63,
        color: '#555555',
        marginLeft: 5.62 * scale,
    },
    txt2: {
        lineHeight: 30.17,
        fontSize: 24,
        fontFamily: 'SourceSansPro-SemiBold',
        color: '#333333',
        textAlign: "center",
        marginTop: 16,
    },
    txt2_2: {
        lineHeight: 15.08,
        fontSize: 12,
        fontFamily: 'SourceSansPro-Regular',
        color: '#333333',
    },
    txt3: {
        lineHeight: 20.11,
        fontSize: 16,
        fontFamily: 'SourceSansPro-Regular',
        color: '#4F4F4F',
        textAlign: "center",
        marginTop: 4,
    },
    txt4: {
        lineHeight: 17.6,
        fontSize: 14,
        fontFamily: 'SourceSansPro-Light',
        color: '#000000',
        textAlign: "center",
        marginTop: 8,
    },
    txtFollow: {
        fontSize: 18,
        fontFamily: 'SourceSansPro-SemiBold',
        lineHeight: 22.63,
        color: '#333333',
    },
    txt5: {
        fontSize: 16,
        fontFamily: 'SourceSansPro-SemiBold',
        lineHeight: 20.11,
        color: '#FFFFFF',
    },
    txt6: {
        fontSize: 20,
        fontFamily: 'SourceSansPro-SemiBold',
        lineHeight: 25.14,
        color: '#333333',
        marginLeft: 16,
        marginTop: 41
    },
    txt7: {
        fontSize: 16,
        fontFamily: 'SourceSansPro-SemiBold',
        lineHeight: 20.11,
        color: '#333333',
    },
    txt8: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Regular',
        lineHeight: 17.6,
        color: '#333333',
    },
    txt9: {
        lineHeight: 25.11,
        fontSize: 20,
        fontFamily: 'SourceSansPro-SemiBold',
        color: '#333333',
    },
    txt9_1: {
        lineHeight: 25.11,
        fontSize: 20,
        fontFamily: 'SourceSansPro-SemiBold',
        color: '#333333',
        marginLeft: 16,
        marginTop: 16,
    },
    txt9_2: {
        lineHeight: 25.11,
        fontSize: 20,
        fontFamily: 'SourceSansPro-SemiBold',
        color: '#333333',
        marginLeft: 16,
        marginTop: 24,
    },
    txt10: {
        lineHeight: 20.11,
        fontSize: 16,
        fontFamily: 'SourceSansPro-Regular',
        color: '#555555',
    },

    view1Host: {
        marginLeft: 16 * scale,
        marginRight: 16 * scale,
        height: 240 * scale,
        borderRadius: 4 * scale,
        backgroundColor: '#FAFAFA',
        marginTop: 16 * scale,
    },
    view2Host: {
        height: 84 * scale,
        marginLeft: 16 * scale,
        marginRight: 16 * scale,
        marginTop: 16 * scale,
    },
    view3Host: {
        height: 64 * scale,
        width: 64 * scale,
        position: 'absolute',
        top: 68 * scale,
        left: 32 * scale,
        // borderRadius: 4 * scale,
        zIndex: 9999,
    },
    view4Host: {
        height: 18.33 * scale,
        width: 21 * scale,
        // backgroundColor: 'gray',
        marginTop: 11 * scale,
        marginLeft: 290 * scale,
    },
    view5Host: {
        marginLeft: 16 * scale,
        marginRight: 16 * scale,
        height: 24 * scale,
        // backgroundColor: 'green',
        marginTop: 10.67 * scale,
    },
    view5_1Host: {
        marginLeft: 16 * scale,
        marginRight: 16 * scale,
        height: 20 * scale,
        // backgroundColor: 'green',
        marginTop: 2 * scale,
    },
    txt1Host: {
        lineHeight: 20.11,
        fontSize: 16,
        fontFamily: 'SourceSansPro-SemiBold',

        color: '#333333',
    },
    txt2Host: {
        lineHeight: 20.11,
        fontSize: 16,
        fontFamily: 'SourceSansPro-Regular',
        color: '#333333',
        fontStyle: 'normal',
    },
    txt2_1Host: {
        lineHeight: 20.11,
        fontSize: 16,
        fontFamily: 'SourceSansPro-Regular',
        color: '#4F4F4F',
        fontStyle: 'normal',
    },
})