import React, { useRef, useState, useEffect } from 'react'
import { View, Alert, Modal, Animated, TextInput, Text, StyleSheet, SafeAreaView, TouchableOpacity, Image, Dimensions, KeyboardAvoidingView, ScrollView, Keyboard } from 'react-native'
import * as Colors from '../../constants/Colors';
const { width, height } = Dimensions.get('window')
import Line from '../../screens_view/Line'
import YouTube from 'react-native-youtube';
import SafeView from '../../screens_view/SafeView'
import ImagePicker from 'react-native-image-crop-picker'
import ExtraDimensions from 'react-native-extra-dimensions-android';
import ImageResizer from 'react-native-image-resizer'
import RNFS from 'react-native-fs'
import { convertLanguage } from '../../services/Helper';
import { useSelector, useDispatch } from "react-redux";
import { actSaveVenueNews, actEditVenueNews, onUpdateVenueNews } from '../../actions/venue'
import Toast from 'react-native-simple-toast';
import Loading from '../../screens_view/Loading'

const scale = width / 360

const WriteVenueNews = (props) => {
  const scrollView = useRef(null)
  const [inputHeight, setInputHeight] = useState(0)
  const [Content, setContent] = useState('')
  const [arrImages, setArrImages] = useState({})
  const [Link, setLink] = useState('')
  const [Type, setType] = useState('')
  const [LinkError, setLinkError] = useState('')
  const [PhotoBase, setPhotoBase] = useState([])
  const [Images, setImages] = useState([])
  const [PhotoId, setPhotoId] = useState([])
  const [modalWarning, setModalWarning] = useState(false)
  const [modalYoutubeLink, setModalYoutubeLink] = useState(false)
  const keyboardHeight = useRef(new Animated.Value(0)).current;

  const { language } = useSelector(state => state.language)
  const { iso_code } = useSelector(state => state.global)
  const dispatch = useDispatch()
  const venue = useSelector(state => state.venue)
  const { isFetching, loading, loadingCreateVenueNews, loadingEditVenueNews } = venue
  useEffect(() => {
    if (props.route.params.VenueNewsId && props.route.params.VenueNewsId !== undefined) {
      dispatch(actEditVenueNews(props.route.params.VenueNewsId)).then((res) => {
        if (res && res.status === 200) {
          setImages(res.data.Images)
          setArrImages(res.data.Images.concat(PhotoBase))
        }
      })
    }
  }, [])

  // useEffect(() => {
  //   if (props.route.params.VenueNewsId && props.route.params.VenueNewsId !== undefined) {
  //     dispatch(actEditVenueNews(props.route.params.VenueNewsId))
  //   }
  // }, [])



  useEffect(() => {
    if (props.route.params.VenueNewsId && props.route.params.VenueNewsId !== undefined && venue.itemVenueNewsEditing) {
      setPhotoId(getPhotoId(venue.itemVenueNewsEditing.Images))
      setContent(venue.itemVenueNewsEditing.Contents)
      setLink(venue.itemVenueNewsEditing.Link ? venue.itemVenueNewsEditing.Link : '')
      setType(venue.itemVenueNewsEditing.Images.length > 0 ? 'photo' : venue.itemVenueNewsEditing.Link ? 'video' : '')
    }
  }, [venue.itemVenueNewsEditing]);

  useEffect(() => {
    setArrImages(Images.concat(PhotoBase))
  }, [PhotoBase]);

  const keyboardWillShow = (event) => {
    Animated.parallel([
      Animated.timing(keyboardHeight, {
        duration: event.duration,
        toValue: event.endCoordinates.height,
      }),
    ]).start();
  };
  const keyboardWillHide = (event) => {
    Animated.parallel([
      Animated.timing(keyboardHeight, {
        duration: event.duration,
        toValue: 0,
      }),
    ]).start()
  };

  useEffect(() => {
    var keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', keyboardWillShow);
    var keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', keyboardWillHide);
    return () => {
      keyboardWillShowSub.remove();
      keyboardWillHideSub.remove();
    }
  }, []);

  const getPhotoId = (Images) => {
    var PhotoId = [];
    Images.forEach((Image) => {
      PhotoId.push(Image.Id)
    })
    return PhotoId;
  }

  const changeType = () => {
    if (Type === 'photo') {
      setImages([])
      setPhotoBase([])
      setPhotoId([])
      setType('video')
      setModalWarning(false)
      setModalYoutubeLink(true)
    } else {
      setType('photo')
      setLink('')
      pickImage()
    }
  }

  const selectImage = () => {
    if (Link.length > 0) {
      setModalWarning(true)
    } else {
      setType('photo')
      pickImage()
    }
  }

  const selectVideo = () => {
    if (PhotoBase.length > 0 || Images.length > 0) {
      setModalWarning(true)
    } else {
      setModalYoutubeLink(true)
      setType('video')
    }
  }

  const [VideoURL, setVideoURL] = useState('')

  const youtube_parser = (url) => {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return (match && match[7].length == 11) ? match[7] : false;
  }

  const onInsertVideo = async () => {
    setLinkError('')
    var Id = youtube_parser(VideoURL)
    if (Id) {
      await setModalYoutubeLink(false)
      await setVideoURL('')
      await setLink(Id)
      scrollView.scrollToEnd();
    } else {
      setLinkError(convertLanguage(language, 'link_not_found'))//language
    }
  }

  const pickImage = () => {
    ImagePicker.openPicker({
      multiple: true,
      maxFiles: 10 - arrImages.length
    }).then(images => {
      setModalWarning(false)

      var data = []
      images.map((image, index) => {
        ImageResizer.createResizedImage(image.path, 600, 800, 'JPEG', 80).then(async (uri) => {
          const base64image = await RNFS.readFile(uri.uri, 'base64');
          await data.push('data:image/png;base64,' + base64image)
          setPhotoBase([...PhotoBase, ...data])
        }).catch((err) => {
          console.log('err', err)
        })
      })
      setTimeout(() => {
        scrollView?.current?.scrollResponderScrollToEnd();
      }, 300);
    }).catch(e => { setModalWarning(false) });
  }
  const deleteImage = (index) => {
    PhotoBase.splice(index - Images.length, 1)
    setPhotoBase(PhotoBase)
    setArrImages(Images.concat(PhotoBase))
  }

  const deleteOldImage = (index) => {
    var index2 = PhotoId.indexOf(Images[index].Id);
    if (index2 !== -1) {
      PhotoId.splice(index2, 1)
    }
    Images.splice(index, 1);
    setImages(Images)
    setPhotoId(PhotoId)
    setArrImages(Images.concat(PhotoBase))
  }

  const checkButtonPost = () => {
    if (PhotoBase.length + Images.length + Link.length === 0 && Content.length === 0) {
      return false;
    }
    return true;
  }

  const validate = () => {
    // if (PhotoBase.length + Images.length > 0) return true;
    if (PhotoBase.length + Images.length + Link.length === 0 && Content.replace(/\s/g, '').length === 0) {
      Toast.showWithGravity(convertLanguage(language, 'validate_content'), Toast.SHORT, Toast.TOP)
      return false;
    }
    return true;
  }

  const warningThan10Photo = () => {
    Alert.alert(
      '',
      convertLanguage(language, 'choose_no_more_than_10_photos'),
      [
        {
          text: convertLanguage(language, 'ok'),
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      { cancelable: false },
    );
  }

  const onSaveVenueNews = () => {
    Keyboard.dismiss();
    if (!validate()) return;
    if (PhotoBase.length + Images.length > 10) {
      Alert.alert(
        '',
        convertLanguage(language, 'choose_no_more_than_10_photos'),
        [
          {
            text: convertLanguage(language, 'ok'),
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ],
        { cancelable: false },
      );
      return;
    }
    var body = {
      Contents: Content,
      PhotoBase,
      PhotoId,
      Link,
      iso_code
    }
    if (props.route.params.VenueNewsId && props.route.params.VenueNewsId !== undefined) {
      dispatch(onUpdateVenueNews(props.route.params.VenueNewsId, body, props.navigation))
        .then((res) => {
          if (res && res.status === 200) {
            props.navigation.goBack()
          }
        })
    } else {
      dispatch(actSaveVenueNews(props.route.params['Slug'], body, props.navigation))
        .then((res) => {
          if (res && res.status === 200) {
            props.navigation.goBack()
          }
        })
    }
  }

  return (
    <>
      <SafeView style={{ backgroundColor: Colors.BG, flex: 1 }}>
        <View style={{ alignSelf: 'stretch', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 0, position: 'relative', zIndex: 10 }}>
          <TouchableOpacity
            onPress={() => props.navigation.goBack()}
            style={{ minWidth: 48, minHeight: 48, justifyContent: 'center', alignItems: 'center' }}>
            <Image source={require('../../assets/icon_back.png')} style={{ width: 24, height: 24 }} />
          </TouchableOpacity>
          <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.TEXT_P }}></Text>
          <TouchableOpacity
            style={{ height: 35, justifyContent: 'center', alignItems: 'center', marginRight: 20 }}
            onPress={() => onSaveVenueNews()}
            disabled={isFetching || loadingCreateVenueNews || loadingEditVenueNews || !checkButtonPost()}
          >
            <Text style={{ fontSize: 16, color: isFetching || loadingCreateVenueNews || loadingEditVenueNews || !checkButtonPost() ? 'gray' : '#00A9F4', fontWeight: 'bold' }}>{convertLanguage(language, 'postNews')}</Text>
          </TouchableOpacity>
        </View>
        <Line />

        <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.OS === 'android' ? -444 : 0} style={{ flex: 1 }} >
          <ScrollView
            style={styles.boxInput}
            contentContainerStyle={{ paddingBottom: 16 }}
            ref={scrollView}>
            <TextInput
              style={[styles.ipContent, { height: Platform.OS === 'android' ? Math.max(208, inputHeight) : 208 }]}
              selectionColor="#bcbcbc"
              name="Content"
              value={Content}
              onChangeText={(Content) => setContent(Content)}
              multiline={true}
              onContentSizeChange={(event) => {
                setInputHeight(event.nativeEvent.contentSize.height)
              }}
            />
            {
              (arrImages.length > 0) &&
              <View>
                <View style={styles.boxImages}>
                  {
                    arrImages.map((item, index) => {
                      return <View style={[styles.boxItem, (index + 1) % 4 === 0 ? { marginRight: 0 } : {}]} key={index}>
                        <Image source={{ uri: item.Photos ? item.Photos.Large : item }} style={styles.image} />
                        <TouchableOpacity style={styles.btnClose} onPress={() => item.Photos ? deleteOldImage(index) : deleteImage(index)}>
                          <Image style={styles.ic_close} source={require('../../assets/close_img.png')} />
                        </TouchableOpacity>
                      </View>
                    })
                  }
                  <TouchableOpacity onPress={() => { arrImages.length >= 10 ? warningThan10Photo() : pickImage() }} style={[styles.boxItem, { borderWidth: 2, borderStyle: 'dashed', borderColor: '#BDBDBD', marginRight: 0, alignItems: 'center', justifyContent: 'center' }]}>
                    <Image source={require('../../assets/plus_img.png')} style={{ width: 32, height: 32 }} />
                    <Text style={{ color: '#BDBDBD', fontSize: 16 }}>{convertLanguage(language, 'attach_file')}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            }
            {
              Link.length > 0 &&
              <YouTube
                videoId={Link} // The YouTube video ID
                play={false} // control playback of video with true/false
                fullscreen={false} // control whether the video should play in fullscreen or inline
                style={{ alignSelf: 'stretch', height: (width - 16) * 9 / 16, marginHorizontal: 16, marginBottom: 16 }}
                apiKey={'AIzaSyASjD7n17ht9T7huOQ8V0GXbfqsz9xYWC0'}
              />
            }
          </ScrollView>
        </KeyboardAvoidingView>
        <Animated.View style={[styles.boxBottom, { position: 'absolute', marginBottom: keyboardHeight, bottom: 0, left: 0, width: '100%' }]}>
          <TouchableOpacity style={[styles.boxUploadImage]} onPress={() => arrImages.length >= 10 ? warningThan10Photo() : selectImage()}>
            <Image source={Type === 'photo' ? require('../../assets/upload-img-focus.png') : require('../../assets/upload-img.png')} style={styles.img_upload} />
            <Text style={[styles.txtUpload, Type === 'photo' ? { color: '#00A9F4' } : { color: '#828282' }]}>{convertLanguage(language, 'photo')}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.boxUploadImage]} onPress={() => selectVideo()}>
            <Image source={Type === 'video' ? require('../../assets/upload-video-focus.png') : require('../../assets/upload-video.png')} style={styles.img_upload} />
            <Text style={[styles.txtUpload, Type === 'video' ? { color: '#00A9F4' } : { color: '#828282' }]}>{convertLanguage(language, 'video')}</Text>
          </TouchableOpacity>
        </Animated.View>
        {
          modalWarning &&
          <Modal
            isVisible={true}
            transparent
            // backdropOpacity={0}
            animationType="none"
            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
          >
            <View style={{
              justifyContent: 'center', flex: 1, paddingHorizontal: 16, borderRadius: 20,
              backgroundColor: 'rgba(0,0,0,0.3)'
            }} >
              <View style={styles.modalContent}>
                <View style={styles.rowClose}>
                  <TouchableOpacity style={styles.btnCloseModal} onPress={() => { setModalWarning(false) }} >
                    <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                  </TouchableOpacity>
                </View>
                <View style={styles.contentModal}>
                  <Text style={styles.txtTitle}>{convertLanguage(language, 'replace_content_news')}</Text>
                  <View style={styles.boxButton}>
                    <TouchableOpacity style={styles.btnNo} onPress={() => { setModalWarning(false) }} >
                      <Text style={styles.txtNo}>{convertLanguage(language, 'no2')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnNo} onPress={() => changeType()} >
                      <Text style={styles.txtNo}>{convertLanguage(language, 'yes')}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
        }

        {
          modalYoutubeLink &&
          <Modal
            isVisible={true}
            backdropOpacity={0}
            animationType="none"
            transparent
            deviceHeight={Platform.OS === 'android' ? ExtraDimensions.getRealWindowHeight() : height}
          >
            <View style={{
              justifyContent: 'center', flex: 1, paddingHorizontal: 16, borderRadius: 20,
              backgroundColor: 'rgba(0,0,0,0.3)'
            }} >
              <Animated.View style={[styles.modalContent, { marginBottom: keyboardHeight }]}>
                <View style={[styles.rowClose, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]}>
                  <Text style={[styles.contentModal, { fontSize: 20, fontWeight: '600', marginTop: 5 }]}>{convertLanguage(language, 'insert_video')}</Text>
                  <TouchableOpacity style={styles.btnCloseModal} onPress={() => { setModalYoutubeLink(false) }} >
                    <Image style={styles.iconClose} source={require('../../assets/close.png')} />
                  </TouchableOpacity>
                </View>
                <View style={[styles.contentModal, { alignItems: 'flex-start' }]}>
                  <Text style={[styles.txtTitle, { textAlign: 'left', marginBottom: 5, fontWeight: '600' }]}>{convertLanguage(language, 'youtube_video_url')}</Text>
                  <TextInput
                    style={styles.ipURL}
                    selectionColor="#bcbcbc"
                    name="VideoURL"
                    value={VideoURL}
                    onChangeText={(VideoURL) => setVideoURL(VideoURL)}
                  />
                  {
                    LinkError.length > 0 &&
                    <Text style={styles.txtError}>{LinkError}</Text>
                  }
                  <TouchableOpacity disabled={VideoURL.length === 0} style={styles.btnInsert} onPress={() => onInsertVideo()} >
                    <Text style={styles.txtInsert}>{convertLanguage(language, 'btn_insert_video')}</Text>
                  </TouchableOpacity>
                </View>
              </Animated.View>
            </View>
          </Modal>
        }
      </SafeView>
      {
        (isFetching || loadingCreateVenueNews || loadingEditVenueNews) &&
        <Loading />
      }
    </>
  )
}
const styles = StyleSheet.create({
  content: {
    // flex: 1,
  },
  boxInput: {
    paddingTop: 24,
    marginBottom: 56,
  },
  boxImages: {
    marginBottom: 5,
    marginHorizontal: 16,
    flexDirection: 'row',
    flexWrap: 'wrap',
    flexGrow: 4,
  },
  boxItem: {
    width: (width - 80) / 4,
    height: (width - 80) / 4,
    marginRight: 16,
    borderRadius: 4,
    marginBottom: 16,
  },
  image: {
    width: (width - 80) / 4,
    height: (width - 80) / 4
  },
  btnClose: {
    position: 'absolute',
    top: -8,
    right: -8,
    width: 16,
    height: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ic_close: {
    width: 16,
    height: 16
  },
  boxUploadImage: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 0.5
    // marginTop: 15
  },
  img_upload: {
    width: 24,
    height: 24,
    marginRight: 4
  },
  ipContent: {
    fontSize: 15,
    color: '#333333',
    padding: 8,
    height: 208,
    textAlignVertical: 'top',
    borderWidth: 1,
    borderColor: '#E0E0E0',
    marginHorizontal: 16,
    borderRadius: 4,
    marginBottom: 16
  },
  boxControl: {
    paddingTop: 10,
    alignItems: 'flex-end',
    justifyContent: 'center',
    flexDirection: 'row',
    alignSelf: 'flex-end'
  },
  boxInsert: {
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 1,
    borderWidth: 1,
    borderColor: '#03a9f4',
    backgroundColor: '#03a9f4'
  },
  txtInsert: {
    color: '#FFFFFF',
    fontSize: 15,
    fontWeight: 'bold',
    paddingLeft: 5,
    paddingRight: 5
  },
  boxCancel: {
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 1,
    borderWidth: 1,
    borderColor: '#333333',
    marginRight: 5,
  },
  txtCancel: {
    color: '#333333',
    fontSize: 14,
    fontWeight: 'bold',
    paddingLeft: 5, paddingRight: 5
  },
  boxBottom: {
    // flex: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: -2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 4,
    height: 56,
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#FFFFFF'
  },
  txtUpload: {
    color: '#00A9F4',
    fontSize: 16,
    fontWeight: '600'
  },
  modalContent: {
    backgroundColor: "white",
    borderRadius: 5
  },
  contentModal: {
    padding: 22,
    justifyContent: "center",
    borderColor: "rgba(0, 0, 0, 0.3)",
    paddingTop: 10,
    alignItems: 'center'
  },
  rowClose: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  btnCloseModal: {
    margin: 10
  },
  iconClose: {
    width: 30,
    height: 30,
  },
  txtTitle: {
    fontSize: 14,
    color: '#333333',
    textAlign: 'center'
  },
  boxButton: {
    marginTop: 24,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%'
  },
  btnDiscard: {
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    flex: 0.45,
    borderWidth: 1,
    borderColor: '#EB5757'
  },
  txtDiscard: {
    color: '#EB5757',
    fontSize: 16
  },
  btnNo: {
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    flex: 0.45,
    borderWidth: 1,
    borderColor: '#4F4F4F'
  },
  txtNo: {
    color: '#4F4F4F',
    fontSize: 16
  },
  ipURL: {
    fontSize: 15,
    color: '#333333',
    padding: 8,
    borderWidth: 1,
    borderColor: '#E0E0E0',
    borderRadius: 4,
    width: '100%'
  },
  btnInsert: {
    alignSelf: 'flex-end',
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    backgroundColor: '#03a9f4',
    marginTop: 16,
    paddingHorizontal: 16
  },
  txtInsert: {
    color: '#FFFFFF',
    fontSize: 16,
  },
  txtError: {
    fontSize: 12,
    color: 'red',
    marginTop: 5
  }
})

export default WriteVenueNews