import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';

export default class Error extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#FFFFFF', justifyContent: 'center', alignItems: 'center', paddingBottom: 60 }}>
                <Image source={require('../assets/500.png')} style={{ width: 296, height: 153, marginBottom: 30 }} />
                <Text style={{ color: '#828282', fontSize: 18, textAlign: 'center', fontWeight: 'bold' }}>{'Internal Server Error!'}</Text>
                <Text style={{ color: '#828282', fontSize: 14, textAlign: 'center' }}>{'Something went wrong'}</Text>
                <Text style={{ color: '#828282', fontSize: 14, textAlign: 'center' }}>{'Try to refresh this page or contact our administrator if the problem persists.'}</Text>
                <TouchableOpacity onPress={() => this.props.onPress()} style={{ width: 101, height: 36, borderRadius: 4, borderColor: '#00A9F4', borderWidth: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginBottom: 20 }}>
                    <Text style={{ color: '#00A9F4', fontSize: 14 }}>Reload</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
