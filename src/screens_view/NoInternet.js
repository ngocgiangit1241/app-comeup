import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';

export default class NoInternet extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#FFFFFF', justifyContent: 'center', alignItems: 'center' }}>
                <Image source={require('../assets/wifi_disconnect_disabled.png')} style={{ width: 110, height: 93, marginBottom: 30 }} />
                <Text style={{ color: '#828282', fontSize: 14, textAlign: 'center' }}>{'Network unavailable'}</Text>
                <Text style={{ color: '#828282', fontSize: 14, textAlign: 'center' }}>{'Check your internet connection and try again.'}</Text>
                <TouchableOpacity onPress={() => this.props.onPress()} style={{ width: 101, height: 36, borderRadius: 4, borderColor: '#00A9F4', borderWidth: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginBottom: 20 }}>
                    <Text style={{ color: '#00A9F4', fontSize: 14 }}>Reload</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
