import AsyncStorage from '@react-native-community/async-storage';
import * as Config from '../constants/Config';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
// import NavigationService from '../router/NavigationService';
// import * as Types from '../constants/ActionType';
callApi = async (endpoint, method = 'GET', body, header = '') => {
    console.log('==============================');
    console.log('api', `${Config.API_URL}/${endpoint}`);
    console.log('==============================');
    return axios({
        method: method,
        url: `${Config.API_URL}/${endpoint}`,
        data: body,
        headers: header,
        timeout: 90000,
    }).then(response => {
        return response;
    }).catch((error) => {
        if (error.response && error.response.status == 401) {
            var res = {
                data: {
                    status: 401,
                    message: 'Authentication required'
                },
                status: 401,
            }
            return res;
        }
        return {
            data: {
                status: 500,
                errors: {
                    message: 'Connect server error',
                }
            }
        }
    });
}

callApiWithHeader = async (endpoint, method = 'GET', body, requiredLogin = false, upload = '') => {
    const auKey = await AsyncStorage.getItem('auKey');
    const auValue = await AsyncStorage.getItem('auValue');
    console.log('==============================');
    console.log('api', `${Config.API_URL}/${endpoint}`);
    console.log('==============================');
    let header = {}
    if (upload === true) {
        header = {
            [auKey]: auValue,
            'Content-Type': 'multipart/form-data',
        }
    } else {

        header = {
            [auKey]: auValue,
        }
    }
    if (requiredLogin && (!auKey || !auValue)) {
        // NavigationService.navigate('Login', {});
        var res = {
            data: {
                status: 401,
                message: 'Authentication required'
            },
            status: 401,
        }
        return res;
    } else {
        return axios({
            method: method,
            url: `${Config.API_URL}/${endpoint}`,
            data: body,
            headers: header,
            timeout: 90000,
            // withCredentials: true
        }).then(response => {
            console.log('responsehttp', response)
            return response;
        }).catch(error => {
            console.log('error', error)
            if (error.response && error.response.status == 401) {
                var res = {
                    data: {
                        status: 401,
                        message: 'Authentication required'
                    },
                    status: 401,
                }
                return res;
            } else if (error.response) {
                Toast.showWithGravity(error.response.status + ' Connect errors', Toast.LONG, Toast.TOP)
            } else {
                Toast.showWithGravity(JSON.stringify(error.message), Toast.LONG, Toast.TOP)
            }
        });
    }
};


module.exports = {
    callApi,
    callApiWithHeader
};
