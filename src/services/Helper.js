import I18n from '../i18n/i18n';
import { Platform } from 'react-native';


getTag = (arr) => {
    if (!arr) return null
    let text = ''
    //  arr.map((e) => {
    //     if (e) {
    //         return ('#' + e.HashTagName + '  ')
    //     }
    //     return ' '
    // })
    arr.forEach(item => {
        text += '#' + item.HashTagName + ' '
    })
    return text
}

dateNow = () => {
    var date = new Date();
    var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    var month = (1 + date.getMonth()) < 10 ? '0' + (1 + date.getMonth()) : 1 + date.getMonth();
    var now = date.getFullYear() + '-' + month + '-' + day;
    return now;
}

datetimeNow = () => {
    var date = new Date();
    var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    var month = (1 + date.getMonth()) < 10 ? '0' + (1 + date.getMonth()) : 1 + date.getMonth();
    var hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    var minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    var now = date.getFullYear() + '-' + month + '-' + day + ' ' + hours + ':' + minutes;
    return now;
}

findIndex = (datas, value) => {
    var result = -1;
    datas.forEach((data, index) => {
        if (data === value) {
            result = index;
            return result;
        }
    });
    return result;
}

formatNumber = (num) => {
    var num = num ? num : 0;
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

convertDate = (datetime1, datetime2) => {
    if (datetime1 && datetime2) {
        var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        var date_arr1 = datetime1.split(" ");
        var date1 = date_arr1[0];
        var time1 = date_arr1[1];
        var year1 = date1.split("-")[0];
        var month1 = date1.split("-")[1];
        var day1 = date1.split("-")[2];
        var date_arr2 = datetime2.split(" ");
        var date2 = date_arr2[0];
        var time2 = date_arr2[1];
        var year2 = date2.split("-")[0];
        var month2 = date2.split("-")[1];
        var day2 = date2.split("-")[2];
        if (year1 == year2 && month1 == month2 && day1 == day2) {
            return year1 + '.' + month1 + '.' + day1 + ' (' + days[new Date(date1).getDay()] + ') ' + time1 + ' ~ ' + time2;
        } else if (year1 == year2 && month1 == month2 && day1 != day2) {
            return year1 + '.' + month1 + '.' + day1 + ' (' + days[new Date(date1).getDay()] + ') ' + time1 + ' ~ ' + day2 + ' (' + days[new Date(date2).getDay()] + ') ' + time2;
        } else if (year1 == year2 && month1 != month2 && day1 != day2) {
            return year1 + '.' + month1 + '.' + day1 + ' (' + days[new Date(date1).getDay()] + ') ' + time1 + ' ~ ' + month2 + '.' + day2 + ' (' + days[new Date(date2).getDay()] + ') ' + time2;
        } else {
            return year1 + '.' + month1 + '.' + day1 + ' (' + days[new Date(date1).getDay()] + ') ' + time1 + ' ~ ' + year2 + '.' + month2 + '.' + day2 + ' (' + days[new Date(date2).getDay()] + ') ' + time2;
        }
    } else {
        return '';
    }
}

convertDate2 = (datetime1, language = 'en') => {
    if (datetime1) {
        var days = ["sunday1", "monday1", "tueday1", "wednesday1", "thursday1", "friday1", "saturday1"];
        // var monthNames = ["", "01", "02", "03", "04", "05", "06",
        //     "07", "08", "09", "10", "11", "12"
        // ];
        var monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        var date_arr1 = datetime1.split(" ");
        var date1 = date_arr1[0];
        var time1 = date_arr1[1];
        var year1 = date1.split("-")[0];
        var month1 = date1.split("-")[1];
        var day1 = date1.split("-")[2];

        console.log('date1', date1);

        var monthAndroid1 = ''
        if (month1 < 10) {
            month1 = month1.substring(1,);
        }
        switch (language) {
            case 'vi':
                monthAndroid1 = 'Th' + month1
                break;
            case 'en':
                monthAndroid1 = monthNames[month1]
                break;
            case 'ko':
                monthAndroid1 = month1.length === 1 ? "0" + month1 : month1
                break;
            default:
                break;
        }
        return year1 + '.' + monthAndroid1 + '.' + day1 + ' (' + convertLanguage(language, days[new Date(year1).getDay()]) + ') ' + time1
    } else {
        return '';
    }
}
export const convertDate4 = (datetime1, datetime2, language) => {
    if (datetime1 && datetime2) {
        var days = ["sunday1", "monday1", "tueday1", "wednesday1", "thursday1", "friday1", "saturday1"];
        var date_arr1 = datetime1.split(" ");
        var date1 = date_arr1[0];
        var time1 = date_arr1[1];
        var year1 = date1.split("-")[0];
        var month1 = date1.split("-")[1];
        var day1 = date1.split("-")[2];
        var date_arr2 = datetime2.split(" ");
        var date2 = date_arr2[0];
        var time2 = date_arr2[1];
        var year2 = date2.split("-")[0];
        var month2 = date2.split("-")[1];
        var day2 = date2.split("-")[2];
        var monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];


        var monthAndroid1 = ''
        var monthAndroid2 = ''
        if (month1 < 10) {
            month1 = month1.substring(1,);
        }
        if (month2 < 10) {
            month2 = month2.substring(1,);
        }
        switch (language) {
            case 'vi':
                monthAndroid1 = 'Th' + month1
                monthAndroid2 = 'Th' + month2
                break;
            case 'en':
                monthAndroid1 = monthNames[month1]
                monthAndroid2 = monthNames[month2]
                break;
            case 'ko':
                // monthAndroid1 = month1 + "월"
                // monthAndroid2 = month2 + "월"
                monthAndroid1 = month1.length === 1 ? "0" + month1 : month1
                monthAndroid2 = month2.length === 1 ? "0" + month2 : month2
                break;
            default:
                break;
        }
        if (year1 === year2 && month1 === month2 && day1 === day2) {
            return monthAndroid1 + '.' + day1 + ' (' + convertLanguage(language, days[new Date(date1).getDay()]) + ') ' + time1 + ' ~ ' + time2
        } else if (year1 === year2) {
            return monthAndroid1 + '.' + day1 + ' (' + convertLanguage(language, days[new Date(date1).getDay()]) + ') ' + time1 + ' ~ ' + monthAndroid2 + '.' + day2 + ' (' + convertLanguage(language, days[new Date(date2).getDay()]) + ') ' + time2
        } else {
            return monthAndroid1 + '.' + day1 + ' (' + convertLanguage(language, days[new Date(date1).getDay()]) + ') ' + time1 + ' ~ ' + year2 + '.' + monthAndroid2 + '.' + day2 + ' (' + convertLanguage(language, days[new Date(date2).getDay()]) + ') ' + time2
        }
    }

    //     } else {
    //         return '';
    //     }
}
convertDate5 = (datetime1, datetime2, language) => {
    if (datetime1 && datetime2) {
        var days = ["sunday1", "monday1", "tueday1", "wednesday1", "thursday1", "friday1", "saturday1"];
        var date_arr1 = datetime1.split(" ");
        var date1 = date_arr1[0];
        var time1 = date_arr1[1];
        var year1 = date1.split("-")[0];
        var month1 = date1.split("-")[1];
        var day1 = date1.split("-")[2];
        var date_arr2 = datetime2.split(" ");
        var date2 = date_arr2[0];
        var time2 = date_arr2[1];
        var year2 = date2.split("-")[0];
        var month2 = date2.split("-")[1];
        var day2 = date2.split("-")[2];
        var monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];

        // if (Platform.OS === 'ios') {
        //     if (year1 === year2 && month1 === month2 && day1 === day2) {
        //         return year1 + '.' + new Date(month1).toLocaleString(language, { month: 'short' }).replace('g ', '') + '.' + day1 + ' ' + time1 + ' - ' + time2
        //     } else if (year1 === year2) {
        //         return year1 + '.' + new Date(month1).toLocaleString(language, { month: 'short' }).replace('g ', '') + '.' + day1 + ' ' + time1 + ' - ' + new Date(month2).toLocaleString(language, { month: 'short' }).replace('g ', '') + '.' + day2 + ' ' + time2
        //     } else {
        //         return year1 + '.' + new Date(month1).toLocaleString(language, { month: 'short' }).replace('g ', '') + '.' + day1 + ' ' + time1 + ' - ' + year2 + '.' + new Date(month2).toLocaleString(language, { month: 'short' }).replace('g ', '') + '.' + day2 + ' ' + time2
        //     }
        // } else {
        var monthAndroid1 = ''
        var monthAndroid2 = ''
        if (month1 < 10) {
            month1 = month1.substring(1,);
        }
        if (month2 < 10) {
            month2 = month2.substring(1,);
        }
        switch (language) {
            case 'vi':
                monthAndroid1 = 'Th' + month1
                monthAndroid2 = 'Th' + month2
                break;
            case 'en':
                monthAndroid1 = monthNames[month1]
                monthAndroid2 = monthNames[month2]
                break;
            case 'ko':
                // monthAndroid1 = month1 + "월"
                // monthAndroid2 = month2 + "월"
                monthAndroid1 = month1.length === 1 ? "0" + month1 : month1
                monthAndroid2 = month2.length === 1 ? "0" + month2 : month2
                break;
            default:
                break;
        }
        if (year1 === year2 && month1 === month2 && day1 === day2) {
            return year1 + '.' + monthAndroid1 + '.' + day1 + ' (' + convertLanguage(language, days[new Date(date1).getDay()]) + ') ' + time1 + ' - ' + time2
        } else if (year1 === year2) {
            return year1 + '.' + monthAndroid1 + '.' + day1 + ' (' + convertLanguage(language, days[new Date(date1).getDay()]) + ') ' + time1 + ' - ' + monthAndroid2 + '.' + day2 + ' (' + convertLanguage(language, days[new Date(date2).getDay()]) + ') ' + time2
        } else {
            return year1 + '.' + monthAndroid1 + '.' + day1 + ' (' + convertLanguage(language, days[new Date(date1).getDay()]) + ') ' + time1 + ' - ' + year2 + '.' + monthAndroid2 + '.' + day2 + ' (' + convertLanguage(language, days[new Date(date2).getDay()]) + ') ' + time2
        }

    }
}
convertDate3 = (datetime1, datetime2, language) => {
    if (datetime1 && datetime2) {
        // var transDates = {
        //     'ko': ['일', '월', '화', '수', '목', '금', '토'],
        //     'vi': ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
        //     'en': ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        // }
        var days = ["sunday1", "monday1", "tueday1", "wednesday1", "thursday1", "friday1", "saturday1"];
        var date_arr1 = datetime1.split(" ");
        var date1 = date_arr1[0];
        var time1 = date_arr1[1];
        var year1 = date1.split("-")[0];
        var month1 = date1.split("-")[1];
        var day1 = date1.split("-")[2];
        var date_arr2 = datetime2.split(" ");
        var date2 = date_arr2[0];
        var time2 = date_arr2[1];
        var year2 = date2.split("-")[0];
        var month2 = date2.split("-")[1];
        var day2 = date2.split("-")[2];
        var monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        // var monthNames = ["", "01", "02", "03", "04", "05", "06",
        //     "07", "08", "09", "10", "11", "12"
        // ];

        // if (Platform.OS === 'ios') {
        //     if (year1 === year2 && month1 === month2 && day1 === day2) {
        //         return year1 + '.' + new Date(month1).toLocaleString(language, { month: 'short' }).replace('g ', '') + '.' + day1 + ' ' + time1 + ' - ' + time2
        //     } else if (year1 === year2) {
        //         return year1 + '.' + new Date(month1).toLocaleString(language, { month: 'short' }).replace('g ', '') + '.' + day1 + ' ' + time1 + ' - ' + new Date(month2).toLocaleString(language, { month: 'short' }).replace('g ', '') + '.' + day2 + ' ' + time2
        //     } else {
        //         return year1 + '.' + new Date(month1).toLocaleString(language, { month: 'short' }).replace('g ', '') + '.' + day1 + ' ' + time1 + ' - ' + year2 + '.' + new Date(month2).toLocaleString(language, { month: 'short' }).replace('g ', '') + '.' + day2 + ' ' + time2
        //     }
        // } else {
        var monthAndroid1 = ''
        var monthAndroid2 = ''
        if (month1 < 10) {
            month1 = month1.substring(1,);
        }
        if (month2 < 10) {
            month2 = month2.substring(1,);
        }

        if (language.language) {
            language = language.language
        }

        switch (language) {
            case 'vi':
                monthAndroid1 = 'Th' + month1
                monthAndroid2 = 'Th' + month2
                break;
            case 'en':
                monthAndroid1 = monthNames[month1]
                monthAndroid2 = monthNames[month2]
                break;
            case 'ko':
                // monthAndroid1 = month1 + "월"
                // monthAndroid2 = month2 + "월"
                monthAndroid1 = month1.length === 1 ? "0" + month1 : month1
                monthAndroid2 = month2.length === 1 ? "0" + month2 : month2
                break;
            default:
                break;
        }
        if (year1 === year2 && month1 === month2 && day1 === day2) {
            return year1 + '.' + monthAndroid1 + '.' + day1 + ' (' + convertLanguage(language, days[new Date(date1).getDay()]) + ') ' + time1 + ' - ' + time2
        } else if (year1 === year2) {
            return year1 + '.' + monthAndroid1 + '.' + day1 + ' (' + convertLanguage(language, days[new Date(date1).getDay()]) + ') ' + time1 + ' - ' + monthAndroid2 + '.' + day2 + ' (' + convertLanguage(language, days[new Date(date2).getDay()]) + ') ' + time2
        } else {
            return year1 + '.' + monthAndroid1 + '.' + day1 + ' (' + convertLanguage(language, days[new Date(date1).getDay()]) + ') ' + time1 + ' - ' + year2 + '.' + monthAndroid2 + '.' + day2 + ' (' + convertLanguage(language, days[new Date(date2).getDay()]) + ') ' + time2
        }
    }

    // var x = monthNames[new Date(Date.UTC(2012, 1, 20, 3, 0, 0)).getMonth()]

    // return x
    // } else {
    //     return '';
    // }
}


convertLanguage = (lang, key, data = null) => {
    var i18n = I18n
    i18n.locale = lang;
    var string = i18n.t(key);
    if (data) {
        for (var key in data) {
            string = string.replace(":" + key, data[key]);
        }
    }
    return string;
}

convertDateTime = (datetime) => {
    if (datetime) {
        var date_arr = datetime.split(" ");
        var date = date_arr[0];
        var time = date_arr[1];
        var year = date.split("-")[2];
        var month = date.split("-")[1];
        var day = date.split("-")[0];
        return year + '-' + month + '-' + day + ' ' + time + ':00';
    }
    return '';
}

convertLanguage2 = (lang, key, data = null) => {

    return 'string';
}

module.exports = {
    getTag,
    dateNow,
    datetimeNow,
    findIndex,
    formatNumber,
    convertDate,
    convertLanguage,
    convertDateTime,
    convertDate2,
    convertDate3,
    convertDate4,
    convertDate5,
    convertLanguage2,
};


